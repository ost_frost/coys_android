//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fanzine.chat.sdk;

import com.fanzine.chat.models.blacklist.FCBlacklist;
import com.fanzine.chat.models.channels.FCChannel;
import com.fanzine.chat.models.channels.FCUserChannel;
import com.fanzine.chat.models.message.FCAttachment;
import com.fanzine.chat.models.message.FCMessage;
import com.fanzine.chat.models.user.FCUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class Routes {
    private static final String FIREBASE_CHAT_ROOT = "FIREBASE_CHAT_ROOT";
    private static final String USERS = "users";
    private static final String MESSAGES = "messages";
    private static final String CHANNELS = "channels";
    private static final String USERS_CHANNELS = "users_channels";
    private static final String BLACKLIST = "blacklist";
    private static final String ATTACHMENT = "attachments";

    public Routes() {
    }

    private static DatabaseReference getRootDataBaseReference() {
        return FirebaseDatabase.getInstance().getReference().child("FIREBASE_CHAT_ROOT");
    }

    private static StorageReference getRootStorageReference() {
        return FirebaseStorage.getInstance().getReference().child("FIREBASE_CHAT_ROOT");
    }

    public static class Attachment {
        public Attachment() {
        }

        public static DatabaseReference getDatabaseReference(String channelId, String messageId) {
            return Routes.Messages.getReferenceMessage(channelId, messageId).child("attachments");
        }

        public static DatabaseReference getDatabaseReference(String channelId, String messageId, String uid) {
            return Routes.Messages.getReferenceMessage(channelId, messageId).child("attachments").child(uid);
        }

        static StorageReference getStorageReference() {
            return Routes.getRootStorageReference().child("messages");
        }

        public static StorageReference getStorageReference(String uid) {
            return getStorageReference().child(uid);
        }

        public static FCAttachment getAttachment(FCMessage message, DataSnapshot snapshot) {
            FCAttachment attachment = (FCAttachment)snapshot.getValue(FCAttachment.class);
            attachment.setUid(snapshot.getKey());
            attachment.setMessageUid(message.getUid());
            attachment.setChannelUid(message.getChannelUid());
            return attachment;
        }
    }

    static class BlackList {
        static String USER = "user";
        static String CHANNEL = "channel";

        BlackList() {
        }

        static DatabaseReference getReference() {
            return Routes.getRootDataBaseReference().child("blacklist");
        }

        static Query getReferenceQuery(String field, String value) {
            return getReference().orderByChild(field).equalTo(value);
        }

        static DatabaseReference getReference(String channelId) {
            return getReference().child(channelId);
        }

        static FCBlacklist getBlacklist(DataSnapshot snapshot) {
            FCBlacklist blacklist = (FCBlacklist)snapshot.getValue(FCBlacklist.class);
            blacklist.setUid(snapshot.getKey());
            return blacklist;
        }
    }

    static class UsersChannels {
        static String USER = "user";
        static String CHANNEL = "channel";

        UsersChannels() {
        }

        static DatabaseReference getReference() {
            return Routes.getRootDataBaseReference().child("users_channels");
        }

        static DatabaseReference getReference(String channelId) {
            return getReference().child(channelId);
        }

        static Query getReferenceQuery(String field, String value) {
            return getReference().orderByChild(field).equalTo(value);
        }

        static FCUserChannel getUserChannel(DataSnapshot snapshot) {
            FCUserChannel user = (FCUserChannel)snapshot.getValue(FCUserChannel.class);
            user.setUid(snapshot.getKey());
            return user;
        }
    }

    public static class Messages {
        static final String TIMESTAMP = "timestamp";

        public Messages() {
        }

        static DatabaseReference getReference() {
            return Routes.getRootDataBaseReference().child("messages");
        }

        static DatabaseReference getReference(String channelId) {
            return getReference().child(channelId);
        }

        public static Query getReferenceSortedQuery(String channelId) {
            return getReference().child(channelId).orderByChild("timestamp");
        }

        public static DatabaseReference getReferenceMessage(String channelId, String messageId) {
            return getReference().child(channelId).child(messageId);
        }

        public static FCMessage getMessage(FCChannel channel, DataSnapshot snapshot) {
            FCMessage message = (FCMessage)snapshot.getValue(FCMessage.class);
            message.setUid(snapshot.getKey());
            message.setChannelUid(channel.getUid());
            return message;
        }
    }

    public static class Channels {
        public Channels() {
        }

        static DatabaseReference getReference() {
            return Routes.getRootDataBaseReference().child("channels");
        }

        public static DatabaseReference getReference(String channelId) {
            return getReference().child(channelId);
        }

        public static FCChannel getChannel(DataSnapshot snapshot) {
            FCChannel user = (FCChannel)snapshot.getValue(FCChannel.class);
            user.setUid(snapshot.getKey());
            return user;
        }
    }

    public static class Users {
        static final String PHONE = "phone";

        public Users() {
        }

        static DatabaseReference getReference() {
            return Routes.getRootDataBaseReference().child("users");
        }

        public static DatabaseReference getReference(String userId) {
            return Routes.getRootDataBaseReference().child("users").child(userId);
        }

        static Query getReferenceQuery(String field, String value) {
            return getReference().orderByChild(field).equalTo(value);
        }

        public static FCUser getUser(DataSnapshot snapshot) {
            FCUser user = (FCUser)snapshot.getValue(FCUser.class);
            if(user != null) {
                user.setUid(snapshot.getKey());
            }

            return user;
        }
    }
}
