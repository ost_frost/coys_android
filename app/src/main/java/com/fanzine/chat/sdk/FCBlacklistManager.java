//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fanzine.chat.sdk;

import android.support.annotation.NonNull;
import com.fanzine.chat.ChatSDK;
import com.fanzine.chat.interfaces.FCSuccessListener;
import com.fanzine.chat.interfaces.FCUsersListener;
import com.fanzine.chat.interfaces.blacklist.FCBlacklistListener;
import com.fanzine.chat.models.blacklist.FCBlacklist;
import com.fanzine.chat.models.channels.FCChannel;
import com.fanzine.chat.models.user.FCUser;
import com.fanzine.chat.sdk.Routes.BlackList;
import com.fanzine.chat.sdk.Routes.Channels;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class FCBlacklistManager implements ChildEventListener {
    private Set<String> blackList;
    private Query query;
    private FCBlacklistListener listener;

    public FCBlacklistManager() {
    }

    public void subscribe(FCUser user) {
        this.query = BlackList.getReferenceQuery(BlackList.USER, user.getUid());
        this.query.addChildEventListener(this);
        this.blackList = new HashSet();
    }

    public void unsubscribe() {
        if(this.query != null) {
            this.query.removeEventListener(this);
        }

        this.query = null;
    }

    public void setBlacklistListener(FCBlacklistListener listener) {
        this.listener = listener;
    }

    public boolean isUserInBlackList(FCChannel channel) {
        return this.blackList.contains(channel.getUid());
    }

    public void banUser(FCChannel channel, final FCUser user, final FCSuccessListener listener) {
        final FCUser currentUser = ChatSDK.getInstance().getCurrentSession().getCurrentUser();
        if(currentUser == null) {
            if(listener != null) {
                listener.onError(new NullPointerException("User isn't authorized"));
            }

        } else {
            Channels.getReference(channel.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                public void onDataChange(DataSnapshot dataSnapshot) {
                    FCChannel channel = Channels.getChannel(dataSnapshot);
                    if(channel != null) {
                        if(channel.isOneToOne()) {
                            FCBlacklistManager.this.addUserToBlackList(channel, user, listener);
                        } else if(channel.getOwner().equals(currentUser.getUid())) {
                            FCBlacklistManager.this.addUserToBlackList(channel, user, listener);
                        } else if(listener != null) {
                            listener.onError(new IllegalAccessException("You are not the owner of this channel"));
                        }
                    } else if(listener != null) {
                        listener.onError(new NullPointerException("This channel isn't exist"));
                    }

                }

                public void onCancelled(DatabaseError databaseError) {
                    if(listener != null) {
                        listener.onError(databaseError.toException());
                    }

                }
            });
        }
    }

    private void addUserToBlackList(final FCChannel channel, final FCUser user, final FCSuccessListener listener) {
        BlackList.getReferenceQuery(BlackList.CHANNEL, channel.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterator var2 = dataSnapshot.getChildren().iterator();

                FCBlacklist blacklist;
                do {
                    if(!var2.hasNext()) {
                        BlackList.getReference(UUID.randomUUID().toString()).setValue(new FCBlacklist(channel, user)).addOnCompleteListener(new OnCompleteListener<Void>() {
                            public void onComplete(@NonNull Task<Void> task) {
                                if(listener != null) {
                                    listener.onSuccess();
                                }

                            }
                        });
                        return;
                    }

                    DataSnapshot child = (DataSnapshot)var2.next();
                    blacklist = BlackList.getBlacklist(child);
                } while(!blacklist.getUser().equals(user.getUid()));

                if(listener != null) {
                    listener.onSuccess();
                }

            }

            public void onCancelled(DatabaseError databaseError) {
                if(listener != null) {
                    listener.onError(databaseError.toException());
                }

            }
        });
    }

    public void unbanUser(FCChannel channel, final FCUser user, final FCSuccessListener listener) {
        BlackList.getReferenceQuery(BlackList.CHANNEL, channel.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterator var2 = dataSnapshot.getChildren().iterator();

                while(var2.hasNext()) {
                    DataSnapshot child = (DataSnapshot)var2.next();
                    FCBlacklist blacklist = BlackList.getBlacklist(child);
                    if(blacklist.getUser().equals(user.getUid())) {
                        BlackList.getReference(blacklist.getUid()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                            public void onComplete(@NonNull Task<Void> task) {
                                if(listener != null) {
                                    listener.onSuccess();
                                }

                            }
                        });
                        return;
                    }

                    if(listener != null) {
                        listener.onSuccess();
                    }
                }

            }

            public void onCancelled(DatabaseError databaseError) {
                if(listener != null) {
                    listener.onError(databaseError.toException());
                }

            }
        });
    }

    public void getAllBannedUsers(FCChannel channel, final FCUsersListener listener) {
        BlackList.getReferenceQuery(BlackList.CHANNEL, channel.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<FCUser> users = new ArrayList();
                Iterator var3 = dataSnapshot.getChildren().iterator();

                while(var3.hasNext()) {
                    DataSnapshot child = (DataSnapshot)var3.next();
                    FCBlacklist blacklist = BlackList.getBlacklist(child);
                    if(blacklist != null) {
                        users.add(new FCUser(blacklist.getUser()));
                    }
                }

                if(listener != null) {
                    listener.onUsersReceived(users);
                }

            }

            public void onCancelled(DatabaseError databaseError) {
                if(listener != null) {
                    listener.onError(databaseError.toException());
                }

            }
        });
    }

    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        FCBlacklist blacklist = BlackList.getBlacklist(dataSnapshot);
        if(blacklist != null) {
            this.blackList.add(blacklist.getChannel());
            if(this.listener != null) {
                this.listener.addedToBlackList(blacklist.getChannel());
            }
        }

    }

    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
    }

    public void onChildRemoved(DataSnapshot dataSnapshot) {
        FCBlacklist blacklist = BlackList.getBlacklist(dataSnapshot);
        if(blacklist != null) {
            this.blackList.remove(blacklist.getChannel());
        }

    }

    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
    }

    public void onCancelled(DatabaseError databaseError) {
    }
}
