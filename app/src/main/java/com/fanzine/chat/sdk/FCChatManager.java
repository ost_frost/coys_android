//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fanzine.chat.sdk;

import android.support.annotation.NonNull;
import com.fanzine.chat.ChatSDK;
import com.fanzine.chat.interfaces.FCChannelsListener;
import com.fanzine.chat.interfaces.FCChatChannelCreateListener;
import com.fanzine.chat.interfaces.FCChatChannelMessageListener;
import com.fanzine.chat.interfaces.FCSuccessListener;
import com.fanzine.chat.interfaces.FCUsersListener;
import com.fanzine.chat.interfaces.getting.FCAttachmentsListener;
import com.fanzine.chat.interfaces.getting.FCUserGettingListener;
import com.fanzine.chat.models.channels.FCChannel;
import com.fanzine.chat.models.channels.FCUserChannel;
import com.fanzine.chat.models.message.FCAttachment;
import com.fanzine.chat.models.message.FCMessage;
import com.fanzine.chat.models.user.FCUser;
import com.fanzine.chat.sdk.Routes.Attachment;
import com.fanzine.chat.sdk.Routes.Channels;
import com.fanzine.chat.sdk.Routes.Messages;
import com.fanzine.chat.sdk.Routes.Users;
import com.fanzine.chat.sdk.Routes.UsersChannels;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.UploadTask.TaskSnapshot;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class FCChatManager {
    public FCChatManager() {
    }

    public void createChannel(List<FCUser> usersList, final FCChatChannelCreateListener listener) {
        if(usersList == null) {
            if(listener != null) {
                listener.onError(new NullPointerException("List of users cannot be null"));
            }

        } else if(usersList.size() == 0) {
            if(listener != null) {
                listener.onError(new RuntimeException("List of users must contain 1 or more users"));
            }

        } else {
            final FCUser currentUser = ChatSDK.getInstance().getCurrentSession().getCurrentUser();
            if(currentUser == null) {
                if(listener != null) {
                    listener.onError(new NullPointerException("Current user is null"));
                }
            } else {
                final List<FCUser> users = new ArrayList(usersList);
                users.add(currentUser);
                if(users.size() == 2) {
                    UsersChannels.getReferenceQuery(UsersChannels.USER, ((FCUser)users.get(0)).getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            final List<FCUserChannel> channelsOne = new ArrayList();
                            Iterator var3 = dataSnapshot.getChildren().iterator();

                            while(var3.hasNext()) {
                                DataSnapshot userSnapshot = (DataSnapshot)var3.next();
                                FCUserChannel userChannel = UsersChannels.getUserChannel(userSnapshot);
                                if(userChannel != null) {
                                    channelsOne.add(userChannel);
                                }
                            }

                            if(channelsOne.size() > 0) {
                                UsersChannels.getReferenceQuery(UsersChannels.USER, ((FCUser)users.get(1)).getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        List<FCUserChannel> channelsTwo = new ArrayList();
                                        Iterator var3 = dataSnapshot.getChildren().iterator();

                                        while(var3.hasNext()) {
                                            DataSnapshot userSnapshot = (DataSnapshot)var3.next();
                                            FCUserChannel userChannel = UsersChannels.getUserChannel(userSnapshot);
                                            if(userChannel != null) {
                                                channelsTwo.add(userChannel);
                                            }
                                        }

                                        if(channelsTwo.size() > 0) {
                                            var3 = channelsOne.iterator();

                                            while(var3.hasNext()) {
                                                FCUserChannel userChannel1 = (FCUserChannel)var3.next();
                                                Iterator var8 = channelsTwo.iterator();

                                                while(var8.hasNext()) {
                                                    FCUserChannel userChannel2 = (FCUserChannel)var8.next();
                                                    if(userChannel1.getChannel().equals(userChannel2.getChannel()) && userChannel1.getType().equals("one-to-one") && userChannel2.getType().equals("one-to-one")) {
                                                        FCChatManager.this.getChatRoom(userChannel1.getChannel(), listener);
                                                        return;
                                                    }
                                                }
                                            }

                                            FCChatManager.this.createChatRoom(FCChannel.createChannel(currentUser.getUid(), "one-to-one"), users, listener);
                                        } else {
                                            FCChatManager.this.createChatRoom(FCChannel.createChannel(currentUser.getUid(), "one-to-one"), users, listener);
                                        }

                                    }

                                    public void onCancelled(DatabaseError databaseError) {
                                        if(listener != null) {
                                            listener.onError(databaseError.toException());
                                        }

                                    }
                                });
                            } else {
                                FCChatManager.this.createChatRoom(FCChannel.createChannel(currentUser.getUid(), "one-to-one"), users, listener);
                            }

                        }

                        public void onCancelled(DatabaseError databaseError) {
                            if(listener != null) {
                                listener.onError(databaseError.toException());
                            }

                        }
                    });
                } else {
                    this.createChatRoom(FCChannel.createChannel(currentUser.getUid(), "one-to-many"), users, listener);
                }
            }

        }
    }

    private void createChatRoom(FCChannel channel, List<FCUser> users, FCChatChannelCreateListener listener) {
        String uid = UUID.randomUUID().toString();
        Channels.getReference(uid).setValue(channel);
        channel.setUid(uid);
        Iterator var5 = users.iterator();

        while(var5.hasNext()) {
            FCUser user = (FCUser)var5.next();
            UsersChannels.getReference().child(UUID.randomUUID().toString()).setValue(new FCUserChannel(user, channel));
        }

        if(listener != null) {
            listener.onChannelCreated(channel);
        }

    }

    private void getChatRoom(String userChannelId, final FCChatChannelCreateListener listener) {
        Channels.getReference(userChannelId).addListenerForSingleValueEvent(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(listener != null) {
                    listener.onChannelExists(Channels.getChannel(dataSnapshot));
                }

            }

            public void onCancelled(DatabaseError databaseError) {
                if(listener != null) {
                    listener.onError(databaseError.toException());
                }

            }
        });
    }

    public void getChannelMessages(final FCChannel channel, final FCChatChannelMessageListener listener) {
        Messages.getReferenceSortedQuery(channel.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<FCMessage> messages = new ArrayList();
                Iterator var3 = dataSnapshot.getChildren().iterator();

                while(var3.hasNext()) {
                    DataSnapshot child = (DataSnapshot)var3.next();
                    FCMessage message = Messages.getMessage(channel, child);
                    if(message != null) {
                        messages.add(message);
                    }
                }

                if(listener != null) {
                    listener.onMessagesReceived(messages);
                }

            }

            public void onCancelled(DatabaseError databaseError) {
                if(listener != null) {
                    listener.onError(databaseError.toException());
                }

            }
        });
    }

    public void sendMessage(FCChannel channel, FCMessage message) {
        Messages.getReference(channel.getUid()).child(UUID.randomUUID().toString()).setValue(message);
    }

    public void sendMessageWhithAttachments(FCChannel channel, FCMessage message, List<FCAttachment> attachments, FCSuccessListener listener) {
        if(attachments != null && attachments.size() != 0) {
            this.sendAttachment(channel, message, attachments, 0, listener);
        } else {
            this.sendMessage(channel, message);
        }

    }

    private void sendAttachment(final FCChannel channel, final FCMessage message, final List<FCAttachment> attachments, final int i, final FCSuccessListener listener) {
        try {
            final String uid = UUID.randomUUID().toString();
            Attachment.getStorageReference(uid).putStream(new FileInputStream(((FCAttachment)attachments.get(0)).getFile())).addOnCompleteListener(new OnCompleteListener<TaskSnapshot>() {
                public void onComplete(@NonNull Task<TaskSnapshot> task) {
                    ((FCAttachment)attachments.get(i)).setUid(uid);
                    if(i < attachments.size() - 1) {
                        FCChatManager.this.sendAttachment(channel, message, attachments, i + 1, listener);
                    } else {
                        String messageUid = UUID.randomUUID().toString();
                        Messages.getReference(channel.getUid()).child(messageUid).setValue(message);
                        Iterator var3 = attachments.iterator();

                        while(var3.hasNext()) {
                            FCAttachment attachment = (FCAttachment)var3.next();
                            Attachment.getDatabaseReference(channel.getUid(), messageUid, attachment.getUid()).setValue(attachment);
                        }

                        if(listener != null) {
                            listener.onSuccess();
                        }
                    }

                }
            }).addOnFailureListener(new OnFailureListener() {
                public void onFailure(@NonNull Exception e) {
                    if(listener != null) {
                        listener.onError(e);
                    }

                }
            });
        } catch (FileNotFoundException var7) {
            if(listener != null) {
                listener.onError(var7);
            }
        }

    }

    public void deleteMessage(final FCChannel channel, FCMessage message) {
        final FCUser currentUser = ChatSDK.getInstance().getCurrentSession().getCurrentUser();
        if(currentUser == null) {
            throw new NullPointerException("Current user is null");
        } else {
            Messages.getReference(channel.getUid()).child(message.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                public void onDataChange(DataSnapshot dataSnapshot) {
                    final FCMessage message = Messages.getMessage(channel, dataSnapshot);
                    if(message.getUser().equals(currentUser.getUid())) {
                        if(!message.getType().equals("message_type_default")) {
                            message.syncAttachmentsList(new FCAttachmentsListener() {
                                public void onAttachmentsLoaded(List<FCAttachment> attachments) {
                                    Iterator var2 = attachments.iterator();

                                    while(var2.hasNext()) {
                                        FCAttachment attachment = (FCAttachment)var2.next();
                                        Attachment.getStorageReference(attachment.getUid()).delete();
                                    }

                                    Messages.getReference(channel.getUid()).child(message.getUid()).removeValue();
                                }

                                public void onError(Exception ex) {
                                }
                            });
                        } else {
                            Messages.getReference(channel.getUid()).child(message.getUid()).removeValue();
                        }
                    }

                }

                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
    }

    public void deleteUsers(FCChannel channel, final List<FCUser> users, final FCSuccessListener listener) {
        final FCUser currentUser = ChatSDK.getInstance().getCurrentSession().getCurrentUser();
        if(currentUser == null) {
            if(listener != null) {
                listener.onError(new NullPointerException("Current user is null"));
            }

        } else if(users != null && users.size() != 0) {
            Iterator var5 = users.iterator();

            FCUser user;
            do {
                if(!var5.hasNext()) {
                    Channels.getReference(channel.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            FCChannel channel = Channels.getChannel(dataSnapshot);
                            if(channel != null) {
                                if(channel.isOneToOne()) {
                                    if(listener != null) {
                                        listener.onError(new IllegalArgumentException("You can't delete users from one-to-one channel"));
                                    }

                                    return;
                                }

                                if(!channel.getOwner().equals(currentUser.getUid())) {
                                    if(listener != null) {
                                        listener.onError(new IllegalArgumentException("You can't delete users because you aren't owner"));
                                    }

                                    return;
                                }

                                UsersChannels.getReferenceQuery(UsersChannels.CHANNEL, channel.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        Iterator var2 = dataSnapshot.getChildren().iterator();

                                        while(true) {
                                            while(true) {
                                                FCUserChannel userChannel;
                                                do {
                                                    if(!var2.hasNext()) {
                                                        if(listener != null) {
                                                            listener.onSuccess();
                                                        }

                                                        return;
                                                    }

                                                    DataSnapshot userSnapshot = (DataSnapshot)var2.next();
                                                    userChannel = UsersChannels.getUserChannel(userSnapshot);
                                                } while(userChannel == null);

                                                for(int i = users.size() - 1; i >= 0; --i) {
                                                    FCUser user = (FCUser)users.get(i);
                                                    if(userChannel.getUser().equals(user.getUid())) {
                                                        users.remove(i);
                                                        UsersChannels.getReference(userChannel.getUid()).removeValue();
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    public void onCancelled(DatabaseError databaseError) {
                                        if(listener != null) {
                                            listener.onError(databaseError.toException());
                                        }

                                    }
                                });
                            } else if(listener != null) {
                                listener.onError(new NullPointerException("This channel isn't exists"));
                            }

                        }

                        public void onCancelled(DatabaseError databaseError) {
                            if(listener != null) {
                                listener.onError(databaseError.toException());
                            }

                        }
                    });
                    return;
                }

                user = (FCUser)var5.next();
            } while(!user.getUid().equals(currentUser.getUid()));

            if(listener != null) {
                listener.onError(new IllegalArgumentException("You can't delete yourself"));
            }

        } else {
            if(listener != null) {
                listener.onError(new NullPointerException("Users list cannot be null or empty"));
            }

        }
    }

    public void leaveChatRoom(FCChannel channel, final FCSuccessListener listener) {
        final FCUser currentUser = ChatSDK.getInstance().getCurrentSession().getCurrentUser();
        if(currentUser == null) {
            if(listener != null) {
                listener.onError(new NullPointerException("Current user is null"));
            }

        } else {
            Channels.getReference(channel.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                public void onDataChange(DataSnapshot dataSnapshot) {
                    FCChannel channel = Channels.getChannel(dataSnapshot);
                    if(channel != null) {
                        if(channel.isOneToOne()) {
                            if(listener != null) {
                                listener.onError(new IllegalArgumentException("You can't leave one-to-one channel"));
                            }

                            return;
                        }

                        UsersChannels.getReferenceQuery(UsersChannels.CHANNEL, channel.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Iterator var2 = dataSnapshot.getChildren().iterator();

                                while(var2.hasNext()) {
                                    DataSnapshot userSnapshot = (DataSnapshot)var2.next();
                                    FCUserChannel userChannel = UsersChannels.getUserChannel(userSnapshot);
                                    if(userChannel != null && userChannel.getUser().equals(currentUser.getUid())) {
                                        UsersChannels.getReference(userChannel.getUid()).removeValue();
                                        break;
                                    }
                                }

                                if(listener != null) {
                                    listener.onSuccess();
                                }

                            }

                            public void onCancelled(DatabaseError databaseError) {
                                if(listener != null) {
                                    listener.onError(databaseError.toException());
                                }

                            }
                        });
                    } else if(listener != null) {
                        listener.onError(new NullPointerException("This channel isn't exists"));
                    }

                }

                public void onCancelled(DatabaseError databaseError) {
                    if(listener != null) {
                        listener.onError(databaseError.toException());
                    }

                }
            });
        }
    }

    public void getChannels(final FCChannelsListener listener) {
        FCUser currentUser = ChatSDK.getInstance().getCurrentSession().getCurrentUser();
        if(currentUser == null) {
            if(listener != null) {
                listener.onError(new NullPointerException("Current user is null"));
            }

        } else {
            UsersChannels.getReferenceQuery(UsersChannels.USER, currentUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                public void onDataChange(DataSnapshot dataSnapshot) {
                    List<FCChannel> channels = new ArrayList();
                    Iterator var3 = dataSnapshot.getChildren().iterator();

                    while(var3.hasNext()) {
                        DataSnapshot channelSnapshot = (DataSnapshot)var3.next();
                        FCUserChannel userChannel = UsersChannels.getUserChannel(channelSnapshot);
                        if(userChannel != null) {
                            channels.add(userChannel.convertToChannel());
                        }
                    }

                    if(listener != null) {
                        listener.onChannelsReceived(channels);
                    }

                }

                public void onCancelled(DatabaseError databaseError) {
                    if(listener != null) {
                        listener.onError(databaseError.toException());
                    }

                }
            });
        }
    }

    public void getUserByPhone(String phone, final FCUserGettingListener listener) {
        FCUser currentUser = ChatSDK.getInstance().getCurrentSession().getCurrentUser();
        if(currentUser == null) {
            if(listener != null) {
                listener.onError(new NullPointerException("Current user isn't authorized"));
            }

        } else {
            Users.getReferenceQuery("phone", phone).addListenerForSingleValueEvent(new ValueEventListener() {
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Iterator var2 = dataSnapshot.getChildren().iterator();
                    if(var2.hasNext()) {
                        DataSnapshot channelSnapshot = (DataSnapshot)var2.next();
                        if(listener != null) {
                            listener.onUserLoaded(Users.getUser(channelSnapshot));
                        }
                    }

                }

                public void onCancelled(DatabaseError databaseError) {
                    if(listener != null) {
                        listener.onError(databaseError.toException());
                    }

                }
            });
        }
    }

    public void getAllChatUsers(FCChannel channel, final FCUsersListener listener) {
        FCUser currentUser = ChatSDK.getInstance().getCurrentSession().getCurrentUser();
        if(currentUser == null) {
            if(listener != null) {
                listener.onError(new NullPointerException("User isn't authorized"));
            }

        } else {
            UsersChannels.getReferenceQuery(UsersChannels.CHANNEL, channel.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                public void onDataChange(DataSnapshot dataSnapshot) {
                    List<FCUser> users = new ArrayList();
                    Iterator var3 = dataSnapshot.getChildren().iterator();

                    while(var3.hasNext()) {
                        DataSnapshot userSnapshot = (DataSnapshot)var3.next();
                        FCUserChannel userChannel = UsersChannels.getUserChannel(userSnapshot);
                        if(userChannel != null) {
                            users.add(new FCUser(userChannel.getUser()));
                        }
                    }

                    if(listener != null) {
                        listener.onUsersReceived(users);
                    }

                }

                public void onCancelled(DatabaseError databaseError) {
                    if(listener != null) {
                        listener.onError(databaseError.toException());
                    }

                }
            });
        }
    }

    public void addUsersToChannel(final FCChannel channel, final List<FCUser> users, final FCSuccessListener listener) {
        final FCUser currentUser = ChatSDK.getInstance().getCurrentSession().getCurrentUser();
        if(currentUser == null) {
            if(listener != null) {
                listener.onError(new NullPointerException("User isn't authorized"));
            }

        } else {
            UsersChannels.getReferenceQuery(UsersChannels.CHANNEL, channel.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                public void onDataChange(DataSnapshot dataSnapshot) {
                    List<FCUserChannel> usersList = new ArrayList();
                    Iterator var3 = dataSnapshot.getChildren().iterator();

                    while(var3.hasNext()) {
                        DataSnapshot userSnapshot = (DataSnapshot)var3.next();
                        FCUserChannel userChannel = UsersChannels.getUserChannel(userSnapshot);
                        if(userChannel != null) {
                            usersList.add(userChannel);
                        }
                    }

                    boolean isInThisChannel = false;

                    for(int i = users.size() - 1; i >= 0; --i) {
                        Iterator var10 = usersList.iterator();

                        while(var10.hasNext()) {
                            FCUserChannel fcUserChannel = (FCUserChannel)var10.next();
                            if(fcUserChannel.getUser().equals(currentUser.getUid())) {
                                isInThisChannel = true;
                                break;
                            }

                            if(fcUserChannel.getUser().equals(((FCUser)users.get(i)).getUid())) {
                                users.remove(i);
                                break;
                            }
                        }
                    }

                    if(users.size() == 0) {
                        if(listener != null) {
                            listener.onSuccess();
                        }
                    } else if(!isInThisChannel) {
                        if(listener != null) {
                            listener.onError(new IllegalAccessException("You are not in this channel"));
                        }
                    } else {
                        Iterator var9 = users.iterator();

                        while(var9.hasNext()) {
                            FCUser user = (FCUser)var9.next();
                            UsersChannels.getReference().child(UUID.randomUUID().toString()).setValue(new FCUserChannel(user, channel));
                        }

                        if(listener != null) {
                            listener.onSuccess();
                        }
                    }

                }

                public void onCancelled(DatabaseError databaseError) {
                    if(listener != null) {
                        listener.onError(databaseError.toException());
                    }

                }
            });
        }
    }
}
