//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fanzine.chat.sdk;

import com.fanzine.chat.models.user.FCUser;
import com.fanzine.chat.sdk.Routes.Users;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.Iterator;

public class FCUserManager {
    public FCUserManager() {
    }

    public void writeNewUser(String userId, FCUser user) {
        FirebaseDatabase.getInstance().getReference().child("users").child(userId).setValue(user);
    }

    public void getUsers() {
        Users.getReference().addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                DataSnapshot userSnapshot;
                FCUser var4;
                for(Iterator var2 = dataSnapshot.getChildren().iterator(); var2.hasNext(); var4 = Users.getUser(userSnapshot)) {
                    userSnapshot = (DataSnapshot)var2.next();
                }

            }

            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
}
