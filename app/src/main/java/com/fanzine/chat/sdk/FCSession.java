/*    */ package com.fanzine.chat.sdk;
/*    */ 
/*    */ import com.fanzine.chat.interfaces.auth.FCAuthorizationListener;
/*    */ import com.fanzine.chat.models.user.FCUser;
/*    */ import com.google.firebase.auth.FirebaseUser;
/*    */ import com.google.firebase.database.DataSnapshot;
/*    */ import com.google.firebase.database.DatabaseError;
/*    */
/*    */ import com.google.firebase.database.ValueEventListener;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class FCSession
/*    */ {
/*    */   private FCUser user;
/*    */   
/*    */   public void init(final FirebaseUser firUser, final FCAuthorizationListener listener)
/*    */   {
/* 20 */     Routes.Users.getReference(firUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener()
/*    */     {
/*    */       public void onDataChange(DataSnapshot dataSnapshot) {
/* 23 */         FCSession.this.user = Routes.Users.getUser(dataSnapshot);
/* 24 */         if (FCSession.this.user == null) {
/* 25 */           FCSession.this.user = new FCUser();
/* 26 */           FCSession.this.user.setEmail(firUser.getEmail());
/* 27 */           FCSession.this.user.setNickname(firUser.getDisplayName());
/*    */           
/* 29 */           Routes.Users.getReference(firUser.getUid()).setValue(FCSession.this.user);
/*    */           
/* 31 */           FCSession.this.user.setUid(firUser.getUid());
/*    */           
/* 33 */           if (listener != null) {
/* 34 */             listener.onAuthorized(FCSession.this.user, true);
/*    */           }
/*    */         }
/* 37 */         else if (listener != null) {
/* 38 */           listener.onAuthorized(FCSession.this.user, false);
/*    */         }
/*    */       }
/*    */       
/*    */       public void onCancelled(DatabaseError databaseError)
/*    */       {
/* 44 */         if (listener != null)
/* 45 */           listener.onError(databaseError.toException());
/*    */       }
/*    */     });
/*    */   }
/*    */   
/*    */   public FCUser getCurrentUser() {
/* 51 */     return this.user;
/*    */   }
/*    */ }


/* Location:              /home/a/Рабочий стол/chat/almetchatsdk-release.aar_FILES/classes.jar!/com/almet/chat/sdk/FCSession.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */