//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fanzine.chat;

import com.fanzine.chat.interfaces.auth.FCAuthorizationListener;
import com.fanzine.chat.models.user.FCUser;
import com.fanzine.chat.sdk.FCBlacklistManager;
import com.fanzine.chat.sdk.FCChatManager;
import com.fanzine.chat.sdk.FCSession;
import com.fanzine.chat.sdk.FCUserManager;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

public class ChatSDK {
    private static final ChatSDK ourInstance = new ChatSDK();
    private FCSession currentSession = new FCSession();
    private FCChatManager chatChanel = new FCChatManager();
    private FCUserManager userManager = new FCUserManager();
    private static FCBlacklistManager blacklistManager;

    public static ChatSDK getInstance() {
        return ourInstance;
    }

    private ChatSDK() {
        blacklistManager = new FCBlacklistManager();
    }

    public void init() {
    }

    public void terminate() {
        blacklistManager.unsubscribe();
    }

    public void offlineModeEnable(boolean isEnabled) {
        FirebaseDatabase.getInstance().setPersistenceEnabled(isEnabled);
    }

    public void authorize(FirebaseUser mFirebaseUser) {
        this.authorize(mFirebaseUser, (FCAuthorizationListener)null);
    }

    public void authorize(FirebaseUser mFirebaseUser, final FCAuthorizationListener listener) {
        if(mFirebaseUser != null) {
            this.currentSession.init(mFirebaseUser, new FCAuthorizationListener() {
                public void onError(Exception ex) {
                    if(listener != null) {
                        listener.onError(ex);
                    }

                }

                public void onAuthorized(FCUser user, boolean isNewUser) {
                    if(listener != null) {
                        listener.onAuthorized(user, isNewUser);
                    }

                    ChatSDK.blacklistManager.subscribe(user);
                }
            });
        } else {
            throw new NullPointerException("FirebaseUser is null");
        }
    }

    public FCSession getCurrentSession() {
        return this.currentSession;
    }

    public FCChatManager getChatManager() {
        return this.chatChanel;
    }

    public FCUserManager getUserManager() {
        return this.userManager;
    }

    public FCBlacklistManager getBlacklistManager() {
        return blacklistManager;
    }
}
