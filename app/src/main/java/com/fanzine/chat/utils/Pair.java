/*    */ package com.fanzine.chat.utils;
/*    */ 
/*    */ 
/*    */ public class Pair<Q, L>
/*    */ {
/*    */   private Q query;
/*    */   
/*    */   private L listener;
/*    */   
/*    */ 
/*    */   public Pair(Q query, L listener)
/*    */   {
/* 13 */     this.listener = listener;
/* 14 */     this.query = query;
/*    */   }
/*    */   
/*    */   public L getListener() {
/* 18 */     return (L)this.listener;
/*    */   }
/*    */   
/*    */   public Q getQuery() {
/* 22 */     return (Q)this.query;
/*    */   }
/*    */ }


/* Location:              /home/a/Рабочий стол/chat/almetchatsdk-release.aar_FILES/classes.jar!/com/almet/chat/utils/Pair.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */