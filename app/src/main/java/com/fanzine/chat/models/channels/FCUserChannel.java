//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fanzine.chat.models.channels;

import com.fanzine.chat.models.user.FCUser;

public class FCUserChannel {
    private String uid;
    private String channel;
    private String type;
    private long timestamp;
    private String user;

    public FCUserChannel() {
    }

    public FCUserChannel(FCUser user, FCChannel channel) {
        this.user = user.getUid();
        this.timestamp = channel.getTimestamp();
        this.channel = channel.getUid();
        this.type = channel.getType();
    }

    public String getUid() {
        return this.uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getChannel() {
        return this.channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public FCChannel convertToChannel() {
        return (new FCChannel()).initFromUserChannel(this);
    }
}
