//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fanzine.chat.models.channels;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.fanzine.chat.interfaces.FCChannelMessageListener;
import com.fanzine.chat.interfaces.sync.FCChannelSyncListener;
import com.fanzine.chat.models.message.FCMessage;
import com.fanzine.chat.models.user.FCUser;
import com.fanzine.chat.sdk.Routes.Channels;
import com.fanzine.chat.sdk.Routes.Messages;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@IgnoreExtraProperties
public final class FCChannel implements Parcelable {
    public static final String ONE_TO_MANY = "one-to-many";
    public static final String ONE_TO_ONE = "one-to-one";
    private String owner;

    private String title;
    private String name;

    private String channel;

    private long timestamp;
    private String type;
    private String uid;
    private Map<String, String> info = new HashMap();
    private ChildEventListener childEventListener;
    private Query query;
    public static final Creator<FCChannel> CREATOR = new Creator<FCChannel>() {
        public FCChannel createFromParcel(Parcel in) {
            return new FCChannel(in);
        }

        public FCChannel[] newArray(int size) {
            return new FCChannel[size];
        }
    };

    protected FCChannel(Parcel in) {
        this.owner = in.readString();
        this.timestamp = in.readLong();
        this.type = in.readString();
        this.uid = in.readString();
        this.info = new HashMap();
        List<String> keys = in.createStringArrayList();
        List<String> values = in.createStringArrayList();

        for(int i = 0; i < keys.size(); ++i) {
            this.info.put(keys.get(i), values.get(i));
        }

    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.owner);
        dest.writeLong(this.timestamp);
        dest.writeString(this.type);
        dest.writeString(this.uid);
        List<String> keys = new ArrayList();
        List<String> values = new ArrayList();
        Iterator var5 = this.info.entrySet().iterator();

        while(var5.hasNext()) {
            Entry<String, String> entry = (Entry)var5.next();
            keys.add(entry.getKey());
            values.add(entry.getValue());
        }

        dest.writeStringList(keys);
        dest.writeStringList(values);
    }

    public int describeContents() {
        return 0;
    }

    public static FCChannel createChannel(String ownerId, String type) {
        FCChannel channel = new FCChannel();
        channel.setTimestamp((new Date()).getTime());
        channel.setOwner(ownerId);
        channel.setType(type);
        return channel;
    }

    public FCChannel() {
    }

    public String getOwner() {
        return this.owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getType() {
        return this.type;
    }

    public Map<String, String> getInfo() {
        return this.info;
    }

    public void setInfo(Map<String, String> info) {
        this.info = info;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Exclude
    public String getUid() {
        return this.uid;
    }

    public void setUid(String id) {
        this.uid = id;
    }

    public void subscribe(final FCChannelMessageListener listener) {
        this.childEventListener = new ChildEventListener() {
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                FCMessage message = Messages.getMessage(FCChannel.this, dataSnapshot);
                if(listener != null) {
                    listener.onNewMessageReceived(message);
                }

            }

            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            public void onChildRemoved(DataSnapshot dataSnapshot) {
                FCMessage message = Messages.getMessage(FCChannel.this, dataSnapshot);
                if(listener != null) {
                    listener.onMessageDeleted(message);
                }

            }

            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            public void onCancelled(DatabaseError databaseError) {
                if(listener != null) {
                    listener.onError(databaseError.toException());
                }

            }
        };
        this.query = Messages.getReferenceSortedQuery(this.uid);
        this.query.addChildEventListener(this.childEventListener);
    }

    public void unsubscribe() {
        if(this.query != null && this.childEventListener != null) {
            this.query.removeEventListener(this.childEventListener);
            this.childEventListener = null;
        }

    }

    @Exclude
    public boolean isOneToOne() {
        return "one-to-one".equals(this.type);
    }

    public void sync(final FCChannelSyncListener listener) {
        if(!TextUtils.isEmpty(this.uid)) {
            Channels.getReference(this.uid).addListenerForSingleValueEvent(new ValueEventListener() {
                public void onDataChange(DataSnapshot dataSnapshot) {
                    FCChannel channel = Channels.getChannel(dataSnapshot);
                    if(listener != null) {
                        listener.onSync(FCChannel.this.init(channel));
                    } else {
                        FCChannel.this.init(channel);
                    }

                }

                public void onCancelled(DatabaseError databaseError) {
                    if(listener != null) {
                        listener.onError(databaseError.toException());
                    }

                }
            });
        } else if(listener != null) {
            listener.onError(new NullPointerException("Uid is empty"));
        }

    }

    FCChannel init(FCChannel channel) {
        this.uid = channel.getUid();
        this.type = channel.getType();
        this.timestamp = channel.getTimestamp();
        this.owner = channel.getOwner();
        this.info = channel.getInfo();
        return this;
    }

    FCChannel initFromUserChannel(FCUserChannel userChannel) {
        this.uid = userChannel.getChannel();
        this.type = userChannel.getType();
        this.timestamp = userChannel.getTimestamp();
        return this;
    }

    @Exclude
    public FCUser getUser() {
        FCUser user = new FCUser();
        user.setUid(this.owner);
        return user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
