//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fanzine.chat.models.blacklist;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.fanzine.chat.models.channels.FCChannel;
import com.fanzine.chat.models.user.FCUser;
import com.google.firebase.database.Exclude;

public class FCBlacklist implements Parcelable {
    private String uid;
    private String channel;
    private String user;
    public static final Creator<FCBlacklist> CREATOR = new Creator<FCBlacklist>() {
        public FCBlacklist createFromParcel(Parcel in) {
            return new FCBlacklist(in);
        }

        public FCBlacklist[] newArray(int size) {
            return new FCBlacklist[size];
        }
    };

    public FCBlacklist() {
    }

    public FCBlacklist(FCChannel channel, FCUser user) {
        this.channel = channel.getUid();
        this.user = user.getUid();
    }

    protected FCBlacklist(Parcel in) {
        this.uid = in.readString();
        this.channel = in.readString();
        this.user = in.readString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uid);
        dest.writeString(this.channel);
        dest.writeString(this.user);
    }

    public int describeContents() {
        return 0;
    }

    @Exclude
    public String getUid() {
        return this.uid;
    }

    public void setUid(String id) {
        if(TextUtils.isEmpty(this.uid)) {
            this.uid = id;
        }

    }

    public String getChannel() {
        return this.channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
