//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fanzine.chat.models.message;

import android.os.Parcel;
import android.os.Parcelable;

import com.fanzine.chat.ChatSDK;
import com.fanzine.chat.interfaces.getting.FCAttachmentsListener;
import com.fanzine.chat.sdk.Routes.Attachment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@IgnoreExtraProperties
public final class FCMessage implements Parcelable {
    public static final String TYPE_DEFAULT = "message_type_default";
    public static final String TYPE_PICTURE = "message_type_picture";
    public static final String TYPE_VIDEO = "message_type_video";
    public static final String TYPE_FILE = "message_type_file";
    private String uid;
    private String channelId;
    private String user;
    private String type;
    private long timestamp;
    private String message;
    private Map<String, String> info = new HashMap();
    private List<FCAttachment> attachments;
    public static final Creator<FCMessage> CREATOR = new Creator<FCMessage>() {
        public FCMessage createFromParcel(Parcel in) {
            return new FCMessage(in);
        }

        public FCMessage[] newArray(int size) {
            return new FCMessage[size];
        }
    };

    public FCMessage() {
    }

    public FCMessage(String type, long timestamp, String message, Map<String, String> info) {
        this.user = ChatSDK.getInstance().getCurrentSession().getCurrentUser() != null?ChatSDK.getInstance().getCurrentSession().getCurrentUser().getUid():"";
        this.type = type;
        this.timestamp = timestamp;
        this.message = message;
        this.info = info;
    }

    public FCMessage(String type, long timestamp, String message) {
        this.user = ChatSDK.getInstance().getCurrentSession().getCurrentUser() != null?ChatSDK.getInstance().getCurrentSession().getCurrentUser().getUid():"";
        this.type = type;
        this.timestamp = timestamp;
        this.message = message;
    }

    protected FCMessage(Parcel in) {
        this.uid = in.readString();
        this.user = in.readString();
        this.type = in.readString();
        this.timestamp = in.readLong();
        this.message = in.readString();
        this.info = new HashMap();
        List<String> keys = in.createStringArrayList();
        List<String> values = in.createStringArrayList();

        for(int i = 0; i < keys.size(); ++i) {
            this.info.put(keys.get(i), values.get(i));
        }

        this.attachments = in.createTypedArrayList(FCAttachment.CREATOR);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uid);
        dest.writeString(this.user);
        dest.writeString(this.type);
        dest.writeLong(this.timestamp);
        dest.writeString(this.message);
        List<String> keys = new ArrayList();
        List<String> values = new ArrayList();
        Iterator var5 = this.info.entrySet().iterator();

        while(var5.hasNext()) {
            Entry<String, String> entry = (Entry)var5.next();
            keys.add(entry.getKey());
            values.add(entry.getValue());
        }

        dest.writeStringList(keys);
        dest.writeStringList(values);
        dest.writeTypedList(this.attachments);
    }

    public int describeContents() {
        return 0;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, String> getInfo() {
        return this.info;
    }

    public void setInfo(Map<String, String> info) {
        this.info = info;
    }

    @Exclude
    public String getUid() {
        return this.uid;
    }

    public void setUid(String id) {
        this.uid = id;
    }

    public void syncAttachmentsList() {
        this.syncAttachmentsList((FCAttachmentsListener)null);
    }

    public void syncAttachmentsList(final FCAttachmentsListener listener) {
        Attachment.getDatabaseReference(this.channelId, this.uid).addListenerForSingleValueEvent(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                FCMessage.this.attachments = new ArrayList();
                Iterator var2 = dataSnapshot.getChildren().iterator();

                while(var2.hasNext()) {
                    DataSnapshot child = (DataSnapshot)var2.next();
                    FCAttachment attachment = Attachment.getAttachment(FCMessage.this, child);
                    if(attachment != null) {
                        FCMessage.this.attachments.add(attachment);
                    }
                }

                if(listener != null) {
                    listener.onAttachmentsLoaded(FCMessage.this.attachments);
                }

            }

            public void onCancelled(DatabaseError databaseError) {
                if(listener != null) {
                    listener.onError(databaseError.toException());
                }

            }
        });
    }

    @Exclude
    public List<FCAttachment> getAttachments() {
        return this.attachments;
    }

    public void setAttachments(List<FCAttachment> attachments) {
        this.attachments = attachments;
    }

    @Exclude
    public String getChannelUid() {
        return this.channelId;
    }

    public void setChannelUid(String channelId) {
        this.channelId = channelId;
    }
}
