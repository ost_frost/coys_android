//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fanzine.chat.models.user;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.fanzine.chat.ChatSDK;
import com.fanzine.chat.interfaces.sync.FCUserSyncListener;
import com.fanzine.chat.sdk.Routes.Users;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@IgnoreExtraProperties
public class FCUser implements Parcelable {
    private String nickname;
    private String phone;
    private String email;
    private String firstName;
    private String lastName;
    private String avatar;
    private Map<String, String> info = new HashMap();
    private String uid;
    public static final Creator<FCUser> CREATOR = new Creator<FCUser>() {
        public FCUser createFromParcel(Parcel in) {
            return new FCUser(in);
        }

        public FCUser[] newArray(int size) {
            return new FCUser[size];
        }
    };

    public FCUser() {
    }

    public FCUser(String nickname, String phone) {
        this.nickname = nickname;
        this.phone = phone;
    }

    public FCUser(String uid) {
        this.uid = uid;
    }

    protected FCUser(Parcel in) {
        this.nickname = in.readString();
        this.phone = in.readString();
        this.email = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.uid = in.readString();
        this.avatar = in.readString();
        this.info = new HashMap();
        List<String> keys = in.createStringArrayList();
        List<String> values = in.createStringArrayList();

        for (int i = 0; i < keys.size(); ++i) {
            this.info.put(keys.get(i), values.get(i));
        }

    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nickname);
        dest.writeString(this.phone);
        dest.writeString(this.email);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.uid);
        dest.writeString(this.avatar);
        List<String> keys = new ArrayList();
        List<String> values = new ArrayList();
        Iterator var5 = this.info.entrySet().iterator();

        while (var5.hasNext()) {
            Entry<String, String> entry = (Entry) var5.next();
            keys.add(entry.getKey());
            values.add(entry.getValue());
        }

        dest.writeStringList(keys);
        dest.writeStringList(values);
    }

    public int describeContents() {
        return 0;
    }

    public String getNickname() {
        return this.nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Exclude
    public String getUid() {
        return this.uid;
    }

    public void setUid(String id) {
        this.uid = id;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Map<String, String> getInfo() {
        return this.info;
    }

    public void setInfo(Map<String, String> info) {
        this.info = info;
    }

    public void sync(final FCUserSyncListener listener) {
        if (!TextUtils.isEmpty(this.uid)) {
            Users.getReference(this.uid).addListenerForSingleValueEvent(new ValueEventListener() {
                public void onDataChange(DataSnapshot dataSnapshot) {
                    FCUser user = Users.getUser(dataSnapshot);

                    //TODO
                    if (user != null) {
                        if (listener != null) {
                            listener.onSync(FCUser.this.init(user));
                        } else {
                            FCUser.this.init(user);
                        }
                    }

                }

                public void onCancelled(DatabaseError databaseError) {
                    if (listener != null) {
                        listener.onError(databaseError.toException());
                    }

                }
            });
        } else if (listener != null) {
            listener.onError(new NullPointerException("Uid is empty"));
        }

    }

    FCUser init(FCUser channel) {
        this.uid = channel.getUid();
        this.firstName = channel.getFirstName();
        this.lastName = channel.getLastName();
        this.email = channel.getEmail();
        this.phone = channel.getPhone();
        this.nickname = channel.getNickname();
        this.info = channel.getInfo();
        this.avatar = channel.getAvatar();
        return this;
    }

    public void synchronize() {
        FCUser currentUser = ChatSDK.getInstance().getCurrentSession().getCurrentUser();
        if (currentUser == null) {
            throw new NullPointerException("Current user is null");
        } else if (!currentUser.getUid().equals(this.uid)) {
            throw new IllegalArgumentException("You can synchronize only yourself");
        } else {
            Users.getReference(this.getUid()).setValue(this);
        }
    }

    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
