//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fanzine.chat.models.message;

import android.os.Parcel;
import android.os.Parcelable;

import com.fanzine.chat.sdk.Routes.Attachment;
import com.google.firebase.database.Exclude;
import com.google.firebase.storage.StorageReference;
import java.io.File;

public class FCAttachment implements Parcelable {
    private String uid;
    private String type;
    private String messageUid;
    private String channelUid;
    private File file;
    public static final Creator<FCAttachment> CREATOR = new Creator<FCAttachment>() {
        public FCAttachment createFromParcel(Parcel in) {
            return new FCAttachment(in);
        }

        public FCAttachment[] newArray(int size) {
            return new FCAttachment[size];
        }
    };

    public FCAttachment() {
    }

    public FCAttachment(String type, File file) {
        this.type = type;
        this.file = file;
    }

    protected FCAttachment(Parcel in) {
        this.uid = in.readString();
        this.type = in.readString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uid);
        dest.writeString(this.type);
    }

    public int describeContents() {
        return 0;
    }

    @Exclude
    public String getUid() {
        return this.uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Exclude
    public StorageReference getReference() {
        return Attachment.getStorageReference(this.uid);
    }

    @Exclude
    public String getMessageUid() {
        return this.messageUid;
    }

    public void setMessageUid(String messageUid) {
        this.messageUid = messageUid;
    }

    @Exclude
    public String getChannelUid() {
        return this.channelUid;
    }

    public void setChannelUid(String ChannelUid) {
        this.channelUid = ChannelUid;
    }

    @Exclude
    public File getFile() {
        return this.file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
