package com.fanzine.chat.interfaces;

import com.fanzine.chat.models.channels.FCChannel;

public abstract interface FCChatChannelCreateListener
{
  public abstract void onChannelCreated(FCChannel paramFCChannel);
  
  public abstract void onChannelExists(FCChannel paramFCChannel);
  
  public abstract void onError(Exception paramException);
}


/* Location:              /home/a/Рабочий стол/chat/almetchatsdk-release.aar_FILES/classes.jar!/com/almet/chat/interfaces/FCChatChannelCreateListener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */