//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fanzine.chat.interfaces.sync;

import com.fanzine.chat.models.channels.FCChannel;

public interface FCChannelSyncListener {
    void onError(Exception var1);

    void onSync(FCChannel var1);
}
