package com.fanzine.chat.interfaces;

public abstract interface FCSuccessListener
{
  public abstract void onSuccess();
  
  public abstract void onError(Exception paramException);
}


/* Location:              /home/a/Рабочий стол/chat/almetchatsdk-release.aar_FILES/classes.jar!/com/almet/chat/interfaces/FCSuccessListener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */