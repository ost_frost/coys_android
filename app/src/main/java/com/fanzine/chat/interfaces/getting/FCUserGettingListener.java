package com.fanzine.chat.interfaces.getting;

import com.fanzine.chat.models.user.FCUser;

public abstract interface FCUserGettingListener
{
  public abstract void onError(Exception paramException);
  
  public abstract void onUserLoaded(FCUser paramFCUser);
}


/* Location:              /home/a/Рабочий стол/chat/almetchatsdk-release.aar_FILES/classes.jar!/com/almet/chat/interfaces/getting/FCUserGettingListener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */