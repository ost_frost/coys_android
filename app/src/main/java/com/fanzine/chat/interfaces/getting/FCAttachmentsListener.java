package com.fanzine.chat.interfaces.getting;

import com.fanzine.chat.models.message.FCAttachment;
import java.util.List;

public abstract interface FCAttachmentsListener
{
  public abstract void onAttachmentsLoaded(List<FCAttachment> paramList);
  
  public abstract void onError(Exception paramException);
}


/* Location:              /home/a/Рабочий стол/chat/almetchatsdk-release.aar_FILES/classes.jar!/com/almet/chat/interfaces/getting/FCAttachmentsListener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */