package com.fanzine.chat.interfaces;

import com.fanzine.chat.models.user.FCUser;
import java.util.List;

public abstract interface FCUsersListener
{
  public abstract void onUsersReceived(List<FCUser> paramList);
  
  public abstract void onError(Exception paramException);
}


/* Location:              /home/a/Рабочий стол/chat/almetchatsdk-release.aar_FILES/classes.jar!/com/almet/chat/interfaces/FCUsersListener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */