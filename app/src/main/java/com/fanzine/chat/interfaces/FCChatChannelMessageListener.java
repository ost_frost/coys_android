package com.fanzine.chat.interfaces;

import com.fanzine.chat.models.message.FCMessage;
import java.util.List;

public abstract interface FCChatChannelMessageListener
{
  public abstract void onMessagesReceived(List<FCMessage> paramList);
  
  public abstract void onError(Exception paramException);
}


/* Location:              /home/a/Рабочий стол/chat/almetchatsdk-release.aar_FILES/classes.jar!/com/almet/chat/interfaces/FCChatChannelMessageListener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */