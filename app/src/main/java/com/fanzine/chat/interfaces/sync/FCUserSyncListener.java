package com.fanzine.chat.interfaces.sync;

import com.fanzine.chat.models.user.FCUser;

public abstract interface FCUserSyncListener
{
  public abstract void onError(Exception paramException);
  
  public abstract void onSync(FCUser paramFCUser);
}


/* Location:              /home/a/Рабочий стол/chat/almetchatsdk-release.aar_FILES/classes.jar!/com/almet/chat/interfaces/sync/FCUserSyncListener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */