package com.fanzine.chat.interfaces.auth;

import com.fanzine.chat.models.user.FCUser;

public abstract interface FCAuthorizationListener
{
  public abstract void onError(Exception paramException);
  
  public abstract void onAuthorized(FCUser paramFCUser, boolean paramBoolean);
}


/* Location:              /home/a/Рабочий стол/chat/almetchatsdk-release.aar_FILES/classes.jar!/com/almet/chat/interfaces/auth/FCAuthorizationListener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */