package com.fanzine.chat.interfaces;

import com.fanzine.chat.models.message.FCMessage;

public abstract interface FCChannelMessageListener
{
  public abstract void onNewMessageReceived(FCMessage paramFCMessage);
  
  public abstract void onMessageDeleted(FCMessage paramFCMessage);
  
  public abstract void onError(Exception paramException);
}


/* Location:              /home/a/Рабочий стол/chat/almetchatsdk-release.aar_FILES/classes.jar!/com/almet/chat/interfaces/FCChannelMessageListener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */