package com.fanzine.mail;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.mails.AttachmentsAdapter;
import com.fanzine.coys.databinding.ActivityMailComposeNewBinding;
import com.fanzine.coys.models.Person;
import com.fanzine.coys.models.mails.NewMail;
import com.fanzine.coys.observables.CreateFileFromUriObservable;
import com.fanzine.coys.subscribers.CreateFileFromUriSubscriber;
import com.fanzine.coys.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx_activity_result.RxActivityResult;

/**
 * Created by Evgenij Krasilnikov on 14-Feb-18
 */

public class MailComposeNewActivity extends AppCompatActivity implements MailComposeNewI {


    private ActivityMailComposeNewBinding binding;

    private MailComposeNewViewPresenterI presenter;

    private String mode = MODE_NEW_MAIL;


    private MenuItem itemSend;
    private AttachmentsAdapter attachmentsAdapter;
    private EditText etSubject, etMailContent, etTo;

    private Spanned mFormattedMailContent;

    public static void start(Context context) {
        context.startActivity(new Intent(context, MailComposeNewActivity.class));
    }

    public static void launch(@NonNull Context context, String newsUrl) {
        Intent intent = new Intent(context, MailComposeNewActivity.class);
        intent.putExtra(NEWS_URL, newsUrl);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMailComposeNewBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());
        setTitle("");
        setToolbar();

        etSubject = binding.acMacoSubject;
        etMailContent = binding.acMacoBody;
        etTo = binding.acMacoTo;

        presenter = new MailComposeNewViewPresenter();
        presenter.bindView(this);

        String mailContent = "";
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(MODE)) {

            mode = intent.getStringExtra(MODE);
            if (mode.equals(MODE_REPLY)) {
                String subject = intent.getStringExtra(MAIL_SUBJECT);

                mailContent = intent.getStringExtra(MAIL_CONTENT);

                String[] recipientArr = intent.getStringArrayExtra(MAIL_RECIPIET_ARRAY);

                String mailSender = intent.getStringExtra(MAIL_SENDER);

                long mailUpdateTime = intent.getLongExtra(MAIL_UPDATE_TIME, 0);

                presenter.setModeReply(mailSender, recipientArr, subject, mailContent, mailUpdateTime);


            } else if (mode.equals(MODE_FORWARD)) {

                String subject = intent.getStringExtra(MAIL_SUBJECT);

                mailContent = intent.getStringExtra(MAIL_CONTENT);
                long mailUpdateTime = intent.getLongExtra(MAIL_UPDATE_TIME, 0);
                String[] recipientArr = intent.getStringArrayExtra(MAIL_RECIPIET_ARRAY);

                String mailSender = intent.getStringExtra(MAIL_SENDER);
                presenter.setModeForward(mailSender, recipientArr, subject, mailContent, mailUpdateTime);

            }
            presenter.setEmailSignture();
        } else {
            setMailContent(getNewsUrl(), "text/html");
        }

        attachmentsAdapter = new AttachmentsAdapter(this);
        binding.attachments.setLayoutManager(new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        binding.attachments.setAdapter(attachmentsAdapter);

        etTo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                itemSend.setEnabled(s.length() > 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.ivExpand.setOnClickListener(view -> binding.acMacoCopyContent.setVisibility(View.VISIBLE));

       /* binding.acMacoTo.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                Drawable drawable = binding.acMacoTo.getCompoundDrawables()[2];
                if (drawable != null && (event.getX() + v.getPaddingLeft()) >= (v.getRight() - drawable.getBounds().width())) {
                    binding.acMacoTo.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    binding.acMacoCopyContent.setVisibility(View.VISIBLE);
                    return true;
                }
            }
            v.performClick();
            return false;
        });*/

    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onStop();
    }

    @Override
    public void setMailContent(String mailContent, String mimeType) {
        mFormattedMailContent = Html.fromHtml(mailContent);
    }

    @Override
    public void attachSignuture(String text) {
        if (mFormattedMailContent != null)
            etMailContent.setText(TextUtils.concat(text, mFormattedMailContent), TextView.BufferType.SPANNABLE);
        else
            etMailContent.setText(text);
    }

    @Override
    public void setCursorToMailContentEdit() {
        etMailContent.setFocusable(true);
        etMailContent.requestFocus();
        etMailContent.setSelection(0);
    }


    @Override
    public void setSubject(String subject) {
        etSubject.setText(subject);
    }

    @Override
    public void showError(String error) {
        DialogUtils.showAlertDialog(this, error);
    }

    @Override
    public void finishView() {
        finish();
    }

    ProgressDialog pd = null;

    @Override
    public void setProgressBarVisability(boolean b) {
        if (b) {
            pd = DialogUtils.showProgressDialog(this, R.string.please_wait);
        } else {
            if (pd != null) {
                pd.dismiss();
            }
        }
    }

    @Override
    public void setTo(String recipientStr) {
        etTo.setText(recipientStr);
    }

    private void setToolbar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            final Drawable backIcon = ContextCompat.getDrawable(this, R.drawable.ic_back_grey);
            actionBar.setHomeAsUpIndicator(backIcon);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_mail_compose_new, menu);
        itemSend = menu.findItem(R.id.action_send);

        if (mode.equals(MODE_NEW_MAIL)) {
            itemSend.setEnabled(false);
        } else if (mode.equals(MODE_REPLY)) {
            itemSend.setEnabled(true);
        } else if (mode.equals(MODE_FORWARD)) {
            itemSend.setEnabled(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_send:

                NewMail newMail = getMail();
                presenter.onSendEmailClick();

                return true;
            case R.id.action_attach:
                addAttachment();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public void addAttachment() {
        Intent takeFile;
        if (Build.VERSION.SDK_INT < 19) {
            takeFile = new Intent(Intent.ACTION_GET_CONTENT);

        } else {
            takeFile = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            takeFile.addCategory(Intent.CATEGORY_OPENABLE);
        }
        takeFile.setType("*/*");

        RxActivityResult.on(this).startIntent(takeFile).subscribe(result -> {
            Intent data = result.data();

            if (result.resultCode() == RESULT_OK) {
                loadAttachments(data.getData());
            }
        });
    }

    private void loadAttachments(Uri data) {
        ProgressDialog pd = ProgressDialog.show(
                this, "", "Please wait", true, false);
        pd.setProgress(ProgressDialog.STYLE_SPINNER);

        Observable.defer(new CreateFileFromUriObservable(this, data))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CreateFileFromUriSubscriber(attachmentsAdapter, pd));
    }

    @Override
    public NewMail getMail() {

        //int size = binding.acMacoTo.getObjects().size();
        List<Person> personList;

        // Log.i("TAG", "binding.acMacoTo.getObjects().size()="+size);
        //size=0;
        //if (size == 0) {
        Person person = new Person(etTo.getText().toString(), etTo.getText().toString());
        personList = new ArrayList<>();
        personList.add(person);
        //} else {
        // personList = binding.acMacoCc.getObjects();
        //}

        // Log.i("TAG", "person email="+personList.get(1).getEmail()+" name="+personList.get(1).getName());

        String msgtext = Html.toHtml(binding.acMacoBody.getText());

        NewMail newMail = new NewMail(personList,
                binding.acMacoCc.getObjects(),
                binding.acMacoBcc.getObjects(),
                binding.acMacoSubject.getText().toString(),
                msgtext,
                attachmentsAdapter.getItems());

        return newMail;
    }

    @Override
    public void showUserMessage(String text) {
        Toast toast = Toast.makeText(getApplicationContext(),
                text, Toast.LENGTH_SHORT);
        toast.show();
    }

    @NonNull
    private String getNewsUrl() {
        Intent intent = getIntent();
        if (intent.hasExtra(NEWS_URL)) {
            return intent.getStringExtra(NEWS_URL);
        }
        return "";
    }

}
