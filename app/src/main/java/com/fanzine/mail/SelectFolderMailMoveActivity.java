package com.fanzine.mail;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.fanzine.coys.R;
import com.fanzine.coys.models.mails.Folder;
import com.fanzine.coys.networking.EmailDataRequestManager;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.RecyclerItemClickListener;
import com.fanzine.coys.widgets.RecyclerViewLineItemDecoration;
import com.fanzine.chat3.view.adapter.base.BaseArrayListAdapter;
import com.google.common.collect.Ordering;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SelectFolderMailMoveActivity extends AppCompatActivity implements SelectFolderMailMoveI {
    public static final String MAIL_UID = "mail_uid";
    public static final String MAIL_SOURCE_FOLDER = "mail_source_folder";
    private RecyclerView rvFolder;
    private int mailUid;
    private String sourceFolder;

    private interface StaticFolders {
        String SENT = "Sent";
        String DRAFTS = "Drafts";
        String SPAM = "Spam";
        String INBOX = "Inbox";
        String FLAGGED = "Flagged";
        String TRASH = "Trash";
        String STARRED = "StarredPresenter";

    }

    private Map<String, Integer> folderIconMap = new HashMap<>();
    private final static Ordering<Folder> ORDERING = Ordering.natural().onResultOf(Folder::getOrder);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_folder_mail_move);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {

            final Drawable backIcon = ContextCompat.getDrawable(this, R.drawable.ic_mail_arrow_back);
            actionBar.setHomeAsUpIndicator(backIcon);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        folderIconMap.put(StaticFolders.SENT, R.drawable.mail_outbox);
        folderIconMap.put(StaticFolders.DRAFTS, R.drawable.mail_drafts);
        folderIconMap.put(StaticFolders.SPAM, R.drawable.mail_spam);
        folderIconMap.put(StaticFolders.FLAGGED, R.drawable.mail_star);
        folderIconMap.put(StaticFolders.INBOX, R.drawable.mail_inbox);
        folderIconMap.put(StaticFolders.TRASH, R.drawable.ic_mail_trash_red);


        setProgressVisability(true);
        EmailDataRequestManager.getInstanse()
                .getMailboxFolders()
                .subscribe(mailboxFolders -> {
                    setProgressVisability(false);
                    onLoaded(mailboxFolders.getFolders());
                }, throwable -> {
                    setProgressVisability(false);
                    throwable.printStackTrace();
                });

        Intent intent = getIntent();
        if (intent != null) {
            mailUid = intent.getIntExtra(MAIL_UID, 0);
            sourceFolder = intent.getStringExtra(MAIL_SOURCE_FOLDER);
        }
    }

    public void onLoaded(List<Folder> data) {

        Collections.sort(data, ORDERING);

        for (Folder folder : data) {

            if (folderIconMap.containsKey(folder.getName()))
                folder.setDrawableIconId(folderIconMap.get(folder.getName()));
            else
                folder.setDrawableIconId(R.drawable.mail_work);

            if (folder.getName().equals(StaticFolders.FLAGGED)) {
                folder.setDisplayName(StaticFolders.STARRED);
            } else {
                folder.setDisplayName(folder.getName());
            }
        }
        setFolderList(data);
    }

    public void setFolderList(List<Folder> list) {
        rvFolder = (RecyclerView) findViewById(R.id.rvFolder);
        rvFolder.addItemDecoration(new RecyclerViewLineItemDecoration(this, 5));

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        rvFolder.setLayoutManager(layoutManager);

        BaseArrayListAdapter<Folder, MailFolderHolder> adapter =
                new BaseArrayListAdapter<Folder, MailFolderHolder>(Folder.class, R.layout.item_mail_folder_select,
                        MailFolderHolder.class, list) {
                    @Override
                    protected void populateViewHolder(final MailFolderHolder viewHolder,
                                                      final Folder model, final int position) {
                        viewHolder.bind(model);
                    }
                };

        rvFolder.setAdapter(adapter);

        rvFolder.addOnItemTouchListener(
                new RecyclerItemClickListener(this, (view, position) -> {

                    setProgressVisability(true);
                    String destFolder = list.get(position).getDisplayName();

                    if (destFolder.equals("StarredPresenter")) {
                        EmailDataRequestManager.getInstanse().setMailStarred(sourceFolder, String.valueOf(mailUid), true)
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        isFlagged -> {
                                            setProgressVisability(false);

                                            Intent intent = new Intent();
                                            intent.putExtra("folder_name", destFolder);
                                            setResult(RESULT_OK, intent);

                                            finish();
                                        },
                                        throwable -> {

                                            throwable.printStackTrace();
                                            setProgressVisability(false);

                                        });

                    } else {
                        EmailDataRequestManager.getInstanse().move(sourceFolder, destFolder, mailUid)
                                .subscribe(
                                        responseBody -> {
                                            setProgressVisability(false);

                                            Intent intent = new Intent();
                                            intent.putExtra("folder_name", destFolder);
                                            setResult(RESULT_OK, intent);

                                            finish();
                                        },
                                        throwable -> {
                                            throwable.printStackTrace();
                                            setProgressVisability(false);
                                        });
                    }

                })
        );
    }

    ProgressDialog pd = null;

    @Override
    public void setProgressVisability(boolean b) {
        if (b) {
            pd = DialogUtils.showProgressDialog(this, R.string.please_wait);
        } else {
            if (pd != null) {
                pd.dismiss();
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
