package com.fanzine.mail;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.coys.models.mails.Folder;


public class MailFolderHolder extends RecyclerView.ViewHolder {
    public static final String LOG_TAG = "GroupInfoUser";
    private Context context;

    private TextView it_mafo_folder_name;
    private ImageView it_mafo_icon;

    private Folder folder;

    public MailFolderHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();

        it_mafo_folder_name = (TextView) itemView.findViewById(R.id.it_mafo_folder_name);

        it_mafo_icon = (ImageView) itemView.findViewById(R.id.it_mafo_icon);
    }


    public void bind(Folder folder) {

        this.folder = folder;

        it_mafo_icon.setImageDrawable(ContextCompat.getDrawable(context, folder.getDrawableIconId()));

        it_mafo_folder_name.setText(folder.getDisplayName());
    }
}