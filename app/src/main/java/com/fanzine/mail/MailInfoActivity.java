package com.fanzine.mail;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.ActivityMailInfoBinding;
import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.models.mails.MailAttachment;
import com.fanzine.coys.utils.RecyclerItemClickListener;
import com.fanzine.chat3.view.adapter.base.BaseArrayListAdapter;
import com.fanzine.chat3.view.custom.AcceptRejectDialog;
import com.fanzine.mail.adapter.MailIncomingAtachmentHolder;
import com.fanzine.mail.view.activity.ImageActivity;
import com.fanzine.mail.view.activity.folder.ListEmailActivity;

import java.util.List;

import rx.Observable;

public class MailInfoActivity extends AppCompatActivity implements MailDetailsViewI {
    public static final String MAIL_BODY = "mail_body";
    private static final int REQUEST_CODE_MOVE_MAIL = 11;
    private static final String FOLDER_NAME = "folder_name";

    private Mail mail;

    private ActivityMailInfoBinding binding;

    private MailInfoPresenterI presenter;
    private String mailContent;
    private String fromAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_mail_info);
        setSupportActionBar(binding.toolbar);

        setTitle("");

        mail = getIntent().getParcelableExtra(MAIL_BODY);

        binding.layReply.setOnClickListener(view -> {
            Intent intent = new Intent(this, MailComposeNewActivity.class);
            intent.putExtra(MailComposeNewActivity.MODE, MailComposeNewActivity.MODE_REPLY);
            String[] arr = {mail.getFrom()};

            intent.putExtra(MailComposeNewActivity.MAIL_SUBJECT, mail.getSubject());
            intent.putExtra(MailComposeNewActivity.MAIL_CONTENT, mailContent);
            intent.putExtra(MailComposeNewActivity.MAIL_RECIPIET_ARRAY, arr);

            intent.putExtra(MailComposeNewActivity.MAIL_SENDER, fromAddress);

            intent.putExtra(MailComposeNewActivity.MAIL_UPDATE_TIME, mail.getUdate());

            startActivity(intent);
        });

        binding.layForward.setOnClickListener(view -> presenter.onForwardMailButtonClick());

        binding.ivStarred.setOnClickListener(view -> presenter.onStarredClick());

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        presenter = new MailInfoPresenter();
        presenter.bindView((MailDetailsViewI) this);
        presenter.bindMail(mail);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onStop();
    }

    @Override
    public void setLayDetailesVisability() {
        if (binding.layDetails.getVisibility() == View.VISIBLE) {
            binding.layDetails.setVisibility(View.GONE);
        } else {
            binding.layDetails.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setSubject(String text) {
        binding.tvSubject.setText(text);
    }

    @Override
    public void setAtachmets(List<MailAttachment> list) {

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;

        int columnCount = (int) Math.ceil((dpWidth - 40) / 70);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(columnCount, StaggeredGridLayoutManager.VERTICAL);

        binding.rvAtachments.setLayoutManager(layoutManager);

        BaseArrayListAdapter<MailAttachment, MailIncomingAtachmentHolder> adapter =
                new BaseArrayListAdapter<MailAttachment, MailIncomingAtachmentHolder>(MailAttachment.class, R.layout.mail_attachment,
                        MailIncomingAtachmentHolder.class, list) {
                    @Override
                    protected void populateViewHolder(final MailIncomingAtachmentHolder viewHolder, final MailAttachment model, final int position) {
                        viewHolder.bind(model);
                    }
                };

        binding.rvAtachments.addOnItemTouchListener(new RecyclerItemClickListener(this, (view, position) -> {

           /* List<String> attacmentList = new ArrayList<>();
            for (MailAttachment mailAttachment : list) {
                String mailAtach = mailAttachment.filePath;
                attacmentList.add(mailAtach);
            }*/
            MailAttachment m = list.get(position);

            if (m.mimeType.split("/")[0].equals("image")) {
                Observable.from(list)
                        .filter(mailAttachment -> mailAttachment.mimeType.split("/")[0].equals("image")
                        )
                        .flatMap(mailAttachment -> Observable.just(mailAttachment.filePath))
                        .toList()
                        .flatMap(list2 -> Observable.just(list2.toArray(new String[list2.size()])))
                        .subscribe(imageArray -> {
                            //Log.i("TAG", "imageArray.length=" + imageArray.length);

                            Intent intent = new Intent(this, ImageActivity.class);
                            intent.putExtra(ImageActivity.KEY_IMAGES_ARRAY, imageArray);
                            intent.putExtra(ImageActivity.KEY_IMAGES_START_POSITION, position);
                            view.getContext().startActivity(intent);

                        }, Throwable::printStackTrace);
            } else {

                try {

                    if (isStoragePermissionGranted()) {

                        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(m.filePath));
                        request.setDescription(m.name);
                        request.setTitle("Gunners. Load mail attachment");

                        request.allowScanningByMediaScanner();
                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "mail_attacments");

                        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                        manager.enqueue(request);

                         /* PackageManager packageManager = getPackageManager();
                    Intent intent = packageManager.getLaunchIntentForPackage(getPackageName());
                    ComponentName componentName = intent.getComponent();
                    Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
                    startActivity(mainIntent);
                    System.exit(0);*/
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }));

        binding.rvAtachments.setAdapter(adapter);

    }

    @Override
    public void setMailContent(String mailContent, String mimeType) {
        this.mailContent = mailContent;
        binding.webMessageBody.loadDataWithBaseURL("", mailContent, mimeType, "UTF-8", "");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_mail_mail_content, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_mail) {
            Intent listMailsIntent = new Intent(this, ListEmailActivity.class);
            listMailsIntent.putExtra(ListEmailActivity.FOLDER, "inbox");
            startActivity(listMailsIntent);
            return true;
        } else if (id == R.id.action_delete) {
            DialogInterface.OnClickListener onPositiveClickListener = (dialogInterface, i) -> presenter.onRemoveMailButtonClick();
            new AcceptRejectDialog(this, "Delete this message?", onPositiveClickListener).show();
            return true;
        } else if (id == R.id.action_reply) {
            Intent intent = new Intent(this, MailComposeNewActivity.class);
            String[] arr = {mail.getFrom()};
            intent.putExtra(MailComposeNewActivity.MAIL_RECIPIET_ARRAY, arr);
            startActivity(intent);

        } else if (id == R.id.action_forward) {
            presenter.onForwardMailButtonClick();

        } else if (id == R.id.action_mark_as_unread) {

            presenter.onButtonSetUnreadClick();

        } else if (id == R.id.action_move) {
            presenter.onMoveButtonClick();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showError(String text) {

    }

    @Override
    public void setFolderName(String folderName) {
        binding.tvFolder.setText(folderName);
    }

    @Override
    public void setProgressBarVisability(boolean visability) {
        if (visability) {
            binding.progressBar.setVisibility(View.VISIBLE);
        } else {
            binding.progressBar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void setAvatarText(String text) {
        binding.ivAvatar.setText(text);
    }

    @Override
    public void setStarred(boolean flagged) {
        int starRes;
        if (flagged) {
            starRes = R.drawable.mail_star;
            binding.ivStarred.setImageResource(starRes);
        } else {
            starRes = R.drawable.mail_star_empty;
        }
        binding.ivStarred.setImageResource(starRes);
    }

    @Override
    public void setDateTime(String timeDateTimeStr) {
        binding.tvDateTime.setText(timeDateTimeStr);
    }

    @Override
    public void setDate(String timeDateStr) {
        binding.tvDate.setText(timeDateStr);
    }

    @Override
    public void setToMail(List<String> to) {
        binding.tvToMail.setText(to.get(0));
        binding.tvToAddress.setText(to.get(0));
    }

    @Override
    public void setFromAddres(String fromAddress) {
        this.fromAddress = fromAddress;
        binding.tvFromMail.setText(fromAddress);
    }

    @Override
    public void setFromName(String fromName) {
        binding.tvFromName.setText(fromName);
        binding.tvFrom.setText(fromName);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void dismiss() {
        onBackPressed();
    }

    @Override
    public void runMailComposeNewActivityForwardMode(String subject, String mailContent) {

        Intent intent = new Intent(this, MailComposeNewActivity.class);
        intent.putExtra(MailComposeNewActivity.MODE, MailComposeNewActivity.MODE_FORWARD);

        String[] arr = {mail.getFrom()};
        intent.putExtra(MailComposeNewActivity.MAIL_SUBJECT, mail.getSubject());
        intent.putExtra(MailComposeNewActivity.MAIL_RECIPIET_ARRAY, arr);

        intent.putExtra(MailComposeNewActivity.MAIL_SENDER, fromAddress);

        intent.putExtra(MailComposeNewActivity.MAIL_UPDATE_TIME, mail.getUdate());

        intent.putExtra(MailComposeNewActivity.MAIL_SUBJECT, subject);
        intent.putExtra(MailComposeNewActivity.MAIL_CONTENT, mailContent);

        startActivity(intent);
    }

    @Override
    public void runMoveActivity(String folderName, int uid) {

        Intent intent = new Intent(this, SelectFolderMailMoveActivity.class);
        intent.putExtra(SelectFolderMailMoveActivity.MAIL_UID, uid);

        intent.putExtra(SelectFolderMailMoveActivity.MAIL_SOURCE_FOLDER, folderName);

        startActivityForResult(intent, REQUEST_CODE_MOVE_MAIL);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // запишем в лог значения requestCode и resultCode
        Log.d("myLogs", "requestCode = " + requestCode + ", resultCode = " + resultCode);
        // если пришло ОК
        if (resultCode == RESULT_OK) {
            switch (requestCode) {

                case REQUEST_CODE_MOVE_MAIL:

                    String folderName = data.getStringExtra(FOLDER_NAME);
                    presenter.onSelectFolder(folderName);
                    break;
            }
        }
    }

   /* @Override
    public void runMailComposeNewActivityReplyMode(String mailFrom, String subject, String mailContent) {

        Intent intent = new Intent(this, MailComposeNewActivity.class);
        intent.putExtra(MailComposeNewActivity.MODE, MailComposeNewActivity.MODE_REPLY);

        intent.putExtra(MailComposeNewActivity.MAIL_TO, mailFrom);


        intent.putExtra(MailComposeNewActivity.MAIL_SUBJECT, subject);
        intent.putExtra(MailComposeNewActivity.MAIL_CONTENT, mailContent);

        startActivity(intent);
    }*/

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {

                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

}
