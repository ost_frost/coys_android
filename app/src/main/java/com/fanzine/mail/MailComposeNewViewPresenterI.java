package com.fanzine.mail;

public interface MailComposeNewViewPresenterI {

    void bindView(MailComposeNewI view);

    void onStart();

    void onStop();

    void onSendEmailClick();

    void setModeReply(String mailSender, String[] recipientArr, String subject, String mailContent, long mailUpdateTime);

    void setModeForward(String mailSender, String[] recipientArr, String subject, String mailContent, long mailUpdateTime);

    void setModeNewMail();

    void setEmailSignture();
}
