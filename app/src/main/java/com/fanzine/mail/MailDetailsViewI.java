package com.fanzine.mail;

import com.fanzine.coys.models.mails.MailAttachment;

import java.util.List;

/**
 * Created by a on 04.03.18
 */

interface MailDetailsViewI {

    void setLayDetailesVisability();

    void setSubject(String text);

    void setAtachmets(List<MailAttachment> list);

    void setMailContent(String data, String mimeType);

    void showError(String text);

    void setFolderName(String folderName);

    void setProgressBarVisability(boolean visability);

    void setAvatarText(String text);

    void setStarred(boolean flagged);

    void setDateTime(String timeDateTimeStr);

    void setDate(String timeDateStr);

    void setToMail(List<String> to);

    void setFromAddres(String fromAddress);

    void setFromName(String fromName);

    void dismiss();

    void runMailComposeNewActivityForwardMode(String subject, String mailContent);

    void runMoveActivity(String folderName, int uid);
}
