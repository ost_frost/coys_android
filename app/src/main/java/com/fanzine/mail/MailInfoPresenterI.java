package com.fanzine.mail;

import com.fanzine.coys.models.mails.Mail;

/**
 * Created by a on 11.03.18
 */

interface MailInfoPresenterI {
    void bindView(MailDetailsViewI view);

    void onStarredClick();

    void bindMail(Mail mail);

    void onRemoveMailButtonClick();

    void onForwardMailButtonClick();

    void onButtonSetUnreadClick();

    void onMoveButtonClick();

    void onStart();

    void onStop();

    void onSelectFolder(String folderName);
}
