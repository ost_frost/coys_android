package com.fanzine.mail;

import com.fanzine.coys.models.mails.Mail;
import com.fanzine.mail.view.activity.folder.ListEmailI;

import java.util.Set;

/**
 * Created by a on 14.03.18
 */

public interface ListEmailPresenterI {
    void bindView(ListEmailI view);

    void unbindView();

    void onSwapRvSwap();

    void setFolder(String folder);

    void changeSelectedItems(Set<Mail> selectedItems);

    void onTrashButtonClick();

    void onTrashAllMailsButtonClick();

    void onStart();

    void onStop();

    void applyListMode();
    void applyEmptyMode();
}
