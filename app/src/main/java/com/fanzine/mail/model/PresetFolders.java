package com.fanzine.mail.model;

/**
 * Created by a on 19.03.18
 */

public class PresetFolders {
    public static final String INBOX ="Inbox";
    public static final String SENT="Sent";
    public static final String TRASH="Trash";
    public static final String SPAM="Spam";
    public static final String DRAFTS="Drafts";
    public static final String STARRED="Flagged";
}
