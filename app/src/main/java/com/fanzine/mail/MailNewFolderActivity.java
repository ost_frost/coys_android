package com.fanzine.mail;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.base.BaseActivity;
import com.fanzine.coys.databinding.ActivityMailNewFolderBinding;
import com.fanzine.coys.viewmodels.fragments.mails.MailNewFolderViewModel;
import com.jakewharton.rxbinding.widget.RxTextView;

/**
 * Created by Evgenij Krasilnikov on 13-Feb-18.
 */

public class MailNewFolderActivity extends BaseActivity {

    private ActivityMailNewFolderBinding binding;
    private MenuItem itemSave;
    private MailNewFolderViewModel viewModel;

    public static void start(Context context) {
        context.startActivity(new Intent(context, MailNewFolderActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMailNewFolderBinding.inflate(getLayoutInflater());
        viewModel = new MailNewFolderViewModel(this);
        binding.setViewModel(viewModel);
        setBaseViewModel(viewModel);
        setContentView(binding.getRoot());
        setTitle(R.string.ac_nefo_title);
        setToolbar();
        binding.acNefoFolderName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                setTitle(s.length() > 0 ? s.toString() : getString(R.string.ac_nefo_title));
                itemSave.setEnabled(s.length() > 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        RxTextView.editorActions(binding.acNefoFolderName).subscribe(integer -> {
            if (integer.equals(EditorInfo.IME_ACTION_DONE) && binding.acNefoFolderName.getText().length() != 0)
                viewModel.createNewFolder();
        });
    }

    private void setToolbar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            final Drawable crossIcon = ContextCompat.getDrawable(this, R.drawable.ic_close_grey600_24dp);
            actionBar.setHomeAsUpIndicator(crossIcon);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_mail_new_folder, menu);
        itemSave = menu.findItem(R.id.action_save);
        itemSave.setEnabled(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_save:
                viewModel.createNewFolder();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
