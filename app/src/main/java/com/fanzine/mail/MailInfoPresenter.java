package com.fanzine.mail;

import android.util.Log;

import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.models.mails.MailContent;
import com.fanzine.coys.networking.EmailDataRequestManager;
import com.fanzine.mail.model.PresetFolders;
import com.fanzine.mail.utils.DetectHtml;
import com.fanzine.mail.utils.TimeDateUtils;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by a on 11.03.18
 */

class MailInfoPresenter implements MailInfoPresenterI {
    private Mail mail;
    private MailDetailsViewI view;
    private MailContent mailContent;
    private boolean isMovedFlag = false;

    @Override
    public void bindView(MailDetailsViewI view) {
        this.view = view;
    }

    @Override
    public void bindMail(Mail mail) {
        this.mail = mail;
        loadEmail(mail.getFolder(), mail.getUid());
    }

    private void loadEmail(String folder, int id) {
        view.setProgressBarVisability(true);

        ApiRequest.getInstance().getApi().getMailContent(folder, id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        mailContent -> {
                            this.mailContent = mailContent;
                            view.setProgressBarVisability(false);

                            view.setSubject(mailContent.getSubject());

                            view.setFolderName(mailContent.getFolder());

                            boolean isHtml = DetectHtml.isHtml(getMailContent(mailContent));
                            if (isHtml) {
                                view.setMailContent(getMailContent(mailContent), "text/html");

                            } else {
                                view.setMailContent(getMailContent(mailContent), "text/plain");
                            }

                            view.setFromName(mailContent.getFromName());

                            view.setFromAddres(mailContent.getFromAddress());

                            view.setToMail(mailContent.getTo());

                            String timeDateStr = TimeDateUtils.getTimeAgo((long) mailContent.getUdate());
                            view.setDate(timeDateStr);

                            String timeDateTimeStr = TimeDateUtils.convertTimestampToLocalTimeDayMonTime((long) mailContent.getUdate() * 1000);
                            view.setDateTime(timeDateTimeStr);

                            view.setStarred(mailContent.getFlagged());

                            if (mailContent.getFromName() != null && mailContent.getFromName().length() > 0) {
                                view.setAvatarText(mailContent.getFromName().substring(0, 1));
                            } else {
                                view.setAvatarText(mailContent.getFromAddress().substring(0, 1));
                            }

                            view.setAtachmets(mailContent.getAttachments());

                            isMovedFlag = false;

                        },
                        throwable -> {
                            throwable.printStackTrace();

                            view.setProgressBarVisability(false);
                            view.showError(throwable.getLocalizedMessage());
                        });
    }

    @Override
    public void onStarredClick() {
        view.setProgressBarVisability(true);

        EmailDataRequestManager.getInstanse().setMailStarred(mail.getFolder(), String.valueOf(mail.getUid()), mail.isFlagged())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        isFlagged -> {
                            view.setProgressBarVisability(false);

                            if (mail.isFlagged()) {
                                mail.setFlagged(false);
                            } else {
                                mail.setFlagged(true);
                            }

                            view.setStarred(mail.isFlagged());
                        },
                        throwable -> {

                            throwable.printStackTrace();

                            view.setProgressBarVisability(false);

                            view.showError(throwable.getLocalizedMessage());

                        });
    }

    @Override
    public void onRemoveMailButtonClick() {
        String folder = mail.getFolder();
        int uid = mail.getUid();

        view.setProgressBarVisability(true);
        new EmailDataRequestManager().deleteMail(folder, String.valueOf(uid))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        responseBody -> view.dismiss(),
                        throwable -> {
                            view.setProgressBarVisability(false);
                            throwable.printStackTrace();
                            view.showError(throwable.getLocalizedMessage());
                        });
    }

    @Override
    public void onForwardMailButtonClick() {
        view.runMailComposeNewActivityForwardMode(mailContent.getSubject(), getMailContent(mailContent));
    }

    @Override
    public void onButtonSetUnreadClick() {
        view.setProgressBarVisability(true);

        new EmailDataRequestManager().setMailUnread(mail.getFolder(), String.valueOf(mail.getUid()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        responseBody -> {
                            view.setProgressBarVisability(false);
                        },
                        throwable -> {
                            view.setProgressBarVisability(false);
                            throwable.printStackTrace();
                            view.showError(throwable.getLocalizedMessage());
                        });
    }

    @Override
    public void onMoveButtonClick() {
        isMovedFlag = true;
        view.runMoveActivity(mail.getFolder(), mail.getUid());
    }

    @Override
    public void onStart() {

        Log.i("TAG", "public void onStart() isMovedFlag=" + isMovedFlag);
        if (isMovedFlag) {
            loadEmail(mail.getFolder(), mail.getUid());

            isMovedFlag = false;
        }
    }

    @Override
    public void onStop() {

    }

    @Override
    public void onSelectFolder(String folderName) {
        if (folderName.equals(PresetFolders.STARRED)) {
            view.setStarred(true);
        } else {
            view.setFolderName(folderName);
        }
    }

    private String getMailContent(MailContent mailContent1) {
        String mailBody;
        if (mailContent1.getTextHtml() != null && mailContent1.getTextHtml().length() > 0) {
            mailBody = mailContent1.getTextHtml();
        } else {
            mailBody = mailContent1.getTextPlain();
        }
        return mailBody;
    }
}
