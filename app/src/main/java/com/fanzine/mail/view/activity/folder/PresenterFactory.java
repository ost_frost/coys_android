package com.fanzine.mail.view.activity.folder;

import com.fanzine.mail.ListEmailPresenter;
import com.fanzine.mail.model.PresetFolders;

class PresenterFactory {

    public ListEmailPresenter getPresenter(String type) {
        if (type == null) return null;

        if (type.equalsIgnoreCase(PresetFolders.INBOX)) {
            return new InboxPresenter();
        } else if (type.equalsIgnoreCase(PresetFolders.SPAM)) {
            return new SpamPresenter();
        } else if (type.equalsIgnoreCase(PresetFolders.DRAFTS)) {
            return new DraftsPresenter();
        } else if (type.equalsIgnoreCase(PresetFolders.SENT)) {
            return new SentPresenter();
        } else if (type.equalsIgnoreCase(PresetFolders.TRASH)) {
            return new TrashPresenter();
        } else if (type.equalsIgnoreCase(PresetFolders.STARRED)) {
            return new StarredPresenter();
        }

        return null;
    }
}