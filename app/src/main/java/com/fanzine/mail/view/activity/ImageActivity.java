package com.fanzine.mail.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.mail.adapter.RecyclerViewBigImagesAdapter;

public class ImageActivity extends AppCompatActivity {

    public static final String KEY_IMAGES_ARRAY = "KEY_IMAGES_ARRAY";

    public static final String KEY_TITLE = "KEY_TITLE";
    public static final String KEY_DESCR = "KEY_DESCR";
    public static final String KEY_IMAGES_START_POSITION = "KEY_IMAGES_START_POSITION";

    private Toolbar toolbar;
    private TextView txtTitle, txtDescription;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundDrawable(null);

        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setNavigationOnClickListener(v -> finish());

        Intent intent = getIntent();
        final String title = intent.getStringExtra(KEY_TITLE);
        final String description = intent.getStringExtra(KEY_DESCR);
        //final String startImageUrl = intent.getStringExtra(KEY_IMAGE);

        String[] imagesArray = intent.getStringArrayExtra(KEY_IMAGES_ARRAY);
        int startImagePosition = intent.getIntExtra(KEY_IMAGES_START_POSITION, 1);

        setImagesRecycler(imagesArray, startImagePosition);

        //txtTitle.setText(title);

        //txtDescription.setText(description);

       /* final ImageView webImage = (ImageView) findViewById(R.id.web_image);

        Picasso.with(this)
                .load(startImageUrl)
                .into(webImage);*/

    }

    private void setImagesRecycler(String[] imagesArray, int startImagePosition) {
        RecyclerView rv = (RecyclerView) findViewById(R.id.rv);

        SnapHelper helper = new LinearSnapHelper();
        helper.attachToRecyclerView(rv);

        if (imagesArray != null && imagesArray.length > 0) {

            LinearLayoutManager layoutManager
                    = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

            if (rv != null) {
                rv.setLayoutManager(layoutManager);
                layoutManager.scrollToPosition(startImagePosition);
            }
            rv.setHasFixedSize(true);

            RecyclerViewBigImagesAdapter adapter = new RecyclerViewBigImagesAdapter(imagesArray);
            rv.setAdapter(adapter);

        } else {
        }

    }

}