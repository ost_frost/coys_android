package com.fanzine.mail.view.activity.folder;

import com.fanzine.mail.ListEmailPresenter;

public class StarredPresenter extends ListEmailPresenter {

    @Override
    protected String getTitle() {
        return "StarredPresenter";
    }

    @Override
    public void applyEmptyMode() {
        super.applyEmptyMode();

        view.showStarredBackground();
    }
}
