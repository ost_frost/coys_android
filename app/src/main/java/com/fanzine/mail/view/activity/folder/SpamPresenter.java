package com.fanzine.mail.view.activity.folder;

import com.fanzine.mail.ListEmailPresenter;
import com.fanzine.mail.model.PresetFolders;

class SpamPresenter extends ListEmailPresenter {

    @Override
    protected String getTitle() {
        return PresetFolders.SPAM;
    }

    @Override
    public void applyListMode() {
        super.applyListMode();

        view.showEmptySpamView();
        view.configEmptySpamButton();
    }

    @Override
    public void applyEmptyMode() {
        super.applyEmptyMode();

        view.showSpamBackground();
        view.hideEmptySpamView();
    }
}