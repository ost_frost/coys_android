package com.fanzine.mail.view.activity.folder;

import com.fanzine.coys.models.mails.Mail;

import java.util.List;

/**
 * Created by a on 14.03.18
 */

public interface ListEmailI {

    int TOOLBAR_MODE_MAIN = 10;
    int TOOLBAR_MODE_SEARCH = 11;
    int TOOLBAR_MODE_SELECT_ITEMS = 12;

    void setRecyclerData(List<Mail> data);

    void setTitle(String title);

    void showErrorMsg(String message);

    void setProgressVisability(boolean b);

    void setMailListMode();

    void removeMailFromList(Mail mail);

    void setToolbarMode(int toolbarMode);

    void setMessageCount(String count);

    void showEmailsNotFound();

    void showEmptyFolderView();
    void hideEmptyFolderView();

    void setFolderTitle(String title);

    void hideTrashIcon();
    void showTrashIcon();

    void hideEmptySpamView();
    void showEmptySpamView();

    void configEmptyTrashButton();
    void configEmptySpamButton();

    void hideListView();

    void showSpamBackground();
    void showInboxBackground();
    void showDraftsBackground();
    void showStarredBackground();
    void showSentBackground();
}
