package com.fanzine.mail.view.activity.folder;

import com.fanzine.mail.ListEmailPresenter;
import com.fanzine.mail.model.PresetFolders;

class SentPresenter extends ListEmailPresenter {

    @Override
    protected String getTitle() {
        return PresetFolders.SENT;
    }

    @Override
    public void applyEmptyMode() {
        super.applyEmptyMode();

        view.showSentBackground();
    }
}