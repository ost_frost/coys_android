package com.fanzine.mail.view.activity.folder;
import com.fanzine.mail.ListEmailPresenter;
import com.fanzine.mail.model.PresetFolders;

class TrashPresenter extends ListEmailPresenter {


    @Override
    protected String getTitle() {
        return PresetFolders.TRASH;
    }

    @Override
    public void applyListMode() {
        super.applyListMode();

        view.showTrashIcon();
        view.configEmptyTrashButton();
    }

    @Override
    public void applyEmptyMode() {
        super.applyEmptyMode();

        view.showStarredBackground();
        view.hideTrashIcon();
    }
}