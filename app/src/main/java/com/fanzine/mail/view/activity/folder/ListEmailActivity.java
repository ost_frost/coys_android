package com.fanzine.mail.view.activity.folder;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.base.NavigationFragmentActivity;
import com.fanzine.coys.adapters.mails.EmailsAdapter;
import com.fanzine.coys.databinding.FragmentListEmailBinding;
import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.widgets.RecyclerViewLineItemDecoration;
import com.fanzine.chat3.view.custom.AcceptRejectDialog;
import com.fanzine.mail.ListEmailPresenterI;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.lang.reflect.Field;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class ListEmailActivity extends NavigationFragmentActivity implements ListEmailI {

    public static final String FOLDER = "folder";

    public static final String DEFAULT_FOLDER = "INBOX";
    private Context context = this;

    private FragmentListEmailBinding binding;
    //private ListEmailsViewModel viewModel;
    private EmailsAdapter adapter;

    private List<Mail> data;

    private String folder;
    private Subscription etSearchSub;

    private ListEmailPresenterI presenter;
    private MailRecycler mailRecycler;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = FragmentListEmailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.progressBar.setVisibility(View.GONE);
        setToolbar();
        setRedStatusBar(getWindow());

        binding.btMenu.setOnClickListener(view -> onBackPressed());

        binding.ivSearch.setOnClickListener(view -> {
            binding.laySearch.setVisibility(View.VISIBLE);
            binding.layToolbarMain.setVisibility(View.GONE);
            binding.toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));

            binding.etSearch.requestFocus();

            etSearchSub =
                    RxTextView.textChanges(binding.etSearch)
                            .skip(1)
                            .debounce(200, TimeUnit.MILLISECONDS)
                            //.filter(text -> text.length() > 0)
                            .subscribe(text ->
                                            Observable.from(data)
                                                    .filter(mail -> mail.getFrom().toLowerCase().contains(text.toString().toLowerCase()))
                                                    .toList()
                                                    .observeOn(AndroidSchedulers.mainThread())
                                                    .subscribe(this::search, Throwable::printStackTrace),
                                    Throwable::printStackTrace);
        });
        setToolbarMode(TOOLBAR_MODE_MAIN);

        binding.ivBack.setOnClickListener(view -> {

            setToolbarMode(TOOLBAR_MODE_MAIN);

            etSearchSub.unsubscribe();
            mailRecycler.search(data);
        });

        binding.ivTrash.setOnClickListener(view -> {
            DialogInterface.OnClickListener onPositiveClickListener = (dialogInterface, i) -> presenter.onTrashButtonClick();
            new AcceptRejectDialog(context, "Delete this messages?", onPositiveClickListener).show();
        });

        mailRecycler = new MailRecycler(this);
        mailRecycler.init();

        try {
            Field f = binding.swipe.getClass().getDeclaredField("mCircleView");
            f.setAccessible(true);
            ImageView img = (ImageView) f.get(binding.swipe);
            img.setAlpha(0.0f);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        binding.swipe.setOnRefreshListener(() -> presenter.onSwapRvSwap());

        folder = getIntent().getStringExtra(FOLDER);

        PresenterFactory folderFactory = new PresenterFactory();

        presenter = folderFactory.getPresenter(folder);
        presenter.bindView(this);
        presenter.setFolder(folder);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onStop();
    }

    private void setToolbar() {
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    public void setRedStatusBar(Window window) {
        if (window != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
    }

    private void search(List<Mail> data1) {
        mailRecycler.search(data1);
    }

    @Override
    public void setRecyclerData(List<Mail> data) {
        this.data = data;

        mailRecycler.setRecyclerData(data);
    }

    @Override
    public void setTitle(String title) {
        binding.toolbarTitle.setText(title);
    }

    @Override
    public void showErrorMsg(String message) {

    }

    ProgressDialog pd = null;

    @Override
    public void setProgressVisability(boolean b) {
        if (b) {
            pd = DialogUtils.showProgressDialog(this, R.string.please_wait);
        } else {
            if (pd != null) {
                pd.dismiss();
            }
        }
    }

    @Override
    public void showEmailsNotFound() {
        presenter.applyEmptyMode();
    }

    @Override
    public void setMailListMode() {
        presenter.applyListMode();
    }

    @Override
    public void removeMailFromList(Mail mail) {
        mailRecycler.removeItem(mail);
    }

    @Override
    public void setToolbarMode(int toolbarMode) {
        if (toolbarMode == TOOLBAR_MODE_MAIN) {
            binding.etSearch.setText("");
            binding.toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.toobar_color));

            binding.layToolbarMain.setVisibility(View.VISIBLE);

            binding.laySearch.setVisibility(View.GONE);
            binding.laySelectedItemsAction.setVisibility(View.GONE);
        } else if (toolbarMode == TOOLBAR_MODE_SEARCH) {
            binding.toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.toobar_color));

            binding.laySearch.setVisibility(View.VISIBLE);

            binding.layToolbarMain.setVisibility(View.GONE);
            binding.laySelectedItemsAction.setVisibility(View.GONE);
        } else if (toolbarMode == TOOLBAR_MODE_SELECT_ITEMS) {
            binding.laySelectedItemsAction.setVisibility(View.VISIBLE);

            binding.layToolbarMain.setVisibility(View.GONE);
            binding.laySearch.setVisibility(View.GONE);
            binding.toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        }
    }

    @Override
    public void setMessageCount(String count) {
        binding.tvCount.setText(count);
    }

    @Override
    public void showEmptyFolderView() {
        binding.layEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyFolderView() {
        binding.layEmpty.setVisibility(View.GONE);
    }

    @Override
    public void setFolderTitle(String title) {
        binding.tvEmpty.setText("Nothing in " + title);
    }

    @Override
    public void hideTrashIcon() {
        binding.layEmptyTrashNow.setVisibility(View.GONE);
    }

    @Override
    public void showTrashIcon() {
        binding.layEmptyTrashNow.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptySpamView() {
        binding.layEmptySpamNow.setVisibility(View.GONE);
    }

    @Override
    public void showEmptySpamView() {
        binding.layEmptySpamNow.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideListView() {
        binding.lay.setVisibility(View.GONE);
    }

    @Override
    public void configEmptyTrashButton() {
        binding.tvEmptyTrashNowButton.setOnClickListener(view -> {

            DialogInterface.OnClickListener onPositiveClickListener = (dialogInterface, i) -> presenter.onTrashAllMailsButtonClick();

            new AcceptRejectDialog(context, "You are about to permanently delete all items.Do you want to continue?", onPositiveClickListener).show();

        });
    }

    @Override
    public void configEmptySpamButton() {
        binding.tvEmptySpamNowButton.setOnClickListener(view -> {

            DialogInterface.OnClickListener onPositiveClickListener = (dialogInterface, i) -> presenter.onTrashAllMailsButtonClick();

            new AcceptRejectDialog(context, "You are about to permanently delete all items. Do you want to continue?", onPositiveClickListener).show();
        });
    }

    @Override
    public void showSpamBackground() {
        binding.ivEmptyFolder.setImageResource(R.drawable.ic_mail_empty_spam);
    }

    @Override
    public void showInboxBackground() {
        binding.ivEmptyFolder.setImageResource(R.drawable.ic_mail_empty_inbox_folder);
    }

    @Override
    public void showDraftsBackground() {
        binding.ivEmptyFolder.setImageResource(R.drawable.ic_mail_empty_drafts_folder);
    }

    @Override
    public void showStarredBackground() {
        binding.ivEmptyFolder.setImageResource(R.drawable.ic_mail_empty_starred_folder);
    }

    @Override
    public void showSentBackground() {
        binding.ivEmptyFolder.setImageResource(R.drawable.ic_mail_empty_sent_folder);
    }

    class MailRecycler {
        private Context context;
        private List<Mail> data;

        MailRecycler(Context context) {
            this.context = context;
        }

        public void setRecyclerData(List<Mail> data) {

            this.data = data;
            adapter = new EmailsAdapter(context, data);

            adapter.setOnItemSelectListener(selectedItems -> presenter.changeSelectedItems(selectedItems));

            binding.rvListInbox.setAdapter(adapter);

            binding.swipe.setRefreshing(false);
        }

        public void init() {

            RecyclerView rv = binding.rvListInbox;
            rv.setLayoutManager(new LinearLayoutManager(context));
            rv.addItemDecoration(new RecyclerViewLineItemDecoration(getApplicationContext()));
        }

        private void search(List<Mail> data) {
            this.data = data;

            adapter = new EmailsAdapter(context, data);
            binding.rvListInbox.setAdapter(adapter);
            binding.rvListInbox.invalidate();
            binding.swipe.setRefreshing(false);
        }


        public void removeItem(Mail mail) {
            int position = data.indexOf(mail);
            this.data.remove(position);
            adapter.notifyItemRemoved(position);
        }
    }
}
