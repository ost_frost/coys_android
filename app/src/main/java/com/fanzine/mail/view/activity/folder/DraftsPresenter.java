package com.fanzine.mail.view.activity.folder;
import com.fanzine.mail.ListEmailPresenter;
import com.fanzine.mail.model.PresetFolders;

class DraftsPresenter extends ListEmailPresenter {

    @Override
    protected String getTitle() {
        return PresetFolders.DRAFTS;
    }

    @Override
    public void applyEmptyMode() {
        super.applyEmptyMode();

        view.showDraftsBackground();
    }
}
