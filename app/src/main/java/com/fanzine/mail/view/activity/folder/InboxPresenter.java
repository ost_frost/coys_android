package com.fanzine.mail.view.activity.folder;

import com.fanzine.mail.ListEmailPresenter;
import com.fanzine.mail.model.PresetFolders;

class InboxPresenter extends ListEmailPresenter {

    @Override
    protected String getTitle() {
        return PresetFolders.INBOX;
    }

    @Override
    public void applyEmptyMode() {
        super.applyEmptyMode();

        view.showInboxBackground();
    }
}

