package com.fanzine.mail;

import com.fanzine.coys.models.mails.NewMail;

public interface MailComposeNewI {
    String MODE = "mode";
    String MODE_FORWARD = "mode_forward";
    String MODE_REPLY = "mode_reply";
    String MODE_NEW_MAIL = "mode_new_mail";

    String MAIL_RECIPIET_ARRAY = "mail_array";
    String MAIL_SUBJECT = "mail_subject";
    String MAIL_CONTENT = "mail_body";
    String MAIL_SENDER = "mail_sender";


    String MAIL_UPDATE_TIME = "mail_update_time";

    String NEWS_URL = "NEWS_URL";
    String MAIL_TO = "mail_to";

    void setMailContent(String mailContent, String mimeType);

    void setCursorToMailContentEdit();

    void setSubject(String subject);

    void showError(String something_goes_wrong);

    void finishView();

    void setProgressBarVisability(boolean b);

    void setTo(String recipientStr);

    NewMail getMail();

    void showUserMessage(String text);

    void attachSignuture(String text);
}
