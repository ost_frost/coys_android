package com.fanzine.mail;

import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.networking.EmailDataRequestManager;
import com.fanzine.mail.model.PresetFolders;
import com.fanzine.mail.view.activity.folder.ListEmailI;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.mail.view.activity.folder.ListEmailI.TOOLBAR_MODE_MAIN;
import static com.fanzine.mail.view.activity.folder.ListEmailI.TOOLBAR_MODE_SELECT_ITEMS;

/**
 * Created by a on 14.03.18
 */
public abstract class ListEmailPresenter implements ListEmailPresenterI {
    protected ListEmailI view;
    private String folder;
    private Set<Mail> selectedItems;
    private List<Mail> mailList;
    private Subscription subscription;


    protected abstract String getTitle();


    public void applyListMode() {
        view.hideEmptyFolderView();
    }

    public void applyEmptyMode() {
        String title = getTitle();

        view.hideListView();
        view.showEmptyFolderView();
        view.setFolderTitle(title);
    }

    @Override
    public void bindView(ListEmailI view) {
        this.view = view;
    }

    @Override
    public void unbindView() {

    }

    @Override
    public void onStart() {
        loadEmailList(folder);
        setSelectItems(0);
    }

    @Override
    public void onStop() {
        unsubscribeAll();
    }

    @Override
    public void setFolder(String folder) {
        this.folder = folder;

        if (folder.equals("Flagged")) {
            view.setTitle("StarredPresenter");
        } else {
            view.setTitle(folder);
        }
    }

    @Override
    public void changeSelectedItems(Set<Mail> selectedItems) {
        this.selectedItems = selectedItems;
        int selectedItemsSize = selectedItems.size();
        setSelectItems(selectedItemsSize);
    }

    private void setSelectItems(int selectedItemsSize) {
        if (selectedItemsSize > 0) {
            view.setToolbarMode(TOOLBAR_MODE_SELECT_ITEMS);
            view.setMessageCount(String.valueOf(selectedItems.size()));
        } else {
            view.setToolbarMode(TOOLBAR_MODE_MAIN);
        }
    }

    @Override
    public void onTrashButtonClick() {
        view.setProgressVisability(true);

        if (folder.equals(PresetFolders.TRASH)) {
            if (selectedItems != null && selectedItems.size() > 0) {

                Mail[] selectedItemsArray = selectedItems.toArray(new Mail[selectedItems.size()]);

                EmailDataRequestManager.getInstanse().deleteMailList(selectedItemsArray)
                        .subscribe(mail -> {
                            view.removeMailFromList(mail);

                            mailList.remove(mail);

                            selectedItems.remove(mail);
                            changeSelectedItems(selectedItems);

                            changeMailList();

                            loadEmailList(folder);
                            setSelectItems(0);

                        }, throwable -> {
                            view.setProgressVisability(false);
                            throwable.printStackTrace();
                        }, () -> {
                            view.setProgressVisability(false);
                            loadEmailList(folder);
                        });
            }

        } else {
            if (mailList != null && mailList.size() > 0) {

                Mail[] mailArray = selectedItems.toArray(new Mail[mailList.size()]);
                EmailDataRequestManager.getInstanse().moveMailsToFolder(mailArray, PresetFolders.TRASH)

                        .subscribe(mail -> {
                            view.removeMailFromList(mail);
                            mailList.remove(mail);
                            changeMailList();

                            selectedItems.remove(mail);
                            changeSelectedItems(selectedItems);

                        }, throwable -> {
                            view.setProgressVisability(false);
                            loadEmailList(folder);

                        }, () -> {
                            view.setProgressVisability(false);
                            loadEmailList(folder);
                        })
                ;
            }
        }
    }

    @Override
    public void onTrashAllMailsButtonClick() {

        view.setProgressVisability(true);

        if (folder.equals(PresetFolders.TRASH)) {
            if (mailList != null && mailList.size() > 0) {

                Mail[] mailArray = mailList.toArray(new Mail[mailList.size()]);
                EmailDataRequestManager.getInstanse().deleteMailList(mailArray)

                        .subscribe(mail -> {
                                    view.removeMailFromList(mail);
                                    mailList.remove(mail);

                                    changeMailList();
                                },
                                throwable -> {
                                    throwable.printStackTrace();
                                    view.setProgressVisability(false);

                                },
                                () -> {
                                    view.setProgressVisability(false);
                                    setSelectItems(0);

                                });
            }

        } else {
            if (mailList != null && mailList.size() > 0) {

                Mail[] mailArray = mailList.toArray(new Mail[mailList.size()]);
                EmailDataRequestManager.getInstanse().moveMailsToFolder(mailArray, PresetFolders.TRASH)

                        .subscribe(mail -> {
                                    view.removeMailFromList(mail);
                                    mailList.remove(mail);
                                    changeMailList();

                                    selectedItems.remove(mail);
                                    changeSelectedItems(selectedItems);
                                },
                                Throwable::printStackTrace, () -> {
                                    view.setProgressVisability(false);
                                });
            }
        }
    }


    @Override
    public void onSwapRvSwap() {
        loadEmailList(folder);
    }

    private void loadEmailList(String folder1) {
        view.setProgressVisability(true);
        setSelectItems(0);

        Observer<? super List<Mail>> subs = new Observer<List<Mail>>() {

            @Override
            public void onCompleted() {
                view.setProgressVisability(false);
            }

            @Override
            public void onError(Throwable throwable) {
                throwable.printStackTrace();
                view.setProgressVisability(false);
                view.showErrorMsg(throwable.getLocalizedMessage());
            }

            @Override
            public void onNext(List<Mail> mailList) {
                ListEmailPresenter.this.mailList = mailList;
                if (mailList.size() > 0) {

                    Collections.sort(mailList, (mail, t1) -> {
                        int compare = Integer.compare(mail.getUid(), t1.getUid());
                        compare *= -1;
                        return compare;
                    });

                    view.setRecyclerData(mailList);
                    view.setMailListMode();
                } else {
                    view.showEmailsNotFound();
                }
            }
        };

        if (folder1.equals("Flagged")) {
            subscription = EmailDataRequestManager.getInstanse().getMailStarred()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subs);

        } else {
            subscription = EmailDataRequestManager.getInstanse().getMailsByFolder(folder1)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subs);
        }
    }

    private void changeMailList() {
        if (mailList.size() > 0) {

            view.setMailListMode();

        } else {
            view.showEmailsNotFound();
        }
    }

    private void unsubscribeAll() {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }
}
