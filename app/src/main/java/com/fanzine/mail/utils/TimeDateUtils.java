package com.fanzine.mail.utils;

import android.annotation.SuppressLint;
import android.util.Log;

import com.fanzine.coys.viewmodels.fragments.team.player.ProfileViewModel;
import com.fanzine.chat3.ChatData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class TimeDateUtils {
    public static final String LOG_TAG = ChatData.LOG_TAG + "TimeDateUtils";
    private static final String dateFormat = "HH:mm:ss";

    private static final String EMPTY_VALUE = "";

    private static final String DF_SERVER = "yyyy-MM-dd HH:mm:ss.SSS";
    private static final String DF_DD_MM_YYYY = "dd/MM/yyyy";

    private static final String DF_BIRTH_YEAR = "dd MMMM yyyy";

    @SuppressLint("SimpleDateFormat")
    public static String convert(String date, String fromPattern, String toPattern) {
        try {
            Date d = new SimpleDateFormat(fromPattern, Locale.ENGLISH)
                    .parse(date);
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            return new SimpleDateFormat(toPattern).format(cal.getTime());
        } catch (ParseException e) {
            Log.i(ProfileViewModel.class.getName(), e.getMessage());
        } catch (NullPointerException e) {
            Log.i(ProfileViewModel.class.getName(), e.getMessage());
        }
        return EMPTY_VALUE;
    }


    // String dateFormat = Tue, 3 Oct 2017 at 15:48;
    public static String convertTimestampToLocalTimeDayMonTime(Long timestamp) {
        timestamp *= 1000;

        Date date = new Date(timestamp);
        date = clearDate(date);

        SimpleDateFormat outFormat = new SimpleDateFormat("EEE, dd MMM yyyy 'at' HH:mm");

        String dayName = outFormat.format(date);

        return dayName;
    }

    // String dateFormat = Tue, 3 Oct 2017 at 15:48;
    public static String convertTimestampTimeMailReplyForward(Long timestamp) {
        timestamp *= 1000;

        Date date = new Date(timestamp);

        SimpleDateFormat outFormat1 = new SimpleDateFormat("EEE, dd MMM yyyy 'at' HH:mm");

        String dayName1 = outFormat1.format(date);

        return dayName1;
    }


    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return "";
        }

        Date date = new Date(time);

        Date currentDate = getCurrentDate();
        Date previousDate = getPreviousDate(currentDate);

       /*
        SimpleDateFormat outFormat1 = new SimpleDateFormat("dd MMM yyyy 'at' HH:mm");
        Log.i("TAG", "currentDate="+outFormat1.format(currentDate));
        Log.i("TAG", "previousDate="+outFormat1.format(previousDate));

         Log.i("TAG", "date.compareTo(currentDate)="+date.compareTo(currentDate));
        */


        if (date.compareTo(currentDate) == 1) {

            SimpleDateFormat inFormat = new SimpleDateFormat("HH:mm");
            String dayName = inFormat.format(date);
            return dayName;
        } else if (date.compareTo(previousDate) == 1) {

            return "yesterday";

        } else if (date.after(getOneWeekAgoDate(date)) || date.before(getPreviousDate(date))) {
            SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
            String dayName = outFormat.format(date);
            return dayName;
        } else {
            SimpleDateFormat inFormat = new SimpleDateFormat("dd-MM-yyyy");
            String dayName = inFormat.format(date);
            return dayName;
        }
    }

    private static Date getCurrentDate() {
        Date currentDate = new Date();

        final Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);

        currentDate = cal.getTime();
        currentDate = clearDate(currentDate);
        return currentDate;
    }

    private static Date getPreviousDate(Date date) {
        final Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        cal.add(Calendar.DATE, -1);
        date = cal.getTime();
        date = clearDate(date);

        return date;
    }

    private static Date getOneWeekAgoDate(Date date) {
        final Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        cal.add(Calendar.DATE, -6);
        date = cal.getTime();
        date = clearDate(date);

        return date;
    }

    private static Date clearDate(Date date) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        cal.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
        cal.set(Calendar.MINUTE, 0);                 // set minute in hour
        cal.set(Calendar.SECOND, 0);                 // set second in minute
        cal.set(Calendar.MILLISECOND, 0);            // set millisecond in second

        return cal.getTime();
    }
}