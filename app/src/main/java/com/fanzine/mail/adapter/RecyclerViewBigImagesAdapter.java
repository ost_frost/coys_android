package com.fanzine.mail.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fanzine.coys.R;
import com.squareup.picasso.Picasso;


public class RecyclerViewBigImagesAdapter extends RecyclerView.Adapter<RecyclerViewBigImagesAdapter.PersonViewHolder> {

    private String[] persons;
    private View v;

    public RecyclerViewBigImagesAdapter(String[] instBaners) {
        this.persons = instBaners;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_big_images, viewGroup, false);
        return new PersonViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
        personViewHolder.url=persons[i];
        Picasso.with(v.getContext())
                .load(persons[i])
                .into(personViewHolder.image);
    }

    @Override
    public int getItemCount() {
        return persons.length;
    }


    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public String url;

        PersonViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.web_image);

           /* itemView.setOnClickListener(view -> {

                Intent intent = new Intent(itemView.getContext(), ImageActivity.class);
                intent.putExtra(ImageActivity.KEY_IMAGE, url);
//                      intent.putExtra(ImageActivity.KEY_TITLE, ititles[finalId]);
//                        intent.putExtra(ImageActivity.KEY_DESCR, idescriptions[finalId]);
                view.getContext().startActivity(intent);

            });*/
        }
    }
}
