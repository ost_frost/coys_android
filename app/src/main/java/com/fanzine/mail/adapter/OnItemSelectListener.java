package com.fanzine.mail.adapter;


import com.fanzine.coys.models.mails.Mail;

import java.util.Set;

public interface OnItemSelectListener {
    void changeSelectedItems(Set<Mail> selectedItems);
}