package com.fanzine.mail.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.fanzine.coys.R;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.databinding.ItemEmailBinding;
import com.fanzine.coys.models.mails.Label;
import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.networking.EmailDataRequestManager;
import com.fanzine.coys.viewmodels.items.ItemEmailViewModel;
import com.fanzine.mail.MailInfoActivity;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by a on 17.03.18
 */
public class EmailHolder extends RecyclerView.ViewHolder {
    private ItemEmailBinding binding;
    private Context context;

    EmailHolder(ItemEmailBinding binding) {

        super(binding.getRoot());
        this.binding = binding;
        context = binding.getRoot().getContext();
    }

    public void bind(Mail mail) {

        if (binding.getViewModel() == null)
            binding.setViewModel(new ItemEmailViewModel(context, mail));
        else
            binding.getViewModel().setMail(mail);

        if (mail.hasLabel()) {
            Label label = mail.getFirstLabel();
//                binding.emailLabel.setVisibility(View.VISIBLE);
//                binding.emailLabel.setText(label.getText());

            Drawable background = ContextCompat.getDrawable(context, R.drawable.rectangle);
            GradientDrawable gradientDrawable = (GradientDrawable) background;

            int color = Color.parseColor(label.getColor().getHex());
            gradientDrawable.setColor(color);

//                binding.emailLabel.setBackground(gradientDrawable);
        }

        binding.civAvatar.setText(mail.getFrom().substring(0, 1));

        setRead(mail.isRead());

        setFlagged(mail.isFlagged());

        setAttachment(mail.isHasAttachment());

        binding.icStar.setOnClickListener(view -> {

            setFlagged(!mail.isFlagged());
            EmailDataRequestManager.getInstanse().setMailStarred(mail.getFolder(), String.valueOf(mail.getUid()), mail.isFlagged())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            isStarred -> {
                                setFlagged(isStarred);
                            },
                            throwable -> {
                                throwable.printStackTrace();

                                FieldError errors = ErrorResponseParser.getErrors(throwable);
                                if (errors.isEmpty()) {
                                    String error = view.getContext().getResources().getString(R.string.smth_goes_wrong);
                                } else {
                                }
                            });
        });


        binding.getRoot().setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext(), MailInfoActivity.class);
            intent.putExtra(MailInfoActivity.MAIL_BODY, mail);

            view.getContext().startActivity(intent);
        });
    }

    private void setAttachment(boolean isHeavAttach) {
        if (isHeavAttach) {
            binding.icAttachment.setVisibility(View.VISIBLE);
        } else {
            binding.icAttachment.setVisibility(View.INVISIBLE);
        }

    }

    private void setRead(boolean isRead) {
        if (isRead) {
            binding.unreadMark.setVisibility(View.INVISIBLE);
            binding.from.setTypeface(binding.from.getTypeface(), Typeface.NORMAL);
            binding.tvSubject.setTypeface(binding.tvSubject.getTypeface(), Typeface.NORMAL);
        } else {
            binding.unreadMark.setVisibility(View.VISIBLE);
            binding.from.setTypeface(binding.from.getTypeface(), Typeface.BOLD);
            binding.tvSubject.setTypeface(binding.tvSubject.getTypeface(), Typeface.BOLD);
        }
    }

    private void setFlagged(boolean isFlaged) {
        int starRes;

        if (isFlaged) {
            starRes = R.drawable.mail_star;
            binding.icStar.setImageResource(starRes);
        } else {
            starRes = R.drawable.mail_star_empty;
        }
        binding.icStar.setImageResource(starRes);
    }
}
