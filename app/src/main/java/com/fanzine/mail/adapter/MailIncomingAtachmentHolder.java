package com.fanzine.mail.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.coys.models.mails.MailAttachment;
import com.squareup.picasso.Picasso;


public class MailIncomingAtachmentHolder extends RecyclerView.ViewHolder {
    private Context context;

    private ImageView iv;
    private TextView tvName, tvMimeType;
    private MailAttachment mailAttachment;

    public MailIncomingAtachmentHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();

        iv = (ImageView) itemView.findViewById(R.id.iv);
        tvName = (TextView) itemView.findViewById(R.id.tvName);
        tvMimeType = (TextView) itemView.findViewById(R.id.tvMimeType);
    }


    public void bind(MailAttachment mailAttachment) {

        this.mailAttachment = mailAttachment;
        tvName.setText(mailAttachment.name);

        String[] mimeArr = mailAttachment.mimeType.split("/");
        if (mimeArr[0].equals("image")) {
            Picasso.with(context).load(mailAttachment.filePath).into(iv);
        } else {
            tvMimeType.setText(mimeArr[1]);
        }
    }
}