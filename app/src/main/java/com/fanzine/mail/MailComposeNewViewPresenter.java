package com.fanzine.mail;

import android.widget.Toast;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.models.mails.NewMail;
import com.fanzine.coys.models.profile.Email;
import com.fanzine.coys.networking.EmailDataRequestManager;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.mail.utils.DetectHtml;
import com.fanzine.mail.utils.TimeDateUtils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.App.getContext;

class MailComposeNewViewPresenter implements MailComposeNewViewPresenterI {

    private MailComposeNewI view;
    private boolean isMailSend = false;


    @Override
    public void bindView(MailComposeNewI view) {
        this.view = view;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (!isMailSend) {
            NewMail newMail = view.getMail();

            EmailDataRequestManager.getInstanse().saveToDraft(newMail)
                    .subscribe(responseBody -> {
                                view.showUserMessage("Mail saved as a draft");
                            },
                            Throwable::printStackTrace);
        }
    }

    @Override
    public void onSendEmailClick() {

        view.setProgressBarVisability(true);

        Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {

            @Override
            public void onCompleted() {
                view.setProgressBarVisability(false);

                isMailSend = true;
            }

            @Override
            public void onError(Throwable e) {

                view.setProgressBarVisability(false);

                FieldError errors = ErrorResponseParser.getErrors(e);
                if (errors.isEmpty()) {
                    view.showError("Something goes wrong");

                } else {
                    String error = errors.getMessage();
                    view.showError(error);
                }
            }

            @Override
            public void onNext(ResponseBody body) {
                view.setProgressBarVisability(false);

                Toast.makeText(getContext(), R.string.emailSended, Toast.LENGTH_SHORT).show();
                view.finishView();

            }
        };
        NewMail newMail = view.getMail();
        EmailDataRequestManager.getInstanse().sendMail(newMail).subscribe(subscriber);
    }

    @Override
    public void setModeReply(String mailSender, String[] recipientArr, String subject, String mailContent, long mailUpdateTime) {
        StringBuilder recipientStr = new StringBuilder();

        if (recipientArr != null && recipientArr.length > 0) {

            for (String str : recipientArr) {
                recipientStr.append(str);
            }
            view.setTo(mailSender);
        }

        subject = "Re:" + subject;
        view.setSubject(subject);

        String timeDate = TimeDateUtils.convertTimestampTimeMailReplyForward(mailUpdateTime);
        String mailContentResult = "\n" + "\n" + "\n" +
                "On " + timeDate + "\n" +
                "<" + mailSender + ">" + " wrote" +
                "\n" + mailContent;

        boolean isHtml = DetectHtml.isHtml(mailContent);
        if (isHtml) {
            Document doc = Jsoup.parse(mailContentResult);
            view.setMailContent(doc.html(), "text/html");

        } else {
            view.setMailContent(mailContentResult, "text/plain");
        }
    }

    @Override
    public void setModeForward(String mailSender, String[] recipientArr, String subject, String mailContent, long mailUpdateTime) {

        subject = "Fwd:" + subject;

        StringBuilder recipientStr = new StringBuilder();

        if (recipientArr != null && recipientArr.length > 0) {

            for (String str : recipientArr) {
                recipientStr.append(str);
            }
        }

        view.setSubject(subject);

        String timeDate = TimeDateUtils.convertTimestampTimeMailReplyForward(mailUpdateTime);
        String mailContentResult =
                "---------- Forwarded message ----------\n" +
                "<" + mailSender + ">" +
                timeDate + "\n" +
                subject + "\n" +
                mailContent;

        boolean isHtml = DetectHtml.isHtml(mailContent);
        if (isHtml) {
            Document doc = Jsoup.parse(mailContentResult);
            view.setMailContent(doc.html(), "text/html");

        } else {
            view.setMailContent(mailContentResult, "text/plain");
        }

        view.setCursorToMailContentEdit();
    }

    @Override
    public void setModeNewMail() {
        setEmailSignture();
    }

    @Override
    public void setEmailSignture() {
        Subscriber<Email> subscriber = new Subscriber<Email>() {

            @Override
            public void onCompleted() {
                view.setCursorToMailContentEdit();

            }

            @Override
            public void onError(Throwable e) {

                FieldError errors = ErrorResponseParser.getErrors(e);

                if (errors.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                } else {
                    DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                }
            }

            @Override
            public void onNext(Email email) {
                String mailSignature = email.getEmailSignature();

                String mailContent = "\n \n" + mailSignature + "\n \n";

                view.attachSignuture(mailContent);
                view.setCursorToMailContentEdit();
            }
        };
        ApiRequest.getInstance().getApi().getProfileEmail()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}

