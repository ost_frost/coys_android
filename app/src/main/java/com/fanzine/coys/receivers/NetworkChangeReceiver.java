package com.fanzine.coys.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.coys.utils.NetworkUtils;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!SharedPrefs.isSent()) {
            if (NetworkUtils.isNetworkAvailable(context)) {
                Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(ResponseBody o) {
                        SharedPrefs.setIsSent();
                    }
                };

                Observable<ResponseBody> observable = ApiRequest.getInstance().getApi().sendDeviceToken(SharedPrefs.getToken());
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(subscriber);
            }
        }
    }
}