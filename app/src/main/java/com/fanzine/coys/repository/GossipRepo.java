package com.fanzine.coys.repository;

import android.content.Context;

import com.fanzine.coys.App;
import com.fanzine.coys.db.DBHelper;
import com.fanzine.coys.models.Gossip;

import java.sql.SQLException;
import java.util.List;

import rx.Observable;


public class GossipRepo {

    static private GossipRepo instance;

    static public GossipRepo getInstance() {
        if (null == instance) {
            instance = new GossipRepo(App.getContext());
        }
        return instance;
    }

    private DBHelper helper;

    private GossipRepo(Context ctx) {
        helper = new DBHelper(ctx);
    }

    private DBHelper getHelper() {
        return helper;
    }

    public Observable<Gossip> getGossip() {
        return Observable.create(subscriber -> {
            try {
                List<Gossip> data = getHelper().getGossipDao().queryForAll();

                if (!subscriber.isUnsubscribed()) {
                    if (data.size() > 0)
                        subscriber.onNext(data.get(0));
                    else
                        subscriber.onNext(null);
                    subscriber.onCompleted();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onError(ex);
                }
            }
        });
    }

    public Observable<Boolean> saveGossip(Gossip gossip) {
        return Observable.create(subscriber -> {
            try {
                getHelper().getNewsDao().callBatchTasks(() -> {
                    getHelper().clearGossip();

                    getHelper().getGossipDao().create(gossip);

                    return true;
                });
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(true);
                    subscriber.onCompleted();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onError(ex);
                }
            }
        });

    }
}
