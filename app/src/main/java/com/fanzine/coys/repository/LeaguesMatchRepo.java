package com.fanzine.coys.repository;

import android.content.Context;

import com.fanzine.coys.App;
import com.fanzine.coys.db.DBHelper;
import com.fanzine.coys.models.LigueMatches;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.models.Team;
import com.fanzine.coys.models.Venue;
import com.j256.ormlite.stmt.SelectArg;

import java.sql.SQLException;
import java.util.List;

import rx.Observable;
import rx.Subscriber;


public class LeaguesMatchRepo {

    static private LeaguesMatchRepo instance;

    static public LeaguesMatchRepo getInstance() {
        if (null == instance) {
            instance = new LeaguesMatchRepo(App.getContext());
        }
        return instance;
    }

    private DBHelper helper;

    private LeaguesMatchRepo(Context ctx) {
        helper = new DBHelper(ctx);
    }

    private DBHelper getHelper() {
        return helper;
    }


    public Observable<List<LigueMatches>> geByDate(String date) {
        return Observable.create(subscriber -> {
            try {
                List<LigueMatches> data = getHelper()
                        .getLiguesMatchesDao()
                        .queryBuilder()
                        .where()
                        .eq("date", new SelectArg(date))
                        .query();

                for (LigueMatches ligueMatches : data) {
                    List<Match> matches = getHelper().getMatchDao().
                            queryBuilder()
                            .where()
                            .eq("league_id", new SelectArg(ligueMatches.getLeagueId()))
                            .and()
                            .eq("date", new SelectArg(date))
                            .query();
                    ligueMatches.setMatches(matches);
                }

                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(data);
                    subscriber.onCompleted();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onError(ex);
                }
            }
        });
    }

    public Observable<Boolean> save(List<LigueMatches> ligueMatches, String date) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                try {
                    getHelper().getLiguesMatchesDao().callBatchTasks(() -> {

                        for (LigueMatches item : ligueMatches) {
                            item.setDate(date);


                            LigueMatches ligueMatches =
                                    getHelper().getLiguesMatchesDao().queryBuilder()
                                            .where()
                                            .eq("date", new SelectArg(date))
                                            .and()
                                            .eq("league_id", new SelectArg(item.getLeagueId()))
                                            .queryForFirst();

                            if (ligueMatches == null) {
                                getHelper().getLiguesMatchesDao().create(item);
                            } else {
                                getHelper().getLiguesMatchesDao().update(item);
                            }

                            for (Match match : item.getMatches()) {
                                match.setLeague_id(item.getLeagueId());

                                try {

                                    // http://gunners.red-carlos.com/api-almet/v1.0/matches
                                    // does not return team id and vanue id ( post_code is always 0 yet)
                                    // for avoiding duplication of teams - search it by name
                                    // for avoiding duplication of venues - search it by name


                                    Team home =
                                            getHelper().getTeamDao().queryForFirst(getHelper().getTeamDao().queryBuilder()
                                                    .where()
                                                    .eq("name", new SelectArg(match.getHomeTeam().getName()))
                                                    .prepare());
                                    Team guest = getHelper().getTeamDao().queryForFirst(getHelper().getTeamDao().queryBuilder()
                                            .where()
                                            .eq("name", new SelectArg(match.getGuestTeam().getName())).prepare());

                                    if (match.getVenue().hasName()) {
                                        Venue venue = getHelper().getVanueDao().queryForFirst(getHelper().getVanueDao().queryBuilder()
                                                .where()
                                                .eq("name", new SelectArg(match.getVenue().getName()))
                                                .prepare());

                                        if (venue == null) {
                                            getHelper().getVanueDao().create(match.getVenue());
                                        }
                                    }

                                    if (home == null) {
                                        getHelper().getTeamDao().create(match.getHomeTeam());

                                    }
                                    if (guest == null) {
                                        getHelper().getTeamDao().create(match.getGuestTeam());
                                    }

                                    getHelper().getMatchDao().createOrUpdate(match);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        return true;
                    });
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onNext(true);
                        subscriber.onCompleted();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onError(ex);
                    }
                }
            }
        });
    }
}
