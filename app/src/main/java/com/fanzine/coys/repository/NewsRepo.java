package com.fanzine.coys.repository;

import android.content.Context;

import com.fanzine.coys.App;
import com.fanzine.coys.db.DBHelper;
import com.fanzine.coys.models.News;

import java.sql.SQLException;
import java.util.List;

import rx.Observable;


public class NewsRepo {

    static private NewsRepo instance;

    static public NewsRepo getInstance() {
        if (null == instance) {
            instance = new NewsRepo(App.getContext());
        }
        return instance;
    }

    private DBHelper helper;

    private NewsRepo(Context ctx) {
        helper = new DBHelper(ctx);
    }

    private DBHelper getHelper() {
        return helper;
    }

    public Observable<List<News>> getNews(int leagueId) {
        return Observable.create(subscriber -> {
            try {
                List<News> data = getHelper().getNewsDao().getNewsByLeague(leagueId);

                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(data);
                    subscriber.onCompleted();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onError(ex);
                }
            }
        });
    }

    public Observable<Boolean> saveNews(List<News> newsList, int leagueId) {
        return Observable.create(subscriber -> {
            try {
                getHelper().getNewsDao().callBatchTasks(() -> {
                    List<News> news = getHelper().getNewsDao().getNewsByLeague(leagueId);
                    getHelper().getNewsDao().delete(news);

                    for (News item : newsList) {
                        getHelper().getNewsDao().create(item);
                    }

                    return true;
                });
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(true);
                    subscriber.onCompleted();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onError(ex);
                }
            }
        });

    }
}
