package com.fanzine.coys.repository;

import android.content.Context;

import com.fanzine.coys.App;
import com.fanzine.coys.db.DBHelper;
import com.fanzine.coys.models.League;

import java.sql.SQLException;
import java.util.List;

import rx.Observable;
import rx.Subscriber;


public class LeaguesRepo {

    static private LeaguesRepo instance;

    static public LeaguesRepo getInstance() {
        if (null == instance) {
            instance = new LeaguesRepo(App.getContext());
        }
        return instance;
    }

    private DBHelper helper;

    private LeaguesRepo(Context ctx) {
        helper = new DBHelper(ctx);
    }

    private DBHelper getHelper() {
        return helper;
    }

    public Observable<List<League>> getLeagues() {
        return Observable.create(new Observable.OnSubscribe<List<League>>() {
            @Override
            public void call(Subscriber<? super List<League>> subscriber) {
                try {
                    List<League> data = getHelper().getLeagueDao().queryForAll();

                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onNext(data);
                        subscriber.onCompleted();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onError(ex);
                    }
                }
            }
        });
    }

    public Observable<Boolean> saveLeagues(List<League> leagues) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                try {
                    getHelper().getMailDao().callBatchTasks(() -> {
                        getHelper().clearLeagues();

                        for (League article : leagues) {
                            getHelper().getLeagueDao().create(article);
                        }

                        return true;
                    });
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onNext(true);
                        subscriber.onCompleted();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onError(ex);
                    }
                }
            }
        });
    }
}
