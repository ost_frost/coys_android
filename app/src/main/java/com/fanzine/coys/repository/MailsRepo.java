package com.fanzine.coys.repository;

import android.content.Context;

import com.fanzine.coys.App;
import com.fanzine.coys.db.DBHelper;
import com.fanzine.coys.models.mails.Color;
import com.fanzine.coys.models.mails.Label;
import com.fanzine.coys.models.mails.Mail;


import java.sql.SQLException;
import java.util.List;

import rx.Observable;


public class MailsRepo {

    static private MailsRepo instance;

    static public MailsRepo getInstance() {
        if (null == instance) {
            instance = new MailsRepo(App.getContext());
        }
        return instance;
    }

    private DBHelper helper;

    private MailsRepo(Context ctx) {
        helper = new DBHelper(ctx);
    }

    private DBHelper getHelper() {
        return helper;
    }

    public Observable<List<Mail>> getMails() {
        return Observable.create(subscriber -> {
            try {
                List<Mail> data = getHelper().getMailDao().queryForAll();

                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(data);
                    subscriber.onCompleted();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onError(ex);
                }
            }
        });
    }

    public Observable<List<Mail>> getMails(String folder) {
        return Observable.create(subscriber -> {
            try {
                List<Mail> data = getMailsByFolder(folder);

                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(data);
                    subscriber.onCompleted();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onError(ex);
                }
            }
        });
    }

    public Observable<Boolean> saveMails(List<Mail> mails) {
        return Observable.create(subscriber -> {
            try {
                getHelper().getMailDao().callBatchTasks(() -> {

                    for (Mail article : mails) {
                        getHelper().getMailDao().createOrUpdate(article);
                    }
                    return true;
                });

                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(true);
                    subscriber.onCompleted();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onError(ex);
                }
            }
        });
    }


    public Observable<Boolean> saveLabels(Label label) {
        return Observable.create(subscriber -> {
            try {
                getHelper().getLabelDao().callBatchTasks(() -> {
                    getHelper().clearLabels();
                    getHelper().getLabelDao().create(label);
                    return true;
                });

                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(true);
                    subscriber.onCompleted();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onError(ex);
                }
            }
        });
    }


    public Observable<Boolean> saveColor(Color color) {
        return Observable.create(subscriber -> {
            try {

                getHelper().clearColors();
                getHelper().getLabelColorsDao().callBatchTasks(() -> {
                    getHelper().clearColors();
                    getHelper().getLabelColorsDao().create(color);
                    return  true;
                });

                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(true);
                    subscriber.onCompleted();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onError(ex);
                }
            }
        });
    }


    public Observable<Boolean> updateMails(List<Mail> mails) {
        return Observable.create(subscriber -> {
            try {
                getHelper().getMailDao().callBatchTasks(() -> {

                    for (Mail mail : mails) {
                        getHelper().getMailDao().createOrUpdate(mail);
                    }

                    return true;
                });

                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(true);
                    subscriber.onCompleted();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                ;
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onError(ex);
                }
            }
        });
    }

    public Observable<Boolean> deleteMail(String folderFrom, int mailId) {
        return Observable.create(subscriber -> {
            try {
                getHelper().getMailDao().deleteMail(folderFrom, mailId);

                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(true);
                    subscriber.onCompleted();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                ;
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onError(ex);
                }
            }
        });
    }

    private List<Mail> getMailsByFolder(String folder) throws SQLException {
        List<Mail> data;

        if (folder == null) {
            folder = "INBOX";
        }

        if (folder.toLowerCase().equals("flagged"))
            data = getHelper().getMailDao().getMailsFlagged();
        else if (folder.toLowerCase().equals("unread"))
            data = getHelper().getMailDao().getMailsUnread();
        else
            data = getHelper().getMailDao().getMailsFromFolder(folder);

        return data;
    }
}
