package com.fanzine.coys.repository;

import android.content.Context;

import com.fanzine.coys.App;
import com.fanzine.coys.db.DBHelper;
import com.fanzine.coys.models.Match;

import java.sql.SQLException;
import java.util.List;

import rx.Observable;
import rx.Subscriber;


public class MatchRepo {

    static private MatchRepo instance;

    static public MatchRepo getInstance() {
        if (null == instance) {
            instance = new MatchRepo(App.getContext());
        }
        return instance;
    }

    private DBHelper helper;

    private MatchRepo(Context ctx) {
        helper = new DBHelper(ctx);
    }

    private DBHelper getHelper() {
        return helper;
    }

    public Observable<List<Match>> getMatches() {
        return Observable.create(new Observable.OnSubscribe<List<Match>>() {
            @Override
            public void call(Subscriber<? super List<Match>> subscriber) {
                try {
                    List<Match> data = getHelper().getMatchDao().queryForAll();

                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onNext(data);
                        subscriber.onCompleted();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onError(ex);
                    }
                }
            }
        });
    }

    public Observable<List<Match>> getMatchesByDate(String date) {
        return Observable.create(subscriber -> {
            try {
                List<Match> data = getHelper()
                        .getMatchDao()
                        .queryBuilder()
                        .where()
                        .eq("date", date).query();

                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(data);
                    subscriber.onCompleted();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onError(ex);
                }
            }
        });
    }

    public Observable<Boolean> saveMatches(List<Match> matches) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                try {
                    getHelper().getMatchDao().callBatchTasks(() -> {

                        for (Match match : matches) {
                            getHelper().getMatchDao().createOrUpdate(match);
                        }
                        return true;
                    });
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onNext(true);
                        subscriber.onCompleted();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onError(ex);
                    }
                }
            }
        });
    }
}
