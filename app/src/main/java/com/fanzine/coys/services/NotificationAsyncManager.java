package com.fanzine.coys.services;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;

import com.fanzine.coys.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

/**
 * Created by mbp on 4/30/18.
 */

public class NotificationAsyncManager  extends AsyncTask<String, Void, Bitmap> {

    private Context context;
    private NotificationModel model;

    public NotificationAsyncManager(Context context, NotificationModel model) {
        this.context = context;
        this.model = model;
    }

    @Override
    protected Bitmap doInBackground(String... strings) {
        InputStream in;
        try {
            URL url = new URL(model.getIconUrl());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            in = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(in);
            return myBitmap;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap icon) {
        super.onPostExecute(icon);
        show(icon);
    }

    private void show(Bitmap largeIcon) {

        model.getIntent().setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntentWithParentStack(model.getIntent());

        PendingIntent contentIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);



        Uri soundUri;

        if  ( model.getSound() == null) {
            soundUri = getDefaultRingtone();
        } else {
            soundUri = getGunnersRingtone();
        }

         NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setContentTitle(model.getTitle())
                .setContentText(model.getDescription())
                .setContentIntent(contentIntent)
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_notification)
                .setLargeIcon(largeIcon)
                .setSound(soundUri)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        int uniqueNotifId = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(uniqueNotifId, mBuilder.build());
    }

    private Uri getDefaultRingtone() {
       return RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    }

    private Uri getGunnersRingtone(){
        int soundResId  = context.getResources().getIdentifier(model.getSound(), "raw",
                context.getPackageName());

       return Uri.parse("android.resource://"
                + context.getPackageName() + "/" + soundResId);

    }
}
