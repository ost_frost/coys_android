package com.fanzine.coys.services;

/**
 * Created by mbp on 4/30/18.
 */

public interface SoundTypes {

    String BREAKING_NEWS = "breacking_news";
    String CHAT_NEW = "chat_new";
    String EMAIL_NEW = "email_new";
    String FULL_TIME = "full_time";
    String GOALS = "goals";
    String HALF_TIME = "half_time";
    String KICL_OF = "kick_of";
    String LINE_UP = "line_up";
    String PENALTY = "penalty";
    String RED_CARDS = "red_cards";
}
