package com.fanzine.coys.services;

import android.content.Intent;
import android.os.Bundle;

import com.fanzine.coys.R;

import com.fanzine.coys.activities.MainActivity;
import com.fanzine.coys.fragments.match.MatchFragment;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.models.Team;
import com.fanzine.coys.models.Venue;
import com.fanzine.mail.view.activity.folder.ListEmailActivity;
import com.fanzine.chat3.view.activity.messages.MessagesListActivity;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Woland on 15.12.2016.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final int CHAT = 25;
    public static final int MATCH = 347;
    public static final int EMAIL = 1;
    public static final int OTHER = 2;

    public interface Types {
        String CHAT_NEW_MESSAGE = "chat_message_";
        String EMAIL_NEW_MESSAGE = "email_message_";
        String MATCH_EVENT = "match_";
        String TEAM_EVENT = "team_";

        String MATCH_END_TEAMS = "user_";

        String NEWS = "breaking_news";
    }

    public interface MessageType {
        String TEXT = "text";
        String USER_NAME = "user_name";
        String TIME = "created_at";
        String RELATION_TYPE = "type";
        String CHAT_ID = "channel_id";
        String USER_ID = "user_id";
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
    }

    @Override
    public void handleIntent(Intent intent) {
        if (intent.getExtras() == null)
            return;

        String topic = intent.getExtras().getString("from");
        Map<String, String> data = new HashMap<>();

        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            for (String key : bundle.keySet()) {
                Object value = bundle.get(key);
                data.put(key, String.valueOf(value));
            }
        }
        handle(topic, data);
    }

    private void handle(String topic, Map<String, String> data) {
        String title = data.get("gcm.notification.title");
        String description = data.get("gcm.notification.body");
        String icon = data.get("gcm.notification.icon");

        NotificationModel message = new NotificationModel(title, description);
        if (icon != null)
            message.setIconUrl(icon);


        if (topic.contains(Types.CHAT_NEW_MESSAGE)) {
            message.setIntent(buildChatIntent(data));
            message.setSound(SoundTypes.CHAT_NEW);
        } else if (topic.contains(Types.EMAIL_NEW_MESSAGE)) {
            message.setIntent(buildMailIntent(data));
            message.setSound(SoundTypes.EMAIL_NEW);
        } else if (topic.contains(Types.MATCH_END_TEAMS)) {
            message.setSound(data.get("gcm.notification.sound2"));
            message.setIntent(buildMatchIntent(data));
        } else if (topic.contains(Types.NEWS)) {
            message.setIntent(buildNewsIntent(data));
            message.setTitle(getString(R.string.notification_title_breaking_news));
            message.setSound(SoundTypes.BREAKING_NEWS);
        }

        if (message.getIntent() != null)
            showNotification(message);
    }


    private Intent buildChatIntent(Map<String, String> data) {
        Long channelId = Long.valueOf(data.get(MessageType.CHAT_ID));

        Intent intent = new Intent(this, MessagesListActivity.class);
        intent.putExtra(MessagesListActivity.CHANNEL_ID, channelId);
        intent.setAction(MessagesListActivity.ACTION_NOTIFICATION);

        return intent;
    }

    private Intent buildMailIntent(Map<String, String> data) {
        Intent startMailListActivity = new Intent(this, ListEmailActivity.class);
        startMailListActivity.putExtra(ListEmailActivity.FOLDER, ListEmailActivity.DEFAULT_FOLDER);

        return startMailListActivity;
    }

    private Intent buildNewsIntent(Map<String, String> data) {
        Intent startMainActivity = new Intent(this, MainActivity.class);

        return startMainActivity;
    }

    private Intent buildMatchIntent(Map<String, String> data) {
        Gson gson = new Gson();
        Intent matchActivityIntent = new Intent(this, MatchFragment.class);

        try {
            Team homeTeam = gson.fromJson(data.get("home_team"), Team.class);
            Team guestTeam = gson.fromJson(data.get("guest_team"), Team.class);
            Venue venue = gson.fromJson(data.get("venue"), Venue.class);

            Match match = new Match();
            match.setDate(data.get("date"));
            match.setState(Integer.valueOf(data.get("state")));
            match.setTimeBegin(data.get("time_begin"));
            match.setTotal(data.get("total"));
            match.setId(Integer.valueOf(data.get("id")));
            match.setCurrentTimeMatch(data.get("current_time_match"));
            match.setVenue(venue);

            match.setHomeTeam(homeTeam);
            match.setGuestTeam(guestTeam);

            matchActivityIntent.putExtra(Match.MATCH, match);
        } catch (Exception e) {
            FirebaseCrash.report(e);
        }
        return matchActivityIntent;
    }

    private void showNotification(NotificationModel notification) {
        new NotificationAsyncManager(this, notification).execute();
    }
}
