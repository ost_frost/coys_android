package com.fanzine.coys.services;

import android.content.Intent;

/**
 * Created by mbp on 4/30/18.
 */

public class NotificationModel {

    private String title;
    private String description;
    private Intent intent;
    private int id;
    private String iconUrl;
    private String sound;

    public NotificationModel(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public String getTitle() {
        return title;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getDescription() {
        return description;
    }

    public Intent getIntent() {
        return intent;
    }

    public int getId() {
        return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }

    public void setId(int id) {
        this.id = id;
    }
}
