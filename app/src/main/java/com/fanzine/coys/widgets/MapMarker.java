package com.fanzine.coys.widgets;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fanzine.coys.R;

/**
 * Created by mbp on 3/27/18.
 */

public class MapMarker extends RelativeLayout {

    private View rootView;
    private TextView tvNumber;

    private String number;

    public MapMarker(Context context, String number, int iconRes, int styleId) {
        super(context);

        rootView = LayoutInflater.from(getContext()).inflate(R.layout.map_pin, this);
        tvNumber = (TextView) rootView.findViewById(R.id.text);

        tvNumber.setBackgroundResource(iconRes);
        tvNumber.setText(number);
        tvNumber.setTextAppearance(getContext(), styleId);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode() || rootView != null) {
            addView(rootView);
        }
    }
}
