package com.fanzine.coys.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.fanzine.coys.R;


/**
 * Created by koresuniku on 2/23/18
 */

public class RecyclerViewLineItemDecoration extends RecyclerView.ItemDecoration {

    private Drawable mDivider;
    private int mDivideAtPosition = -1;

    public RecyclerViewLineItemDecoration(Context context) {
        mDivider = context.getResources().getDrawable(R.drawable.divider_1dp);
    }

    public RecyclerViewLineItemDecoration(Context context, int divideAtPosition) {
        mDivider = context.getResources().getDrawable(R.drawable.divider_1dp);
        mDivideAtPosition = divideAtPosition;
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft();
        int right = (parent.getWidth() - parent.getPaddingRight());

        int childCount = mDivideAtPosition == -1 ? parent.getChildCount() : mDivideAtPosition + 1;
        for (int i = mDivideAtPosition == -1 ? 0 : mDivideAtPosition; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        if (parent.getChildAdapterPosition(view) == 0) return;

        outRect.top = mDivider.getIntrinsicHeight();
    }
}