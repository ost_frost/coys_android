package com.fanzine.coys.widgets;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.fanzine.coys.activities.MainActivity;
import com.fanzine.coys.activities.base.NavigationFragmentActivity;
import com.fanzine.coys.databinding.WidgetBottomNavigationBinding;
import com.fanzine.chat3.view.activity.messages.MessagesListActivity;
import com.fanzine.mail.view.activity.folder.ListEmailActivity;
import com.jakewharton.rxbinding.view.RxView;

/**
 * Created by Woland on 10.01.2017.
 */

public class BottomNavigation extends LinearLayout {

    private WidgetBottomNavigationBinding binding;

    public BottomNavigation(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context);
    }

    private void init(Context context) {
        try {
            binding = WidgetBottomNavigationBinding.inflate(LayoutInflater.from(context), this, false);
        } catch (Exception ignored) {

        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode() || binding != null) {
            addView(binding.getRoot());

            RxView.clicks(binding.football).subscribe(aVoid -> {
                if (getContext() instanceof MainActivity) {
                    ((MainActivity) getContext()).openDrawer();
                } else if (getContext() instanceof NavigationFragmentActivity) {

                    ((NavigationFragmentActivity) getContext()).openDrawer();
                } else if (getContext() instanceof ListEmailActivity) {

                    //((ListEmailActivity) getContext()).openDrawer();
                }
            });

            RxView.clicks(binding.settings).subscribe(aVoid -> {
                if (getContext() instanceof MainActivity)
                    ((MainActivity) getContext()).openProfile();
                else {
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    intent.setAction(MainActivity.BOTTOM_MENU_OPEN_SETTINGS);

                    getContext().startActivity(intent);
                }

            });

            RxView.clicks(binding.venues).subscribe(aVoid -> {
                if (getContext() instanceof MainActivity)
                    ((MainActivity) getContext()).openVenues();
                else {
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    intent.setAction(MainActivity.BOTTOM_MENU_OPEN_VENUES);

                    getContext().startActivity(intent);
                }
            });

            RxView.clicks(binding.chat).subscribe(aVoid -> {
                if (getContext() instanceof MainActivity) {
                    ((MainActivity) getContext()).openChat();
                } else if (getContext() instanceof MessagesListActivity) {
                    ((MessagesListActivity) getContext()).onBackPressed();
                } else {
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    intent.setAction(MainActivity.BOTTOM_MENU_OPEN_CHAT);

                    getContext().startActivity(intent);
                }
            });

            RxView.clicks(binding.messages).subscribe(aVoid -> {
                if (getContext() instanceof MainActivity)
                    ((MainActivity) getContext()).openEmails();
                else {
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    intent.setAction(MainActivity.BOTTOM_MENU_OPEN_EMAIL);

                    getContext().startActivity(intent);
                }
            });
        }
    }
}
