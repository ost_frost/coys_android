package com.fanzine.coys.widgets;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by mbp on 3/8/18.
 */

public class CarouselView extends com.gtomato.android.ui.widget.CarouselView {

    public CarouselView(Context context) {
        super(context);
    }

    public CarouselView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CarouselView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public com.gtomato.android.ui.widget.CarouselView setOnItemSelectedListener(OnItemSelectedListener onItemSelectedListener) {

        return this;
    }
}
