package com.fanzine.coys.widgets;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;

/**
 * Created by Woland on 07.03.2017.
 */

public class MapInfoLayout extends LinearLayout {

    public MapInfoLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setVisibility(GONE);
    }

    public void expand() {
        if (getVisibility() == GONE) {
            setVisibility(VISIBLE);
            measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
            int targetHeight = getMeasuredHeight();
            ValueAnimator valueAnimator = ValueAnimator.ofInt(0, targetHeight);
            valueAnimator.addUpdateListener(animation -> {
                getLayoutParams().height = (int) animation.getAnimatedValue();
                requestLayout();
            });
            valueAnimator.setInterpolator(new DecelerateInterpolator());
            valueAnimator.setDuration(250);
            valueAnimator.start();
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    clearAnimation();
                }
            });
        }
    }

    public void collapse() {
        if (getVisibility() == VISIBLE) {
            measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
            int prevHeight = getMeasuredHeight();
            ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, 0);
            valueAnimator.setInterpolator(new DecelerateInterpolator());
            valueAnimator.addUpdateListener(animation -> {
                getLayoutParams().height = (int) animation.getAnimatedValue();
                requestLayout();
            });
            valueAnimator.setInterpolator(new DecelerateInterpolator());
            valueAnimator.setDuration(250);
            valueAnimator.start();
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    clearAnimation();
                    setVisibility(GONE);
                }
            });
        }
    }

}
