package com.fanzine.coys.widgets;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.venues.FixturesAdapter;
import com.fanzine.coys.databinding.WidgetUpcomingFixturesBinding;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.viewmodels.UpcomingFixturesViewModel;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by mbp on 2/5/18.
 */

public class UpcomingFixtures extends LinearLayout implements
        UpcomingFixturesViewModel.OnFixturesLoadingListener,
        FixturesAdapter.OnMatchClickListener {

    private final static int FIXTURES_LIMIT = 10;

    private WidgetUpcomingFixturesBinding binding;
    private UpcomingFixturesViewModel viewModel;

    private FixturesAdapter fixturesAdapter;

    private Match mSelectedcMatch;
    private LatLng mMatchVenueLatLng;
    private String mAddress;

    private FixturesAdapter.OnMatchClickListener mOnMatchClickListener;

    public UpcomingFixtures(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init(context);
    }

    public WidgetUpcomingFixturesBinding getBinding() {
        return binding;
    }

    private void init(Context context) {
        try {
            viewModel = new UpcomingFixturesViewModel(context, this);

            binding = WidgetUpcomingFixturesBinding.inflate(LayoutInflater.from(context), this, false);
            binding.setViewModel(viewModel);

            fixturesAdapter = new FixturesAdapter(context, this);
        } catch (Exception ignored) {

        }
    }

    public void clear() {
        binding.defaultLocationHeaderText.setText("");
        mSelectedcMatch = null;
    }

    public boolean isLocationEmpty() {
        return binding.defaultLocationHeaderText.getText().length() == 0;
    }


    public void setTitle(String title) {
        binding.defaultLocationHeaderText.setText(title);
    }

    public void hideCurrentLocationHeader() {
        binding.tvCurrentLocationFixtures.setVisibility(INVISIBLE);
    }

    public void attachOnMatchClickListener(FixturesAdapter.OnMatchClickListener onMatchClickListener) {
        mOnMatchClickListener = onMatchClickListener;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode() || binding != null) {
            addView(binding.getRoot());

            binding.rvMatches.setLayoutManager(new LinearLayoutManager(getContext()));
            binding.rvMatches.setAdapter(fixturesAdapter);

            binding.openFixturesBtn.setOnClickListener(view -> {
                showFixtures();
            });

            binding.defaultLocationHeaderText.setOnClickListener(view -> {
                showFixtures();
            });

            binding.showSelectedLocationBtn.setOnClickListener(view -> {
                hideFixtures();
            });

            setOnClickListenerOnCurrentLocation();

            viewModel.loadFixtures(FIXTURES_LIMIT);
        }
    }

    @Override
    public void onLoaded(List<Match> matches) {
        fixturesAdapter.addAll(matches);
    }

    @Override
    public void onMatchClicked(Match match) {
        mSelectedcMatch = match;

        hideFixtures();
        showSelectedLocation(match);

        if (mOnMatchClickListener != null)
            mOnMatchClickListener.onMatchClicked(match);
    }

    @Override
    public void onAddressReady(com.google.android.gms.location.places.Place place) {
        mAddress = place.getAddress().toString();
        mMatchVenueLatLng = new LatLng(place.getViewport().getCenter().latitude, place.getViewport().getCenter().longitude);

        binding.defaultLocationHeaderText.setText(mAddress);
    }

    @Override
    public void onAddressError() {

    }

    public boolean isSearchByMatch() {
        return mSelectedcMatch != null;
    }

    public LatLng getLatLng() {
        return mMatchVenueLatLng;
    }

    public String getAddress() {
        return mAddress;
    }

    public void showFixtures() {
        binding.defaultLocationHeader.setVisibility(View.GONE);
        binding.llFixturesLayout.setVisibility(View.VISIBLE);
    }

    public void hideFixtures() {
        binding.defaultLocationHeader.setVisibility(View.VISIBLE);
        binding.llFixturesLayout.setVisibility(View.GONE);

    }

    private void showSelectedLocation(Match match) {
        String query = match.getVenue().getName() + "," + match.getVenue().getPostalCode();
        viewModel.requestGeoCode(query);

        hideFixtures();
    }


    private void setOnClickListenerOnCurrentLocation() {
        binding.tvCurrentLocationFixtures.setOnClickListener(view -> {
            mSelectedcMatch = null;

            binding.defaultLocationHeaderText.setText(getContext().getString(R.string.venue_search_current_location));

            hideFixtures();
        });
    }

    public void slideUp(View view) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    public void slideDown(View view) {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }
}
