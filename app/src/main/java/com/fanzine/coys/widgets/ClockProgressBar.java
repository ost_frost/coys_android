package com.fanzine.coys.widgets;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.fanzine.coys.R;
import com.google.firebase.crash.FirebaseCrash;

/**
 * Created by mbp on 3/7/18.
 */

public class ClockProgressBar extends android.support.v7.widget.AppCompatImageView {

    private AnimationDrawable animationDrawable;

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (animationDrawable != null) animationDrawable.stop();
    }

    public ClockProgressBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        try {
            if (!isInEditMode()) {
                setBackground(ContextCompat.getDrawable(getContext(), R.drawable.clock_progress));

                animationDrawable = (AnimationDrawable) getBackground();
                animationDrawable.start();
            }
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            FirebaseCrash.report(e);
        }
    }
}
