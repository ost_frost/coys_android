package com.fanzine.coys.widgets;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;

import com.fanzine.coys.R;

/**
 * Created by Woland on 01.03.2017.
 */

public class GestureLinearLayout extends LinearLayout {

    private final GestureDetector gestureDetector;

    public GestureLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        gestureDetector = new GestureDetector(context, new MyGestureListener());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (gestureDetector.onTouchEvent(event)) return true;
        return true;
    }

    private static final int SWIPE_MIN_DISTANCE = 50;
    private static final int SWIPE_THRESHOLD_VELOCITY = 150;

    private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                               float velocityY) {
            if (e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY){
                hide();
                return true;
            }
            return false;
        }
    }

    public void hide() {
        LinearLayout linearLayout = (LinearLayout) getParent();
        int colorFrom = Color.parseColor("#00000000");
        int colorTo = Color.parseColor("#af000000");
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorTo, colorFrom);
        colorAnimation.setDuration(250); // milliseconds
        colorAnimation.addUpdateListener(animator -> linearLayout.setBackgroundColor((int) animator.getAnimatedValue()));
        colorAnimation.start();

        View view = linearLayout.findViewById(R.id.gestureLayout);

        view.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);

        collapse(linearLayout, view, 250);
    }

    public void collapse(LinearLayout linearLayout, final View v, int duration) {
        int prevHeight = v.getMeasuredHeight();
        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, 0);
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.addUpdateListener(animation -> {
            v.getLayoutParams().height = (int) animation.getAnimatedValue();
            v.requestLayout();
        });
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.setDuration(duration);
        valueAnimator.start();
        valueAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                v.clearAnimation();
                linearLayout.setVisibility(GONE);
            }
        });
    }

}
