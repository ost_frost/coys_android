package com.fanzine.coys.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.ViewStatsBinding;

/**
 * Created by Siarhei on 18.01.2017.
 */

public class StatsView extends FrameLayout {
    private int homeValue;
    private int guestValue;
    private String titleValue;
    private ViewStatsBinding binding;

    public StatsView(Context context) {
        super(context);
    }

    public StatsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public StatsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public StatsView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    public void init(Context context, AttributeSet attrs) {

        binding = ViewStatsBinding.inflate(LayoutInflater.from(context), this, false);
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.StatsView);
            final int styledAttributesCount = typedArray.getIndexCount();
            for (int i = 0; i < styledAttributesCount; ++i) {
                int attr = typedArray.getIndex(i);
                if (attr == R.styleable.StatsView_titleValue) {
                    titleValue = typedArray.getString(attr);
                }
                if (attr == R.styleable.StatsView_homeValue) {
                    homeValue = typedArray.getInteger(attr, 0);
                }
                if (attr == R.styleable.StatsView_guestValue) {
                    guestValue = typedArray.getInteger(attr, 0);
                }
            }
            typedArray.recycle();
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        addView(binding.getRoot());

    }

    String titleWithPersent = "Passing Accuracy";

    public void setTitleValue(String titleValue) {
        this.titleValue = titleValue;
    }

    public void setHomeValue(int homeValue) {
        if (titleValue.toLowerCase().equals(titleWithPersent.toLowerCase())) {
            binding.homeViewText.setText(String.valueOf(homeValue) + "%");
        } else {
            binding.homeViewText.setText(String.valueOf(homeValue));
        }
        this.homeValue = homeValue;
    }

    public void setGuestValue(int guestValue) {
        if (titleValue.toLowerCase().equals(titleWithPersent.toLowerCase())) {
            binding.guestView.setText(String.valueOf(guestValue) + "%");
        } else {
            binding.guestView.setText(String.valueOf(guestValue));
        }
        this.guestValue = guestValue;
        //binding.guestViewLay.setLayoutParams(new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, guestValue));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (guestValue == 0 && homeValue == 0) {

        } else {

           if (homeValue == 0 ) {
               binding.bgView.setBackgroundResource(R.color.stats_red);
           }

            {
                LinearLayout.LayoutParams loparams = (LinearLayout.LayoutParams) binding.homeView.getLayoutParams();
                loparams.weight = homeValue;
                binding.homeView.setLayoutParams(loparams);
            }

            {
                LinearLayout.LayoutParams loparamsn = (LinearLayout.LayoutParams) binding.guestViewLay.getLayoutParams();
                loparamsn.weight = guestValue;
                binding.guestViewLay.setLayoutParams(loparamsn);
            }

        }
    }
}
  