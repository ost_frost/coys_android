package com.fanzine.coys.widgets;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;

/**
 * Created by mbp on 3/5/18.
 */

public class FloatCircle extends FloatingActionButton {

    public FloatCircle(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public FloatCircle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public FloatCircle(Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);


    }
}
