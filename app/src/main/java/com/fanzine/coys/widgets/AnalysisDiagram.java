package com.fanzine.coys.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.ViewAnalysisDiagramBinding;

/**
 * Created by mbp on 11/2/17.
 */

public class AnalysisDiagram extends RelativeLayout {

    private String percentage;
    private String total;
    private boolean isVoted;

    public AnalysisDiagram(Context context) {
        super(context);
    }

    public AnalysisDiagram(Context context, AttributeSet attrs) {
        super(context, attrs);

        ViewAnalysisDiagramBinding binding =
                ViewAnalysisDiagramBinding.inflate(LayoutInflater.from(context), this, false);

        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.AnalysisDiagramView);
            final int styledAttributesCount = typedArray.getIndexCount();
            for (int i = 0; i < styledAttributesCount; ++i) {
                int attr = typedArray.getIndex(i);
                if (attr == R.styleable.AnalysisDiagramView_percentage) {
                    percentage = typedArray.getString(attr);
                }
                if (attr == R.styleable.AnalysisDiagramView_total) {
                    total = typedArray.getString(attr);
                }
                if (attr == R.styleable.AnalysisDiagramView_isVoted) {
                    isVoted = typedArray.getBoolean(attr, false);
                }
            }
            typedArray.recycle();
        }

        binding.procent.setText(percentage);
        binding.total.setText(total);

        if (isVoted) {
            binding.votedLabel.setVisibility(VISIBLE);
        }
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public boolean isVoted() {
        return isVoted;
    }

    public void setIsVoted(boolean voted) {
        isVoted = voted;
    }
}
