package com.fanzine.coys.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.fanzine.coys.utils.FontCache;

/**
 * Created by mbp on 10/20/17.
 */

public class SanFranciscoText extends android.support.v7.widget.AppCompatTextView {

    public SanFranciscoText(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public SanFranciscoText(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public SanFranciscoText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("SanFranciscoText-Regular.otf", context);
        setTypeface(customFont);
    }
}
