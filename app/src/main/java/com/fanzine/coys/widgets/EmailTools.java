package com.fanzine.coys.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.WidgetEmailToolsBinding;

/**
 * Created by Woland on 10.01.2017.
 */

public class EmailTools extends LinearLayout {

    private WidgetEmailToolsBinding binding;

    public EmailTools(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context);
    }

    private void init(Context context) {
        binding = WidgetEmailToolsBinding.inflate(LayoutInflater.from(context), this, false);
    }

    public void setClickListener(OnClickListener clickListener) {
        binding.btnFlag.setOnClickListener(clickListener);
        binding.btnChangeFolder.setOnClickListener(clickListener);
        binding.btnDelete.setOnClickListener(clickListener);
        binding.btnReply.setOnClickListener(clickListener);
        binding.btnAddLabel.setOnClickListener(clickListener);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode() || binding != null) {
            addView(binding.getRoot());
        }
    }

    public void setFlag(boolean isFlagged) {
        if (isFlagged)
            binding.btnFlagImage.setImageResource(R.drawable.activeflag);
        else
            binding.btnFlagImage.setImageResource(R.drawable.ic_flag);
    }
}
