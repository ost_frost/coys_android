package com.fanzine.coys.exceptions;

/**
 * Created by mbp on 1/26/18.
 */

public class LocationIsNull extends Exception {

    private final static String MESSAGE = "Last known location is null";

    public LocationIsNull() {
        super(MESSAGE);
    }
}
