package com.fanzine.coys.exceptions;

/**
 * Created by mbp on 1/29/18.
 */

public class GPSProviderIsDisabled extends Exception {
    private final static String MESSAGE = "GPS provider for detecting user location is disabled";

    public GPSProviderIsDisabled() {
    }
}
