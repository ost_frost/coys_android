package com.fanzine.coys.exceptions;

/**
 * Created by mbp on 1/29/18.
 */

public class NetworkLocationIsDisabled extends Exception {
    private final static String MESSAGE = "Network provider for detecting user location is disabled";

    public NetworkLocationIsDisabled() {
        super(MESSAGE);
    }
}
