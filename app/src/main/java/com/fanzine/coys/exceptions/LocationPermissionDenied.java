package com.fanzine.coys.exceptions;

/**
 * Created by mbp on 1/31/18.
 */

public class LocationPermissionDenied extends Exception {

    private final static String MESSAGE = "Permission ACCESS_FINE_LOCATION and ACCESS_COARSE_LOCATION is not provided";

    public LocationPermissionDenied() {
        super(MESSAGE);
    }
}
