package com.fanzine.coys.exceptions;

/**
 * Created by mbp on 2/17/18.
 */

public class VerificationCodeException extends Throwable {

    public VerificationCodeException(String message) {
        super(message);
    }
}
