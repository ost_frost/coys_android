package com.fanzine.coys.sprefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.fanzine.coys.App;
import com.fanzine.coys.models.login.Credentials;
import com.fanzine.coys.models.login.UserToken;
import com.google.gson.Gson;

/**
 * Created by Woland on 17.02.2017.
 */
public class SharedPrefs {

    private static final String NAME = "SharedPrefs";

    private static final String USER = "USER";
    private static final String CREDENTIALS = "credentials";
    private static final String USER_TOKEN = "USER_TOKEN";
    private static final String FIR_TOKEN = "FIR_TOKEN";
    private static final String IS_SENT = "is_SENT";
    private static final String EMAIL_SIGNUTURE = "email_signature";


    public static void saveUserToken(UserToken userToken) {
        SharedPreferences preferences = App.getContext().getSharedPreferences(NAME, Context.MODE_PRIVATE);
        if (userToken != null)
            preferences.edit().putString(USER, new Gson().toJson(userToken)).apply();
        else
            preferences.edit().putString(USER, null).apply();
    }

    public static void saveUserCredentials(Credentials credentials) {
        SharedPreferences preferences = App.getContext().getSharedPreferences(NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(CREDENTIALS, new Gson().toJson(credentials)).apply();
    }

    public static void flushCredentials() {
        SharedPreferences preferences = App.getContext().getSharedPreferences(NAME, Context.MODE_PRIVATE);
        preferences.edit().remove(CREDENTIALS).apply();
    }

    public static Credentials getUserCredentials() {
        SharedPreferences preferences = App.getContext().getSharedPreferences(NAME, Context.MODE_PRIVATE);
        String json = preferences.getString(CREDENTIALS, null);
        if (!TextUtils.isEmpty(json))
            return new Gson().fromJson(json, Credentials.class);
        else
            return null;
    }

    public static void saveFIRToken(String firToken) {
        SharedPreferences preferences = App.getContext().getSharedPreferences(NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(FIR_TOKEN, firToken).apply();
    }

    public static UserToken getUserToken() {
        SharedPreferences preferences = App.getContext().getSharedPreferences(NAME, Context.MODE_PRIVATE);
        String json = preferences.getString(USER, null);
        if (!TextUtils.isEmpty(json))
            return new Gson().fromJson(json, UserToken.class);
        else
            return null;
    }

    public static String getFIRToken() {
        SharedPreferences preferences = App.getContext().getSharedPreferences(NAME, Context.MODE_PRIVATE);
        return preferences.getString(FIR_TOKEN, "");
    }

    public static String getToken() {
        SharedPreferences preferences = App.getContext().getSharedPreferences(NAME, Context.MODE_PRIVATE);
        return preferences.getString(USER_TOKEN, null);
    }

    public static void setToken(String token) {
        SharedPreferences preferences = App.getContext().getSharedPreferences(NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(USER_TOKEN, token).putBoolean(IS_SENT, false).apply();
    }

    public static boolean isSent() {
        SharedPreferences preferences = App.getContext().getSharedPreferences(NAME, Context.MODE_PRIVATE);
        return TextUtils.isEmpty(preferences.getString(USER_TOKEN, null)) || preferences.getBoolean(IS_SENT, false);
    }

    public static void setIsSent() {
        SharedPreferences preferences = App.getContext().getSharedPreferences(NAME, Context.MODE_PRIVATE);
        preferences.edit().putBoolean(IS_SENT, true).apply();
    }

    public static void setEmailSignuture(String signuture) {
        SharedPreferences preferences = App.getContext().getSharedPreferences(EMAIL_SIGNUTURE, Context.MODE_PRIVATE);
        preferences.edit().putString(EMAIL_SIGNUTURE, signuture).apply();
    }


    public static String getEmailSignuture() {
        SharedPreferences preferences = App.getContext().getSharedPreferences(EMAIL_SIGNUTURE, Context.MODE_PRIVATE);
        return preferences.getString(EMAIL_SIGNUTURE, "");
    }
}
