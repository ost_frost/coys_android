package com.fanzine.coys.interfaces;

import java.io.Serializable;

/**
 * Created by mbp on 3/21/18.
 */

public interface MatchNotificationListener extends Serializable {
    void turnOn();

    void turnOff();
}
