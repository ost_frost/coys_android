package com.fanzine.coys.interfaces;

import com.fanzine.coys.models.matches.MatchNotification;

import java.io.Serializable;

/**
 * Created by mbp on 3/21/18.
 */

public interface LeagueNotificationListener extends Serializable {
    void save(MatchNotification matchNotification);
}
