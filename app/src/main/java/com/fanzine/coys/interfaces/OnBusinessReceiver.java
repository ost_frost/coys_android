package com.fanzine.coys.interfaces;

import com.fanzine.coys.fragments.venue.OnFragmentChangeListener;
import com.google.android.gms.maps.model.LatLng;
import com.yelp.fusion.client.models.Business;

import java.util.List;

/**
 * Created by Woland on 07.03.2017.
 */

public interface OnBusinessReceiver {
    void init(List<Business> businesses, LatLng currentLocation, OnFragmentChangeListener onFragmentChangeListener);
}
