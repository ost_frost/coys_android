package com.fanzine.coys.interfaces;

/**
 * Created by Woland on 19.01.2017.
 */

public interface LoadingListener<T> {

    void onLoaded(T data);
    void onError();

}
