package com.fanzine.coys.interfaces;

import com.fanzine.coys.models.profile.Profile;

public interface OnProfileLoader {
    void onProfileLoaded(Profile profile);
}