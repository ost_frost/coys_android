package com.fanzine.coys.interfaces;

import com.fanzine.coys.models.login.Country;

import java.util.List;

/**
 * Created by mbp on 3/21/18.
 */

public interface CountriesConsumer {
    void accept(List<Country> countryList);
}
