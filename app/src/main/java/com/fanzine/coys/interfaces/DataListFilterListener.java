package com.fanzine.coys.interfaces;

import java.util.List;

/**
 * Created by maximdrobonoh on 03.10.17.
 */

public interface DataListFilterListener<T> {

    void onFilterLoaded(List<T> data);

    void onFilterLoaded(T data);

    void onFilterError();
}
