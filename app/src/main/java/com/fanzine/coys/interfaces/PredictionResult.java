package com.fanzine.coys.interfaces;

import com.fanzine.coys.models.analysis.Prediction;

/**
 * Created by maximdrobonoh on 10.10.17.
 */

public interface PredictionResult {

    void showDiagram(Prediction prediction);
    void showVotedLabel(Prediction prediction);
}
