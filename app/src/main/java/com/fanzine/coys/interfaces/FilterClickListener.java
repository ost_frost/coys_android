package com.fanzine.coys.interfaces;

import com.fanzine.coys.models.table.Filter;

/**
 * Created by maximdrobonoh on 04.10.17.
 */

public interface FilterClickListener {

    void onClick(Filter filter);
}
