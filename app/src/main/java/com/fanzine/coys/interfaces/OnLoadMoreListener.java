package com.fanzine.coys.interfaces;

/**
 * Created by mbp on 12/12/17.
 */

public interface OnLoadMoreListener {

    void onLoadMore();
}
