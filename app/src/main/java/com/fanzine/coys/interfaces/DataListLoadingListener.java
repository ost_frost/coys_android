package com.fanzine.coys.interfaces;

import java.util.List;

/**
 * Created by Woland on 19.01.2017.
 */

public interface DataListLoadingListener<T> {

    void onLoaded(List<T> data);

    void onLoaded(T data);

    void onError();

}
