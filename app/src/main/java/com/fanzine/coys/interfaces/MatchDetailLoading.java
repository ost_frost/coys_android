package com.fanzine.coys.interfaces;

import com.fanzine.coys.models.Match;

/**
 * Created by mbp on 12/13/17.
 */

public interface MatchDetailLoading {

    void onLoaded(Match match);
}
