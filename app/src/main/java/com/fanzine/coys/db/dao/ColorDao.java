package com.fanzine.coys.db.dao;

import com.fanzine.coys.models.mails.Color;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

/**
 * Created by maximdrobonoh on 19.09.17.
 */

public class ColorDao extends BaseDaoImpl<Color, Long> {

    public ColorDao(ConnectionSource connectionSource, Class<Color> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
