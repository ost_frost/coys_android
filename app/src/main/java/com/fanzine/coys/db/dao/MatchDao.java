package com.fanzine.coys.db.dao;

import com.fanzine.coys.models.Match;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

/**
 * Created by mbp on 12/9/17.
 */

public class MatchDao extends BaseDaoImpl<Match, Integer> {

    public MatchDao(ConnectionSource connectionSource, Class<Match> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
