package com.fanzine.coys.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.fanzine.coys.db.dao.ColorDao;
import com.fanzine.coys.db.dao.GossipDao;
import com.fanzine.coys.db.dao.LabelDao;
import com.fanzine.coys.db.dao.LeagueDao;
import com.fanzine.coys.db.dao.LiguesMatchesDao;
import com.fanzine.coys.db.dao.MailDao;
import com.fanzine.coys.db.dao.MatchDao;
import com.fanzine.coys.db.dao.NewsDao;
import com.fanzine.coys.db.dao.TeamDao;
import com.fanzine.coys.db.dao.VenueDao;
import com.fanzine.coys.models.Gossip;
import com.fanzine.coys.models.League;
import com.fanzine.coys.models.LigueMatches;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.models.News;
import com.fanzine.coys.models.Team;
import com.fanzine.coys.models.Venue;
import com.fanzine.coys.models.mails.Color;
import com.fanzine.coys.models.mails.Label;
import com.fanzine.coys.models.mails.Mail;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DBHelper extends OrmLiteSqliteOpenHelper {
    private static final String TAG = DBHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "database.db";
    private static final int DATABASE_VERSION = 21;
    private NewsDao news;
    private MailDao mails;
    private LeagueDao leagues;
    private GossipDao gossip;
    private MatchDao match;
    private LabelDao labels;
    private ColorDao labelColors;
    private VenueDao venueDao;
    private LiguesMatchesDao ligueMatches;
    private TeamDao team;


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, News.class);
            TableUtils.createTable(connectionSource, Color.class);
            TableUtils.createTable(connectionSource, Label.class);
            TableUtils.createTable(connectionSource, Mail.class);
            TableUtils.createTable(connectionSource, League.class);
            TableUtils.createTable(connectionSource, Gossip.class);
            TableUtils.createTable(connectionSource, Venue.class);
            TableUtils.createTable(connectionSource, Team.class);
            TableUtils.createTable(connectionSource, Match.class);
            TableUtils.createTable(connectionSource, LigueMatches.class);
        } catch (SQLException e) {
            Log.e(TAG, "error creating DB " + DATABASE_NAME);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer,
                          int newVer) {
        try {
            //Так делают ленивые, гораздо предпочтительнее не удаляя БД аккуратно вносить изменения
            TableUtils.dropTable(connectionSource, News.class, true);
            TableUtils.dropTable(connectionSource, Mail.class, true);
            TableUtils.dropTable(connectionSource, Label.class, true);
            TableUtils.dropTable(connectionSource, Color.class, true);
            TableUtils.dropTable(connectionSource, League.class, true);
            TableUtils.dropTable(connectionSource, Gossip.class, true);
            TableUtils.dropTable(connectionSource, Venue.class, true);
            TableUtils.dropTable(connectionSource, Team.class, true);
            TableUtils.dropTable(connectionSource, Match.class, true);
            TableUtils.dropTable(connectionSource, LigueMatches.class, true);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(TAG, "error upgrading db " + DATABASE_NAME + "from ver " + oldVer);
            throw new RuntimeException(e);
        }
    }


    public NewsDao getNewsDao() throws SQLException {
        if (news == null) {
            news = new NewsDao(getConnectionSource(), News.class);
        }
        return news;
    }

    public MailDao getMailDao() throws SQLException {
        if (mails == null) {
            mails = new MailDao(getConnectionSource(), Mail.class);
        }
        return mails;
    }

    public VenueDao getVanueDao() throws SQLException {
        if (venueDao == null) {
            venueDao = new VenueDao(getConnectionSource(), Venue.class);
        }
        return venueDao;
    }

    public MatchDao getMatchDao() throws SQLException {
        if (match == null) {
            match = new MatchDao(getConnectionSource(), Match.class);
        }
        return match;
    }

    public TeamDao getTeamDao() throws SQLException {
        if (team == null) {
            team = new TeamDao(getConnectionSource(), Team.class);
        }
        return team;
    }


    public LeagueDao getLeagueDao() throws SQLException {
        if (leagues == null) {
            leagues = new LeagueDao(getConnectionSource(), League.class);
        }
        return leagues;
    }

    public GossipDao getGossipDao() throws SQLException {
        if (gossip == null) {
            gossip = new GossipDao(getConnectionSource(), Gossip.class);
        }
        return gossip;
    }

    public LabelDao getLabelDao() throws SQLException {
        if ( labels == null) {
            labels = new LabelDao(getConnectionSource(), Label.class);
        }
        return labels;
    }

    public LiguesMatchesDao getLiguesMatchesDao() throws SQLException {
        if ( ligueMatches == null) {
            ligueMatches = new LiguesMatchesDao(getConnectionSource(), LigueMatches.class);
        }
        return ligueMatches;
    }

    public ColorDao getLabelColorsDao() throws SQLException {
        if ( labelColors == null) {
            labelColors = new ColorDao(getConnectionSource(), Color.class);
        }
        return labelColors;
    }

    @Override
    public void close() {
        super.close();
        news = null;
        leagues = null;
        gossip = null;
        mails = null;
    }

    public void clearMails(){
        try {
            TableUtils.clearTable(connectionSource, Mail.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void clearMatches(){
        try {
            TableUtils.clearTable(connectionSource, Match.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void clearLabels() {
        try {
            TableUtils.clearTable(connectionSource, Label.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void clearColors() {
        try {
            TableUtils.clearTable(connectionSource, Color.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void clearLeagues(){
        try {
            TableUtils.clearTable(connectionSource, League.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void clearGossip(){
        try {
            TableUtils.clearTable(connectionSource, Gossip.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}