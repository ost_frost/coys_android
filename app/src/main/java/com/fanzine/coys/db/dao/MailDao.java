package com.fanzine.coys.db.dao;

import com.fanzine.coys.models.mails.Mail;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Serj Woland on 24.07.2015.
 */
public class MailDao extends BaseDaoImpl<Mail, Long> {

    public MailDao(ConnectionSource connectionSource, Class<Mail> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Mail> getMailsFromFolder(String folder) throws SQLException {
        QueryBuilder<Mail, Long> queryBuilder = this.queryBuilder();
        queryBuilder.where().eq("folder", folder);

        return queryBuilder.query();
    }

    public void readMail(String folderFrom, int id) throws SQLException {
        UpdateBuilder<Mail, Long> updateBuilder = this.updateBuilder();
        updateBuilder.updateColumnValue("seen", 1);
        updateBuilder.where().eq("id", id).and().eq("folder", folderFrom);
        updateBuilder.update();
    }

    public void deleteMail(String folderFrom, int id) throws SQLException {
        DeleteBuilder<Mail, Long> deleteBuilder = this.deleteBuilder();
        deleteBuilder.where().eq("id", id).and().eq("folder", folderFrom);
        deleteBuilder.delete();
    }

    public void updateFlag(String folderFrom, int mailId, int flagged) throws SQLException {
        UpdateBuilder<Mail, Long> updateBuilder = this.updateBuilder();
        updateBuilder.updateColumnValue("flagged", flagged);
        updateBuilder.where().eq("id", mailId).and().eq("folder", folderFrom);
        updateBuilder.update();
    }

    public List<Mail> getMailsFlagged() throws SQLException {
        QueryBuilder<Mail, Long> queryBuilder = this.queryBuilder();
        queryBuilder.where().eq("flagged", 1);
        return queryBuilder.query();
    }

    public List<Mail> getMailsUnread() throws SQLException {
        QueryBuilder<Mail, Long> queryBuilder = this.queryBuilder();
        queryBuilder.where().eq("seen", 0);
        return queryBuilder.query();
    }
}