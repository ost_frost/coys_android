package com.fanzine.coys.db.dao;

import com.fanzine.coys.models.LigueMatches;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

/**
 * Created by mbp on 12/9/17.
 */

public class LiguesMatchesDao extends BaseDaoImpl<LigueMatches, Long> {

    public LiguesMatchesDao(ConnectionSource connectionSource, Class<LigueMatches> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}