package com.fanzine.coys.db.dao;

import com.fanzine.coys.models.Venue;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

/**
 * Created by mbp on 12/9/17.
 */

public class VenueDao extends BaseDaoImpl<Venue, Integer> {

    public VenueDao(ConnectionSource connectionSource, Class<Venue> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
