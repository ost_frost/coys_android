package com.fanzine.coys.db.dao;

import com.fanzine.coys.models.Team;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

/**
 * Created by mbp on 12/9/17.
 */

public class TeamDao extends BaseDaoImpl<Team, Integer> {

    public TeamDao(ConnectionSource connectionSource, Class<Team> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
