package com.fanzine.coys.db.dao;

import com.fanzine.coys.models.League;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

/**
 * Created by Woland on 13.02.2017.
 */

public class LeagueDao extends BaseDaoImpl<League, Integer> {

    public LeagueDao(ConnectionSource connectionSource, Class<League> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
