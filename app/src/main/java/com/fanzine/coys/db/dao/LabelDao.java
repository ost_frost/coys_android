package com.fanzine.coys.db.dao;

import com.fanzine.coys.models.mails.Label;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

/**
 * Created by maximdrobonoh on 19.09.17.
 */

public class LabelDao extends BaseDaoImpl<Label, Long> {

    public LabelDao(ConnectionSource connectionSource, Class<Label> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
