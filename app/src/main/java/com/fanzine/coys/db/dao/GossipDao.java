package com.fanzine.coys.db.dao;

import com.fanzine.coys.models.Gossip;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Serj Woland on 24.07.2015.
 */
public class GossipDao extends BaseDaoImpl<Gossip, Long> {

    public GossipDao(ConnectionSource connectionSource, Class<Gossip> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Gossip> getGossipsFromFolder(String folder) throws SQLException {
        QueryBuilder<Gossip, Long> queryBuilder = this.queryBuilder();
        queryBuilder.where().eq("folder", folder);
        return queryBuilder.query();
    }
}