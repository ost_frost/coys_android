package com.fanzine.coys.db.dao;

import com.fanzine.coys.models.News;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Serj Woland on 24.07.2015.
 */
public class NewsDao extends BaseDaoImpl<News, Long> {

    public NewsDao(ConnectionSource connectionSource, Class<News> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<News> getNewsByLeague(int id) throws SQLException {
        QueryBuilder<News, Long> queryBuilder = this.queryBuilder();
        queryBuilder.where().eq("id", id);
        PreparedQuery<News> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery);
    }
//
//    public void deleteProduct(String id) throws SQLException {
//        DeleteBuilder<Product, Integer> deleteBuilder = this.deleteBuilder();
//        deleteBuilder.where().eq("id", id);
//        deleteBuilder.delete();
//    }
}