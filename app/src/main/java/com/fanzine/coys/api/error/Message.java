package com.fanzine.coys.api.error;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Woland on 27.04.2017.
 */

public class Message {

    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
