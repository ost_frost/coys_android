package com.fanzine.coys.api;

import android.content.Intent;

import com.fanzine.coys.App;
import com.fanzine.coys.activities.StartActivity;
import com.fanzine.coys.api.gson.GsonConfig;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.coys.utils.DiviceInfoUtil;
import com.google.firebase.auth.FirebaseAuth;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by razir on 3/29/2016.
 */
public class ApiRequest {

    private static ApiRequest apiRequest;

    private static final String MAIN_URL = "http://test.coys.com/api-almet/v1.0/";
    //private static final String MAIN_URL = "http://gunners.red-carlos.com/api-almet/v1.0/";

    // private static final String MAIN_URL = "https://private-c29599-gunners.apiary-mock.com/api-almet/v1.0/";
    private ApiInterface api;
    private Retrofit retrofit;

    public static ApiRequest getInstance() {
        if (apiRequest == null) {
            apiRequest = new ApiRequest();
        }
        return apiRequest;
    }

    public ApiRequest() {
        retrofit = initRetrofit();
        api = retrofit.create(ApiInterface.class);
    }

    protected Retrofit initRetrofit() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(chain -> chain.proceed(chain.request().newBuilder()
                        .addHeader("Authorization", SharedPrefs.getUserToken() != null
                                ? "Bearer " + SharedPrefs.getUserToken().getToken() : "")
                        .addHeader("User-Agent", DiviceInfoUtil.getInstance().getDeviceInfo())
                        .build()))
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    Response response = chain.proceed(original);
                    if (response.code() == 401) {
                        SharedPrefs.saveUserToken(null);
                        FirebaseAuth.getInstance().signOut();
                        Intent intent = StartActivity.getStartIntent(App.getContext());
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        App.getContext().startActivity(intent);
                    }
                    return response;
                })
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        return new Retrofit.Builder()
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonConfig.buildDefaultJson()))
                .baseUrl(MAIN_URL)
                .build();
    }

    public ApiInterface getApi() {
        return api;
    }
}
