package com.fanzine.coys.api.response;

import java.util.ArrayList;
import java.util.List;

import com.fanzine.coys.models.League;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Woland on 26.01.2017.
 */

public class LeagueList {

    @SerializedName("leagues")
    @Expose
    private List<League> leagues = new ArrayList<>();

    public List<League> getLeagues() {
        return leagues;
    }

    public void setLeagues(List<League> leagues) {
        this.leagues = leagues;
    }

}