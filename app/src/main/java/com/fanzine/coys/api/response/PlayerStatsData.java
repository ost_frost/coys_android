package com.fanzine.coys.api.response;

import com.fanzine.coys.models.table.PlayerStats;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Woland on 22.02.2017.
 */

public class PlayerStatsData {

    @SerializedName("goals")
    private List<PlayerStats> goals = new ArrayList<>();

    @SerializedName("assists")
    private List<PlayerStats> assists = new ArrayList<>();

    @SerializedName("reds")
    private List<PlayerStats> reds = new ArrayList<>();

    @SerializedName("yellows")
    private List<PlayerStats> yellows = new ArrayList<>();

    public List<PlayerStats> getGoals() {
        return goals;
    }

    public void setGoals(List<PlayerStats> goals) {
        this.goals = goals;
    }

    public List<PlayerStats> getAssists() {
        return assists;
    }

    public void setAssists(List<PlayerStats> assists) {
        this.assists = assists;
    }

    public List<PlayerStats> getReds() {
        return reds;
    }

    public void setReds(List<PlayerStats> reds) {
        this.reds = reds;
    }

    public List<PlayerStats> getYellows() {
        return yellows;
    }

    public void setYellows(List<PlayerStats> yellows) {
        this.yellows = yellows;
    }

    public boolean isEmpty() {
        return goals.size() == 0 && assists.size() == 0 && reds.size() == 0 && yellows.size() == 0;
    }
}
