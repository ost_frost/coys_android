package com.fanzine.coys.api;

import com.fanzine.coys.models.venues.GooglePlaceResults;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;


public interface GoogleApiInterface {

    @GET("geocode/json")
    Observable<GooglePlaceResults> geoCode(@Query("address") String address, @Query("key") String key);
}
