package com.fanzine.coys.api.model;

import android.content.Intent;

import com.fanzine.coys.App;
import com.fanzine.coys.activities.StartActivity;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by a on 09.12.17
 */

public class LoginModel {

    public void logOut() {
        SharedPrefs.saveUserToken(null);
        FirebaseAuth.getInstance().signOut();
        Intent intent = StartActivity.getStartIntent(App.getContext());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        App.getContext().startActivity(intent);
    }

}
