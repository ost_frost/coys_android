package com.fanzine.coys.api.response;

import com.fanzine.coys.models.VideoItem;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Woland on 27.01.2017.
 */

public class VideoData {

    @SerializedName("arsenal")
    private List<VideoItem> arsenal = new ArrayList<>();

    @SerializedName("trending")
    private List<VideoItem> trending = new ArrayList<>();

    public List<VideoItem> getTrending() {
        if (trending == null)
            trending = new ArrayList<>();

        return trending;
    }

    public void setTrending(List<VideoItem> trending) {
        this.trending = trending;
    }

    public List<VideoItem> getArsenal() {
        if (arsenal == null)
            arsenal = new ArrayList<>();

        return arsenal;
    }

    public void setArsenal(List<VideoItem> arsenal) {
        this.arsenal = arsenal;
    }
}
