package com.fanzine.coys.api.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Woland on 28.02.2017.
 */

public class Uid {

    @SerializedName("uid")
    private int uid;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }
}
