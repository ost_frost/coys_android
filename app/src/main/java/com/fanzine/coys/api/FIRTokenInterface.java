package com.fanzine.coys.api;

import com.fanzine.coys.models.login.UserToken;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by mbp on 3/20/18.
 */

public interface FIRTokenInterface {

    @GET("token")
    Observable<UserToken> getToken(@Query("key") String key, @Query("uid") String uid);
}
