package com.fanzine.coys.api.response;

import com.fanzine.coys.models.mails.Folder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximdrobonoh on 13.09.17.
 */

public class MailboxFolders {

    @SerializedName("folders")
    @Expose
    private List<Folder> leagues = new ArrayList<>();

    public List<Folder> getFolders() {
        return leagues;
    }

    public void setFolders(List<Folder> leagues) {
        this.leagues = leagues;
    }
}
