package com.fanzine.coys.api.gson;


import com.fanzine.coys.models.table.TeamScore;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class GsonConfig {

    private static DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

    private static JsonDeserializer<DateTime> dateDeser = (json, typeOfT, context) -> {
        if (json == null) {
            return null;
        } else {
            if (!json.isJsonNull()) {
                try {
                    return formatter.parseDateTime(json.getAsString());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            } else {
                return null;
            }
        }
    };

    private static GsonBuilder getBuilder() {
        return new GsonBuilder().registerTypeAdapter(DateTime.class, dateDeser);
    }

    public static Gson buildDefaultJson() {
        return getBuilder().create();
    }

    public static Gson buildTeamsScoreJson(String criteria) {
        return new GsonBuilder().registerTypeAdapter(
                        TeamScore.class, new TeamScoreAdapter(criteria)).create();
    }

}
