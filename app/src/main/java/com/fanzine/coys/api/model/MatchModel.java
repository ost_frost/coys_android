package com.fanzine.coys.api.model;

import android.util.Log;

import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.models.LigueMatches;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import rx.Observable;

/**
 * Created by a on 09.12.17
 */
public class MatchModel {
    private static final String LOG_TAG = "Log";

    public Observable<List<LigueMatches>> getMatch(String date) {

        Observable<ResponseBody> obsNet = ApiRequest.getInstance().getApi().getMatches(date);

        Observable<List<LigueMatches>> obsList = obsNet.flatMap(responseBody -> {
            String responseBodyStr = null;

            List<LigueMatches> list = null;

            try {
                responseBodyStr = responseBody.string();
                Log.i(LOG_TAG, "getMatch() response=" + responseBodyStr);

            } catch (IOException e) {
                e.printStackTrace();
                return Observable.error(e);
            }

            Gson gson = new GsonBuilder()
                    .create();

            try {
                list = gson.fromJson(responseBodyStr, new TypeToken<List<LigueMatches>>() {
                }.getType());
            } catch (JsonParseException e) {
                e.printStackTrace();
                new LoginModel().logOut();
                return Observable.error(e);

            }
            
            return Observable.just(list);

        });

        return obsList;
    }
}
