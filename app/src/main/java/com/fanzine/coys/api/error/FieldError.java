package com.fanzine.coys.api.error;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by razir on 8/4/2016.
 */
public class FieldError {
    @SerializedName("success")
    String success = "";
    @SerializedName(value = "message", alternate = "error")
    String message = "";
    @SerializedName("field_name")
    String field_name = "";

    public String getField() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public boolean isEmpty() {
        return TextUtils.isEmpty(success) || TextUtils.isEmpty(message);
    }

    public String getFieldName() {
        return field_name;
    }
}
