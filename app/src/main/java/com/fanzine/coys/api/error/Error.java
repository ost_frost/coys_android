package com.fanzine.coys.api.error;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Woland on 22.03.2017.
 */

public class Error {

    @SerializedName("error")
    private String error = "";

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
