package com.fanzine.coys.api.gson;

import com.fanzine.coys.models.table.TeamScore;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by maximdrobonoh on 03.10.17.
 */

public class TeamScoreAdapter implements JsonDeserializer<TeamScore> {

    private String criteriaFiledName;


    public TeamScoreAdapter(String criteriaFiledName) {
        this.criteriaFiledName = criteriaFiledName;
    }

    @Override
    public TeamScore deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        final JsonObject jsonObject = json.getAsJsonObject();

        int total = 0;

        if ( jsonObject.has(criteriaFiledName))  {
            total = jsonObject.get(criteriaFiledName).getAsInt();
        }

        TeamScore teamScore = context.deserialize(jsonObject, TeamScore.class);
        teamScore.setTotal(total);

        return teamScore;
    }

}
