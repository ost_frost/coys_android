package com.fanzine.coys.api.error;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by razir on 8/4/2016.
 */
public class ErrorResponseParser {

    public static FieldError getErrors(Throwable throwable) {
        if (throwable instanceof HttpException) {
            HttpException err = (HttpException) throwable;
            try {
                FieldError response = new Gson().fromJson(err.response().errorBody().string(), FieldError.class);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new FieldError();
    }

    public static List<FieldError> getErrorList(Throwable throwable) {
        if (throwable instanceof HttpException) {
            HttpException err = (HttpException) throwable;
            try {
                return new Gson().fromJson(err.response().errorBody().string(), Errors.class).getErrors();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new ArrayList<>();
    }

    public static Error getError(Throwable throwable) {
        if (throwable instanceof HttpException) {
            HttpException err = (HttpException) throwable;
            try {
                Error response = new Gson().fromJson(err.response().errorBody().string(), Error.class);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new Error();
    }

    public static ApiError getApiError(Throwable throwable) {
        if (throwable instanceof HttpException) {
            HttpException err = (HttpException) throwable;
            try {
                ApiError response = new Gson().fromJson(err.response().errorBody().string(), ApiError.class);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private class Errors {
        @SerializedName("errors")
        private List<FieldError> errors;

        public List<FieldError> getErrors() {
            return errors;
        }

        public void setErrors(List<FieldError> errors) {
            this.errors = errors;
        }
    }

    public class ApiError {
        @SerializedName("error")
        private List<FieldError> errors;

        public boolean isEmpty() {
            return errors == null;
        }

        public FieldError getError() {
            if ( !isEmpty() && errors.size() > 0) return  errors.get(0);

            return null;
        }
    }


    public static ApiErrorWithMessage getApiErrorWithMessage(Throwable throwable) {
        if (throwable instanceof HttpException) {
            HttpException err = (HttpException) throwable;
            try {
                ApiErrorWithMessage response
                        = new Gson().fromJson(err.response().errorBody().string(), ApiErrorWithMessage.class);
                return response;
            } catch (Exception e) {
                e.printStackTrace();

                return null;
            }
        }
        return null;
    }

    public class ApiErrorWithMessage {
        @SerializedName("message")
        public String message;
        @SerializedName("success")
        public Boolean isSuccess;
    }
}
