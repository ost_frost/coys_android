package com.fanzine.coys.api;

import com.fanzine.coys.api.gson.GsonConfig;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mbp on 3/20/18.
 */

public class FirebaseApiRequest {

    private static FirebaseApiRequest apiRequest;

    private static final String MAIN_URL = "https://us-central1-steam-insight-160106.cloudfunctions.net/";

    private FIRTokenInterface api;
    private Retrofit retrofit;

    public static FirebaseApiRequest getInstance() {
        if (apiRequest == null) {
            apiRequest = new FirebaseApiRequest();
        }
        return apiRequest;
    }

    public FirebaseApiRequest() {
        retrofit = initRetrofit();
        api = retrofit.create(FIRTokenInterface.class);
    }

    protected Retrofit initRetrofit() {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonConfig.buildDefaultJson()))
                .baseUrl(MAIN_URL)
                .build();
    }

    public FIRTokenInterface getApi() {
        return api;
    }
}
