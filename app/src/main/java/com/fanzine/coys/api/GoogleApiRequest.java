package com.fanzine.coys.api;

import com.fanzine.coys.api.gson.GsonConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by razir on 3/29/2016.
 */
public class GoogleApiRequest {

    private static GoogleApiRequest apiRequest;

    private static final String MAIN_URL = "https://maps.googleapis.com/maps/api/";

    private GoogleApiInterface api;
    private Retrofit retrofit;

    public static GoogleApiRequest getInstance() {
        if (apiRequest == null) {
            apiRequest = new GoogleApiRequest();
        }
        return apiRequest;
    }


    public GoogleApiRequest() {
        retrofit = initRetrofit();
        api = retrofit.create(GoogleApiInterface.class);
    }

    protected Retrofit initRetrofit() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        return new Retrofit.Builder()
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonConfig.buildDefaultJson()))
                .baseUrl(MAIN_URL)
                .build();
    }

    public GoogleApiInterface getApi() {
        return api;
    }
}
