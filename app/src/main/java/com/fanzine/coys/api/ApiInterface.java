package com.fanzine.coys.api;

import com.fanzine.coys.api.error.Message;
import com.fanzine.coys.api.response.LeagueList;
import com.fanzine.coys.api.response.MailboxFolders;
import com.fanzine.coys.api.response.PlayerStatsData;
import com.fanzine.coys.models.Currency;
import com.fanzine.coys.models.Gossip;
import com.fanzine.coys.models.LigueMatches;
import com.fanzine.coys.models.LikeResponse;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.models.MatchComment;
import com.fanzine.coys.models.News;
import com.fanzine.coys.models.PriceInfo;
import com.fanzine.coys.models.ProEmail;
import com.fanzine.coys.models.Social;
import com.fanzine.coys.models.Stats;
import com.fanzine.coys.models.Status;
import com.fanzine.coys.models.Video;
import com.fanzine.coys.models.analysis.Analysis;
import com.fanzine.coys.models.analysis.Prediction;
import com.fanzine.coys.models.lineups.LineUp;
import com.fanzine.coys.models.login.Country;
import com.fanzine.coys.models.login.Teams;
import com.fanzine.coys.models.login.UserToken;
import com.fanzine.coys.models.mails.Color;
import com.fanzine.coys.models.mails.Label;
import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.models.mails.MailContent;
import com.fanzine.coys.models.matches.MatchNotification;
import com.fanzine.coys.models.payment.PaymentToken;
import com.fanzine.coys.models.profile.ChatNotificationState;
import com.fanzine.coys.models.profile.ChatNotifications;
import com.fanzine.coys.models.profile.Email;
import com.fanzine.coys.models.profile.EmailNotifications;
import com.fanzine.coys.models.profile.League;
import com.fanzine.coys.models.profile.LeagueType;
import com.fanzine.coys.models.profile.Notifications;
import com.fanzine.coys.models.profile.Profile;
import com.fanzine.coys.models.profile.ProfileBody;
import com.fanzine.coys.models.profile.Terms;
import com.fanzine.coys.models.summary.Summary;
import com.fanzine.coys.models.table.Filter;
import com.fanzine.coys.models.table.Standings;
import com.fanzine.coys.models.table.TablesMatch;
import com.fanzine.coys.models.team.Player;
import com.fanzine.coys.models.team.PlayerInfo;
import com.fanzine.coys.models.team.PlayerPhoto;
import com.fanzine.coys.models.team.PlayerProfile;
import com.fanzine.coys.models.team.PlayerStat;
import com.fanzine.coys.models.venues.MajorCategory;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ChannelPost;
import com.fanzine.chat3.model.pojo.ChatUser;
import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.model.pojo.ContactSync;
import com.fanzine.chat3.model.pojo.MessagePost;
import com.fanzine.chat3.model.pojo.RenameGroup;
import com.fanzine.chat3.networking.Responce.DialogNameChangeRequestResponce;
import com.fanzine.chat3.networking.Responce.PostDialogImageResponce;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;


public interface ApiInterface {

    @DELETE("user/{user_id}")
    Observable<ResponseBody> deleteUser(@Path("user_id") String userId, @Query("password") String password);

    @FormUrlEncoded
    @POST("user/enable/android")
    Observable<ResponseBody> sendPaymentInfo(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("password_check")
    Observable<ResponseBody> recoveryPasswordCheck(@FieldMap Map<String, String> map);

    @GET("register/countries")
    Observable<List<Country>> getCountries();

    @FormUrlEncoded
    @POST("validate/pro")
    Observable<Currency> validatePro(@FieldMap Map<String, String> map);

    @GET("yelp/major_category")
    Observable<MajorCategory> getYelpMajorCategory(@Query("category") String category);

    @GET("team/players/{player_id}/profile")
    Observable<PlayerProfile> getPlayerProfile(@Path("player_id") int playerId);

    @GET("team/players/{player_id}/social")
    Observable<List<Social>> getPlayerSocial(@Path("player_id") int playerId);

    @GET("team/players/{player_id}/statistics")
    Observable<List<PlayerStat>> getTeamPlayerStats(@Path("player_id") int playerId);

    @GET("team/players/{player_id}/photos")
    Observable<List<PlayerPhoto>> getTeamPlayerPhotos(@Path("player_id") int playerId);

    @GET("team/players/{player_id}/videos")
    Observable<List<Video>> getTeamPlayerVideos(@Path("player_id") int playerId);

    @GET("team/players")
    Observable<List<JsonObject>> getPlayers(@Query("squad") String squad);

    @GET("news")
    Observable<List<News>> getNews(@Query("league_id") int league_id, @Query("page") int page, @Query("limit") int limit);

    @GET("match/fixtures/upcoming/verbose")
    Observable<List<Match>> getFixturesUpcoming(@Query("limit") int limit);

    @GET("news/gossips")
    Observable<List<Gossip>> getGossips();

    @GET("news/{news_id}")
    Observable<News> getNewsContent(@Path("news_id") int news_id);

    @GET("news/social")
    Observable<List<Social>> getSocialFeed(@Query("page") int page, @Query("limit") int limit);

    @POST("news/{id}/like")
    Observable<LikeResponse> likeNews(@Path("id") int id);

    @POST("news/{id}/unlike")
    Observable<LikeResponse> unlikeNews(@Path("id") int id);

//    @GET("video")
//    Observable<VideoData> getVideos(@Query("league_id") int league_id, @Query("page") int page);

    @GET("videos")
    Observable<List<Video>> getVideoList(@Query("sort") String sort,
                                         @Query("upload_date") String uploadDate,
                                         @Query("search") String query);

    @POST("videos/{id}/like")
    Observable<LikeResponse> likeVideo(@Path("id") int id);

    @POST("videos/{id}/unlike")
    Observable<LikeResponse> unlikeVideo(@Path("id") int id);


    @GET("leagues")
    Observable<LeagueList> getLeagues();

    @GET("news/leagues")
    Observable<LeagueList> getMajorLeagues();

    @GET("matches")
    Observable<ResponseBody> getMatches(@Query("date") String date);

    @GET("leagues/{leagueId}/matches")
    Observable<LigueMatches> getMathchesByLeague(@Path("leagueId") int leagueId, @Query("season") String season, @Query("date") String date, @Query("week") int week);

    @GET("leagues/{leagueId}/matches")
    Observable<LigueMatches> getMathchesByLeague(@Path("leagueId") int leagueId, @Query("season") String season, @Query("date") String date);


    @GET("leagues/{leagueId}/matches")
    Observable<TablesMatch> getTableMathches(@Path("leagueId") int leagueId, @Query("season") String season, @Query("date") String date, @Query("week") int week);

    @GET("leagues/{leagueId}/matches")
    Observable<TablesMatch> getTableMathches(@Path("leagueId") int leagueId, @Query("season") String season, @Query("date") String date);

    @GET("match/{id}")
    Observable<Match> getMatch(@Query("id") int id);

    @GET("match/stats")
    Observable<List<Stats>> getStats(@Query("match_id") int idMatch);

    @GET("match/commentaries")
    Observable<List<MatchComment>> getMatchCommentaries(@Query("match_id") int idMatch);

    @GET("matches/{id}/lineups")
    Observable<LineUp> getLineUp(@Path("id") int idMatch);

    @GET("match/events")
    Observable<Summary> getSummary(@Query("match_id") int idMatch);

    @GET("match/standings")
    Observable<Standings> getTeamTables(@Query("league_id") int league_id);

    @GET("matches/{id}/analysis")
    Observable<Analysis> getAnalysis(@Path("id") int matchId);

    @POST("match/analysis/vote")
    @FormUrlEncoded
    Observable<Prediction> castVote(@Field("match_id") int matchId, @Field("vote") String vote);

    @GET("stats/players")
    Observable<PlayerStatsData> getPlayerStats(@Query("league_id") int league_id);

    @GET("email/{folder_from}/{mailId}")
    Observable<MailContent> getMailContent(@Path("folder_from") String folderFrom, @Path("mailId") int mailId);

    @GET("email/{folder_from}/{mailId}")
    Call<MailContent> getMailContentCall(@Path("folder_from") String folderFrom, @Path("mailId") int mailId);

    @GET("email/list")
    Observable<List<Mail>> getMails();

    @GET("email/list/{folder}")
    Observable<List<Mail>> getMailsByFolder(@Path("folder") String folder);

    @GET("email/{folder_from}/{folder_name}/move/{email_id}")
    Observable<ResponseBody> moveMail(@Path("folder_from") String folderFrom, @Path("folder_name") String folder, @Path("email_id") int email_id);

    @GET("email/{folder_from}/{email_id}/remove")
    Observable<ResponseBody> removeMail(@Path("folder_from") String folderFrom, @Path("email_id") String email_id);

    @GET("email/{folder_from}/{email_id}/read")
    Observable<ResponseBody> readMail(@Path("folder_from") String folderFrom, @Path("email_id") int email_id);

    @GET("email/{folder_from}/{email_id}/flagged")
    Observable<ResponseBody> flagMail(@Path("folder_from") String folderFrom, @Path("email_id") int email_id);

    @Multipart
    @POST("email/send")
    Observable<ResponseBody> sendMail(@PartMap() Map<String, RequestBody> partMap, @Part List<MultipartBody.Part> attachments);

    @Multipart
    @POST("email/draft")
    Observable<ResponseBody> saveToDraft(@PartMap() Map<String, RequestBody> partMap, @Part List<MultipartBody.Part> attachments);

    @GET("players/list")
    Observable<List<Player>> getPlayers();

    @GET("player/{id}")
    Observable<PlayerInfo> getPlayerInfo(@Path("id") int id);

    @GET("notifications")
    Observable<MatchNotification> getMatchNotification(@Query("match_id") int match_id);

    @POST("notifications")
    Observable<ResponseBody> setMatchNotification(@Body MatchNotification matchNotification);

    @GET("profile/notifications/email")
    Observable<EmailNotifications> getEmailNotification();

    @POST("profile/notifications/email")
    Observable<ResponseBody> setEmailNotification(@Body EmailNotifications notification);

    @GET("profile/notifications/chat")
    Observable<ChatNotifications> getChatNotification();

    @POST("profile/notifications/chat")
    Observable<ResponseBody> setChatNotification(@Body ChatNotifications notification);

    @GET("profile/notifications/teams")
    Observable<List<League>> getTeamsNotification();

    @POST("profile/notifications/teams")
    Observable<ResponseBody> setTeamsNotification(@Body Notifications notification);

    @FormUrlEncoded
    @POST("login")
    Observable<UserToken> fbLogIn(@Field("fb_token") String fb_token, @Field("fb_id") String fb_id);

    @GET("register")
    Observable<Teams> getTeams();

    @FormUrlEncoded
    @POST("register")
    Observable<UserToken> register(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("register_pro")
    Observable<UserToken> registerPro(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("validate_data")
    Observable<PriceInfo> validateData(@FieldMap Map<String, String> map);

    @GET("get_names")
    Observable<List<ProEmail>> getNames(@Query("first_name") String fisrtName, @Query("last_name") String lastName);

    @FormUrlEncoded
    @POST("login")
    Observable<UserToken> login(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("password_reset")
    Observable<Message> passwordReset(@FieldMap Map<String, String> map);

    @GET("profile/account")
    Observable<Profile> getProfile();

    @POST("profile/account")
    Observable<Profile> setProfile(@Body ProfileBody profile);

    @FormUrlEncoded
    @POST("profile/password/reset")
    Observable<ResponseBody> resetPassword(@FieldMap Map<String, String> map);

    @GET("profile/terms-of-usage")
    Observable<Terms> getTermsOfUsage();

    @GET("profile/notifications/chat")
    Observable<ChatNotificationState> getChatNotificationsStatus();

    @GET("profile/email")
    Observable<Email> getProfileEmail();

    @GET("profile/notifications/teams")
    Observable<List<LeagueType>> getProfileNotificationsTeams();

    @POST("profile/notifications/teams")
    Observable<ResponseBody> setProfileNotificationsTeams(@Body Notifications body);

    @FormUrlEncoded
    @POST("profile/email/notification")
    Observable<ResponseBody> setEmailNotification(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("email/signature")
    Observable<ResponseBody> setEmailSignature(@FieldMap Map<String, String> map);


    @POST("profile/notifications/chat")
    Observable<ChatNotificationState> setChatNotificationsState(@Body ChatNotificationState chatNotificationState);

    @Multipart
    @POST("profile/account")
    Observable<Profile> setProfile(@PartMap Map<String, RequestBody> params, @Part MultipartBody.Part photo_file);

    @GET("check_phone")
    Observable<ResponseBody> checkPhone(@Query("phonecode") String phonecode, @Query("phone") String phone);

    @FormUrlEncoded
    @POST("check_phone")
    Observable<ResponseBody> checkPhone(@Field("phonecode") String phonecode, @Field("phone") String phone, @Field("verification_code") String verification_code);

    @FormUrlEncoded
    @POST("password_save")
    Observable<ResponseBody> saveNewPassword(@FieldMap Map<String, String> map);

    @GET("get_user_status")
    Observable<Status> getUserStatus();

    @GET("get_payment_token")
    Observable<PaymentToken> getPaymentToken();

    @FormUrlEncoded
    @POST("subscribe_pro")
    Observable<Status> sendPayment(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("upgrade_pro")
    Observable<Status> upgradeToPro(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("instance_tokens")
    Observable<ResponseBody> sendDeviceToken(@Field("instance_token") String token);

    @FormUrlEncoded
    @POST("email/folder")
    Observable<ResponseBody> addMailbox(@Field("name") String name);

    @GET("email/folder")
    Observable<MailboxFolders> getMailboxFolders();

    @FormUrlEncoded
    @POST("email/folder/delete")
    Observable<ResponseBody> removeMailbox(@Field("name") String name);

    @GET("email/labels/list")
    Observable<List<Label>> getLabelsList();

    @GET("email/labels/colors")
    Observable<List<Color>> getLabelColors();

    @FormUrlEncoded
    @POST("email/labels/addToUser")
    Observable<List<Label>> createEmailLabel(@Field("name") String name, @Field("color_id") int colorId);

    @FormUrlEncoded
    @POST("email/labels/deleteFromUser")
    Observable<ResponseBody> deleteLabel(@Field("label_id") int labelId);

    @FormUrlEncoded
    @POST("email/labels/add")
    Observable<ResponseBody> addLabelToMail(@Field("folder_name") String folderName,
                                            @Field("email_id") int emailId,
                                            @Field("label_id") int labelId);

    @FormUrlEncoded
    @POST("email/labels/delete")
    Observable<ResponseBody> deleteLabelFromMail(@Field("folder_name") String folderName,
                                                 @Field("email_id") int emailId,
                                                 @Field("label_id") int labelId);

    @GET("email/{folder_from}/{email_id}/unread")
    Observable<ResponseBody> setMailUnread(@Path("folder_from") String folderName,
                                           @Path("email_id") String emailId);


    @GET("leagues/{league_id}/teams/statistics")
    Observable<List<JsonObject>> getTeams(@Path("league_id") int leagueId, @Query("season") String season);

    @GET("leagues/{league_id}/players/statistics")
    Observable<List<JsonObject>> getPlayers(@Path("league_id") int leagueId, @Query("sort_field") String sortField);

    @GET("matches/getTeamsFilterCriteries")
    Observable<List<Filter>> getTeamsFilters();

    @GET("leagues/players/statistics/filters")
    Observable<List<Filter>> getPlayersFilters();

    @GET("listed-regions/leagues")
    Observable<JsonArray> getRegionsLeagues();

    //-------------------------CHAT
    //send invite sms to chat user
    @FormUrlEncoded
    @POST("chat/refer")
    Observable<ResponseBody> sendInviteSms(@Field("phone") String phone);

    @GET("chat/channels")
    Observable<List<Channel>> getChatDialogs();

    @GET("chat/channels/{uid}")
    Observable<Channel> getChannelByuid(@Path("uid") String uid);


    @POST("chat/contacts/sync")
    Observable<List<ContactPhone>> syncContatcs(@Body ContactSync contactSync);

    //POST http://gunners.red-carlos.com/api-almet/v1.0/channels
    @POST("chat/channels")
    Observable<Channel> postDialog(@Body ChannelPost channel);

    @POST("chat/channels/{channel_id}/messages/send")
    Observable<com.fanzine.chat3.model.pojo.Message> postMessage(@Path("channel_id") String channelId, @Body MessagePost messagePost);

    @GET("chat/channels/{channel_id}/users")
    Observable<List<ChatUser>> getChannelUser(@Path("channel_id") String channelId);

    //@DELETE("chat/channels/{channel_id}/users")
   /* @FormUrlEncoded
    @HTTP(method = "DELETE", path = "chat/channels/{channel_id}/users", hasBody = true)
    Observable<List<ChatUser>> deleteChannelUser(@Path("channel_id") String channelId,
                                                 @Field("user_ids") String user);*/
    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "chat/channels/{channel_id}/users", hasBody = true)
    Observable<ResponseBody> deleteChannelUser(@Path("channel_id") String channelId,
                                               @Field("user_ids[]") List<String> ids);

    @PUT("chat/channels/{channel_id}/name")
    Observable<DialogNameChangeRequestResponce> renameChannel(@Path("channel_id") String channelId,
                                                              @Body DialogNameChangeRequestResponce name);

    @Multipart
    @POST("chat/channels/{channel_id}/photo")
    Observable<PostDialogImageResponce> postDialogImage(@Path("channel_id") String channelId, @Part MultipartBody.Part image);

    @GET("chat/channels/{channel_id}/messages")
    Observable<List<com.fanzine.chat3.model.pojo.Message>> getMessages(@Path("channel_id") String channelId);

    @Multipart
    @POST("chat/attachments/{type}")
    Observable<ResponseBody> postMessageImage(@Path("type") String type, @Part MultipartBody.Part image);

    @FormUrlEncoded
    @POST("chat/attachments/location")
    Observable<ResponseBody> postMessageLocation(@Field("latitude") String latitude,
                                                 @Field("longitude") String longitude);

    /*@POST("chat/attachments/location")
    Observable<ResponseBody> postMessageLocation(@Body RequestPostMsgPosition requestPostMsgPosition);*/

    @PUT("chat/channels/{channel_id}/name")
    Observable<RenameGroup> renameGroup(@Path("channel_id") String channelid, @Body RenameGroup renameGroup);

    @POST("chat/channels/{channel_id}/leave")
    Observable<ResponseBody> leaveChannel(@Path("channel_id") String channelId);

    @DELETE("chat/channels/{channel_id}")
    Observable<ResponseBody> deleteChannel(@Path("channel_id") String channelId);

    @DELETE("chat/channels/{channel_id}/messages")
    Observable<ResponseBody> deleteMessagesFromChannel(@Path("channel_id") String channelId);

    @POST("chat/channels/{channel_id}/users")
    @FormUrlEncoded
    Observable<List<ChatUser>> addParticipantsToChannel(@Path("channel_id") String channelId,
                                                        @Field("user_ids[]") List<String> userIds);

    @GET("email/{folder_from}/{uid}/flagged")
    Observable<ResponseBody> setMailStarred(@Path("folder_from") String folderFrom, @Path("uid") String uid);


}
