package com.fanzine.coys.activities;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.view.WindowManager;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.base.BaseActivity;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.databinding.FragmentTermsPolicyBinding;
import com.fanzine.coys.models.profile.Terms;
import com.fanzine.coys.utils.DialogUtils;

import java.util.concurrent.Callable;

import rx.Scheduler;
import rx.Single;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mbp on 4/3/18.
 */

public class TermsAndPolicyActivity extends BaseActivity {

    private FragmentTermsPolicyBinding binding;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.fragment_terms_policy);

        ProgressDialog pd = DialogUtils.createProgressDialog(this, "Loading..");
        pd.show();

        ApiRequest.getInstance().getApi().getTermsOfUsage()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Terms>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        pd.dismiss();
                        DialogUtils.showAlertDialog(getContext(), "Server issue");
                    }

                    @Override
                    public void onNext(Terms terms) {
                        pd.dismiss();
                        binding.webView.loadUrl(terms.getTerms());
                    }
                });

        setTranslucentStatusBarLollipop(getWindow());
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setTranslucentStatusBarLollipop(Window window) {
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.transparent));
    }
}
