package com.fanzine.coys.activities.base;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.fragments.chat.ChatFragment;
import com.fanzine.coys.fragments.mails.EmailFragment;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.models.mails.MailContent;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.chat.ChatSDK;
import com.fanzine.chat.interfaces.FCChannelsListener;
import com.fanzine.chat.models.channels.FCChannel;
import com.fanzine.chat3.model.pojo.Message;
import com.fanzine.chat3.view.activity.messages.MessagesListActivity;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;
import java.util.Map;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static com.fanzine.coys.services.MyFirebaseMessagingService.*;

/**
 * Created by Woland on 21.06.2017.
 */

@Deprecated
public abstract class NotificationActivity extends BaseActivity {

    public static final String NOTIFICATION = "notification";
    public static final String NOTIFICATION_ID = "id";

    private CompositeSubscription subscription = new CompositeSubscription();

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (intent.getExtras() != null) {
            RemoteMessage remoteMessage = getIntent().getParcelableExtra(NOTIFICATION);
            handle(remoteMessage);
        }
    }

    private void handle(RemoteMessage remoteMessage) {
        String topic = remoteMessage.getFrom();

        if (topic.contains(Types.CHAT_NEW_MESSAGE)) {

            Message message = buildMessagePojo(remoteMessage.getData());

            showNewMessageNotification(message);
        }
    }

    private Message buildMessagePojo(Map<String, String> data) {
            String text = data.get(MessageType.TEXT);
            String userId = data.get(MessageType.USER_ID);
            String type = data.get(MessageType.RELATION_TYPE);

            Message message = new Message(text, userId, type);
            message.setUserName(data.get(MessageType.USER_NAME));
            message.setChannelId(Long.valueOf(data.get(MessageType.CHAT_ID)));

            return message;
    }

    private void showNewMessageNotification(Message message) {
        Intent intent = new Intent(this, MessagesListActivity.class);
        intent.putExtra(MessagesListActivity.CHANNEL_ID, message.getChannelId());
        intent.setAction(MessagesListActivity.ACTION_NOTIFICATION);

        startActivity(intent);
    }

    private void openEmail(String folder, int id) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            Subscriber<MailContent> subscriber = new Subscriber<MailContent>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(MailContent mail) {
                    pd.dismiss();


                    FragmentUtils.changeFragment(NotificationActivity.this, R.id.content_frame, EmailFragment.newInstance(mail), true);
                }
            };
            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getMailContent(folder, id)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    private void openMatch(String id) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            Subscriber<Match> subscriber = new Subscriber<Match>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(Match match) {
                    pd.dismiss();


                }
            };
            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getMatch(Integer.getInteger(id))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    private void openChat(String uid) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ChatSDK.getInstance().getChatManager().getChannels(new FCChannelsListener() {
                @Override
                public void onChannelsReceived(List<FCChannel> list) {
                    for (FCChannel channel : list) {
                        if (channel.getUid().equals(uid)) {
                            FragmentUtils.changeFragment((AppCompatActivity) getContext(), R.id.content_frame, ChatFragment.newInstance(channel, false), true);
                            break;
                        }
                    }
                }

                @Override
                public void onError(Exception e) {
                    e.printStackTrace();
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                }
            });
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
