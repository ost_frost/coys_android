package com.fanzine.coys.activities;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.fanzine.coys.R;
import com.fanzine.coys.fragments.start.TeaserFragment;
import com.fanzine.coys.models.Fullname;
import com.fanzine.coys.utils.FragmentUtils;

/**
 * Created by mbp on 4/4/18.
 */

public class TeaserAcitivity extends AppCompatActivity {

    public static final String KEY_FULLNAME = "key_fullname";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_teaser);
        setTranslucentStatusBarLollipop(getWindow());

        Fullname fullname = getIntent().getParcelableExtra(KEY_FULLNAME);

        TeaserFragment teaserFragment = TeaserFragment.newInstance(fullname, false);
        FragmentUtils.changeFragment(getSupportFragmentManager(), R.id.content, teaserFragment, false);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setTranslucentStatusBarLollipop(Window window) {
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.transparent));
    }
}
