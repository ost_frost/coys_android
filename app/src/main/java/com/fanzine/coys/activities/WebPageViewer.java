package com.fanzine.coys.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.ActivityWebpageViewerBinding;

/**
 * Created by mbp on 2/1/18.
 */

public class WebPageViewer extends FragmentActivity {

    public final static String URL = "url";

    private WebView webView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityWebpageViewerBinding binding =
                DataBindingUtil.setContentView(this, R.layout.activity_webpage_viewer);

        String url = getIntent().getStringExtra(URL);

        webView =  binding.myWebView;
        webView.loadUrl(url);

        webView.setWebViewClient(new WebViewClient());

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }
}
