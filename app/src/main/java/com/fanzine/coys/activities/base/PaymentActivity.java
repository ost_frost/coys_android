package com.fanzine.coys.activities.base;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.fanzine.coys.App;
import com.fanzine.coys.models.PurchaseDto;
import com.google.firebase.crash.FirebaseCrash;

import org.solovyev.android.checkout.ActivityCheckout;
import org.solovyev.android.checkout.Checkout;
import org.solovyev.android.checkout.Inventory;
import org.solovyev.android.checkout.Purchase;
import org.solovyev.android.checkout.RequestListener;
import org.solovyev.android.checkout.Sku;

import javax.annotation.Nonnull;

import static org.solovyev.android.checkout.ProductTypes.SUBSCRIPTION;

/**
 * Created by mbp on 4/13/18.
 */

public abstract class PaymentActivity extends BaseActivity implements Inventory.Callback {

    public static final int REQUEST_TEASER_EMAIL = 4513;
    static final int DEFAULT_CHECKOUT_REQUEST_CODE = 0XCAFE;


    protected static final String SKU_SUBSCRIPTION = "1.0_productionsubscription";

    protected ActivityCheckout mCheckout;

    protected boolean isPurchasingFlowOpened = false;


    public abstract void successPayment(Purchase purchase);

    public abstract void failedPayment(int codeError);

    ProgressDialog mPd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPd = new ProgressDialog(this);
        mPd.setMessage("Processing..");
        mPd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mPd.setCancelable(false);
    }

    public void startPurchase() {
        try {
            mPd.show();

            final Inventory.Request request = Inventory.Request.create();
            request.loadPurchases(SUBSCRIPTION);
            request.loadSkus(SUBSCRIPTION, SKU_SUBSCRIPTION);

            mCheckout.loadInventory(request, this);
        } catch (Exception e) {
            FirebaseCrash.report(e);
            FirebaseCrash.logcat(Log.DEBUG, " startPurchase()", e.getMessage());

            mPd.dismiss();

            failedPayment(-1);
        }
    }

    public void startCheckout() {
        mCheckout = Checkout.forActivity(this, App.get().getBilling());
        mCheckout.start();
    }

    public PurchaseDto buildPurchaseDto(Purchase purchase) {
        PurchaseDto purchaseDto = new PurchaseDto();
        purchaseDto.orderId = purchase.orderId;
        purchaseDto.packegeName = purchase.packageName;
        purchaseDto.signature = purchase.signature;
        purchaseDto.state = purchase.state.id;
        purchaseDto.time = purchase.time;
        purchaseDto.token = purchase.token;
        purchaseDto.productId = SKU_SUBSCRIPTION;

        return purchaseDto;
    }

    @Override
    public void onLoaded(@Nonnull Inventory.Products products) {
        final Inventory.Product product = products.get(SUBSCRIPTION);

        Sku subscriptionSku = product.getSku(SKU_SUBSCRIPTION);

        mPd.dismiss();

        try {
            if (isPurchasingFlowOpened) {
                mCheckout.destroyPurchaseFlow();
            }

            isPurchasingFlowOpened = true;


            mCheckout.startPurchaseFlow(subscriptionSku, null, new RequestListener<Purchase>() {
                @Override
                public void onSuccess(@Nonnull Purchase result) {
                    successPayment(result);

                    FirebaseCrash.report(new Exception("PURCHASE SUCCESS" + result.toJson()));
                }

                @Override
                public void onError(int response, @Nonnull Exception e) {
                    FirebaseCrash.report(e);
                    FirebaseCrash.report(new Exception("PURCHASE ERROR CODE=" + response));

                    failedPayment(response);
                }
            });
        } catch (Exception e) {
            failedPayment(-1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == DEFAULT_CHECKOUT_REQUEST_CODE)
            mCheckout.onActivityResult(requestCode, resultCode, data);
        else
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        if (mCheckout != null) mCheckout.stop();

        super.onDestroy();
    }
}
