package com.fanzine.coys.activities.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.MainActivity;
import com.fanzine.coys.adapters.NavigationAdapter;
import com.fanzine.coys.fragments.base.BaseLeaguesBarFragment;
import com.fanzine.coys.fragments.news.FeedBaseFragment;
import com.fanzine.coys.fragments.news.GossipFragment;
import com.fanzine.coys.fragments.profile.ProfileFragment;
import com.fanzine.coys.fragments.table.BaseTableFragment;
import com.fanzine.coys.fragments.team.TeamFragment;
import com.fanzine.coys.fragments.venue.VenueFragment;
import com.fanzine.coys.fragments.video.ListVideoFragment;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.utils.RecyclerItemClickListener;
import com.fanzine.chat3.view.fragment.dialogs.DialogListFragment;

import static com.fanzine.coys.adapters.NavigationAdapter.GOSSIP;
import static com.fanzine.coys.adapters.NavigationAdapter.MATCH;
import static com.fanzine.coys.adapters.NavigationAdapter.NEWS;
import static com.fanzine.coys.adapters.NavigationAdapter.TABLE;
import static com.fanzine.coys.adapters.NavigationAdapter.TEAM;
import static com.fanzine.coys.adapters.NavigationAdapter.VIDEO;

//todo: refactor
//todo: duplicated code with MainActivity
public class NavigationFragmentActivity extends AppCompatActivity {

    private FrameLayout view_stub; //This is the framelayout to keep your content view
    private RecyclerView navigation_view; // The new navigation view from Android Design Library. Can inflate menu resources. Easy
    private DrawerLayout mDrawerLayout;

    public static final String BOTTOM_MENU_OPEN_NAVIGATION = "BOTTOM_MENU_OPEN_NAVIGATION";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_main);// The base layout that contains your navigation drawer.
        view_stub = (FrameLayout) findViewById(R.id.content_frame);
        navigation_view = (RecyclerView) findViewById(R.id.nav_view);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        navigation_view.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        navigation_view.setAdapter(new NavigationAdapter(this));
        navigation_view.addOnItemTouchListener(new RecyclerItemClickListener(this, (view, position) -> {
            if ( mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }

            ((NavigationAdapter) navigation_view.getAdapter()).setSelected(position);

            switch (position) {
                case NEWS:
                    FragmentUtils.changeFragment(this, R.id.content_frame, FeedBaseFragment.newInstance(), false);
                    break;

                case MATCH:
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.setAction(MainActivity.OPEN_MATCHES);

                    startActivity(intent);
                    break;

                case TEAM:
                    FragmentUtils.changeFragment(this, R.id.content_frame, TeamFragment.newInstance(), false);
                    break;

                case VIDEO:
                    FragmentUtils.changeFragment(this, R.id.content_frame, ListVideoFragment.newInstance(), false);
                    break;
                case TABLE:
                    BaseLeaguesBarFragment.setLeagueId(BaseLeaguesBarFragment.LEAGUE_ONE_PLAY_OFFS);
                    FragmentUtils.changeFragment(this, R.id.content_frame,  BaseTableFragment.newInstance(BaseTableFragment.PRIMEIRA_LIGA), false);
                    break;

//                case STATS:
//                    FragmentUtils.changeFragment(this, R.id.content_frame, SheetGoalFragment.newInstance(), false);
//                    break;

                case GOSSIP:
                    FragmentUtils.changeFragment(this, R.id.content_frame, GossipFragment.newInstance(), false);
                    break;

            }
        }));
    }


    @Override
    public void setContentView(int layoutResID) {
        if (view_stub != null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            View stubView = inflater.inflate(layoutResID, view_stub, false);
            view_stub.addView(stubView, lp);
        }
    }

    @Override
    public void setContentView(View view) {
        if (view_stub != null) {
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            view_stub.addView(view, lp);
        }
    }


    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        if (view_stub != null) {
            view_stub.addView(view, params);
        }
    }


    public void openProfile() {
        open(ProfileFragment.newInstance());
    }

    public void openDrawer() {
        mDrawerLayout.openDrawer(GravityCompat.START);
    }

    public void openVenues() {
        Intent startVenues = new Intent(this, VenueFragment.class);
        startVenues.setAction(VenueFragment.ACTION_LATLNG_REQUEST);

        startActivity(startVenues);
    }

    public void openEmails() {
//        open(ListEmailActivity.newInstance(null));
    }

    public void open(Fragment fragment) {
        if (isOpenedNotTheSameFragment(fragment)) {
                FragmentUtils.changeFragment(this, R.id.content_frame, fragment, false);
        }
    }

    private boolean isOpenedNotTheSameFragment(Fragment fragment) {
        return FragmentUtils.getCurrent(this) != null & FragmentUtils.getCurrent(this).getClass() != fragment.getClass();
    }

    public void openChat() {
        open(DialogListFragment.newInstance());
    }
}
