package com.fanzine.coys.activities;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.Window;
import android.view.WindowManager;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.base.PaymentActivity;
import com.fanzine.coys.fragments.start.RegistrationFragment;
import com.fanzine.coys.fragments.start.StartFragment;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.coys.utils.FragmentUtils;

import org.solovyev.android.checkout.Purchase;

public class StartActivity extends PaymentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (SharedPrefs.getUserToken() != null && !TextUtils.isEmpty(SharedPrefs.getFIRToken())) {
            startActivity(MainActivity.getStartIntent(this));
            finish();
        } else {
            DataBindingUtil.setContentView(this, R.layout.activity_start);
            FragmentUtils.changeFragment(this, R.id.content_frame, StartFragment.newInstance(), false);

            setTranslucentStatusBar(getWindow());
        }

        startCheckout();
    }


    @Override
    public void successPayment(Purchase purchase) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(FragmentUtils.FRAGMENT_TAG);
        if (fragment != null) {
            Intent intent = new Intent();
            intent.putExtra(RegistrationFragment.PURCHASE, buildPurchaseDto(purchase));

            fragment.onActivityResult(RegistrationFragment.PURCHASE_REQUEST, RegistrationFragment.PURCHASE_SUCCESS, intent);

            mCheckout.destroyPurchaseFlow();
        }
    }

    @Override
    public void failedPayment(int codeError) {
        Fragment fragment = (RegistrationFragment) getSupportFragmentManager().findFragmentByTag(FragmentUtils.FRAGMENT_TAG);

        Intent intent = new Intent();
        intent.putExtra(RegistrationFragment.PURCHASE_ERROR_CODE, codeError);

        fragment.onActivityResult(RegistrationFragment.PURCHASE_REQUEST, RegistrationFragment.PURCHASE_ERROR, intent);

        mCheckout.destroyPurchaseFlow();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TEASER_EMAIL) {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(FragmentUtils.FRAGMENT_TAG);
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setTranslucentStatusBarLollipop(Window window) {
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.transparent));
    }

    public void setTranslucentStatusBar(Window window) {
        if (window != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setTranslucentStatusBarLollipop(window);
        }
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context, StartActivity.class);
    }
}
