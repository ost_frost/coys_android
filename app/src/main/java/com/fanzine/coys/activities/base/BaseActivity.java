package com.fanzine.coys.activities.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.fanzine.coys.viewmodels.base.BaseViewModel;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Woland on 05.01.2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    protected BaseViewModel baseViewModel;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    @Override
    public void onDestroy() {
        super.onDestroy();

        if (baseViewModel != null)
            baseViewModel.destroy();
    }

    protected void setBaseViewModel(BaseViewModel model) {
        this.baseViewModel = model;
    }

    public Context getContext() {
        return this;
    }
}
