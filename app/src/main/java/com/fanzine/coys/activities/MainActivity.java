package com.fanzine.coys.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.base.PaymentActivity;
import com.fanzine.coys.adapters.NavigationAdapter;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.databinding.ActivityMainBinding;
import com.fanzine.coys.fragments.base.BaseLeaguesBarFragment;
import com.fanzine.coys.fragments.mails.MailHomeFragment;
import com.fanzine.coys.fragments.match.MatchesFragment;
import com.fanzine.coys.fragments.news.FeedBaseFragment;
import com.fanzine.coys.fragments.news.GossipFragment;
import com.fanzine.coys.fragments.profile.ProfileFragment;
import com.fanzine.coys.fragments.table.BaseTableFragment;
import com.fanzine.coys.fragments.team.TeamFragment;
import com.fanzine.coys.fragments.venue.VenueFragment;
import com.fanzine.coys.fragments.video.ListVideoFragment;
import com.fanzine.coys.models.login.UserToken;
import com.fanzine.coys.models.payment.PaymentToken;
import com.fanzine.coys.models.profile.Notifications;
import com.fanzine.coys.services.MyFirebaseMessagingService;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.utils.RecyclerItemClickListener;
import com.fanzine.coys.viewmodels.activities.MainActivityViewModel;
import com.fanzine.chat.ChatSDK;
import com.fanzine.chat.interfaces.auth.FCAuthorizationListener;
import com.fanzine.chat.models.user.FCUser;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ChatUser;
import com.fanzine.chat3.view.fragment.dialogs.DialogListFragment;
import com.fanzine.chat3.view.fragment.group.AddParticipantFragment;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.messaging.FirebaseMessaging;

import org.solovyev.android.checkout.Purchase;

import java.util.List;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.adapters.NavigationAdapter.GOSSIP;
import static com.fanzine.coys.adapters.NavigationAdapter.MATCH;
import static com.fanzine.coys.adapters.NavigationAdapter.NEWS;
import static com.fanzine.coys.adapters.NavigationAdapter.TABLE;
import static com.fanzine.coys.adapters.NavigationAdapter.TEAM;
import static com.fanzine.coys.adapters.NavigationAdapter.VIDEO;

public class MainActivity extends PaymentActivity {

    public static final String OPEN_CHAT_FRAGMENT = "open_chat_fragment";
    public static final String CHANT_CHANNEL = "chat_channel";
    public static final String CHAT_GROUP_PARTICIPANTS_LIST = "chat_group_participants_list";
    public static final String OPEN_GROUP_INFO_ADD_PARTICIPANTS = "open_group_info_add_participants";
    public static final String OPEN_MATCHES = "open_matches";

    public static final String BOTTOM_MENU_OPEN_NAVIGATION = "BOTTOM_MENU_OPEN_NAVIGATION";
    public static final String BOTTOM_MENU_OPEN_SETTINGS = "BOTTOM_MENU_OPEN_SETTINGS";
    public static final String BOTTOM_MENU_OPEN_VENUES = "BOTTOM_MENU_OPEN_VENUES";
    public static final String BOTTOM_MENU_OPEN_CHAT = "BOTTOM_MENU_OPEN_CHAT";
    public static final String BOTTOM_MENU_OPEN_EMAIL = "BOTTOM_MENU_OPEN_EMAIL";


    private ActivityMainBinding binding;
    private GoogleApiClient mGoogleApiClient;
    private MainActivityViewModel viewModel;

    private static final int REQUEST_CODE = 1121;


    public static Intent getStartIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        viewModel = new MainActivityViewModel(this);
        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);

        binding.navView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        binding.navView.setAdapter(new NavigationAdapter(this));
        binding.navView.addOnItemTouchListener(new RecyclerItemClickListener(this, (view, position) -> {
            if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
                binding.drawerLayout.closeDrawer(GravityCompat.START);
            }


            ((NavigationAdapter) binding.navView.getAdapter()).setSelected(position);

            switch (position) {
                case NEWS:
                    FragmentUtils.changeFragment(this, R.id.content_frame, FeedBaseFragment.newInstance(), false);
                    break;

                case MATCH:
                    FragmentUtils.changeFragment(this, R.id.content_frame, MatchesFragment.newInstance(), false);
                    break;

                case TEAM:
                    FragmentUtils.changeFragment(this, R.id.content_frame, TeamFragment.newInstance(), false);
                    break;

                case VIDEO:
                    Log.i(getClass().getName(), "VIDEO FRAGMENT CHANGE");
                    FragmentUtils.changeFragment(this, R.id.content_frame, ListVideoFragment.newInstance(), false);
                    break;
                case TABLE:
                    BaseLeaguesBarFragment.setLeagueId(BaseLeaguesBarFragment.LEAGUE_ONE_PLAY_OFFS);
                    FragmentUtils.changeFragment(this, R.id.content_frame, BaseTableFragment.newInstance(BaseTableFragment.PRIMEIRA_LIGA), false);
                    break;

//                case STATS:
//                    FragmentUtils.changeFragment(this, R.id.content_frame, SheetGoalFragment.newInstance(), false);
//                    break;

                case GOSSIP:
                    FragmentUtils.changeFragment(this, R.id.content_frame, GossipFragment.newInstance(), false);
                    break;

            }
        }));

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .build();

        if (FirebaseAuth.getInstance().getCurrentUser() != null)
            authorize();
        else {
            FirebaseAuth.getInstance().signInWithCustomToken(SharedPrefs.getFIRToken()).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    authorize();
                }
            });
        }

        sendToken();
        FragmentUtils.changeFragment(this, R.id.content_frame, FeedBaseFragment.newInstance(), false);

        UserToken userToken = SharedPrefs.getUserToken();
        subscribeOnNotifications(userToken);

        if (getIntent().getAction() != null) {

            switch (getIntent().getAction()) {
                case BOTTOM_MENU_OPEN_CHAT: {
                    openChat();
                    break;
                }
                case BOTTOM_MENU_OPEN_EMAIL: {
                    openEmails();
                    break;
                }
                case BOTTOM_MENU_OPEN_NAVIGATION: {
                    openDrawer();
                    break;
                }
                case BOTTOM_MENU_OPEN_VENUES: {
                    openVenues();
                    break;
                }
                case BOTTOM_MENU_OPEN_SETTINGS: {
                    openProfile();
                    break;
                }
                case OPEN_CHAT_FRAGMENT: {
                    FragmentUtils.changeFragment(this, R.id.content_frame, DialogListFragment.newInstance(),
                            false);
                    break;
                }
                case OPEN_GROUP_INFO_ADD_PARTICIPANTS: {
                    Channel channel = getIntent().getParcelableExtra(CHANT_CHANNEL);

                    List<ChatUser> chatUserList = getIntent().getParcelableArrayListExtra(CHAT_GROUP_PARTICIPANTS_LIST);

                    FragmentUtils.changeFragment(this, R.id.content_frame,
                            AddParticipantFragment.newInstance(channel, chatUserList),
                            false);
                    break;
                }
                case OPEN_MATCHES: {
                    FragmentUtils.changeFragment(this, R.id.content_frame, MatchesFragment.newInstance(), false);
                }
            }
        }
    }


    //region Upgrade from free to Pro
    public void upgrade(String email) {
        viewModel.upgrade(email, this::startPurchase);
    }

    @Override
    public void successPayment(Purchase purchase) {
       viewModel.enableProAccount(buildPurchaseDto(purchase));
    }

    @Override
    public void failedPayment(int codeError) {
        DialogUtils.showErrorDialog(this, "Payment error");
    }
    //endregion

    private void subscribeOnNotifications(UserToken userToken) {
        String topic = MyFirebaseMessagingService.Types.MATCH_END_TEAMS + userToken.getUserId();
        FirebaseMessaging.getInstance().subscribeToTopic(MyFirebaseMessagingService.Types.NEWS);
        FirebaseMessaging.getInstance().subscribeToTopic(topic);

        Notifications matchNotification = new Notifications();
        matchNotification.setTeamId(8150);
        matchNotification.setFullTime(true);
        matchNotification.setGoals(true);
        matchNotification.setKickOf(true);
        matchNotification.setLineUp(true);
        matchNotification.setRedCards(true);


        ApiRequest.getInstance().getApi().setTeamsNotification(matchNotification)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    private void sendToken() {
        if (!SharedPrefs.isSent() && !TextUtils.isEmpty(SharedPrefs.getToken())) {
            if (NetworkUtils.isNetworkAvailable(this)) {
                Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(ResponseBody o) {
                        SharedPrefs.setIsSent();
                    }
                };

                Observable<ResponseBody> observable = ApiRequest.getInstance().getApi().sendDeviceToken(SharedPrefs.getToken());
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(subscriber);
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    private void authorize() {
        ChatSDK.getInstance().authorize(FirebaseAuth.getInstance().getCurrentUser(), new FCAuthorizationListener() {
            @Override
            public void onError(Exception e) {
                int i = 1;
            }

            @Override
            public void onAuthorized(FCUser fcUser, boolean isNewUser) {
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void openDrawer() {
        binding.drawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    public void openProfile() {
        open(ProfileFragment.newInstance());
    }

    public void openVenues() {
        Intent startVenues = new Intent(this, VenueFragment.class);
        startVenues.setAction(VenueFragment.ACTION_LATLNG_REQUEST);

        startActivity(startVenues);
    }

    public void openEmails() {
        viewModel.openMails(MailHomeFragment.newInstance());
    }

    public void open(Fragment fragment) {
        FragmentUtils.changeFragment(this, R.id.content_frame, fragment, false);
    }

    public void openChat() {
        open(DialogListFragment.newInstance());
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Deprecated
    public void showPayment(PaymentToken paymentToken, String email) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode,data);
        if (requestCode == REQUEST_TEASER_EMAIL && resultCode == RESULT_OK) {


        } else if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                // use the result to update your UI and send the payment method nonce to your server
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // the user canceled
                DialogUtils.showAlertDialog(getContext(), R.string.paymentCanceled);
            } else {
                // handle errors here, an exception may be available in
                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                DialogUtils.showAlertDialog(getContext(), error.getMessage());
            }
        }
    }
}
