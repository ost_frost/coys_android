package com.fanzine.coys.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mbp on 12/13/17.
 */

public class MatchChannel {

    @SerializedName("name")
    private String name;
    @SerializedName("link")
    private String link;
    @SerializedName("description")
    private String description;
    @SerializedName("icon")
    private String icon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
