package com.fanzine.coys.models.table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by a on 13.12.17
 */
public class Bands {

    @SerializedName("promotion_quantity")
    @Expose
    private Integer promotionQuantity;
    @SerializedName("below_promotion_quality")
    @Expose
    private Integer belowPromotionQuality;
    @SerializedName("above_relegation_quantity")
    @Expose
    private Integer aboveRelegationQuantity;
    @SerializedName("relegation_quantity")
    @Expose
    private Integer relegationQuantity;
    @SerializedName("promotion_color")
    @Expose
    private String promotionColor;
    @SerializedName("below_promotion_color")
    @Expose
    private String belowPromotionColor;
    @SerializedName("above_relegation_color")
    @Expose
    private String aboveRelegationColor;
    @SerializedName("relegation_color")
    @Expose
    private String relegationColor;

    public Integer getPromotionQuantity() {
        return promotionQuantity;
    }

    public void setPromotionQuantity(Integer promotionQuantity) {
        this.promotionQuantity = promotionQuantity;
    }

    public Integer getBelowPromotionQuality() {
        return belowPromotionQuality;
    }

    public void setBelowPromotionQuality(Integer belowPromotionQuality) {
        this.belowPromotionQuality = belowPromotionQuality;
    }

    public Integer getAboveRelegationQuantity() {
        return aboveRelegationQuantity;
    }

    public void setAboveRelegationQuantity(Integer aboveRelegationQuantity) {
        this.aboveRelegationQuantity = aboveRelegationQuantity;
    }

    public Integer getRelegationQuantity() {
        return relegationQuantity;
    }

    public void setRelegationQuantity(Integer relegationQuantity) {
        this.relegationQuantity = relegationQuantity;
    }

    public String getPromotionColor() {
        return promotionColor;
    }

    public void setPromotionColor(String promotionColor) {
        this.promotionColor = promotionColor;
    }

    public String getBelowPromotionColor() {
        return belowPromotionColor;
    }

    public void setBelowPromotionColor(String belowPromotionColor) {
        this.belowPromotionColor = belowPromotionColor;
    }

    public String getAboveRelegationColor() {
        return aboveRelegationColor;
    }

    public void setAboveRelegationColor(String aboveRelegationColor) {
        this.aboveRelegationColor = aboveRelegationColor;
    }

    public String getRelegationColor() {
        return relegationColor;
    }

    public void setRelegationColor(String relegationColor) {
        this.relegationColor = relegationColor;
    }

}
