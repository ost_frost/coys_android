package com.fanzine.coys.models;

import java.util.Date;

/**
 * Created by Siarhei on 13.01.2017.
 */

public class DayInfo {
    int dayOfMonth;
    String nameDay;
    String nameMonth;
    Date date;

    public DayInfo(Date date, int dayOfMonth, String nameDay, String nameMonth) {
        this.date = date;
        this.dayOfMonth = dayOfMonth;
        this.nameDay = nameDay;
        this.nameMonth = nameMonth;
    }

    public int getDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(int dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    public String getNameDay() {
        return nameDay;
    }

    public void setNameDay(String nameDay) {
        this.nameDay = nameDay;
    }

    public String getNameMonth() {
        return nameMonth;
    }

    public void setNameMonth(String nameMonth) {
        this.nameMonth = nameMonth;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
