package com.fanzine.coys.models.mails;

import com.google.gson.annotations.SerializedName;

/**
 * Created by a on 27.03.18
 */

public class MailAttachment {

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("filePath")
    public String filePath;

    @SerializedName("mimeType")
    public String mimeType;
}
