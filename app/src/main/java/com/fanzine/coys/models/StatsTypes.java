package com.fanzine.coys.models;

/**
 * Created by maximdrobonoh on 12.10.17.
 */

public interface StatsTypes {
    String PIE_CHART = "Possesion time";
    String SHOATS_TOTAL = "Shots total";
    String SHOATS_ON_GOAL = "Shots on goal";
    String CORNERS = "Corners";
    String OFFSIDES = "Offsides";
    String YELLOW_CARDS = "Yellow cards";
    String RED_CARDS = "Red cards";
}
