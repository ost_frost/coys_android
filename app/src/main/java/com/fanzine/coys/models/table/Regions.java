package com.fanzine.coys.models.table;

import com.fanzine.coys.models.League;

import java.util.List;
import java.util.Map;

/**
 * Created by mbp on 11/22/17.
 */

public class Regions {

    private Map<String, List<League>> entries;

    public Map<String, List<League>> getEntries() {
        return entries;
    }

    public void setEntries(Map<String, List<League>> entries) {
        this.entries = entries;
    }
}
