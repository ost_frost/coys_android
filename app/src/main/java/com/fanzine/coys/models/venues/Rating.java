package com.fanzine.coys.models.venues;

/**
 * Created by mbp on 12/26/17.
 */

public enum Rating {
    UP, DOWN
}
