package com.fanzine.coys.models.mails;

import android.annotation.SuppressLint;

import com.fanzine.coys.models.Person;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Evgenij Krasilnikov on 16-Feb-18.
 */

public class NewMail {

    private List<Person> to;
    private List<Person> cc;
    private List<Person> bcc;
    private String subject;
    public String body;
    private List<Attachment> attachments;

    public NewMail(List<Person> to, List<Person> cc, List<Person> bcc, String subject, String body, List<Attachment> attachments) {
        this.to = to;
        this.cc = cc;
        this.bcc = bcc;
        this.subject = subject;
        this.body = body;
        this.attachments = attachments;
    }

    public HashMap<String, RequestBody> getHeaderPart() {
        HashMap<String, RequestBody> part = new HashMap<>();
        part.putAll(addFieldToPart("to", to));
        part.putAll(addFieldToPart("cc", cc));
        part.putAll(addFieldToPart("bcc", bcc));
        part.put("subject", requestBodyFromString(subject));
        part.put("body", requestBodyFromString(body));
        return part;
    }

    @SuppressLint("DefaultLocale")
    public List<MultipartBody.Part> getAttachmentsPart() {
        List<MultipartBody.Part> part = new ArrayList<>();
//        for (Attachment attachment : attachments) {
//            RequestBody requestFile = MultipartBody.create(MediaType.parse("multipart/form-data"), attachment.getFile());
//
//            MultipartBody.Part formData = MultipartBody.Part.createFormData("attachments", attachment.getName(), requestFile);
//            part.add(formData);
//        }
        for (int i = 0; i < attachments.size(); i++) {
            Attachment attachment = attachments.get(i);
            RequestBody requestFile = MultipartBody.create(MediaType.parse("multipart/form-data"), attachment.getFile());
            MultipartBody.Part formData = MultipartBody.Part.createFormData(String.format("attachments[%1$d]", i), attachment.getName(), requestFile);
            part.add(formData);
        }
        return part;
    }

    @SuppressLint("DefaultLocale")
    private HashMap<String, RequestBody> addFieldToPart(String field, List<Person> list) {
        HashMap<String, RequestBody> hashMap = new HashMap<>();
        for (int i = 0; i < list.size(); i++) {
            hashMap.put(String.format("%1$s[%2$d]", field, i), RequestBody.create(MultipartBody.FORM, list.get(i).getEmail()));
        }
        return hashMap;
    }

    private RequestBody requestBodyFromString(String part) {
        return  RequestBody.create(MediaType.parse("text/plain"), part != null ? part : "");
    }
}
