package com.fanzine.coys.models;

/**
 * Created by mbp on 3/23/18.
 */

public interface SocialMedia {

    boolean isPinned();

    boolean isLiked();
}
