package com.fanzine.coys.models.lineups;

/**
 * Created by mbp on 11/24/17.
 */

public class PlayerСell {

    private float width;
    private float height;

    public PlayerСell(float width, float height) {
        this.width = width;
        this.height = height;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }
}
