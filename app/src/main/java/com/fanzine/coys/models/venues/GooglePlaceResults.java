package com.fanzine.coys.models.venues;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mbp on 2/3/18.
 */

public class GooglePlaceResults {

    @SerializedName("results")
    private List<Result> resultList;

    public List<Result> getResultList() {
        return resultList;
    }

    public class Result {

        @SerializedName("place_id")
        private String placeId;

        public String getPlaceId() {
            return placeId;
        }
    }
}
