package com.fanzine.coys.models.team;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mbp on 1/11/18.
 */

public class PlayerStatGroup {

    @SerializedName("name")
    private String name;

    @SerializedName("value")
    private long value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
