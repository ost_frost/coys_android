package com.fanzine.coys.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Siarhei on 03.02.2017.
 */

public class Stats {
    @SerializedName("id")
    private int id;

    @SerializedName("title_stats")
    private String titleStats;

    @SerializedName("home_value")
    private int homeValue;

    @SerializedName("guest_value")
    private int guestValue;

    public int getId() {
        return id;
    }

    public String getTitleStats() {
        if (!titleStats.equals("Possesion time"))
            return titleStats;
        else
            return "Possesion (%)";
    }

    public void setTitleStats(String titleStats) {
        this.titleStats = titleStats;
    }

    public int getHomeValue() {
        return homeValue;
    }

    public void setHomeValue(int homeValue) {
        this.homeValue = homeValue;
    }

    public int getGuestValue() {
        return guestValue;
    }

    public void setGuestValue(int guestValue) {
        this.guestValue = guestValue;
    }

    public String getHomeValuePorcent() {
        return homeValue + "%";
    }

    public String getGuestValuePorcent() {
        return guestValue + "%";
    }

    public boolean compareTitles(String otherTitle) {
        if (this.titleStats == otherTitle) {
            return true;
        }
        int n = titleStats.length();
        if (n == otherTitle.length()) {
            char v1[] = titleStats.toCharArray();
            char v2[] = otherTitle.toCharArray();
            int i = 0;
            int j = 0;
            while (n-- != 0) {
                if (v1[i++] != v2[j++])
                    return false;
            }
            return true;
        }

        return false;
    }
}
