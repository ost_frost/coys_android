package com.fanzine.coys.models.analysis;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by maximdrobonoh on 05.10.17.
 */

public class Analysis {

    @SerializedName("match_prediction")
    private Prediction prediction;

    @SerializedName("recent_games")
    private RecentGames recentGames;

    @SerializedName("past_games")
    private List<PastGame> pastGames;

    public Prediction getPrediction() {
        return prediction;
    }

    public void setPrediction(Prediction prediction) {
        this.prediction = prediction;
    }

    public RecentGames getRecentGames() {
        return recentGames;
    }

    public void setRecentGames(RecentGames recentGames) {
        this.recentGames = recentGames;
    }

    public List<PastGame> getPastGames() {
        return pastGames;
    }

    public void setPastGames(List<PastGame> pastGames) {
        this.pastGames = pastGames;
    }
}
