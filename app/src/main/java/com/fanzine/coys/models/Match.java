package com.fanzine.coys.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.fanzine.coys.utils.TimeDateUtils;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Siarhei on 02.02.2017.
 */

public class Match implements Parcelable {

    public static final String MATCH = "ligueMatches";

    public static final int LAST = 1;
    public static final int LIVE = 2;
    public static final int NEXT = 3;
    public static final int POSTPONED = 4;

    @DatabaseField(columnName = "id", id = true)
    @SerializedName("id")
    private int matchId;

    @SerializedName("current_time_match")
    @DatabaseField(columnName = "currentTimeMatch")
    private String currentTimeMatch;

    @SerializedName("time_begin")
    @DatabaseField(columnName = "timeBegin")
    private String timeBegin;

    @SerializedName("league_id")
    @DatabaseField(columnName = "league_id")
    private int league_id;

    @SerializedName("state")
    @DatabaseField(columnName = "state", canBeNull = true)
    private int state;

    @SerializedName("total")
    @DatabaseField(columnName = "total", canBeNull = true)
    private String total;

    @SerializedName("datetime")
    private String datetime;

    @SerializedName("date")
    @DatabaseField(columnName = "date")
    private String date;

    @SerializedName("venue")
    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "venue_id")
    private Venue venue;

    @SerializedName("home_team")
    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "home_team_id")
    private Team homeTeam;

    @SerializedName("guest_team")
    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "guest_team_id")
    private Team guestTeam;

    @SerializedName("channels")
    private List<MatchChannel> matchChannel;

    @SerializedName("alt_name")
    private String altName;

    @SerializedName("stream_links")
    private List<String> streamLinks;

    @SerializedName("has_notification")
    private boolean hasNotification;

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getGuestTeam() {
        return guestTeam;
    }

    public void setGuestTeam(Team guestTeam) {
        this.guestTeam = guestTeam;
    }

    public int getId() {
        return matchId;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCurrentTimeMatch() {
        return currentTimeMatch;
    }

    public void setCurrentTimeMatch(String currentTimeMatch) {
        this.currentTimeMatch = currentTimeMatch;
    }

    public String getTimeBegin() {
        return TimeDateUtils.convertUtcToLocalTime(timeBegin);
    }

    public String getTimeBeginWithoutSeconds() {
        return timeBegin.substring(0, 5);
    }

    public void setTimeBegin(String timeBegin) {
        this.timeBegin = timeBegin;
    }

    public int getLeague_id() {
        return league_id;
    }

    public void setLeague_id(int league_id) {
        this.league_id = league_id;
    }

    public void setId(int id) {
        this.matchId = id;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setHasNotification(boolean hasNotification) {
        this.hasNotification = hasNotification;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public String getAltName() {
        return altName;
    }

    public void setAltName(String altName) {
        this.altName = altName;
    }

    public boolean isHasNotification() {
        return hasNotification;
    }

    public String getTotalFormatted() {
        char[] total = this.total.toCharArray();

        if (total.length == 3) {
            return total[0] + " - " + total[2];
        }
        return this.total;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.matchId);
        dest.writeString(this.currentTimeMatch);
        dest.writeString(this.timeBegin);
        dest.writeInt(this.league_id);
        dest.writeInt(this.state);
        dest.writeString(this.total);
        dest.writeParcelable(this.venue, flags);
        dest.writeParcelable(this.homeTeam, flags);
        dest.writeParcelable(this.guestTeam, flags);
    }

    public Match() {
    }

    protected Match(Parcel in) {
        this.matchId = in.readInt();
        this.currentTimeMatch = in.readString();
        this.timeBegin = in.readString();
        this.league_id = in.readInt();
        this.state = in.readInt();
        this.total = in.readString();
        this.venue = in.readParcelable(Venue.class.getClassLoader());
        this.homeTeam = in.readParcelable(Team.class.getClassLoader());
        this.guestTeam = in.readParcelable(Team.class.getClassLoader());
    }

    public static final Creator<Match> CREATOR = new Creator<Match>() {
        @Override
        public Match createFromParcel(Parcel source) {
            return new Match(source);
        }

        @Override
        public Match[] newArray(int size) {
            return new Match[size];
        }
    };

    public boolean isTeamsExist() {
        return getGuestTeam() != null && getHomeTeam() != null;
    }

    public List<MatchChannel> getMatchChannel() {
        return matchChannel;
    }

    public void setMatchChannel(List<MatchChannel> matchChannel) {
        this.matchChannel = matchChannel;
    }

    public String getTimeBeginWithLocaleTime() {

        try {
            SimpleDateFormat sourceFormat = new SimpleDateFormat("HH:mm:ss");
            Date date = sourceFormat.parse( timeBegin );

            int utcOffset = TimeZone.getDefault().getOffset(System.currentTimeMillis());
            long now = date.getTime() + utcOffset;

            DateTime parsed = new DateTime(now);
            String dateFormat = "HH:mm";

            DateTimeFormatter outputFormatter = DateTimeFormat.forPattern(dateFormat)
                    .withZone(DateTimeZone.getDefault());

            Log.i(getClass().getName(), "DateTimeZone.getDefault() =" + DateTimeZone.getDefault());

            return outputFormatter.print(parsed);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
