package com.fanzine.coys.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fanzine.coys.models.analysis.RecentGames;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbp on 11/16/17.
 */

public class Team implements Parcelable {

    @DatabaseField(generatedId =  true)
    private transient int id;

    @SerializedName("id")
    @DatabaseField(columnName = "team_id")
    private int teamId;

    @SerializedName("icon")
    @DatabaseField(columnName = "icon")
    private String icon;

    @SerializedName("name")
    @DatabaseField(columnName = "name")
    private String name;

    @SerializedName("short_name")
    private String shortName;

    @SerializedName("recent_games")
    private List<RecentGames> recentGames;


    public int getId() {
        return teamId;
    }

    public void setId(int id) {
        this.id = teamId;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RecentGames> getRecentGames() {
        return recentGames;
    }

    public void setRecentGames(List<RecentGames> recentGames) {
        this.recentGames = recentGames;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.icon);
        dest.writeString(this.name);
        dest.writeList(this.recentGames);
    }

    public Team() {
    }

    protected Team(Parcel in) {
        this.id = in.readInt();
        this.icon = in.readString();
        this.name = in.readString();
        this.recentGames = new ArrayList<RecentGames>();
        in.readList(this.recentGames, RecentGames.class.getClassLoader());
    }

    public static final Creator<Team> CREATOR = new Creator<Team>() {
        @Override
        public Team createFromParcel(Parcel source) {
            return new Team(source);
        }

        @Override
        public Team[] newArray(int size) {
            return new Team[size];
        }
    };
}
