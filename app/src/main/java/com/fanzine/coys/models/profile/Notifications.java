
package com.fanzine.coys.models.profile;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notifications implements Parcelable {

    @SerializedName("kick_of")
    @Expose
    private boolean kickOf;
    @SerializedName("half_time")
    @Expose
    private boolean halfTime;
    @SerializedName("full_time")
    @Expose
    private boolean fullTime;
    @SerializedName("goals")
    @Expose
    private boolean goals;
    @SerializedName("red_cards")
    @Expose
    private boolean redCards;
    @SerializedName("line_up")
    private boolean lineUp;
    @SerializedName("penalty")
    private boolean penalty;
    @SerializedName("league_id")
    private int leagueId;
    @SerializedName("team_id")
    private int teamId;

    public boolean isKickOf() {
        return kickOf;
    }

    public void setKickOf(boolean kickOf) {
        this.kickOf = kickOf;
    }

    public boolean isHalfTime() {
        return halfTime;
    }

    public void setHalfTime(boolean halfTime) {
        this.halfTime = halfTime;
    }

    public boolean isFullTime() {
        return fullTime;
    }

    public void setFullTime(boolean fullTime) {
        this.fullTime = fullTime;
    }

    public boolean isGoals() {
        return goals;
    }

    public void setGoals(boolean goals) {
        this.goals = goals;
    }

    public boolean isRedCards() {
        return redCards;
    }

    public void setRedCards(boolean redCards) {
        this.redCards = redCards;
    }

    public int getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(int leagueId) {
        this.leagueId = leagueId;
    }

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public boolean isLineUp() {
        return lineUp;
    }

    public void setLineUp(boolean lineUp) {
        this.lineUp = lineUp;
    }

    public boolean isPenalty() {
        return penalty;
    }

    public void setPenalty(boolean penalty) {
        this.penalty = penalty;
    }

    public boolean isNotificationsActive() {
        return kickOf || halfTime || fullTime || goals || redCards || lineUp || penalty;
    }

    public boolean isAllChecked(){
        return kickOf && halfTime && fullTime && goals && redCards && lineUp && penalty;
    }

    @Override
    public String toString() {
        return "Notifications{" +
                "kickOf=" + kickOf +
                ", halfTime=" + halfTime +
                ", fullTime=" + fullTime +
                ", goals=" + goals +
                ", redCards=" + redCards +
                ", leagueId=" + leagueId +
                ", teamId=" + teamId +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.kickOf ? (byte) 1 : (byte) 0);
        dest.writeByte(this.halfTime ? (byte) 1 : (byte) 0);
        dest.writeByte(this.fullTime ? (byte) 1 : (byte) 0);
        dest.writeByte(this.goals ? (byte) 1 : (byte) 0);
        dest.writeByte(this.redCards ? (byte) 1 : (byte) 0);
        dest.writeByte(this.lineUp ? (byte) 1 : (byte) 0);
        dest.writeByte(this.penalty ? (byte) 1 : (byte) 0);
        dest.writeInt(this.leagueId);
        dest.writeInt(this.teamId);
    }

    public Notifications() {
    }

    protected Notifications(Parcel in) {
        this.kickOf = in.readByte() != 0;
        this.halfTime = in.readByte() != 0;
        this.fullTime = in.readByte() != 0;
        this.goals = in.readByte() != 0;
        this.redCards = in.readByte() != 0;
        this.lineUp = in.readByte() != 0;
        this.penalty = in.readByte() != 0;
        this.leagueId = in.readInt();
        this.teamId = in.readInt();
    }

    public static final Creator<Notifications> CREATOR = new Creator<Notifications>() {
        @Override
        public Notifications createFromParcel(Parcel source) {
            return new Notifications(source);
        }

        @Override
        public Notifications[] newArray(int size) {
            return new Notifications[size];
        }
    };
}
