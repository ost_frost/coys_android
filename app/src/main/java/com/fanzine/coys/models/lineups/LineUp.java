
package com.fanzine.coys.models.lineups;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LineUp {

    @SerializedName("localteam")
    @Expose
    private LocalTeam localteam;
    @SerializedName("visitorteam")
    @Expose
    private VisitorTeam visitorteam;

    public LocalTeam getLocalteam() {
        return localteam;
    }

    public void setLocalteam(LocalTeam localteam) {
        this.localteam = localteam;
    }

    public VisitorTeam getVisitorteam() {
        return visitorteam;
    }

    public void setVisitorteam(VisitorTeam visitorteam) {
        this.visitorteam = visitorteam;
    }

    public boolean hasLocalSubstitution() {
        return localteam.getSubstitution().size() > 0;
    }

    public boolean hasVisitorSubstitution() {
         return visitorteam.getSidelined().size() > 0;
    }

    public boolean isLocalSquadExist() {
        return localteam.getSquad() != null;
    }

    public boolean isVisitorSquadExist() {
        return localteam.getSquad() != null;
    }

}
