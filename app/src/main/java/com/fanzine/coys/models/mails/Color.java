package com.fanzine.coys.models.mails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by maximdrobonoh on 14.09.17.
 */


public class Color implements Parcelable {

    @SerializedName("id")
    @DatabaseField(columnName = "id", id = true)
    private int id;

    @SerializedName("hex")
    @DatabaseField(columnName = "hex")
    private String hex;

    @SerializedName("name")
    @DatabaseField(columnName = "name")
    private String name;

    public Color() {
    }

    private Color(Parcel in) {
        id = in.readInt();
        hex = in.readString();
        name = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(hex);
        parcel.writeString(name);
    }

    public static final Creator<Color> CREATOR = new Creator<Color>() {
        @Override
        public Color createFromParcel(Parcel in) {
            return new Color(in);
        }

        @Override
        public Color[] newArray(int size) {
            return new Color[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHex() {
        return hex;
    }

    public void setHex(String hex) {
        this.hex = hex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Creator<Color> getCREATOR() {
        return CREATOR;
    }
}
