package com.fanzine.coys.models.profile;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vitaliygerasymchuk on 2/10/18
 */

public class ChatNotificationState {

    @SerializedName("chat")
    public int chat = 0;

    public ChatNotificationState(int chat) {
        this.chat = chat;
    }

    public int getChat() {
        return chat;
    }
}
