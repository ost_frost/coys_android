package com.fanzine.coys.models.analysis;

import com.google.gson.annotations.SerializedName;

/**
 * Created by maximdrobonoh on 06.10.17.
 */

public class PastGame {

    @SerializedName("date")
    private String date;

    @SerializedName("competition")
    private String competition;

    @SerializedName("home")
    private String home;

    @SerializedName("away")
    private String away;

    @SerializedName("result")
    private String result;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCompetition() {
        return competition;
    }

    public void setCompetition(String competition) {
        this.competition = competition;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getAway() {
        return away;
    }

    public void setAway(String away) {
        this.away = away;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
