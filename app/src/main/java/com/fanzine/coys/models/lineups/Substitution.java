
package com.fanzine.coys.models.lineups;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Substitution {

    @SerializedName("off_name")
    @Expose
    private String offName;
    @SerializedName("on_name")
    @Expose
    private String onName;
    @SerializedName("off_icon")
    @Expose
    private String offIcon;
    @SerializedName("on_icon")
    @Expose
    private String onIcon;
    @SerializedName("on_player_number")
    @Expose
    private Integer onPlayerNumber;
    @SerializedName("off_player_number")
    @Expose
    private Integer offPlayerNumber;
    @SerializedName("minute")
    @Expose
    private String minute;

    @SerializedName("on_goals")
    private int  onGoals;

    @SerializedName("on_yellowcards")
    private int onYellowCards;

    @SerializedName("on_redcards")
    private int onRedcards;


    public String getOffName() {
        return offName;
    }

    public void setOffName(String offName) {
        this.offName = offName;
    }

    public String getOnName() {
        return onName;
    }

    public void setOnName(String onName) {
        this.onName = onName;
    }

    public String getOffIcon() {
        return offIcon;
    }

    public void setOffIcon(String offIcon) {
        this.offIcon = offIcon;
    }

    public String getOnIcon() {
        return onIcon;
    }

    public void setOnIcon(String onIcon) {
        this.onIcon = onIcon;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public Integer getOnPlayerNumber() {
        return onPlayerNumber;
    }

    public void setOnPlayerNumber(int onPlayerNumber) {
        this.onPlayerNumber = onPlayerNumber;
    }

    public Integer getOffPlayerNumber() {
        return offPlayerNumber;
    }

    public void setOffPlayerNumber(int offPlayerNumber) {
        this.offPlayerNumber = offPlayerNumber;
    }

    public String getLastName() {
        String[] name = this.onName.split("\\s+");

        if (name.length == 2) {
            return String.valueOf(name[1]);
        }
        if (name.length == 3) {
            return String.valueOf(name[2]);
        }
        return this.onName;
    }

    public void setOnPlayerNumber(Integer onPlayerNumber) {
        this.onPlayerNumber = onPlayerNumber;
    }

    public void setOffPlayerNumber(Integer offPlayerNumber) {
        this.offPlayerNumber = offPlayerNumber;
    }

    public int getOnGoals() {
        return onGoals;
    }

    public void setOnGoals(int onGoals) {
        this.onGoals = onGoals;
    }

    public int getOnYellowCards() {
        return onYellowCards;
    }

    public void setOnYellowCards(int onYellowCards) {
        this.onYellowCards = onYellowCards;
    }

    public int getOnRedcards() {
        return onRedcards;
    }

    public void setOnRedcards(int onRedcards) {
        this.onRedcards = onRedcards;
    }
}
