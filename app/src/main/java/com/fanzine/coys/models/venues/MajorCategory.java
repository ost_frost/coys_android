package com.fanzine.coys.models.venues;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mbp on 1/31/18.
 */

public class MajorCategory {

    @SerializedName("major_category")
    private String majorCategory;

    public String getMajorCategory() {
        return majorCategory;
    }
}
