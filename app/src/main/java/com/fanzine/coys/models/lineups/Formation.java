package com.fanzine.coys.models.lineups;

/**
 * Created by mbp on 11/24/17.
 */

public interface Formation {

    String THREE_FOUR_THREE = "3-4-3";
    String THREE_ONE_FOUR_TWO = "3-1-4-2";
    String THREE_FOUR_TWO_ONE = "3-4-2-1";
    String THREE_FOUR_ONE_TWO = "3-4-1-2";
    String THREE_FIVE_ONE_ONE = "3-5-1-1";
    String THREE_FIVE_TWO = "3-5-2";
    String FOUR_ONE_FOUR_ONE = "4-1-4-1";
    String FOUR_TWO_THREE_ONE = "4-2-3-1";
    String FOUR_TWO_TWO_TWO = "4-2-2-2";
    String FOUR_THREE_THREE = "4-3-3";
    String FOUR_THREE_ONE_TWO = "4-3-1-2";
    String FOUR_FOUR_ONE_ONE = "4-4-1-1";
    String FOUR_FOUR_TWO = "4-4-2";
    String FIVE_TREE_TWO = "5-3-2";
    String FOUR_FIVE_ONE = "4-5-1";
    String FIVE_FOUR_ONE = "5-4-1";
    String FOUR_THREE_TWO_ONE = "4-3-2-1";


}
