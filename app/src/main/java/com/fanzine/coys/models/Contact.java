package com.fanzine.coys.models;

import android.net.Uri;
import android.text.TextUtils;

/**
 * Created by Siarhei on 31.01.2017.
 */

public class Contact {

    private String id;
    private Uri photoUri;
    private String name;
    private String phone;

    public Contact() {
    }

    public Contact(String id, String name, String phone, String photoUri) {
        this.id = id;
        this.name = name;
        if (!TextUtils.isEmpty(photoUri))
            this.photoUri = Uri.parse(photoUri);
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public String getPhoneCutted() {
        return phone.replace("+", "").replaceAll("-", "").replaceAll(" ", "");
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Uri getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(Uri photoUri) {
        this.photoUri = photoUri;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
