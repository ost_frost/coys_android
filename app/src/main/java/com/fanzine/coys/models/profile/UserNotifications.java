
package com.fanzine.coys.models.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserNotifications {

    @SerializedName("profile_notifications")
    @Expose
    private UserNotifications profileNotifications;

    public UserNotifications getProfileNotifications() {
        return profileNotifications;
    }

    public void setProfileNotifications(UserNotifications profileNotifications) {
        this.profileNotifications = profileNotifications;
    }

}
