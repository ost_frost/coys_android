package com.fanzine.coys.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fanzine.coys.utils.TimeDateUtils;
import com.google.gson.annotations.SerializedName;


/**
 * Created by user on 07.11.2017.
 */

public class Social implements Parcelable {

    @SerializedName("news_id")
    private int newsId;

    @SerializedName("title")
    private String title;

    @SerializedName("author")
    private String author;

    @SerializedName("author_name")
    private String author_name;

    @SerializedName("author_photo")
    private String authorPhoto;

    @SerializedName("socialNetwork")
    private String socialNetwork;

    @SerializedName("image")
    private String image;

    @SerializedName("image_w")
    private int image_w;

    @SerializedName("image_h")
    private int image_h;

    @SerializedName("datetime")
    private String dateTime;

    @SerializedName("commentsCount")
    private int commentsCount;

    @SerializedName("likesCount")
    private int likesCount;

    @SerializedName("sharesCount")
    private int sharesCount;

    @SerializedName("content")
    private String content;

    @SerializedName("url")
    private String url;

    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public String getAuthor_photo() {
        return authorPhoto;
    }

    public void setAuthor_photo(String author_photo) {
        this.authorPhoto = author_photo;
    }

    public String getSocialNetwork() {
        return socialNetwork;
    }

    public void setSocialNetwork(String socialNetwork) {
        this.socialNetwork = socialNetwork;
    }

    public int getImage_w() {
        return image_w;
    }

    public void setImage_w(int image_w) {
        this.image_w = image_w;
    }

    public int getImage_h() {
        return image_h;
    }

    public void setImage_h(int image_h) {
        this.image_h = image_h;
    }


    public int getNewsId() {
        return newsId;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAuthorPhoto() {
        return authorPhoto;
    }

    public void setAuthorPhoto(String authorPhoto) {
        this.authorPhoto = authorPhoto;
    }

    public String getDateTime() {
        return TimeDateUtils.getCreatedAgo(dateTime);
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public int getSharesCount() {
        return sharesCount;
    }

    public void setSharesCount(int sharesCount) {
        this.sharesCount = sharesCount;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.newsId);
        dest.writeString(this.title);
        dest.writeString(this.author);
        dest.writeString(this.author_name);
        dest.writeString(this.authorPhoto);
        dest.writeString(this.socialNetwork);
        dest.writeString(this.image);
        dest.writeInt(this.image_w);
        dest.writeInt(this.image_h);
        dest.writeString(this.dateTime);
        dest.writeInt(this.commentsCount);
        dest.writeInt(this.likesCount);
        dest.writeInt(this.sharesCount);
        dest.writeString(this.content);
        dest.writeString(this.url);
    }

    public Social() {
    }

    protected Social(Parcel in) {
        this.newsId = in.readInt();
        this.title = in.readString();
        this.author = in.readString();
        this.author_name = in.readString();
        this.authorPhoto = in.readString();
        this.socialNetwork = in.readString();
        this.image = in.readString();
        this.image_w = in.readInt();
        this.image_h = in.readInt();
        this.dateTime = in.readString();
        this.commentsCount = in.readInt();
        this.likesCount = in.readInt();
        this.sharesCount = in.readInt();
        this.content = in.readString();
        this.url = in.readString();
    }

    public static final Parcelable.Creator<Social> CREATOR = new Parcelable.Creator<Social>() {
        @Override
        public Social createFromParcel(Parcel source) {
            return new Social(source);
        }

        @Override
        public Social[] newArray(int size) {
            return new Social[size];
        }
    };
}
