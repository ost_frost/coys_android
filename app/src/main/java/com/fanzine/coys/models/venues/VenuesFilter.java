
package com.fanzine.coys.models.venues;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.fanzine.coys.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class VenuesFilter {

    public final static String TRANSIT = "food";
    public final static String TRAVEL_HOTELS = "hotelstravel";
    public final static String BARS = "nightlife";
    public final static String RESTAURANTS = "restaurants";

    private static List<Category> categories;

    public static void parseCategories(Context context) {
        if (categories == null) {
            InputStream is = context.getResources().openRawResource(R.raw.venues_filters);
            Writer writer = new StringWriter();
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            categories = new Gson().fromJson(writer.toString(), new TypeToken<List<Category>>() {
            }.getType());
        }
    }

    public static List<Category> getCategories() {
        return categories;
    }

    public static Set<Category> getSubCategories(String parentCategory) {
        if (categories != null) {
            for (Category category : categories) {
                if (category.getAlias().equals(parentCategory))
                    return new HashSet<>(category.getCategories());
            }
        }
        return new HashSet<>();
    }
}
