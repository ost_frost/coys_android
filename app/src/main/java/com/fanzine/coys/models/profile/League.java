
package com.fanzine.coys.models.profile;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class League implements Parcelable, Comparable<League> {

    @SerializedName("league_id")
    @Expose
    private int leagueId;
    @SerializedName("sort")
    @Expose
    private int sort;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("teams")
    @Expose
    private List<Team> teams = new ArrayList<>();

    public int getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(int leagueId) {
        this.leagueId = leagueId;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.leagueId);
        dest.writeInt(this.sort);
        dest.writeString(this.name);
        dest.writeString(this.icon);
        dest.writeTypedList(this.teams);
    }

    public League() {
    }

    protected League(Parcel in) {
        this.leagueId = in.readInt();
        this.sort = in.readInt();
        this.name = in.readString();
        this.icon = in.readString();
        this.teams = in.createTypedArrayList(Team.CREATOR);
    }

    public static final Creator<League> CREATOR = new Creator<League>() {
        @Override
        public League createFromParcel(Parcel source) {
            return new League(source);
        }

        @Override
        public League[] newArray(int size) {
            return new League[size];
        }
    };

    @Override
    public int compareTo(@NonNull League o) {
        if (this.getSort() < o.getSort()) {
            return -1;
        } else {
            return 1;
        }
    }
}
