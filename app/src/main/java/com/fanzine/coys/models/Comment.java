package com.fanzine.coys.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Woland on 20.01.2017.
 */

public class Comment implements Parcelable {
    protected Comment(Parcel in) {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel in) {
            return new Comment(in);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };
}
