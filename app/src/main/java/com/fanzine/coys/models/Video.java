package com.fanzine.coys.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.fanzine.coys.utils.NumberFormatter;
import com.fanzine.coys.utils.TimeDateUtils;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mbp on 1/8/18.
 */

public class Video implements Parcelable, SocialMedia {

    @SerializedName("id")
    private long id;

    @SerializedName("url")
    private String url;

    @SerializedName("preview_image")
    private String previewImage;

    @SerializedName("title")
    private String title;

    @SerializedName("likes_count")
    private long likesCount;

    @SerializedName("views_count")
    private long viewCount;

    @SerializedName("is_liked")
    public boolean isLiked;

    @SerializedName("duration")
    private String duration;

    @SerializedName("channel_title")
    private String channelTitle;

    @SerializedName("published_at")
    private PublishedAt publishedAt;

    public long getId() {
        return id;
    }

    @NonNull
    public String getUrl() {
        return validString(url);
    }

    @NonNull
    public String getPreviewImage() {
        return validString(previewImage);
    }

    @NonNull
    public String getTitle() {
        return validString(title).replaceAll("\n", "");
    }


    @NonNull
    public String getDuration() {
        return validString(duration);
    }

    @NonNull
    public String getChannelTitle() {
        return validString(channelTitle);
    }

    public String getLikesCount() {
        return NumberFormatter.formatNumber(likesCount);
    }

    @NonNull
    public String getViewCount() {
        return NumberFormatter.formatNumber(viewCount);
    }

    @NonNull
    public String getPublishedAgo() {
        if (publishedAt != null) {
            return TimeDateUtils.getCreatedAgo(publishedAt.getDate()).replaceAll("ago", "");
        }
        return "";
    }

    public void setLikesCount(long likesCount) {
        this.likesCount = likesCount;
    }

    public class PublishedAt implements Parcelable {

        @SerializedName("date")
        private String date;

        @SerializedName("timezone_type")
        private String timezoneType;

        @SerializedName("timezone")
        private String timezone;

        public String getDate() {
            return date;
        }

        public String getTimezoneType() {
            return timezoneType;
        }

        public String getTimezone() {
            return timezone;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.date);
            dest.writeString(this.timezoneType);
            dest.writeString(this.timezone);
        }

        public PublishedAt() {
        }

        protected PublishedAt(Parcel in) {
            this.date = in.readString();
            this.timezoneType = in.readString();
            this.timezone = in.readString();
        }

        public final Creator<PublishedAt> CREATOR = new Creator<PublishedAt>() {
            @Override
            public PublishedAt createFromParcel(Parcel source) {
                return new PublishedAt(source);
            }

            @Override
            public PublishedAt[] newArray(int size) {
                return new PublishedAt[size];
            }
        };
    }

    @NonNull
    private String validString(@Nullable String str) {
        if (TextUtils.isEmpty(str)) return "";
        return str;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Video video = (Video) o;

        return id == video.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Video{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", previewImage='" + previewImage + '\'' +
                ", title='" + title + '\'' +
                ", likesCount=" + likesCount +
                ", viewCount=" + viewCount +
                ", isLiked='" + isLiked + '\'' +
                ", duration='" + duration + '\'' +
                ", channelTitle='" + channelTitle + '\'' +
                ", publishedAt=" + publishedAt +
                '}';
    }

    public Video() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.url);
        dest.writeString(this.previewImage);
        dest.writeString(this.title);
        dest.writeLong(this.likesCount);
        dest.writeLong(this.viewCount);
        dest.writeByte(this.isLiked ? (byte) 1 : (byte) 0);
        dest.writeString(this.duration);
        dest.writeString(this.channelTitle);
        dest.writeParcelable(this.publishedAt, flags);
    }

    protected Video(Parcel in) {
        this.id = in.readLong();
        this.url = in.readString();
        this.previewImage = in.readString();
        this.title = in.readString();
        this.likesCount = in.readLong();
        this.viewCount = in.readLong();
        this.isLiked = in.readByte() != 0;
        this.duration = in.readString();
        this.channelTitle = in.readString();
        this.publishedAt = in.readParcelable(PublishedAt.class.getClassLoader());
    }

    public static final Creator<Video> CREATOR = new Creator<Video>() {
        @Override
        public Video createFromParcel(Parcel source) {
            return new Video(source);
        }

        @Override
        public Video[] newArray(int size) {
            return new Video[size];
        }
    };

    @Override
    public boolean isPinned() {
        return false;
    }

    @Override
    public boolean isLiked() {
        return isLiked;
    }
}
