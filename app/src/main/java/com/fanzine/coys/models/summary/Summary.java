package com.fanzine.coys.models.summary;

import com.fanzine.coys.models.lineups.LocalTeam;
import com.fanzine.coys.models.lineups.VisitorTeam;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Siarhei on 10.02.2017.
 */

public class Summary {
    @SerializedName("localteam")
    private LocalTeam localTeam;

    @SerializedName("visitorteam")
    private VisitorTeam visitorTeam;

    @SerializedName("events")
    private List<Event> events;

    public LocalTeam getLocalTeam() {
        return localTeam;
    }

    public void setLocalTeam(LocalTeam localTeam) {
        this.localTeam = localTeam;
    }

    public VisitorTeam getVisitorTeam() {
        return visitorTeam;
    }

    public void setVisitorTeam(VisitorTeam visitorTeam) {
        this.visitorTeam = visitorTeam;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }
}
