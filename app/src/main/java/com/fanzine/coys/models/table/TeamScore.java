package com.fanzine.coys.models.table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maximdrobonoh on 29.09.17.
 */

public class TeamScore {

    @Expose
    private int position;

    @Expose
    private int total;

    @SerializedName("team_ico")
    private String teamIcon;

    @SerializedName("team_name")
    private String title;

    public String getShortName() {
        String[] name = this.title.split("\\s+");

        if (name.length == 2) {
            return String.valueOf(name[0].charAt(0)) + ". " + name[1];
        }
        if (name.length == 3) {
            return String.valueOf(name[0].charAt(0)) + ". " + name[2];
        }
        return this.title;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getTeamIcon() {
        return teamIcon;
    }

    public void setTeamIcon(String teamIcon) {
        this.teamIcon = teamIcon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
