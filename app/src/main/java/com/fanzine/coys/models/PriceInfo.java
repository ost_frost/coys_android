package com.fanzine.coys.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 01.03.2018.
 */


public class PriceInfo {
    @SerializedName("status")
    private String status;
    @SerializedName("price")
    private String price;
    @SerializedName("currency")
    private String currency;

    public String getStatus() {
        return status;
    }

    public String getPrice() {
        return price;
    }

    public String getCurrency() {
        return currency;
    }
}
