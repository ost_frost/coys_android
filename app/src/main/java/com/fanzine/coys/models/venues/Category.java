
package com.fanzine.coys.models.venues;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Category implements Comparable<Category>,Parcelable {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("alias")
    @Expose
    private String alias;
    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public int compareTo(@NonNull Category o) {
        return alias.compareTo(o.getAlias());
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Category(com.yelp.fusion.client.models.Category category) {
        this.alias = category.getAlias();
        this.title = category.getTitle();
        this.setCategories(new ArrayList<>());
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.alias);
        dest.writeList(this.categories);
    }

    public Category() {
    }

    protected Category(Parcel in) {
        this.title = in.readString();
        this.alias = in.readString();
        this.categories = new ArrayList<Category>();
        in.readList(this.categories, Category.class.getClassLoader());
    }

    public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
