package com.fanzine.coys.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mbp on 2/16/18.
 */

public class Currency {
    /*
    "{
   ""status"": ""ok"",
   ""currency"":""GBP""
   ""price"":1.99
   ""payment_token"": ""sagsagasgwe35235v235...""
}
     */

    @SerializedName("status")
    private String status;

    @SerializedName("currency")
    private String currency;

    @SerializedName("price")
    private String price;

    @SerializedName("payment_token")
    private String paymentToken;

    public String getStatus() {
        return status;
    }

    public String getCurrency() {
        return currency;
    }

    public String getPrice() {
        return price;
    }

    public String getPaymentToken() {
        return paymentToken;
    }
}
