package com.fanzine.coys.models.venues;

/**
 * Created by mbp on 12/26/17.
 */

public enum Price {

    LEVEL_1(1),
    LEVEL_2(2),
    LEVEL_3(3),
    LEVEL_4(4);

    private final int id;
    Price(int id) { this.id = id; }
    public int getValue() { return id; }
}
