package com.fanzine.coys.models.table;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mbp on 11/14/17.
 */
public class Standings {

    @SerializedName("standings")
    private List<TeamTable> teams;

    @SerializedName("bands")
    private Bands bands;

    public List<TeamTable> getTeams() {
        return teams;
    }

    public void setTeams(List<TeamTable> teams) {
        this.teams = teams;
    }

    public Bands getBands() {
        return bands;
    }

    public void setBands(Bands bands) {
        this.bands = bands;
    }
}
