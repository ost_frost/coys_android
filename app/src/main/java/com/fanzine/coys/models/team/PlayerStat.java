package com.fanzine.coys.models.team;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mbp on 1/11/18.
 */

public class PlayerStat implements Comparable<PlayerStat>, Parcelable {

    @SerializedName("name")
    private String name;

    @SerializedName("value")
    private String value;

    @SerializedName("order")
    private int order;

    public String getName() {
        return name;
    }

    @NonNull
    public StatName getStatName() {
        return StatName.ofValue(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    @NonNull
    public String getValue() {
        return value;
    }

    public void setValue(@Nullable String value) {
        this.value = value;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int value) {
        this.order = value;
    }

    @Override
    public int compareTo(PlayerStat playerStat) {

        if (this.getOrder() > playerStat.getOrder()) {
            return 1;
        } else if (this.getOrder() < playerStat.getOrder()) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PlayerStat that = (PlayerStat) o;

        if (order != that.order) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + order;
        return result;
    }



    public enum StatName {

        Goals("Goals"),
        Saves("Saves"),
        Assists("Assists"),
        Appearances("Appearances"),
        MinutesPlayed("Minutes Played");

        private final String val;

        StatName(@NonNull String val) {
            this.val = val;
        }

        @NonNull
        public static StatName ofValue(@NonNull String val) {
            if (TextUtils.equals(val, "Appearances")) {
                return StatName.Appearances;
            } else if (TextUtils.equals(val, "Goals")) {
                return StatName.Goals;
            } else if (TextUtils.equals("Assists", val)) {
                return StatName.Assists;
            } else return StatName.MinutesPlayed;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.value);
        dest.writeInt(this.order);
    }

    public PlayerStat() {
    }

    protected PlayerStat(Parcel in) {
        this.name = in.readString();
        this.value = in.readString();
        this.order = in.readInt();
    }

    public static final Creator<PlayerStat> CREATOR = new Creator<PlayerStat>() {
        @Override
        public PlayerStat createFromParcel(Parcel source) {
            return new PlayerStat(source);
        }

        @Override
        public PlayerStat[] newArray(int size) {
            return new PlayerStat[size];
        }
    };

    @Override
    public String toString() {
        return "PlayerStat{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", order=" + order +
                '}';
    }
}
