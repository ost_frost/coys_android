package com.fanzine.coys.models.venues;

/**
 * Created by mbp on 2/5/18.
 */

public class VenueSearchItem {

    private String title;

    private String alias;

    public VenueSearchItem(String title) {
        this.title = title;
    }

    public VenueSearchItem(String title, String alias) {
        this.title = title;
        this.alias = alias;
    }

    public String getTitle() {
        return title;
    }

    public String getAlias() {
        return alias;
    }
}
