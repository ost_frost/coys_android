package com.fanzine.coys.models.venues;

import android.graphics.drawable.Drawable;

import com.fanzine.coys.models.CarouselFilter;

/**
 * Created by mbp on 12/6/17.
 */

public class VenueFilter  implements CarouselFilter{

    private String title;
    private Drawable icon;
    private int iconRes;
    private String allies;

    public VenueFilter(String title, String allies, int icon) {
        this.title = title;
        this.iconRes = icon;
        this.allies = allies;
    }

    public String getTitle() {
        return title;
    }

    public String getAlias() {
        return allies;
    }

    public int getIcon() {
        return iconRes;
    }
}
