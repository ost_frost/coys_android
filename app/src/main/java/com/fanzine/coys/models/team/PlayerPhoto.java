package com.fanzine.coys.models.team;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mbp on 1/12/18.
 */

public class PlayerPhoto implements Parcelable {

    @SerializedName("url")
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.url);
    }

    public PlayerPhoto() {
    }

    protected PlayerPhoto(Parcel in) {
        this.url = in.readString();
    }

    public static final Parcelable.Creator<PlayerPhoto> CREATOR = new Parcelable.Creator<PlayerPhoto>() {
        @Override
        public PlayerPhoto createFromParcel(Parcel source) {
            return new PlayerPhoto(source);
        }

        @Override
        public PlayerPhoto[] newArray(int size) {
            return new PlayerPhoto[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PlayerPhoto that = (PlayerPhoto) o;

        return url != null ? url.equals(that.url) : that.url == null;
    }

    @Override
    public int hashCode() {
        return url != null ? url.hashCode() : 0;
    }
}
