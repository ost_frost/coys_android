package com.fanzine.coys.models.table;

/**
 * Created by Woland on 22.02.2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

public class PlayerStats {

    @DatabaseField(columnName = "id", generatedId = true)
    private int id;

    @SerializedName(value = "goals")
    @DatabaseField(columnName = "goals")
    private int goals = -1;

    @SerializedName(value = "assists")
    @DatabaseField(columnName = "assists")
    private int assists = -1;

    @SerializedName(value = "redcards")
    @DatabaseField(columnName = "redcards")
    private int redcards = -1;

    @SerializedName(value = "yellowcards")
    @DatabaseField(columnName = "yellowcards")
    private int yellowcards = -1;

    @SerializedName("name")
    @DatabaseField(columnName = "name")
    private String name;

    @SerializedName("sgoals")
    @DatabaseField(columnName = "sgoals")
    private String sgoals;

    @SerializedName("sassists")
    @DatabaseField(columnName = "sassists")
    private String sassists;

    @SerializedName("sred")
    @DatabaseField(columnName = "sred")
    private String sred;

    @SerializedName("syellow")
    @DatabaseField(columnName = "syellow")
    private String syellow;

    @SerializedName("football_api_id")
    @DatabaseField(columnName = "football_api_id")
    private int footballApiId;

    @SerializedName("icon")
    @DatabaseField(columnName = "icon")
    private String icon;


    @SerializedName("team_icon")
    @DatabaseField(columnName = "teamIcon")
    private String teamIcon;

    @SerializedName("team_name")
    private String teamName;

    @Expose
    private int position;

    @Expose
    private int total;

    public String getValue() {
        if (goals != -1)
            return Integer.toString(goals);
        else if (assists != -1)
            return Integer.toString(assists);
        else if (redcards != -1)
            return Integer.toString(redcards);
        else if (yellowcards != -1)
            return Integer.toString(yellowcards);
        else return "";
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getShorTeamName() {
        String[] name = this.teamName.split("\\s+");

        if (name.length == 2 ) {
            return String.valueOf(name[0].substring(0,3)) + ". " +
                    (name[1].length() > 4 ? (name[1].substring(0,3) + ".")  : name[1]);
        }
        if (name.length == 3) {
            return String.valueOf(name[0].charAt(0)) + ". " + name[2];
        }
        return this.teamName;
    }

    public String getShortPlayerName() {
        String[] name = this.name.split("\\s+");

        if (name.length == 2) {
            return String.valueOf(name[0].charAt(0)) + ". " + name[1];
        }
        if (name.length == 3) {
            return String.valueOf(name[0].charAt(0)) + ". " + name[2];
        }
        return this.name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSgoals() {
        return sgoals;
    }

    public void setSgoals(String sgoals) {
        this.sgoals = sgoals;
    }

    public String getSassists() {
        return sassists;
    }

    public void setSassists(String sassists) {
        this.sassists = sassists;
    }

    public String getSred() {
        return sred;
    }

    public void setSred(String sred) {
        this.sred = sred;
    }

    public String getSyellow() {
        return syellow;
    }

    public void setSyellow(String syellow) {
        this.syellow = syellow;
    }

    public int getFootballApiId() {
        return footballApiId;
    }

    public void setFootballApiId(int footballApiId) {
        this.footballApiId = footballApiId;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTeamIcon() {
        return teamIcon;
    }

    public void setTeamIcon(String teamIcon) {
        this.teamIcon = teamIcon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGoals() {
        return goals;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }

    public int getAssists() {
        return assists;
    }

    public void setAssists(int assists) {
        this.assists = assists;
    }

    public int getRedcards() {
        return redcards;
    }

    public void setRedcards(int redcards) {
        this.redcards = redcards;
    }

    public int getYellowcards() {
        return yellowcards;
    }

    public void setYellowcards(int yellowcards) {
        this.yellowcards = yellowcards;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}