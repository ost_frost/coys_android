package com.fanzine.coys.models.payment;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Woland on 12.05.2017.
 */

public class PaymentToken {

    @SerializedName("payment_token")
    private String paymentToken;

    public String getPaymentToken() {
        return paymentToken;
    }
}
