package com.fanzine.coys.models.venues;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by mbp on 12/6/17.
 */

public class Location {

    private static final double DEFAULT_V1 = 51.553;
    private static final double DEFAULT_V2 = -0.10517;

    public static LatLng getDefault() {
        return new LatLng(DEFAULT_V1, DEFAULT_V2);
    }
}
