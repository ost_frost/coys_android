
package com.fanzine.coys.models.mails;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

public class MailContent implements Parcelable {

    @SerializedName("uid")
    @Expose
    private int uid;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("fromName")
    @Expose
    private String fromName;
    @SerializedName("fromAddress")
    @Expose
    private String fromAddress;
    @SerializedName("to")
    @Expose
    private List<String> to = new ArrayList<>();
    @SerializedName("toString")
    @Expose
    private String toString;
    @SerializedName("cc")
    @Expose
    private List<String> cc = new ArrayList<>();
    @SerializedName("replyTo")
    @Expose
    private List<String> replyTo = new ArrayList<>();
    @SerializedName("textHtml")
    @Expose
    private String textHtml;

    @SerializedName("textPlain")
    @Expose
    private String textPlain;

    @SerializedName("udate")
    @Expose
    private long udate;

    @SerializedName("flagged")
    @Expose
    private boolean flagged;

    @SerializedName("deleted")
    @Expose
    private boolean deleted;

    @SerializedName("seen")
    @Expose
    private boolean seen;

    @SerializedName("bcc")
    @Expose
    private List<String> bcc = new ArrayList<>();


    public List<MailAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<MailAttachment> attachments) {
        this.attachments = attachments;
    }

    @SerializedName("attachments")
    @Expose
    private List<MailAttachment> attachments = new ArrayList<>();

    @SerializedName("folder")
    private String folder;

    private DateTime dateTime;

    protected MailContent(Parcel in) {
        uid = in.readInt();
        subject = in.readString();
        fromName = in.readString();
        fromAddress = in.readString();
        toString = in.readString();
        textHtml = in.readString();
        udate = in.readInt();
        flagged = in.readByte() != 0;
        deleted = in.readByte() != 0;
        seen = in.readByte() != 0;
        int size = in.readInt();
        for (int i = 0; i < size; i++) {
            //String key = in.readString();
            //String value = in.readString();
            //to.put(key, value);
            to.add(in.readString());
        }
        size = in.readInt();
        for (int i = 0; i < size; i++) {
            //String key = in.readString();
            //String value = in.readString();
            //replyTo.put(key, value);
            replyTo.add(in.readString());
        }
        cc = in.createStringArrayList();
        bcc = in.createStringArrayList();
        folder = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(uid);
        dest.writeString(subject);
        dest.writeString(fromName);
        dest.writeString(fromAddress);
        dest.writeString(toString);
        dest.writeString(textHtml);
        dest.writeLong(udate);
        dest.writeByte((byte) (flagged ? 1 : 0));
        dest.writeInt((byte) (deleted ? 1 : 0));
        dest.writeInt((byte) (seen ? 1 : 0));
        dest.writeInt(to.size());
//        for (Map.Entry<String, String> entry : to.entrySet()) {
//            dest.writeString(entry.getKey());
//            dest.writeString(entry.getValue());
//        }
        for (String aTo : to) dest.writeString(aTo);
        dest.writeInt(replyTo.size());
//        for (Map.Entry<String, String> entry : replyTo.entrySet()) {
//            dest.writeString(entry.getKey());
//            dest.writeString(entry.getValue());
//        }
        for (String aReplyTo : replyTo) dest.writeString(aReplyTo);
        dest.writeStringList(cc);
        dest.writeStringList(bcc);
        dest.writeString(folder);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MailContent> CREATOR = new Creator<MailContent>() {
        @Override
        public MailContent createFromParcel(Parcel in) {
            return new MailContent(in);
        }

        @Override
        public MailContent[] newArray(int size) {
            return new MailContent[size];
        }
    };

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public List<String> getTo() {
        return to;
    }

    public void setTo(List<String> to) {
        this.to = to;
    }

    public String getToString() {
        return toString;
    }

    public void setToString(String toString) {
        this.toString = toString;
    }

    public List<String> getCc() {
        return cc;
    }

    public void setCc(List<String> cc) {
        this.cc = cc;
    }

    public List<String> getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(List<String> replyTo) {
        this.replyTo = replyTo;
    }

    public String getTextHtml() {
        return textHtml;
    }

    public void setTextHtml(String textHtml) {
        this.textHtml = textHtml;
    }

    public long getUdate() {
        return udate;
    }

    public void setUdate(int udate) {
        this.udate = udate;
    }

    public boolean getFlagged() {
        return flagged;
    }

    public void setFlagged(boolean flagged) {
        this.flagged = flagged;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean getSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public List<String> getBcc() {
        return bcc;
    }

    public void setBcc(List<String> bcc) {
        this.bcc = bcc;
    }

    public DateTime getDateTime() {
        if (dateTime == null)
            dateTime = new DateTime(udate * 1000);

        return dateTime;
    }

    public String getDateFormatted(DateTimeFormatter dateTimeFormatter) {
        if (dateTime == null)
            dateTime = new DateTime(udate * 1000);

        return dateTime.toString(dateTimeFormatter);
    }

    public boolean isReaded() {
        return seen;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public boolean isFlagged() {
        return flagged;
    }

    public void changeFlag() {
        flagged = !flagged;
    }

    public String getTextPlain() {
        return textPlain;
    }

    public void setTextPlain(String textPlain) {
        this.textPlain = textPlain;
    }

    public String getMessageContent() {
        if (!TextUtils.isEmpty(textHtml))
            return textHtml;

        if (!TextUtils.isEmpty(textPlain))
            return textPlain;

        return "";
    }
}
