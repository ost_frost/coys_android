package com.fanzine.coys.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Woland on 14.04.2017.
 */

public class ProEmail {

    @SerializedName("email")
    private String email;
    @SerializedName("cost")
    private int cost;

    @SerializedName("taken")
    private String taken;

    private boolean isSelected;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isTaken() {
        return  taken != null;
    }
}
