package com.fanzine.coys.models.mails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by a on 04.03.18
 */
public class From implements Parcelable {
    @SerializedName("email")
    public String email;
    public String name;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.email);
        dest.writeString(this.name);
    }

    public From() {
    }

    protected From(Parcel in) {
        this.email = in.readString();
        this.name = in.readString();
    }

    public static final Creator<From> CREATOR = new Creator<From>() {
        @Override
        public From createFromParcel(Parcel source) {
            return new From(source);
        }

        @Override
        public From[] newArray(int size) {
            return new From[size];
        }
    };
}
