package com.fanzine.coys.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Woland on 21.02.2017.
 */

public class Gossip {

    private transient int position = 0;

    @SerializedName("text")
    private String text;

    @SerializedName("url")
    private String url;

    @SerializedName("url_name")
    private String urlName;

    @SerializedName("sort")
    private int sort;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlName() {
        return urlName;
    }

    public void setUrlName(String urlName) {
        this.urlName = urlName;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
