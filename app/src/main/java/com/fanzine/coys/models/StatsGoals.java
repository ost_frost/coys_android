package com.fanzine.coys.models;

/**
 * Created by maximdrobonoh on 13.10.17.
 */

public class StatsGoals {

    private Stats shotsOnTarget;
    private Stats shotsOffTarget;

    public StatsGoals(Stats shotsOnTarget, Stats shotsOffTarget) {
        this.shotsOnTarget = shotsOnTarget;
        this.shotsOffTarget = shotsOffTarget;
    }

    public Stats getShotsOnTarget() {
        return shotsOnTarget;
    }

    public void setShotsOnTarget(Stats shotsOnTarget) {
        this.shotsOnTarget = shotsOnTarget;
    }

    public Stats getShotsOffTarget() {
        return shotsOffTarget;
    }

    public void setShotsOffTarget(Stats shotsOffTarget) {
        this.shotsOffTarget = shotsOffTarget;
    }
}
