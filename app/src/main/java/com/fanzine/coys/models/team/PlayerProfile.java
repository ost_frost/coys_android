package com.fanzine.coys.models.team;

import android.os.Parcel;
import android.os.Parcelable;

import com.fanzine.coys.utils.TimeDateUtils;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbp on 1/10/18.
 */

public class PlayerProfile {

    @SerializedName("header")
    private Header header;

    @SerializedName("profile")
    private Profile profile;


    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }


    public class Header {
        @SerializedName("icon")
        public String icon;

        @SerializedName("position")
        public String position;

        @SerializedName("number")
        public int number;

        @SerializedName("full_name")
        public String fullname;
    }

    public class Profile implements Parcelable {
        @SerializedName("is_injured")
        public String isInjured;

        @SerializedName("injure_description")
        public String injureDescription;

        @SerializedName("expected_return")
        public String expectedReturn;

        @SerializedName("profile_image")
        public String profileImage;

        @SerializedName("height")
        public float height;

        @SerializedName("weight")
        public float weight;

        @SerializedName("age")
        public int age;

        @SerializedName("birth_date")
        public Birthday birthday;

        @SerializedName("biography")
        public String biography;

        public String getBiography() {
            return biography;
        }

        @SerializedName("stats")
        public List<PlayerStat> stats = new ArrayList<>();

        @SerializedName("league_name")
        public String leagueName;

        @SerializedName("season")
        public String season;

        public boolean hasExpectedReturn() {
            return expectedReturn != null && expectedReturn.length() > 0;
        }

        public boolean hasBiography() {
            return biography != null && biography.length() > 0;
        }

        public String getYearOfBirth() {
            if (birthday != null) {
                return TimeDateUtils.formatBirthYear(birthday.date);
            }
            return "";
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.isInjured);
            dest.writeString(this.injureDescription);
            dest.writeString(this.expectedReturn);
            dest.writeString(this.profileImage);
            dest.writeString(this.biography);
            dest.writeFloat(this.height);
            dest.writeFloat(this.weight);
            dest.writeInt(this.age);
            dest.writeParcelable(this.birthday, flags);
        }

        public Profile() {
        }

        protected Profile(Parcel in) {
            this.isInjured = in.readString();
            this.injureDescription = in.readString();
            this.expectedReturn = in.readString();
            this.profileImage = in.readString();
            this.biography = in.readString();
            this.height = in.readFloat();
            this.weight = in.readFloat();
            this.age = in.readInt();
            this.birthday = in.readParcelable(Birthday.class.getClassLoader());
        }

        public final Parcelable.Creator<Profile> CREATOR = new Parcelable.Creator<Profile>() {
            @Override
            public Profile createFromParcel(Parcel source) {
                return new Profile(source);
            }

            @Override
            public Profile[] newArray(int size) {
                return new Profile[size];
            }
        };
    }

    public class Birthday implements Parcelable {
        @SerializedName("date")
        public String date;

        @SerializedName("timezone_type")
        public String timezoneType;

        @SerializedName("timezone")
        public String timezone;


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.date);
            dest.writeString(this.timezoneType);
            dest.writeString(this.timezone);
        }

        public Birthday() {
        }

        protected Birthday(Parcel in) {
            this.date = in.readString();
            this.timezoneType = in.readString();
            this.timezone = in.readString();
        }

        public final Parcelable.Creator<Birthday> CREATOR = new Parcelable.Creator<Birthday>() {
            @Override
            public Birthday createFromParcel(Parcel source) {
                return new Birthday(source);
            }

            @Override
            public Birthday[] newArray(int size) {
                return new Birthday[size];
            }
        };
    }

    public class Biography implements Parcelable {


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {

        }

        public Biography() {
        }

        protected Biography(Parcel in) {

        }

        public final Parcelable.Creator<Biography> CREATOR = new Parcelable.Creator<Biography>() {
            @Override
            public Biography createFromParcel(Parcel source) {
                return new Biography(source);
            }

            @Override
            public Biography[] newArray(int size) {
                return new Biography[size];
            }
        };
    }

//   "header": {
//        "icon": "http://gunners.red-carlos.com/not-found/player-404.png",
//                "position": "Midfielder",
//                "number": null,
//                "full_name": "Əfran İsmayılov"
//    },
//            "profile": {
//        "is_injured": false,
//                "injure_description": null,
//                "expected_return": "",
//                "profile_image": "http://gunners.red-carlos.com/not-found/player-404.png",
//                "height": 0,
//                "weight": 0,
//                "age": 29,
//                "birth_date": {
//            "date": "1988-10-08 00:00:00.000000",
//                    "timezone_type": 3,
//                    "timezone": "UTC"
//        },
//        "biography": null,
//                "league_name": "Premier League",
//                "season": "2017/2018",
//                "stats": null
//    }
}
