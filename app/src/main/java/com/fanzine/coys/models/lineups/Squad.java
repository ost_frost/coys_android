
package com.fanzine.coys.models.lineups;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Squad {

    @SerializedName("schema")
    @Expose
    private String schema;
    @SerializedName("goalkeeper")
    @Expose
    private Player goalkeeper;

    @SerializedName("line1")
    @Expose
    private List<Player> line1 = null;

    @SerializedName("line2")
    @Expose
    private List<Player> line2 = null;

    @SerializedName("line3")
    @Expose
    private List<Player> line3 = null;

    @SerializedName("line4")
    @Expose
    private List<Player> line4 = null;

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public Player getGoalkeeper() {
        return goalkeeper;
    }

    public void setGoalkeeper(Player goalkeeper) {
        this.goalkeeper = goalkeeper;
    }

    public List<Player> getLine1() {
        return line1;
    }

    public List<Player> getLine2() {
        return line2;
    }

    public List<Player> getLine3() {
        return line3;
    }

    public List<Player> getLine4() {
        return line4;
    }

    public boolean isPlayersExist() {
        return  line1 != null && line2 != null;
    }

    public int getCountOfLines() {
        int lines = 1;

        if ( line1 != null)
            lines++;

        if ( line2 != null)
            lines++;

        if ( line3 != null)
            lines++;

        if ( line4 != null)
            lines++;

        return lines;
    }
}
