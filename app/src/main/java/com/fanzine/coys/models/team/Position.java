package com.fanzine.coys.models.team;

import com.fanzine.coys.models.CarouselFilter;

/**
 * Created by mbp on 1/9/18.
 */

public class Position implements CarouselFilter {

    private int icon;

    private String name;

    public Position(int icon, String name) {
        this.icon = icon;
        this.name = name;
    }

    @Override
    public String getTitle() {
        return name;
    }

    @Override
    public int getIcon() {
        return icon;
    }
}
