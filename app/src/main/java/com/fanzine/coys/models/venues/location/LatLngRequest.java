package com.fanzine.coys.models.venues.location;

import android.location.Location;
import android.os.Parcel;

import com.fanzine.coys.models.venues.Category;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by mbp on 1/25/18.
 */

public class LatLngRequest extends BaseRequest {

    private LatLng latLng;

    public LatLngRequest(ArrayList<Category> categories, ArrayList<Integer> selectedMoney, LatLng latLng) {
        super(categories, selectedMoney);

        this.latLng = latLng;
    }

    public LatLngRequest(LatLng latLng) {
        super();
        this.latLng = latLng;
    }

    public LatLngRequest() {
        super();
    }

    public static LatLngRequest buildInstance(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        return new LatLngRequest(latLng);
    }

    @Override
    public Map<String, String> mapParams() {
        Map<String, String> params = super.mapParams();

        params.put(Keys.LATITUDE, Double.toString(latLng.latitude));
        params.put(Keys.LONGTITUDE, Double.toString(latLng.longitude));

        if ( term != null) {
            params.put(Keys.TERM, term);
        }
        return params;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    protected LatLngRequest(Parcel in) {
        super(in);
    }

    public static final Creator<LatLngRequest> CREATOR = new Creator<LatLngRequest>() {
        @Override
        public LatLngRequest createFromParcel(Parcel source) {
            return new LatLngRequest(source);
        }

        @Override
        public LatLngRequest[] newArray(int size) {
            return new LatLngRequest[size];
        }
    };
}
