
package com.fanzine.coys.models.mails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Mail implements Parcelable {

    public final static String ID = "id";

    @DatabaseField(columnName = "subject")
    @SerializedName("subject")
    private String subject;

    @SerializedName("from")
    private From from;

    @DatabaseField(columnName = "to")
    @SerializedName("to")
    private String[] to;

    private DateTime dateTime;

    @DatabaseField(columnName = ID, id = true)
    @SerializedName("uid")
    private int uid;

    @DatabaseField(columnName = "flagged")
    @SerializedName("flagged")
    private boolean flagged;


    @DatabaseField(columnName = "has_attachment")
    @SerializedName("has_attachment")
    private boolean hasAttachment;

    @DatabaseField(columnName = "deleted")
    @SerializedName("deleted")
    private boolean deleted;

    @DatabaseField(columnName = "seen")
    @SerializedName("seen")
    private boolean seen;

    @DatabaseField(columnName = "udate")
    @SerializedName("udate")
    private long udate;

    @DatabaseField(columnName = "folder")
    @SerializedName("folder")
    private String folder;

    @SerializedName("description")
    private String description;

    @SerializedName("labels")
    private Collection<Label> labels = new ArrayList<>();

    public Mail() {
    }

    public boolean hasLabel() {
        if (!labels.isEmpty()) {
            return labels.size() > 0;
        }
        return false;
    }

    public Label getFirstLabel() {
        if (labels.size() > 0) {
            return getLabels().get(0);
        }
        return new Label();
    }

    public String getLabelName() {
        if (hasLabel()) {
            return getFirstLabel().getText();
        }
        return "";
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setDateTime(DateTime dateTime) {
        this.dateTime = dateTime;
    }

    public void setFlagged(boolean flagged) {
        this.flagged = flagged;
    }

    public boolean isHasAttachment() {
        return hasAttachment;
    }

    public void setHasAttachment(boolean hasAttachment) {
        this.hasAttachment = hasAttachment;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public void setLabels(Collection<Label> labels) {
        this.labels = labels;
    }

    public String getFrom() {
        return from.email;
    }


    public String getTo() {
        return to[0];
    }

    public void setTo(String to) {
        this.to = new String[]{to};
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getDescription() {
        return description;
    }

    public long getUdate() {
        return udate;
    }

    public void setUdate(long udate) {
        this.udate = udate;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public DateTime getDateTime() {
        if (dateTime == null)
            dateTime = new DateTime(udate * 1000);

        return dateTime;
    }

    public String getDateFormatted(DateTimeFormatter dateTimeFormatter) {
        if (dateTime == null)
            dateTime = new DateTime(udate * 1000);

        return dateTime.toString(dateTimeFormatter);
    }

    public boolean isRead() {
        return seen;
    }

    public boolean isFlagged() {
        return flagged;
    }

    public Collection<Label> labels() {
        return labels;
    }

    public List<Label> getLabels() {
        List<Label> labelsList = new ArrayList<>();

        labelsList.addAll(labels);
        return labelsList;
    }

    public void addLabel(Label label) {
        this.labels.add(label);
    }

    public void setLabels(List<Label> labels) {
        this.labels.addAll(labels);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.subject);
        dest.writeParcelable(this.from, flags);
        dest.writeStringArray(this.to);
        dest.writeSerializable(this.dateTime);
        dest.writeInt(this.uid);
        dest.writeByte(this.flagged ? (byte) 1 : (byte) 0);
        dest.writeByte(this.deleted ? (byte) 1 : (byte) 0);
        dest.writeByte(this.seen ? (byte) 1 : (byte) 0);
        dest.writeLong(this.udate);
        dest.writeString(this.folder);
    }

    protected Mail(Parcel in) {
        this.subject = in.readString();
        this.from = in.readParcelable(From.class.getClassLoader());
        this.to = in.createStringArray();
        this.dateTime = (DateTime) in.readSerializable();
        this.uid = in.readInt();
        this.flagged = in.readByte() != 0;
        this.deleted = in.readByte() != 0;
        this.seen = in.readByte() != 0;
        this.udate = in.readLong();
        this.folder = in.readString();
    }

    public static final Creator<Mail> CREATOR = new Creator<Mail>() {
        @Override
        public Mail createFromParcel(Parcel source) {
            return new Mail(source);
        }

        @Override
        public Mail[] newArray(int size) {
            return new Mail[size];
        }
    };
}
