package com.fanzine.coys.models.analysis;

/**
 * Created by mbp on 11/2/17.
 */

public class PredictonDiagram {

    private String percentage;
    private String total;
    private boolean isVoted = false;

    public PredictonDiagram(int percentage, int total, boolean isVoted) {
        this.percentage = String.valueOf(percentage);
        this.total = String.valueOf(total);
        this.isVoted = isVoted;
    }

    public String getPercentage() {
        return percentage + " % ";
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getTotal() {
        return "(" + total + ")";
    }

    public String getResult() {
        return getPercentage() + " " + getTotal();
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public boolean isVoted() {
        return isVoted;
    }

    public void setVoted(boolean voted) {
        isVoted = voted;
    }
}
