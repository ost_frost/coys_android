package com.fanzine.coys.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mbp on 4/2/18.
 */

public class PurchaseDto implements Parcelable {

    public String orderId;
    public String packegeName;
    public String signature;
    public String sku;
    public int state;
    public String productId;

    public long time;
    public String token;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.orderId);
        dest.writeString(this.packegeName);
        dest.writeString(this.signature);
        dest.writeString(this.sku);
        dest.writeLong(this.time);
        dest.writeInt(this.state);
        dest.writeString(this.token);
        dest.writeString(this.productId);
    }

    public PurchaseDto() {
    }

    protected PurchaseDto(Parcel in) {
        this.orderId = in.readString();
        this.packegeName = in.readString();
        this.signature = in.readString();
        this.sku = in.readString();
        this.time = in.readLong();
        this.state = in.readInt();
        this.token = in.readString();
        this.productId = in.readString();
    }

    public static final Creator<PurchaseDto> CREATOR = new Creator<PurchaseDto>() {
        @Override
        public PurchaseDto createFromParcel(Parcel source) {
            return new PurchaseDto(source);
        }

        @Override
        public PurchaseDto[] newArray(int size) {
            return new PurchaseDto[size];
        }
    };
}
