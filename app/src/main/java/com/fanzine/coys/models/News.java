package com.fanzine.coys.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fanzine.coys.utils.TimeAgoUtil;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Woland on 19.01.2017.
 */

public class News implements Parcelable, SocialMedia {

    private static final DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm");

    @DatabaseField(columnName = "id")
    @SerializedName("news_id")
    private int newsId;

    @DatabaseField(columnName = "title")
    @SerializedName("title")
    private String title;

    @DatabaseField(columnName = "author")
    @SerializedName("author")
    private String author;

    @DatabaseField(columnName = "image")
    @SerializedName("image")
    private String image;

    @DatabaseField(columnName = "datetime")
    @SerializedName("datetime")
    private DateTime datetime;

    @DatabaseField(columnName = "commentsCount")
    @SerializedName("commentsCount")
    private int commentsCount;

    @DatabaseField(columnName = "likesCount")
    @SerializedName("likes_count")
    private long likesCount;

    @SerializedName("is_liked")
    public boolean isLiked;

    @DatabaseField(columnName = "sharesCount")
    @SerializedName("sharesCount")
    private int sharesCount;

    @DatabaseField(columnName = "content")
    @SerializedName("content")
    private String content;

    @DatabaseField(columnName = "league")
    private int league;

    @DatabaseField(columnName = "url")
    @SerializedName("url")
    private String url;

    @SerializedName("is_pinned")
    private boolean isPinned;

    @SerializedName("comments")
    private List<Comment> comments = new ArrayList<>();

    public int getNewsId() {
        return newsId;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public DateTime getDatetime() {
        return datetime;
    }

    public String getDatetimeString() {
        return datetime.toString(formatter);
    }

    public String getDatetimeStringTime() {
//        return datetime.toString(formatterTime);
        if ( datetime != null) {
            return TimeAgoUtil.getTimeAgo(datetime.getMillis());
        }
        return "";
    }

    public void setDatetime(DateTime datetime) {
        this.datetime = datetime;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public long getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(long likesCount) {
        this.likesCount = likesCount;
    }

    public int getSharesCount() {
        return sharesCount;
    }

    public void setSharesCount(int sharesCount) {
        this.sharesCount = sharesCount;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public boolean isPinned() {
        return isPinned;
    }

    public void setPinned(boolean pinned) {
        isPinned = pinned;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String autor) {
        this.author = autor;
    }

    public int getLeague() {
        return league;
    }

    public News setLeague(int league) {
        this.league = league;

        return this;
    }

    public String getUrl() {
        return url;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.newsId);
        dest.writeString(this.title);
        dest.writeString(this.author);
        dest.writeString(this.image);
        dest.writeSerializable(this.datetime);
        dest.writeInt(this.commentsCount);
        dest.writeLong(this.likesCount);
        dest.writeByte(this.isLiked ? (byte) 1 : (byte) 0);
        dest.writeInt(this.sharesCount);
        dest.writeString(this.content);
        dest.writeInt(this.league);
        dest.writeString(this.url);
        dest.writeByte(this.isPinned ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.comments);
    }

    public News() {
    }

    protected News(Parcel in) {
        this.newsId = in.readInt();
        this.title = in.readString();
        this.author = in.readString();
        this.image = in.readString();
        this.datetime = (DateTime) in.readSerializable();
        this.commentsCount = in.readInt();
        this.likesCount = in.readLong();
        this.isLiked = in.readByte() != 0;
        this.sharesCount = in.readInt();
        this.content = in.readString();
        this.league = in.readInt();
        this.url = in.readString();
        this.isPinned = in.readByte() != 0;
        this.comments = in.createTypedArrayList(Comment.CREATOR);
    }

    public static final Creator<News> CREATOR = new Creator<News>() {
        @Override
        public News createFromParcel(Parcel source) {
            return new News(source);
        }

        @Override
        public News[] newArray(int size) {
            return new News[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        News news = (News) o;

        return newsId == news.newsId;
    }

    @Override
    public int hashCode() {
        return newsId;
    }

    @Override
    public boolean isLiked() {
        return isLiked;
    }
}
