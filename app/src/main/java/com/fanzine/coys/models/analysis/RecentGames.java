package com.fanzine.coys.models.analysis;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by maximdrobonoh on 05.10.17.
 */

public class RecentGames {

    @SerializedName("local_team")
    private List<Item> localTeam;

    @SerializedName("visitor_team")
    private List<Item> visitorTeam;

    public class Item {

        @SerializedName("date")
        private String date;

        @SerializedName("competition")
        private String competetion;

        @SerializedName("home")
        private String home;

        @SerializedName("result")
        private String result;

        @SerializedName("away")
        private String away;

        @SerializedName("general_result")
        private String generalResult;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getCompetetion() {
            return competetion;
        }

        public void setCompetetion(String competetion) {
            this.competetion = competetion;
        }

        public String getHome() {
            return home;
        }

        public void setHome(String home) {
            this.home = home;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public String getAway() {
            return away;
        }

        public void setAway(String away) {
            this.away = away;
        }

        public String getGeneralResult() {
            return generalResult;
        }

        public void setGeneralResult(String generalResult) {
            this.generalResult = generalResult;
        }
    }

    public List<Item> getLocalTeam() {
        return localTeam;
    }

    public void setLocalTeam(List<Item> localTeam) {
        this.localTeam = localTeam;
    }

    public List<Item> getVisitorTeam() {
        return visitorTeam;
    }

    public void setVisitorTeam(List<Item> visitorTeam) {
        this.visitorTeam = visitorTeam;
    }


}
