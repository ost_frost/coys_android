package com.fanzine.coys.models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by Woland on 26.01.2017.
 */

public class League {

    @DatabaseField(columnName = "id", id = true)
    @SerializedName("league_id")
    private int leagueId;

    @DatabaseField(columnName = "name")
    @SerializedName("name")
    private String name;

    @DatabaseField(columnName = "icon")
    @SerializedName("icon")
    private String icon;

    @SerializedName("sort")
    @DatabaseField(columnName = "sort")
    private int sort;

    @SerializedName("web_sort")
    private int webSort;

    @SerializedName("listed_region_sort")
    private int listed_region_sort;

    public League(int leagueId, String name, String icon, int sort) {
        this.leagueId = leagueId;
        this.name = name;
        this.icon = icon;
        this.sort = sort;
    }

    public League() {
    }

    public int getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(int leagueId) {
        this.leagueId = leagueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getWebSort() {
        return webSort;
    }

    public void setWebSort(int webSort) {
        this.webSort = webSort;
    }

    public boolean isSvgFormat() {
        return icon.length() > 3 && icon.substring(icon.length() - 3, icon.length()).equals("svg");
    }

    public int getListed_region_sort() {
        return listed_region_sort;
    }

    public void setListed_region_sort(int listed_region_sort) {
        this.listed_region_sort = listed_region_sort;
    }
}

