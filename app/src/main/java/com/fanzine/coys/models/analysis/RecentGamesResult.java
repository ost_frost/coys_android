package com.fanzine.coys.models.analysis;

/**
 * Created by mbp on 11/2/17.
 */

public interface RecentGamesResult {
    String DRAW = "draw";
    String LOSE = "lose";
    String WIN = "win";
}
