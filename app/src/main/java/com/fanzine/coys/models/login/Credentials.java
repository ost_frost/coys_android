package com.fanzine.coys.models.login;

import com.google.gson.Gson;

/**
 * Created by mbp on 3/20/18.
 */

public class Credentials {

    private String country;

    private String phone;

    private String password;

    private boolean isEmail;

    private String email;

    public Credentials(Country country, String phone, String password) {
        this.country =  new Gson().toJson(country);
        this.phone = phone;
        this.password = password;

        this.isEmail = false;
    }

    public Credentials(String email, String password) {
        this.password = password;
        this.email = email;

        this.isEmail = true;
    }

    public Country getCountry() {
        return new Gson().fromJson(country, Country.class);
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public boolean isEmail() {
        return isEmail;
    }
}
