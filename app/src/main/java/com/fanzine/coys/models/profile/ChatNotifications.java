
package com.fanzine.coys.models.profile;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatNotifications implements Parcelable {

    @SerializedName("chat")
    @Expose
    private int notifyChat;

    protected ChatNotifications(Parcel in) {
        notifyChat = in.readInt();
    }

    public static final Creator<ChatNotifications> CREATOR = new Creator<ChatNotifications>() {
        @Override
        public ChatNotifications createFromParcel(Parcel in) {
            return new ChatNotifications(in);
        }

        @Override
        public ChatNotifications[] newArray(int size) {
            return new ChatNotifications[size];
        }
    };

    public int getNotifyChat() {
        return notifyChat;
    }

    public void setNotifyChat(int notifyChat) {
        this.notifyChat = notifyChat;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(notifyChat);
    }

    public void toggleChat() {
        notifyChat = Math.abs(1 - notifyChat);
    }
}
