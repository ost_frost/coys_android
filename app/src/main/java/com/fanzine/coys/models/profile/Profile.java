package com.fanzine.coys.models.profile;

import android.os.Parcel;
import android.os.Parcelable;

import com.fanzine.coys.utils.TimeDateUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Profile implements Parcelable {
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("birthday")
    @Expose
    private Birthday birthday;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phonecode")
    @Expose
    private int phonecode;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("card_last_numbers")
    @Expose
    private String cardLastNumbers;
    @SerializedName("braintree_id")
    private String braintreeId;

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Birthday getBirthday() {
        return birthday;
    }

    public String getBirthDayDate(){
        if (birthday != null){
            return TimeDateUtils.formatBirthday(birthday.getDate());
        }
        return "";
    }

    public void setBirthday(Birthday birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhonecode() {
        return phonecode;
    }

    public String getPhoneCodeString(){
        return "+" + phonecode;
    }

    public void setPhonecode(int phonecode) {
        this.phonecode = phonecode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCardLastNumbers() {
        return cardLastNumbers;
    }

    public void setCardLastNumbers(String cardLastNumbers) {
        this.cardLastNumbers = cardLastNumbers;
    }

    public String getBraintreeId() {
        return braintreeId;
    }

    public void setBraintreeId(String braintreeId) {
        this.braintreeId = braintreeId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.profileImage);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeParcelable(this.birthday, flags);
        dest.writeString(this.email);
        dest.writeInt(this.phonecode);
        dest.writeString(this.phone);
        dest.writeString(this.cardLastNumbers);
        dest.writeString(this.braintreeId);
    }

    public Profile() {
    }

    protected Profile(Parcel in) {
        this.profileImage = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.birthday = in.readParcelable(Birthday.class.getClassLoader());
        this.email = in.readString();
        this.phonecode = in.readInt();
        this.phone = in.readString();
        this.cardLastNumbers = in.readString();
        this.braintreeId = in.readString();
    }

    public static final Creator<Profile> CREATOR = new Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel source) {
            return new Profile(source);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };
}
