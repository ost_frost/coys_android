package com.fanzine.coys.models.table;

import com.google.gson.annotations.SerializedName;

/**
 * Created by maximdrobonoh on 29.09.17.
 */

public class Filter {

    private int id;

    @SerializedName("display_name")
    private String displayName;

    @SerializedName("field_name")
    private String fieldName;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
}
