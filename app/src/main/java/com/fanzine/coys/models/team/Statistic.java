package com.fanzine.coys.models.team;

/**
 * Created by mbp on 1/9/18.
 */

public class Statistic {
    private long goals;
    private long assists;
    private String appearances;
    private long minutesPlayed;

    public long getGoals() {
        return goals;
    }

    public long getAssists() {
        return assists;
    }

    public String getAppearances() {
        return appearances;
    }

    public long getMinutesPlayed() {
        return minutesPlayed;
    }

    public void setGoals(long goals) {
        this.goals = goals;
    }

    public void setAssists(long assists) {
        this.assists = assists;
    }

    public void setAppearances(String appearances) {
        this.appearances = appearances;
    }

    public void setMinutesPlayed(long minutesPlayed) {
        this.minutesPlayed = minutesPlayed;
    }
}
