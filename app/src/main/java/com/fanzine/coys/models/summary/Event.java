package com.fanzine.coys.models.summary;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Siarhei on 10.02.2017.
 */

public class Event implements Parcelable {

    public static final String EVENT = "event";

    public static final String FULL_TIME = "FT";
    public static final String HALF_TIME = "HT";

    @SerializedName("id")
    private int id;

    @SerializedName("type")
    private String type;

    @SerializedName("minute")
    private String minute;

    @SerializedName("extra_min")
    private Integer extra_min;

    @SerializedName("team")
    private String team;

    @SerializedName("player")
    private String player;

    @SerializedName("assist")
    private String assist;

    @SerializedName("result")
    private String result;

    protected Event(Parcel in) {
        id = in.readInt();
        type = in.readString();
        minute = in.readString();
        team = in.readString();
        player = in.readString();
        assist = in.readString();
        result = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(type);
        dest.writeString(minute);
        dest.writeString(team);
        dest.writeString(player);
        dest.writeString(assist);
        dest.writeString(result);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String  minute) {
        this.minute = minute;
    }

    public Integer getExtra_min() {
        return extra_min;
    }

    public void setExtra_min(Integer extra_min) {
        this.extra_min = extra_min;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public String getAssist() {
        return assist;
    }

    public void setAssist(String assist) {
        this.assist = assist;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
