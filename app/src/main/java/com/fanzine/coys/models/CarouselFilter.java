package com.fanzine.coys.models;

/**
 * Created by mbp on 1/9/18.
 */

public interface CarouselFilter {

    String getTitle();

    int getIcon();
}
