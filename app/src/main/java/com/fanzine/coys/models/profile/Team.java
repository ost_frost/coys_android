
package com.fanzine.coys.models.profile;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Team implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("team_ico")
    @Expose
    private String teamIco;
    @SerializedName("competition_id")
    @Expose
    private int competitionId;
    @SerializedName("notifications")
    @Expose
    private Notifications notifications;

    protected Team(Parcel in) {
        id = in.readInt();
        name = in.readString();
        teamIco = in.readString();
        competitionId = in.readInt();
        notifications = in.readParcelable(Notifications.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(teamIco);
        dest.writeInt(competitionId);
        dest.writeParcelable(notifications, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Team> CREATOR = new Creator<Team>() {
        @Override
        public Team createFromParcel(Parcel in) {
            return new Team(in);
        }

        @Override
        public Team[] newArray(int size) {
            return new Team[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeamIco() {
        return teamIco;
    }

    public void setTeamIco(String teamIco) {
        this.teamIco = teamIco;
    }

    public int getCompetitionId() {
        return competitionId;
    }

    public void setCompetitionId(int competitionId) {
        this.competitionId = competitionId;
    }

    public Notifications getNotifications() {
        return notifications;
    }

    public void setNotifications(Notifications notifications) {
        this.notifications = notifications;
    }

}
