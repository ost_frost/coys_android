package com.fanzine.coys.models.profile;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vitaliygerasymchuk on 2/12/18
 */

public class Email {

    @SerializedName("email")
    @Nullable
    private String email;

    @SerializedName("email_notification")
    private boolean emailNotification;

    @SerializedName("email_signature")
    @Nullable
    private String emailSignature;

    @NonNull
    public String getEmail() {
        return notNull(email);
    }

    public boolean isNotificationOn() {
        return emailNotification;
    }

    @NonNull
    public String getEmailSignature() {
        return notNull(emailSignature);
    }

    @NonNull
    private String notNull(@Nullable String string) {
        return TextUtils.isEmpty(string) ? "" : string;
    }

    public void setEmail(@Nullable String email) {
        this.email = email;
    }

    public void setEmailNotification(boolean emailNotification) {
        this.emailNotification = emailNotification;
    }

    public void setEmailSignature(@Nullable String emailSignature) {
        this.emailSignature = emailSignature;
    }

    @Override
    public String toString() {
        return "Email{" +
                "email='" + email + '\'' +
                ", emailNotification='" + emailNotification + '\'' +
                ", emailSignature='" + emailSignature + '\'' +
                '}';
    }
}
