package com.fanzine.coys.models.team;

/**
 * Created by mbp on 1/9/18.
 */

public enum  TeamSquad {

    FIRST_TEAM("First Team"), ACADEMY("Academy"), LADIES("Ladies");

    private final String title;
    TeamSquad(String title) { this.title = title; }
    public String getValue() { return title; }
}
