package com.fanzine.coys.models.table;

import com.fanzine.coys.models.Team;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Woland on 13.02.2017.
 */

public class TeamTable {

    @SerializedName("position")
    private int position;
    @SerializedName("pl")
    private int pl;
    @SerializedName("gd")
    private int gd;
    @SerializedName("pts")
    private int pts;
    @SerializedName("arrow")
    private String arrow;
    @SerializedName("group")
    private String group;
    @SerializedName("win")
    private int win;
    @SerializedName("draw")
    private int draw;
    @SerializedName("lose")
    private int lose;
    @SerializedName("gd2")
    private String gd2;
    @SerializedName("team")
    private Team team;

    public int getPl() {
        return pl;
    }

    public void setPl(int pl) {
        this.pl = pl;
    }

    public int getGd() {
        return gd;
    }

    public void setGd(int gd) {
        this.gd = gd;
    }

    public int getPts() {
        return pts;
    }

    public void setPts(int pts) {
        this.pts = pts;
    }

    public String getArrow() {
        return arrow;
    }

    public void setArrow(String arrow) {
        this.arrow = arrow;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getWin() {
        return win;
    }

    public void setWin(int win) {
        this.win = win;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int getLose() {
        return lose;
    }

    public void setLose(int lose) {
        this.lose = lose;
    }

    public String getGd2() {
        return gd2;
    }

    public void setGd2(String gd2) {
        this.gd2 = gd2;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
}
