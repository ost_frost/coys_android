package com.fanzine.coys.models.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Woland on 22.03.2017.
 */

public class Country {

    @SerializedName("id")
    private int id;
    @SerializedName("nicename")
    private String nicename;
    @SerializedName("phonecode")
    private int phonecode;

    public Country(int id, int code) {
        this.id = id;
        this.phonecode = code;
    }

    public String getNicename() {
        return nicename;
    }

    public void setNicename(String nicename) {
        this.nicename = nicename;
    }

    public int getPhonecode() {
        return phonecode;
    }

    public String getPhonecodeText() {
        return "+" + Integer.toString(phonecode);
    }

    public void setPhonecode(int phonecode) {
        this.phonecode = phonecode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
