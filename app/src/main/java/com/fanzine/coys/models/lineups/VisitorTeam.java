
package com.fanzine.coys.models.lineups;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VisitorTeam {

    @SerializedName("visitorteam_name")
    @Expose
    private String visitorteamName;
    @SerializedName("visitorteam_icon")
    @Expose
    private String visitorteamIcon;
    @SerializedName("squad")
    @Expose
    private Squad squad;
    @SerializedName("substitutions")
    @Expose
    private List<Substitution> substitution = new ArrayList<>();

    @SerializedName("sidelined")
    @Expose
    private List<SidelinedPlayer> sidelined = new ArrayList<>();

    public String getVisitorteamName() {
        return visitorteamName;
    }

    public void setVisitorteamName(String visitorteamName) {
        this.visitorteamName = visitorteamName;
    }

    public String getVisitorteamIcon() {
        return visitorteamIcon;
    }

    public void setVisitorteamIcon(String visitorteamIcon) {
        this.visitorteamIcon = visitorteamIcon;
    }

    public Squad getSquad() {
        return squad;
    }

    public List<Substitution> getSubstitution() {
        return substitution;
    }

    public void setSubstitution(List<Substitution> substitution) {
        this.substitution = substitution;
    }

    public List<SidelinedPlayer> getSidelined() {
        return sidelined;
    }


    public boolean hasSidelined() {
        return sidelined != null && sidelined.size() > 0;
    }
}
