
package com.fanzine.coys.models.lineups;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Player {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("posx")
    @Expose
    private int posx;
    @SerializedName("posy")
    @Expose
    private int posy;
    @SerializedName("local_visitor_team")
    @Expose
    private int localVisitorTeam;
    @SerializedName("num")
    @Expose
    private int num;
    @SerializedName("goals")
    @Expose
    private int goals;
    @SerializedName("yellowcards")
    @Expose
    private int yellowcards;
    @SerializedName("redcards")
    @Expose
    private int redcards;

    @SerializedName("player_number")
    @Expose
    private int playerNumber;

    @SerializedName("icon")
    @Expose
    private String icon;

    @SerializedName("last_name")
    private String lastName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPosx() {
        return posx;
    }

    public void setPosx(int posx) {
        this.posx = posx;
    }

    public int getPosy() {
        return posy;
    }

    public void setPosy(int posy) {
        this.posy = posy;
    }

    public int getLocalVisitorTeam() {
        return localVisitorTeam;
    }

    public void setLocalVisitorTeam(int localVisitorTeam) {
        this.localVisitorTeam = localVisitorTeam;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getGoals() {
        return goals;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }

    public int getYellowcards() {
        return yellowcards;
    }

    public void setYellowcards(int yellowcards) {
        this.yellowcards = yellowcards;
    }

    public int getRedcards() {
        return redcards;
    }

    public void setRedcards(int redcards) {
        this.redcards = redcards;
    }

    public int getPlayerNumber() {
        return playerNumber;
    }

    public void setPlayerNumber(int playerNumber) {
        this.playerNumber = playerNumber;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public boolean hasCard() {
        return yellowcards != 0 || redcards != 0;
    }

    public boolean hasRedAnaYellowCard() {
        return yellowcards != 0 && redcards != 0;
    }

    public String getShortName() {
        String[] name = this.name.split("\\s+");

        if (name.length == 2) {
            return String.valueOf(name[0].charAt(0)) + ". " + name[1];
        }
        if (name.length == 3) {
            return String.valueOf(name[0].charAt(0)) + ". " + name[2];
        }
        return this.name;
    }

    public String getLastName() {
        if (lastName != null) {

            String[] name = this.lastName.split("\\s+");

            if (name.length == 2) {
                return String.valueOf(name[1]);
            }
            if (name.length == 3) {
                return String.valueOf(name[2]);
            }
            return this.lastName;
        }

        return name;
    }
}
