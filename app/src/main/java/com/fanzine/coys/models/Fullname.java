package com.fanzine.coys.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mbp on 4/5/18.
 */

public class Fullname implements Parcelable {

    private String firstName;
    private String lastName;

    public Fullname(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
    }

    protected Fullname(Parcel in) {
        this.firstName = in.readString();
        this.lastName = in.readString();
    }

    public static final Parcelable.Creator<Fullname> CREATOR = new Parcelable.Creator<Fullname>() {
        @Override
        public Fullname createFromParcel(Parcel source) {
            return new Fullname(source);
        }

        @Override
        public Fullname[] newArray(int size) {
            return new Fullname[size];
        }
    };
}
