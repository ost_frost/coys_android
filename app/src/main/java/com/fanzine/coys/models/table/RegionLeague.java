package com.fanzine.coys.models.table;

import android.os.Parcel;
import android.os.Parcelable;

import com.fanzine.coys.models.League;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbp on 12/4/17.
 */

public class RegionLeague implements Parcelable {

    @SerializedName("name")
    private String name;

    @SerializedName("order")
    private int order;

    @SerializedName("web_sort")
    private int webOrder;

    @SerializedName("leagues")
    private List<League> leagues;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public List<League> getLeagues() {
        return leagues;
    }

    public void setLeagues(List<League> leagues) {
        this.leagues = leagues;
    }

    public int getWebOrder() {
        return webOrder;
    }

    public void setWebOrder(int webOrder) {
        this.webOrder = webOrder;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.order);
        dest.writeList(this.leagues);
    }

    public RegionLeague() {
    }

    protected RegionLeague(Parcel in) {
        this.name = in.readString();
        this.order = in.readInt();
        this.leagues = new ArrayList<League>();
        in.readList(this.leagues, League.class.getClassLoader());
    }

    public static final Creator<RegionLeague> CREATOR = new Creator<RegionLeague>() {
        @Override
        public RegionLeague createFromParcel(Parcel source) {
            return new RegionLeague(source);
        }

        @Override
        public RegionLeague[] newArray(int size) {
            return new RegionLeague[size];
        }
    };
}
