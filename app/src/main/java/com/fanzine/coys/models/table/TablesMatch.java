package com.fanzine.coys.models.table;

import com.fanzine.coys.models.Match;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mbp on 11/15/17.
 */

public class TablesMatch {

    @SerializedName("maxWeek")
    private int maxWeek;

    @SerializedName("week")
    private int week;

    @SerializedName("matches")
    private List<Match> matches;

    public int getMaxWeek() {
        return maxWeek;
    }

    public void setMaxWeek(int maxWeek) {
        this.maxWeek = maxWeek;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }
}
