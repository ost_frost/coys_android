
package com.fanzine.coys.models.team;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import com.fanzine.coys.models.VideoItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

public class PlayerInfo implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("team")
    @Expose
    private String team;
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("birthdate")
    @Expose
    private String birthdate;
    @SerializedName("age")
    @Expose
    private int age;
    @SerializedName("birthcountry")
    @Expose
    private String birthcountry;
    @SerializedName("goals")
    @Expose
    private double goals;
    @SerializedName("assists")
    @Expose
    private double assists;
    @SerializedName("fouls")
    @Expose
    private double fouls;
    @SerializedName("position")
    @Expose
    private String position;
    @SerializedName("height")
    @Expose
    private int height;
    @SerializedName("weight")
    @Expose
    private int weight;
    @SerializedName("number")
    @Expose
    private int number;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("bio")
    @Expose
    private String bio;
    @SerializedName("videos")
    @Expose
    private List<VideoItem> videos = new ArrayList<>();
    @SerializedName("photos")
    @Expose
    private List<Photo> photos = new ArrayList<>();
    @SerializedName("twitter")
    @Expose
    private String twitter;

    protected PlayerInfo(Parcel in) {
        id = in.readInt();
        name = in.readString();
        team = in.readString();
        nationality = in.readString();
        birthdate = in.readString();
        age = in.readInt();
        birthcountry = in.readString();
        goals = in.readDouble();
        assists = in.readDouble();
        fouls = in.readDouble();
        position = in.readString();
        height = in.readInt();
        weight = in.readInt();
        number = in.readInt();
        icon = in.readString();
        bio = in.readString();
        videos = in.createTypedArrayList(VideoItem.CREATOR);
        photos = in.createTypedArrayList(Photo.CREATOR);
        twitter = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(team);
        dest.writeString(nationality);
        dest.writeString(birthdate);
        dest.writeDouble(age);
        dest.writeString(birthcountry);
        dest.writeDouble(goals);
        dest.writeDouble(assists);
        dest.writeDouble(fouls);
        dest.writeString(position);
        dest.writeInt(height);
        dest.writeInt(weight);
        dest.writeInt(number);
        dest.writeString(icon);
        dest.writeString(bio);
        dest.writeTypedList(videos);
        dest.writeTypedList(photos);
        dest.writeString(twitter);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PlayerInfo> CREATOR = new Creator<PlayerInfo>() {
        @Override
        public PlayerInfo createFromParcel(Parcel in) {
            return new PlayerInfo(in);
        }

        @Override
        public PlayerInfo[] newArray(int size) {
            return new PlayerInfo[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public int getAge() {
        return age;
    }

    public String getBirthcountry() {
        return birthcountry;
    }

    public void setBirthcountry(String birthcountry) {
        this.birthcountry = birthcountry;
    }

    public double getGoals() {
        return goals;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }

    public double getAssists() {
        return assists;
    }

    public double getFouls() {
        return fouls;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getBio() {
        if (!TextUtils.isEmpty(bio))
            return bio;
        else
            return "";
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public List<VideoItem> getVideos() {
        return videos;
    }

    public void setVideos(List<VideoItem> videos) {
        this.videos = videos;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getBirthdateFormatted() {
        if (TextUtils.isEmpty(birthdate))
            return "";
        else
            return DateTime.parse(birthdate, DateTimeFormat.forPattern("yyyy-MM-dd")).toString(DateTimeFormat.forPattern("dd.MM.yyyy"));
    }

}
