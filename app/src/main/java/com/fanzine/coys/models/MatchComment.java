package com.fanzine.coys.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Woland on 03.02.2017.
 */

public class MatchComment {

    @SerializedName("minute")
    private String minute;
    @SerializedName("text")
    private String text;
    @SerializedName("important")
    private int important;
    @SerializedName("is_goal")
    private int isGoal;

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getImportant() {
        return important;
    }

    public void setImportant(int important) {
        this.important = important;
    }

    public int getIsGoal() {
        return isGoal;
    }

    public void setIsGoal(int isGoal) {
        this.isGoal = isGoal;
    }
}
