
package com.fanzine.coys.models.profile;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmailNotifications implements Parcelable {

    @SerializedName("email")
    @Expose
    private int notifyEmail;

    protected EmailNotifications(Parcel in) {
        notifyEmail = in.readInt();
    }

    public static final Creator<EmailNotifications> CREATOR = new Creator<EmailNotifications>() {
        @Override
        public EmailNotifications createFromParcel(Parcel in) {
            return new EmailNotifications(in);
        }

        @Override
        public EmailNotifications[] newArray(int size) {
            return new EmailNotifications[size];
        }
    };

    public int getNotifyEmail() {
        return notifyEmail;
    }

    public void setNotifyEmail(int notifyEmail) {
        this.notifyEmail = notifyEmail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(notifyEmail);
    }

    public void toggleEmail() {
        notifyEmail = Math.abs(1 - notifyEmail);
    }

}
