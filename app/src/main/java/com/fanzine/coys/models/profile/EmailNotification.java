package com.fanzine.coys.models.profile;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vitaliygerasymchuk on 2/12/18
 */

public class EmailNotification {

    @SerializedName("email_notification")
    public boolean emailNotification;
}
