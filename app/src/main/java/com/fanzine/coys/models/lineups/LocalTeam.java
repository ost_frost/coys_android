
package com.fanzine.coys.models.lineups;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocalTeam {

    @SerializedName("localteam_name")
    @Expose
    private String localteamName;
    @SerializedName("localteam_icon")
    @Expose
    private String localteamIcon;
    @SerializedName("squad")
    @Expose
    private Squad squad;
    @SerializedName("substitutions")
    @Expose
    private List<Substitution> substitutions = new ArrayList<>();

    @SerializedName("sidelined")
    @Expose
    private List<SidelinedPlayer> sidelined = new ArrayList<>();


    public String getLocalteamName() {
        return localteamName;
    }

    public void setLocalteamName(String localteamName) {
        this.localteamName = localteamName;
    }

    public String getLocalteamIcon() {
        return localteamIcon;
    }

    public void setLocalteamIcon(String localteamIcon) {
        this.localteamIcon = localteamIcon;
    }

    public Squad getSquad() {
        return squad;
    }

    public void setSquad(Squad squad) {
        this.squad = squad;
    }

    public List<Substitution> getSubstitution() {
        return substitutions;
    }

    public void setSubstitution(List<Substitution> substitution) {
        this.substitutions = substitution;
    }

    public List<Substitution> getSubstitutions() {
        return substitutions;
    }

    public List<SidelinedPlayer> getSidelined() {
        return sidelined;
    }

    public boolean hasSidelined() {
        return sidelined != null && sidelined.size() > 0;
    }
}
