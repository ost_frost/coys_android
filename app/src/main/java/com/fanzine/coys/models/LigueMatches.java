package com.fanzine.coys.models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import java.util.List;

/**
 * Created by Siarhei on 03.02.2017.
 */

public class LigueMatches {

    @DatabaseField(generatedId = true)
    private int id;

    @SerializedName("league_id")
    @DatabaseField(columnName = "league_id")
    private int leagueId;

    @SerializedName("league_name")
    @DatabaseField(columnName = "league_name")
    private String name;

    @SerializedName("league_icon")
    @DatabaseField(columnName = "league_icon")
    private String icon;

    @SerializedName("matches")
    private List<Match> matches;

    @SerializedName("sort")
    @DatabaseField(columnName = "sort")
    private int sort;

    @DatabaseField(columnName = "date")
    private String date;


    public int getLeagueId() {
        return leagueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }


    public void setId(int leagueId) {
        this.id = leagueId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
