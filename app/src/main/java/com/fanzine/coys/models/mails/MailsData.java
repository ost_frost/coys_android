
package com.fanzine.coys.models.mails;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MailsData {

    @SerializedName("folders")
    @Expose
    private List<String> folders = null;
    @SerializedName("inbox")
    @Expose
    private List<Mail> inbox = null;

    public List<String> getFolders() {
        return folders;
    }

    public void setFolders(List<String> folders) {
        this.folders = folders;
    }

    public List<Mail> getInbox() {
        return inbox;
    }

    public void setInbox(List<Mail> inbox) {
        this.inbox = inbox;
    }

}
