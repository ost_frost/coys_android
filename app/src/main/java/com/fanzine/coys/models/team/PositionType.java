package com.fanzine.coys.models.team;

/**
 * Created by mbp on 1/10/18.
 */

public interface PositionType {

    String MIDFIELDER = "Midfielder";
    String DEFENDER = "Defender";
    String ATTACKER = "Attacker";
    String GOALKEEPER = "Goalkeeper";
}
