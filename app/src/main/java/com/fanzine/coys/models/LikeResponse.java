package com.fanzine.coys.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vitaliygerasymchuk on 3/9/18
 */

public class LikeResponse {
    @SerializedName("likes_count")
    public long likesCount;
}
