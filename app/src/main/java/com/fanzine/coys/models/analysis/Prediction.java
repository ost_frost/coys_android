package com.fanzine.coys.models.analysis;

import com.google.gson.annotations.SerializedName;

/**
 * Created by maximdrobonoh on 05.10.17.
 */

public class Prediction {

    @SerializedName("all_votes")
    private long allVotes;

    @SerializedName("vote_home")
    private int voteHome;

    @SerializedName("vote_draw")
    private int voteDraw;

    @SerializedName("vote_away")
    private int voteAway;

    @SerializedName("user_vote")
    private String userVote;


    public long getAllVotes() {
        return allVotes;
    }

    public void setAllVotes(long allVotes) {
        this.allVotes = allVotes;
    }

    public int getVoteHome() {
        return voteHome;
    }

    public void setVoteHome(int voteHome) {
        this.voteHome = voteHome;
    }

    public int getVoteDraw() {
        return voteDraw;
    }

    public void setVoteDraw(int voteDraw) {
        this.voteDraw = voteDraw;
    }

    public int getVoteAway() {
        return voteAway;
    }

    public void setVoteAway(int voteAway) {
        this.voteAway = voteAway;
    }

    public String getUserVote() {
        return userVote;
    }

    public void setUserVote(String userVote) {
        this.userVote = userVote;
    }

    public int getHomeProcent() {
        return Math.round(((float) voteHome / allVotes) * 100);
    }

    public int getDrafProcent() {
        return Math.round(((float) voteDraw / allVotes) * 100);
    }

    public int getAwayProcent() {
        return Math.round(((float) voteAway / allVotes) * 100);
    }
}
