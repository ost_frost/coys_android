package com.fanzine.coys.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Woland on 20.04.2017.
 */

public class Status {

    public static final String PRO = "pro";

    @SerializedName(value = "user_status", alternate = ("status"))
    private String status;

    public boolean isPro() {
        return status.equals(PRO);
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
