package com.fanzine.coys.models.mails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by maximdrobonoh on 14.09.17.
 */


public class Label  implements Parcelable {

    public final static String ID = "uid";
    public final static String MAIL_ID = "mail_id";
    public final static String FOLDER_NAME = "folder_name";
    public final static String API_LABEL_ID = "api_label_id";


    @DatabaseField(columnName = "uid", generatedId = true)
    @Expose
    private int uid;

    @DatabaseField(columnName = API_LABEL_ID, unique = false)
    @SerializedName("id")
    private int apiLabelId;

    @DatabaseField(columnName = FOLDER_NAME)
    private String folder;

    @DatabaseField(columnName = "text")
    @SerializedName("text")
    private String text;


    @DatabaseField(foreign = true, canBeNull = false, foreignColumnName = "id")
    @SerializedName("color")
    private Color color;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = MAIL_ID, canBeNull = false)
    private Mail mail;

    public Label() {
    }

    private Label(Parcel in) {
        uid = in.readInt();
        text = in.readString();
        color = in.readParcelable(Color.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(uid);
        parcel.writeString(text);
    }

    public static final Creator<Label> CREATOR = new Creator<Label>() {
        @Override
        public Label createFromParcel(Parcel in) {
            return new Label(in);
        }

        @Override
        public Label[] newArray(int size) {
            return new Label[size];
        }
    };

    public static Creator<Label> getCREATOR() {
        return CREATOR;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getText() {
        return text;
    }


    public int getApiLabelId() {
        return apiLabelId;
    }

    public void setApiLabelId(int apiLabelId) {
        this.apiLabelId = apiLabelId;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Mail getMail() {
        return mail;
    }

    public void setMail(Mail mail) {
        this.mail = mail;
    }
}