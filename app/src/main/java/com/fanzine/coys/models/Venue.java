package com.fanzine.coys.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by mbp on 12/1/17.
 */

public class Venue implements Parcelable{

    @DatabaseField(generatedId = true)
    private int id;

    @SerializedName("name")
    @DatabaseField(columnName = "name")
    private String name;

    @SerializedName("postal_code")
    @DatabaseField(columnName = "postalCode")
    private String postalCode;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public boolean hasName() {
        return name != null;
    }

    public static Creator<Venue> getCREATOR() {
        return CREATOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.postalCode);
    }

    public Venue() {
    }

    protected Venue(Parcel in) {
        this.name = in.readString();
        this.postalCode = in.readString();
    }

    public static final Creator<Venue> CREATOR = new Creator<Venue>() {
        @Override
        public Venue createFromParcel(Parcel source) {
            return new Venue(source);
        }

        @Override
        public Venue[] newArray(int size) {
            return new Venue[size];
        }
    };
}
