package com.fanzine.coys.models.profile;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import static android.text.TextUtils.isEmpty;

/**
 * Created by vitaliygerasymchuk on 2/10/18
 */

public class Terms {

    @SerializedName("terms")
    private String terms;

    @NonNull
    public String getTerms() {
        if (isEmpty(terms)) {
            return "";
        }
        return terms;
    }
}
