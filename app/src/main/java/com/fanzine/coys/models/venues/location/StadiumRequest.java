package com.fanzine.coys.models.venues.location;

import android.os.Parcel;
import android.os.Parcelable;

import com.fanzine.coys.models.venues.Category;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by mbp on 1/25/18.
 */

public class StadiumRequest  extends BaseRequest implements Parcelable {

    private String location;

    public StadiumRequest(ArrayList<Category> categories, ArrayList<Integer> selectedMoney, String location) {
        super(categories, selectedMoney);
        this.location = location;
    }

    public StadiumRequest( String location) {
        super();
        this.location = location;
    }

    @Override
    public Map<String, String> mapParams() {
        Map<String, String> params =  super.mapParams();
        params.put(Keys.STADIUM_LOCATION, location);

        return params;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.location);
    }

    protected StadiumRequest(Parcel in) {
        super(in);
        this.location = in.readString();
    }

    public static final Creator<StadiumRequest> CREATOR = new Creator<StadiumRequest>() {
        @Override
        public StadiumRequest createFromParcel(Parcel source) {
            return new StadiumRequest(source);
        }

        @Override
        public StadiumRequest[] newArray(int size) {
            return new StadiumRequest[size];
        }
    };
}
