package com.fanzine.coys.models.matches;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MatchNotification implements Parcelable {

    @SerializedName("match_id")
    @Expose
    private Integer matchId;
    @SerializedName("kick_of")
    @Expose
    private int kickOf;
    @SerializedName("half_time")
    @Expose
    private int halfTime;
    @SerializedName("full_time")
    @Expose
    private int fullTime;
    @SerializedName("goals")
    @Expose
    private int goals;
    @SerializedName("red_cards")
    @Expose
    private int redCards;

    @SerializedName("line_up")
    private int lineUp;

    @SerializedName("penalty")
    private int penalty;

    @SerializedName("league_id")
    private Integer leagueId;

    @SerializedName("date")
    private String date;

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public int getPenalty() {
        return penalty;
    }

    public void setPenalty(int penalty) {
        this.penalty = penalty;
    }

    public int getKickOf() {
        return kickOf;
    }

    public void toggleKickOf() {
        kickOf = Math.abs(kickOf - 1);
    }

    public int getHalfTime() {
        return halfTime;
    }

    public void toggleHalfTime() {
        halfTime = Math.abs(halfTime - 1);
    }

    public int getFullTime() {
        return fullTime;
    }

    public void toggleFullTime() {
        fullTime = Math.abs(fullTime - 1);
    }

    public void toggleLineUp() {
        lineUp = Math.abs(lineUp - 1);
    }

    public int getGoals() {
        return goals;
    }

    public void toggleGoals() {
        goals = Math.abs(goals - 1);
    }

    public int getRedCards() {
        return redCards;
    }

    public void toggleRedCards() {
        redCards = Math.abs(redCards - 1);
    }

    public void togglePenalty() {
        penalty = Math.abs(penalty - 1);
    }

    public int getLineUp() {
        return lineUp;
    }

    public void setLineUp(int lineUp) {
        this.lineUp = lineUp;
    }

    public Integer getLeagueId() {
        return leagueId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setMatchIdNull(){
        this.matchId = null;
    }

    public void setLeagueId(Integer leagueId) {
        this.leagueId = leagueId;
    }

    public boolean isAll() {
        return halfTime == 1 && fullTime == 1 && goals == 1 && redCards == 1 && kickOf == 1 && lineUp == 1 && penalty == 1;
    }

    public boolean isNotificationsActive() {
        return halfTime == 1 || fullTime == 1 || goals == 1 || redCards == 1 || kickOf == 1 || lineUp == 1 || penalty == 1;
    }

    public void turnOffAll() {
        halfTime = 0;
        fullTime = 0;
        goals = 0;
        redCards = 0;
        kickOf = 0;
        lineUp = 0;
        penalty = 0;
    }

    public void turnOnAll() {
        halfTime = 1;
        fullTime = 1;
        goals = 1;
        redCards = 1;
        kickOf = 1;
        lineUp = 1;
        penalty = 1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.matchId);
        dest.writeInt(this.kickOf);
        dest.writeInt(this.halfTime);
        dest.writeInt(this.fullTime);
        dest.writeInt(this.goals);
        dest.writeInt(this.redCards);
        dest.writeInt(this.lineUp);
        dest.writeInt(this.penalty);
        dest.writeInt(this.leagueId);
    }

    public MatchNotification() {
    }

    protected MatchNotification(Parcel in) {
        this.matchId = in.readInt();
        this.kickOf = in.readInt();
        this.halfTime = in.readInt();
        this.fullTime = in.readInt();
        this.goals = in.readInt();
        this.redCards = in.readInt();
        this.lineUp = in.readInt();
        this.penalty = in.readInt();
        this.leagueId = in.readInt();
    }

    public static final Creator<MatchNotification> CREATOR = new Creator<MatchNotification>() {
        @Override
        public MatchNotification createFromParcel(Parcel source) {
            return new MatchNotification(source);
        }

        @Override
        public MatchNotification[] newArray(int size) {
            return new MatchNotification[size];
        }
    };
}