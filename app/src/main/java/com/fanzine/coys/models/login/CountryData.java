package com.fanzine.coys.models.login;

import android.content.Context;

import com.fanzine.coys.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;

/**
 * Created by Woland on 22.03.2017.
 */

public class CountryData {

    private static List<Country> countries;

    public static List<Country> getCategories(Context context) {
        if (countries == null) {
            InputStream is = context.getResources().openRawResource(R.raw.countries);
            Writer writer = new StringWriter();
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            countries = new Gson().fromJson(writer.toString(), new TypeToken<List<Country>>() {}.getType());
        }

        return countries;
    }

}
