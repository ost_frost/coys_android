package com.fanzine.coys.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by Siarhei on 19.01.2017.
 */

@Deprecated
public class VideoItem implements Parcelable {

    public static final int ARSENAL = 0x1;
    public static final int TRENDING = 0x2;

    @DatabaseField(columnName = "id", generatedId = true)
    private int id;

    @DatabaseField(columnName = "url_video")
    @SerializedName(value = "url_video", alternate = ("link"))
    String urlVideo;

    @DatabaseField(columnName = "preview_image")
    @SerializedName("preview_image")
    String preview_image;


    @DatabaseField(columnName = "description")
    @SerializedName("description")
    String description;


    @DatabaseField(columnName = "sharesCount")
    @SerializedName("sharesCount")
    int sharesCount;


    @DatabaseField(columnName = "likesCount")
    @SerializedName("likesCount")
    int likesCount;


    @DatabaseField(columnName = "type")
    int type;

    public VideoItem() {
    }

    public VideoItem(String urlVideo, String description, int shareCount, int likeCount) {
        this.urlVideo = urlVideo;
        this.description = description;
        this.sharesCount = shareCount;
        this.likesCount = likeCount;
    }

    protected VideoItem(Parcel in) {
        id = in.readInt();
        urlVideo = in.readString();
        preview_image = in.readString();
        description = in.readString();
        sharesCount = in.readInt();
        likesCount = in.readInt();
        type = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(urlVideo);
        dest.writeString(preview_image);
        dest.writeString(description);
        dest.writeInt(sharesCount);
        dest.writeInt(likesCount);
        dest.writeInt(type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VideoItem> CREATOR = new Creator<VideoItem>() {
        @Override
        public VideoItem createFromParcel(Parcel in) {
            return new VideoItem(in);
        }

        @Override
        public VideoItem[] newArray(int size) {
            return new VideoItem[size];
        }
    };

    public String getUrlVideo() {
        return urlVideo;
    }

    public void setUrlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
    }

    public String getPreviewImage() {
//        return "https://img.youtube.com/vi/" + YouTubeHelper.extractVideoIdFromUrl(urlVideo)+ "/0.jpg";
        return preview_image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSharesCount() {
        return sharesCount;
    }

    public void setSharesCount(int sharesCount) {
        this.sharesCount = sharesCount;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public VideoItem saveArsenal() {
        type = ARSENAL;
        return this;
    }

    public VideoItem saveTrending() {
        type = TRENDING;
        return this;
    }
}
