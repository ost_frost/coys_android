package com.fanzine.coys.models.table;

/**
 * Created by maximdrobonoh on 29.09.17.
 */

public class PlayerStatsFilter {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
