package com.fanzine.coys.models.profile;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by vitaliygerasymchuk on 2/12/18
 */

public class LeagueType implements Parcelable, Comparable<LeagueType> {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("order")
    @Expose
    private int order;
    @SerializedName("leagues")
    @Expose
    private List<League> leagues = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public List<League> getLeagues() {
        Collections.sort(leagues);
        return leagues;
    }

    public void setLeagues(List<League> leagues) {
        this.leagues = leagues;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.order);
        dest.writeTypedList(this.leagues);
    }

    public LeagueType() {
    }

    protected LeagueType(Parcel in) {
        this.name = in.readString();
        this.order = in.readInt();
        this.leagues = in.createTypedArrayList(League.CREATOR);
    }

    public static final Parcelable.Creator<LeagueType> CREATOR = new Parcelable.Creator<LeagueType>() {
        @Override
        public LeagueType createFromParcel(Parcel source) {
            return new LeagueType(source);
        }

        @Override
        public LeagueType[] newArray(int size) {
            return new LeagueType[size];
        }
    };

    @Override
    public int compareTo(@NonNull LeagueType o) {
        if (this.getOrder() < o.getOrder()) {
            return -1;
        } else {
            return 1;
        }
    }
}
