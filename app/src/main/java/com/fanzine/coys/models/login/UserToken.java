package com.fanzine.coys.models.login;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Woland on 22.03.2017.
 */

public class UserToken {

    @SerializedName("token")
    private String token;

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("phonecode")
    private String phonecode;

    @SerializedName("phone")
    private String phone;

    @SerializedName("first_name")
    private String first_name;

    @SerializedName("last_name")
    private String last_name;

    @SerializedName("error")
    private String error;

    public boolean isValid() {
        return TextUtils.isEmpty(error) && !TextUtils.isEmpty(token);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getUserId() {
        return user_id;
    }

    public void setUserId(String user_id) {
        this.user_id = user_id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstName() {
        return first_name;
    }

    public void setFirstName(String firstName) {
        this.first_name = firstName;
    }

    public String getLastName() {
        return last_name;
    }

    public void setLastName(String lastName) {
        this.last_name = lastName;
    }

    public String getPhonecode() {
        return phonecode;
    }

    public void setPhonecode(String phonecode) {
        this.phonecode = phonecode;
    }
}
