package com.fanzine.coys.models.venues.location;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.fanzine.coys.models.venues.Category;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by mbp on 1/25/18.
 */

public  class BaseRequest implements Parcelable {

    private static final String EMPTY_SEQUENCE = "";

    interface Keys {
        String LANG = "lang";
        String OAUTH_SIGNATURE_METHOD = "oauth_signature_method";
        String TERM = "term";
        String PRICE = "price";
        String SORT_BY = "sort_by";
        String LATITUDE = "latitude";
        String LONGTITUDE = "longitude";
        String CATEGORIES = "categories";
        String STADIUM_LOCATION = "stadium_location";
    }

    interface Values {
        String LANG_EN = "en";
        String HMAC_SHA1 = "hmac-sha1";
        String SORT_DISTANCE = "distance";
    }


    private ArrayList<Category> categories;
    private ArrayList<Integer> selectedMoney;

    protected String term;

    public String getTerm() {
        return term;
    }

    public BaseRequest(ArrayList<Category> categories, ArrayList<Integer> selectedMoney) {
        this.categories = categories;
        this.selectedMoney = selectedMoney;
    }



    public BaseRequest() {
        this.categories = new ArrayList<>();
        this.selectedMoney = new ArrayList<>();
    }

    public void replaceCategories(Set<Category> categorySet) {
        this.categories.clear();
        this.categories.addAll(categorySet);
    }

    public void replaceSelectedMoney(Set<Integer> selectedMoney) {
        this.selectedMoney.clear();
        this.selectedMoney.addAll(selectedMoney);
    }


    public Map<String, String> mapParams() {
        Map<String, String> params = new HashMap<>();

        if (!TextUtils.isEmpty(getTerm()))
            params.put(Keys.TERM, getTerm());


        params.put(Keys.LANG, Values.LANG_EN);
        params.put(Keys.OAUTH_SIGNATURE_METHOD, Values.HMAC_SHA1);

        if (hasCategories()) {
            params.put(Keys.CATEGORIES, buildCategoriesSequence());
        }

        if ( hasMoney()) {
            params.put(Keys.PRICE, buildMoneySequence());
        }

        params.put(Keys.SORT_BY, Values.SORT_DISTANCE);

        return params;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public boolean hasCategories() {
        return categories != null && categories.size() > 0;
    }

    public boolean hasMoney() {
        return selectedMoney != null && selectedMoney.size() > 0;
    }


    protected String buildCategoriesSequence() {
        if (categories.size() > 0) {
            StringBuilder categoriesSequence = new StringBuilder();

            for (Category category : categories) {
                categoriesSequence.append(category.getAlias()).append(",");
            }

            return categoriesSequence.substring(0, categoriesSequence.length() - 1);
        }
        return EMPTY_SEQUENCE;
    }

    protected String buildMoneySequence() {
        if (selectedMoney.size() > 0) {
            StringBuilder money = new StringBuilder();

            for (Integer s : selectedMoney) {
                money.append(s).append(",");
            }

            return money.substring(0, money.length() - 1);
        }
        return EMPTY_SEQUENCE;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.categories);
        dest.writeList(this.selectedMoney);
        dest.writeString(this.term);
    }

    protected BaseRequest(Parcel in) {
        this.categories = in.createTypedArrayList(Category.CREATOR);
        this.selectedMoney = new ArrayList<Integer>();
        in.readList(this.selectedMoney, Integer.class.getClassLoader());
        this.term = in.readString();
    }

    public static final Creator<BaseRequest> CREATOR = new Creator<BaseRequest>() {
        @Override
        public BaseRequest createFromParcel(Parcel source) {
            return new BaseRequest(source);
        }

        @Override
        public BaseRequest[] newArray(int size) {
            return new BaseRequest[size];
        }
    };
}
