package com.fanzine.coys.models.team;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Woland on 22.02.2017.
 */

public class Player implements Parcelable {
    
    @SerializedName("id")
    @DatabaseField(columnName = "id")
    private int id;

    @SerializedName("name")
    @DatabaseField(columnName = "name")
    private String name;

    @SerializedName("arsenal_team")
    @DatabaseField(columnName = "arsenal_team")
    private int team;

    @SerializedName("nationality")
    @DatabaseField(columnName = "nationality")
    private String nationality;

    @SerializedName("birthdate")
    @DatabaseField(columnName = "birthdate")
    private String birthdate;

    @SerializedName("age")
    @DatabaseField(columnName = "age")
    private int age;

    @SerializedName("birthcountry")
    @DatabaseField(columnName = "birthcountry")
    private String birthcountry;

    @SerializedName("position")
    @DatabaseField(columnName = "position")
    private String position;

    @SerializedName("height")
    @DatabaseField(columnName = "height")
    private int height;

    @SerializedName("weight")
    @DatabaseField(columnName = "weight")
    private int weight;

    @SerializedName("number")
    @DatabaseField(columnName = "number")
    private int number;

    @SerializedName("icon")
    @DatabaseField(columnName = "icon")
    private String icon;

    @SerializedName("statistic")
    public List<PlayerStat> statisticList = new ArrayList<>();



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTeam() {
        return team;
    }

    public void setTeam(int team) {
        this.team = team;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBirthcountry() {
        return birthcountry;
    }

    public void setBirthcountry(String birthcountry) {
        this.birthcountry = birthcountry;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeInt(this.team);
        dest.writeString(this.nationality);
        dest.writeString(this.birthdate);
        dest.writeInt(this.age);
        dest.writeString(this.birthcountry);
        dest.writeString(this.position);
        dest.writeInt(this.height);
        dest.writeInt(this.weight);
        dest.writeInt(this.number);
        dest.writeString(this.icon);
        dest.writeList(this.statisticList);
    }

    public Player() {
    }

    protected Player(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.team = in.readInt();
        this.nationality = in.readString();
        this.birthdate = in.readString();
        this.age = in.readInt();
        this.birthcountry = in.readString();
        this.position = in.readString();
        this.height = in.readInt();
        this.weight = in.readInt();
        this.number = in.readInt();
        this.icon = in.readString();
        this.statisticList = new ArrayList<PlayerStat>();
        in.readList(this.statisticList, PlayerStat.class.getClassLoader());
    }

    public static final Creator<Player> CREATOR = new Creator<Player>() {
        @Override
        public Player createFromParcel(Parcel source) {
            return new Player(source);
        }

        @Override
        public Player[] newArray(int size) {
            return new Player[size];
        }
    };
}