package com.fanzine.coys.models.mails;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by maximdrobonoh on 13.09.17.
 */

public class Folder implements Parcelable {

    @SerializedName("name")
    private String name;

    @SerializedName("required")
    private String required;
    @SerializedName("order")
    private Integer order;
    @SerializedName("image_path")
    private String imagePath;

    private String displayName;

    private int drawableIconId;

    public Folder() {
    }

    protected Folder(Parcel in) {
        name = in.readString();
        required = in.readString();
        order = in.readInt();
        imagePath = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(required);
        dest.writeInt(order);
        dest.writeString(imagePath);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Folder> CREATOR = new Creator<Folder>() {
        @Override
        public Folder createFromParcel(Parcel in) {
            return new Folder(in);
        }

        @Override
        public Folder[] newArray(int size) {
            return new Folder[size];
        }
    };

    public int getDrawableIconId() {
        return drawableIconId;
    }

    public void setDrawableIconId(int drawableIconId) {
        this.drawableIconId = drawableIconId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getOrder() {
        return order != null ? order.intValue() : Integer.MAX_VALUE;
    }

    public String getImagePath() {
        return imagePath;
    }

    public static Creator<Folder> getCREATOR() {
        return CREATOR;
    }
}