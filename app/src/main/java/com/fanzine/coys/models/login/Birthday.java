package com.fanzine.coys.models.login;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by maximdrobonoh on 17.08.17.
 */

public class Birthday {

    @SerializedName("day")
    private int day;
    @SerializedName("mouth")
    private int mouth;
    @SerializedName("year")
    private int year;

    public Birthday(int day, int mouth, int year) {
        this.day = day;
        this.mouth = mouth;
        this.year = year;
    }

    public String getFormatted() {
        Calendar calendar = Calendar.getInstance();
        //Months are zero-based in Calendar
        calendar.set(year, mouth - 1, day);

        Date date = calendar.getTime();

        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }

    public int getDay() {
        return day;
    }

    public int getMouth() {
        return mouth;
    }

    public int getYear() {
        return year;
    }
}
