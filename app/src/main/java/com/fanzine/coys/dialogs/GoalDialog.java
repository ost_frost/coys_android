package com.fanzine.coys.dialogs;


import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.databinding.DialogGoalBinding;
import com.fanzine.coys.dialogs.base.BaseDialog;
import com.fanzine.coys.models.summary.Event;
import com.fanzine.coys.viewmodels.fragments.match.GoalDialogViewModel;
import com.jakewharton.rxbinding.view.RxView;

public class GoalDialog extends BaseDialog {
    public static GoalDialog newInstance() {
        GoalDialog fragment = new GoalDialog();

        return fragment;
    }

    public static void show(FragmentManager manager, Event event) {
        GoalDialog fragment = new GoalDialog();
        Bundle args = new Bundle();
        args.putParcelable(Event.EVENT, event);
        fragment.setArguments(args);
        fragment.show(manager, MatchNotifications.class.getName());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DialogGoalBinding binding = DialogGoalBinding.inflate(inflater, container, false);
        binding.setViewModel(new GoalDialogViewModel(getContext(), getArguments().getParcelable(Event.EVENT)));
        RxView.clicks(binding.btnClose).subscribe(aVoid -> {
            dismiss();
        });

        return binding.getRoot();
    }

}
