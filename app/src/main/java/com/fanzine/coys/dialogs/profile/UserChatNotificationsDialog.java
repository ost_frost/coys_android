package com.fanzine.coys.dialogs.profile;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.fanzine.coys.databinding.FragmentUserNotificationsBinding;
import com.fanzine.coys.dialogs.base.BaseDialog;
import com.fanzine.coys.models.profile.ChatNotifications;
import com.fanzine.coys.viewmodels.dialogs.UserNotificationViewModel;
import com.jakewharton.rxbinding.view.RxView;


public class UserChatNotificationsDialog extends BaseDialog implements DialogInterface {

    private static final String USER = "USER";

    public static void show(FragmentManager manager, ChatNotifications notifications) {
        UserChatNotificationsDialog fragment = new UserChatNotificationsDialog();
        Bundle args = new Bundle();
        args.putParcelable(USER, notifications);
        fragment.setArguments(args);
        fragment.show(manager, UserChatNotificationsDialog.class.getName());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentUserNotificationsBinding binding = FragmentUserNotificationsBinding.inflate(inflater, container, false);
        UserNotificationViewModel viewModel = new UserNotificationViewModel(getContext());
        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);

        viewModel.notif.set(getArguments().getParcelable(USER));

        RxView.clicks(binding.btnSave).subscribe(aVoid -> viewModel.saveMatchNotification(this));

        return binding.getRoot();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void cancel() {

    }
}
