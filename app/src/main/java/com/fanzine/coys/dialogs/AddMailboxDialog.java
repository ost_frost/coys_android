package com.fanzine.coys.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.DialogAddMailboxBinding;
import com.fanzine.coys.dialogs.base.BaseDialog;

/**
 * Created by maximdrobonoh on 13.09.17.
 */

public class AddMailboxDialog extends BaseDialog implements DialogInterface {

    private static final String MAIL = "MAIL";

    public AddMailboxDialog() {
        // Required empty public constructor
    }

    public static AddMailboxDialog newInstance() {
        AddMailboxDialog fragment = new AddMailboxDialog();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        DialogAddMailboxBinding binding = DialogAddMailboxBinding.inflate(LayoutInflater.from(getContext()), null);


        builder.setView(binding.getRoot());

        builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dismiss());

        return builder.create();
    }

    @Override
    public void cancel() {

    }
}