package com.fanzine.coys.dialogs.profile;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.dialogs.TeamNotificationDialogAdapter;
import com.fanzine.coys.databinding.FragmentDialogNotificationsBinding;
import com.fanzine.coys.dialogs.base.BaseDialog;
import com.fanzine.coys.models.profile.League;
import com.fanzine.coys.utils.RecyclerItemClickListener;
import com.fanzine.coys.viewmodels.dialogs.NotificationsViewModel;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Woland on 21.04.2017.
 */

public class NotificationsDialog extends BaseDialog implements DialogInterface {

    private static final String LEAGUES = "leagues";
    private int leaguePosition;

    public static void show(FragmentManager manager, List<League> leagues) {
        NotificationsDialog fragment = new NotificationsDialog();
        Bundle args = new Bundle();
        args.putParcelableArrayList(LEAGUES, new ArrayList<>(leagues));
        fragment.setArguments(args);
        fragment.show(manager, UserChatNotificationsDialog.class.getName());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentDialogNotificationsBinding binding = FragmentDialogNotificationsBinding.inflate(inflater, container, false);
        NotificationsViewModel viewModel = new NotificationsViewModel(getContext());
        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);

        List<League> leagues = getArguments().getParcelableArrayList(LEAGUES);

        TeamNotificationDialogAdapter adapter = new TeamNotificationDialogAdapter(getContext(), leagues);

        binding.rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        binding.rv.setAdapter(adapter);
        binding.rv.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), (view, position) -> {
            if (viewModel.leagueId != -1) {
                viewModel.notif.set(leagues.get(leaguePosition).getTeams().get(position).getNotifications());
                viewModel.teamId = leagues.get(leaguePosition).getTeams().get(position).getId();
            }
            else {
                leaguePosition = position;
                viewModel.leagueId = leagues.get(leaguePosition).getLeagueId();
                adapter.setTeams(leagues.get(leaguePosition).getTeams());
            }
        }));

        RxView.clicks(binding.btnSave).subscribe(aVoid -> viewModel.saveNotification(this));

        RxView.clicks(binding.back).subscribe(aVoid -> {
            if (viewModel.notif.get() != null) {
                viewModel.notif.set(null);
            }
            else {
                viewModel.leagueId = -1;
                adapter.setTeams(null);
            }
        });

        return binding.getRoot();
    }

    @Override
    public void cancel() {

    }
}
