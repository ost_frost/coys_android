package com.fanzine.coys.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.DialogChangeFolderBinding;
import com.fanzine.coys.dialogs.base.BaseDialog;
import com.fanzine.coys.models.mails.MailContent;
import com.fanzine.coys.viewmodels.dialogs.ChangeFolderDialogViewModel;

/**
 * Created by maximdrobonoh on 15.09.17.
 */

public class LabelActionDialog extends BaseDialog implements DialogInterface {

    private static final String MAIL = "MAIL";

    public LabelActionDialog() {
        // Required empty public constructor
    }

    public static ChangeFolderDialog newInstance(MailContent mailContent) {
        ChangeFolderDialog fragment = new ChangeFolderDialog();
        Bundle args = new Bundle();
        args.putParcelable(MAIL, mailContent);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        DialogChangeFolderBinding binding = DialogChangeFolderBinding.inflate(LayoutInflater.from(getContext()), null);
        ChangeFolderDialogViewModel viewModel = new ChangeFolderDialogViewModel(getContext());

        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);

        MailContent mail = getArguments().getParcelable(MAIL);

//        binding.rvFolders.addOnItemTouchListener(
//                new RecyclerItemClickListener(
//                        getContext(),
//                        (view, position) -> viewModel.moveMail(mail.getPresenter(), mail.getUid(), categories.get(position), this)));

        builder.setView(binding.getRoot());

        builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dismiss());

        return builder.create();
    }

    @Override
    public void cancel() {

    }
}