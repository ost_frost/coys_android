package com.fanzine.coys.dialogs;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.mails.MailCategoriesAdapter;
import com.fanzine.coys.databinding.DialogChangeFolderBinding;
import com.fanzine.coys.dialogs.base.BaseDialog;
import com.fanzine.coys.models.mails.MailContent;
import com.fanzine.coys.utils.RecyclerItemClickListener;
import com.fanzine.coys.viewmodels.dialogs.ChangeFolderDialogViewModel;

import java.util.ArrayList;
import java.util.List;


public class ChangeFolderDialog extends BaseDialog implements DialogInterface {

    private static final String MAIL = "MAIL";

    public ChangeFolderDialog() {
        // Required empty public constructor
    }

    public static ChangeFolderDialog newInstance(MailContent mailContent) {
        ChangeFolderDialog fragment = new ChangeFolderDialog();
        Bundle args = new Bundle();
        args.putParcelable(MAIL, mailContent);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        DialogChangeFolderBinding binding = DialogChangeFolderBinding.inflate(LayoutInflater.from(getContext()), null);
        ChangeFolderDialogViewModel viewModel = new ChangeFolderDialogViewModel(getContext());

        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);

        List<String> categories = new ArrayList<>();

        categories.add("Drafts");
        categories.add("INBOX");
        categories.add("Sent");
        categories.add("Spam");
        categories.add("Trash");

        binding.rvFolders.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        binding.rvFolders.setAdapter(new MailCategoriesAdapter(getContext(), categories));
        binding.rvFolders.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        MailContent mail = getArguments().getParcelable(MAIL);

        binding.rvFolders.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        getContext(),
                        (view, position) -> viewModel.moveMail(mail.getFolder(), mail.getUid(), categories.get(position), this)));

        builder.setView(binding.getRoot());

        builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dismiss());

        return builder.create();
    }

    @Override
    public void cancel() {

    }
}
