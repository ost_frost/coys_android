package com.fanzine.coys.dialogs;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.match.GoalsTimeAdapter;
import com.fanzine.coys.adapters.match.ScorersAdapter;
import com.fanzine.coys.databinding.FragmentMatchInfoBinding;
import com.fanzine.coys.dialogs.base.BaseDialog;


public class MatchInfo extends BaseDialog {

    public MatchInfo() {
        // Required empty public constructor
    }

    public static MatchInfo newInstance() {
        MatchInfo fragment = new MatchInfo();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public static void show(FragmentManager manager) {
        MatchInfo fragment = new MatchInfo();
        fragment.show(manager, MatchInfo.class.getName());
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {}
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        FragmentMatchInfoBinding binding = FragmentMatchInfoBinding.inflate(LayoutInflater.from(getContext()), null);

        binding.rvHomeScorers.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        binding.rvHomeScorers.setAdapter(new ScorersAdapter(getContext(), true));

        binding.rvGuestScorers.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        binding.rvGuestScorers.setAdapter(new ScorersAdapter(getContext(), false));

        binding.rvGoalsTime.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        binding.rvGoalsTime.setAdapter(new GoalsTimeAdapter(getContext()));

        builder.setView(binding.getRoot());

        builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
            dismiss();
        });

        return builder.create();
    }
}
