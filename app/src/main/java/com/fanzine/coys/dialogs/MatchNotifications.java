package com.fanzine.coys.dialogs;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.fanzine.coys.databinding.FragmentMatchNotificationsBinding;
import com.fanzine.coys.dialogs.base.BaseDialog;
import com.fanzine.coys.interfaces.MatchNotificationListener;
import com.fanzine.coys.models.matches.MatchNotification;
import com.fanzine.coys.viewmodels.dialogs.MatchNotificationViewModel;
import com.jakewharton.rxbinding.view.RxView;


public class MatchNotifications extends BaseDialog implements DialogInterface {


    private static final String MATCH = "matchNotification";
    private static final String MATCH_LISTENER = "matchNotificationListener";

    private FragmentMatchNotificationsBinding binding;

    public static void show(FragmentManager manager, MatchNotification matchNotification, MatchNotificationListener listener) {
        MatchNotifications fragment = new MatchNotifications();
        Bundle args = new Bundle();
        args.putParcelable(MATCH, matchNotification);
        args.putSerializable(MATCH_LISTENER, listener);
        fragment.setArguments(args);
        fragment.show(manager, MatchNotifications.class.getName());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentMatchNotificationsBinding.inflate(inflater, container, false);
        MatchNotificationViewModel viewModel = new MatchNotificationViewModel(getContext());
        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);

        viewModel.match.set(getArguments().getParcelable(MATCH));

        MatchNotificationListener listener = (MatchNotificationListener) getArguments().get(MATCH_LISTENER);

        RxView.clicks(binding.btnSave).subscribe(aVoid -> {
            viewModel.saveMatchNotification(this);

            if (listener == null) return;

            if (viewModel.match.get().isNotificationsActive())
                listener.turnOn();
            else
                listener.turnOff();
        });
        RxView.clicks(binding.btnCancel).subscribe(aVoid -> dismiss());

        binding.selectAll.setOnClickListener(view -> {

            if (viewModel.getIsAll()) {
                toogleAll(false);
                viewModel.turnAllOff();
            } else {
                toogleAll(true);
                viewModel.turnAllOn();
            }
        });

        return binding.getRoot();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void cancel() {

    }

    private void toogleAll(boolean isAllSelected) {
        binding.full.setChecked(isAllSelected);
        binding.lineUp.setChecked(isAllSelected);
        binding.half.setChecked(isAllSelected);
        binding.goals.setChecked(isAllSelected);
        binding.kick.setChecked(isAllSelected);
        binding.redCard.setChecked(isAllSelected);
        binding.penalty.setChecked(isAllSelected);
    }
}
