package com.fanzine.coys.dialogs.base;

import android.support.v4.app.DialogFragment;

import com.fanzine.coys.utils.KeyboardUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Siarhei on 16.01.2017.
 */

public class BaseDialog extends DialogFragment {

    protected BaseViewModel baseViewModel;

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (baseViewModel != null)
            baseViewModel.destroy();
    }

    protected void setBaseViewModel(BaseViewModel model) {
        this.baseViewModel = model;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        KeyboardUtils.hideSoftKeyboard(getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (baseViewModel == null) {
            new RuntimeException("Please setup base view model!!! Example on Login Fragment");
        }
    }
}
