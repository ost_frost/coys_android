package com.fanzine.coys.dialogs;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.fanzine.coys.databinding.FragmentMatchNotificationsBinding;
import com.fanzine.coys.dialogs.base.BaseDialog;
import com.fanzine.coys.interfaces.LeagueNotificationListener;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.viewmodels.dialogs.MatchNotificationViewModel;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;


public class LeagueMatchesNotifications extends BaseDialog implements DialogInterface {

    private static final String MATCHES = "matchesNotification";
    private static final String LEAGUE_LISTENER = "leagueNotificationListener";

    private FragmentMatchNotificationsBinding binding;

    public static void show(FragmentManager manager, List<Match> matchList, LeagueNotificationListener listener) {
        LeagueMatchesNotifications fragment = new LeagueMatchesNotifications();
        Bundle args = new Bundle();
        args.putParcelableArrayList(MATCHES, (ArrayList<Match>) matchList);
        args.putSerializable(LEAGUE_LISTENER, listener);
        fragment.setArguments(args);
        fragment.show(manager, LeagueMatchesNotifications.class.getName());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentMatchNotificationsBinding.inflate(inflater, container, false);
        MatchNotificationViewModel viewModel = new MatchNotificationViewModel(getContext());

        binding.setViewModel(viewModel);

        LeagueNotificationListener listener = (LeagueNotificationListener) getArguments().get(LEAGUE_LISTENER);

        RxView.clicks(binding.btnSave).subscribe(aVoid -> {
            //todo::request save
            if (listener == null) return;
//            viewModel.saveMatchNotification();
            listener.save(viewModel.getMatchNotification());
        });
        RxView.clicks(binding.btnCancel).subscribe(aVoid -> dismiss());

        binding.selectAll.setOnClickListener(view -> {
            toogleAll(!binding.selectAll.isChecked());
        });

        return binding.getRoot();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void cancel() {

    }

    private void toogleAll(boolean isAllSelected) {
        binding.full.setChecked(isAllSelected);
        binding.lineUp.setChecked(isAllSelected);
        binding.half.setChecked(isAllSelected);
        binding.goals.setChecked(isAllSelected);
        binding.kick.setChecked(isAllSelected);
        binding.redCard.setChecked(isAllSelected);
        binding.penalty.setChecked(isAllSelected);
    }
}
