package com.fanzine.coys.adapters.login;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemCountryBinding;
import com.fanzine.coys.models.ProEmail;
import com.fanzine.coys.utils.RecyclerItemClickListener;
import com.fanzine.coys.viewmodels.items.login.ItemCountryViewModel;

import java.util.List;

/**
 * Created by Woland on 22.03.2017.
 */

public class EmailsAdapter extends BaseAdapter<EmailsAdapter.Holder> implements RecyclerItemClickListener.OnItemClickListener {

    private final List<ProEmail> data;
    private final boolean enable;
    private int selected = -1;

    public EmailsAdapter(Context context, List<ProEmail> data, boolean enable) {
        super(context);

        this.data = data;
        this.enable = enable;
    }

    public EmailsAdapter(Context context, List<ProEmail> data) {
        this(context, data, false);
    }

    public int getSelected() {
        return selected;
    }

    @Override
    public EmailsAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemCountryBinding.inflate(inflater, parent, false)) ;
    }

    @Override
    public void onBindViewHolder(EmailsAdapter.Holder holder, int position) {
        holder.init(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onItemClick(View view, int position) {
        if (enable) {
            int lastSelected = selected;
            selected = position;
            notifyItemChanged(lastSelected);
            notifyItemChanged(selected);
        }
    }

    public class Holder extends RecyclerView.ViewHolder {

        private final ItemCountryBinding binding;

        public Holder(ItemCountryBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(int position) {
            binding.setViewModel(new ItemCountryViewModel(getContext(), data.get(position), enable, position == selected));
            binding.executePendingBindings();
        }

    }
}
