package com.fanzine.coys.adapters.mails;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemMailCategoryBinding;
import com.fanzine.coys.fragments.mails.MailCategoriesClickCallback;
import com.fanzine.coys.viewmodels.items.ItemMailCategoryViewModel;

import java.util.ArrayList;
import java.util.List;

@Deprecated
public class MailCategoriesAdapter extends BaseAdapter<MailCategoriesAdapter.Holder> {

    private final List<String> data;
    private ArrayList<String> selectedItems = new ArrayList<String>();

    public ActionMode actionMode;

    private MailCategoriesClickCallback mailCategoriesClickCallback;

    private boolean multiSelect = false;

    private ActionMode.Callback actionModeCallbacks = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            multiSelect = true;
            menu.add("Delete");

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            mailCategoriesClickCallback.hideToolbar();

            for (String intItem : selectedItems) {
                data.remove(intItem);
                mailCategoriesClickCallback.deleteFolder(intItem);
            }
            mode.finish();
            return true;
        }


        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mailCategoriesClickCallback.showToolbar();

            multiSelect = false;
            selectedItems.clear();
            notifyDataSetChanged();
        }
    };


    public MailCategoriesAdapter(Context context, List<String> data, MailCategoriesClickCallback categoriesClickCallback) {
        super(context);
        this.data = data;
        this.mailCategoriesClickCallback = categoriesClickCallback;
    }


    public MailCategoriesAdapter(Context context, List<String> data) {
        super(context);
        this.data = data;
    }



    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemMailCategoryBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    class Holder extends RecyclerView.ViewHolder {
        private ItemMailCategoryBinding binding;


        Holder(ItemMailCategoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }


        void selectItem(String item) {
            mailCategoriesClickCallback.hideToolbar();
            if (multiSelect) {
                if (selectedItems.contains(item)) {
                    selectedItems.remove(item);
                    binding.itemMailCategory.setBackgroundColor(Color.WHITE);
                } else {
                    selectedItems.add(item);
                    binding.itemMailCategory.setBackgroundColor(Color.LTGRAY);
                }
            }
        }

        void bind(int position) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemMailCategoryViewModel(getContext(), data.get(position)));
            } else {
                binding.getViewModel().setNameCategory(data.get(position));
            }
            binding.executePendingBindings();

            if (selectedItems.contains(data.get(position))) {
                binding.itemMailCategory.setBackgroundColor(Color.LTGRAY);
            } else {
                binding.itemMailCategory.setBackgroundColor(Color.WHITE);
            }

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    mailCategoriesClickCallback.hideToolbar();
                    actionMode = ((AppCompatActivity) view.getContext()).startSupportActionMode(actionModeCallbacks);
                    selectItem(data.get(position));
                    return true;
                }
            });
        }

    }

    public void add(String folder) {
        if (data != null) {
            data.add(folder);

            notifyDataSetChanged();
        }
    }
}
