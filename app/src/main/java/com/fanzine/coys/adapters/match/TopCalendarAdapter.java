package com.fanzine.coys.adapters.match;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemDayBinding;
import com.fanzine.coys.models.DayInfo;
import com.fanzine.coys.utils.FontCache;
import com.fanzine.coys.viewmodels.items.ItemDayInfoViewModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class TopCalendarAdapter extends BaseAdapter<TopCalendarAdapter.Holder> {

    private final int width;
    private List<DayInfo> data;
    private String[] namesDays = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    private String[] namesMonths = {"Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"};

    private int selected = 0;

    public TopCalendarAdapter(Context context, int selected) {
        super(context);

        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x / 5;
        data = new ArrayList<>();
        initDataCalendar();

        this.selected = getCurrentDateIndex();
    }

    private int getCurrentDateIndex() {
        Calendar calendar = Calendar.getInstance();
        Date currentDate = calendar.getTime();

        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getDate().compareTo(currentDate) == 1) {
                return i - 1;
            }
        }
        return 0;
    }

    public Date getCurrentDate() {
        return data.get(selected).getDate();
    }

    private void initDataCalendar() {
        String startDate = "2017-08-1";
        String endDate = "2018-07-31";

        List<Date> dates = getDates(startDate, endDate);

        Calendar calendarToday = Calendar.getInstance();

        Calendar calendar = null;
        for (Date date : dates) {
            calendar = Calendar.getInstance();
            calendar.setTime(date);

            if (calendar.get(Calendar.DAY_OF_YEAR) == calendarToday.get(Calendar.DAY_OF_YEAR))
                data.add(new DayInfo(calendar.getTime(), calendar.get(Calendar.DAY_OF_MONTH), getContext().getString(R.string.today), getNameMonth(calendar.get(Calendar.MONTH))));
            else
                data.add(new DayInfo(calendar.getTime(), calendar.get(Calendar.DAY_OF_MONTH), getNameDay(calendar.get(Calendar.DAY_OF_WEEK)), getNameMonth(calendar.get(Calendar.MONTH))));
        }
    }

    private String getNameDay(int dayOfWeek) {
        return namesDays[dayOfWeek - 1];
    }

    private String getNameMonth(int month) {
        return namesMonths[month];
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemDayBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(TopCalendarAdapter.Holder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setSelected(int selected) {
        this.selected = selected;
        notifyDataSetChanged();
    }

    public int getSelectedIndex() {
        return selected;
    }

    public Date getSelectedDate() {

        try {
            return data.get(selected).getDate();

        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }

    }

    public DayInfo getSelectedDayInfo() {

        try {
            return data.get(selected);

        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }

    }

    private View initView(View view) {
        view.setLayoutParams(new RecyclerView.LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT));

        return view;
    }

    public boolean isToday(int position) {
        return data.get(position).getNameDay().equals(getContext().getString(R.string.today));
    }

    class Holder extends RecyclerView.ViewHolder {

        private final ItemDayBinding binding;


        Holder(ItemDayBinding binding) {
            super(initView(binding.getRoot()));
            this.binding = binding;
        }

        void bind(int position) {
            binding.setViewModel(new ItemDayInfoViewModel(getContext(), data.get(position)));

            if (binding.getViewModel().isToday(data.get(position))) {
                binding.dateTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);

                Typeface customFont = FontCache.getTypeface("fonts/roboto/Roboto-Bold.ttf", getContext());
                binding.dateTitle.setTypeface(customFont, Typeface.BOLD);

                binding.dateTitle.setAllCaps(true);
            } else {
                binding.nameDay.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                binding.dateTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);

                Typeface customFont = FontCache.getTypeface("fonts/roboto/Roboto-Regular.ttf", getContext());
                binding.dateTitle.setTypeface(customFont, Typeface.NORMAL);
                binding.nameDay.setAllCaps(false);
                binding.dateTitle.setAllCaps(false);
            }

            binding.getViewModel().isSelected.set(position == selected);
            binding.executePendingBindings();

            Log.i(getClass().getName(), data.get(position).getNameDay());
        }
    }

    private static List<Date> getDates(String dateString1, String dateString2) {
        ArrayList<Date> dates = new ArrayList<Date>();
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1.parse(dateString1);
            date2 = df1.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);


        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while (!cal1.after(cal2)) {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }
}
