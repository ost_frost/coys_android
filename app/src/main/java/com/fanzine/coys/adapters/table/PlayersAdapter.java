package com.fanzine.coys.adapters.table;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemTablePlayerBinding;
import com.fanzine.coys.models.table.PlayerStats;
import com.fanzine.coys.viewmodels.items.table.ItemTablePlayerViewModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximdrobonoh on 29.09.17.
 */

public class PlayersAdapter extends BaseAdapter<PlayersAdapter.ViewHolder> {

    private List<PlayerStats> data;

    public PlayersAdapter(Context context) {
        super(context);

        data = new ArrayList<>();
    }

    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<PlayerStats> data) {
        this.data = data;

        notifyDataSetChanged();
    }

    @Override
    public PlayersAdapter.ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new PlayersAdapter.ViewHolder(ItemTablePlayerBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(PlayersAdapter.ViewHolder holder, int position) {
        holder.init(data.get(position), position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ItemTablePlayerBinding binding;

        public ViewHolder(ItemTablePlayerBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(PlayerStats playerStats, int position) {
            binding.setViewModel(new ItemTablePlayerViewModel(getContext(), playerStats));

            Glide.with(getContext()).load(playerStats.getIcon()).into(binding.icon);
            Glide.with(getContext()).load(playerStats.getTeamIcon()).into(binding.teamIcon);
        }
    }
}
