package com.fanzine.coys.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.databinding.ItemImageviewBinding;
import com.fanzine.coys.viewmodels.items.venues.ImageViewItem;

import java.lang.ref.WeakReference;
import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.Holder> {

    WeakReference<Context> context;
    WeakReference<LayoutInflater> inflater;
    List<String> data;

    public ImageAdapter(Context context, List<String> banners) {
        data = banners;
        inflater = new WeakReference<>(LayoutInflater.from(context));
        this.context = new WeakReference<>(context);
    }

    @Override
    public ImageAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(ItemImageviewBinding.inflate(inflater.get(), parent, false));
    }

    @Override
    public void onBindViewHolder(ImageAdapter.Holder holder, int position) {
        holder.init(position);
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private final ItemImageviewBinding binding;

        public Holder(ItemImageviewBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(int position) {
            binding.setViewModel(new ImageViewItem(context.get(), data.get(position)));
        }

    }
}