package com.fanzine.coys.adapters.venues;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemVenueNewBinding;
import com.fanzine.coys.viewmodels.items.venues.VenueItemViewModel;
import com.yelp.fusion.client.models.Business;

import java.util.List;


public class VenuesAdapter extends BaseAdapter<VenuesAdapter.Holder> {

    public interface OnItemClickListener {
        void showVenue(Business business);
        void callPhone(String phone);
        void findWebsiteByGoogleApi(Business business);
        void showDirection(Business business);
    }

    private final List<Business> data;

    private OnItemClickListener onItemClickListener;

    public VenuesAdapter(Context context, List<Business> businesses, OnItemClickListener onItemClickListener) {
        super(context);

        this.data = businesses;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemVenueNewBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(VenuesAdapter.Holder holder, int position) {
        holder.init(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private ItemVenueNewBinding binding;
        private Business business;

        Holder(ItemVenueNewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void init(Business business) {
            this.business = business;
            if (binding.getViewModel() == null) {
                binding.setViewModel(new VenueItemViewModel(getContext()));
            }

            binding.venueCity.setText(business.getLocation().getCity());

            if (business.getPrice().length() == 1) {
                binding.ivPrice.setImageResource(R.drawable.price_1w);
            } else if (business.getPrice().length() == 2) {
                binding.ivPrice.setImageResource(R.drawable.price_2w);
            } else if (business.getPrice().length() == 3) {
                binding.ivPrice.setImageResource(R.drawable.price_3w);
            } else if (business.getPrice().length() == 4) {
                binding.ivPrice.setImageResource(R.drawable.price_4w);
            }

            setRating();
            binding.getViewModel().business.set(business);

            binding.leftContainer.setOnClickListener(view -> {
                onItemClickListener.showVenue(business);
            });

            binding.titleContainer.setOnClickListener(view -> {
                onItemClickListener.showVenue(business);
            });

            binding.contentContainer.setOnClickListener(view -> {
                onItemClickListener.showVenue(business);
            });

            binding.venueWeb.setOnClickListener(view -> {
              onItemClickListener.findWebsiteByGoogleApi(business);
            });

            binding.venueCall.setOnClickListener(view -> {
                onItemClickListener.callPhone(business.getPhone());
            });

            binding.venueDirections.setOnClickListener(view -> {
                onItemClickListener.showDirection(business);
            });

            if ( business.getHours() == null ) {
                binding.venueTiming.setVisibility(View.GONE);
            } else  {
                Log.i(getClass().getName(), business.toString());
            }

            Typeface face = Typeface.createFromAsset(getContext().getAssets(),
                    "fonts/roboto/roboto-condensed.bold.ttf");
            binding.venueName.setTypeface(face);
            binding.executePendingBindings();
        }

        private void setRating() {
            Log.i("business.getRating()=", String.valueOf(business.getRating()));
            double rating = business.getRating();

            binding.ratingBar.setEmptyDrawableRes(R.drawable.star_empty);
            binding.ratingBar.setFilledDrawableRes(R.drawable.star_full);
            binding.ratingBar.setRating((float) rating);
        }
    }
}
