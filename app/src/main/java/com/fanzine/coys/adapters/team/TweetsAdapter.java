package com.fanzine.coys.adapters.team;

import android.content.Context;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemLoadingBinding;
import com.fanzine.coys.databinding.ItemTweetBinding;
import com.fanzine.coys.models.Tweet;

import java.util.List;

/**
 * Created by Woland on 12.01.2017.
 */

@Deprecated
public class TweetsAdapter extends BaseAdapter<TweetsAdapter.Holder> {

    private final List<Tweet> data;

    public TweetsAdapter(Context context, List<Tweet> data) {
        super(context);
        this.data = data;
    }

    public Tweet getTweet(int position) {
        return data.get(position);
    }

    @Override
    public TweetsAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int position) {
        if (position == VIEW_TYPE_LOADING) {
            return new Holder(ItemLoadingBinding.inflate(inflater, parent, false));
        } else {
//            if (!TextUtils.isEmpty(data.get(position).getContent()))
            return new Holder(ItemTweetBinding.inflate(inflater, parent, false));
//            else
//                return new Holder(ItemNewsImageBinding.inflate(inflater, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(TweetsAdapter.Holder holder, int position) {
//        if (position < data.size())
//            holder.init(getTweet(position));
//        else {
//            if (NetworkUtils.isNetworkAvailable(getContext()))
//                holder.itemView.setVisibility(View.VISIBLE);
//            else
//                holder.itemView.setVisibility(View.GONE);
//        }
    }

//    @Override
//    public int getItemViewType(int position) {
//        if (position < data.size())
//            return position;
//        else
//            return VIEW_TYPE_LOADING;
//    }

    @Override
    public int getItemCount() {
//        return data.size() + 1;
        return 10;
    }

    public void addNews(List<Tweet> data) {
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    class Holder extends RecyclerView.ViewHolder {

        private final ViewDataBinding binding;

        Holder(ViewDataBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        void init(Tweet tweet) {
//            binding.setVariable(BR.viewModel, new ItemNewsViewModel(getContext(), tweet));
        }

    }
}
