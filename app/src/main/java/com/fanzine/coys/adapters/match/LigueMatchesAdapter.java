package com.fanzine.coys.adapters.match;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemLigueMatchesBinding;
import com.fanzine.coys.models.LigueMatches;

import java.util.ArrayList;
import java.util.List;


public class LigueMatchesAdapter extends BaseAdapter<LigueMatchesAdapter.Holder> {

    private List<LigueMatches> data;


    public LigueMatchesAdapter(Context context) {
        super(context);
        this.data = new ArrayList<>();
    }

    public LigueMatchesAdapter(Context context, List<LigueMatches> data) {
        super(context);
        this.data = data;
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemLigueMatchesBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(LigueMatchesAdapter.Holder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<LigueMatches> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    class Holder extends RecyclerView.ViewHolder {
        private ItemLigueMatchesBinding binding;

        Holder(ItemLigueMatchesBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(int position) {
//            if (binding.getViewModel() == null) {
//                binding.setViewModel(new ItemLigueMatchesViewModel(getContext(), data.get(position)));
//            } else {
//                binding.getViewModel().setLigueMatches(data.get(position));
//            }
//
//            if (binding.rvMatches.getLayoutManager() == null || binding.rvMatches.getAdapter() == null) {
//                binding.rvMatches.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
//                binding.rvMatches.setNestedScrollingEnabled(false);
//            }
//
//            RxView.clicks(binding.btnNavigateToTable).subscribe(aVoid -> {
//
//                BaseLeaguesBarFragment.setLeagueId(data.get(position).getLeagueId());
//                FragmentUtils.changeFragment((AppCompatActivity) getContext(),
//                        R.id.content_frame, BaseTableFragment.newInstance(), false);
//            });
//
//            binding.rvMatches.setAdapter(new MatchAdapter(getContext(), data.get(position).getMatches(), position, this));
//            Glide.with(getContext()).load(data.get(position).getIcon()).into(binding.leagueImage);
//
//            binding.executePendingBindings();
        }

    }

}


