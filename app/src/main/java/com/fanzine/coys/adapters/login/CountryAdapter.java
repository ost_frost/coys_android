package com.fanzine.coys.adapters.login;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemCountryBinding;
import com.fanzine.coys.models.login.Country;
import com.fanzine.coys.viewmodels.items.login.ItemCountryViewModel;

import java.util.List;

/**
 * Created by Woland on 22.03.2017.
 */

public class CountryAdapter extends BaseAdapter<CountryAdapter.Holder> {

    private final List<Country> data;

    public CountryAdapter(Context context, List<Country> data) {
        super(context);

        this.data = data;
    }

    @Override
    public CountryAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemCountryBinding.inflate(inflater, parent, false)) ;
    }

    @Override
    public void onBindViewHolder(CountryAdapter.Holder holder, int position) {
        holder.init(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private final ItemCountryBinding binding;

        public Holder(ItemCountryBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(Country country) {
            binding.setViewModel(new ItemCountryViewModel(getContext(), country));
        }

    }
}
