package com.fanzine.coys.adapters.team.player;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemPhotoPlayerBinding;
import com.fanzine.coys.models.team.PlayerPhoto;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

public class PhotoPlayerAdapter extends BaseAdapter<PhotoPlayerAdapter.Holder> {

    public interface Callback {
        void onPhotoSelected(PlayerPhoto playerPhoto);
    }

    public final List<PlayerPhoto> data = new ArrayList<>();

    private StaggeredGridLayoutManager layoutManager;

    @NonNull
    private final Callback callback;

    private static final int FULL_WIDTH = 1;
    private static final int HALF_WIDTH = 2;

    public PhotoPlayerAdapter(Context context, StaggeredGridLayoutManager layoutManager, @NonNull Callback callback) {
        super(context);
        this.callback = callback;
        this.layoutManager = layoutManager;
    }

    public void refresh(@NonNull List<PlayerPhoto> photos) {
        data.clear();
        data.addAll(photos);
        notifyDataSetChanged();
    }

    @Override
    public PhotoPlayerAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        ItemPhotoPlayerBinding binding = ItemPhotoPlayerBinding.inflate(inflater, parent, false);
        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) binding.getRoot().getLayoutParams();
        binding.getRoot().getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                if (viewType == FULL_WIDTH) {
                    layoutParams.setFullSpan(true);
                } else {
                    layoutParams.width = layoutParams.width / 2;
                    layoutParams.setFullSpan(false);
                }
                layoutManager.invalidateSpanAssignments();
                binding.getRoot().getViewTreeObserver().removeOnPreDrawListener(this);
                return true;
            }
        });
        return new Holder(binding);
    }

    @Override
    public void onBindViewHolder(PhotoPlayerAdapter.Holder holder, int position) {
        if (position < data.size()) {
            holder.bindTo(data.get(position).getUrl());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (getItemCount() > 2) {
            if (position % 3 == 0 || position == 0) {
                return FULL_WIDTH;
            } else return HALF_WIDTH;
        } else {
            return FULL_WIDTH;
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class Holder extends RecyclerView.ViewHolder {

        private final ItemPhotoPlayerBinding binding;

        Holder(ItemPhotoPlayerBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindTo(String link) {
            binding.click.setOnClickListener(v -> {
                if (getAdapterPosition() < data.size()) {
                    PlayerPhoto photo = data.get(getAdapterPosition());
                    if (photo != null) {
                        callback.onPhotoSelected(photo);
                    }
                }
            });

            boolean showDividers = getAdapterPosition() % 3 != 0;

            if (showDividers) {
                double off = ((double) getAdapterPosition()) / 3.0;
                off = off - (long) off;

                if (off < 0.5) {
                    binding.divLeft.setVisibility(View.GONE);
                    binding.divRight.setVisibility(View.VISIBLE);
                } else {
                    binding.divRight.setVisibility(View.GONE);
                    binding.divLeft.setVisibility(View.VISIBLE);
                }
            } else {
                binding.divLeft.setVisibility(View.GONE);
                binding.divRight.setVisibility(View.GONE);
            }

            Glide.with(getContext()).load(link)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(binding.ivImage);
        }
    }
}
