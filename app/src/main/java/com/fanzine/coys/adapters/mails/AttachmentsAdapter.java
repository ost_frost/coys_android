package com.fanzine.coys.adapters.mails;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemAttachmentBinding;
import com.fanzine.coys.models.mails.Attachment;
import com.fanzine.coys.viewmodels.items.ItemAttachmentViewModel;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximdrobonoh on 07.09.17.
 */

public class AttachmentsAdapter extends BaseAdapter<AttachmentsAdapter.Holder> {

    private List<Attachment> data;

    public AttachmentsAdapter(Context context) {
        super(context);

        this.data = new ArrayList<>();
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new AttachmentsAdapter.Holder(ItemAttachmentBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public List<Attachment> getItems() {
        return data;
    }

    class Holder extends RecyclerView.ViewHolder {

        private final ItemAttachmentBinding binding;

        Holder(ItemAttachmentBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }


        void bind(int position) {
            Attachment attachment = data.get(position);

            if (binding.getViewModel() == null)
                binding.setViewModel(new ItemAttachmentViewModel(getContext(), attachment));
            else
                binding.getViewModel().setAttachment(attachment);

            RxView.clicks(binding.removeAttchment).subscribe(aVoid -> {
                if (data.size() > 0) {
                    data.remove(position);

                    notifyDataSetChanged();
                }
            });
        }
    }

    public void add(List list) {
        if (data != null) {
            data.addAll(list);
        } else {
            data = list;
        }
        notifyDataSetChanged();
    }
}
