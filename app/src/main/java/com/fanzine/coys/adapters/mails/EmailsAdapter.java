package com.fanzine.coys.adapters.mails;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.databinding.ItemEmailBinding;
import com.fanzine.coys.databinding.ItemLoadingBinding;
import com.fanzine.coys.models.mails.Label;
import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.networking.EmailDataRequestManager;
import com.fanzine.coys.viewmodels.items.ItemEmailViewModel;
import com.fanzine.mail.MailInfoActivity;
import com.fanzine.mail.adapter.OnItemSelectListener;
import com.fanzine.mail.model.PresetFolders;
import com.fanzine.mail.utils.TimeDateUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class EmailsAdapter extends BaseAdapter<EmailsAdapter.Holder> {


    private List<Mail> data;
    private Set<Mail> selectedItems = new HashSet<>();

    private OnItemSelectListener onItemSelectListener;

    public EmailsAdapter(Context context, List<Mail> data) {
        super(context);
        this.data = data;
    }

    public EmailsAdapter(Context context) {
        super(context);

        data = new ArrayList<>();
    }

    public void clearItems() {
        if (data != null) {
            data.clear();
        }
    }

    public void add(List<Mail> mails) {
        data.clear();
        data.addAll(data);

    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_LOADING) {
            return new Holder(ItemLoadingBinding.inflate(inflater, parent, false));
        } else {
            return new Holder(ItemEmailBinding.inflate(inflater, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(EmailsAdapter.Holder holder, int position) {
        Mail mail = data.get(position);
        holder.bind(mail);
    }

    @Override
    public int getItemViewType(int position) {
        if (position < data.size())
            return position;
        else
            return VIEW_TYPE_LOADING;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setOnItemSelectListener(OnItemSelectListener onItemSelectListener) {

        this.onItemSelectListener = onItemSelectListener;
    }

    boolean clickOnSelectItem(Mail mail) {

        boolean returnResult;
        if (selectedItems.contains(mail)) {
            selectedItems.remove(mail);
            returnResult = false;
        } else {
            selectedItems.add(mail);
            returnResult = true;
        }

        if (onItemSelectListener != null) {
            onItemSelectListener.changeSelectedItems(selectedItems);
        }
        return returnResult;
    }

    class Holder extends RecyclerView.ViewHolder {
        private ItemEmailBinding binding;
        private Mail mail;

        Holder(ItemLoadingBinding binding) {
            super(binding.getRoot());
        }

        Holder(ItemEmailBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(Mail mail) {
            this.mail = mail;

            if (binding.getViewModel() == null)
                binding.setViewModel(new ItemEmailViewModel(getContext(), mail));
            else
                binding.getViewModel().setMail(mail);

            if (mail.getFolder().equals(PresetFolders.SENT)) {
                binding.from.setText(mail.getTo());
            } else {
                binding.from.setText(mail.getFrom());
            }


            if (mail.hasLabel()) {
                Label label = mail.getFirstLabel();
//                binding.emailLabel.setVisibility(View.VISIBLE);
//                binding.emailLabel.setText(label.getText());

                Drawable background = ContextCompat.getDrawable(getContext(), R.drawable.rectangle);
                GradientDrawable gradientDrawable = (GradientDrawable) background;

                int color = Color.parseColor(label.getColor().getHex());
                gradientDrawable.setColor(color);

//                binding.emailLabel.setBackground(gradientDrawable);
            }

            binding.civAvatar.setText(mail.getFrom().substring(0, 1));

            binding.civAvatar.setOnClickListener(view -> {
                boolean isSelected = clickOnSelectItem(mail);
                setItemSelectedMode(isSelected);
            });

            setRead(mail.isRead());

            setFlagged(mail.isFlagged());

            setAttachment(mail.isHasAttachment());

            setTimeAgo(mail.getDateTime().getMillis());

            binding.icStar.setOnClickListener(view -> {

                setFlagged(!mail.isFlagged());
                EmailDataRequestManager.getInstanse().setMailStarred(mail.getFolder(), String.valueOf(mail.getUid()), mail.isFlagged())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                isStarred -> {
                                    setFlagged(isStarred);
                                    mail.setFlagged(isStarred);
                                },
                                throwable -> {
                                    throwable.printStackTrace();

                                    FieldError errors = ErrorResponseParser.getErrors(throwable);
                                    if (errors.isEmpty()) {
                                        String error = view.getContext().getResources().getString(R.string.smth_goes_wrong);
                                    } else {
                                    }
                                });
            });

            binding.getRoot().setOnClickListener(view -> {
                setRead(true);
                mail.setSeen(true);
                Intent intent = new Intent(view.getContext(), MailInfoActivity.class);
                intent.putExtra(MailInfoActivity.MAIL_BODY, mail);

                view.getContext().startActivity(intent);
            });
        }

        private void setTimeAgo(long millis) {

            String timeAgoStr = TimeDateUtils.getTimeAgo(mail.getDateTime().getMillis());
            binding.timeAgo.setText(timeAgoStr);
        }

        private void setItemSelectedMode(boolean isSelected) {
            if (isSelected) {
                binding.laySelected.setVisibility(View.VISIBLE);
            } else {
                binding.laySelected.setVisibility(View.GONE);
            }
        }

        private void setAttachment(boolean isHeavAttach) {
            if (isHeavAttach) {
                binding.icAttachment.setVisibility(View.VISIBLE);
            } else {
                binding.icAttachment.setVisibility(View.INVISIBLE);
            }

        }

        private void setRead(boolean isRead) {
            if (isRead) {
                binding.unreadMark.setVisibility(View.INVISIBLE);
                binding.from.setTypeface(binding.from.getTypeface(), Typeface.NORMAL);
                binding.tvSubject.setTypeface(binding.tvSubject.getTypeface(), Typeface.NORMAL);
            } else {
                binding.unreadMark.setVisibility(View.VISIBLE);
                binding.from.setTypeface(binding.from.getTypeface(), Typeface.BOLD);
                binding.tvSubject.setTypeface(binding.tvSubject.getTypeface(), Typeface.BOLD);
            }
        }

        private void setFlagged(boolean isFlaged) {
            int starRes;

            if (isFlaged) {
                starRes = R.drawable.mail_star;
                binding.icStar.setImageResource(starRes);
            } else {
                starRes = R.drawable.mail_star_empty;
            }
            binding.icStar.setImageResource(starRes);
        }

    }
}
