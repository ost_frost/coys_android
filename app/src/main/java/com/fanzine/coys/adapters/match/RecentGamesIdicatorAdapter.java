package com.fanzine.coys.adapters.match;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemRecentGamesIndicatorBinding;
import com.fanzine.coys.models.analysis.RecentGames;
import com.fanzine.coys.models.analysis.RecentGamesResult;
import com.fanzine.coys.viewmodels.items.match.ItemRecentGamesIndicator;

import java.util.List;

/**
 * Created by mbp on 11/2/17.
 */

public class RecentGamesIdicatorAdapter extends BaseAdapter<RecentGamesIdicatorAdapter.Holder> {

    private List<RecentGames.Item> indicators;

    public RecentGamesIdicatorAdapter(Context context, List<RecentGames.Item> data) {
        super(context);
        indicators = data;
    }

    @Override
    public RecentGamesIdicatorAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new RecentGamesIdicatorAdapter.Holder
                (ItemRecentGamesIndicatorBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.init(indicators.get(position));
    }

    @Override
    public int getItemCount() {
        return indicators.size();
    }

    class Holder extends RecyclerView.ViewHolder {

        private ItemRecentGamesIndicatorBinding binding;

        public Holder(ItemRecentGamesIndicatorBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void init(RecentGames.Item item) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemRecentGamesIndicator(getContext(),
                        item.getGeneralResult()));
            }

            switch (item.getGeneralResult()) {
                case RecentGamesResult.WIN:
                    binding.indicator.setBackgroundResource(R.drawable.circle_green);
                    break;
                case RecentGamesResult.LOSE:
                    binding.indicator.setBackgroundResource(R.drawable.circle_red);
                    break;
                case RecentGamesResult.DRAW:
                    binding.indicator.setBackgroundResource(R.drawable.circle_gray);
                    break;
            }
        }
    }
}
