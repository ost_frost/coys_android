package com.fanzine.coys.adapters.chat;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemContactBinding;
import com.fanzine.coys.fragments.chat.ChatFragment;
import com.fanzine.coys.fragments.chat.UserListFragment;
import com.fanzine.coys.models.Contact;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.viewmodels.items.ItemContactViewModel;
import com.fanzine.chat.ChatSDK;
import com.fanzine.chat.interfaces.FCChatChannelCreateListener;
import com.fanzine.chat.interfaces.FCSuccessListener;
import com.fanzine.chat.interfaces.getting.FCUserGettingListener;
import com.fanzine.chat.models.channels.FCChannel;
import com.fanzine.chat.models.user.FCUser;
import com.jakewharton.rxbinding.view.RxView;
import com.l4digital.fastscroll.FastScroller;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ContactsAdapter extends BaseAdapter<ContactsAdapter.Holder> implements FastScroller.SectionIndexer {

    private final WeakReference<ShowMenuItemListener> listener;
    private List<Contact> data;
    private Map<Contact, FCUser> selected = new HashMap<>();

    private AnimatorSet mSetRightOut;
    private AnimatorSet mSetLeftIn;

    public interface ShowMenuItemListener {
        void selected(Map<Contact, FCUser> selected);
    }

    public ContactsAdapter(Context context, List<Contact> contacts, ShowMenuItemListener listener) {
        super(context);
        this.data = contacts;
        this.listener = new WeakReference<>(listener);

        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.out_animation);
        mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.in_animation);
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemContactBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(ContactsAdapter.Holder holder, int position) {
        holder.init(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public String getSectionText(int position) {
        return data.get(position).getName().substring(0, 1);
    }

    class Holder extends RecyclerView.ViewHolder {

        private boolean mIsBackVisible = false;

        private ItemContactBinding binding;

        Holder(ItemContactBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void init(final Contact contact) {
            if (selected.get(contact) != null) {
                binding.image.setImageAlpha(0);
                binding.imageSelected.setImageAlpha(1);

                mIsBackVisible = true;
            }
            else {
                binding.image.setImageAlpha(1);
                binding.imageSelected.setImageAlpha(0);

                mIsBackVisible = false;
            }

            if (contact.getPhotoUri() == Uri.EMPTY)
                binding.image.setImageResource(R.drawable.user);
            else
                binding.image.setImageURI(contact.getPhotoUri());

            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemContactViewModel(getContext(), contact));
            } else {
                binding.getViewModel().setContact(contact);
            }

            binding.executePendingBindings();

            RxView.clicks(binding.getRoot()).subscribe(aVoid -> {
                if (selected.get(contact) != null) {
                    selected.remove(contact);

                    if (listener != null && listener.get() != null)
                        listener.get().selected(selected);

                    flipCard();
                }
                else {
                    ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);
                    ChatSDK.getInstance().getChatManager().getUserByPhone(contact.getPhoneCutted(), new FCUserGettingListener() {

                        @Override
                        public void onUserLoaded(FCUser fcUser) {
                            pd.dismiss();
                            if (fcUser == null)
                                DialogUtils.showAlertDialog(getContext(), R.string.userNotRegistredInThisApp);
                            else {
                                selected.put(contact, fcUser);

                                if (listener != null && listener.get() != null)
                                    listener.get().selected(selected);

                                flipCard();
                            }
                        }

                        @Override
                        public void onError(Exception e) {
                            pd.dismiss();
                            DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                        }
                    });
                }
            });
        }

        public void flipCard() {
            if (!mIsBackVisible) {
                mSetRightOut.setTarget(binding.image);
                mSetLeftIn.setTarget(binding.imageSelected);
                mSetRightOut.start();
                mSetLeftIn.start();
                mIsBackVisible = true;
            } else {
                mSetRightOut.setTarget(binding.imageSelected);
                mSetLeftIn.setTarget(binding.image);
                mSetRightOut.start();
                mSetLeftIn.start();
                mIsBackVisible = false;
            }
        }
    }

    public void addUsers(List<FCUser> users) {
        ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

        ChatSDK.getInstance().getChatManager().createChannel(users, new FCChatChannelCreateListener() {
            @Override
            public void onChannelCreated(FCChannel fcChannel) {
                pd.dismiss();
                ((AppCompatActivity) getContext()).onBackPressed();
                FragmentUtils.changeFragment((AppCompatActivity) getContext(), R.id.content_frame, ChatFragment.newInstance(fcChannel, true), true);
            }

            @Override
            public void onChannelExists(FCChannel fcChannel) {
                pd.dismiss();
                ((AppCompatActivity) getContext()).onBackPressed();
                FragmentUtils.changeFragment((AppCompatActivity) getContext(), R.id.content_frame, ChatFragment.newInstance(fcChannel, true), true);
            }

            @Override
            public void onError(Exception e) {
                pd.dismiss();
                DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
            }
        });
    }

    public void insertUsers(FCChannel channel, List<FCUser> users) {
        ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

        ChatSDK.getInstance().getChatManager().addUsersToChannel(channel, users, new FCSuccessListener() {

            @Override
            public void onSuccess() {
                pd.dismiss();

                getContext().sendBroadcast(new Intent(UserListFragment.UsersReceiver.ACTION).putParcelableArrayListExtra(UserListFragment.UsersReceiver.USERS, new ArrayList<>(users)));

                ((AppCompatActivity) getContext()).onBackPressed();
            }

            @Override
            public void onError(Exception e) {
                pd.dismiss();
                DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
            }
        });
    }

}
