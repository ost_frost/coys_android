package com.fanzine.coys.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemProfileBinding;
import com.fanzine.coys.viewmodels.items.ItemProfileViewModel;

/**
 * Created by Woland on 13.01.2017.
 */

public class ProfileAdapter extends BaseAdapter<ProfileAdapter.Holder> {

    public ProfileAdapter(Context context) {
        super(context);
    }

    @Override
    public ProfileAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemProfileBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(ProfileAdapter.Holder holder, int position) {
        holder.init(position);
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    class Holder extends RecyclerView.ViewHolder {

        private ItemProfileBinding binding;

        Holder(ItemProfileBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        void init(int position) {
            if (binding.getViewModel() == null)
                binding.setViewModel(new ItemProfileViewModel(getContext()));

            binding.getViewModel().position.set(position);
        }
    }
}
