package com.fanzine.coys.adapters.match;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemCommentaryBinding;
import com.fanzine.coys.databinding.ItemCommentaryFulltimeBinding;
import com.fanzine.coys.models.MatchComment;
import com.fanzine.coys.viewmodels.items.ItemCommentaryViewModel;

import java.util.List;


public class CommentaryAdapter extends BaseAdapter<RecyclerView.ViewHolder> {

    private final List<MatchComment> data;

    private final static int FULLTIME = 2;
    private final static int HALFTIME = 1;
    private final static int MINUTE = 0;

    public CommentaryAdapter(Context context, List<MatchComment> data) {
        super(context);

        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {

        if ( viewType == FULLTIME || viewType == HALFTIME) {
            return new FulltimeHolder(ItemCommentaryFulltimeBinding.inflate(inflater, parent, false));
        }

        return new Holder(ItemCommentaryBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if ( getItemViewType(position) == FULLTIME || getItemViewType(position) == HALFTIME) {
            ((FulltimeHolder)(holder)).init(position);
        } else if ( getItemViewType(position) == MINUTE) {
            ((Holder)(holder)).init(position);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (data.get(position).getMinute().equals("HT")) {
            return HALFTIME;
        }

        if ( data.get(position).getMinute().equals("FT")) {
            return FULLTIME;
        }

        return MINUTE;
    }

    class Holder extends RecyclerView.ViewHolder {
        private ItemCommentaryBinding binding;

        Holder(ItemCommentaryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void init(int position) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemCommentaryViewModel(getContext(), position == getItemCount() - 1));
            } else {
                binding.getViewModel().setLast(position == getItemCount() - 1);
            }
            binding.getViewModel().init(data.get(position));
            binding.executePendingBindings();
        }

    }

    class FulltimeHolder extends RecyclerView.ViewHolder {
        private ItemCommentaryFulltimeBinding binding;

        FulltimeHolder(ItemCommentaryFulltimeBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void init(int position) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemCommentaryViewModel(getContext(), position == getItemCount() - 1));
            } else {
                binding.getViewModel().setLast(position == getItemCount() - 1);
            }
            binding.getViewModel().init(data.get(position));
            binding.executePendingBindings();
        }

    }
}
