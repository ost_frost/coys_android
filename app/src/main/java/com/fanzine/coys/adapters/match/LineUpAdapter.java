package com.fanzine.coys.adapters.match;

import android.content.Context;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.fanzine.coys.BR;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemLinearlayoutBinding;
import com.fanzine.coys.databinding.ItemLineupBinding;
import com.fanzine.coys.databinding.ItemLineupVisitorBinding;
import com.fanzine.coys.models.lineups.Player;
import com.fanzine.coys.models.lineups.PlayerСell;
import com.fanzine.coys.models.lineups.Squad;
import com.fanzine.coys.models.lineups.Formation;
import com.fanzine.coys.models.lineups.Substitution;
import com.fanzine.coys.viewmodels.items.match.ItemLineUpViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Woland on 09.02.2017.
 */

public class LineUpAdapter extends BaseAdapter<LineUpAdapter.Holder> {

    private static final int TABLE_SIZE = 5;

    private final Squad data;
    private final boolean isUp;
    private final List<Substitution> substitutions;
    private int rvHeight;
    private int rvWidth;


    public LineUpAdapter(Context context, Squad data, List<Substitution> substitution, boolean isUp,
                         int rvHeight, int rvWidth) {
        super(context);

        this.data = data;
        this.isUp = isUp;
        this.substitutions = substitution;
        this.rvHeight = rvHeight;
        this.rvWidth = rvWidth;
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemLinearlayoutBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        PlayerСell cell = getCell(position);

        switch (position) {
            case 0:
                List<Player> players = new ArrayList<>();
                players.add(data.getGoalkeeper());
                holder.init(players, cell);
                break;

            case 1:
                holder.init(data.getLine1(), cell);
                break;

            case 2:
                holder.init(data.getLine2(), cell);
                break;

            case 3:
                holder.init(data.getLine3(), cell);
                break;

            case 4:
                holder.init(data.getLine4(), cell);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return TABLE_SIZE;
    }


    public class Holder extends RecyclerView.ViewHolder {

        private final ItemLinearlayoutBinding bindingMain;

        public Holder(ItemLinearlayoutBinding binding) {
            super(binding.getRoot());

            this.bindingMain = binding;
        }

        public void initEmptyRow() {
            bindingMain.layout.removeAllViews();

            for (int i = 0; i < 5; i++) {
                bindingMain.layout.addView(getGapView());
            }
        }


        public void init(List<Player> players, PlayerСell cell) {
            bindingMain.layout.removeAllViews();

            if (players == null) {
                return;
            }

            for (int i = 0; i < players.size(); i++) {
                ViewDataBinding binding = getBinding();
                ItemLineUpViewModel viewModel = new ItemLineUpViewModel(getContext());

                viewModel.player.set(players.get(i));
                viewModel.isUp.set(isUp);
                viewModel.isGone.set(isOut(players.get(i)));

                binding.setVariable(BR.viewModel, viewModel);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        (int) cell.getWidth(), (int) cell.getHeight());

                binding.getRoot().setLayoutParams(params);
                bindingMain.layout.addView(binding.getRoot());
            }

        }

        private boolean isTwoPlayers(List<Player> players) {
            return players.size() == 2;
        }

        private void addPlayers(List<Player> players) {
            for (int i = 0; i < players.size(); i++) {
                bindingMain.layout.addView(getView(players, i));
            }
        }

        private boolean isTreePlayers(List<Player> players) {
            return players.size() == 3;
        }

        private boolean isFourRows() {
            return data.getCountOfLines() == 4;
        }

        private View getGapView() {
            LinearLayout linearLayout = new LinearLayout(getContext());
            linearLayout.setLayoutParams(new LinearLayout.LayoutParams(0, rvHeight / data.getCountOfLines(), 1));

            return linearLayout;
        }

        private View getView(List<Player> players, int position) {
            ViewDataBinding binding = getBinding();

            binding.getRoot().setMinimumWidth(rvWidth / players.size());
            ItemLineUpViewModel viewModel = new ItemLineUpViewModel(getContext());

            viewModel.player.set(players.get(position));
            viewModel.isUp.set(isUp);
            viewModel.isGone.set(isOut(players.get(position)));

            binding.setVariable(BR.viewModel, viewModel);

            int height = (rvHeight / data.getCountOfLines());


            binding.getRoot().setLayoutParams(new LinearLayout.LayoutParams(0, height, 1));

            return binding.getRoot();
        }

        private ViewDataBinding getBinding() {
            if (isUp) {
                return ItemLineupBinding.inflate(getLayoutInflater());
            }
            return ItemLineupVisitorBinding.inflate(getLayoutInflater());
        }
    }

    private boolean isOut(Player player) {
        for (Substitution substitution : substitutions)
            if (substitution != null && player != null && player.getName() != null
                    && substitution.getOffName() != null
                    && substitution.getOffName().equals(player.getName()))
                return true;

        return false;
    }

    private PlayerСell getCell(int position) {
        switch (data.getSchema()) {
            case Formation.FOUR_FOUR_ONE_ONE: {

                if (position == 0) {
                    return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.19f);
                } else if (position == 1) {
                    return new PlayerСell(rvWidth * 0.9f / 4, rvHeight * 0.18f);
                } else if (position == 2) {
                    return new PlayerСell(rvWidth * 0.88f / 4, rvHeight * 0.26f);
                } else if (position == 3) {
                    return new PlayerСell(rvWidth * 0.2f, rvHeight * 0.18f);
                } else {
                    return new PlayerСell(rvWidth * 0.2f, rvHeight * 0.17f);
                }
            }
            case Formation.FOUR_FOUR_TWO: {
                if (position == 0) {
                    return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.19f);
                } else if (position == 1) {
                    return new PlayerСell(rvWidth * 0.9f / 4, rvHeight * 0.19f);
                } else if (position == 2) {
                    return new PlayerСell(rvWidth * 0.88f / 4, rvHeight * 0.27f);
                } else {
                    return new PlayerСell(rvWidth * 0.23f, rvHeight * 0.25f);
                }
            }
            case Formation.FOUR_THREE_THREE: {
                if (position == 0) {
                    return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.19f);
                } else if (position == 1) {
                    return new PlayerСell(rvWidth * 0.9f / 4, rvHeight * 0.19f);
                } else if (position == 2) {
                    return new PlayerСell(rvWidth * 0.7f / 3, rvHeight * 0.27f);
                } else {
                    return new PlayerСell(rvWidth * 0.8f / 3, rvHeight * 0.29f);
                }
            }
            case Formation.FOUR_TWO_THREE_ONE: {
                if (position == 0) {
                    return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.19f);
                } else if (position == 1) {
                    return new PlayerСell(rvWidth * 0.9f / 4, rvHeight * 0.19f);
                } else if (position == 2) {
                    return new PlayerСell(rvWidth * 0.22f, rvHeight * 0.20f);
                } else if (position == 3) {
                    return new PlayerСell(rvWidth * 0.8f / 3, rvHeight * 0.17f);
                } else {
                    return new PlayerСell(rvWidth * 0.2f, rvHeight * 0.19f);
                }
            }
            case Formation.THREE_FIVE_ONE_ONE: {
                if (position == 0) {
                    return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.18f);
                } else if (position == 1) {
                    return new PlayerСell(rvWidth * 0.8f / 3, rvHeight * 0.24f);
                } else if (position == 2) {
                    return new PlayerСell(rvWidth / 5, rvHeight * 0.22f);
                } else if (position == 3) {
                    return new PlayerСell(rvWidth * 0.2f, rvHeight * 0.18f);
                } else {
                    return new PlayerСell(rvWidth * 0.2f, rvHeight * 0.15f);
                }
            }
            case Formation.THREE_FIVE_TWO: {
                if (position == 0) {
                    return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.19f);
                } else if (position == 1) {
                    return new PlayerСell(rvWidth * 0.8f / 3, rvHeight * 0.18f);
                } else if (position == 2) {
                    return new PlayerСell(rvWidth / 5, rvHeight * 0.27f);
                } else {
                    return new PlayerСell(rvWidth * 0.2f, rvHeight * 0.26f);
                }
            }
            case Formation.THREE_FOUR_THREE: {
                if (position == 0) {
                    return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.19f);
                } else if (position == 1) {
                    return new PlayerСell(rvWidth * 0.8f / 3, rvHeight * 0.19f);
                } else if (position == 2) {
                    return new PlayerСell(rvWidth / 4, rvHeight * 0.26f);
                } else {
                    return new PlayerСell(rvWidth * 0.2f, rvHeight * 0.26f);
                }
            }
            case Formation.THREE_FOUR_TWO_ONE: {
                if (position == 0) {
                    return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.19f);
                } else if (position == 1) {
                    return new PlayerСell(rvWidth * 0.8f / 3, rvHeight * 0.22f);
                } else if (position == 2) {
                    return new PlayerСell(rvWidth / 4, rvHeight * 0.24f);
                } else if (position == 3) {
                    return new PlayerСell(rvWidth / 2, rvHeight * 0.16f);
                } else {
                    return new PlayerСell(rvWidth * 0.2f, rvHeight * 0.15f);
                }
            }
            case Formation.THREE_ONE_FOUR_TWO: {
                if (position == 0) {
                    return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.19f);
                } else if (position == 1) {
                    return new PlayerСell(rvWidth * 0.2f, rvHeight * 0.19f);
                } else if (position == 2) {
                    return new PlayerСell(rvWidth / 4, rvHeight * 0.16f);
                } else if (position == 3) {
                    return new PlayerСell(rvWidth * 0.95f / 4, rvHeight * 0.15f);
                } else {
                    return new PlayerСell(rvWidth * 0.2f, rvHeight * 0.26f);
                }
            }
            case Formation.THREE_FOUR_ONE_TWO: {
                if (position == 0) {
                    return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.19f);
                } else if (position == 1) {
                    return new PlayerСell(rvWidth * 0.9f / 3, rvHeight * 0.18f);
                } else if (position == 2) {
                    return new PlayerСell(rvWidth * 0.95f / 4, rvHeight * 0.22f);
                } else if (position == 3) {
                    return new PlayerСell(rvWidth / 2, rvHeight * 0.17f);
                } else {
                    return new PlayerСell(rvWidth * 0.55f / 2, rvHeight * 0.19f);
                }
            }
            case Formation.FOUR_THREE_ONE_TWO: {
                if (position == 0) {
                    return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.19f);
                } else if (position == 1) {
                    return new PlayerСell(rvWidth * 0.95f / 4, rvHeight * 0.19f);
                } else if (position == 2) {
                    return new PlayerСell(rvWidth * 0.8f / 3, rvHeight * 0.20f);
                } else if (position == 3) {
                    return new PlayerСell(rvWidth * 0.2f, rvHeight * 0.18f);
                } else {
                    return new PlayerСell(rvWidth * 0.55f / 2, rvHeight * 0.18f);
                }
            }
            case Formation.FOUR_TWO_TWO_TWO: {
                if (position == 0) {
                    return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.19f);
                } else if (position == 1) {
                    return new PlayerСell(rvWidth * 0.95f / 4, rvHeight * 0.18f);
                } else if (position == 2) {
                    return new PlayerСell(rvWidth * 0.95f / 4, rvHeight * 0.21f);
                } else if (position == 3) {
                    return new PlayerСell(rvWidth * 0.95f/ 2, rvHeight * 0.21f);
                } else {
                    return new PlayerСell(rvWidth * 0.95f / 4, rvHeight * 0.15f);
                }
            }
            case Formation.FOUR_ONE_FOUR_ONE: {
                if (position == 0) {
                    return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.19f);
                } else if (position == 1) {
                    return new PlayerСell(rvWidth * 0.95f / 4, rvHeight * 0.18f);
                } else if (position == 2) {
                    return new PlayerСell(rvWidth * 0.2f, rvHeight * 0.19f);
                } else if (position == 3) {
                    return new PlayerСell(rvWidth * 0.95f / 4, rvHeight * 0.19f);
                } else {
                    return new PlayerСell(rvWidth * 0.2f, rvHeight * 0.19f);
                }
            }

            case Formation.FIVE_TREE_TWO: {
                if (position == 0) {
                    return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.19f);
                } else if (position == 1) {
                    return new PlayerСell(rvWidth * 0.95f / 5, rvHeight * 0.25f);
                } else if (position == 2) {
                    return new PlayerСell(rvWidth * 0.8f / 3, rvHeight * 0.26f);
                } else {
                    return new PlayerСell(rvWidth * 0.2f, rvHeight * 0.24f);
                }
            }
            case Formation.FOUR_FIVE_ONE: {
                if (position == 0) {
                    return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.19f);
                } else if (position == 1) {
                    return new PlayerСell(rvWidth * 0.95f / 4, rvHeight * 0.23f);
                } else if (position == 2) {
                    return new PlayerСell(rvWidth * 0.95f / 5, rvHeight * 0.28f);
                } else {
                    return new PlayerСell(rvWidth * 0.2f, rvHeight * 0.26f);
                }
            }
            case Formation.FIVE_FOUR_ONE: {
                if (position == 0) {
                    return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.19f);
                } else if (position == 1) {
                    return new PlayerСell(rvWidth * 0.95f / 5, rvHeight * 0.19f);
                } else if (position == 2) {
                    return new PlayerСell(rvWidth * 0.95f / 4, rvHeight * 0.31f);
                } else {
                    return new PlayerСell(rvWidth * 0.2f, rvHeight * 0.26f);
                }
            }
            case Formation.FOUR_THREE_TWO_ONE: {
                if (position == 0) {
                    return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.19f);
                } else if (position == 1) {
                    return new PlayerСell(rvWidth * 0.95f / 4, rvHeight * 0.19f);
                } else if (position == 2) {
                    return new PlayerСell(rvWidth * 0.8f / 3, rvHeight * 0.20f);
                } else if (position == 3) {
                    return new PlayerСell(rvWidth * 0.55f / 2, rvHeight * 0.18f);
                } else {
                    return new PlayerСell(rvWidth * 0.2f, rvHeight * 0.18f);
                }
            }
            default:{

                if (data.getCountOfLines() == 4) {
                    if (position == 0) {
                        return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.19f);
                    } else if (position == 1) {
                        return new PlayerСell(rvWidth * 0.95f / 4, rvHeight * 0.22f);
                    } else if (position == 2) {
                        return new PlayerСell(rvWidth * 0.95f / 4, rvHeight * 0.21f);
                    } else if (position == 3) {
                        return new PlayerСell(rvWidth * 0.95f/ 2, rvHeight * 0.19f);
                    } else {
                        return new PlayerСell(rvWidth * 0.95f / 4, rvHeight * 0.14f);
                    }
                } else {
                    if (position == 0) {
                        return new PlayerСell(rvWidth * 0.3f, rvHeight * 0.19f);
                    } else if (position == 1) {
                        return new PlayerСell(rvWidth * 0.9f / 4, rvHeight * 0.25f);
                    } else if (position == 2) {
                        return new PlayerСell(rvWidth * 0.88f / 4, rvHeight * 0.27f);
                    } else {
                        return new PlayerСell(rvWidth * 0.23f, rvHeight * 0.23f);
                    }
                }
            }
        }
    }

}
