package com.fanzine.coys.adapters.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;

/**
 * Created by Woland on 13.01.2017.
 */

public abstract class BaseAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    protected static final int VIEW_TYPE_LOADING = -1;

    private  WeakReference<LayoutInflater> mLayoutInflater;

    public BaseAdapter(Context context) {
        try {


            this.mLayoutInflater = new WeakReference<>(LayoutInflater.from(context));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Context getContext() {
        if (mLayoutInflater != null && mLayoutInflater.get() != null)
            return mLayoutInflater.get().getContext();
        else
            return null;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        return onCreateViewHolder(mLayoutInflater.get(), parent, viewType);
    }

    public abstract VH onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType);

    protected LayoutInflater getLayoutInflater() {
        return mLayoutInflater.get();
    }
}
