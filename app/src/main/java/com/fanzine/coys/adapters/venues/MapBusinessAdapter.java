package com.fanzine.coys.adapters.venues;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemMapBussinessBinding;
import com.fanzine.coys.viewmodels.items.venues.ItemMapBusiness;
import com.yelp.fusion.client.models.Business;

import java.util.List;

/**
 * Created by mbp on 3/23/18.
 */

public class MapBusinessAdapter extends BaseAdapter<MapBusinessAdapter.Holder> {

    int mLastSelectedPosition = 0;

    public interface OnItemClickListener {
        void showSelectedBusiness(Business business);
    }

    private List<Business> mBusiness;
    private OnItemClickListener mOnItemClickListener;

    public MapBusinessAdapter(Context context, List<Business> businessList, OnItemClickListener onItemClickListener) {
        super(context);
        this.mBusiness = businessList;
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemMapBussinessBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Business business = mBusiness.get(position);

        holder.bind(business);
    }

    @Override
    public int getItemCount() {
        return mBusiness.size();
    }

    public void setLastSelectedPosition(int position) {
        this.mLastSelectedPosition = position;
    }

    public int getLastSelectedPosition() { return  mLastSelectedPosition;}

    public Business getBusiness(int position) {
        return mBusiness.get(position);
    }

    public int getPosition(Business business) {
        for ( int i = 0; i < mBusiness.size(); i++ ) {
            if ( business.getId().equals(mBusiness.get(i).getId())) return  i;
        }
        return 0;
    }

    class Holder extends RecyclerView.ViewHolder {
        ItemMapBussinessBinding binding;

        public Holder(ItemMapBussinessBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Business business) {
            float rating = (float) business.getRating();
            int number = getAdapterPosition() + 1;

            configureRatingBar(rating);

            binding.getRoot().setOnClickListener(view -> {
                mOnItemClickListener.showSelectedBusiness(business);
            });
            binding.setViewModel(new ItemMapBusiness(getContext(), business, number));
            binding.executePendingBindings();
        }

        private void configureRatingBar(float rating) {
            binding.brbRating.setEmptyDrawableRes(R.drawable.star_empty);
            binding.brbRating.setFilledDrawableRes(R.drawable.star_full);
            binding.brbRating.setRating(rating);
        }
    }
}
