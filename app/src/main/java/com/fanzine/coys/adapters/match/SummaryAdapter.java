package com.fanzine.coys.adapters.match;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemSummaryBinding;
import com.fanzine.coys.databinding.ItemSummaryScoreBinding;
import com.fanzine.coys.models.summary.Event;
import com.fanzine.coys.viewmodels.items.ItemSummaryViewModel;

import java.util.ArrayList;
import java.util.List;


public class SummaryAdapter extends BaseAdapter<RecyclerView.ViewHolder> {

    private static final int VIEW_MINUTE = 1;
    private static final int VIEW_SCORE = 2;

    private List<Event> data;

    public SummaryAdapter(Context context) {
        super(context);
        this.data = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        if ( viewType == VIEW_MINUTE) {
            return new HolderMinute(ItemSummaryBinding.inflate(inflater, parent, false));
        }

        return new HolderScore(ItemSummaryScoreBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if ( holder instanceof HolderScore )
            ((HolderScore)holder).bind(position);
        else if ( holder instanceof HolderMinute)
            ((HolderMinute)holder).bind(position);
    }

    @Override
    public int getItemViewType(int position) {

        if ( isScore(position)) {
            return VIEW_SCORE;
        }
        return VIEW_MINUTE;
    }

    public void setData(List<Event> data) {
        this.data.clear();
        this.data = data;
        notifyDataSetChanged();
    }

    private boolean isScore(int position) {
        return getItem(position).getType().equals(Event.HALF_TIME) || getItem(position).getType().equals(Event.FULL_TIME);
    }

    private Event getItem(int position) {
        return data.get(position);
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    class HolderMinute extends RecyclerView.ViewHolder {
        private ItemSummaryBinding binding;

        HolderMinute(ItemSummaryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(int position) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemSummaryViewModel(getContext(), position == 0, position == data.size()-1,
                        data.get(position)));
            } else {
                binding.getViewModel().setLast(position == 0);
                binding.getViewModel().setEvent(data.get(position));
            }

            binding.executePendingBindings();
        }
    }

    class HolderScore extends RecyclerView.ViewHolder {
        private ItemSummaryScoreBinding binding;

        HolderScore(ItemSummaryScoreBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(int position) {
            String title = getItem(position).getType() + ": " + getItem(position).getResult();

            if ( getItem(position).getType().equals(Event.FULL_TIME))
                binding.topLine.setVisibility(View.INVISIBLE);

            if ( data.size() == 2 &&  getItem(position).getType().equals(Event.HALF_TIME))
                binding.bottomLine.setVisibility(View.INVISIBLE);

            binding.tvTitle.setText(title);
            binding.executePendingBindings();
        }
    }
}
