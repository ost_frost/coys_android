package com.fanzine.coys.adapters.venues;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemSpinnerVenuesTypesBinding;
import com.fanzine.coys.models.venues.Category;
import com.jakewharton.rxbinding.view.RxView;

import java.util.List;
import java.util.Set;

/**
 * Created by Woland on 17.03.2017.
 */

public class VenuesTypeDialogAdapter extends BaseAdapter<VenuesTypeDialogAdapter.Holder> {

    private final List<Category> filters;
    private final Set<Category> selected;

    public VenuesTypeDialogAdapter(Context context, List<Category> filters, Set<Category> selected) {
        super(context);

        this.filters = filters;
        this.selected = selected;
    }

    @Override
    public VenuesTypeDialogAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemSpinnerVenuesTypesBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(VenuesTypeDialogAdapter.Holder holder, int position) {
        holder.init(filters.get(position));
    }

    public Set<Category> getSelected() {
        return selected;
    }

    @Override
    public int getItemCount() {
        return filters.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        public ItemSpinnerVenuesTypesBinding binding;

        public Holder(ItemSpinnerVenuesTypesBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(Category filter) {
            binding.title.setText(filter.getTitle());

            RxView.clicks(binding.title).subscribe(aVoid -> binding.checkbox.setChecked(!binding.checkbox.isChecked()));

            binding.checkbox.setChecked(selected.contains(filter));
            binding.checkbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked)
                    selected.add(filter);
                else
                    selected.remove(filter);
            });
        }
    }
}
