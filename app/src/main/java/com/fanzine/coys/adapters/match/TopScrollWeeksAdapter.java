package com.fanzine.coys.adapters.match;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemWeekBinding;
import com.fanzine.coys.viewmodels.items.table.ItemWeekViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by maximdrobonoh on 27.09.17.
 */

public class TopScrollWeeksAdapter extends BaseAdapter<TopScrollWeeksAdapter.Holder> {

    private final int width;
    private List<String> data;

    private String[] namesWeeks = {
            "Week 1","Week 2","Week 3","Week 4","Week 5","Week 6","Week 7","Week 8"
    };

    private int selected = 0;

    public TopScrollWeeksAdapter(Context context, int selected) {
        super(context);

        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x-60;
        data = new ArrayList<>();
        initDataCalendar();

        this.selected = selected;
    }

    private void initDataCalendar() {
        Collections.addAll(data, namesWeeks);
    }

    private String getNameWeek(int week) {
        return namesWeeks[week-1];
    }

    @Override
    public TopScrollWeeksAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new TopScrollWeeksAdapter.Holder(ItemWeekBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(TopScrollWeeksAdapter.Holder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setSelected(int selected) {
        this.selected = selected;
        notifyDataSetChanged();
    }

    public String getSelectedDate() {
        return data.get(selected);
    }

    public int getPosition() {
        return selected;
    }

    private View initView(View view) {
        view.setLayoutParams(new RecyclerView.LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT));

        return view;
    }

    class Holder extends RecyclerView.ViewHolder {

        private ItemWeekBinding binding;

        Holder(ItemWeekBinding binding) {
            super(initView(binding.getRoot()));
            this.binding = binding;
        }

        void bind(int position) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemWeekViewModel(getContext(), data.get(position)));
            } else {
                binding.getViewModel().setWeek(data.get(position));
            }
            binding.getViewModel().isSelected.set(position == selected);
            binding.executePendingBindings();
        }

    }
}
