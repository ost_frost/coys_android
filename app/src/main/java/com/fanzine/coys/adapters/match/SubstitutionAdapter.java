package com.fanzine.coys.adapters.match;

import android.content.Context;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.BR;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemSubstitutionGuestBinding;
import com.fanzine.coys.databinding.ItemSubstitutionsBinding;
import com.fanzine.coys.models.lineups.Substitution;
import com.fanzine.coys.viewmodels.items.match.ItemSubstitutionViewModel;

import java.util.ArrayList;
import java.util.List;


public class SubstitutionAdapter extends BaseAdapter<SubstitutionAdapter.Holder> {

    private final boolean isGuest;
    private List<Substitution> data = new ArrayList<>();

    public SubstitutionAdapter(Context context, List<Substitution> data, boolean isGuest) {
        super(context);

        this.data = data;
        this.isGuest = isGuest;
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        if (isGuest)
            return new Holder(ItemSubstitutionGuestBinding.inflate(inflater, parent, false));
        else
            return new Holder(ItemSubstitutionsBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(SubstitutionAdapter.Holder holder, int position) {
        holder.init(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    class Holder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        Holder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void init(Substitution substitution) {
            binding.setVariable(BR.viewModel, new ItemSubstitutionViewModel(getContext(), substitution));
        }

    }
}
