package com.fanzine.coys.adapters.team.player;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.BR;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemPlayerSocialInstagramBinding;
import com.fanzine.coys.models.Social;
import com.fanzine.coys.viewmodels.items.ItemSocialViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbp on 1/13/18.
 */

public class InstagramAdapter extends BaseAdapter<RecyclerView.ViewHolder> {

    private List<Social> data;
    private boolean finished = false;

    public InstagramAdapter(Context context, List<Social> data) {
        super(context);
        this.data = data;
    }

    public InstagramAdapter(Context context) {
        super(context);

        this.data = new ArrayList<>();
    }

    public void addAll(List<Social> newItems) {
        data.addAll(newItems);

        notifyDataSetChanged();
    }

    public void clear() {
        data.clear();

        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent,
                                                      int viewType) {
        ItemPlayerSocialInstagramBinding binding = ItemPlayerSocialInstagramBinding.inflate(inflater, parent,
                false);
        return new Holder(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Social social = getSocial(position);
        ((Holder) holder).init(social);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private Social getSocial(int position) {
        return data.get(position);
    }


    class Holder extends RecyclerView.ViewHolder {

        private final ItemPlayerSocialInstagramBinding binding;

        Holder(ItemPlayerSocialInstagramBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void init(Social social) {
            binding.setVariable(BR.viewModel, new ItemSocialViewModel(getContext(), social));
        }
    }
}