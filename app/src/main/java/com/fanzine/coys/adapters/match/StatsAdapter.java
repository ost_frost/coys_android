package com.fanzine.coys.adapters.match;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemStatsBinding;
import com.fanzine.coys.models.Stats;
import com.fanzine.coys.viewmodels.items.ItemStatsViewModel;

import java.util.ArrayList;
import java.util.List;


public class StatsAdapter extends BaseAdapter<StatsAdapter.Holder> {

    private List<Stats> data;

    private Typeface robotoMedium;

    public StatsAdapter(Context context) {
        super(context);
        this.data = new ArrayList<>();
        this.robotoMedium = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/roboto/Roboto-Medium.ttf");
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemStatsBinding.inflate(inflater, parent, false));
    }

    public void changeData(List<Stats> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(StatsAdapter.Holder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private ItemStatsBinding binding;

        Holder(ItemStatsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.tvTitle.setTypeface(robotoMedium);
        }

        void bind(int position) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemStatsViewModel(getContext(), data.get(position)));
            } else {
                binding.getViewModel().setStats(data.get(position));
            }
        }

    }
}
