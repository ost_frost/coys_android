package com.fanzine.coys.adapters.venues;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemSpinnerVenuesTypesBinding;
import com.jakewharton.rxbinding.view.RxView;

import java.util.Set;

/**
 * Created by Woland on 17.03.2017.
 */

public class VenuesMoneyDialogAdapter extends BaseAdapter<VenuesMoneyDialogAdapter.Holder> {

    private final Set<String> selected;

    public VenuesMoneyDialogAdapter(Context context, Set<String> selected) {
        super(context);

        this.selected = selected;
    }

    @Override
    public VenuesMoneyDialogAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemSpinnerVenuesTypesBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(VenuesMoneyDialogAdapter.Holder holder, int position) {
        switch (position) {
            case 0:
                holder.init("£");
                break;
            case 1:
                holder.init("££");
                break;
            case 2:
                holder.init("£££");
                break;
            case 3:
                holder.init("££££");
                break;
        }
    }

    public Set<String> getSelected() {
        return selected;
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class Holder extends RecyclerView.ViewHolder {

        public ItemSpinnerVenuesTypesBinding binding;

        public Holder(ItemSpinnerVenuesTypesBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(String value) {
            binding.title.setText(value);

            RxView.clicks(binding.title).subscribe(aVoid -> binding.checkbox.setChecked(!binding.checkbox.isChecked()));

            binding.checkbox.setChecked(selected.contains(Integer.toString(value.length())));
            binding.checkbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked)
                    selected.add(Integer.toString(value.length()));
                else
                    selected.remove(Integer.toString(value.length()));
            });
        }
    }
}
