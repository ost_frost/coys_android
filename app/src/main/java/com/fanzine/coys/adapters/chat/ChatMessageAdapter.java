package com.fanzine.coys.adapters.chat;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemMessageBinding;
import com.fanzine.coys.fragments.team.FullScreenPhotoFragment;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.viewmodels.items.chat.ItemMessageViewModel;
import com.fanzine.chat.ChatSDK;
import com.fanzine.chat.interfaces.FCChannelMessageListener;
import com.fanzine.chat.interfaces.getting.FCAttachmentsListener;
import com.fanzine.chat.interfaces.sync.FCUserSyncListener;
import com.fanzine.chat.models.channels.FCChannel;
import com.fanzine.chat.models.message.FCAttachment;
import com.fanzine.chat.models.message.FCMessage;
import com.fanzine.chat.models.user.FCUser;
import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.functions.Action1;

/**
 * Created by Woland on 12.04.2017.
 */

public class ChatMessageAdapter extends BaseAdapter<ChatMessageAdapter.Holder> implements FCChannelMessageListener {

    private final List<FCMessage> data;
    private final Map<String, FCUser> map = new HashMap<>();
    private final FCChannel channel;

    private final LoadedListener listener;

    public interface LoadedListener {
        void loaded();

        void added();
    }

    public ChatMessageAdapter(Context context, FCChannel channel, LoadedListener listener) {
        super(context);

        data = new ArrayList<>();
        this.channel = channel;

        this.listener = listener;
    }

    @Override
    public ChatMessageAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemMessageBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(ChatMessageAdapter.Holder holder, int position) {
        holder.init(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onNewMessageReceived(FCMessage fcMessage) {
        if (!data.contains(fcMessage)) {
            data.add(0, fcMessage);
            notifyItemInserted(0);

            if (data.size() == 1)
                listener.loaded();

            listener.added();
        }
    }

    @Override
    public void onMessageDeleted(FCMessage fcMessage) {
        for (int i = 0; i < data.size(); ++i) {
            if (data.get(i).getUid().equals(fcMessage.getUid())) {
                data.remove(i);
                notifyItemRemoved(i);
            }
        }
    }

    @Override
    public void onError(Exception e) {

    }

    public class Holder extends RecyclerView.ViewHolder {

        private final ItemMessageBinding binding;

        public Holder(ItemMessageBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(FCMessage message) {
            FCUser user = map.get(message.getUser());
            ItemMessageViewModel viewModel;

            if (binding.getViewModel() == null) {
                viewModel = new ItemMessageViewModel(getContext());
                binding.setViewModel(viewModel);
            } else {
                viewModel = binding.getViewModel();
            }

            viewModel.init(message, channel.isOneToOne());

            if (!viewModel.isText.get()) {
                if (message.getAttachments() != null && message.getAttachments().size() > 0) {
                    Glide.with(getContext())
                            .using(new FirebaseImageLoader())
                            .load(message.getAttachments().get(0).getReference())
                            .into(viewModel.isSelf.get() ? binding.image1 : binding.image2);

                    RxView.clicks(viewModel.isSelf.get() ? binding.image1 : binding.image2).subscribe(aVoid ->
                            FragmentUtils.changeFragment((AppCompatActivity) getContext(), R.id.content_frame, FullScreenPhotoFragment.newInstance(message.getAttachments().get(0)), true));
                } else
                    message.syncAttachmentsList(new FCAttachmentsListener() {
                        @Override
                        public void onAttachmentsLoaded(List<FCAttachment> list) {
                            if (list.size() > 0) {
                                Glide.with(getContext())
                                        .using(new FirebaseImageLoader())
                                        .load(list.get(0).getReference())
                                        .into(viewModel.isSelf.get() ? binding.image1 : binding.image2);

                                RxView.clicks(viewModel.isSelf.get() ? binding.image1 : binding.image2).subscribe(aVoid ->
                                        FragmentUtils.changeFragment((AppCompatActivity) getContext(), R.id.content_frame, FullScreenPhotoFragment.newInstance(list.get(0)), true));
                            }
                        }

                        @Override
                        public void onError(Exception e) {
                            viewModel.isText.set(true);
                        }
                    });
            }

            if (!channel.isOneToOne()) {
                if (user != null)
                    viewModel.user.set(user);
                else
                    getUser(viewModel, message);
            }

            RxView.clicks(binding.message1).subscribe(aVoid -> viewModel.showTime());
            RxView.clicks(binding.message2).subscribe(aVoid -> viewModel.showTime());

            Action1 action = aVoid -> new AlertDialog.Builder(getContext()).setMessage(R.string.doYouWantDelMess)
                            .setNegativeButton(R.string.cancel, null)
                            .setPositiveButton(R.string.ok, (dialog, which) -> ChatSDK.getInstance().getChatManager().deleteMessage(channel, message)).show();

            RxView.longClicks(binding.image1).subscribe(action);
            RxView.longClicks(binding.message1).subscribe(action);

            binding.executePendingBindings();
        }

        private void getUser(ItemMessageViewModel viewModel, FCMessage message) {
            new FCUser(message.getUser()).sync(new FCUserSyncListener() {
                @Override
                public void onError(Exception e) {

                }

                @Override
                public void onSync(FCUser fcUser) {
                    viewModel.user.set(fcUser);
                    map.put(message.getUser(), fcUser);
                }
            });
        }

    }
}
