package com.fanzine.coys.adapters.match;

import android.content.Context;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemScorerGuestBinding;
import com.fanzine.coys.databinding.ItemScorerHomeBinding;

/**
 * Created by Woland on 12.01.2017.
 */

public class ScorersAdapter extends BaseAdapter<ScorersAdapter.Holder> {

    private final boolean isHome;
    private final static int TYPE_HOME = 1;
    private final static int TYPE_GUEST = 2;

    public ScorersAdapter(Context context, boolean isHome) {
        super(context);
        this.isHome = isHome;
    }

    @Override
    public ScorersAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        Holder holder = null;

        if(viewType == TYPE_HOME) {
            holder =  new Holder(ItemScorerHomeBinding.inflate(inflater, parent, false));
        } else {
            holder =  new Holder(ItemScorerGuestBinding.inflate(inflater, parent, false));
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(ScorersAdapter.Holder holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemViewType(int position) {
        return isHome ? TYPE_HOME : TYPE_GUEST;
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    class Holder extends RecyclerView.ViewHolder {

        private final ViewDataBinding binding;

        Holder(ViewDataBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        void bind() {
        }

    }
}
