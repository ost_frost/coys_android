package com.fanzine.coys.adapters.table;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemTablePlayerFilterBinding;
import com.fanzine.coys.models.table.PlayerStatsFilter;
import com.fanzine.coys.viewmodels.items.table.ItemTablePlayerFilterViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximdrobonoh on 29.09.17.
 */

public class PlayerFilterAdapter extends BaseAdapter<PlayerFilterAdapter.ViewHolder> {

    private List<PlayerStatsFilter> data;

    public PlayerFilterAdapter(Context context) {
        super(context);

        data = new ArrayList<>();

        //mock list of filters
        String[] mockData = { "Goals", "Goals Conceded", "Clean Sheets", "Red Cards",
                "Yellow Cards", "Fouls", "Offsides", "Shots", "Shots On Target", "Shots Off Target",
                "Passes"};

        for ( int i = 0; i < mockData.length; i++) {
            PlayerStatsFilter filter = new PlayerStatsFilter();
            filter.setName(mockData[i]);
            data.add(filter);
        }
    }

    @Override
    public PlayerFilterAdapter.ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new PlayerFilterAdapter.ViewHolder(ItemTablePlayerFilterBinding.inflate(inflater,parent, false));
    }

    @Override
    public void onBindViewHolder(PlayerFilterAdapter.ViewHolder holder, int position) {
        holder.init(data.get(position), position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ItemTablePlayerFilterBinding binding;

        public ViewHolder(ItemTablePlayerFilterBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(PlayerStatsFilter playerStatsFilter, int position) {
            if ( binding.getViewModel() == null) {
                binding.setViewModel(new ItemTablePlayerFilterViewModel(getContext(), playerStatsFilter));
            }
        }
    }
}
