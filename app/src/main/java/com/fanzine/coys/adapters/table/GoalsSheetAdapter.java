package com.fanzine.coys.adapters.table;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemSheetGoalsBinding;
import com.fanzine.coys.models.table.PlayerStats;
import com.fanzine.coys.utils.RecyclerItemClickListener;
import com.fanzine.coys.viewmodels.items.ItemSheetGoalViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Woland on 17.01.2017.
 */

public class GoalsSheetAdapter extends BaseAdapter<GoalsSheetAdapter.Holder> implements RecyclerItemClickListener.OnItemClickListener {

    private int selected = -1;

    private List<PlayerStats> data = new ArrayList<>();

    public GoalsSheetAdapter(Context context) {
        super(context);
    }

    public void setData(List<PlayerStats> data) {
        this.data = data;
        selected = -1;
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemSheetGoalsBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.init(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onItemClick(View view, int position) {
        if (selected != position)
            selected = position;
        else
            selected = -1;

        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private final ItemSheetGoalsBinding binding;

        public Holder(ItemSheetGoalsBinding inflate) {
            super(inflate.getRoot());

            binding = inflate;
        }

        public void init(int position) {
            if (binding.getViewModel() == null)
                binding.setViewModel(new ItemSheetGoalViewModel(getContext()));

            binding.getViewModel().isClicked.set(position == selected);
            binding.getViewModel().player.set(data.get(position));
            binding.getViewModel().position.set(position + 1);

            binding.executePendingBindings();
        }
    }
}
