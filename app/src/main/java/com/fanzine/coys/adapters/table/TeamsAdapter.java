package com.fanzine.coys.adapters.table;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemTableTeamScoreBinding;
import com.fanzine.coys.models.table.TeamScore;
import com.fanzine.coys.viewmodels.items.table.ItemTableTeamScoreViewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximdrobonoh on 29.09.17.
 */

public class TeamsAdapter extends BaseAdapter<TeamsAdapter.ViewHolder> {

    private List<TeamScore> data;

    public TeamsAdapter(Context context) {
        super(context);

        data = new ArrayList<>();
    }

    public TeamsAdapter(Context context, List<TeamScore> data) {
        super(context);
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new ViewHolder(ItemTableTeamScoreBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.init(data.get(position), position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<TeamScore> data) {
        this.data.addAll(data);

        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ItemTableTeamScoreBinding binding;

        public ViewHolder(ItemTableTeamScoreBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(TeamScore teamScore, int position) {
            binding.setViewModel(new ItemTableTeamScoreViewModel(getContext(), teamScore));

            Picasso.with(getContext()).load(teamScore.getTeamIcon()).noFade().into(binding.icon);
            binding.getViewModel().teamScore.set(teamScore);
        }
    }
}
