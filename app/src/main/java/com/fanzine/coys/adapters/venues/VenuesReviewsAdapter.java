package com.fanzine.coys.adapters.venues;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemVenuesReviewsNewBinding;
import com.fanzine.coys.viewmodels.items.venues.ItemReviewsVenusViewModel;
import com.yelp.fusion.client.models.Review;

import java.util.List;

/**
 * Created by Woland on 15.03.2017.
 */

public class VenuesReviewsAdapter extends BaseAdapter<VenuesReviewsAdapter.Holder> {

    private final List<Review> data;

    public VenuesReviewsAdapter(Context context, List<Review> reviews) {
        super(context);

        this.data = reviews;
    }

    @Override
    public VenuesReviewsAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemVenuesReviewsNewBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(VenuesReviewsAdapter.Holder holder, int position) {
        holder.init(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder {

        private final ItemVenuesReviewsNewBinding binding;

        public Holder(ItemVenuesReviewsNewBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(Review review) {
            if (binding.getViewModel() == null)
                binding.setViewModel(new ItemReviewsVenusViewModel(getContext(), review));
            else
                binding.getViewModel().review.set(review);
        }
    }
}
