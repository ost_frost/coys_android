package com.fanzine.coys.adapters.team;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.CustomTabBinding;
import com.fanzine.coys.fragments.team.PhotoPlayerFragment;
import com.fanzine.coys.fragments.team.TweetsFragment;
import com.fanzine.coys.fragments.team.PlayerListVideoFragment;
import com.fanzine.coys.models.team.PlayerInfo;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;

/**
 * Created by Siarhei on 24.01.2017.
 */

public class PlayerInfoPagerAdapter extends FragmentPagerAdapter {

    private final PlayerInfo player;
    private String tabTitles[] = new String[]{"Profile", "Photos", "Videos", "Tweets"};
    private int[] imageResId = {R.drawable.user_player, R.drawable.ic_camera, R.drawable.ic_video, R.drawable.ic_tweets};
    private WeakReference<Context> context;

    public PlayerInfoPagerAdapter(Context context, FragmentManager fm, PlayerInfo player) {
        super(fm);
        this.context = new WeakReference<>(context);
        this.player = player;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                //fragment = PagerFragment.newInstance(player);
                break;
            case 1:
                fragment = PhotoPlayerFragment.newInstance(player.getPhotos());
                break;
            case 2:
                fragment = PlayerListVideoFragment.newInstance(player.getVideos());
                break;
            case 3:
                fragment = TweetsFragment.newInstance(player.getTwitter());
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }

    public View getTabView(int position) {
        CustomTabBinding binding = CustomTabBinding.inflate(LayoutInflater.from(context.get()));
        binding.getRoot().setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        Picasso.with(context.get()).load(imageResId[position]).into(binding.icon);
        binding.tvName.setText(tabTitles[position]);

        switch (position) {
            case 1:
                binding.tvCount.setText(Integer.toString(player.getPhotos().size()));
                break;
            case 2:
                binding.tvCount.setText(Integer.toString(player.getVideos().size()));
                break;
            case 3:
                break;
        }

        if (position == 0 || position == 3) {
            binding.tvCount.setVisibility(View.GONE);
        }

        return binding.getRoot();
    }
}
