package com.fanzine.coys.adapters.match;

import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.ItemLigueMatchesBinding;
import com.fanzine.coys.databinding.ItemMatchBinding;
import com.fanzine.coys.fragments.base.BaseLeaguesBarFragment;
import com.fanzine.coys.fragments.match.OnMatchClickListener;
import com.fanzine.coys.fragments.table.BaseTableFragment;
import com.fanzine.coys.models.LigueMatches;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.utils.TimeAgoUtil;
import com.fanzine.coys.viewmodels.items.ItemLigueMatchesViewModel;
import com.fanzine.coys.viewmodels.items.ItemMatchViewModel;
import com.bumptech.glide.Glide;
import com.jakewharton.rxbinding.view.RxView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

import static com.fanzine.coys.App.getContext;

/**
 * Created by mbp on 12/12/17.
 */

public class LeagueSection extends StatelessSection {

    public interface DataSetChangedListener {
        void dataSetChanged();

        void sectionChanged(LeagueSection section);
    }

    private LigueMatches ligueMatches;
    private OnMatchClickListener onMatchClickListener;
    private AppCompatActivity activity;
    private DataSetChangedListener mOnDataSetChangedListener;


    public LeagueSection(AppCompatActivity activity,
                         LigueMatches ligueMatches, OnMatchClickListener onMatchClickListener,
                         DataSetChangedListener onDataSetChangeListener) {
        super(new SectionParameters.Builder(R.layout.item_match)
                .headerResourceId(R.layout.item_ligue_matches)
                .build());

        this.activity = activity;
        this.ligueMatches = ligueMatches;
        this.onMatchClickListener = onMatchClickListener;
        this.mOnDataSetChangedListener = onDataSetChangeListener;

    }


    @Override
    public int getContentItemsTotal() {
        return ligueMatches.getMatches().size();
    }

    @Override
    public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
        return new HeaderHolder(ItemLigueMatchesBinding.bind(view));
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        return new ItemHolder(ItemMatchBinding.bind(view));
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
        ((HeaderHolder) holder).bind(ligueMatches);
    }

    @Override
    public void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ItemHolder) holder).bind(position);
    }


    class HeaderHolder extends RecyclerView.ViewHolder {

        ItemLigueMatchesBinding binding;
        ItemLigueMatchesViewModel viewModel;

        HeaderHolder(ItemLigueMatchesBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            EventBus.getDefault().register(this);
        }

        /*
          *Detect by first match in league whether league bell notification is active
         */
        public void bind(LigueMatches item) {
            boolean isNotificationActive = false;

            if (ligueMatches.getMatches().size() > 0) {
                isNotificationActive = ligueMatches.getMatches().get(0).isHasNotification();
            }


            if ( isNotificationActive) {
                binding.ivLeagueBellIcon.setImageResource(R.drawable.ic_bell_red);
            }else {
                binding.ivLeagueBellIcon.setImageResource(R.drawable.ic_match_notifacation_black);
            }

            if (!onMatchClickListener.isToday() && isLast(item)) {
                binding.llBell.setVisibility(View.INVISIBLE);
            }

            viewModel = new ItemLigueMatchesViewModel(activity, ligueMatches, isNotificationActive, this::changeBellIconState);

            binding.setViewModel(viewModel);

            RxView.clicks(binding.llBell).subscribe(aVoid -> {
                //todo::load notifications settigns for first match. @OstFrost fix
                viewModel.loadNotificationSettings(item.getMatches().get(0));
            });

            RxView.clicks(binding.btnNavigateToTable).subscribe(aVoid -> {
                BaseLeaguesBarFragment.setLeagueId(item.getLeagueId());
                FragmentUtils.changeFragment(activity,
                        R.id.content_frame, BaseTableFragment.newInstance(item.getLeagueId()), true);
            });

            Glide.with(getContext()).load(item.getIcon()).override(35, 35)
                    .into(binding.leagueImage);
        }

        @Subscribe(threadMode = ThreadMode.MAIN)
        public void updateUI(Match match) {

            for (Match i : ligueMatches.getMatches()) {
                if (i.getId() == match.getId()) {
                    i.setHasNotification(match.isHasNotification());
                }
            }

            viewModel.isNotificationActive.set(isAllMatchesHasNotifications());
            mOnDataSetChangedListener.dataSetChanged();
        }


        private boolean isAllMatchesHasNotifications() {
            for (Match i : ligueMatches.getMatches()) {
                if (!i.isHasNotification()) return false;
            }
            return true;
        }

        private boolean isLast(LigueMatches item) {
            for (Match match : item.getMatches()) {
                if (match.getState() != Match.LAST) return false;
            }
            return true;
        }

        private void changeBellIconState(boolean isActive) {
            for (Match match : ligueMatches.getMatches()) {
                match.setHasNotification(isActive);
            }
            mOnDataSetChangedListener.dataSetChanged();
        }
    }

    class ItemHolder extends RecyclerView.ViewHolder {

        private ItemMatchBinding binding;

        ItemHolder(ItemMatchBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(int position) {

            Match match = ligueMatches.getMatches().get(position);

            ItemMatchViewModel viewModel = new ItemMatchViewModel(activity, match);

            binding.setViewModel(viewModel);

            if (!onMatchClickListener.isToday() && match.getState() == Match.LAST) {
                binding.llBell.setVisibility(View.INVISIBLE);
            }

            if (match.isTeamsExist()) {
                Glide.with(getContext()).load(match.getHomeTeam().getIcon())
                        .into(binding.homeTeamIcon);

                Glide.with(getContext()).load(match.getGuestTeam().getIcon())
                        .into(binding.guestTeamIcon);
            }

            RxView.clicks(binding.getRoot()).subscribe(aVoid -> {
                onMatchClickListener.onClick(match, ligueMatches.getName());
            });

            binding.matchVenue.setPaintFlags(binding.matchVenue.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


            if (onMatchClickListener.isToday()) {
                if (isInteger(match.getCurrentTimeMatch()))
                    displayTimer(match.getCurrentTimeMatch(), Integer.valueOf(match.
                            getCurrentTimeMatch()));
                else if (match.getCurrentTimeMatch().equals("FT")) {
                    displayTimer("FT", 0);
                } else if (match.getCurrentTimeMatch().equals("HT")) {
                    displayTimer("HT", 0);
                } else {
                    binding.timer.setVisibility(View.INVISIBLE);
                    binding.timerMinute.setVisibility(View.INVISIBLE);
                }
            } else if (match.getCurrentTimeMatch().equals("FT")) {
                displayTimer("FT", 0);
            } else if (TimeAgoUtil.isPastDate(onMatchClickListener.getCurrentDate())) {
                displayTimer("FT",0);
            }
        }

        private void displayTimer(String value, int degree) {
            binding.timer.setVisibility(View.VISIBLE);
            binding.timerMinute.setVisibility(View.VISIBLE);
            binding.timerMinute.setText(value);

            binding.timer.setDegree(degree);
            binding.timer.invalidate();
        }
    }

    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
// s is a valid integer
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

}
