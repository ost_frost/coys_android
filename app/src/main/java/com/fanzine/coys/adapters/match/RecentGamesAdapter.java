package com.fanzine.coys.adapters.match;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemRecentGamesBinding;
import com.fanzine.coys.models.analysis.RecentGames;
import com.fanzine.coys.models.analysis.RecentGamesResult;
import com.fanzine.coys.viewmodels.items.match.ItemAnalysisRecentGame;

import java.util.List;

/**
 * Created by maximdrobonoh on 06.10.17.
 */

public class RecentGamesAdapter extends BaseAdapter<RecentGamesAdapter.Holder> {

    private List<RecentGames.Item> data;
    private boolean isLocal;
    private String teamTitle;

    public RecentGamesAdapter(Context context, List<RecentGames.Item> data,
                              boolean isLocal, String teamTitle) {
        super(context);
        this.data = data;
        this.isLocal = isLocal;
        this.teamTitle = teamTitle;
    }

    @Override
    public RecentGamesAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new RecentGamesAdapter.Holder(ItemRecentGamesBinding.inflate(inflater, parent, false));
    }

    public void changeData(List<RecentGames.Item> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecentGamesAdapter.Holder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private ItemRecentGamesBinding binding;

        Holder(ItemRecentGamesBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(int position) {
            RecentGames.Item item = data.get(position);

            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemAnalysisRecentGame(getContext(), item));
            } else {
                binding.getViewModel().setRecentGames(item);
            }

            if (isLocal) {
                binding.teamTitle.setText(getTeamTitle(item));
            } else {
                binding.teamTitle.setText(getTeamTitle(item));
            }

            switch (item.getGeneralResult()) {
                case RecentGamesResult.WIN:
                    binding.result.setTextColor(Color.parseColor("#7ED321"));
                    break;
                case RecentGamesResult.LOSE:
                    binding.result.setTextColor(Color.parseColor("#E40000"));
                    break;
                case RecentGamesResult.DRAW:
                    binding.result.setTextColor(Color.parseColor("#C6C6C6"));
                    break;
            }

        }

        private String getTeamTitle(RecentGames.Item item) {
            String home = "(A) ";
            String away = "(H) ";

            if (!item.getAway().equals(teamTitle)) {
                return away + item.getAway();
            }
            return home + item.getHome();
        }
    }
}
