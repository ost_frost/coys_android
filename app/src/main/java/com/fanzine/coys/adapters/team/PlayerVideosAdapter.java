package com.fanzine.coys.adapters.team;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.VideoAdapter;
import com.fanzine.coys.databinding.ItemVideoBinding;
import com.fanzine.coys.models.Video;
import com.fanzine.coys.viewmodels.items.ItemVideoViewModel;

import java.util.List;

/**
 * Created by mbp on 1/12/18.
 */

public class PlayerVideosAdapter extends VideoAdapter {

    @NonNull
    private final String playerName;

    public PlayerVideosAdapter(Context context, List<Video> data, @NonNull String playerName) {
        super(context, data);
        this.playerName = playerName;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemVideoBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((Holder) holder).bind(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class Holder extends VideoAdapter.Holder {

        Holder(ItemVideoBinding binding) {
            super(binding);
        }

        @Override
        public void bind(int position) {
            ItemVideoViewModel videoViewModel = new ItemVideoViewModel(getContext());

            videoViewModel.setHeaderTitle(playerName);

            binding.setViewModel(videoViewModel);

            binding.getViewModel().setVideoItem(data.get(position));
            binding.executePendingBindings();
        }
    }
}
