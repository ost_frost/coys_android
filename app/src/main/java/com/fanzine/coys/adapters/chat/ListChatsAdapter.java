package com.fanzine.coys.adapters.chat;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemChatBinding;
import com.fanzine.coys.fragments.chat.ChatFragment;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.viewmodels.items.chat.ItemChatViewModel;
import com.fanzine.chat.ChatSDK;
import com.fanzine.chat.interfaces.FCChatChannelMessageListener;
import com.fanzine.chat.interfaces.FCUsersListener;
import com.fanzine.chat.interfaces.sync.FCUserSyncListener;
import com.fanzine.chat.models.channels.FCChannel;
import com.fanzine.chat.models.message.FCMessage;
import com.fanzine.chat.models.user.FCUser;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.fanzine.chat.models.message.FCMessage.TYPE_PICTURE;


public class ListChatsAdapter extends BaseAdapter<ListChatsAdapter.Holder> {

    private final List<FCChannel> data;
    private static Map<String, List<FCUser>> mapUsers = new HashMap<>();
    private static Map<String, FCMessage> mapMessages = new HashMap<>();

    public ListChatsAdapter(Context context, List<FCChannel> list) {
        super(context);

        data = list;
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemChatBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(ListChatsAdapter.Holder holder, int position) {
        holder.init(data.get(position));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private ItemChatBinding binding;

        Holder(ItemChatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void init(FCChannel channel) {
            ItemChatViewModel viewModel;

            if (binding.getViewModel() == null) {
                viewModel = new ItemChatViewModel(getContext());
                binding.setViewModel(viewModel);
            } else {
                viewModel = binding.getViewModel();
            }

            if (mapUsers.get(channel.getUid()) == null)
                loadUsers(channel, viewModel);
            else {
                String title = "";
                for (FCUser user : mapUsers.get(channel.getUid()))
                    if (TextUtils.isEmpty(title))
                        title += user.getFirstName() + " " + user.getLastName();
                    else
                        title += ", " + user.getFirstName() + " " + user.getLastName();

                viewModel.title.set(title);
            }

            if (!ChatSDK.getInstance().getBlacklistManager().isUserInBlackList(channel)) {
                if (mapMessages.get(channel.getUid()) != null) {
                    FCMessage message = mapMessages.get(channel.getUid());
                    if (message.getType().equals(TYPE_PICTURE))
                        viewModel.message.set(getContext().getString(R.string.picture));
                    else
                        viewModel.message.set(message.getMessage());
                    viewModel.time.set(message.getTimestamp());
                }

                viewModel.isBanned.set(false);

                loadMessage(channel, viewModel);
            } else {
                viewModel.isBanned.set(true);
                viewModel.message.set(getContext().getString(R.string.yourAreBannedFromThisChat));
            }

            RxView.clicks(binding.getRoot()).subscribe(aVoid -> {
                if (!ChatSDK.getInstance().getBlacklistManager().isUserInBlackList(channel))
                    FragmentUtils.changeFragment((AppCompatActivity) getContext(), R.id.content_frame, ChatFragment.newInstance(channel, mapMessages.get(channel.getUid()) == null), true);
                else
                    DialogUtils.showAlertDialog(getContext(), R.string.yourAreBannedFromThisChat);
            });
            binding.executePendingBindings();
        }

        private void loadMessage(FCChannel channel, ItemChatViewModel viewModel) {
            ChatSDK.getInstance().getChatManager().getChannelMessages(channel, new FCChatChannelMessageListener() {
                @Override
                public void onMessagesReceived(List<FCMessage> list) {
                    if (list.size() > 0) {
                        FCMessage message = list.get(list.size() - 1);
                        if (message.getType().equals(TYPE_PICTURE))
                            viewModel.message.set(getContext().getString(R.string.picture));
                        else
                            viewModel.message.set(message.getMessage());
                        viewModel.time.set(message.getTimestamp());

                        mapMessages.put(channel.getUid(), message);
                    }
                }

                @Override
                public void onError(Exception e) {

                }
            });
        }

        private void loadUsers(FCChannel channel, ItemChatViewModel viewModel) {
            ChatSDK.getInstance().getChatManager().getAllChatUsers(channel, new FCUsersListener() {
                @Override
                public void onUsersReceived(List<FCUser> list) {
                    List<FCUser> users = new ArrayList<>();

                    for (FCUser user : list) {
                        if (!user.getUid().equals(ChatSDK.getInstance().getCurrentSession().getCurrentUser().getUid())) {
                            user.sync(new FCUserSyncListener() {
                                @Override
                                public void onError(Exception e) {

                                }

                                @Override
                                public void onSync(FCUser fcUser) {
                                    if (!TextUtils.isEmpty(viewModel.title.get()))
                                        viewModel.title.set(viewModel.title.get() + ", " + user.getFirstName() + " " + user.getLastName());
                                    else
                                        viewModel.title.set(user.getFirstName() + " " + user.getLastName());
                                }
                            });
                            users.add(user);

                            if (users.size() >= 5)
                                break;
                        }
                    }

                    mapUsers.put(channel.getUid(), users);
                }

                @Override
                public void onError(Exception e) {

                }
            });
        }
    }
}
