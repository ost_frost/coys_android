package com.fanzine.coys.adapters.table;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemTableTeamFilterBinding;
import com.fanzine.coys.interfaces.FilterClickListener;
import com.fanzine.coys.models.table.Filter;
import com.fanzine.coys.viewmodels.items.table.ItemTableTeamFilterViewModel;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximdrobonoh on 29.09.17.
 */

public class FilterAdapter extends BaseAdapter<FilterAdapter.ViewHolder> {

    private List<Filter> data;

    private Filter currentItem;

    private FilterClickListener filterClickListener;

    public FilterAdapter(Context context) {
        super(context);

        data = new ArrayList<>();

        //mock list of filters
        String[] mockData = {"Goals", "Goals Conceded", "Clean Sheets", "Red Cards",
                "Yellow Cards", "Fouls", "Offsides", "Shots", "Shots On Target", "Shots Off Target",
                "Passes"};

        for (int i = 0; i < mockData.length; i++) {
            Filter filter = new Filter();
            //filter.setName(mockData[i]);
            data.add(filter);
        }

        currentItem = data.get(0);
    }

    public FilterAdapter(Context context, List<Filter> filterList, FilterClickListener clickListener) {
        super(context);

        this.data = filterList;
        this.filterClickListener = clickListener;

        currentItem = data.get(0);
    }

    @Override
    public ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new FilterAdapter.ViewHolder(ItemTableTeamFilterBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.init(data.get(position), position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ItemTableTeamFilterBinding binding;

        public ViewHolder(ItemTableTeamFilterBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(Filter filter, int position) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemTableTeamFilterViewModel(getContext(), filter));
            }

            if (filter.equals(currentItem)) {
                binding.getRoot().setBackgroundColor(ContextCompat.getColor(getContext(),
                        R.color.colorPrimary));
            } else {
                binding.getRoot().setBackgroundColor(ContextCompat.getColor(getContext(),
                        R.color.colorGray));
            }


            RxView.clicks(binding.getRoot()).subscribe(aVoid -> {
                Filter prevSelected = currentItem;
                currentItem = data.get(position);
                notifyItemChanged(data.indexOf(prevSelected));
                notifyItemChanged(position);

                filterClickListener.onClick(filter);
            });
            binding.getViewModel().filter.set(filter);
        }
    }
}
