package com.fanzine.coys.adapters.table;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemLeagueBinding;
import com.fanzine.coys.databinding.ItemRegionLiguesBinding;
import com.fanzine.coys.fragments.table.RegionLeagueClickListener;
import com.fanzine.coys.models.League;
import com.fanzine.coys.models.table.RegionLeague;
import com.fanzine.coys.viewmodels.items.ItemLeagueViewModel;
import com.fanzine.coys.viewmodels.items.ItemRegionLeaguesViewModel;
import com.bumptech.glide.Glide;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class RegionLeaguesAdapter extends BaseAdapter<RegionLeaguesAdapter.Holder> {


    private List<RegionLeague> data = new ArrayList<>();

    private RegionLeagueClickListener clickListener;


    public RegionLeaguesAdapter(Context context, RegionLeagueClickListener clickListener) {
        super(context);
        this.data = new ArrayList<>();
        this.clickListener = clickListener;
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemRegionLiguesBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(RegionLeaguesAdapter.Holder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<RegionLeague> data) {
        this.data.clear();
        this.data.addAll(data);

        notifyDataSetChanged();
    }

    class Holder extends RecyclerView.ViewHolder {
        private ItemRegionLiguesBinding binding;

        Holder(ItemRegionLiguesBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(int position) {

            List<League> leagues = getLeagues(position);

            int totalSize = leagues.size();

            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemRegionLeaguesViewModel(getContext(), data.get(position).getName()));
            } else {
                binding.getViewModel().title.set(data.get(position).getName());
            }

            GridLayoutManager manager = new GridLayoutManager(getContext(), 6);
            manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    int span;
                    span = totalSize % 3;
                    if (totalSize < 3) {
                        return 6;
                    } else if (span == 0 || (position <= ((totalSize - 1) - span))) {
                        return 2;
                    } else if (span == 1) {
                        return 6;
                    } else {
                        return 3;
                    }
                }
            });

            binding.rvLeagues.setLayoutManager(manager);
            binding.rvLeagues.setHasFixedSize(true);
            binding.rvLeagues.setAdapter(new LeagueAdapter(getContext(), data.get(position)));
            binding.executePendingBindings();
        }

        private List<League> getLeagues(int position) {
            List<League> leagues = data.get(position).getLeagues();
            Collections.sort(leagues, (l2, l1) -> ((Integer) l2.getWebSort()).compareTo(l1.getWebSort()));

            return leagues;
        }
    }

    private class LeagueAdapter extends BaseAdapter<LeagueAdapter.Holder> {

        private List<League> leagues;
        private RegionLeague regionLeague;

        public LeagueAdapter(Context context, RegionLeague regionLeague) {
            super(context);

            this.regionLeague = regionLeague;
            this.leagues = regionLeague.getLeagues();
        }

        @Override
        public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
            return new Holder(ItemLeagueBinding.inflate(inflater, parent, false));
        }

        @Override
        public void onBindViewHolder(Holder holder, int position) {
            holder.bind(leagues.get(position));
        }

        @Override
        public int getItemCount() {
            return leagues.size();
        }

        class Holder extends RecyclerView.ViewHolder {
            ItemLeagueBinding binding;

            public Holder(ItemLeagueBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }

            public void bind(League league) {
                if (binding.getViewModel() == null) {
                    binding.setViewModel(new ItemLeagueViewModel(getContext(), league));
                }

                if (league.isSvgFormat()) {
//                    Glide.with(getContext())
//                            .using(Glide.buildStreamModelLoader(Uri.class, getContext()), InputStream.class)
//                            .from(Uri.class)
//                            .as(SVG.class)
//                            .transcode(new SvgDrawableTranscoder() {
//                                @Override
//                                public String getId() {
//                                    return RegionLeaguesAdapter.class.getName();
//                                }
//                            }, PictureDrawable.class)
//                            .sourceEncoder(new StreamEncoder())
//                            .override(150, 150)
//                            .cacheDecoder(new FileToStreamDecoder<>(new SvgDecoder()))
//                            .listener(new SvgSoftwareLayerSetter<Uri>())
//                            .decoder(new SvgDecoder()).diskCacheStrategy(DiskCacheStrategy.SOURCE).
//                            load(Uri.parse(league.getIcon()))
//                            .into(binding.ivImage);
                } else {
                    Glide.with(getContext())
                            .load(league.getIcon())
                            .into(binding.ivImage);
                }


                RxView.clicks(binding.ivImage).subscribe(aVoid -> {
                    clickListener.onLeagueClicked(regionLeague, league);
                });
            }
        }
    }
}


