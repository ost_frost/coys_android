package com.fanzine.coys.adapters.venues;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemUpcomingMatchBinding;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.viewmodels.items.ItemMatchViewModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbp on 1/30/18.
 */

public class FixturesAdapter extends BaseAdapter<FixturesAdapter.Holder> {

    public interface OnMatchClickListener {
        void onMatchClicked(Match match);
    }

    private OnMatchClickListener onMatchClickListener;

    private int selectedPosition = -1;

    private List<Match> data;

    public FixturesAdapter(Context context, OnMatchClickListener onMatchClickListener) {
        super(context);

        data = new ArrayList<>();
        this.onMatchClickListener = onMatchClickListener;
    }

    @Override
    public FixturesAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new FixturesAdapter.Holder(ItemUpcomingMatchBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(FixturesAdapter.Holder holder, int position) {
        holder.bind(position);
    }

    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<Match> data) {
        this.data.addAll(data);

        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private ItemUpcomingMatchBinding binding;

        Holder(ItemUpcomingMatchBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(int position) {
            Match match = data.get(position);

            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemMatchViewModel(getContext(), match));
            } else {
                binding.getViewModel().setPosition(selectedPosition, position);
                binding.getViewModel().setMatch(match);
            }

            if (match.isTeamsExist()) {
                Glide.with(getContext()).load(match.getHomeTeam().getIcon()).into(binding.homeTeamIcon);
                Glide.with(getContext()).load(match.getGuestTeam().getIcon()).into(binding.guestTeamIcon);
            }

            if ( position == selectedPosition) {
                binding.main.setBackgroundColor(ContextCompat.getColor(getContext(),
                        R.color.selected_contact_background));
            } else {
                binding.main.setBackgroundColor(ContextCompat.getColor(getContext(),
                        R.color.venue_fixtures_background));
            }

            binding.getRoot().setOnClickListener(view -> {
                onMatchClickListener.onMatchClicked(match);

                selectedPosition = position;
                notifyDataSetChanged();
            });

            binding.executePendingBindings();
        }
    }
}