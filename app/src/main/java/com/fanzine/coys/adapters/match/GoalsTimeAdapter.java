package com.fanzine.coys.adapters.match;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemGoalTimeBinding;


public class GoalsTimeAdapter extends BaseAdapter<GoalsTimeAdapter.Holder> {

    public GoalsTimeAdapter(Context context) {
        super(context);
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemGoalTimeBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(GoalsTimeAdapter.Holder holder, int position) {
        holder.init();

    }

    @Override
    public int getItemCount() {
        return 4;
    }

    class Holder extends RecyclerView.ViewHolder {
        private ItemGoalTimeBinding binding;

        Holder(ItemGoalTimeBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void init() {

        }

    }
}
