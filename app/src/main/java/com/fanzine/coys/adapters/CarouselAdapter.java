package com.fanzine.coys.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.databinding.ItemVenuesCategoryBinding;
import com.fanzine.coys.models.CarouselFilter;

import java.util.List;

import static com.fanzine.coys.App.getContext;

/**
 * Created by mbp on 12/6/17.
 */

public class CarouselAdapter extends RecyclerView.Adapter<CarouselAdapter.Holder> {

    private List<CarouselFilter> categories;

    public CarouselAdapter(List<CarouselFilter> categories) {
        this.categories = categories;
    }

    @Override
    public CarouselAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CarouselAdapter.Holder(ItemVenuesCategoryBinding.inflate(LayoutInflater.from(getContext())));
    }

    @Override
    public void onBindViewHolder(CarouselAdapter.Holder holder, int position) {
        holder.init(categories.get(position));
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public int getItemPosition(String category) {
        for (int pos = 0; pos < categories.size(); pos++) {
            if (categories.get(pos).getTitle().equals(category)) {
                return pos;
            }
        }
        return 0;
    }

    public CarouselFilter getItem(int position) {
        return categories.get(position);
    }

    class Holder extends RecyclerView.ViewHolder {

        private ItemVenuesCategoryBinding binding;

        public Holder(ItemVenuesCategoryBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(CarouselFilter filter) {
            binding.ivImage.setImageResource(filter.getIcon());
        }
    }
}