package com.fanzine.coys.adapters.match;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemMatchBinding;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.viewmodels.items.ItemMatchViewModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;


public class MatchAdapter extends BaseAdapter<MatchAdapter.Holder> {

    private int selectedPosition = -1;

    private List<Match> data;

    public MatchAdapter(Context context, List<Match> data) {
        super(context);

        this.data = data;
    }

    public MatchAdapter(Context context) {
        super(context);

        data = new ArrayList<>();
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemMatchBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(MatchAdapter.Holder holder, int position) {
        holder.bind(position);
    }

    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<Match> data) {
        this.data.addAll(data);

        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private ItemMatchBinding binding;

        Holder(ItemMatchBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(int position) {
            Match match = data.get(position);

            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemMatchViewModel(getContext(), match));
            } else {
                binding.getViewModel().setPosition(selectedPosition, position);
                binding.getViewModel().setMatch(match);
            }

            if (match.isTeamsExist()) {
                Glide.with(getContext()).load(match.getHomeTeam().getIcon()).into(binding.homeTeamIcon);
                Glide.with(getContext()).load(match.getGuestTeam().getIcon()).into(binding.guestTeamIcon);
            }


            binding.matchVenue.setPaintFlags(binding.matchVenue.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

//            if (data.get(position).getState() == Match.LIVE) {
//                binding.setProgress(Integer.parseInt(data.get(position).getCurrentTimeMatch()));
//            }

            binding.executePendingBindings();
        }
    }
}


