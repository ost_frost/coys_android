package com.fanzine.coys.adapters.table;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.ItemTableGroupBinding;
import com.fanzine.coys.databinding.ItemTableGroupHeaderBinding;
import com.fanzine.coys.models.table.Bands;
import com.fanzine.coys.models.table.TeamTable;
import com.fanzine.coys.viewmodels.items.table.ItemTableGroupViewModel;

import java.util.List;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

import static com.fanzine.coys.App.getContext;

/**
 * Created by mbp on 12/12/17.
 */

public class TableSection extends StatelessSection {

    private List<TeamTable> teamTables;
    private String section;
    private Bands bands;

    public TableSection(List<TeamTable> teamTables, String section, Bands bands) {
        super(new SectionParameters.Builder(R.layout.item_table_group)
                .headerResourceId(R.layout.item_table_group_header)
                .build());

        this.teamTables = teamTables;
        this.section = section;
        this.bands = bands;
    }

    @Override
    public int getContentItemsTotal() {
        return teamTables.size();
    }

    @Override
    public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
        return new HeaderHolder(ItemTableGroupHeaderBinding.bind(view));
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        return new Holder(ItemTableGroupBinding.bind(view));
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
        ((HeaderHolder) holder).init(section);
    }

    @Override
    public void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((Holder) holder).init(teamTables.get(position), position);
    }



    class Holder extends RecyclerView.ViewHolder {

        private final ItemTableGroupBinding binding;

        Holder(ItemTableGroupBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(TeamTable teamTable, int position) {
            binding.setViewModel(new ItemTableGroupViewModel(getContext()));
            binding.getViewModel().team.set(teamTable);

            /*if( position >= 0 && position < 4 )  {
                binding.getRoot().setBackgroundColor(Color.parseColor("#868686"));
                changeRowTextColor("#FFFFFF");
            } else if ( position == 4 ) {
                binding.getRoot().setBackgroundColor(Color.parseColor("#D9D9D9"));
                changeRowTextColor("#1D1D1B");
            } else if ( position >= data.size()-3 && position < data.size()) {
                binding.getRoot().setBackgroundColor(Color.parseColor("#868686"));
                changeRowTextColor("#FFFFFF");
            }
            else {
                binding.getRoot().setBackgroundColor(Color.parseColor("#ffffff"));
                changeRowTextColor("#1D1D1B");

            }*/
            Log.i("LogTag", "bands.getPromotionQuantity()0 && positio" + bands.getPromotionQuantity());
            Log.i("LogTag", "bands.getPromotionColor()" + bands.getPromotionColor());

            Log.i("LogTag", "bands.getBelowPromotionQuality() " + bands.getBelowPromotionQuality());
            Log.i("LogTag", "bands.getBelowPromotionColor()" + bands.getBelowPromotionColor());

            Log.i("LogTag", "getItemCount()=" + getContentItemsTotal());


            if (position <= bands.getPromotionQuantity() - 1) {
                binding.getRoot().setBackgroundColor(Color.parseColor(bands.getPromotionColor()));
                changeRowTextColor("#FFFFFF");

                //changeRowTextColor(bands.getPromotionColor());
            } else if (position >= bands.getPromotionQuantity()
                    && position < bands.getPromotionQuantity()
                    + bands.getBelowPromotionQuality()) {

                binding.getRoot().setBackgroundColor(Color.parseColor(bands.getBelowPromotionColor()));
                changeRowTextColor("#1D1D1B");
            } else if (position > getContentItemsTotal() - bands.getRelegationQuantity()) {
                binding.getRoot().setBackgroundColor(Color.parseColor(bands.getRelegationColor()));
                changeRowTextColor("#FFFFFF");
            } else {
                binding.getRoot().setBackgroundColor(Color.parseColor("#ffffff"));
                changeRowTextColor("#1D1D1B");
            }
        }

        private void changeRowTextColor(String hex) {
            binding.tableColumnDraw.setTextColor(Color.parseColor(hex));
            binding.tableColumnGd2.setTextColor(Color.parseColor(hex));
            binding.tableColumnLose.setTextColor(Color.parseColor(hex));
            binding.tableColumnP.setTextColor(Color.parseColor(hex));
            binding.tableColumnPosition.setTextColor(Color.parseColor(hex));
            binding.tableColumnWin.setTextColor(Color.parseColor(hex));
            binding.tableColumnGd.setTextColor(Color.parseColor(hex));
            binding.tableColumnPts.setTextColor(Color.parseColor(hex));
            binding.tableColumnTeamName.setTextColor(Color.parseColor(hex));
        }

    }

    class HeaderHolder extends RecyclerView.ViewHolder {

        private final ItemTableGroupHeaderBinding binding;

        HeaderHolder(ItemTableGroupHeaderBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(String s) {
            if (!TextUtils.isEmpty(s))
                binding.groupName.setText(getContext().getString(R.string.group, s));
        }

    }
}
