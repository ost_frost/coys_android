package com.fanzine.coys.adapters.dialogs;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemTeamNotificationBinding;
import com.fanzine.coys.models.profile.League;
import com.fanzine.coys.models.profile.Team;
import com.fanzine.coys.viewmodels.items.dialog.ItemTeamNotificationViewModel;

import java.util.List;

/**
 * Created by Woland on 21.04.2017.
 */

public class TeamNotificationDialogAdapter extends BaseAdapter<TeamNotificationDialogAdapter.Holder> {

    private List<League> leagues;
    private List<Team> teams;

    public TeamNotificationDialogAdapter(Context context, List<League> leagues) {
        super(context);
        this.leagues = leagues;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
        notifyDataSetChanged();
    }

    @Override
    public TeamNotificationDialogAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemTeamNotificationBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(TeamNotificationDialogAdapter.Holder holder, int position) {
        if (teams != null) {
            holder.init(teams.get(position));
        }
        else
            holder.init(leagues.get(position));
    }

    @Override
    public int getItemCount() {
        return teams == null ? leagues == null ? 0 : leagues.size() : teams.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private final ItemTeamNotificationBinding binding;

        public Holder(ItemTeamNotificationBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(League league) {
            ItemTeamNotificationViewModel viewModel;

            if (binding.getViewModel() != null)
                viewModel = binding.getViewModel();
            else {
                viewModel = new ItemTeamNotificationViewModel(getContext());
                binding.setViewModel(viewModel);
            }

            viewModel.name.set(league.getName());
            viewModel.icon.set(league.getIcon());
        }

        public void init(Team team) {
            ItemTeamNotificationViewModel viewModel;

            if (binding.getViewModel() != null)
                viewModel = binding.getViewModel();
            else {
                viewModel = new ItemTeamNotificationViewModel(getContext());
                binding.setViewModel(viewModel);
            }

            viewModel.name.set(team.getName());
            viewModel.icon.set(team.getTeamIco());
        }

    }
}
