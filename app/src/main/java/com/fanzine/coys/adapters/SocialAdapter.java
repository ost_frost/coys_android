package com.fanzine.coys.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.BR;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemSocialInstagramBinding;
import com.fanzine.coys.databinding.ItemSocialTwitterBinding;
import com.fanzine.coys.models.Social;
import com.fanzine.coys.viewmodels.items.ItemSocialViewModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 07.11.2017.
 */

public class SocialAdapter extends BaseAdapter<RecyclerView.ViewHolder> {

    public interface OnSocialListener {
        void openPost(String url);

        void openImage(String url);
    }

    private List<Social> data = new ArrayList<>();
    private boolean finished = false;

    private OnSocialListener mOnSocialListener;

    public static final int ITEM_TYPE_INSTAGRAN = 0;
    public static final int ITEM_TYPE_TWITTER = 1;


    public SocialAdapter(Context context, List<Social> data, OnSocialListener onSocialListener) {
        super(context);
        addAll(data);

        this.mOnSocialListener = onSocialListener;
    }

    public SocialAdapter(Context context, OnSocialListener mOnSocialListener) {
        super(context);
        this.mOnSocialListener = mOnSocialListener;
    }

    public SocialAdapter(Context context, List<Social> data) {
        super(context);
        this.data = data;
    }

    public SocialAdapter(Context context) {
        super(context);
    }

    public void addAll(List<Social> newItems) {
        data.addAll(newItems);
        notifyDataSetChanged();
    }

    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position).getSocialNetwork().equals("Instagram")) {
            return ITEM_TYPE_INSTAGRAN;
        } else if (data.get(position).getSocialNetwork().equals("Twitter")) {
            return ITEM_TYPE_TWITTER;
        }
        return -1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent,
                                                      int viewType) {
        if (viewType == ITEM_TYPE_INSTAGRAN) {
            ItemSocialInstagramBinding binding = ItemSocialInstagramBinding.inflate(inflater, parent,
                    false);
            return new InstagramHolder(binding);
        }

        if (viewType == ITEM_TYPE_TWITTER) {
            ItemSocialTwitterBinding binding = ItemSocialTwitterBinding.inflate(inflater, parent,
                    false);
            return new TwitterHolder(binding);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final int itemType = getItemViewType(position);
        Social social = getSocial(position);

        if (itemType == ITEM_TYPE_INSTAGRAN) {
            ((InstagramHolder) holder).init(social);
        } else if (itemType == ITEM_TYPE_TWITTER) {
            ((TwitterHolder) holder).init(social);
        }

        holder.itemView.setOnClickListener(view -> {
            if (mOnSocialListener != null)
                mOnSocialListener.openPost(data.get(position).getUrl());
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private Social getSocial(int position) {
        return data.get(position);
    }


    class InstagramHolder extends RecyclerView.ViewHolder {

        private final ItemSocialInstagramBinding binding;

        InstagramHolder(ItemSocialInstagramBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.mainImg.setOnClickListener(v -> {
                if (mOnSocialListener != null) {
                    if (getAdapterPosition() < data.size()) {
                        mOnSocialListener.openImage(data.get(getAdapterPosition()).getImage());
                    }
                }
            });
        }

        public void init(Social social) {
            binding.setVariable(BR.viewModel, new ItemSocialViewModel(getContext(), social));

            Glide.with(getContext()).load(social.getImage()).into(binding.mainImg);
            Glide.with(getContext()).load(social.getAuthor_photo()).into(binding.socialLogo);
            binding.executePendingBindings();
        }
    }

    class TwitterHolder extends RecyclerView.ViewHolder {

        private final ItemSocialTwitterBinding binding;

        TwitterHolder(ItemSocialTwitterBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void init(Social social) {
            binding.setVariable(BR.viewModel, new ItemSocialViewModel(getContext(), social));
            if (TextUtils.isEmpty(social.getImage())) {
                binding.mainImg.setVisibility(View.GONE);
                binding.pb.setVisibility(View.GONE);
            } else {
                binding.mainImg.setVisibility(View.VISIBLE);
                Glide.with(getContext())
                        .load(social.getImage())
                        .into(binding.mainImg);
            }
            Glide.with(getContext()).load(social.getAuthor_photo()).into(binding.socialLogo);
            binding.executePendingBindings();
        }
    }
}
