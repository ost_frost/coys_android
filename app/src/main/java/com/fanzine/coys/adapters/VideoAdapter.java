package com.fanzine.coys.adapters;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemLoadingBinding;
import com.fanzine.coys.databinding.ItemVideoBinding;
import com.fanzine.coys.models.Video;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.items.ItemVideoViewModel;

import java.util.List;


public class VideoAdapter extends BaseAdapter {

    public interface Callback {
        void onLike(Video video);

        void onFbPostShare(Video video);

        void onTwitterShare(Video video);

        void onChatShare(Video video);

        void onMailShare(Video video);

        void onClipboardCopy(Video video);
    }

    protected final List<Video> data;
    private boolean finished = false;
    private Callback callback;

    public void refreshItem(Video video) {
        if (data.contains(video)) {
            final int idx = data.indexOf(video);
            data.remove(idx);
            data.add(idx, video);
            notifyItemChanged(idx);
        }
    }

    private AnimatorSet mSetRightOut;
    private AnimatorSet mSetLeftIn;

    public VideoAdapter(Context context, List<Video> data) {
        super(context);
        this.data = data;
        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.out_animation);
        mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.in_animation);
    }

    public VideoAdapter(Context context, List<Video> data, boolean finished) {
        super(context);
        this.data = data;
        this.finished = finished;
        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.out_animation);
        mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.in_animation);
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void addItems(List<Video> items) {
        final int positionStart = data.size() > 0 ? data.size() - 1 : 0;
        int count = 0;
        if (items.size() > 0) {
            for (int i = 0; i < items.size(); i++) {
                final Video video = items.get(i);
                if (!data.contains(video)) {
                    count++;
                    data.add(video);
                }
            }
            if (count > 0) {
                notifyItemRangeInserted(positionStart, count);
            }
            finished = true;
        } else {
            finished = true;
            notifyDataSetChanged();
        }
    }

    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_LOADING) {
            return new LoadingHolder(ItemLoadingBinding.inflate(inflater, parent, false));
        } else
            return new Holder(ItemVideoBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position < data.size())
            ((Holder) holder).bind(position);
        else {
            if (!NetworkUtils.isNetworkAvailable(getContext()) || finished)
                holder.itemView.setVisibility(View.GONE);
            else
                holder.itemView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position < data.size())
            return super.getItemViewType(position);
        else
            return VIEW_TYPE_LOADING;
    }

    @Override
    public int getItemCount() {
        return data.size() > 0 ? data.size() + 1 : 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        protected ItemVideoBinding binding;

        public Holder(ItemVideoBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.ivLike.setOnClickListener(v -> {
                if (getAdapterPosition() < getItemCount()) {
                    if (callback != null) {
                        callback.onLike(data.get(getAdapterPosition()));
                    }
                }
            });
            binding.ibtnShare.setOnClickListener(v -> {
                if (binding.shareIcons.getVisibility() == View.GONE) {
                    mSetRightOut.setTarget(binding.shareIcons);
                    mSetLeftIn.setTarget(binding.shareIcons);
                    mSetRightOut.start();
                    mSetLeftIn.start();
                    binding.shareIcons.setVisibility(View.VISIBLE);
                } else {
                    binding.shareIcons.setVisibility(View.GONE);
                }
            });

            binding.shareMs.setOnClickListener(v -> {
                binding.shareIcons.setVisibility(View.GONE);
                callback.onMailShare(getVideo());
            });
            binding.shareFb.setOnClickListener(view -> {
                binding.shareIcons.setVisibility(View.GONE);
                callback.onFbPostShare(getVideo());
            });
            binding.shareChat.setOnClickListener(v -> {
                binding.shareIcons.setVisibility(View.GONE);
                callback.onChatShare(getVideo());
            });
            binding.shareDoc.setOnClickListener(v -> {
                binding.shareIcons.setVisibility(View.GONE);
                callback.onClipboardCopy(getVideo());
            });
            binding.shareTw.setOnClickListener(view -> {
                binding.shareIcons.setVisibility(View.GONE);
                callback.onTwitterShare(getVideo());
            });
        }

        public void bind(int position) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemVideoViewModel(getContext()));
            }

            binding.getViewModel().headerTitle.set(binding.getViewModel().getPublishedAgo(data.get(position)));

            binding.getViewModel().setHeaderTitle(data.get(position).getPublishedAgo());
            binding.getViewModel().setVideoItem(data.get(position));
            binding.executePendingBindings();
        }

        @Nullable
        private Video getVideo(){
            if (getAdapterPosition() < data.size()){
                return data.get(getAdapterPosition());
            }
            return null;
        }

    }

    class LoadingHolder extends RecyclerView.ViewHolder {

        LoadingHolder(ItemLoadingBinding binding) {
            super(binding.getRoot());
        }

    }
}
