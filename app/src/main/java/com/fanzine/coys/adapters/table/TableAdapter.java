package com.fanzine.coys.adapters.table;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemTableGroupBinding;
import com.fanzine.coys.models.table.Bands;
import com.fanzine.coys.models.table.TeamTable;
import com.fanzine.coys.viewmodels.items.table.ItemTableGroupViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Woland on 16.01.2017.
 */

public class TableAdapter extends BaseAdapter<TableAdapter.Holder> {

    private final List<TeamTable> data;
    private Bands bands;

    public TableAdapter(Context context, List<TeamTable> data, Bands bands) {
        super(context);
        this.bands = bands;

        this.data = new ArrayList<>();
        this.data.addAll(data);
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemTableGroupBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.init(data.get(position), position);

        Glide.with(getContext()).load(data.get(position).getTeam().getIcon()).into(holder.binding.ivTeamIcon);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class Holder extends RecyclerView.ViewHolder {

        private final ItemTableGroupBinding binding;

        Holder(ItemTableGroupBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(TeamTable teamTable, int position) {
            binding.setViewModel(new ItemTableGroupViewModel(getContext()));
            binding.getViewModel().team.set(teamTable);

            /*if( position >= 0 && position < 4 )  {
                binding.getRoot().setBackgroundColor(Color.parseColor("#868686"));
                changeRowTextColor("#FFFFFF");
            } else if ( position == 4 ) {
                binding.getRoot().setBackgroundColor(Color.parseColor("#D9D9D9"));
                changeRowTextColor("#1D1D1B");
            } else if ( position >= data.size()-3 && position < data.size()) {
                binding.getRoot().setBackgroundColor(Color.parseColor("#868686"));
                changeRowTextColor("#FFFFFF");
            }
            else {
                binding.getRoot().setBackgroundColor(Color.parseColor("#ffffff"));
                changeRowTextColor("#1D1D1B");

            }*/
            Log.i("LogTag", "bands.getPromotionQuantity()0 && positio" + bands.getPromotionQuantity());
            Log.i("LogTag", "bands.getPromotionColor()" + bands.getPromotionColor());

            Log.i("LogTag", "bands.getBelowPromotionQuality() " + bands.getBelowPromotionQuality());
            Log.i("LogTag", "bands.getBelowPromotionColor()" + bands.getBelowPromotionColor());

            Log.i("LogTag", "getItemCount()=" + getItemCount());


            if (position <= bands.getPromotionQuantity() - 1) {

                binding.getRoot().setBackgroundColor(Color.parseColor(bands.getPromotionColor()));
                changeRowTextColor("#FFFFFF");

                //changeRowTextColor(bands.getPromotionColor());
            } else if (position >= bands.getPromotionQuantity()
                    && position < bands.getPromotionQuantity()
                    + bands.getBelowPromotionQuality()) {
                binding.getRoot().setBackgroundColor(Color.parseColor(bands.getBelowPromotionColor()));
                changeRowTextColor("#1D1D1B");
            } else if (position < getItemCount() - bands.getRelegationQuantity() &&
                    position >= getItemCount() - bands.getRelegationQuantity() - bands.getAboveRelegationQuantity()) {
                binding.getRoot().setBackgroundColor(Color.parseColor(bands.getAboveRelegationColor()));
                changeRowTextColor("#1D1D1B");
            } else if (position >= getItemCount() - bands.getRelegationQuantity()) {
                binding.getRoot().setBackgroundColor(Color.parseColor(bands.getRelegationColor()));
                changeRowTextColor("#FFFFFF");
            } else {
                binding.getRoot().setBackgroundColor(Color.parseColor("#ffffff"));
                changeRowTextColor("#1D1D1B");
            }
        }

        private void changeRowTextColor(String hex) {
            binding.tableColumnDraw.setTextColor(Color.parseColor(hex));
            binding.tableColumnGd2.setTextColor(Color.parseColor(hex));
            binding.tableColumnLose.setTextColor(Color.parseColor(hex));
            binding.tableColumnP.setTextColor(Color.parseColor(hex));
            binding.tableColumnPosition.setTextColor(Color.parseColor(hex));
            binding.tableColumnWin.setTextColor(Color.parseColor(hex));
            binding.tableColumnGd.setTextColor(Color.parseColor(hex));
            binding.tableColumnPts.setTextColor(Color.parseColor(hex));
            binding.tableColumnTeamName.setTextColor(Color.parseColor(hex));
        }
    }
}
