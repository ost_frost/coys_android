package com.fanzine.coys.adapters.venues;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemSearchCategoryBinding;
import com.fanzine.coys.models.venues.VenueSearchItem;
import com.fanzine.coys.viewmodels.items.venues.ItemSearchCategoryViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbp on 4/11/18.
 */

public class SearchItemsAdapter extends BaseAdapter<SearchItemsAdapter.Holder> {

    public interface OnClickListener {
        void findBusiness(String category);
    }

    private List<VenueSearchItem> data;

    private OnClickListener mOnClickListner;

    public SearchItemsAdapter(Context context, OnClickListener onClickListner) {
        super(context);
        this.data = new ArrayList<>();
        mOnClickListner = onClickListner;
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new SearchItemsAdapter.Holder(ItemSearchCategoryBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(SearchItemsAdapter.Holder holder, int position) {
        holder.init(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void addAll(List<VenueSearchItem> items) {
        data.clear();
        data.addAll(items);

        notifyItemRangeChanged(0, items.size());
        notifyDataSetChanged();
    }

    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }

    class Holder extends RecyclerView.ViewHolder {

        private ItemSearchCategoryBinding binding;

        public Holder(ItemSearchCategoryBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(VenueSearchItem item) {
            binding.setViewModel(new ItemSearchCategoryViewModel(getContext(), item));

            binding.getRoot().setOnClickListener(view -> {
                mOnClickListner.findBusiness(item.getAlias());
            });
        }
    }
}