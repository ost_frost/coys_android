package com.fanzine.coys.adapters.team;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemPlayerVerticalBinding;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.team.Player;
import com.fanzine.coys.viewmodels.items.team.ItemPlayerHorizontalViewModel;
import com.jakewharton.rxbinding.view.RxView;

import java.lang.ref.WeakReference;
import java.util.List;


public class TeamAdapter extends BaseAdapter<TeamAdapter.Holder> {

    private final List<Player> data;
    private final WeakReference<DataListLoadingListener<Player>> mListener;

    public TeamAdapter(Context context, List<Player> data, DataListLoadingListener<Player> listener) {
        super(context);

        this.data = data;

        mListener = new WeakReference<>(listener);
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemPlayerVerticalBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(TeamAdapter.Holder holder, int position) {
        holder.init(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private ItemPlayerVerticalBinding binding;

        Holder(ItemPlayerVerticalBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void init(Player player) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemPlayerHorizontalViewModel(getContext()));
            }

            binding.getViewModel().player.set(player);

            RxView.clicks(binding.getRoot()).subscribe(aVoid -> {
                if (mListener != null && mListener.get() != null)
                    mListener.get().onLoaded(player);
            });

            binding.executePendingBindings();
        }
    }
}
