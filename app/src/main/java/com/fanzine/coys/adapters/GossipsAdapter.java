package com.fanzine.coys.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemGossipBinding;
import com.fanzine.coys.models.Gossip;
import com.fanzine.coys.viewmodels.items.ItemGossipViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbp on 3/6/18.
 */

public class GossipsAdapter extends BaseAdapter<GossipsAdapter.Holder> {

    private List<Gossip> data = new ArrayList<>();

    public GossipsAdapter(Context context, List<Gossip> gossips) {
        super(context);

        if (gossips != null) data.addAll(gossips);
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemGossipBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        ItemGossipBinding binding;

        public Holder(ItemGossipBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Gossip gossip) {
            binding.setViewModel(new ItemGossipViewModel(getContext(), gossip));
            binding.executePendingBindings();

            binding.getRoot().setOnClickListener(view -> {
                binding.getViewModel().openUrl(gossip);
            });
        }
    }
}
