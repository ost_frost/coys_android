package com.fanzine.coys.adapters.match;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemSidelinedBinding;
import com.fanzine.coys.models.lineups.SidelinedPlayer;
import com.fanzine.coys.viewmodels.items.match.ItemSidelinedPlayer;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by mbp on 10/20/17.
 */

public class SidelinedAdapter extends BaseAdapter<SidelinedAdapter.Holder> {

    private List<SidelinedPlayer> players;
    private boolean isLocal;

    public SidelinedAdapter(Context context, List<SidelinedPlayer> players, boolean isLocal) {
        super(context);
        this.players = players;
        this.isLocal = isLocal;
    }

    @Override
    public SidelinedAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemSidelinedBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(SidelinedAdapter.Holder holder, int position) {
        holder.bind(players.get(position));
    }

    @Override
    public int getItemCount() {
        return players.size();
    }

    class Holder extends RecyclerView.ViewHolder {

        private ItemSidelinedBinding binding;

        public Holder(ItemSidelinedBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(SidelinedPlayer player) {

            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemSidelinedPlayer(getContext()));
            }
            binding.getViewModel().player.set(player);

            Picasso.with(getContext()).load(player.getImage())
                    .into(binding.playerImage);

            if (isLocal) {
                binding.playerName.setTextColor(ContextCompat.getColor(getContext(),
                        R.color.sidelined_red));
                binding.playerImage.setBorderWidth(6);
                binding.playerImage.setBorderColor(ContextCompat.getColor(getContext(), R.color.sidelined_red));
            } else {
                binding.playerName.setTextColor(ContextCompat.getColor(getContext(),
                        R.color.sidelined_blue));
                binding.playerImage.setBorderWidth(6);
                binding.playerImage.setBorderColor(ContextCompat.getColor(getContext(), R.color.sidelined_blue));
            }

            Glide.with(getContext()).load(player.getImage()).into(binding.playerImage);
        }
    }

}
