package com.fanzine.coys.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemTopNavigationBinding;
import com.fanzine.coys.models.League;
import com.fanzine.coys.viewmodels.items.ItemTopPanelViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Woland on 11.01.2017.
 */

public class TopPanelAdapter extends BaseAdapter<TopPanelAdapter.Holder> {

    private final List<League> leagues;

    private int selected = 0;

    public TopPanelAdapter(Context context, List<League> leagues, int selected) {
        super(context);

        if (leagues == null) {
            leagues = new ArrayList<>();
        }
        this.leagues = leagues;
        this.selected = selected;
    }

    @Override
    public TopPanelAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemTopNavigationBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(TopPanelAdapter.Holder holder, int position) {
        holder.init(position);
    }

    @Override
    public int getItemCount() {
        return leagues.size();
    }

    public void setSelected(int selected) {
        this.selected = selected;
        notifyDataSetChanged();
    }

    class Holder extends RecyclerView.ViewHolder {

        private final ItemTopNavigationBinding binding;

        Holder(ItemTopNavigationBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        void init(int position) {
            if (binding.getViewModel() == null)
                binding.setViewModel(new ItemTopPanelViewModel(getContext()));

            League league = leagues.get(position);

            binding.getViewModel().icon.set(league.getIcon());
            binding.executePendingBindings();
        }

    }
}
