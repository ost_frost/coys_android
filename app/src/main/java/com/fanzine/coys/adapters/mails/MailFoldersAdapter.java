package com.fanzine.coys.adapters.mails;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemMailFolderBinding;
import com.fanzine.coys.models.mails.Folder;

import java.util.List;

/**
 * Created by Evgenij Krasilnikov on 09-Feb-18.
 */

public class MailFoldersAdapter extends BaseAdapter<MailFoldersAdapter.ViewHolder> {

    public interface OnFolderClickListener {
        void showMailsList(Folder folder);
    }

    private final List<Folder> folderList;

    private OnFolderClickListener mOnFolderClickListener;

    public MailFoldersAdapter(Context context, List<Folder> folderList, OnFolderClickListener onFolderClickListener) {
        super(context);

        this.folderList = folderList;
        this.mOnFolderClickListener = onFolderClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new ViewHolder(ItemMailFolderBinding.inflate(inflater, parent, false).getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.binding.setFolder(folderList.get(position));
        holder.binding.executePendingBindings();

        holder.bind(folderList.get(position));
    }

    @Override
    public int getItemCount() {
        return folderList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final ItemMailFolderBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        public void bind(Folder folder) {
            binding.getRoot().setOnClickListener(view -> {
                mOnFolderClickListener.showMailsList(folder);
            });
            binding.itMafoIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), folder.getDrawableIconId()));
        }
    }
}
