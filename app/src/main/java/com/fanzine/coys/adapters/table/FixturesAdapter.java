package com.fanzine.coys.adapters.table;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemTableFixtures2Binding;
import com.fanzine.coys.fragments.match.MatchFragment;
import com.fanzine.coys.fragments.table.BaseTableFragment;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.viewmodels.items.ItemMatchViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 09.11.2017.
 */

public class FixturesAdapter extends BaseAdapter<FixturesAdapter.Holder> {

    private int selectedPosition = -1;
    private List<Match> data;

    public FixturesAdapter(Context context) {
        super(context);

        data = new ArrayList<>();
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemTableFixtures2Binding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(FixturesAdapter.Holder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<Match> data) {
        int start = this.data.size()-1;
        this.data.addAll(data);

        notifyItemRangeInserted(start, data.size());
    }


    class Holder extends RecyclerView.ViewHolder {
        private ItemTableFixtures2Binding binding;

        Holder(ItemTableFixtures2Binding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(int position) {
            ItemMatchViewModel viewModel = new ItemMatchViewModel(getContext(), data.get(position));
            viewModel.setPosition(selectedPosition, position);
            viewModel.setMatch(data.get(position));

            binding.setViewModel(viewModel);
            binding.tvDate.setPaintFlags(binding.tvDate.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);

            if ( data.get(position).getState() == Match.LAST) {
                binding.llBell.setVisibility(View.INVISIBLE);
            } else {
                binding.llBell.setVisibility(View.VISIBLE);
            }

            if (data.get(position).isTeamsExist()) {
                Glide.with(getContext()).load(data.get(position).getHomeTeam().getIcon())
                        .into(binding.homeTeamIcon);

                Glide.with(getContext()).load(data.get(position).getGuestTeam().getIcon())
                        .into(binding.guestTeamIcon);
            }

            binding.getRoot().setOnClickListener(view -> {
                Intent openMatch = new Intent(getContext(), MatchFragment.class);
                openMatch.putExtra(MatchFragment.LEAGUE_TITLE,BaseTableFragment.getSelectedLeagueName());
                openMatch.putExtra(Match.MATCH, data.get(position));

                getContext().startActivity(openMatch);
            });

            binding.executePendingBindings();
        }
    }

}
