package com.fanzine.coys.adapters.login;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemTextviewBinding;
import com.fanzine.coys.models.login.Teams;
import com.fanzine.coys.viewmodels.items.login.ItemTextViewViewModel;

import java.util.List;

/**
 * Created by Woland on 23.03.2017.
 */

public class TextAdapter extends BaseAdapter<TextAdapter.Holder> {

    private final List<Teams.Team> data;

    public TextAdapter(Context context, List<Teams.Team> teams) {
        super(context);

        data = teams;
    }

    @Override
    public TextAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemTextviewBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(TextAdapter.Holder holder, int position) {
        holder.init(data.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private final ItemTextviewBinding binding;

        public Holder(ItemTextviewBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(String s) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemTextViewViewModel(getContext(), s));
            }
            else
                binding.getViewModel().text.set(s);
        }

    }
}
