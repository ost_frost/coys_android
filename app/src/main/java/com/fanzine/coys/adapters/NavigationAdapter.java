package com.fanzine.coys.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemNavigationBinding;
import com.fanzine.coys.viewmodels.items.NavigationItemViewModel;

/**
 * Created by Woland on 10.01.2017.
 */

public class NavigationAdapter extends BaseAdapter<NavigationAdapter.Holder> {

    public final static int NEWS = 0;
    public final static int GOSSIP = 1;
    public final static int MATCH = 2;
    public final static int TABLE = 3;
    public final static int VIDEO = 4;
    public final static int TEAM = 5;

    private int selected = 0;

    public NavigationAdapter(Context context) {
        super(context);
    }

    @Override
    public NavigationAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemNavigationBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(NavigationAdapter.Holder holder, int position) {
        holder.init(position);
    }

    public void setSelected(int selected) {
        this.selected = selected;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return 6;
    }

    class Holder extends RecyclerView.ViewHolder {

        private final ItemNavigationBinding binding;

        Holder(ItemNavigationBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        void init(int position) {
            if (binding.getViewModel() == null)
                binding.setViewModel(new NavigationItemViewModel(getContext()));

            binding.getViewModel().isSelected.set(position == selected);

            switch (position) {
                case NEWS:
                    binding.getViewModel().icon.set(R.drawable.menu_news);
                    binding.getViewModel().name.set(R.string.news);
                    break;

                case GOSSIP:
                    binding.getViewModel().icon.set(R.drawable.menu_gossip);
                    binding.getViewModel().name.set(R.string.gossip);
                    break;

                case MATCH:
                    binding.getViewModel().icon.set(R.drawable.menu_match);
                    binding.getViewModel().name.set(R.string.match);
                    break;

                case TEAM:
                    binding.getViewModel().icon.set(R.drawable.menu_team);
                    binding.getViewModel().name.set(R.string.team);
                    break;

                case VIDEO:
                    binding.getViewModel().icon.set(R.drawable.menu_video);
                    binding.getViewModel().name.set(R.string.video);
                    break;

                case TABLE:
                    binding.getViewModel().icon.set(R.drawable.menu_table);
                    binding.getViewModel().name.set(R.string.table);
                    break;

//                case STATS:
//                    binding.getViewModel().icon.set(R.drawable.menu_feedback);
//                    binding.getViewModel().name.set(R.string.feedback);
//                    break;
            }
        }
    }
}
