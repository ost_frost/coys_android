package com.fanzine.coys.adapters.team;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemLastTweetBinding;


public class LastTweetsAdapter extends BaseAdapter<LastTweetsAdapter.Holder> {

    public LastTweetsAdapter(Context context) {
        super(context);
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemLastTweetBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(LastTweetsAdapter.Holder holder, int position) {
        holder.init(position);

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class Holder extends RecyclerView.ViewHolder {
        private ItemLastTweetBinding binding;

        Holder(ItemLastTweetBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void init(int position) {
//            if (binding.getViewModel() == null) {
//                binding.setViewModel(new ItemCommentaryViewModel(getContext(), position == getItemCount() - 1));
//            } else {
//                binding.getViewModel().setLast(position == getItemCount() - 1);
//            }
//
//            binding.executePendingBindings();
        }

    }
}
