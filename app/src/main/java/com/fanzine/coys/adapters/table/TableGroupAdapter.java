package com.fanzine.coys.adapters.table;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.base.BaseHeaderAdapter;
import com.fanzine.coys.databinding.ItemTableGroupBinding;
import com.fanzine.coys.databinding.ItemTableGroupHeaderBinding;
import com.fanzine.coys.models.table.Bands;
import com.fanzine.coys.models.table.TeamTable;
import com.fanzine.coys.viewmodels.items.table.ItemTableGroupViewModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Woland on 16.01.2017.
 */

public class TableGroupAdapter extends BaseHeaderAdapter<TableGroupAdapter.Holder, TableGroupAdapter.HeaderHolder> {

    private final Map<String, List<TeamTable>> data;

    private ArrayList<TeamTable> values = new ArrayList<>();

    private Bands bands;

    public TableGroupAdapter(Context context, Map<String, List<TeamTable>> data, Bands bands) {
        super(context);
        this.bands = bands;

        this.data = data;

        values = (ArrayList)data.values();
    }

    @Override
    public HeaderHolder onCreateHeaderViewHolder(LayoutInflater inflater, ViewGroup parent) {
        return new HeaderHolder(ItemTableGroupHeaderBinding.inflate(inflater, parent, false));
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemTableGroupBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.init(values.get(position), position);

        Glide.with(getContext()).load(values.get(position).getTeam().getIcon()).into(holder.binding.ivTeamIcon);
    }

    @Override
    public void onBindHeaderViewHolder(HeaderHolder holder, int position) {
        holder.init(values.get(position).getGroup());
    }

    @Override
    public long getHeaderId(int position) {
        if (!TextUtils.isEmpty(values.get(position).getGroup()))
            return values.get(position).getGroup().charAt(0);
        else
            return 0;
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    private TeamTable getTeamTable(String group, int position) {
        return data.get(group).get(position);
    }

    class Holder extends RecyclerView.ViewHolder {

        private final ItemTableGroupBinding binding;

        Holder(ItemTableGroupBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(TeamTable teamTable, int position) {
            binding.setViewModel(new ItemTableGroupViewModel(getContext()));
            binding.getViewModel().team.set(teamTable);

            /*if( position >= 0 && position < 4 )  {
                binding.getRoot().setBackgroundColor(Color.parseColor("#868686"));
                changeRowTextColor("#FFFFFF");
            } else if ( position == 4 ) {
                binding.getRoot().setBackgroundColor(Color.parseColor("#D9D9D9"));
                changeRowTextColor("#1D1D1B");
            } else if ( position >= data.size()-3 && position < data.size()) {
                binding.getRoot().setBackgroundColor(Color.parseColor("#868686"));
                changeRowTextColor("#FFFFFF");
            }
            else {
                binding.getRoot().setBackgroundColor(Color.parseColor("#ffffff"));
                changeRowTextColor("#1D1D1B");

            }*/
            Log.i("LogTag", "bands.getPromotionQuantity()0 && positio" + bands.getPromotionQuantity());
            Log.i("LogTag", "bands.getPromotionColor()" + bands.getPromotionColor());

            Log.i("LogTag", "bands.getBelowPromotionQuality() " + bands.getBelowPromotionQuality());
            Log.i("LogTag", "bands.getBelowPromotionColor()" + bands.getBelowPromotionColor());

            Log.i("LogTag", "getItemCount()=" + getItemCount());



            if (position <= bands.getPromotionQuantity() - 1) {
                binding.getRoot().setBackgroundColor(Color.parseColor(bands.getPromotionColor()));
                changeRowTextColor("#FFFFFF");

                //changeRowTextColor(bands.getPromotionColor());
            } else if (position >= bands.getPromotionQuantity()
                    && position < bands.getPromotionQuantity()
                    + bands.getBelowPromotionQuality()) {

                binding.getRoot().setBackgroundColor(Color.parseColor(bands.getBelowPromotionColor()));
                changeRowTextColor("#1D1D1B");
            } else if (position > getItemCount() - bands.getRelegationQuantity()) {
                binding.getRoot().setBackgroundColor(Color.parseColor(bands.getRelegationColor()));
                changeRowTextColor("#FFFFFF");
            } else {
                binding.getRoot().setBackgroundColor(Color.parseColor("#ffffff"));
                changeRowTextColor("#1D1D1B");
            }
        }

        private void changeRowTextColor(String hex) {
            binding.tableColumnDraw.setTextColor(Color.parseColor(hex));
            binding.tableColumnGd2.setTextColor(Color.parseColor(hex));
            binding.tableColumnLose.setTextColor(Color.parseColor(hex));
            binding.tableColumnP.setTextColor(Color.parseColor(hex));
            binding.tableColumnPosition.setTextColor(Color.parseColor(hex));
            binding.tableColumnWin.setTextColor(Color.parseColor(hex));
            binding.tableColumnGd.setTextColor(Color.parseColor(hex));
            binding.tableColumnPts.setTextColor(Color.parseColor(hex));
            binding.tableColumnTeamName.setTextColor(Color.parseColor(hex));
        }

    }

    class HeaderHolder extends RecyclerView.ViewHolder {

        private final ItemTableGroupHeaderBinding binding;

        HeaderHolder(ItemTableGroupHeaderBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(String s) {
            if (!TextUtils.isEmpty(s))
                binding.groupName.setText(getContext().getString(R.string.group, s));
        }

    }
}
