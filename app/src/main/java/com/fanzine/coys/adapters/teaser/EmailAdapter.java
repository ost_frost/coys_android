package com.fanzine.coys.adapters.teaser;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemMailPopupBinding;
import com.fanzine.coys.models.ProEmail;
import com.fanzine.coys.viewmodels.items.teaser.ItemEmailViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbp on 4/5/18.
 */

public class EmailAdapter extends BaseAdapter<EmailAdapter.ViewHolder> {

    private List<ProEmail> mProEmails;

    public EmailAdapter(Context context) {
        super(context);
        mProEmails = new ArrayList<>();
    }

    public void replace(List<ProEmail> emails) {
        mProEmails.clear();
        mProEmails.addAll(emails);

        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new ViewHolder(ItemMailPopupBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.init(mProEmails.get(position));

        int lastItemPosition = getItemCount()-1;
        holder.bottomLineVisibility(position != lastItemPosition);
    }

    @Override
    public int getItemCount() {
        return mProEmails.size();
    }

    public ProEmail getChecked() {
        for (ProEmail email : mProEmails) {
            if (email.isSelected()) return email;
        }
        return null;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ItemMailPopupBinding binding;

        public ViewHolder(ItemMailPopupBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bottomLineVisibility(boolean visible) {
            binding.bottomLine.setVisibility( visible ? View.VISIBLE : View.GONE);
        }

        void init(ProEmail proEmail) {
            binding.setViewModel(new ItemEmailViewModel(getContext(), proEmail));

            Typeface font = Typeface.createFromAsset(getContext().getAssets(),
                    "fonts/SanFranciscoText-Semibold.otf");
            binding.mailText.setTypeface(font);

            binding.chkBox.setOnClickListener(view -> {
                if (binding.chkBox.isChecked()) {
                    proEmail.setSelected(true);

                    for (ProEmail email : mProEmails)
                        if (!email.getEmail().equals(proEmail.getEmail())) email.setSelected(false);

                    notifyDataSetChanged();
                }
            });

            binding.executePendingBindings();
        }
    }
}
