package com.fanzine.coys.adapters.mails;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemLabelBinding;
import com.fanzine.coys.fragments.mails.OnActionLabelLinster;
import com.fanzine.coys.models.mails.Label;
import com.fanzine.coys.viewmodels.items.ItemLabelViewModel;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by maximdrobonoh on 14.09.17.
 */

public class LabelsAdapter extends BaseAdapter<LabelsAdapter.Holder> {

    private List<Label> lables;

    private OnActionLabelLinster labelActionLister;

    public LabelsAdapter(Context context) {
        super(context);

        this.lables = new ArrayList<>();
    }

    public LabelsAdapter(Context context, List<Label> labels, OnActionLabelLinster onLabelDeleteListener) {
        super(context);

        this.lables = labels;
        this.labelActionLister = onLabelDeleteListener;
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new LabelsAdapter.Holder(ItemLabelBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return lables.size();
    }

    public void add(ArrayList<Label> list) {
        if (lables != null) {
            lables.addAll(list);
        } else {
            lables = list;
        }
        notifyDataSetChanged();
    }

    class Holder extends RecyclerView.ViewHolder {

        private ItemLabelBinding binding;

        public Holder(ItemLabelBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void bind(int position) {
            Label label = lables.get(position);

            if (binding.getViewModel() == null)
                binding.setViewModel(new ItemLabelViewModel(getContext(), label));
            else
                binding.getViewModel().setLabel(label);

            binding.labelColor.setBackgroundColor(Color.parseColor(label.getColor().getHex()));

            RxView.clicks(binding.getRoot()).subscribe(aVoid -> {
                labelActionLister.onLabelClick(label);
            });

            RxView.clicks(binding.labelDelete).subscribe(aVoid -> {
                if (lables.size() > 0) {
                    labelActionLister.onLabelDelete(label);

                    lables.remove(position);
                    notifyDataSetChanged();
                }
            });
        }
    }
}
