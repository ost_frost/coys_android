package com.fanzine.coys.adapters.team;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemTeamPlayerBinding;
import com.fanzine.coys.models.team.Player;
import com.fanzine.coys.models.team.PlayerStat;
import com.fanzine.coys.models.team.TeamSquad;
import com.fanzine.coys.viewmodels.items.team.ItemPlayerViewModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbp on 1/9/18.
 */

public class PlayersAdapter extends BaseAdapter<PlayersAdapter.Holder> implements Filterable {

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                filteredPlayerList.clear();
                for (Player player : allPlayerList) {
                    if (player.getPosition().equals(constraint.toString())) {
                        filteredPlayerList.add(player);
                    }
                }
                FilterResults results = new FilterResults();
                results.count = filteredPlayerList.size();
                results.values = filteredPlayerList;
                return null;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                displayList.clear();
                displayList.addAll(filteredPlayerList);
                notifyDataSetChanged();
            }
        };
    }

    public interface PlayerClickListener {
        void showPlayer(Player player);
    }

    private List<Player> allPlayerList = new ArrayList<>();

    private List<Player> displayList = new ArrayList<>();

    private List<Player> filteredPlayerList = new ArrayList<>();

    private PlayerClickListener mPlayerClickListener;

    private TeamSquad squad;

    public PlayersAdapter(Context context, TeamSquad squad, PlayerClickListener playerClickListener) {
        super(context);
        this.squad = squad;
        mPlayerClickListener = playerClickListener;
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemTeamPlayerBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bind(displayList.get(position));
    }

    @Override
    public int getItemCount() {
        return displayList.size();
    }

    public void replace(List<Player> players) {
        displayList.clear();
        displayList.addAll(players);

        allPlayerList.clear();
        allPlayerList.addAll(players);

        notifyDataSetChanged();
    }

    public void clear() {
        this.allPlayerList.clear();
        this.displayList.clear();

        notifyDataSetChanged();
    }

    class Holder extends RecyclerView.ViewHolder {

        private ItemTeamPlayerBinding binding;

        private StatsAdapter statsAdapter;

        public Holder(ItemTeamPlayerBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.recViewStats.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
            statsAdapter = new StatsAdapter(squad);
            binding.recViewStats.setAdapter(statsAdapter);
        }

        public void bind(Player player) {
            binding.setViewModel(new ItemPlayerViewModel(getContext(), player, squad));
            statsAdapter.refresh(player.statisticList);
            loadAva(player.getIcon(), binding.ivAvatar);

            this.binding.getRoot().setOnClickListener(view -> mPlayerClickListener.showPlayer(player));
        }

        private void loadAva(String url, @NonNull ImageView ivLogo) {
            Glide.with(itemView.getContext()).load(url).asBitmap().centerCrop().into(new BitmapImageViewTarget(ivLogo) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(itemView.getContext().getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    ivLogo.setImageDrawable(circularBitmapDrawable);
                }
            });
        }
    }

    private class StatsAdapter extends RecyclerView.Adapter<StatsAdapter.StatHolder> {

        @NonNull
        private List<PlayerStat> stats = new ArrayList<>();

        private TeamSquad squad;

        StatsAdapter(TeamSquad squad) {
            this.squad = squad;
        }

        void refresh(@NonNull List<PlayerStat> stats) {
            this.stats.clear();
            this.stats.addAll(stats);
            this.notifyDataSetChanged();
        }

        @Override
        public StatHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_stat, parent, false);
            return new StatHolder(view);
        }

        @Override
        public void onBindViewHolder(StatHolder holder, int position) {
            if (position < stats.size()) {
                holder.setBottomDividerVisible(position < stats.size() - 1);
                holder.bind(stats.get(position));
            }
        }

        @Override
        public int getItemCount() {
            return stats.size();
        }

        class StatHolder extends RecyclerView.ViewHolder {

            TextView tvStatName;
            TextView tvStatValue;
            View bottomDivider;

            StatHolder(View itemView) {
                super(itemView);
                tvStatName = (TextView) itemView.findViewById(R.id.tv_stat_name);
                tvStatValue = (TextView) itemView.findViewById(R.id.tv_stat_value);
                bottomDivider = itemView.findViewById(R.id.bottomDivider);
            }

            public void bind(@NonNull PlayerStat stat) {
                tvStatName.setText(stat.getName());
                if (squad == TeamSquad.LADIES || squad == TeamSquad.ACADEMY){
                    tvStatValue.setText("n/a");
                }else {
                    tvStatValue.setText(stat.getValue());
                }
            }

            void setBottomDividerVisible(boolean v) {
                bottomDivider.setVisibility(v ? View.VISIBLE : View.GONE);
            }
        }
    }
}
