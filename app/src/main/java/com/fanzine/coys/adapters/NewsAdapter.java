package com.fanzine.coys.adapters;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.fanzine.coys.BR;
import com.fanzine.coys.R;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemNews1Binding;
import com.fanzine.coys.models.News;
import com.fanzine.coys.viewmodels.items.ItemNewsViewModel;

import java.util.List;

/**
 * Created by Woland on 12.01.2017.
 */

public class NewsAdapter extends BaseAdapter<NewsAdapter.Holder> {

    public interface NewsCallback {
        void onFbPostShare(News news);

        void onTwitterShare(News news);

        void onChatShare(News news);

        void onMailShare(News news);

        void onLikePost(News news);

        void onClipboardCopy(News news);
    }

    private final List<News> data;
    private boolean finished = false;

    private AnimatorSet mSetRightOut;
    private AnimatorSet mSetLeftIn;

    private NewsCallback newsCallback;


    public NewsAdapter(Context context, List<News> data, NewsCallback newsCallback) {
        super(context);
        this.data = data;

        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.out_animation);
        mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.in_animation);
        this.newsCallback = newsCallback;
    }

    public void refresh(List<News> data) {
        this.data.clear();
        this.data.addAll(data);

        notifyItemRangeChanged(0, data.size());
    }

    public News getNews(int position) {
        return data.get(position);
    }

    @Override
    public NewsAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int position) {

        int height = parent.getMeasuredHeight() / 3;

        ItemNews1Binding binding = ItemNews1Binding.inflate(inflater, parent, false);
        binding.main.setMinimumHeight(height);

        binding.newsShare.setOnClickListener(view -> {
            if (binding.shareIcons.getVisibility() == View.GONE) {
                mSetRightOut.setTarget(binding.shareIcons);
                mSetLeftIn.setTarget(binding.shareIcons);
                mSetRightOut.start();
                mSetLeftIn.start();
                binding.shareIcons.setVisibility(View.VISIBLE);
            } else {
                binding.shareIcons.setVisibility(View.GONE);
            }
        });

        binding.main.setOnClickListener(view -> {
            if (binding.getViewModel() != null) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getNews(position).getUrl()));

                if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                    getContext().startActivity(intent);
                }
            }
        });

        return new Holder(binding);
    }

    @Override
    public void onBindViewHolder(NewsAdapter.Holder holder, int position) {
        holder.init(getNews(position));
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public void addNews(List<News> data) {
        int prevSize = this.data.size();

        if (data.size() > 0) {
            this.data.addAll(data);
        } else
            finished = true;
        notifyItemRangeChanged(prevSize, prevSize + data.size());
    }

    public void refreshItem(News news) {
        if (data.contains(news)) {
            final int idx = data.indexOf(news);
            data.remove(idx);
            data.add(idx, news);
            notifyItemChanged(idx);
        }
    }

    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }

    class Holder extends RecyclerView.ViewHolder {

        private final ItemNews1Binding binding;

        Holder(ItemNews1Binding binding) {
            super(binding.getRoot());

            this.binding = binding;

            binding.shareMs.setOnClickListener(v -> {
                binding.shareIcons.setVisibility(View.GONE);
                newsCallback.onMailShare(getNews());
            });
            binding.newsLike.setOnClickListener(v -> {
                binding.shareIcons.setVisibility(View.GONE);
                newsCallback.onLikePost(getNews());
            });
            binding.shareFb.setOnClickListener(view -> {
                binding.shareIcons.setVisibility(View.GONE);
                newsCallback.onFbPostShare(getNews());
            });
            binding.shareChat.setOnClickListener(v -> {
                binding.shareIcons.setVisibility(View.GONE);
                newsCallback.onChatShare(getNews());
            });
            binding.shareDoc.setOnClickListener(v -> {
                binding.shareIcons.setVisibility(View.GONE);
                newsCallback.onClipboardCopy(getNews());
            });
            binding.shareTw.setOnClickListener(view -> {
                binding.shareIcons.setVisibility(View.GONE);
                newsCallback.onTwitterShare(getNews());
            });
        }

        void init(News news) {
            binding.setVariable(BR.viewModel, new ItemNewsViewModel(getContext(), news));
            binding.executePendingBindings();
        }

        @Nullable
        private News getNews() {
            if (getAdapterPosition() < data.size()) {
                return data.get(getAdapterPosition());
            }
            return null;
        }
    }
}
