package com.fanzine.coys.adapters.chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.coys.databinding.ItemChatUserListBinding;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.viewmodels.items.chat.ItemUserListViewModel;
import com.fanzine.chat.ChatSDK;
import com.fanzine.chat.interfaces.FCSuccessListener;
import com.fanzine.chat.models.channels.FCChannel;
import com.fanzine.chat.models.user.FCUser;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Woland on 17.04.2017.
 */

public class UsersListAdapter extends BaseAdapter<UsersListAdapter.Holder> {

    private final List<FCUser> data;
    private final FCChannel channel;
    private final boolean isOwner;
    private Set<String> banned;

    public UsersListAdapter(Context context, List<FCUser> users, FCChannel channel) {
        super(context);

        data = users;
        this.channel = channel;

        isOwner = channel.getOwner().equals(ChatSDK.getInstance().getCurrentSession().getCurrentUser().getUid());
    }

    public List<FCUser> getUsers() {
        return data;
    }

    public void setBanned(Set<String> banned) {
        this.banned = banned;
        notifyDataSetChanged();
    }

    @Override
    public UsersListAdapter.Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new Holder(ItemChatUserListBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(UsersListAdapter.Holder holder, int position) {
        holder.init(position, data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void addUsers(List<FCUser> users) {
        this.data.addAll(users);
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private final ItemChatUserListBinding binding;

        public Holder(ItemChatUserListBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void init(int position, FCUser user) {
            ItemUserListViewModel viewModel;
            if (binding.getViewModel() == null) {
                viewModel = new ItemUserListViewModel(getContext());
                binding.setViewModel(viewModel);
            } else
                viewModel = binding.getViewModel();

            viewModel.isOwner.set(isOwner);
            viewModel.isOneToOne.set(channel.isOneToOne());

            viewModel.name.set(user.getFirstName() + " " + user.getLastName());


            if (banned != null) {
                viewModel.isBanned.set(banned.contains(user.getUid()));

                if (isOwner) {
                    RxView.clicks(binding.checkboxLayout).subscribe(aVoid -> {
                        if (binding.checkbox.isChecked()) {
                            ChatSDK.getInstance().getBlacklistManager().unbanUser(channel, user, new FCSuccessListener() {
                                @Override
                                public void onSuccess() {
                                    viewModel.isBanned.set(false);
                                    banned.remove(user.getUid());

                                    viewModel.name.set(user.getFirstName() + " " + user.getLastName());
                                }

                                @Override
                                public void onError(Exception e) {
                                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                                }
                            });
                        } else {
                            ChatSDK.getInstance().getBlacklistManager().banUser(channel, user, new FCSuccessListener() {
                                @Override
                                public void onSuccess() {
                                    viewModel.isBanned.set(true);
                                    banned.add(user.getUid());
                                    viewModel.name.set(user.getFirstName() + " " + user.getLastName() + " " + getContext().getString(R.string.banned));
                                }

                                @Override
                                public void onError(Exception e) {
                                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                                }
                            });
                        }
                    });

                    RxView.clicks(binding.remove).subscribe(aVoid -> {
                        List<FCUser> users = new ArrayList<>();
                        users.add(user);
                        ChatSDK.getInstance().getChatManager().deleteUsers(channel, users, new FCSuccessListener() {
                            @Override
                            public void onSuccess() {
                                data.remove(position);
                                notifyItemRemoved(position);
                            }

                            @Override
                            public void onError(Exception e) {
                                DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                            }
                        });
                    });
                }

                if (viewModel.isBanned.get())
                    viewModel.name.set(viewModel.name.get() + " " + getContext().getString(R.string.banned));
            }

            binding.executePendingBindings();
        }
    }
}
