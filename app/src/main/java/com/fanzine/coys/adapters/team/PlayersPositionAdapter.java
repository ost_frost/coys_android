package com.fanzine.coys.adapters.team;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.fanzine.coys.databinding.ItemPlayerPositionBinding;
import com.fanzine.coys.databinding.ItemPlayerPositionHeaderBinding;
import com.fanzine.coys.models.team.Player;
import com.fanzine.coys.viewmodels.items.team.ItemPlayerPositionViewModel;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by Woland on 23.02.2017.
 */

public class PlayersPositionAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private final List<Player> data;
    private final WeakReference<OnClick> listener;
    private WeakReference<LayoutInflater> inflater;

    public interface OnClick {
        void clicked(int id);
    }

    public PlayersPositionAdapter(Context context, List<Player> players, OnClick click) {
        inflater = new WeakReference<>(LayoutInflater.from(context));
        listener = new WeakReference<>(click);
        this.data = new ArrayList<>(players);
    }

    public Context getContext() {
        return inflater.get().getContext();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;

        if (convertView == null) {
            holder = new Holder(ItemPlayerPositionBinding.inflate(inflater.get(), parent, false));
            convertView = holder.getView();
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.init(data.get(position));

        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderHolder holder;
        if (convertView == null) {
            holder = new HeaderHolder(ItemPlayerPositionHeaderBinding.inflate(inflater.get(), parent, false));
            convertView = holder.getView();
            convertView.setTag(holder);
        } else {
            holder = (HeaderHolder) convertView.getTag();
        }

        holder.init(data.get(position).getPosition());
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        return data.get(position).getPosition().charAt(0);
    }

    public class Holder {

        private final ItemPlayerPositionBinding binding;

        public Holder(ItemPlayerPositionBinding binding) {
            this.binding = binding;
        }

        public void init(Player player) {
            if (binding.getViewModel() == null)
                binding.setViewModel(new ItemPlayerPositionViewModel(getContext(), Integer.toString(player.getNumber()), player.getName()));
            else
                binding.getViewModel().init(Integer.toString(player.getNumber()), player.getName());

            binding.getRoot().setOnClickListener(v -> {
                if (listener != null && listener.get() != null)
                    listener.get().clicked(player.getId());
            });
        }

        public View getView() {
            return binding.getRoot();
        }
    }

    public class HeaderHolder {

        private final ItemPlayerPositionHeaderBinding binding;

        public HeaderHolder(ItemPlayerPositionHeaderBinding binding) {
            this.binding = binding;
        }

        public View getView() {
            return binding.getRoot();
        }

        public void init(String name) {
            if (binding.getViewModel() == null)
                binding.setViewModel(new ItemPlayerPositionViewModel(getContext(), "", name));
            else
                binding.getViewModel().init("", name);
        }
    }
}
