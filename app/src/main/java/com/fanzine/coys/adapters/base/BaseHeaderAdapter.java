package com.fanzine.coys.adapters.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter;

/**
 * Created by Woland on 17.01.2017.
 */

public abstract class BaseHeaderAdapter<VH extends RecyclerView.ViewHolder, HVH extends RecyclerView.ViewHolder> extends BaseAdapter<VH> implements StickyHeaderAdapter<HVH> {

    public BaseHeaderAdapter(Context context) {
        super(context);
    }

    @Override
    public HVH onCreateHeaderViewHolder(ViewGroup parent) {
        return onCreateHeaderViewHolder(getLayoutInflater(), parent);
    }

    public abstract HVH onCreateHeaderViewHolder(LayoutInflater inflater, ViewGroup parent);

}
