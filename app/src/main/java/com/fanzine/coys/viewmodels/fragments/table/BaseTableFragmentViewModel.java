package com.fanzine.coys.viewmodels.fragments.table;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.fragments.table.PlayersFragment;
import com.fanzine.coys.fragments.table.RegionsLoadingListener;
import com.fanzine.coys.fragments.table.TableFixturesFragment;
import com.fanzine.coys.fragments.table.TablesFragment;
import com.fanzine.coys.fragments.table.TeamsFragment;
import com.fanzine.coys.models.table.RegionLeague;
import com.fanzine.coys.utils.FragmentUtils;;
import com.fanzine.coys.viewmodels.base.BaseLeaguesBarViewModel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Woland on 13.02.2017.
 */

public class BaseTableFragmentViewModel extends BaseLeaguesBarViewModel {

    private WeakReference<FragmentManager> fragmentManager;

    enum Tabs {
        TABLES, FIXTURES, TEAMS, PLAYERS
    }

    private Tabs selectedTab;

    private RegionsLoadingListener regionsLoadingListener;


    public BaseTableFragmentViewModel(Context context, FragmentManager fragmentManager, BaseLeaguesBarViewModel.LeagueLoadingListener leagueLoadingListener, RegionsLoadingListener regionsLoadingListener) {
        super(context, leagueLoadingListener);
        this.fragmentManager = new WeakReference<>(fragmentManager);
        this.regionsLoadingListener = regionsLoadingListener;
    }

    public void restore() {
        if (selectedTab == Tabs.FIXTURES) {
            openFixtures();
        } else if (selectedTab == Tabs.PLAYERS) {
            openPlayers();
        } else if (selectedTab == Tabs.TEAMS) {
            openTeams();
        } else if (selectedTab == Tabs.TABLES) {
            openTables();
        }
    }

    public void openTables() {
        selectedTab = Tabs.TABLES;
        changeContent(TablesFragment.newInstance());
    }

    public void openFixtures() {
        selectedTab = Tabs.FIXTURES;
        changeContent(new TableFixturesFragment());
    }

    public void openTeams() {
        selectedTab = Tabs.TEAMS;
        changeContent(new TeamsFragment());
    }

    public void openPlayers() {
        selectedTab = Tabs.PLAYERS;
        changeContent(new PlayersFragment());
    }

    public void loadRegions() {

        ApiRequest.getInstance().getApi().getRegionsLeagues()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<JsonArray>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(JsonArray items) {
                        Type regionLeagueType =
                                new TypeToken<List<RegionLeague>>() {
                                }.getType();

                        List<RegionLeague> mapItems = new Gson().fromJson(items, regionLeagueType);

                        regionsLoadingListener.onLeagueRegionsLoaded(mapItems);
                    }
                });
    }

    private void changeContent(Fragment fragment) {
        FragmentUtils.changeFragment(fragmentManager.get(), R.id.content_frame, fragment, true);
    }
}
