package com.fanzine.coys.viewmodels.fragments.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.ObservableField;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.Message;
import com.fanzine.coys.fragments.start.VerifyPassDialog;
import com.fanzine.coys.models.login.Country;
import com.fanzine.coys.utils.DialogUtils;
import com.google.firebase.crash.FirebaseCrash;

import java.util.HashMap;
import java.util.Map;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Woland on 05.01.2017.
 */

public class ForgotPassViewModel extends AuthViewModel {

    public ObservableField<String> phone = new ObservableField<>();
    public ObservableField<Country> country = new ObservableField<>();
    public ObservableField<String> pass = new ObservableField<>();
    public ObservableField<String> passConfirm = new ObservableField<>();

    private DialogInterface mDi;

    public ForgotPassViewModel(Context context, DialogInterface di) {
        super(context);

        this.mDi = di;
    }

    public void onClick() {
        try {

            if (isDataInvalid()) {
                DialogUtils.showAlertDialog(getContext(), R.string.fieldsRequired);
            } else if (isPasswordsMismatch()) {
                DialogUtils.showErrorDialog(getContext(), getString(R.string.passwords_mismatch));
            } else
                resetPassword();

        } catch (Exception e) {
            FirebaseCrash.report(e);
            FirebaseCrash.log(e.getMessage());
        }

    }

    private boolean isPasswordsMismatch() {
        return !pass.get().equals(passConfirm.get());
    }

    private boolean isDataInvalid() {
        boolean isInvalid = TextUtils.isEmpty(phone.get()) || country.get() == null || TextUtils.isEmpty(pass.get())
                || TextUtils.isEmpty(passConfirm.get());

        return isInvalid;
    }

    private void resetPassword() {
        ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

        Subscriber<Message> subscriber = new Subscriber<Message>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                pd.dismiss();

                try {
                    ErrorResponseParser.ApiError error = ErrorResponseParser.getApiError(e);
                    if (error.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), error.getError().getMessage());
                    }
                } catch (Exception validationException) {
                    FirebaseCrash.report(e);

                    Log.i(getClass().getName(), validationException.getMessage());
                }
            }

            @Override
            public void onNext(Message message) {
                pd.dismiss();
                mDi.cancel();

                VerifyPassDialog verifyPassDialog =
                        VerifyPassDialog.newInstance(
                                Integer.toString(country.get().getPhonecode()), phone.get(), pass.get());


                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                verifyPassDialog.show(fragmentManager, verifyPassDialog.getTag());
            }
        };

        subscription.add(subscriber);

        Map<String, String> map = new HashMap<>();
        map.put("phonecode", Integer.toString(country.get().getPhonecode()));
        map.put("phone", phone.get());

        ApiRequest.getInstance().getApi().passwordReset(map)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}