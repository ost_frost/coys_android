package com.fanzine.coys.viewmodels.fragments.login;

import android.content.Context;
import android.databinding.ObservableField;
import android.text.TextUtils;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.fragments.start.ForgotPassFragment;
import com.fanzine.coys.models.login.Country;
import com.fanzine.coys.models.login.Credentials;
import com.fanzine.coys.models.login.UserToken;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.validators.EmailValidator;
import com.google.firebase.crash.FirebaseCrash;

import java.util.HashMap;
import java.util.Map;

import rx.Observable;

/**
 * Created by Woland on 05.01.2017.
 */

public class LoginFragmentViewModel extends AuthViewModel {

    public ObservableField<String> phone = new ObservableField<>();
    public ObservableField<String> email = new ObservableField<>();
    public ObservableField<String> pass = new ObservableField<>();
    public ObservableField<Country> country = new ObservableField<>();
    public ObservableField<Boolean> isRemember = new ObservableField<>();

    private EmailValidator emailValidator;

    public LoginFragmentViewModel(Context context) {
        super(context);

        emailValidator = new EmailValidator();

        Credentials credentials = SharedPrefs.getUserCredentials();

        if (credentials != null) {
            isRemember.set(true);

            restoreCredentials(credentials);
        } else {
            isRemember.set(false);
        }
    }

    public void onLoginClick() {

        try {

            Map<String, String> fields = new HashMap<>();

            //todo:: refatctor fields validation
            if (isLoginByEmail()) {

                String gunnersEmail = email.get() + "@gunners.com";

                if (!emailValidator.validate(gunnersEmail)) {
                    DialogUtils.showAlertDialog(getContext(), R.string.email_incorrect);
                    return;
                }

                fields.put("email", email.get());
            } else if (isLoginByPhone()) {
                fields.put("phonecode", Integer.toString(country.get().getPhonecode()));
                fields.put("phone", phone.get());
            }

            fields.put("password", pass.get());

            for (String key : fields.keySet()) {
                if (TextUtils.isEmpty(fields.get(key))) {
                    DialogUtils.showAlertDialog(getContext(), R.string.fieldsRequired);

                    return;
                }
            }

            if (isRemember.get()) {

                Credentials credentials;

                if (isLoginByEmail())
                    credentials = new Credentials(email.get(), pass.get());
                else
                    credentials = new Credentials(country.get(), phone.get(), pass.get());

                SharedPrefs.saveUserCredentials(credentials);
            } else {
                SharedPrefs.flushCredentials();
            }

            if (NetworkUtils.isNetworkAvailable(getContext())) {
                login(fields);
            } else {
                DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
            }
        } catch (Exception e) {
            FirebaseCrash.report(e);
            FirebaseCrash.log(e.getMessage());
        }
    }

    public void forgotPass() {
        ForgotPassFragment forgotPassFragment = new ForgotPassFragment();
        forgotPassFragment.show(getActivity().getSupportFragmentManager(), ForgotPassFragment.class.getName());
    }

    private boolean isLoginByEmail() {
        return !TextUtils.isEmpty(email.get()) && !TextUtils.isEmpty(pass.get());
    }

    private boolean isLoginByPhone() {
        return !TextUtils.isEmpty(phone.get()) && country != null;
    }

    private void login(Map<String, String> fields) {
        Observable<UserToken> tokenObservable = ApiRequest.getInstance().getApi().login(fields);

        auth(tokenObservable);
    }

    private void restoreCredentials(Credentials data) {
        if (data.isEmail()) {
            email.set(data.getEmail());
        } else {
            country.set(data.getCountry());
            phone.set(data.getPhone());
        }

        pass.set(data.getPassword());
    }
}
