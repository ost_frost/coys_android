package com.fanzine.coys.viewmodels.fragments.video;

import android.content.Context;

import com.fanzine.coys.viewmodels.base.BaseLeaguesBarViewModel;

/**
 * Created by mbp on 1/8/18.
 */

public class BaseVideosViewModel extends BaseLeaguesBarViewModel {

    public BaseVideosViewModel(Context context, LeagueLoadingListener leagueListener) {
        super(context, leagueListener);
    }
}
