package com.fanzine.coys.viewmodels.fragments.venue;

import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.net.Uri;

import com.fanzine.coys.R;
import com.fanzine.coys.api.YelpConfig;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;
import com.yelp.fusion.client.connection.YelpFusionApi;
import com.yelp.fusion.client.connection.YelpFusionApiFactory;
import com.yelp.fusion.client.models.Business;
import com.yelp.fusion.client.models.Category;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * Created by Woland on 07.03.2017.
 */

public class MapVenuesViewModel extends BaseViewModel {

    private WeakReference<OnBusinessLoaded> mListener;

    public ObservableField<Business> business = new ObservableField<>();

    public interface OnBusinessLoaded {
        void onLoaded(Business business);
    }

    public MapVenuesViewModel(Context context, OnBusinessLoaded listener) {
        super(context);
        mListener = new WeakReference<>(listener);
    }

    public MapVenuesViewModel(Context context) {
        super(context);
    }

    public void loadBusiness(String id) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {

            YelpFusionApiFactory apiFactory = new YelpFusionApiFactory();

            Observable.fromCallable(() -> apiFactory.createAPI(YelpConfig.clientId, YelpConfig.clientSecret))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(Schedulers.newThread())      // subscriber on different thread
                    .subscribe(new Subscriber<YelpFusionApi>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            getActivity().runOnUiThread(() -> {
                                DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                            });
                        }

                        @Override
                        public void onNext(YelpFusionApi yelpAPI) {
                            Call<Business> call = yelpAPI.getBusiness(id);

                            call.enqueue(new Callback<Business>() {
                                @Override
                                public void onResponse(Call<Business> call, Response<Business> response) {
                                    if (mListener != null && mListener.get() != null)
                                        mListener.get().onLoaded(response.body());
                                }

                                @Override
                                public void onFailure(Call<Business> call, Throwable t) {
                                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                                }
                            });
                        }
                    });

        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    public String getAddress(Business business) {
        if (business != null) {
            com.yelp.fusion.client.models.Location location = business.getLocation();
            return location.getAddress1() + ", " + location.getCity() + ", " + location.getCountry() + ", " + location.getZipCode();
        } else
            return "";
    }

    public String getCategories(Business business) {
        if (business != null) {
            ArrayList<Category> categories = business.getCategories();
            String category = "";

            for (int i = 0; i < categories.size(); ++i) {
                category += categories.get(i).getTitle();

                if (i != categories.size() - 1)
                    category += ", ";
            }

            return category;
        } else
            return "";
    }

    public int getRating(Business business) {
        if (business != null) {
            return (int) business.getRating();
        } else
            return 0;
    }

    public void emptyClick() {
    }

    public void goToWebSite() {
        getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(business.get().getUrl())));
    }

    public void call() {
        getContext().startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + business.get().getDisplayPhone())));
    }

    public void view() {
    }

}
