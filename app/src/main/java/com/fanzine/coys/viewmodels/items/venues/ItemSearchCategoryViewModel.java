package com.fanzine.coys.viewmodels.items.venues;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.models.venues.VenueSearchItem;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by mbp on 12/27/17.
 */

public class ItemSearchCategoryViewModel extends BaseViewModel {

    public ObservableField<VenueSearchItem> category = new ObservableField<>();

    public ItemSearchCategoryViewModel(Context context, VenueSearchItem item) {
        super(context);

        this.category.set(item);
    }
}
