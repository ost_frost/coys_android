package com.fanzine.coys.viewmodels.fragments.mails;

import android.content.Context;
import android.util.Log;

import com.fanzine.coys.api.response.MailboxFolders;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.mails.Folder;
import com.fanzine.coys.networking.EmailDataRequestManager;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;

import rx.Subscriber;

import static com.fanzine.coys.utils.LoadingStates.DONE;
import static com.fanzine.coys.utils.LoadingStates.ERROR;
import static com.fanzine.coys.utils.LoadingStates.LOADING;

/**
 * Created by Evgenij Krasilnikov on 09-Feb-18.
 */

public class MailHomeViewModel extends BaseStateViewModel<Folder> {

    public MailHomeViewModel(Context context, DataListLoadingListener<Folder> listener) {
        super(context, listener);
    }

    @Override
    public void loadData(int page) {
        Subscriber<MailboxFolders> subscriber = new Subscriber<MailboxFolders>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                if (getStateValue() == LOADING)
                    setState(ERROR);

                getListener().onError();
                getListener().onLoaded(new Folder());
            }

            @Override
            public void onNext(MailboxFolders mailboxFolders) {
                Log.i("FOLDER", "folder list.size()="+mailboxFolders.getFolders());

                setState(DONE);
                getListener().onLoaded(mailboxFolders.getFolders());
            }
        };
        subscription.add(subscriber);

        EmailDataRequestManager.getInstanse()
                .getMailboxFolders()
                .subscribe(subscriber);
    }
}
