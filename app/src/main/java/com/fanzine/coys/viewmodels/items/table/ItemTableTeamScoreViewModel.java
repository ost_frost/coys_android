package com.fanzine.coys.viewmodels.items.table;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.models.table.TeamScore;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by maximdrobonoh on 29.09.17.
 */

public class ItemTableTeamScoreViewModel extends BaseViewModel {

    public ObservableField<TeamScore> teamScore = new ObservableField<>();


    public ItemTableTeamScoreViewModel(Context context, TeamScore teamScore) {
        super(context);

        this.teamScore.set(teamScore);
    }

    public String getPosition() {
        return String.valueOf(teamScore.get().getPosition());
    }

    public String getTotal() {
        return String.valueOf(teamScore.get().getTotal());
    }

    public String getIcon() {
        return String.valueOf(teamScore.get().getTeamIcon());
    }
}
