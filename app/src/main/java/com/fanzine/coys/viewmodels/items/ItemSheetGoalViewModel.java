package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;

import com.fanzine.coys.models.table.PlayerStats;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 18.01.2017.
 */

public class ItemSheetGoalViewModel extends BaseViewModel {

    public ObservableBoolean isClicked = new ObservableBoolean();
    public ObservableInt position = new ObservableInt();
    public ObservableField<PlayerStats> player = new ObservableField<>();

    public ItemSheetGoalViewModel(Context context) {
        super(context);
    }

}
