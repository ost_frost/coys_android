package com.fanzine.coys.viewmodels.items.dialog;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 21.04.2017.
 */

public class ItemTeamNotificationViewModel extends BaseViewModel {

    public ObservableField<String> name = new ObservableField<>();
    public ObservableField<String> icon = new ObservableField<>();

    public ItemTeamNotificationViewModel(Context context) {
        super(context);
    }

}
