package com.fanzine.coys.viewmodels.fragments.match;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.lineups.LineUp;
import com.fanzine.coys.models.lineups.Substitution;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;

import java.util.Collections;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.utils.LoadingStates.DONE;
import static com.fanzine.coys.utils.LoadingStates.ERROR;
import static com.fanzine.coys.utils.LoadingStates.NO_DATA;

/**
 * Created by Woland on 09.02.2017.
 */

public class LineUpFragmentViewModel extends BaseStateViewModel<LineUp> {

    private final int matchId;

    public ObservableField<String> localIcon = new ObservableField<>();
    public ObservableField<String> localName = new ObservableField<>();
    public ObservableField<String> guestIcon = new ObservableField<>();
    public ObservableField<String> guestName = new ObservableField<>();

    public ObservableField<LineUp> mLineUp = new ObservableField<>();

    public LineUpFragmentViewModel(Context context, int id, DataListLoadingListener<LineUp> listener) {
        super(context, listener);
        this.matchId = id;
    }

    @Override
    public void loadData(int i) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            Subscriber<LineUp> subscriber = new Subscriber<LineUp>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    setState(ERROR);
                    e.printStackTrace();
                    getListener().onError();
                }

                @Override
                public void onNext(LineUp lineUp) {
                    if ( lineUp.getLocalteam() != null && lineUp.getVisitorteam() != null) {
                        getListener().onLoaded(lineUp);
                        setState(DONE);

                        sortSubstitution(lineUp.getLocalteam().getSubstitution());
                        sortSubstitution(lineUp.getVisitorteam().getSubstitution());

                        mLineUp.set(lineUp);

                        localIcon.set(lineUp.getLocalteam().getLocalteamIcon());
                        localName.set(lineUp.getLocalteam().getLocalteamName());

                        guestIcon.set(lineUp.getVisitorteam().getVisitorteamIcon());
                        guestName.set(lineUp.getVisitorteam().getVisitorteamName());
                    } else {
                        setState(NO_DATA);
                        getListener().onError();
                    }
                }
            };
            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getLineUp(matchId)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            setState(ERROR);
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    private void sortSubstitution(List<Substitution> list) {
        Collections.sort(list,
                (o1, o2) -> {
                    if (o1.getOnPlayerNumber() == o2.getOnPlayerNumber())
                        return 0;
                    return o1.getOnPlayerNumber() < o2.getOnPlayerNumber() ? -1 : 1;
                });
    }
}
