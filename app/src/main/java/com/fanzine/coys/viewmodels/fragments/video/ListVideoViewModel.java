package com.fanzine.coys.viewmodels.fragments.video;

import android.content.Context;
import android.util.Log;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.Video;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.fragments.BaseVideoViewModel;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.utils.LoadingStates.ERROR;

/**
 * Created by Woland on 19.01.2017.
 */

public class ListVideoViewModel extends BaseVideoViewModel {

    private String sortBy;
    private String uploadDate;
    private String query;

    public ListVideoViewModel(Context context, DataListLoadingListener<Video> listener, String sortBy, String uploadDate) {
        super(context, listener);

        this.sortBy = sortBy;
        this.uploadDate = uploadDate;

        Log.i(getClass().getName(),"ListVideoViewModel CREATED");
    }

    public void setQuery(String query) {
        this.query = query;
    }

    @Override
    public void loadData(int page) {
        Log.i(getClass().getName(),"CALL LOAD DATA #");

        if (NetworkUtils.isNetworkAvailable(getContext())) {

            Subscriber<List<Video>> subscriber = getSubscriber(page);

            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getVideoList(sortBy, uploadDate, query)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
            setState(ERROR);
        }
    }
}
