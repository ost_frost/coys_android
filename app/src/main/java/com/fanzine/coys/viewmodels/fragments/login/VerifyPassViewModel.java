package com.fanzine.coys.viewmodels.fragments.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableField;
import android.text.TextUtils;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.exceptions.VerificationCodeException;
import com.fanzine.coys.models.login.UserToken;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.google.firebase.crash.FirebaseCrash;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Woland on 05.01.2017.
 */

public class VerifyPassViewModel extends AuthViewModel {

    public ObservableField<String> resetPassword = new ObservableField<>();

    private String phone;
    private String phoneCode;
    private String newPassword;

    public VerifyPassViewModel(Context context, String phoneCode, String phone, String newPassword) {
        super(context);

        this.phone = phone;
        this.phoneCode = phoneCode;
        this.newPassword = newPassword;
    }


    public void onClick() {

        try {

            if (TextUtils.isEmpty(resetPassword.get())) {
                DialogUtils.showAlertDialog(getContext(), R.string.fieldsRequired);
            } else {
                if (NetworkUtils.isNetworkAvailable(getContext())) {
                    ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

                    Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            pd.dismiss();

                            if (e instanceof VerificationCodeException) {
                                pd.dismiss();
                                DialogUtils.showAlertDialog(getContext(), e.getMessage());
                            } else {

                                List<FieldError> errors = ErrorResponseParser.getErrorList(e);

                                if (errors.isEmpty()) {
                                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                                } else {
                                    DialogUtils.showAlertDialog(getContext(), errors.get(0).getMessage());
                                }
                            }
                        }

                        @Override
                        public void onNext(ResponseBody message) {
                            pd.dismiss();

                            Map<String, String> fields = new HashMap<>();
                            fields.put("phonecode", phoneCode);
                            fields.put("phone", phone);
                            fields.put("password", newPassword);

                            Observable<UserToken> tokenObservable =
                                    ApiRequest.getInstance().getApi().login(fields);

                            auth(tokenObservable);
                        }
                    };

                    subscription.add(subscriber);

                    Map<String, String> map = new HashMap<>();
                    map.put("phone", phone);
                    map.put("phonecode", phoneCode);
                    map.put("reset_password", resetPassword.get());

                    ApiRequest.getInstance().getApi().recoveryPasswordCheck(map)
                            .flatMap(responseBody -> {
                                if (!responseBody.toString().contains("errors")) {
                                    map.put("password", newPassword);
                                    return ApiRequest.getInstance().getApi().saveNewPassword(map);
                                }

                                return Observable.error(new VerificationCodeException("The verification code is incorrect"));
                            })
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(subscriber);
                } else {
                    DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
                }
            }
        } catch (Exception e) {
            FirebaseCrash.log(e.getMessage());
            FirebaseCrash.report(e);
        }
    }
}
