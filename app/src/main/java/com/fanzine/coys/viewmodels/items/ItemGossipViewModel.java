package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.databinding.ObservableField;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.coys.models.Gossip;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by mbp on 3/6/18.
 */

public class ItemGossipViewModel extends BaseViewModel {

    public ObservableField<Gossip> gossip = new ObservableField<>();

    public ItemGossipViewModel(Context context, Gossip gossip) {
        super(context);
        this.gossip.set(gossip);
    }

    public String getUrlName(Gossip gossip) {
        return "(" + gossip.getUrlName() + ")";
    }

    public boolean isBoldText(Gossip gossip) {
        return gossip.getPosition() % 2 != 0;
    }

    public void openUrl(Gossip gossip) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(gossip.getUrl()));
        getActivity().startActivity(intent);
    }

    @android.databinding.BindingAdapter("android:typeface")
    public static void setTypeface(TextView v, String style) {
        AssetManager assetManager = v.getContext().getAssets();
        Typeface typeface = Typeface.createFromAsset(assetManager, "fonts/roboto/Roboto-Regular.ttf");

        switch (style) {
            case "bold":
                v.setTypeface(typeface, Typeface.BOLD);
                v.setTextColor(ContextCompat.getColor(v.getContext(), R.color.black));
                break;
            default:
                v.setTypeface(typeface, Typeface.NORMAL);
                v.setTextColor(ContextCompat.getColor(v.getContext(), R.color.item_gossip_text_black_light));
                break;
        }
    }
}
