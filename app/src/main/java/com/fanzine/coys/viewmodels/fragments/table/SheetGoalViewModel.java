package com.fanzine.coys.viewmodels.fragments.table;

import android.content.Context;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.response.PlayerStatsData;
import com.fanzine.coys.fragments.base.BaseTopPanelFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseTopPanelViewModel;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.utils.LoadingStates.DONE;
import static com.fanzine.coys.utils.LoadingStates.ERROR;
import static com.fanzine.coys.utils.LoadingStates.NO_DATA;

/**
 * Created by Woland on 22.02.2017.
 */

public class SheetGoalViewModel extends BaseTopPanelViewModel<PlayerStatsData> {

    public SheetGoalViewModel(Context context, LeagueLoadingListener leagueListener, DataListLoadingListener<PlayerStatsData> listener) {
        super(context, leagueListener, listener);
    }

    @Override
    public void startDataLoading(int page) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            Subscriber<PlayerStatsData> subscriber = new Subscriber<PlayerStatsData>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    setState(ERROR);
                    e.printStackTrace();
                    getListener().onError();
                }

                @Override
                public void onNext(PlayerStatsData data) {
                    if (!data.isEmpty()) {
                        getListener().onLoaded(data);
                        setState(DONE);
                    } else {
                        setState(NO_DATA);
                        getListener().onError();
                    }
                }
            };
            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getPlayerStats(BaseTopPanelFragment.getSelectedLeagueId())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        }
        else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);

            setState(ERROR);
        }
    }
}
