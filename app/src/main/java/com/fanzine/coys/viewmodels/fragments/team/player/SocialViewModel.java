package com.fanzine.coys.viewmodels.fragments.team.player;

import android.content.Context;

import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.Social;
import com.fanzine.coys.models.team.Player;
import com.fanzine.coys.utils.LoadingStates;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mbp on 1/13/18.
 */

public class SocialViewModel extends BaseStateViewModel<Social> {

    private Player player;

    public SocialViewModel(Context context, DataListLoadingListener<Social> listener, Player player) {
        super(context, listener);
        this.player = player;
    }

    @Override
    public void loadData(int page) {

        Subscriber<List<Social>> subscriber = new Subscriber<List<Social>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                setState(LoadingStates.ERROR);
            }

            @Override
            public void onNext(List<Social> news) {
                setState(LoadingStates.DONE);
                getListener().onLoaded(news);
            }
        };

        subscription.add(subscriber);

        ApiRequest.getInstance().getApi().getPlayerSocial(player.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}
