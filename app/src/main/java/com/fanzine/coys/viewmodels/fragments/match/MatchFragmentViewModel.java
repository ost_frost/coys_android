package com.fanzine.coys.viewmodels.fragments.match;

import android.content.Context;
import android.databinding.ObservableField;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.fragments.match.AnalysisFragment;
import com.fanzine.coys.fragments.match.CommentaryFragment;
import com.fanzine.coys.fragments.match.LineUpFragment;
import com.fanzine.coys.fragments.match.StatsFragment;
import com.fanzine.coys.fragments.match.SummaryFragment;
import com.fanzine.coys.interfaces.MatchDetailLoading;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

import java.lang.ref.WeakReference;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Siarhei on 17.01.2017.
 */

public class MatchFragmentViewModel extends BaseViewModel {

    private WeakReference<FragmentManager> fragmentManager;
    private WeakReference<Match> match;
    public ObservableField<String> leagueTitle = new ObservableField<>();
    private MatchDetailLoading matchDetailLoading;

    public MatchFragmentViewModel(Context context, FragmentManager fragmentManager, Match match,
                                  MatchDetailLoading matchDetailLoading) {
        super(context);
        this.fragmentManager = new WeakReference<>(fragmentManager);
        this.match = new WeakReference<>(match);
        this.matchDetailLoading = matchDetailLoading;

        openLineUp();
    }

    public void setLeagueTitle(String  title) {
        leagueTitle.set(title);
    }

    public void getMatchDetail() {

        ApiRequest.getInstance().getApi().getMatch(match.get().getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Match>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Match match) {
                        matchDetailLoading.onLoaded(match);
                    }
                });
    }

    public void openLineUp() {
        changeContent(LineUpFragment.newInstance(match.get()));
    }

    public void openCommentary() {
        changeContent(CommentaryFragment.newInstance(match.get()));
    }

    public void openSummary() {
        changeContent(SummaryFragment.newInstance(match.get()));
    }

    public void openStats() {
        changeContent(StatsFragment.newInstance(match.get()));
    }

    public void openAnalysis() {
        changeContent(AnalysisFragment.newInstance(match.get()));
    }

    private void changeContent(Fragment fragment) {
        FragmentUtils.changeFragment(fragmentManager.get(), R.id.content_match, fragment, false);
    }

    public String getTitle() {
        return this.leagueTitle.get();
    }

    public String getTime() {
        return match.get().getTimeBeginWithLocaleTime();
    }

    public String getHomeTeamTitle() {
        return match.get().getHomeTeam().getName();
    }

    public String getGuestTeamTitle() {
        return match.get().getGuestTeam().getName();
    }

    public String getHomeIcon() {
        return match.get().getHomeTeam().getIcon();
    }

    public String getGuestIcon() {
        return match.get().getGuestTeam().getIcon();
    }

    public String getTotalScore() {
        return match.get().getTotalFormatted();
    }
}
