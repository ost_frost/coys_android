package com.fanzine.coys.viewmodels.fragments.mails;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableField;
import android.widget.Toast;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.mails.AttachmentsAdapter;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.api.request.EmailSend;
import com.fanzine.coys.models.Person;
import com.fanzine.coys.models.mails.Attachment;
import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Siarhei on 23.01.2017.
 */

@Deprecated
public class NewMailViewModel extends BaseViewModel {

    public ObservableField<String> message = new ObservableField<>();
    public ObservableField<String> subject = new ObservableField<>();

    private AttachmentsAdapter adapter;

    public NewMailViewModel(Context context, AttachmentsAdapter adapter) {
        super(context);

        this.adapter = adapter;

        String signuture = SharedPrefs.getEmailSignuture();


        if ( signuture.length() > 0 ) {
            this.message.set(System.getProperty ("line.separator") + signuture);
        }
    }


    public void setMail(Mail mail) {
        subject.set("Re: " + mail.getSubject());
    }

    public void sendMail(List<Person> to, List<Person> cc, List<Person> bcc) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(ResponseBody body) {
                    pd.dismiss();

                    Toast.makeText(getContext(), R.string.emailSended, Toast.LENGTH_SHORT).show();

                    //todo:: ListEmailActivity
                   // FragmentUtils.changeFragment(getActivity(), R.id.content_frame, ListEmailActivity.newInstance(null), false);
                }
            };
            subscription.add(subscriber);


            List<Attachment> attachments = adapter.getItems();
            List<MultipartBody.Part> parts = new ArrayList<>();

            for (Attachment attachment : attachments) {
                RequestBody requestFile = MultipartBody.create(MediaType.parse("multipart/form-data"),
                        attachment.getFile());

                MultipartBody.Part att = MultipartBody.Part.createFormData("attachments[]", attachment.getName(), requestFile);
                parts.add(att);
            }

            EmailSend emailSend = new EmailSend(to,cc, bcc, subject.get(), message.get());

            HashMap<String, RequestBody> emailFields = new HashMap<>();

            emailFields.put("to", createPartFromString(emailSend.getTo()));
            emailFields.put("subject", createPartFromString(emailSend.getSubject()));
            emailFields.put("cc", createPartFromString(emailSend.getCc()));
            emailFields.put("bcc", createPartFromString(emailSend.getBcc()));
            emailFields.put("body", createPartFromString(emailSend.getBody()));;

            ApiRequest.getInstance().getApi()
                    .sendMail(emailFields , parts)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);

        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    private RequestBody createPartFromString(String part) {
        return  MultipartBody.create(MultipartBody.FORM, part);
    }
}
