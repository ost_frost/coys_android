package com.fanzine.coys.viewmodels.items.table;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.models.table.PlayerStats;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by maximdrobonoh on 29.09.17.
 */

public class ItemTablePlayerViewModel extends BaseViewModel {

    public ObservableField<PlayerStats> player = new ObservableField<>();

    public ItemTablePlayerViewModel(Context context, PlayerStats playerStats) {
        super(context);

        this.player.set(playerStats);
    }

    public ObservableField<PlayerStats> getPlayer() {
        return player;
    }

    public String getPosition() {
        return String.valueOf(player.get().getPosition());
    }

    public String getTeam() {
        return player.get().getShorTeamName();
    }

    public String getTeamIcon() { return player.get().getTeamIcon(); }

    public String getTotal() {
        return String.valueOf(player.get().getTotal());
    }

    public String getPlayerName() {
        return String.valueOf(player.get().getShortPlayerName());
    }
}
