package com.fanzine.coys.viewmodels.fragments.table;

import android.content.Context;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.fragments.base.BaseLeaguesBarFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.table.Standings;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.utils.LoadingStates.DONE;
import static com.fanzine.coys.utils.LoadingStates.ERROR;
import static com.fanzine.coys.utils.LoadingStates.NO_DATA;

/**
 * Created by maximdrobonoh on 22.09.17.
 */

public class TablesFragmentViewModel extends BaseStateViewModel<Standings> {

    public TablesFragmentViewModel(Context context, DataListLoadingListener<Standings> listener) {
        super(context, listener);
    }

    @Override
    public void loadData(int page) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            Subscriber<Standings> subscriber = new Subscriber<Standings>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    setState(ERROR);
                    e.printStackTrace();
                    getListener().onError();
                }

                @Override
                public void onNext(Standings standings) {
                    if (standings.getTeams().size() > 0) {

                        getListener().onLoaded(standings);
                        setState(DONE);
                    } else {
                        setState(NO_DATA);
                        getListener().onError();
                    }
                }
            };
            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getTeamTables(BaseLeaguesBarFragment.getSelectedLeagueId())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            setState(ERROR);
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }
}

