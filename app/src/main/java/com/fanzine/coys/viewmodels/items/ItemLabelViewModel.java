package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.models.mails.Label;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by maximdrobonoh on 15.09.17.
 */

public class ItemLabelViewModel extends BaseViewModel {

    public ObservableField<Label> label = new ObservableField<>();

    public ItemLabelViewModel(Context context, Label label) {
        super(context);

        this.label.set(label);
    }

    public void setLabel(Label label) {
        this.label.set(label);
    }
}