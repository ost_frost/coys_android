package com.fanzine.coys.viewmodels.items.table;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by maximdrobonoh on 27.09.17.
 */

public class ItemWeekViewModel extends BaseViewModel {

    public ObservableField<String> week = new ObservableField<>();
    public ObservableBoolean isSelected = new ObservableBoolean();

    public ItemWeekViewModel(Context context, String week) {
        super(context);
        this.week.set(week);
    }

    public void setWeek(String week) {
        this.week.set(week);
    }

    public String getTitle() {
        return week.get();
    }
}
