package com.fanzine.coys.viewmodels.items.venues;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.R;
import com.fanzine.coys.viewmodels.base.BaseViewModel;
import com.yelp.fusion.client.models.Business;
import com.yelp.fusion.client.models.Category;
import com.yelp.fusion.client.models.Location;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Woland on 28.02.2017.
 */

public class VenueItemViewModel extends BaseViewModel {

    public ObservableField<Business> business = new ObservableField<>();

    public VenueItemViewModel(Context context) {
        super(context);
    }

    public String getAddress(Business business) {
        Location location = business.getLocation();
        return location.getAddress1() + ", " + location.getCity() + ", " + location.getCountry() + ", " + location.getZipCode();
    }

    public String getCategories(Business business) {
        ArrayList<Category> categories = business.getCategories();
        String category = "";

        for (int i = 0; i < categories.size(); ++i) {
            category += categories.get(i).getTitle();

            if (i != categories.size() - 1)
                category += ", ";
        }

        return category;
    }

   private void setRating(){


   }
    
    public int getRating(Business business) {
        return (int) business.getRating();
    }

    public String getReviewsCount(Business business) {
        return "(" + Integer.toString(business.getReviewCount()) + ")";
    }

    public String getDistance(Business business) {
        return getString(R.string.mi, String.format(Locale.UK, "%.2f", business.getDistance() / 1609.34));
    }
}
