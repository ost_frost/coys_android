package com.fanzine.coys.viewmodels.items.team;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.models.team.PlayerStat;
import com.fanzine.coys.models.team.TeamSquad;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by mbp on 1/11/18.
 */

public class ItemPlayerStatsViewModel extends BaseViewModel {

    public ObservableField<PlayerStat> stat = new ObservableField<>();
    private TeamSquad squad;

    public ItemPlayerStatsViewModel(Context context, PlayerStat playerStat, TeamSquad squad) {
        super(context);
        this.squad = squad;
        this.stat.set(playerStat);
    }

    public String getStatValue(PlayerStat stat) {
        if (stat != null) {
            if (squad == TeamSquad.ACADEMY || squad == TeamSquad.LADIES) {
                return "n/a";
            } else {
                return stat.getValue();
            }
        }
        return "";
    }
}
