package com.fanzine.coys.viewmodels.fragments.chat;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.fanzine.coys.viewmodels.base.BaseViewModel;
import com.fanzine.chat.ChatSDK;
import com.fanzine.chat.interfaces.FCUsersListener;
import com.fanzine.chat.models.channels.FCChannel;

/**
 * Created by Woland on 17.04.2017.
 */

public class ChatUserListViewModel extends BaseViewModel {

    public ObservableBoolean isOwner = new ObservableBoolean();
    public ObservableBoolean isOneToOne = new ObservableBoolean();
    public ObservableField<String> admin = new ObservableField<>();

    public ChatUserListViewModel(Context context) {
        super(context);
    }

    public void loadBanned(FCChannel channel, FCUsersListener listener) {
        ChatSDK.getInstance().getBlacklistManager().getAllBannedUsers(channel, listener);
    }

}
