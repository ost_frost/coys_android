package com.fanzine.coys.viewmodels.fragments.table;

import android.content.Context;

import com.fanzine.coys.fragments.table.BaseTableFragment;
import com.fanzine.coys.interfaces.DataListFilterListener;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.table.Filter;
import com.fanzine.coys.models.table.TeamScore;
import com.fanzine.coys.networking.MatchDataRequestManager;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.Subscriber;

import static com.fanzine.coys.utils.LoadingStates.DONE;
import static com.fanzine.coys.utils.LoadingStates.LOADING;

/**
 * Created by maximdrobonoh on 28.09.17.
 */

public class TeamsViewModel extends BaseStateViewModel<TeamScore> {

    private DataListFilterListener<Filter> filterListener;
    private DataListLoadingListener<TeamScore> teamScoreListener;


    public TeamsViewModel(Context context, DataListLoadingListener<TeamScore> listener,
                          DataListFilterListener<Filter> filterListener) {
        super(context, listener);

        this.teamScoreListener = listener;
        this.filterListener = filterListener;
    }

    @Override
    public void loadData(int page) {

    }

    public void loadData(Filter filter) {

        setState(LOADING);

        Subscriber<List<JsonObject>> teamSubscriber = new Subscriber<List<JsonObject>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<JsonObject> jsonTeamsList) {

                List<TeamScore> teamScores = new ArrayList<>();


                for (JsonObject item : jsonTeamsList) {
                    try {

                        TeamScore teamScore = new TeamScore();
                        teamScore.setTotal(item.get(filter.getFieldName()).getAsInt());
                        teamScore.setTeamIcon(item.get("team_icon").getAsString());
                        teamScore.setTitle(item.get("team_name").getAsString());

                        if (teamScore.getTotal() > 0)
                            teamScores.add(teamScore);
                    } catch (Exception e) {

                    }
                }
                Collections.sort(teamScores, (teamScore, t1) -> {
                    int compare = teamScore.getTotal() - t1.getTotal();

                    compare *= -1;

                    return compare;
                });

                int number = 1;

                for (TeamScore teamScore : teamScores) {
                    teamScore.setPosition(number);

                    number++;
                }

                setState(DONE);
                teamScoreListener.onLoaded(teamScores);
            }
        };
        MatchDataRequestManager.getInstance().getTeamsScore(BaseTableFragment.getSelectedLeagueId(), "2017/2018")
                .subscribe(teamSubscriber);
    }


    public void loadFilters() {

        Subscriber<List<Filter>> filterSubscriber = new Subscriber<List<Filter>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<Filter> teamFilters) {
                filterListener.onFilterLoaded(teamFilters);
            }
        };
        MatchDataRequestManager.getInstance().getTeamFilters()
                .subscribe(filterSubscriber);
    }
}
