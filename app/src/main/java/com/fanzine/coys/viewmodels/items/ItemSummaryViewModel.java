package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.fanzine.coys.R;
import com.fanzine.coys.models.summary.Event;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Siarhei on 17.01.2017.
 */

public class ItemSummaryViewModel extends BaseViewModel {

    public static final String LOCAL_TEAM = "localteam";

    public static final String YELLOW_CARD = "yellowcard";
    public static final String RED_CARD = "redcard";
    public static final String YELLOW_RED_CARD = "yellowred";
    public static final String GOAL = "goal";
    public static final String SUBSTITUTIONS = "subst";
    public static final String EXTRA_MIN = "extra_min";

    public ObservableBoolean isLast = new ObservableBoolean();
    public ObservableBoolean isFirst = new ObservableBoolean();
    public ObservableField<Event> event = new ObservableField<>();

    public ItemSummaryViewModel(Context context, boolean isLast, boolean isFirst, Event event) {
        super(context);
        this.isLast.set(isLast);
        this.event.set(event);
        this.isFirst.set(isFirst);
    }

    public void setEvent(Event event) {
        this.event.set(event);
    }

    public void setLast(boolean isLast) {
        this.isLast.set(isLast);
    }

    public void showGoalDialog() {
//        if (event.get().getType().equals(GOAL)) {
//            GoalDialog.show(getActivity().getSupportFragmentManager(), event.get());
//        }
    }

    public boolean isLocalTeam(Event event) {
        if ( event.getTeam() != null)
        return event.getTeam().equals(LOCAL_TEAM);

        return false;
    }

    public String getMinute(Event event) {
        return String.valueOf(event.getMinute());
    }


    public String getTitle(Event event) {
        if (isGoal(event)) {
            return event.getPlayer() + " (" + event.getResult() + ")";
        }
        return event.getPlayer();
    }

    public String playerName() {
        return event.get().getPlayer();
    }


    public int getImage(Event event) {
        switch (event.getType()) {
            case YELLOW_CARD:
                return R.drawable.match_yellow_card;
            case RED_CARD:
                return R.drawable.match_red_card;
            case YELLOW_RED_CARD:
                return R.drawable.match_yellow_red_card;
            case GOAL:
                return R.drawable.match_goal;
            case SUBSTITUTIONS:
                return R.drawable.ic_icon_player_change;
            default:
                return 0;
        }
    }

    public boolean isExtraMinute(Event event) {
        return event.getExtra_min().intValue() > 0;
    }

    public boolean isGoal(Event event) {
        return event.getType().equals(GOAL);
    }

    public boolean isSubstitution(Event event) {
        return event.getType().equals(SUBSTITUTIONS);
    }

    public String getAssist() {
        return event.get().getAssist();
    }

    public String getExtraMinute(Event event) {
        return  "+" + String.valueOf(event.getExtra_min());
    }

    public boolean isHomeSubstitution(Event event) {
        return isSubstitution(event) & isLocalTeam(event);
    }

    public boolean isGuestSubstitution(Event event) {
        return isSubstitution(event) & !isLocalTeam(event);
    }
}
