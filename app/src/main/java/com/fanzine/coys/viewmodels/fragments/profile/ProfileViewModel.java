package com.fanzine.coys.viewmodels.fragments.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableField;
import android.support.annotation.Nullable;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.StartActivity;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.dialogs.profile.NotificationsDialog;
import com.fanzine.coys.dialogs.profile.UserChatNotificationsDialog;
import com.fanzine.coys.fragments.profile.EmailSettingsFragment;
import com.fanzine.coys.interfaces.OnProfileLoader;
import com.fanzine.coys.models.profile.ChatNotifications;
import com.fanzine.coys.models.profile.EmailNotifications;
import com.fanzine.coys.models.profile.League;
import com.fanzine.coys.models.profile.Profile;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.UpgradeViewModel;
import com.google.firebase.auth.FirebaseAuth;

import java.io.File;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Woland on 03.04.2017.
 */

public class ProfileViewModel extends UpgradeViewModel {

    public ObservableField<String> name = new ObservableField<>();
    public ObservableField<String> avatar = new ObservableField<>();

    private File imageFile;

    public ProfileViewModel(Context context) {
        super(context);
    }

    public void setImageFile(@Nullable File imageFile) {
        this.imageFile = imageFile;
    }

    public void logout() {
        SharedPrefs.saveUserToken(null);
        FirebaseAuth.getInstance().signOut();
        getContext().startActivity(StartActivity.getStartIntent(getContext()));
        getActivity().finish();
    }

    public void getProfile(OnProfileLoader profileLoader) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            Subscriber<Profile> subscriber = new Subscriber<Profile>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(Profile profile) {
                    pd.dismiss();

                    profileLoader.onProfileLoaded(profile);
                }
            };

            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getProfile()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    public void openEmailSettings() {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            Subscriber<EmailNotifications> subscriber = new Subscriber<EmailNotifications>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(EmailNotifications notifications) {
                    pd.dismiss();
                    if (notifications.getNotifyEmail() != -1)
                        FragmentUtils.changeFragment(getActivity(), R.id.content_frame,
                                EmailSettingsFragment.newInstance(notifications), true);
                    else
                        showBuyProDialog();
                }
            };
            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getEmailNotification()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    public void openChatSettings() {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            Subscriber<ChatNotifications> subscriber = new Subscriber<ChatNotifications>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(ChatNotifications notifications) {
                    pd.dismiss();
                    if (notifications.getNotifyChat() != -1)
                        UserChatNotificationsDialog.show(getActivity().getSupportFragmentManager(), notifications);
                    else
                        showBuyProDialog();
                }
            };
            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getChatNotification()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    public void openNotifications() {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            Subscriber<List<League>> subscriber = new Subscriber<List<League>>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(List<League> notifications) {
                    pd.dismiss();
                    NotificationsDialog.show(getActivity().getSupportFragmentManager(), notifications);
                }
            };
            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getTeamsNotification()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

}
