package com.fanzine.coys.viewmodels.dialogs;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.ObservableField;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.models.matches.MatchNotification;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Woland on 20.03.2017.
 */

public class MatchNotificationViewModel extends BaseViewModel {

    public ObservableField<MatchNotification> match = new ObservableField<>();

    public MatchNotificationViewModel(Context context) {
        super(context);
    }

    public void saveMatchNotification(DialogInterface dialogInterface) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(ResponseBody uid) {
                    pd.dismiss();
                    dialogInterface.dismiss();
                }
            };
            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().setMatchNotification(match.get())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    public MatchNotification getMatchNotification() {
        return match.get();
    }

    public void turnAllOff() {
        match.get().turnOffAll();
    }

    public void turnAllOn() {
        match.get().turnOnAll();
    }

    public void toggleKickOff() {
        match.get().toggleKickOf();
    }

    public void toggleLineUp() {
        match.get().toggleLineUp();
    }

    public void toggleGoals() {
        match.get().toggleGoals();
    }

    public void toggleFullTime() {
        match.get().toggleFullTime();
    }

    public void toggleHalfTime() {
        match.get().toggleHalfTime();
    }

    public void togglePenalty() {
        match.get().togglePenalty();
    }

    public void toggleRedCards() {
        match.get().toggleRedCards();
    }

    public Boolean getIsAll() {
        return match.get().isAll();
    }
}
