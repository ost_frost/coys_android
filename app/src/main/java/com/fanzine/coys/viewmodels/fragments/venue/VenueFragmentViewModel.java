package com.fanzine.coys.viewmodels.fragments.venue;

import android.content.Context;
import android.text.TextUtils;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.YelpConfig;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.venues.Category;
import com.fanzine.coys.models.venues.MajorCategory;
import com.fanzine.coys.models.venues.location.BaseRequest;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.fragments.GoogleMapBaseViewModel;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.maps.model.LatLng;
import com.yelp.fusion.client.connection.YelpFusionApi;
import com.yelp.fusion.client.connection.YelpFusionApiFactory;
import com.yelp.fusion.client.models.Business;
import com.yelp.fusion.client.models.SearchResponse;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Woland on 01.03.2017.
 */

public class VenueFragmentViewModel extends GoogleMapBaseViewModel {

    public interface OnVenueLocationLinstener {
        void setVenueLocation(LatLng latLng);

        void onAddressReady(LatLng latLng);
    }

    public interface OnMajorCateogrySetListener {
        void scrollToMajorCategory(String majorCateogry);
    }

    private final WeakReference<DataListLoadingListener<Business>> listener;
    private OnVenueLocationLinstener locationSetLinstener;
    private OnMajorCateogrySetListener onMajorCateogrySetListener;

    public VenueFragmentViewModel(Context context,
                                  DataListLoadingListener<Business> listLoadingListener,
                                  OnVenueLocationLinstener onVenueLocationLinstener,
                                  OnMajorCateogrySetListener onMajorCateogrySetListener) {
        super(context);

        listener = new WeakReference<>(listLoadingListener);
        this.locationSetLinstener = onVenueLocationLinstener;
        this.onMajorCateogrySetListener = onMajorCateogrySetListener;
    }

    @Override
    protected void onPlacesLoaded(PlaceBuffer places) {
        if (places.getStatus().isSuccess())
            locationSetLinstener.onAddressReady(places.get(0).getLatLng());
    }

    @Override
    protected void onPlacesLoadError() {

    }

    public void requestMajorCategory(String category) {

        ApiRequest.getInstance().getApi().getYelpMajorCategory(category)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<MajorCategory>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(MajorCategory majorCategory) {
                        onMajorCateogrySetListener.scrollToMajorCategory(majorCategory.getMajorCategory());
                    }
                });

    }

    public void performRequest(BaseRequest baseRequest) {

        YelpFusionApiFactory apiFactory = new YelpFusionApiFactory();
        //ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);


        Observable.fromCallable(() -> apiFactory.createAPI(YelpConfig.clientId, YelpConfig.clientSecret))
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())      // subscriber on different thread
                .subscribe(new Subscriber<YelpFusionApi>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        getActivity().runOnUiThread(() -> {
//                            if (pd.isShowing())
//                                pd.dismiss();

                            DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                        });
                    }

                    @Override
                    public void onNext(YelpFusionApi yelpAPI) {

                        Map<String, String> params = baseRequest.mapParams();

                        Call<SearchResponse> call = yelpAPI.getBusinessSearch(params);

                        call.enqueue(new Callback<SearchResponse>() {
                            @Override
                            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
//                                if (pd.isShowing())
//                                    pd.dismiss();

                                SearchResponse searchResponse = response.body();
                                double latitude = searchResponse.getRegion().getCenter().getLatitude();
                                double longitude = searchResponse.getRegion().getCenter().getLongitude();

                                LatLng latLng = new LatLng(latitude, longitude);
                                locationSetLinstener.setVenueLocation(latLng);

                                if (searchResponse.getBusinesses() != null && searchResponse.getBusinesses().size() > 0) {
                                    if (listener != null && listener.get() != null) {
                                        listener.get().onLoaded(searchResponse.getBusinesses());
                                    }
                                } else {
                                    //DialogUtils.showAlertDialog(getContext(), R.string.noDataForThisCity);

                                    if (listener != null && listener.get() != null)
                                        listener.get().onLoaded(new ArrayList<>());
                                }
                            }

                            @Override
                            public void onFailure(Call<SearchResponse> call, Throwable t) {
//                                if (pd.isShowing())
//                                    pd.dismiss();

                                DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                            }
                        });
                    }
                });
    }

    @Deprecated
    public void loadVenues(LatLng latLng, String term, Set<Category> selected, Set<Integer> selectedMoney) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {

            YelpFusionApiFactory apiFactory = new YelpFusionApiFactory();

            Observable.fromCallable(() -> apiFactory.createAPI("nWMgOmM0oQj0mf1oVdHUNg", "o3HrKq0YZynDU1rJz70E7ftDFwJra2YMEqBVEzr1U2RhlSy91ZfXtRazkUNRKA6J"))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(Schedulers.newThread())      // subscriber on different thread
                    .subscribe(new Subscriber<YelpFusionApi>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            getActivity().runOnUiThread(() -> {

                                DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                            });
                        }

                        @Override
                        public void onNext(YelpFusionApi yelpAPI) {
                            Map<String, String> params = new HashMap<>();

                            if (!TextUtils.isEmpty(term))
                                params.put("term", term);


                            params.put("lang", "en");
                            params.put("oauth_signature_method", "hmac-sha1");

                            if (selected.size() > 0) {
                                String categories = "";

                                for (Category category : selected) {
                                    categories += category.getAlias() + ",";
                                }

                                params.put("categories", categories.substring(0, categories.length() - 1));
                            }

                            if (selectedMoney.size() > 0) {
                                StringBuilder money = new StringBuilder();

                                for (Integer s : selectedMoney) {
                                    money.append(s).append(",");
                                }

                                params.put("price", money.substring(0, money.length() - 1));
                            }
                            params.put("sort_by", "distance");

                            params.put("latitude", Double.toString(latLng.latitude));
                            params.put("longitude", Double.toString(latLng.longitude));

                            Call<SearchResponse> call = yelpAPI.getBusinessSearch(params);

                            call.enqueue(new Callback<SearchResponse>() {
                                @Override
                                public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                                    SearchResponse searchResponse = response.body();

                                    if (searchResponse.getBusinesses() != null && searchResponse.getBusinesses().size() > 0) {
                                        if (listener != null && listener.get() != null)
                                            listener.get().onLoaded(searchResponse.getBusinesses());
                                    } else {
                                        DialogUtils.showAlertDialog(getContext(), R.string.noDataForThisCity);

                                        if (listener != null && listener.get() != null)
                                            listener.get().onLoaded(new ArrayList<>());
                                    }
                                }

                                @Override
                                public void onFailure(Call<SearchResponse> call, Throwable t) {

                                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                                }
                            });
                        }
                    });

        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

}
