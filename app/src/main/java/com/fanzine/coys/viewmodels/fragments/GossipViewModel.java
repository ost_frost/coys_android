package com.fanzine.coys.viewmodels.fragments;

import android.content.Context;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.Gossip;
import com.fanzine.coys.repository.GossipRepo;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;
import com.google.common.collect.Ordering;

import java.util.Collections;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.utils.LoadingStates.*;

/**
 * Created by Woland on 20.03.2017.
 */

public class GossipViewModel extends BaseStateViewModel<Gossip> {

    private final static Ordering<Gossip> ORDERING = Ordering.natural().onResultOf(Gossip::getSort);

    public GossipViewModel(Context context, DataListLoadingListener<Gossip> listener) {
        super(context, listener);
    }

    @Override
    public void loadData(int page) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            Subscriber<List<Gossip>> subscriber = new Subscriber<List<Gossip>>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                    setState(ERROR);
                }

                @Override
                public void onNext(List<Gossip> gossips) {
                    if (getListener() != null) {
                        Collections.sort(gossips, ORDERING);

                        for (int i = 0; i < gossips.size(); i++)
                            gossips.get(i).setPosition(i);

                        getListener().onLoaded(gossips);
                    }

                    setState(DONE);
                }
            };
            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getGossips()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            loadFromDB();
        }
    }

    private void saveToRepo(Gossip data) {
        subscription.add(GossipRepo.getInstance().saveGossip(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {

                    }
                }));
    }

    private void loadFromDB() {
        Subscriber<Gossip> subscriber = new Subscriber<Gossip>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);

                setState(ERROR);
            }

            @Override
            public void onNext(Gossip gossip) {
                if (gossip == null) {
                    DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
                } else {
                    if (getListener() != null)
                        getListener().onLoaded(gossip);

                    setState(DONE);
                }
            }
        };

        subscription.add(subscriber);
        Observable<Gossip> observable = GossipRepo.getInstance().getGossip();
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}
