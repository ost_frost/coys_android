package com.fanzine.coys.viewmodels.items.teaser;

import android.content.Context;
import android.databinding.ObservableField;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.widget.ImageView;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.coys.models.ProEmail;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by mbp on 4/5/18.
 */

public class ItemEmailViewModel extends BaseViewModel {

    public ObservableField<ProEmail> proEmail = new ObservableField<>();

    public ItemEmailViewModel(Context context, ProEmail proEmail) {
        super(context);
        this.proEmail.set(proEmail);
    }


    @android.databinding.BindingAdapter(value = {"titleColor"})
    public static void setColor(TextView tv, ProEmail email) {
        if (!email.isTaken()) {
            tv.setTextColor(ContextCompat.getColor(tv.getContext(), R.color.black));
        } else  {
           tv.setTextColor(ContextCompat.getColor(tv.getContext(), R.color.black));
        }
    }

    @android.databinding.BindingAdapter(value = {"email"})
    public static void setIcon(AppCompatImageView ivc, ProEmail email) {
        if (!email.isTaken()) {
            ivc.setScaleType(ImageView.ScaleType.FIT_CENTER);
        } else  {
            ivc.setScaleType(ImageView.ScaleType.FIT_CENTER);
            ivc.setImageResource(R.drawable.ic_email_taken);
        }
    }
}
