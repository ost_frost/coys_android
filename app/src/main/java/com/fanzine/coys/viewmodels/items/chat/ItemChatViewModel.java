package com.fanzine.coys.viewmodels.items.chat;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableLong;

import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 13.04.2017.
 */

public class ItemChatViewModel extends BaseViewModel {

    public ObservableField<String> title = new ObservableField<>();
    public ObservableField<String> message = new ObservableField<>();
    public ObservableLong time = new ObservableLong();
    public ObservableBoolean isBanned = new ObservableBoolean();

    public ItemChatViewModel(Context context) {
        super(context);
    }

}
