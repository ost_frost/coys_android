package com.fanzine.coys.viewmodels.fragments.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableField;
import android.text.TextUtils;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.fragments.start.LoginFragment;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;
import com.google.firebase.crash.FirebaseCrash;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by maximdrobonoh on 22.08.17.
 */

public class ResetPasswordViewModel extends BaseViewModel {

    public ObservableField<String> newPassword = new ObservableField<>();
    public ObservableField<String> confirmNewPassword = new ObservableField<>();


    private String phoneCode;
    private String phone;

    public ResetPasswordViewModel(Context context, String phonecode, String phone) {
        super(context);

        this.phoneCode = phonecode;
        this.phone = phone;
    }

    public void onReset() {
        try {


            //validate all fields
            if (TextUtils.isEmpty(newPassword.get()) || TextUtils.isEmpty(confirmNewPassword.get())) {
                DialogUtils.showAlertDialog(getContext(), R.string.fieldsRequired);
                return;
            }

            //validate password confirm
            if (!newPassword.get().equals(confirmNewPassword.get())) {
                DialogUtils.showAlertDialog(getContext(), R.string.password_not_match);
                return;
            }

            if (NetworkUtils.isNetworkAvailable(getContext())) {
                ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

                Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        pd.dismiss();

                        List<FieldError> errors = ErrorResponseParser.getErrorList(e);
                        if (errors.isEmpty()) {
                            DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                        } else {
                            DialogUtils.showAlertDialog(getContext(), errors.get(0).getMessage());
                        }
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        pd.dismiss();

                        FragmentUtils.changeFragment(getActivity(), R.id.content_frame, LoginFragment.newInstance(), true);
                    }
                };

                subscription.add(subscriber);

                Map<String, String> map = new HashMap<>();
                map.put("phonecode", phoneCode);
                map.put("phone", phone);
                map.put("password", newPassword.get());

                ApiRequest.getInstance().getApi().saveNewPassword(map)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(subscriber);
            } else {
                DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
            }
        } catch (Exception e) {
            FirebaseCrash.log(e.getMessage());
            FirebaseCrash.report(e);
        }
    }
}
