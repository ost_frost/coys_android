package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableInt;

import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 10.01.2017.
 */

public class NavigationItemViewModel extends BaseViewModel {

    public ObservableInt name = new ObservableInt();
    public ObservableBoolean isSelected = new ObservableBoolean();
    public ObservableInt icon = new ObservableInt();

    public NavigationItemViewModel(Context context) {
        super(context);
    }

}
