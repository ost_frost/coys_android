package com.fanzine.coys.viewmodels.base;

import android.content.Context;
import android.databinding.ObservableInt;

import com.fanzine.coys.interfaces.DataListLoadingListener;

import java.lang.ref.WeakReference;

import static com.fanzine.coys.utils.LoadingStates.*;

/**
 * Created by Woland on 24.01.2017.
 */

public abstract class BaseStateViewModel<T> extends BaseViewModel {

    public ObservableInt state = new ObservableInt(LOADING);
    private WeakReference<DataListLoadingListener<T>> listener;

    public BaseStateViewModel(Context context, DataListLoadingListener<T> listener) {
        super(context);
        if (listener != null)
            this.listener = new WeakReference<>(listener);
    }

    public void setState(int state) {
        this.state.set(state);
    }

    public int getStateValue() {
        return state.get();
    }

    public abstract void loadData(int page);

    public void onLoad() {
        loadData(0);
        setState(LOADING);
    }

    protected DataListLoadingListener<T> getListener() {
        if (listener != null)
            return listener.get();
        else
            return null;
    }

}
