package com.fanzine.coys.viewmodels.fragments.chat;

import android.content.Context;

import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;
import com.fanzine.chat.ChatSDK;
import com.fanzine.chat.interfaces.FCChannelsListener;
import com.fanzine.chat.models.channels.FCChannel;

import java.util.List;

import static com.fanzine.coys.utils.LoadingStates.*;

/**
 * Created by Woland on 12.04.2017.
 */

public class ListChatViewModel extends BaseStateViewModel<FCChannel> {

    public ListChatViewModel(Context context, DataListLoadingListener<FCChannel> listener) {
        super(context, listener);
    }

    @Override
    public void loadData(int page) {
        ChatSDK.getInstance().getChatManager().getChannels(new FCChannelsListener() {
            @Override
            public void onChannelsReceived(List<FCChannel> list) {
                if (getListener() != null)
                    getListener().onLoaded(list);

                setState(DONE);
            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();

                setState(ERROR);
            }
        });
    }
}
