package com.fanzine.coys.viewmodels.items.team;

import android.content.Context;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.fanzine.coys.models.team.Player;
import com.fanzine.coys.models.team.PlayerStat;
import com.fanzine.coys.models.team.TeamSquad;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

import java.util.HashMap;

/**
 * Created by mbp on 1/9/18.
 */

public class ItemPlayerViewModel extends BaseViewModel {

    @NonNull
    public ObservableField<Player> player = new ObservableField<>();
    @NonNull
    public HashMap<PlayerStat.StatName, PlayerStat> statMap = new HashMap<>();

    @NonNull
    private TeamSquad squad;

    private static String EMPTY_VALUE = "0";
    private static String NA = "n/a";

    public ItemPlayerViewModel(Context context, Player player, TeamSquad squad) {
        super(context);
        this.squad = squad;
        Log.d("ItemPlayerViewModel", "squad " + squad + " player " + player.getName());
        EMPTY_VALUE = (squad == TeamSquad.ACADEMY || squad == TeamSquad.LADIES) ? NA : EMPTY_VALUE;
        for (PlayerStat s : player.statisticList) {
            Log.d("ItemPlayerViewModel", "squad " + squad + " PlayerStat " + s);
            statMap.put(s.getStatName(), s);
        }
        this.player.set(player);
    }

    @NonNull
    public String number() {
        return String.valueOf(player.get().getNumber());
    }

    @NonNull
    public String goals() {
        final PlayerStat goals = getStat(PlayerStat.StatName.Goals);
        if (goals != null) {
            return goals.getValue();
        }
        return EMPTY_VALUE;
    }

    @NonNull
    public String assists() {
        if (squad == TeamSquad.ACADEMY || squad == TeamSquad.LADIES) return NA;
        final PlayerStat assists = getStat(PlayerStat.StatName.Assists);
        if (assists != null) {
            return assists.getValue();
        }
        return EMPTY_VALUE;
    }

    @NonNull
    public String appearances() {
        if (squad == TeamSquad.ACADEMY || squad == TeamSquad.LADIES) return NA;
        final PlayerStat appearances = getStat(PlayerStat.StatName.Appearances);
        if (appearances != null) {
            return appearances.getValue();
        }
        return EMPTY_VALUE;
    }

    @NonNull
    public String minute() {
        if (squad == TeamSquad.ACADEMY || squad == TeamSquad.LADIES) return NA;
        final PlayerStat minute = getStat(PlayerStat.StatName.MinutesPlayed);
        if (minute != null) {
            return minute.getValue();
        }
        return EMPTY_VALUE;
    }

    @Nullable
    private PlayerStat getStat(@NonNull PlayerStat.StatName statName) {
        if (statMap.containsKey(statName)) {
            return statMap.get(statName);
        }
        return null;
    }
}
