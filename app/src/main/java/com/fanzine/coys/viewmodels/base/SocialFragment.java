package com.fanzine.coys.viewmodels.base;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.MainActivity;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.Error;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.fragments.start.SelectPlanFragment;
import com.fanzine.coys.models.login.UserToken;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;

import java.util.Arrays;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Woland on 02.08.2016.
 */
public abstract class SocialFragment extends BaseFragment {

    private CallbackManager callbackManager;
    private CompositeSubscription subscription = new CompositeSubscription();

    public void onFacebookLoginSuccess(LoginResult loginResult) {
        fetchFacebookProfile(loginResult);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        onFacebookLoginSuccess(loginResult);
                    }

                    @Override
                    public void onCancel() {
                        Log.e("dd", "facebook login canceled");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.e("dd", exception.getMessage());
                    }
                });
    }

    protected void fetchFacebookProfile(final LoginResult loginResult) {
        final ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                (object, response) -> {
                    String country = "", name = "", id = "", gender = "";
                    try {
                        id = object.getString("id");
                        name = object.getString("name");
                        if (!object.isNull("location")) {
                            String[] tmp = object.getJSONObject("location").getString("name").split(" ", -1);
                            if (tmp.length != 0)
                                country = tmp[tmp.length - 1];
                        }
                        if (!object.isNull("gender")) {
                            gender = object.getString("gender");
                        }

//                        picture = "https://graph.facebook.com/" + id + "/picture?type=large";
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    facebookLoggedIn(pd, loginResult.getAccessToken().getToken(), id, name, country, gender);
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,location,gender");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {

            if (callbackManager != null)
                callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void onFbLoginClicked() {
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_location"));
    }

    public abstract void facebookLoggedIn(ProgressDialog pd, String token, String id, String name, String country, String gender);

    public void checkUserData(ProgressDialog pd, String token, String id, String name, String country, String gender) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            Subscriber<UserToken> subscriber = new Subscriber<UserToken>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    Error error = ErrorResponseParser.getError(e);
                    if (TextUtils.isEmpty(error.getError())) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        if (error.getError().equals("credentials incorrect"))
                            FragmentUtils.changeFragment(getActivity(), R.id.content_frame, SelectPlanFragment.newInstance(id, name, country, gender), true);
                        else
                            DialogUtils.showAlertDialog(getContext(), error.getError());
                    }
                }

                @Override
                public void onNext(UserToken token) {
                    pd.dismiss();

                    if (!TextUtils.isEmpty(token.getError())) {
                        if (token.getError().equals("credentials incorrect"))
                            FragmentUtils.changeFragment(getActivity(), R.id.content_frame, SelectPlanFragment.newInstance(id, name, country, gender), true);
                        else
                            FragmentUtils.changeFragment(getActivity(), R.id.content_frame, SelectPlanFragment.newInstance(id, name, country, gender), true);
                    } else {
                        SharedPrefs.saveFIRToken(token.getToken());

                        getContext().startActivity(MainActivity.getStartIntent(getContext()));
                        getActivity().finish();
                    }
                }
            };
            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().fbLogIn(token, id)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    @Override
    public void onDestroy() {
        if (subscription != null)
            subscription.unsubscribe();

        super.onDestroy();
    }
}
