package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.databinding.ObservableField;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.text.Html;
import android.text.Spanned;
import android.widget.ImageView;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.coys.models.News;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 11.01.2017.
 */

public class ItemNewsViewModel extends BaseViewModel {

    public ObservableField<News> news = new ObservableField<>();
    public ObservableField<String> likesCount = new ObservableField<>();

    public ItemNewsViewModel(Context context, News news) {
        super(context);
        this.news.set(news);
        this.likesCount.set(String.valueOf(news.getLikesCount()));
    }

    public String getTitle(News news) {
        if (news.isPinned()) {
            return getString(R.string.item_news_breaking_news);
        }
        return news.getDatetimeStringTime();
    }

    public String getContent(News news) {
        return news.getTitle();
    }

    public Spanned getContent(String content) {
        return Html.fromHtml(content);
    }

    @android.databinding.BindingAdapter("android:textAllCaps")
    public static void textAllCaps(TextView v, News news) {
        if (news.isPinned())
            v.setAllCaps(true);
        else
            v.setAllCaps(false);
    }

    @android.databinding.BindingAdapter("android:textColor")
    public static void textColor(TextView v, News news) {
        if (news.isPinned())
            v.setTextColor(ContextCompat.getColor(v.getContext(), R.color.black));
        else
            v.setTextColor(ContextCompat.getColor(v.getContext(), R.color.white));
    }

    @android.databinding.BindingAdapter(value = {"news", "iconType"})
    public static void setIcon(AppCompatImageView ivc, News news, String type) {
        if (type.equals("like")) {
            if (news.isPinned() && news.isLiked) {
                ivc.setImageResource(R.drawable.news_like_black);
                ivc.setScaleType(ImageView.ScaleType.FIT_CENTER);
            } else if (news.isPinned()) {
                ivc.setScaleType(ImageView.ScaleType.CENTER_CROP);
                ivc.setImageResource(R.drawable.like_black);
            } else if (news.isLiked) {
                ivc.setImageResource(R.drawable.news_like);
                ivc.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }
            else{
                ivc.setImageResource(R.drawable.ic_icon_likewhite);
                ivc.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
        } else if (type.equals("share")) {
            if (news.isPinned())
                ivc.setImageResource(R.drawable.news_share_black);
            else
                ivc.setImageResource(R.drawable.news_share);
        }
    }
}
