package com.fanzine.coys.viewmodels.activities;

import android.content.Context;
import android.databinding.ObservableField;
import android.util.Log;

import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.models.Fullname;
import com.fanzine.coys.models.ProEmail;
import com.fanzine.coys.viewmodels.base.BaseViewModel;
import com.google.firebase.crash.FirebaseCrash;

import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

/**
 * Created by mbp on 4/5/18.
 */

public class TeaserViewModel extends BaseViewModel {

    public ObservableField<Boolean> isRequestPending = new ObservableField<>();

    private PublishSubject<Fullname> subject;

    private boolean isUpgrade;

    public interface EmailLoadListener {
        void showEmails(List<ProEmail> emails);
    }

    private EmailLoadListener mEmailLoadListener;

    public TeaserViewModel(Context context, EmailLoadListener emailLoadListener) {
        super(context);
        mEmailLoadListener = emailLoadListener;

        subject = PublishSubject.create();

        subject.debounce(300, TimeUnit.MILLISECONDS)
                .switchMap(this::emailFromApi)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<ProEmail>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        isRequestPending.set(false);

                        Log.i(getClass().getName(), e.getMessage());
                        FirebaseCrash.report(e);
                    }

                    @Override
                    public void onNext(List<ProEmail> emails) {
                        isRequestPending.set(false);

                        mEmailLoadListener.showEmails(emails);
                    }
                });
    }

    public void submit() {
        isRequestPending.set(true);
        subject.onCompleted();
    }

    public void autocomplete(Fullname fullname) {
        isRequestPending.set(true);
        subject.onNext(fullname);
    }

    public void load(Fullname fullname) {
        isRequestPending.set(true);
        ApiRequest.getInstance().getApi().getNames(fullname.getFirstName(), fullname.getLastName())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<ProEmail>>() {
                    @Override
                    public void onCompleted() {
                        isRequestPending.set(false);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<ProEmail> emails) {
                        mEmailLoadListener.showEmails(emails);
                    }
                });
    }

    private Observable<List<ProEmail>> emailFromApi(Fullname fullname) {
        return ApiRequest.getInstance().getApi().getNames(fullname.getFirstName(), fullname.getLastName())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
