package com.fanzine.coys.viewmodels.fragments.match;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.util.Log;

import com.fanzine.coys.api.model.MatchModel;
import com.fanzine.coys.fragments.base.BaseTopCalendarFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.LigueMatches;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.utils.LoadingStates.DONE;
import static com.fanzine.coys.utils.LoadingStates.LOADING;

/**
 * Created by Siarhei on 16.01.2017.
 */

public class MatchesFragmentViewModel extends BaseStateViewModel<LigueMatches> {

    public ObservableBoolean matchSelected = new ObservableBoolean(false);

    public MatchesFragmentViewModel(Context context, DataListLoadingListener<LigueMatches> listener) {
        super(context, listener);
    }

    public interface OnUpdateListener {
        void update(List<LigueMatches> ligueMatches);
    }

    private Subscription receivesMatchesSubscription;

    public ObservableField<String> mSelectedDate = new ObservableField<>();

    @Override
    public void loadData(int page) {
        setState(LOADING);

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date selectedDate = BaseTopCalendarFragment.getSelectedDate();

            mSelectedDate.set(dateFormat.format(selectedDate));

            subscription.add(getLigueMatchesList(dateFormat.format(selectedDate)).subscribe(ligueMatches -> {
                Collections.sort(ligueMatches, (ligueMatches1, t1) -> ligueMatches1.getSort() - t1.getSort());

                getListener().onLoaded(ligueMatches);
            }, throwable -> throwable.printStackTrace()));
        } catch (Exception e) {
            e.printStackTrace();

            setState(DONE);
        }
    }

    public void update(OnUpdateListener listener) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date selectedDate = BaseTopCalendarFragment.getSelectedDate();

            mSelectedDate.set(dateFormat.format(selectedDate));

            subscription.add(getLigueMatchesList(dateFormat.format(selectedDate)).subscribe(ligueMatches -> {
                Collections.sort(ligueMatches, (ligueMatches1, t1) -> ligueMatches1.getSort() - t1.getSort());

                listener.update(ligueMatches);
            }, throwable -> throwable.printStackTrace()));
        } catch (Exception e) {
            e.printStackTrace();

            setState(DONE);
        }
    }

    public void stopReceiveMatches() {
        if ( receivesMatchesSubscription != null) {
            Log.i(getClass().getName(), "STOP receive matches every minute on today");
            subscription.remove(receivesMatchesSubscription);
        }
    }

    public void startIntervalUpdating() {
        Log.i(getClass().getName(), "START receive matches every minute on today");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = BaseTopCalendarFragment.getSelectedDate();

        receivesMatchesSubscription =
                receiveMatchesListEveryMinute(dateFormat.format(selectedDate)).subscribe(ligueMatches -> {
                    Log.i(getClass().getName(), "GOT  matches from updater");

                    Collections.sort(ligueMatches, (ligueMatches1, t1) -> ligueMatches1.getSort() - t1.getSort());
                    getListener().onLoaded(ligueMatches);
                });
        subscription.add(receivesMatchesSubscription);
    }
    private Observable<List<LigueMatches>> getLigueMatchesList(String date) {
        Observable<List<LigueMatches>> liguesMatchesApi = new MatchModel().getMatch(date)
                .map(ligueMatches -> ligueMatches);
        return liguesMatchesApi
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observable<List<LigueMatches>> receiveMatchesListEveryMinute(String date) {
        Observable<List<LigueMatches>> liguesMatchesApi = new MatchModel().getMatch(date)
                .map(ligueMatches -> ligueMatches);

        return Observable.interval(1, TimeUnit.MINUTES)
                .flatMap(aLong -> liguesMatchesApi)
                .subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());
    }
}
