package com.fanzine.coys.viewmodels.items.match;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.models.analysis.RecentGames;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by maximdrobonoh on 06.10.17.
 */

public class ItemAnalysisRecentGame  extends BaseViewModel {

    public ObservableField<RecentGames.Item> item = new ObservableField<>();

    public ItemAnalysisRecentGame(Context context, RecentGames.Item item) {
        super(context);
        this.item.set(item);
    }

    public ObservableField<RecentGames.Item> getRecentGames() {
        return item;
    }

    public void setRecentGames(RecentGames.Item item) {
        this.item.set(item);
    }

    public String getResult() {
        return item.get().getResult();
    }
}
