package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Siarhei on 13.01.2017.
 */

public class ItemMailCategoryViewModel extends BaseViewModel {

    public ObservableField<String> nameCategory = new ObservableField<>();

    public ItemMailCategoryViewModel(Context context, String name) {
        super(context);
        this.nameCategory.set(name);
    }

    public void setNameCategory(String name) {
        this.nameCategory.set(name);
    }
}
