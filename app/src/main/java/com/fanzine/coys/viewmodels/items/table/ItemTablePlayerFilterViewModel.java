package com.fanzine.coys.viewmodels.items.table;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.models.table.PlayerStatsFilter;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by maximdrobonoh on 29.09.17.
 */

public class ItemTablePlayerFilterViewModel extends BaseViewModel {

    public ObservableField<PlayerStatsFilter> filter = new ObservableField<>();

    public ItemTablePlayerFilterViewModel(Context context, PlayerStatsFilter filter) {
        super(context);

        this.filter.set(filter);
    }

    public ObservableField<PlayerStatsFilter> getFilter() {
        return filter;
    }
}
