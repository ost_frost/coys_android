package com.fanzine.coys.viewmodels.items.team;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.models.team.Player;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 22.02.2017.
 */

public class ItemPlayerHorizontalViewModel extends BaseViewModel {

    public ObservableField<Player> player = new ObservableField<>();

    public ItemPlayerHorizontalViewModel(Context context) {
        super(context);
    }

}
