package com.fanzine.coys.viewmodels.fragments.news;

import android.content.Context;
import android.databinding.ObservableField;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.fanzine.coys.R;
import com.fanzine.coys.fragments.news.NewsFragment;
import com.fanzine.coys.fragments.news.SocialFragment;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.viewmodels.base.BaseLeaguesBarViewModel;

import java.lang.ref.WeakReference;

/**
 * Created by user on 07.11.2017.
 */

public class FeedViewModel extends BaseLeaguesBarViewModel {

    private WeakReference<FragmentManager> fragmentManager;

    private ObservableField<Integer> leagueId = new ObservableField<>();

    private FeedType currentFeedType;

    public FeedViewModel(Context context, FragmentManager fragmentManager,
                         LeagueLoadingListener loadingListener) {
        super(context, loadingListener);
        this.fragmentManager = new WeakReference<>(fragmentManager);
    }

    public void openNews() {
        currentFeedType = FeedType.NEWS;
        changeContent(NewsFragment.newInstance(leagueId.get()));
    }

    public void openSocial() {
        currentFeedType = FeedType.SOCIAL;
        changeContent(SocialFragment.newInstance(leagueId.get()));
    }

    public void update() {
        if (currentFeedType == FeedType.NEWS)
            openNews();
        else if ( currentFeedType == FeedType.SOCIAL)
            openSocial();

    }

    private void changeContent(Fragment fragment) {
        Fragment currentFragment = FragmentUtils.getCurrent(getActivity());
        if (currentFragment != null & currentFragment.getClass() != fragment.getClass()) {
            FragmentUtils.changeFragment(fragmentManager.get(), R.id.parent, fragment, false);
        }
    }


    public Integer getLeagueId() {
        return leagueId.get();
    }

    public void setLeagueId(Integer leagueId) {
        this.leagueId.set(leagueId);
    }
}
