package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.widget.ImageView;

import com.fanzine.coys.R;
import com.fanzine.coys.models.SocialMedia;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by mbp on 3/23/18.
 */

public class SocialItemViewModel extends BaseViewModel {
    public SocialItemViewModel(Context context) {
        super(context);
    }

    @android.databinding.BindingAdapter(value = {"social", "iconType"})
    public static void setIcon(AppCompatImageView ivc, SocialMedia news, String type) {
        if (type.equals("like")) {
            if (news.isPinned() && news.isLiked()) {
                ivc.setImageResource(R.drawable.news_like_black);
                ivc.setScaleType(ImageView.ScaleType.FIT_CENTER);
            } else if (news.isPinned()) {
                ivc.setScaleType(ImageView.ScaleType.CENTER_CROP);
                ivc.setImageResource(R.drawable.like_black);
            } else if (news.isLiked()) {
                ivc.setImageResource(R.drawable.news_like);
                ivc.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }
            else{
                ivc.setImageResource(R.drawable.ic_like_white);
                ivc.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
        } else if (type.equals("share")) {
            if (news.isPinned())
                ivc.setImageResource(R.drawable.news_share_black);
            else
                ivc.setImageResource(R.drawable.news_share);
        }
    }
}
