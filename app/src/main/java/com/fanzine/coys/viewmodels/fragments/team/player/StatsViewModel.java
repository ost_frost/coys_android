package com.fanzine.coys.viewmodels.fragments.team.player;

import android.content.Context;

import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.models.team.Player;
import com.fanzine.coys.models.team.PlayerStat;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

import java.util.Collections;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by mbp on 1/11/18.
 */

public class StatsViewModel extends BaseViewModel {

    public interface OnStatsLoadListener {
        void onSuccess(List<PlayerStat> playerStats);
    }

    private OnStatsLoadListener onStatsLoadListener;

    public StatsViewModel(Context context, OnStatsLoadListener onStatsLoadListener) {
        super(context);
        this.onStatsLoadListener = onStatsLoadListener;
    }

    public void load(Player player) {
        ApiRequest.getInstance().getApi().getTeamPlayerStats(player.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<PlayerStat>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<PlayerStat> playerStats) {
                        //order playerStats
                        Collections.sort(playerStats);
                        onStatsLoadListener.onSuccess(playerStats);
                    }
                });
    }
}
