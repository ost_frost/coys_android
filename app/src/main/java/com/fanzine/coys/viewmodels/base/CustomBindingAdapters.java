package com.fanzine.coys.viewmodels.base;

import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.widget.ImageView;

import com.fanzine.coys.R;
import com.fanzine.coys.utils.transform.CircleTransform;
import com.fanzine.coys.utils.transform.RoundedTransformationBuilder;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

/**
 * Created by Woland on 19.01.2017.
 */

public class CustomBindingAdapters {

    @BindingAdapter({"newsImageUrlFull"})
    public static void loadNewsImageFull(ImageView view, String link) {
        Picasso.with(view.getContext())
                .load(link)
                .transform(new RoundedTransformationBuilder().cornerRadiusDp(10, true).build())
                .fit()
                .centerCrop()
                .into(view);
    }

    @BindingAdapter({"newsImageUrl"})
    public static void loadNewsImage(ImageView view, String link) {
        Picasso.with(view.getContext())
                .load(link)
                .transform(new RoundedTransformationBuilder().cornerRadiusDp(10, false).build())
                .fit()
                .centerCrop()
                .into(view);
    }

    @BindingAdapter({"roundedImageUrl"})
    public static void loadRoundedImage(ImageView view, String link) {
        if (!TextUtils.isEmpty(link)) {
            Picasso.with(view.getContext())
                    .load(link)
                    .transform(new CircleTransform())
                    .fit()
                    .centerCrop()
                    .into(view);
        }
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView view, String link) {
        if (!TextUtils.isEmpty(link))
            Picasso.with(view.getContext())
                    .load(link)
                    .fit()
                    .centerCrop()
                    .into(view);
    }

    @BindingAdapter({"imageUrlNotFit"})
    public static void loadImageNotFit(ImageView view, String link) {
        if (!TextUtils.isEmpty(link))
            Picasso.with(view.getContext())
                    .load(link)
                    .into(view);
    }

    @BindingAdapter({"imageInsideUrl"})
    public static void imageInsideUrl(ImageView view, String link) {
        if (!TextUtils.isEmpty(link))
            Picasso.with(view.getContext())
                    .load(link)
                    .fit()
                    .centerInside()
                    .into(view);
    }

    @BindingAdapter({"imageProfileUrl"})
    public static void loadProfileImage(ImageView imageView, String imageUrl) {
        Glide.with(imageView.getContext())
                .load(imageUrl)
                .centerCrop()
                .placeholder(R.drawable.ic_avatar_placeholder)
                .into(imageView);
    }

    @BindingAdapter({"iconUrl"})
    public static void loadIcon(ImageView imageView, String imageUrl) {
        Glide.with(imageView.getContext())
                .load(imageUrl)
                .fitCenter()
                .into(imageView);
    }
}
