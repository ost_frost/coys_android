package com.fanzine.coys.viewmodels.dialogs;

import android.content.Context;
import android.databinding.ObservableField;
import android.support.v7.app.AlertDialog;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.mails.MailCategoriesAdapter;
import com.fanzine.coys.networking.EmailDataRequestManager;
import com.fanzine.coys.subscribers.AddMailboxSubscriber;

import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by maximdrobonoh on 13.09.17.
 */

public class AddMailboxViewModel extends BaseViewModel {

    public ObservableField<String> folderName = new ObservableField<>();

    private EmailDataRequestManager emailDataRequestManager;
    private MailCategoriesAdapter adapter;

    public AddMailboxViewModel(Context context, MailCategoriesAdapter adapter) {
        super(context);

        this.emailDataRequestManager = EmailDataRequestManager.getInstanse();
        this.adapter = adapter;
    }

    public void addMailbox(AlertDialog alertDialog) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {

            AddMailboxSubscriber subscriber = new AddMailboxSubscriber(
                    getContext(),
                    adapter, folderName.get(),
                    alertDialog
            );

            emailDataRequestManager.addMailbox(folderName.get())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }
}
