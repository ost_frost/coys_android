package com.fanzine.coys.viewmodels.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.WindowManager;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.MainActivity;
import com.fanzine.coys.adapters.login.EmailsAdapter;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.databinding.DialogRecyclerviewBinding;
import com.fanzine.coys.models.ProEmail;
import com.fanzine.coys.models.Status;
import com.fanzine.coys.models.login.UserToken;
import com.fanzine.coys.models.payment.PaymentToken;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.utils.RecyclerItemClickListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mbp on 3/21/18.
 */

public class UpgradeViewModel extends BaseViewModel {

    public UpgradeViewModel(Context context) {
        super(context);
    }

    public void showBuyProDialog() {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);
            Subscriber<List<ProEmail>> subscriber = new Subscriber<List<ProEmail>>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(List<ProEmail> emails) {
                    pd.dismiss();

                    showDialogWithEmails(emails);
                }
            };
            subscription.add(subscriber);

            UserToken token = SharedPrefs.getUserToken();

            ApiRequest.getInstance().getApi().getNames(token.getFirstName(), token.getLastName())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    private void showDialogWithEmails(List<ProEmail> data) {
        DialogRecyclerviewBinding bindingDialog = DialogRecyclerviewBinding.inflate(LayoutInflater.from(getContext()));
        bindingDialog.rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        EmailsAdapter adapter = new EmailsAdapter(getContext(), data, true);
        bindingDialog.rv.setAdapter(adapter);
        bindingDialog.rv.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), adapter));
        bindingDialog.executePendingBindings();

        final AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setView(bindingDialog.getRoot())
                .setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.buy, (dialog1, which) -> getPaymentToken(data.get(adapter.getSelected()).getEmail()))
                .show();

        dialog.getWindow().setBackgroundDrawableResource(R.color.backgroundDialog);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    private void getPaymentToken(String email) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            Subscriber<PaymentToken> subscriber = new Subscriber<PaymentToken>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();
                    List<FieldError> errors = ErrorResponseParser.getErrorList(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.get(0).getMessage());
                    }
                }

                @Override
                public void onNext(PaymentToken paymentToken) {
                    pd.dismiss();

                    ((MainActivity) getActivity()).showPayment(paymentToken, email);
                }
            };

            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getPaymentToken()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    public void uypgradeToPro(String email, String nonce) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            Subscriber<Status> subscriber = new Subscriber<Status>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();
                    List<FieldError> errors = ErrorResponseParser.getErrorList(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.get(0).getMessage());
                    }
                }

                @Override
                public void onNext(Status status) {
                    if (!status.getStatus().equals("ok")) {
                        pd.dismiss();
                        DialogUtils.showAlertDialog(getContext(), getString(R.string.status) + ": " + status.getStatus());
                    }
                    else
                        DialogUtils.showAlertDialog(getContext(), R.string.nowPro);
                }
            };

            subscription.add(subscriber);

            Map<String, String> map = new HashMap<>();
            map.put("payment_nonce", nonce);
            map.put("email", email);

            ApiRequest.getInstance().getApi().upgradeToPro(map)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    protected void doWithNetwork(Runnable runnable) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            runnable.run();
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }
}
