package com.fanzine.coys.viewmodels.items.team;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 23.02.2017.
 */

public class PhotoViewModel extends BaseViewModel {

    public ObservableField<String> link = new ObservableField<>();
    public ObservableField<Boolean> isDrawLine = new ObservableField<>();
    public ObservableField<Boolean> isLast = new ObservableField<>();


    public PhotoViewModel(Context context, String url, boolean isDrawLine, boolean isLast) {
        super(context);
        this.link.set(url);
        this.isDrawLine.set(isDrawLine);
        this.isLast.set(isLast);
    }
}
