package com.fanzine.coys.viewmodels.fragments.news;

import android.content.Context;

import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.Social;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.utils.LoadingStates.DONE;
import static com.fanzine.coys.utils.LoadingStates.ERROR;
import static com.fanzine.coys.utils.LoadingStates.LOADING;

/**
 * Created by mbp on 11/9/17.
 */

public class SocialFeedViewModel extends BaseStateViewModel<Social> {

    private static final int LIMIT = 15;

    public SocialFeedViewModel(Context context, DataListLoadingListener<Social> listener) {
        super(context, listener);
    }

    @Override
    public void loadData(int page) {
        setState(LOADING);
        Subscriber<List<Social>> subscriber = new Subscriber<List<Social>>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                if (page == 0)
                    setState(ERROR);

                getListener().onError();
            }

            @Override
            public void onNext(List<Social> list) {
                if (list.size() > 0) {
                    getListener().onLoaded(list);

                    setState(DONE);
                } else {
                    if (page == 0)
                        setState(ERROR);

                    getListener().onError();
                }
            }
        };
        subscription.add(subscriber);

        ApiRequest.getInstance().getApi().getSocialFeed(page, LIMIT)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}
