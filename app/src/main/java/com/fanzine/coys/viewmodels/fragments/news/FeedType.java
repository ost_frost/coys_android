package com.fanzine.coys.viewmodels.fragments.news;

/**
 * Created by mbp on 11/9/17.
 */

public enum FeedType {
    SOCIAL, NEWS
}
