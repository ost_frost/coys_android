package com.fanzine.coys.viewmodels.fragments.mails;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.R;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.fragments.mails.ListLabelFragment;
import com.fanzine.coys.models.mails.Label;
import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.networking.EmailDataRequestManager;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

import java.util.List;

import rx.Subscriber;


/**
 * Created by maximdrobonoh on 15.09.17.
 */

public class NewLabelViewModel extends BaseViewModel {

    public ObservableField<String> labelName = new ObservableField<>();

    public NewLabelViewModel(Context context) {
        super(context);
    }


    public void createLabel(int colorId, Mail mail) {

        ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

        Subscriber<List<Label>> subscriber = new Subscriber<List<Label>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                pd.dismiss();

                FieldError errors = ErrorResponseParser.getErrors(e);

                if (errors.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                } else {
                    DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                }
            }

            @Override
            public void onNext(List<Label> labels) {
                pd.dismiss();

                FragmentUtils.changeFragment(getActivity(), R.id.content_frame,
                        ListLabelFragment.newInstance(labels, mail), false);
            }
        };

        EmailDataRequestManager.getInstanse()
                .createEmailLabelForUser(labelName.get(), colorId)
                .subscribe(subscriber);
    }
}
