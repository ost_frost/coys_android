package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.models.Contact;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Siarhei on 31.01.2017.
 */

public class ItemContactViewModel extends BaseViewModel {

    public ObservableField<Contact> contact = new ObservableField<>();

    public ItemContactViewModel(Context context, Contact contact) {
        super(context);
        this.contact.set(contact);
    }

    public void setContact(Contact contact) {
        this.contact.set(contact);
    }
}
