package com.fanzine.coys.viewmodels.fragments.table;

import android.content.Context;

import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.table.Filter;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;

import java.util.ArrayList;

/**
 * Created by maximdrobonoh on 29.09.17.
 */

public class FilterViewModel extends BaseStateViewModel<Filter> {


    public FilterViewModel(Context context, DataListLoadingListener<Filter> listener) {
        super(context, listener);
    }

    @Override
    public void loadData(int page) {
        getListener().onLoaded(new ArrayList<Filter>());
    }
}
