package com.fanzine.coys.viewmodels.base;

import android.content.Context;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.api.response.LeagueList;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.League;
import com.fanzine.coys.repository.LeaguesRepo;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;

import java.lang.ref.WeakReference;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.utils.LoadingStates.ERROR;

/**
 * Created by Woland on 13.02.2017.
 */

public abstract class BaseTopPanelViewModel<T> extends BaseStateViewModel<T> {

    private boolean leaguesLoaded = false;

    public void setLeaguesLoaded(boolean leagueLoaded) {
        this.leaguesLoaded = leagueLoaded;
    }

    public interface LeagueLoadingListener {
        void onLeagueLoaded(List<League> list);
    }

    private WeakReference<LeagueLoadingListener> leagueLoadingListener;

    public BaseTopPanelViewModel(Context context, LeagueLoadingListener leagueListener, DataListLoadingListener<T> listener) {
        super(context, listener);

        leagueLoadingListener = new WeakReference<>(leagueListener);
    }

    public void loadLeagues() {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            Subscriber<LeagueList> subscriber = new Subscriber<LeagueList>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (!errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }

                    setState(ERROR);

                }

                @Override
                public void onNext(LeagueList list) {
                    leaguesLoaded = true;

                    if (leagueLoadingListener != null && leagueLoadingListener.get() != null)
                        leagueLoadingListener.get().onLeagueLoaded(list.getLeagues());

                    saveToRepo(list.getLeagues());

                    startDataLoading(0);
                }
            };
            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getMajorLeagues()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        }
        else
            loadFromDB();
    }

    private void saveToRepo(List<League> data) {
        subscription.add(LeaguesRepo.getInstance().saveLeagues(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {

                    }
                }));
    }

    private void loadFromDB() {
        Subscriber<List<League>> subscriber = new Subscriber<List<League>>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                setState(ERROR);
                e.printStackTrace();
                DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
            }

            @Override
            public void onNext(List<League> data) {
                if (data == null || data.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);

                    setState(ERROR);
                } else {
                    if (leagueLoadingListener != null && leagueLoadingListener.get() != null)
                        leagueLoadingListener.get().onLeagueLoaded(data);

                    startDataLoading(0);

                    leaguesLoaded = true;

                }
            }
        };

        subscription.add(subscriber);
        Observable<List<League>> observable = LeaguesRepo.getInstance().getLeagues();
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    @Override
    public void loadData(int page) {
        if (leaguesLoaded)
            startDataLoading(page);
        else
            loadLeagues();
    }

    public abstract void startDataLoading(int page);
}
