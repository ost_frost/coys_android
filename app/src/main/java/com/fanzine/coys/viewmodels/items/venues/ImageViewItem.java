package com.fanzine.coys.viewmodels.items.venues;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 01.12.2016.
 */

public class ImageViewItem extends BaseViewModel {

    public ObservableField<String> image = new ObservableField<>();

    public ImageViewItem(Context context, String url) {
        super(context);
        this.image.set(url);
    }

}
