package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;

import com.fanzine.coys.models.Social;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by user on 07.11.2017.
 */

public class ItemSocialViewModel extends BaseViewModel {

    public ObservableField<Social> social = new ObservableField<>();

    public ItemSocialViewModel(Context context, Social social) {
        super(context);
        this.social.set(social);
    }

    @NonNull
    public String getCreatedAgo() {
        final Social s = social.get();
        if (s != null) {
            return s.getDateTime();
        }
        return "";
    }

}
