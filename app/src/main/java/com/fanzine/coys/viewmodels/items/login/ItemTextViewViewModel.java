package com.fanzine.coys.viewmodels.items.login;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 23.03.2017.
 */

public class ItemTextViewViewModel extends BaseViewModel {

    public ObservableField<String> text = new ObservableField<>();

    public ItemTextViewViewModel(Context context, String text) {
        super(context);

        this.text.set(text);
    }

}
