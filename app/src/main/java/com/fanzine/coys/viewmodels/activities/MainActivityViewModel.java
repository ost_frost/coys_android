package com.fanzine.coys.viewmodels.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.fragments.mails.MailHomeFragment;
import com.fanzine.coys.fragments.start.TeaserFragment;
import com.fanzine.coys.models.Fullname;
import com.fanzine.coys.models.PurchaseDto;
import com.fanzine.coys.models.Status;
import com.fanzine.coys.models.login.UserToken;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.UpgradeViewModel;
import com.google.firebase.crash.FirebaseCrash;

import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Woland on 24.02.2017.
 */

public class MainActivityViewModel extends UpgradeViewModel {

    public interface UpgradeListener {
        void startPurchasing();
    }

    private String mUpgradeToProEmail;

    public MainActivityViewModel(Context context) {
        super(context);
    }

    public void enableProAccount(PurchaseDto purchaseDto) {

        Map<String, String> params = new HashMap<>();
        params.put("email", mUpgradeToProEmail);

        ProgressDialog pd = DialogUtils.createProgressDialog(getContext(), "Processing..");

        pd.show();

        ApiRequest.getInstance().getApi().upgradeToPro(params)
                .flatMap(status -> sendPaymentData(purchaseDto))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        pd.dismiss();

                        e.printStackTrace();

                        DialogUtils.showErrorDialog(getContext(), "Payment error");

                        FirebaseCrash.report(e);
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        pd.dismiss();

                        openMails(MailHomeFragment.newInstance());
                    }
                });
    }


    public void upgrade(String email, UpgradeListener upgradeListener) {
        this.mUpgradeToProEmail = email;
        upgradeListener.startPurchasing();
    }

    public void openMails(Fragment fragment) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            Subscriber<Status> subscriber = new Subscriber<Status>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(Status status) {
                    pd.dismiss();

                    if (status.isPro())
                        FragmentUtils.changeFragment(getActivity(), R.id.content_frame, fragment, false);
                    else {
                        UserToken userToken = SharedPrefs.getUserToken();

                        if (userToken == null) {
                            DialogUtils.showAlertDialog(getContext(), "Please, try to re-login");
                            return;
                        }

                        Fullname fullname = new Fullname(userToken.getFirstName(), userToken.getLastName());

                        TeaserFragment teaserFragment = TeaserFragment.newInstance(fullname, true);

                        FragmentUtils.changeFragment(getActivity(), R.id.content_frame, teaserFragment, false);
                    }

                }
            };

            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getUserStatus()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    private Observable<ResponseBody> sendPaymentData(PurchaseDto purchaseDto) {
        Map<String, String> paymentData = new HashMap<>();

        paymentData.put("order_id", purchaseDto.orderId);
        paymentData.put("package_name", purchaseDto.packegeName);
        paymentData.put("signature", purchaseDto.signature);
        paymentData.put("state", String.valueOf(purchaseDto.state));
        paymentData.put("time", String.valueOf(purchaseDto.time));
        paymentData.put("token", purchaseDto.token);
        paymentData.put("email", mUpgradeToProEmail);
        paymentData.put("product_id",  purchaseDto.productId);

        return ApiRequest.getInstance().getApi().sendPaymentInfo(paymentData);
    }
}
