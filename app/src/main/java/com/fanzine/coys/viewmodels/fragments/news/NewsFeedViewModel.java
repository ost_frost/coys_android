package com.fanzine.coys.viewmodels.fragments.news;

import android.content.Context;

import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.News;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;
import com.google.common.collect.Ordering;

import java.util.Collections;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.utils.LoadingStates.DONE;
import static com.fanzine.coys.utils.LoadingStates.ERROR;
import static com.fanzine.coys.utils.LoadingStates.LOADING;

/**
 * Created by Woland on 19.01.2017.
 */

public class NewsFeedViewModel extends BaseStateViewModel<News> {

    private final static Ordering<News> ORDERING = Ordering.natural().onResultOf(News::getNewsId);

    private int leagueId;
    private static final int limit = 18;

    public NewsFeedViewModel(Context context, DataListLoadingListener<News> listener, int leagueId) {
        super(context, listener);
        this.leagueId = leagueId;
    }

    @Override
    public void loadData(int page) {
        setState(LOADING);
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            Subscriber<List<News>> subscriber = new Subscriber<List<News>>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    if (page == 1)
                        setState(ERROR);

                    getListener().onError();
                }

                @Override
                public void onNext(List<News> list) {
                    if (list.size() > 0) {
                        setState(DONE);

                        Collections.sort(list, (news, t1) -> {
                            int isPinnedCompare = Boolean.compare(news.isPinned(), t1.isPinned());

                            if (isPinnedCompare == -1)
                                return 1;

                            int compare = news.getDatetime().compareTo(t1.getDatetime());
                            compare *= -1;

                            return compare;
                        });

                        getListener().onLoaded(list);
                        setState(DONE);
                    } else {
                        if (page == 1)
                            setState(ERROR);

                        getListener().onError();
                    }
                }
            };
            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getNews(leagueId, page, limit)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        }
    }
}
