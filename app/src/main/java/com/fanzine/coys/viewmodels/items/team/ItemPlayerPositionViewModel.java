package com.fanzine.coys.viewmodels.items.team;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 23.02.2017.
 */

public class ItemPlayerPositionViewModel extends BaseViewModel {

    public ObservableField<String> number = new ObservableField<>();
    public ObservableField<String> name = new ObservableField<>();

    public ItemPlayerPositionViewModel(Context context, String number, String name) {
        super(context);
        this.name.set(name);
        this.number.set(number);
    }

    public void init(String number, String name) {
        this.name.set(name);
        this.number.set(number);
    }


}
