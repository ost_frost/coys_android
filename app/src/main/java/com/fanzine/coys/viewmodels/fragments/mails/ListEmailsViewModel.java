package com.fanzine.coys.viewmodels.fragments.mails;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableField;
import android.text.TextUtils;
import android.util.Log;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.db.factory.HelperFactory;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.mails.Color;
import com.fanzine.coys.models.mails.Label;
import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.models.mails.MailContent;
import com.fanzine.coys.networking.EmailDataRequestManager;
import com.fanzine.coys.repository.MailsRepo;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.utils.LoadingStates.DONE;
import static com.fanzine.coys.utils.LoadingStates.ERROR;
import static com.fanzine.coys.utils.LoadingStates.LOADING;
import static com.fanzine.coys.utils.LoadingStates.NO_DATA;

/**
 * Created by Siarhei on 20.01.2017.
 */

public class ListEmailsViewModel extends BaseStateViewModel<Mail> {

    public ObservableField<String> toolbarTitle = new ObservableField<>();

    private EmailDataRequestManager emailDataRequestManager;


    public ListEmailsViewModel(Context context, DataListLoadingListener<Mail> listener) {
        super(context, listener);

        this.emailDataRequestManager = EmailDataRequestManager.getInstanse();
    }

    public void loadData(int page) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            Subscriber<List<Mail>> subscriber = new Subscriber<List<Mail>>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    if (getStateValue() == LOADING)
                        setState(ERROR);

                    getListener().onError();
                    getListener().onLoaded(new Mail());
                }

                @Override
                public void onNext(List<Mail> list) {
                    Collections.sort(list, (o1, o2) -> o2.getDateTime().compareTo(o1.getDateTime()));

//                    saveToRepo(list);

                    setState(DONE);

                    List<Mail> data = new ArrayList<>();
                    for (Mail mail : list) {
                        data.add(mail);
                    }
                    if (data.isEmpty())
                        setState(NO_DATA);
                    else {
                        getListener().onLoaded(data);
                    }
                    getListener().onLoaded(new Mail());
                }
            };
            subscription.add(subscriber);

            emailDataRequestManager.getMailsByFolder("INBOX").subscribe(subscriber);
        } else {
            loadFromDB(null);
        }
    }

    public void loadEmailsFromApi(String folder) {
        Subscriber<List<Mail>> subscriber = new Subscriber<List<Mail>>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                if (getStateValue() == LOADING)
                    setState(ERROR);

                getListener().onError();

                getListener().onLoaded(new Mail());
            }

            @Override
            public void onNext(List<Mail> list) {
                Collections.sort(list, (o1, o2) -> o2.getDateTime().compareTo(o1.getDateTime()));

                saveToRepo(list);

                setState(DONE);

                List<Mail> data = new ArrayList<>();
                for (Mail mail : list) {
                    data.add(mail);
                }
                if (data.isEmpty())
                    setState(NO_DATA);
                else {
                    getListener().onLoaded(data);
                }
                getListener().onLoaded(new Mail());
            }
        };

        emailDataRequestManager.getMailsByFolder(folder).subscribe(subscriber);
    }

    private void saveToRepo(List<Mail> data) {

        for (Mail mail : data) {

            try {
                HelperFactory.getHelper().getMailDao().createOrUpdate(mail);
                if (mail.hasLabel()) {

                    DeleteBuilder<Label, Long> deleteBuilder =
                            HelperFactory.getHelper().getLabelDao().deleteBuilder();

                    deleteBuilder.where()
                            .eq(Label.FOLDER_NAME, mail.getFolder())
                            .and()
                            .eq(Label.MAIL_ID, mail.getUid());
                    deleteBuilder.delete();

                    Label label = mail.getFirstLabel();
                    Color color = label.getColor();

                    HelperFactory.getHelper().getLabelColorsDao().createOrUpdate(color);

                    label.setMail(mail);
                    label.setColor(color);
                    label.setFolder(mail.getFolder());

                    HelperFactory.getHelper().getLabelDao().createOrUpdate(label);
                }
            } catch (SQLException e) {
                Log.i(this.getClass().getName(), e.getMessage());
            }
        }

    }

    public void loadFromDB(String folder) {
        Subscriber<List<Mail>> subscriber = new Subscriber<List<Mail>>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                if (!TextUtils.isEmpty(folder))
                    setState(NO_DATA);
                else
                    setState(ERROR);
            }

            @Override
            public void onNext(List<Mail> list) {
                Collections.sort(list, (o1, o2) -> o2.getDateTime().compareTo(o1.getDateTime()));

                if (list == null || list.isEmpty()) {
                    try {
                        if (!TextUtils.isEmpty(folder) || HelperFactory.getHelper().getMailDao().countOf() > 0)
                            setState(NO_DATA);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (getListener() != null) {

                        try {
                            for (Mail mail : list) {
                                QueryBuilder<Label, Long> lb = HelperFactory.getHelper().getLabelDao().queryBuilder();
                                List<Label> labelList =
                                        lb.where().eq(Label.FOLDER_NAME, folder).and().eq(Label.MAIL_ID, mail.getUid())
                                                .query();

                                mail.setLabels(labelList);
                            }
                        } catch (SQLException e) {
                            Log.i(this.getClass().getName(), e.getMessage());
                        }
                        getListener().onLoaded(list);
                    }
                    setState(DONE);
                }
            }
        };

        subscription.add(subscriber);
        Observable<List<Mail>> observable = MailsRepo.getInstance().getMails(folder);
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    public void loadMailContent(List<Mail> data, int position) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            Subscriber<MailContent> subscriber = new Subscriber<MailContent>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    Log.d("LEVM", "onError: " + e.getMessage());

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(MailContent mail) {
                    pd.dismiss();

//                    FragmentUtils.changeFragment(
//                            getActivity(),
//                            R.id.content_frame,
//                            EmailFragment.newInstance(data, position, mail, toolbarTitle.get()),
//                            true);


//	                Bundle args = new Bundle();
//	                args.putInt(POSITION, position);
//	                args.putString(FOLDER, toolbarTitle.get());
//	                args.putParcelable(MAIL, mail);
//	                args.putParcelableArrayList(MAILS, new ArrayList<>(data));
//
//	                Intent launchMailContentIntent = new Intent(getActivity(), MailContentActivity.class);
//	                launchMailContentIntent.putExtras(args);
//	                getActivity().startActivity(launchMailContentIntent);

                }
            };
            subscription.add(subscriber);

            String folder = data.get(position).getFolder();
            int idMail = data.get(position).getUid();

            ApiRequest.getInstance().getApi().getMailContent(folder, idMail)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);

        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    public void setToolbarTitle(String folder) {
        toolbarTitle.set(folder);
    }
}
