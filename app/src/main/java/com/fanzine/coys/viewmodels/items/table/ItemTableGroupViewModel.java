package com.fanzine.coys.viewmodels.items.table;

import android.content.Context;
import android.databinding.ObservableField;
import android.text.TextUtils;

import com.fanzine.coys.R;
import com.fanzine.coys.models.table.TeamTable;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 13.02.2017.
 */

public class ItemTableGroupViewModel extends BaseViewModel {

    public ObservableField<TeamTable> team = new ObservableField<>();

    public ItemTableGroupViewModel(Context context) {
        super(context);
    }

    public int getArrowType(TeamTable team) {
        if (!TextUtils.isEmpty(team.getArrow())) {
            if (team.getArrow().equals("arrow"))
                return R.drawable.ic_arrowdown;
            else if (team.getArrow().equals("down"))
                return R.drawable.ic_arrowup;
        }

        return R.drawable.ic_arrownone;
    }

    public String getGd() {
        return String.valueOf(team.get().getGd());
    }
}
