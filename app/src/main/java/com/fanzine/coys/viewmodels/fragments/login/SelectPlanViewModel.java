package com.fanzine.coys.viewmodels.fragments.login;

import android.content.Context;
import android.databinding.ObservableBoolean;

import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 30.03.2017.
 */

public class SelectPlanViewModel extends BaseViewModel {

    public ObservableBoolean isPro = new ObservableBoolean(true);

    public SelectPlanViewModel(Context context) {
        super(context);
    }

    public void free() {
        isPro.set(false);
    }

    public void pro() {
        isPro.set(true);
    }

}
