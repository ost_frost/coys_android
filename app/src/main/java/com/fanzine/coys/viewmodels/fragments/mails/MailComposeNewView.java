package com.fanzine.coys.viewmodels.fragments.mails;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableField;
import android.widget.Toast;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.models.mails.NewMail;
import com.fanzine.coys.models.profile.Email;
import com.fanzine.coys.networking.EmailDataRequestManager;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Evgenij Krasilnikov on 14-Feb-18.
 */

public class MailComposeNewView extends BaseViewModel {

    public ObservableField<String> body = new ObservableField<>();

    public MailComposeNewView(Context context) {
        super(context);
    }

    public void sendMail(NewMail mail) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(ResponseBody body) {
                    pd.dismiss();
                    Toast.makeText(getContext(), R.string.emailSended, Toast.LENGTH_SHORT).show();
                    getActivity().finish();

                }
            };
            subscription.add(subscriber);

            EmailDataRequestManager.getInstanse().sendMail(mail).subscribe(subscriber);

        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }


    public void setEmailSignture(String mailContent) {
        ProgressDialog pd = DialogUtils.createProgressDialog(getContext(), R.string.please_wait);
        showDialog(pd);
        Subscriber<Email> subscriber = new Subscriber<Email>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                dismissDialog(pd);

                FieldError errors = ErrorResponseParser.getErrors(e);

                if (errors.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                } else {
                    DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                }
            }

            @Override
            public void onNext(Email email) {
                dismissDialog(pd);
                String mailSignature = email.getEmailSignature();
                String mailBobyWithSignature = mailContent + "\n" + "\n" + mailSignature;

                body.set(mailBobyWithSignature);
            }
        };
        subscription.add(subscriber);

        ApiRequest.getInstance().getApi().getProfileEmail()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

}
