package com.fanzine.coys.viewmodels.fragments.match;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.models.summary.Event;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

import java.util.Locale;

/**
 * Created by Siarhei on 10.02.2017.
 */

public class GoalDialogViewModel extends BaseViewModel {

    public ObservableField<Event> event = new ObservableField<>();

    public GoalDialogViewModel(Context context, Event event) {
        super(context);
        this.event.set(event);
    }

    public String getMinuteAndPlayer(Event event) {
        return String.format(Locale.ENGLISH, "%d'  %s", event.getMinute(), event.getPlayer());
    }
}
