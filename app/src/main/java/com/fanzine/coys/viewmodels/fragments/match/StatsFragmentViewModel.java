package com.fanzine.coys.viewmodels.fragments.match;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.models.Stats;
import com.fanzine.coys.models.StatsGoals;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.utils.LoadingStates.DONE;
import static com.fanzine.coys.utils.LoadingStates.ERROR;
import static com.fanzine.coys.utils.LoadingStates.NO_DATA;

/**
 * Created by Siarhei on 03.02.2017.
 */

public class StatsFragmentViewModel extends BaseStateViewModel<Stats> {

    public ObservableField<Match> match = new ObservableField<>();

    public ObservableField<StatsGoals> statsGoals = new ObservableField<>();
    public ObservableField<Stats> totalPossesion = new ObservableField<>();

    public ObservableField<Stats> corners = new ObservableField<>();
    public ObservableField<Stats> offsides = new ObservableField<>();
    public ObservableField<Stats> yellowCards = new ObservableField<>();
    public ObservableField<Stats> redCards = new ObservableField<>();

    public StatsFragmentViewModel(Context context, DataListLoadingListener<Stats> listener, Match match) {
        super(context, listener);
        this.match.set(match);
    }

    @Override
    public void loadData(int page) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            Subscriber<List<Stats>> subscriber = new Subscriber<List<Stats>>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    setState(ERROR);
                    e.printStackTrace();
                    getListener().onError();
                }

                @Override
                public void onNext(List<Stats> list) {
                    if (list.size() > 0) {
                        getListener().onLoaded(list);
                        setState(DONE);
                    } else {
                        setState(NO_DATA);
                        getListener().onError();
                    }
                }
            };
            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getStats(match.get().getId())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            setState(ERROR);
//            loadFromDB();
        }
    }
}
