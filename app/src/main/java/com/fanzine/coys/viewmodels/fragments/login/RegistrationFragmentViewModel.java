package com.fanzine.coys.viewmodels.fragments.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.databinding.DialogCodeVerificationBinding;
import com.fanzine.coys.interfaces.LoadingListener;
import com.fanzine.coys.models.Currency;
import com.fanzine.coys.models.Fullname;
import com.fanzine.coys.models.PriceInfo;
import com.fanzine.coys.models.ProEmail;
import com.fanzine.coys.models.PurchaseDto;
import com.fanzine.coys.models.login.Birthday;
import com.fanzine.coys.models.login.Country;
import com.fanzine.coys.models.login.Teams;
import com.fanzine.coys.models.login.UserToken;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;

import java.lang.ref.WeakReference;
import java.util.HashMap;

import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Woland on 22.03.2017.
 */

public class RegistrationFragmentViewModel extends AuthViewModel {


    private interface Keys {
        String FIRST_NAME = "first_name";
        String LAST_NAME = "last_name";
        String BIRTHDAY = "birthday";
        String PHONE = "phone";
        String GENDER = "gender";
        String COUNTRY = "country";
        String TEAM = "team";
        String PASSWORD = "password";
        String PASSWORD_CONFIRMATION = "password_confirmation";
    }

    private final WeakReference<LoadingListener<Teams>> mListener;
    private final WeakReference<SignUpCallback> mSignUpCallback;
    private String fbId;

    private Map<String, String> formFields = new HashMap<>();

    public ObservableField<String> firstName = new ObservableField<>();
    public ObservableField<String> lastName = new ObservableField<>();
    public ObservableField<String> gender = new ObservableField<>();
    public ObservableField<Birthday> birthday = new ObservableField<>();
    public ObservableField<String> phone = new ObservableField<>();
    public ObservableField<String> pass = new ObservableField<>();
    public ObservableField<String> pass2 = new ObservableField<>();
    public ObservableField<ProEmail> proMail = new ObservableField<>();
    public ObservableField<Country> country = new ObservableField<>();
    public ObservableField<Teams.Team> team = new ObservableField<>();
    public ObservableBoolean isPayed = new ObservableBoolean();
    public ObservableField<String> promoCode = new ObservableField<>();
    public ObservableField<Boolean> isAccepted = new ObservableField<>();

    public ObservableBoolean isPro = new ObservableBoolean();
    private PriceInfo mPriceInfo;

    private String checkedPhone = "";

    private UserToken mUserToken = null;



    public interface SignUpCallback {
        void onLoaded(List<ProEmail> data);

        void onError();

        void showPayment();

        void openTeaser(Fullname fullname);

        void openTermsAndPolicy();

        void openFreeUserPage();
    }

    public RegistrationFragmentViewModel(Context context, String id, boolean isPro,
                                         LoadingListener<Teams> listener, SignUpCallback mSignUpCallback) {
        super(context);

        this.mListener = new WeakReference<>(listener);
        this.mSignUpCallback = new WeakReference<>(mSignUpCallback);
        this.fbId = id;

        this.isPro.set(isPro);

        loadTeams();

        isAccepted.set(false);
        firstName.set("");
        lastName.set("");
    }

    public void showTermsAndPolicy() {
        mSignUpCallback.get().openTermsAndPolicy();
    }

    public void toogleTermsAccept() {
        isAccepted.set(!isAccepted.get());
    }

    public HashMap<String, String> getFormData() {
        Gson gson = new Gson();

        HashMap<String, String> data = new HashMap<>();

        data.put(Keys.PASSWORD, pass.get());
        data.put(Keys.PASSWORD_CONFIRMATION, pass2.get());
        data.put(Keys.FIRST_NAME, firstName.get());
        data.put(Keys.LAST_NAME, lastName.get());
        data.put(Keys.GENDER, gender.get());
        data.put(Keys.PHONE, phone.get());

        data.put(Keys.TEAM, gson.toJson(team.get()));
        data.put(Keys.BIRTHDAY, gson.toJson(birthday.get()));
        data.put(Keys.COUNTRY, gson.toJson(country.get()));

        return data;
    }

    public void setFormData(HashMap<String, String> data) {
        Gson gson = new Gson();

        pass.set(data.get(Keys.PASSWORD));
        pass2.set(data.get(Keys.PASSWORD_CONFIRMATION));
        firstName.set(data.get(Keys.FIRST_NAME));
        lastName.set(data.get(Keys.LAST_NAME));
        gender.set(data.get(Keys.GENDER));
        phone.set(data.get(Keys.PHONE));

        team.set(gson.fromJson(data.get(Keys.TEAM), Teams.Team.class));
        birthday.set(gson.fromJson(data.get(Keys.BIRTHDAY), Birthday.class));
        country.set(gson.fromJson(data.get(Keys.COUNTRY), Country.class));
    }


    public void registerInvincible() {
        if (isFormDataValid()) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);
            pd.show();

            prepareRequestData();

            String phoneCode = String.valueOf(country.get().getPhonecode());
            String phoneNumber = phone.get();

            rx.Observable<Currency> validatePro = ApiRequest.getInstance().getApi().validatePro(formFields);
            rx.Observable<ResponseBody> sendSms = ApiRequest.getInstance().getApi().checkPhone(phoneCode, phoneNumber);
            rx.Observable<UserToken> register = ApiRequest.getInstance().getApi().registerPro(formFields);


            AlertDialog verificationDialog = getVerificationCodeDialog(new OnVerifyCodeListener() {

                @Override
                public void check(AlertDialog dialog, String smsCode) {
                    pd.show();

                    rx.Observable<ResponseBody> checkSmsCode =
                            ApiRequest.getInstance().getApi().checkPhone(phoneCode, phoneNumber, smsCode);

                    checkSmsCode
                            .flatMap(ok -> register)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Subscriber<UserToken>() {
                                @Override
                                public void onCompleted() {
                                }

                                @Override
                                public void onError(Throwable e) {
                                    pd.dismiss();
                                    showRegisterError(e);
                                }

                                @Override
                                public void onNext(UserToken userToken) {
                                    dialog.dismiss();
                                    pd.dismiss();
                                    mUserToken = userToken;
                                    mSignUpCallback.get().showPayment();
                                }
                            });
                }
            });

            validatePro.flatMap(currency -> sendSms)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<ResponseBody>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            pd.dismiss();
                            showRegisterError(e);
                        }

                        @Override
                        public void onNext(ResponseBody ok) {
                            pd.dismiss();
                            verificationDialog.show();
                        }
                    });
        }

    }

    private interface OnVerifyCodeListener {
        void check(AlertDialog dialog,String smsCode);
    }

    private void showRegisterError(Throwable e) {
        ErrorResponseParser.ApiErrorWithMessage message =
                ErrorResponseParser.getApiErrorWithMessage(e);

        if (message != null)
            DialogUtils.showAlertDialog(getContext(), message.message);
    }

    private AlertDialog getVerificationCodeDialog(OnVerifyCodeListener verifyCodeListener) {
        DialogCodeVerificationBinding binding = DialogCodeVerificationBinding.inflate(LayoutInflater.from(getContext()));

        AlertDialog dialog = new AlertDialog.Builder(getContext()).setView(binding.getRoot()).create();
        binding.cancel.setOnClickListener(view -> {
           dialog.dismiss();
        });

        binding.enter.setOnClickListener(view -> {
            String smsCode = binding.code.getText().toString();
            verifyCodeListener.check(dialog, smsCode);
        });

        return dialog;
    }

    private boolean isFormDataValid() {
        if (!isAccepted.get()) {
            DialogUtils.showAlertDialog(getContext(), getString(R.string.policy_alert));

            return false;
        }

        if (this.validateInputs()) {
            DialogUtils.showAlertDialog(getContext(), R.string.fieldsRequired);
            return false;
        }

        return true;
    }

    public void deleteUser() {
        ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

        ApiRequest.getInstance().getApi().deleteUser(mUserToken.getUserId(), pass.get())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                        pd.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        pd.dismiss();
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        pd.dismiss();
                        mSignUpCallback.get().openFreeUserPage();
                    }
                });
    }

    public void loadTeams() {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            Subscriber<Teams> subscriber = new Subscriber<Teams>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }

                    if (mListener != null && mListener.get() != null)
                        mListener.get().onError();
                }

                @Override
                public void onNext(Teams teams) {
                    if (mListener != null && mListener.get() != null)
                        mListener.get().onLoaded(teams);
                }
            };
            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getTeams()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    public void showTeaser() {
        if (firstName.get().length() == 0 && lastName.get().length() == 0) {
            DialogUtils.showAlertDialog(getContext(),
                    getString(R.string.sign_up_email_validation));

            return;
        }
        Fullname fullname = new Fullname(firstName.get(), lastName.get());
        mSignUpCallback.get().openTeaser(fullname);
    }

    private void prepareRequestData() {
        formFields.put("first_name", firstName.get());
        formFields.put("last_name", lastName.get());
        formFields.put("birthday", birthday.get().getFormatted());
        formFields.put("phonecode", Integer.toString(country.get().getPhonecode()));
        formFields.put("phone", phone.get());
        formFields.put("gender", gender.get().toLowerCase());
        formFields.put("country_id", Integer.toString(country.get().getId()));
        formFields.put("team_id", Integer.toString(team.get().getId()));
        formFields.put("password", pass.get());
        formFields.put("password_confirmation", pass2.get());


        if (isPro.get()) {
            formFields.put("email", proMail.get().getEmail());

            if (!TextUtils.isEmpty(promoCode.get())) {
                formFields.put("promo_code", promoCode.get());
            }
        }
    }

    public void signUp() {
        try {
            if (!isAccepted.get()) {
                DialogUtils.showAlertDialog(getContext(), getString(R.string.policy_alert));

                return;
            }

            if (this.validateInputs()) {
                DialogUtils.showAlertDialog(getContext(), R.string.fieldsRequired);
                return;
            }

            if (NetworkUtils.isNetworkAvailable(getContext())) {
                ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

                Subscriber<PriceInfo> subscriber = new Subscriber<PriceInfo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        pd.dismiss();

                        showValidationError(e);
                    }

                    @Override
                    public void onNext(PriceInfo priceInfo) {
                        mPriceInfo = priceInfo;

                        checkPhone(pd);
                    }
                };

                subscription.add(subscriber);

                prepareRequestData();

                ApiRequest.getInstance().getApi().validateData(formFields)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(subscriber);
            } else {
                DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
            }
        } catch (Exception e) {
            sendCrashReport(e);
        }
    }

    private void showValidationError(Throwable e) {
        try {

            ErrorResponseParser.ApiErrorWithMessage error = ErrorResponseParser.getApiErrorWithMessage(e);
            if (error == null) {
                DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
            } else {
                DialogUtils.showAlertDialog(getContext(), error.message);
            }
        } catch (Exception parseEx) {
            sendCrashReport(parseEx);

            DialogUtils.showAlertDialog(getContext(), "Validation error.Please, check your data");
        }
    }

    private void checkPhone(ProgressDialog pd) {
        if (!checkedPhone.equals(phone.get())) {
            checkedPhone = "";

            Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    List<FieldError> errors = ErrorResponseParser.getErrorList(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.get(0).getMessage());
                    }
                }

                @Override
                public void onNext(ResponseBody profile) {
                    pd.dismiss();

                    showDialog();
                }
            };

            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().checkPhone(Integer.toString(country.get().getPhonecode()), phone.get())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else
            pay(pd);
    }

    public void showDialog() {
        DialogCodeVerificationBinding binding = DialogCodeVerificationBinding.inflate(LayoutInflater.from(getContext()));


        AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setView(binding.getRoot()).create();


        binding.cancel.setOnClickListener(view -> {
            dialog.dismiss();
        });

        binding.enter.setOnClickListener(view -> {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    ErrorResponseParser.ApiError error = ErrorResponseParser.getApiError(e);
                    if (error == null || error.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), error.getError().getMessage());
                    }
                }

                @Override
                public void onNext(ResponseBody profile) {
                    checkedPhone = phone.get();

                    if (dialog != null) dialog.dismiss();
                    pay(pd);
                }
            };

            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().checkPhone(Integer.toString(country.get().getPhonecode()), phone.get(), binding.code.getText().toString())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        });

        dialog.show();
    }

    private void pay(ProgressDialog pd) {
        if (TextUtils.isEmpty(firstName.get()) || TextUtils.isEmpty(lastName.get()) || TextUtils.isEmpty(gender.get())
                || TextUtils.isEmpty(phone.get()) || TextUtils.isEmpty(pass.get()) || TextUtils.isEmpty(pass2.get())
                || country.get() == null || team.get() == null || (proMail.get() == null && isPro.get())) {
            pd.dismiss();
            DialogUtils.showAlertDialog(getContext(), R.string.fieldsRequired);

            return;
        }

        if (isPro.get()) {
            Subscriber<Currency> subscriber = new Subscriber<Currency>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();
                    ErrorResponseParser.ApiErrorWithMessage error = ErrorResponseParser.getApiErrorWithMessage(e);
                    if (error == null) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), error.message);
                    }
                }

                @Override
                public void onNext(Currency currency) {
                    pd.dismiss();

                    registerPro(currency);
                }
            };
            subscription.add(subscriber);


            ApiRequest.getInstance().getApi().validatePro(formFields)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else
            register(pd);
    }

    public void finish(@NonNull PurchaseDto purchaseDto) {
        Map<String, String> paymentData = new HashMap<>();

        paymentData.put("order_id", purchaseDto.orderId);
        paymentData.put("package_name", purchaseDto.packegeName);
        paymentData.put("signature", purchaseDto.signature);
        paymentData.put("state", String.valueOf(purchaseDto.state));
        paymentData.put("time", String.valueOf(purchaseDto.time));
        paymentData.put("token", purchaseDto.token);
        paymentData.put("email", proMail.get().getEmail());
        paymentData.put("product_id", purchaseDto.productId);

        ApiRequest.getInstance().getApi().sendPaymentInfo(paymentData)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        DialogUtils.showAlertDialog(getContext(), "Payment error");
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        auth(rx.Observable.just(mUserToken));
                    }
                });
    }

    public void registerPro(Currency currency) {
        try {
            if (NetworkUtils.isNetworkAvailable(getContext())) {
                ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);


                ApiRequest.getInstance().getApi().registerPro(formFields)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<UserToken>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                                pd.dismiss();

                                FirebaseCrash.report(e);
                            }

                            @Override
                            public void onNext(UserToken userToken) {
                                pd.dismiss();

                                mUserToken = userToken;
                                mSignUpCallback.get().showPayment();
                            }
                        });
            } else {
                DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
            }
        } catch (Exception e) {
            FirebaseCrash.log(e.getMessage());
            FirebaseCrash.report(e);
        }
    }

    public void register(ProgressDialog pd) {
        try {
            if (TextUtils.isEmpty(firstName.get()) || TextUtils.isEmpty(lastName.get()) || TextUtils.isEmpty(gender.get())
                    || TextUtils.isEmpty(phone.get()) || TextUtils.isEmpty(pass.get()) || TextUtils.isEmpty(pass2.get())
                    || country.get() == null || team.get() == null || (proMail.get() == null && isPro.get())) {
                pd.dismiss();
                DialogUtils.showAlertDialog(getContext(), R.string.fieldsRequired);

                return;
            }

            if (pd != null)
                pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            formFields.put("first_name", firstName.get());
            formFields.put("last_name", lastName.get());
            formFields.put("phonecode", Integer.toString(country.get().getPhonecode()));
            formFields.put("phone", phone.get());
            formFields.put("gender", gender.get().toLowerCase());
            formFields.put("country_id", Integer.toString(country.get().getId()));
            formFields.put("team_id", Integer.toString(team.get().getId()));
            formFields.put("password", pass.get());
            formFields.put("password_confirmation", pass2.get());

            if (!TextUtils.isEmpty(fbId))
                formFields.put("fb_id", fbId);

            rx.Observable<UserToken> tokenObservable = ApiRequest.getInstance().getApi().register(formFields);
            auth(tokenObservable);
        } catch (Exception e) {
            FirebaseCrash.log(e.getMessage());
            FirebaseCrash.report(e);
        }
    }

    public void setId(String id) {
        this.fbId = id;
    }

    private boolean validateInputs() {
        return TextUtils.isEmpty(firstName.get())
                || TextUtils.isEmpty(lastName.get())
                || birthday.get() == null
                || TextUtils.isEmpty(gender.get())
                || TextUtils.isEmpty(phone.get())
                || TextUtils.isEmpty(pass.get())
                || TextUtils.isEmpty(pass2.get())
                || country.get() == null
                || team.get() == null
                || (proMail.get() == null && isPro.get());
    }
}
