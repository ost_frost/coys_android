package com.fanzine.coys.viewmodels.base;

import android.content.Context;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.api.response.LeagueList;
import com.fanzine.coys.models.League;
import com.fanzine.coys.repository.LeaguesRepo;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;

import java.lang.ref.WeakReference;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by maximdrobonoh on 25.09.17.
 */

public abstract class BaseLeaguesBarViewModel extends BaseViewModel {

    private boolean leaguesLoaded = false;

    public void setLeaguesLoaded(boolean leagueLoaded) {
        this.leaguesLoaded = leagueLoaded;
    }


    private Subscriber<LeagueList> subscriber = new Subscriber<LeagueList>() {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

            FieldError errors = ErrorResponseParser.getErrors(e);
            if (!errors.isEmpty()) {
                DialogUtils.showAlertDialog(getContext(), errors.getMessage());
            }
        }

        @Override
        public void onNext(LeagueList list) {
            leaguesLoaded = true;

            if (leagueLoadingListener != null && leagueLoadingListener.get() != null)
                leagueLoadingListener.get().onLeagueLoaded(list.getLeagues());

            saveToRepo(list.getLeagues());
        }
    };

    public interface LeagueLoadingListener {
        void onLeagueLoaded(List<League> list);
    }

    private WeakReference<BaseLeaguesBarViewModel.LeagueLoadingListener> leagueLoadingListener;

    public LeagueLoadingListener getLeagueLoadingListener() {
        return this.leagueLoadingListener.get();
    }

    public void setLeagueLoadingListener(WeakReference<LeagueLoadingListener> leagueLoadingListener) {
        this.leagueLoadingListener = leagueLoadingListener;
    }

    public BaseLeaguesBarViewModel(Context context, BaseLeaguesBarViewModel.LeagueLoadingListener leagueListener) {
        super(context);

        this.leagueLoadingListener = new WeakReference<>(leagueListener);
    }

    public void loadAllLeagues() {
        if (NetworkUtils.isNetworkAvailable(getContext())) {

            subscription.add(this.subscriber);

            ApiRequest.getInstance().getApi().getLeagues()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this.subscriber);
        } else
            loadFromDB();
    }

    public void loadLeagues() {
        if (NetworkUtils.isNetworkAvailable(getContext())) {

            subscription.add(this.subscriber);

            ApiRequest.getInstance().getApi().getMajorLeagues()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this.subscriber);
        } else
            loadFromDB();
    }

    private void saveToRepo(List<League> data) {
        subscription.add(LeaguesRepo.getInstance().saveLeagues(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {

                    }
                }));
    }

    private void loadFromDB() {
        Subscriber<List<League>> subscriber = new Subscriber<List<League>>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
            }

            @Override
            public void onNext(List<League> data) {
                if (data == null || data.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);

                } else {
                    if (leagueLoadingListener != null && leagueLoadingListener.get() != null)
                        leagueLoadingListener.get().onLeagueLoaded(data);

                    leaguesLoaded = true;
                }
            }
        };

        subscription.add(subscriber);
        Observable<List<League>> observable = LeaguesRepo.getInstance().getLeagues();
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}
