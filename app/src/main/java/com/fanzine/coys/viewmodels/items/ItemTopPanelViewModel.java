package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 10.01.2017.
 */

public class ItemTopPanelViewModel extends BaseViewModel {

    public ObservableField<String> name = new ObservableField<>();
    public ObservableBoolean isSelected = new ObservableBoolean();
    public ObservableField<String> icon = new ObservableField<>();

    public ItemTopPanelViewModel(Context context) {
        super(context);
    }

}
