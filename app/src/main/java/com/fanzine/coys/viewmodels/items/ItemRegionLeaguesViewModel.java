package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Siarhei on 13.01.2017.
 */

public class ItemRegionLeaguesViewModel extends BaseViewModel {

    public ObservableField<String> title = new ObservableField<>();

    public ItemRegionLeaguesViewModel(Context context, String title) {
        super(context);
        this.title.set(title);
    }

}
