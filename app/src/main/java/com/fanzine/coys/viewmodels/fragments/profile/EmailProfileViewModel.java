package com.fanzine.coys.viewmodels.fragments.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.models.Status;
import com.fanzine.coys.models.login.UserToken;
import com.fanzine.coys.models.profile.Email;
import com.fanzine.coys.services.MyFirebaseMessagingService;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.coys.utils.DialogUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by vitaliygerasymchuk on 2/12/18
 */

public class EmailProfileViewModel extends NotificationViewModel {

    @NonNull
    public ObservableField<Boolean> notification = new ObservableField<>();
    @NonNull
    public ObservableField<String> signature = new ObservableField<>();
    @NonNull
    public ObservableField<String> emailName = new ObservableField<>();

    public boolean isVisible;

    @Nullable
    private Email email;

    public EmailProfileViewModel(Context context) {
        super(context);
    }

    @Override
    public String getTopic() {
        UserToken userToken = SharedPrefs.getUserToken();
        return MyFirebaseMessagingService.Types.EMAIL_NEW_MESSAGE + userToken.getUserId();
    }

    public void setEmail(@NonNull Email email) {
        this.email = email;
        this.emailName.set(email.getEmail());
        this.signature.set(email.getEmailSignature());
        this.notification.set(email.isNotificationOn());
    }

    public boolean hasData() {
        return email != null;
    }

    public boolean isNotificationStateDiffers(boolean newState) {
        return notification.get() != newState;
    }

    public void getEmail() {
        doWithNetwork(this::getEmailInternal);
    }

    public rx.Observable<Boolean> isPro() {
        return ApiRequest.getInstance().getApi().getUserStatus()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(status -> rx.Observable.just(status.isPro()));
    }


    private void getEmailInternal() {
        ProgressDialog pd = DialogUtils.createProgressDialog(getContext(), R.string.please_wait);
        showDialog(pd);
        Subscriber<Email> subscriber = new Subscriber<Email>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                dismissDialog(pd);

                FieldError errors = ErrorResponseParser.getErrors(e);

                if (errors.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), "Email is not available now.");
                } else {
                    DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                }

            }

            @Override
            public void onNext(Email emails) {
                dismissDialog(pd);
                setEmail(emails);
            }
        };
        subscription.add(subscriber);

        ApiRequest.getInstance().getApi().getProfileEmail()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    public void toggleEmailNotification() {
        doWithNetwork(this::toggleEmailNotificationInternal);
    }

    private void toggleEmailNotificationInternal() {
        ProgressDialog pd = DialogUtils.createProgressDialog(getContext(), R.string.please_wait);
        if (isVisible) {
            showDialog(pd);
        }
        Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                dismissDialog(pd);

                FieldError errors = ErrorResponseParser.getErrors(e);
                if (errors != null) {
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }
            }

            @Override
            public void onNext(ResponseBody emailNotification) {
                if (email != null) {
                    notification.set(!notification.get());
                    email.setEmailNotification(notification.get());
                    setEmail(email);
                }
                dismissDialog(pd);
            }
        };

        subscription.add(subscriber);

        Map<String, String> body = new HashMap<>();
        body.put("email_notification", String.valueOf(!notification.get() ? 1 : 0));

        ApiRequest.getInstance().getApi().setEmailNotification(body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    public boolean isNeedToSaveSignature(@NonNull String newSign) {
        return email != null && !TextUtils.equals(email.getEmailSignature(), newSign);
    }

    public void setEmailSignature(@NonNull String signature) {
        doWithNetwork(() -> setEmailSignatureInternal(signature));
    }

    private void setEmailSignatureInternal(@NonNull String signature) {
        ProgressDialog pd = DialogUtils.createProgressDialog(getContext(), R.string.please_wait);
        if (isVisible) {
            showDialog(pd);
        }
        Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                dismissDialog(pd);

                FieldError errors = ErrorResponseParser.getErrors(e);

                if (email != null) {
                    EmailProfileViewModel.this.signature.set(email.getEmailSignature());
                }

                if (errors != null) {
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                } else {
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                }
            }

            @Override
            public void onNext(ResponseBody responseBody) {
                if (email != null) {
                    email.setEmailSignature(signature);
                }
                dismissDialog(pd);
            }
        };
        subscription.add(subscriber);

        Map<String, String> body = new HashMap<>();
        body.put("signature", signature);

        ApiRequest.getInstance().getApi().setEmailSignature(body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}
