package com.fanzine.coys.viewmodels.fragments.team.player;

import android.content.Context;

import com.fanzine.coys.models.team.PlayerPhoto;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.models.team.Player;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mbp on 1/12/18.
 */

public class PhotosViewModel extends BaseViewModel {

    public interface OnPhotosLoadListener {
        void onSuccess(List<PlayerPhoto> photos);
    }

    private Player player;
    private OnPhotosLoadListener loadListener;

    public PhotosViewModel(Context context, Player player, OnPhotosLoadListener loadListener) {
        super(context);
        this.player = player;
        this.loadListener = loadListener;
    }

    public void loadPhotos() {
        Subscriber<List<PlayerPhoto>> subscriber = new Subscriber<List<PlayerPhoto>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<PlayerPhoto> photos) {
                loadListener.onSuccess(photos);
            }
        };

        ApiRequest.getInstance().getApi().getTeamPlayerPhotos(player.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}
