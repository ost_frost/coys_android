package com.fanzine.coys.viewmodels.fragments.team.player;

import android.content.Context;
import android.databinding.ObservableField;
import android.text.Html;

import com.fanzine.coys.models.team.PlayerProfile.Profile;
import com.fanzine.coys.utils.TimeDateUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;


/**
 * Created by mbp on 1/11/18.
 */

public class ProfileViewModel extends BaseViewModel {

    private final static String UNIT = "cm";
    private final static String AGE_PREFIX = "Years";
    private final static String API_BIRTHDAY_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSSSSS";
    private final static String DISPLAYED_BIRTHDAY_DATE_FORMAT = "dd MMM yyyy";
    private final static String EMPTY_BIRTHDAY_DATE = "";
    private final static String EMPTY_DESCRIPTION = "";
    private final static String UNKNOWN_EXPECTED_RETURN = "Unknown";
    private final static String SEASON_DELIMITER = " ";

    public ObservableField<Profile> profile = new ObservableField<>();

    public ProfileViewModel(Context context, Profile profile) {
        super(context);
        this.profile.set(profile);
    }

    public String getHeight(Profile profile) {
        return String.valueOf(profile.height) + UNIT;
    }

    public String getWeight(Profile profile) {
        return String.valueOf(profile.weight) + " " + UNIT;
    }

    public String getAge(Profile profile) {
        return String.valueOf(profile.age);
    }

    public String getYearOfBirth(Profile profile) {
        return profile.getYearOfBirth();
    }

    public String getGoals(Profile profile) {
        if (profile.stats.size() > 1) {
            return profile.stats.get(1).getValue();
        }
        return "0";
    }

    public String getAssists(Profile profile) {
        if (profile.stats.size() > 2) {
            return profile.stats.get(2).getValue();
        }
        return "0";
    }

    public String getAppearance(Profile profile) {
        if (profile.stats.size() > 0) {
            return profile.stats.get(0).getValue();
        }
        return "0";
    }

    public String getBirthdayDate(Profile profile) {
        String birthdayDate = TimeDateUtils.convert(profile.birthday.date, API_BIRTHDAY_DATE_FORMAT,
                DISPLAYED_BIRTHDAY_DATE_FORMAT);

        return birthdayDate.length() > 0 ? birthdayDate : EMPTY_BIRTHDAY_DATE;
    }

    public String getSeason(Profile profile) {
        return profile.leagueName + SEASON_DELIMITER + profile.season;
    }

    public String getInjureDescription(Profile profile) {
        return profile.injureDescription;
    }

    public String getExpectedReturn(Profile profile) {
        return profile.hasExpectedReturn() ? profile.expectedReturn : UNKNOWN_EXPECTED_RETURN;
    }

    public String getBiography(Profile profile) {
        return profile.hasBiography() ? Html.fromHtml(profile.biography).toString() : EMPTY_DESCRIPTION;
    }
}
