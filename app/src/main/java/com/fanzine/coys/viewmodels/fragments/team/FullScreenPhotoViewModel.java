package com.fanzine.coys.viewmodels.fragments.team;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.models.team.Photo;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 16.03.2017.
 */

public class FullScreenPhotoViewModel extends BaseViewModel {

    public ObservableField<Photo> photo = new ObservableField<>();

    public FullScreenPhotoViewModel(Context context) {
        super(context);
    }

}
