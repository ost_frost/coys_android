package com.fanzine.coys.viewmodels.items.match;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.fanzine.coys.models.lineups.Player;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 09.02.2017.
 */

public class ItemLineUpViewModel extends BaseViewModel {

    public ObservableField<Player> player = new ObservableField<>();
    public ObservableBoolean isUp = new ObservableBoolean();
    public ObservableBoolean isGone = new ObservableBoolean();

    public ItemLineUpViewModel(Context context) {
        super(context);
    }

}
