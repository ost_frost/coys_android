package com.fanzine.coys.viewmodels.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.fanzine.coys.R;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.google.firebase.crash.FirebaseCrash;

import java.lang.ref.WeakReference;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by Woland on 05.01.2017.
 */

public abstract class BaseViewModel {

    private WeakReference<Context> mContext;
    protected CompositeSubscription subscription = new CompositeSubscription();

    public BaseViewModel(Context context) {
        this.mContext = new WeakReference<>(context);
    }

    public void destroy() {
        mContext = null;
        subscription.unsubscribe();
    }

    public void clearSubscriptions() {
        subscription.clear();
    }

    protected Context getContext() {
        return mContext.get();
    }

    protected AppCompatActivity getActivity() {
        if (mContext.get() instanceof AppCompatActivity)
            return (AppCompatActivity) mContext.get();
        else
            return null;
    }

    public String getString(int id) {
        return mContext.get().getString(id);
    }

    public String getString(int id, Object... args) {
        return mContext.get().getString(id, args);
    }

    public ProgressDialog showProgressDialog() {
        ProgressDialog pd = new ProgressDialog(getContext());
        pd.setMessage(getString(R.string.pleaseWait));
        pd.setCancelable(false);
        pd.show();

        return pd;
    }

    protected void doWithNetwork(Runnable runnable) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            runnable.run();
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    public ProgressDialog buildProgressDialog() {
        return DialogUtils.showProgressDialog(getContext(), R.string.please_wait);
    }

    public void sendCrashReport(Throwable e) {
        FirebaseCrash.report(e);
        FirebaseCrash.log(e.getMessage());
    }

    protected void dismissDialog(@Nullable ProgressDialog dialog) {
        if (dialog != null && dialog.isShowing()) dialog.dismiss();
    }

    protected void showDialog(@Nullable ProgressDialog dialog) {
        if (dialog != null) {
            dialog.show();
        }
    }
}
