package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.databinding.ObservableInt;

import com.fanzine.coys.R;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 13.01.2017.
 */

public class ItemProfileViewModel extends BaseViewModel {

    public ObservableInt position = new ObservableInt();

    public ItemProfileViewModel(Context context) {
        super(context);
    }

    public int getImageId(int position) {
        switch (position) {
            default:
                return 0;

            case 0:
                return R.drawable.ic_account;

            case 1:
                return R.drawable.ic_chat;

            case 2:
                return R.drawable.ic_notification;

            case 3:
                return R.drawable.ic_email;

            case 4:
                return R.drawable.ic_information;
        }
    }

    public String getTitle(int position) {
        switch (position) {
            default:
                return "";

            case 0:
                return getString(R.string.account);

            case 1:
                return getString(R.string.chats);

            case 2:
                return getString(R.string.notifications);

            case 3:
                return getString(R.string.email);

            case 4:
                return getString(R.string.aboutAndHelp);
        }
    }

}
