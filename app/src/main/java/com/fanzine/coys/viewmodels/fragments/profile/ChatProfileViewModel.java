package com.fanzine.coys.viewmodels.fragments.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.models.login.UserToken;
import com.fanzine.coys.models.profile.ChatNotificationState;
import com.fanzine.coys.services.MyFirebaseMessagingService;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by vitaliygerasymchuk on 2/10/18
 */

public class ChatProfileViewModel extends NotificationViewModel {

    public ObservableField<Integer> chatState = new ObservableField<>();

    public interface ChatStatusCallback {
        void onChatState(ChatNotificationState state);
    }

    public ChatProfileViewModel(Context context) {
        super(context);
    }

    @Override
    public String getTopic() {
        UserToken userToken = SharedPrefs.getUserToken();
        return MyFirebaseMessagingService.Types.CHAT_NEW_MESSAGE + userToken.getUserId();
    }

    public void setChatState(ChatNotificationState state) {
        this.chatState.set(state.chat);
    }

    public boolean isStatesDiffer(int newState) {
        return chatState.get() != null && chatState.get() != newState;
    }

    public void getChatNotificationsStatus(ChatStatusCallback callback) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
//            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);
            Subscriber<ChatNotificationState> subscriber = new Subscriber<ChatNotificationState>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
//                    pd.dismiss();

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(ChatNotificationState responseBody) {
                    callback.onChatState(responseBody);
//                    pd.dismiss();
                }
            };

            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getChatNotificationsStatus()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }
    public void postChatNotificationState(ChatNotificationState state, ChatStatusCallback callback) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);
            Subscriber<ChatNotificationState> subscriber = new Subscriber<ChatNotificationState>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(ChatNotificationState responseBody) {
                    callback.onChatState(state);
                    pd.dismiss();
                    chatState.set(state.chat);
                }
            };

            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().setChatNotificationsState(state)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }
}
