package com.fanzine.coys.viewmodels.items.table;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.models.table.Filter;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by maximdrobonoh on 29.09.17.
 */

public class ItemTableTeamFilterViewModel extends BaseViewModel {

    public ObservableField<Filter> filter = new ObservableField<>();

    public ItemTableTeamFilterViewModel(Context context, Filter filter) {
        super(context );

        this.filter.set(filter);
    }

    public ObservableField<Filter> getFilter() {
        return filter;
    }
}
