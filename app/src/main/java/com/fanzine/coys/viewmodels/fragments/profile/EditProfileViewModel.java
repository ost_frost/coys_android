package com.fanzine.coys.viewmodels.fragments.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableField;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.databinding.DialogEdittextBinding;
import com.fanzine.coys.models.login.Country;
import com.fanzine.coys.models.login.UserToken;
import com.fanzine.coys.models.profile.Profile;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.ImageUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;
import com.fanzine.chat.ChatSDK;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Woland on 20.04.2017.
 */

public class EditProfileViewModel extends BaseViewModel {

    public ObservableField<Profile> profile = new ObservableField<>();
    public ObservableField<Country> country = new ObservableField<>();
    public ObservableField<String> phone = new ObservableField<>();

    private File image;

    public boolean isChecked = false;

    public EditProfileViewModel(Context context) {
        super(context);
    }

    public void setImageFile(File file) {
        this.image = file;
    }

    public void save() {
        if (country.get().getPhonecode() == profile.get().getPhonecode() && phone.get().equals(profile.get().getPhone())) {
            sendData();
        }
        else {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    List<FieldError> errors = ErrorResponseParser.getErrorList(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.get(0).getMessage());
                    }
                }

                @Override
                public void onNext(ResponseBody profile) {
                    pd.dismiss();
                    showCheckCodeDialog();
                }
            };

            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().checkPhone(Integer.toString(country.get().getPhonecode()), phone.get())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        }
    }

    private void showCheckCodeDialog() {
        DialogEdittextBinding binding = DialogEdittextBinding.inflate(LayoutInflater.from(getContext()));
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.enterCodeVer)
                .setView(binding.getRoot())
                .setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.ok, (dialog, which) -> {
                    ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

                    Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {

                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            pd.dismiss();

                            List<FieldError> errors = ErrorResponseParser.getErrorList(e);
                            if (errors.isEmpty()) {
                                DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                            } else {
                                DialogUtils.showAlertDialog(getContext(), errors.get(0).getMessage());
                            }
                        }

                        @Override
                        public void onNext(ResponseBody profile) {
                            pd.dismiss();
                            sendData();
                        }
                    };

                    subscription.add(subscriber);

                    ApiRequest.getInstance().getApi().checkPhone(Integer.toString(country.get().getPhonecode()), phone.get(), binding.edEmail.getText().toString())
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(subscriber);
                })
                .show();
    }

    public void sendData() {
        profile.get().setProfileImage(null);
        Observable<Profile> observable;
        if (image == null) {
//            observable = ApiRequest.getInstance().getApi().setProfile(profile.get());
        }
        else {
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), image);

            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part photo_file =
                    MultipartBody.Part.createFormData("profile_image", image.getName(), requestFile);


            Map<String, RequestBody> params = new HashMap<>();
            params.put("first_name", RequestBody.create(MediaType.parse("text/plain"), profile.get().getFirstName()));
            params.put("last_name", RequestBody.create(MediaType.parse("text/plain"), profile.get().getLastName()));
            params.put("phonecode", RequestBody.create(MediaType.parse("text/plain"), Integer.toString(country.get().getPhonecode())));
            params.put("phone", RequestBody.create(MediaType.parse("text/plain"), phone.get()));

            observable = ApiRequest.getInstance().getApi().setProfile(params, photo_file);
        }

        ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

        Subscriber<Profile> subscriber = new Subscriber<Profile>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                pd.dismiss();

                List<FieldError> errors = ErrorResponseParser.getErrorList(e);
                if (errors.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                } else {
                    DialogUtils.showAlertDialog(getContext(), errors.get(0).getMessage());
                }
            }

            @Override
            public void onNext(Profile profile) {
                pd.dismiss();

                if (image != null)
                    ImageUtils.deleteImage(getContext(), image);

                UserToken token = SharedPrefs.getUserToken();
                token.setFirstName(profile.getFirstName());
                token.setLastName(profile.getLastName());
                SharedPrefs.saveUserToken(token);

                ChatSDK.getInstance().getCurrentSession().getCurrentUser().setPhone(profile.getPhonecode() + profile.getPhone());
                ChatSDK.getInstance().getCurrentSession().getCurrentUser().setFirstName(profile.getFirstName());
                ChatSDK.getInstance().getCurrentSession().getCurrentUser().setLastName(profile.getLastName());
                if (!TextUtils.isEmpty(profile.getProfileImage()))
                    ChatSDK.getInstance().getCurrentSession().getCurrentUser().setAvatar(profile.getProfileImage());

                ChatSDK.getInstance().getCurrentSession().getCurrentUser().synchronize();

                getActivity().onBackPressed();
            }
        };

        subscription.add(subscriber);

//        observable.subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(subscriber);
    }

}
