package com.fanzine.coys.viewmodels.items.venues;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.utils.TimeAgoUtil;
import com.fanzine.coys.viewmodels.base.BaseViewModel;
import com.yelp.fusion.client.models.Review;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

/**
 * Created by Woland on 15.03.2017.
 */

public class ItemReviewsVenusViewModel extends BaseViewModel {

    public ObservableField<Review> review = new ObservableField<>();

    public ItemReviewsVenusViewModel(Context context, Review review) {
        super(context);

        this.review.set(review);
    }

    public String getUserImage(Review review) {
        return review.getUser().getImageUrl();
    }

    public String getTime(Review review) {
        return TimeAgoUtil.getTimeAgo(DateTime.parse(review.getTimeCreated(), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).getMillis());
    }

}
