package com.fanzine.coys.viewmodels.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.fanzine.coys.R;
import com.fanzine.coys.api.GoogleApiRequest;
import com.fanzine.coys.models.venues.GooglePlaceResults;
import com.fanzine.coys.viewmodels.base.BaseViewModel;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.Places;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mbp on 2/5/18.
 */

public abstract class GoogleMapBaseViewModel extends BaseViewModel {

    private GoogleApiClient mGoogleApiClient;

    public GoogleMapBaseViewModel(Context context) {
        super(context);

        this.mGoogleApiClient = new GoogleApiClient
                .Builder(getContext())
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();

        this.mGoogleApiClient.connect();
    }

    public boolean isGoogleMapConnected() {
        return mGoogleApiClient.isConnected();
    }

    protected abstract void onPlacesLoaded(PlaceBuffer places);

    protected abstract void onPlacesLoadError();


    protected void requestPlacesByid(String placeId) {
        if (mGoogleApiClient.isConnected()) {

            PendingResult<PlaceBuffer> bufferPendingResult =
                    Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);

            bufferPendingResult.setResultCallback(places -> {
//                                    mPendingDialog.dismiss();

                if (places.getStatus().isSuccess()) {
                    onPlacesLoaded(places);
                } else {
                    onPlacesLoadError();
                }
            });
        } else {
            onPlacesLoadError();
        }
    }

    public void requestGeoCode(String address) {
        String key = getContext().getString(R.string.google_maps_api_key);

//        mPendingDialog.show();

        GoogleApiRequest.getInstance().getApi().geoCode(address, key)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GooglePlaceResults>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i(getClass().getName(), e.getMessage());
//                        mPendingDialog.dismiss();
                    }

                    @Override
                    public void onNext(GooglePlaceResults googlePlaceResults) {
                        if (googlePlaceResults != null && googlePlaceResults.getResultList().size() > 0) {
                            GooglePlaceResults.Result result = googlePlaceResults.getResultList().get(0);

                            requestPlacesByid(result.getPlaceId());
                        }
                    }
                });
    }


    protected Bitmap requestPhotoByPlaceId(String placeId) {

        try {
            PlacePhotoMetadataResult result = Places.GeoDataApi
                    .getPlacePhotos(mGoogleApiClient, placeId).await();

            if (result.getStatus().isSuccess()) {
                PlacePhotoMetadataBuffer photoMetadataBuffer = result.getPhotoMetadata();


                return photoMetadataBuffer.get(0).getPhoto(mGoogleApiClient).await().getBitmap();
            }
        } catch (Exception e){
            //todo:: handle com.almet.arsenal.viewmodels.fragments.GoogleMapBaseViewModel.requestPhotoByPlaceId
        }

        return null;
    }
}
