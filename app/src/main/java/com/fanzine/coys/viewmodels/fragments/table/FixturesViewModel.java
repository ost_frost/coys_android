package com.fanzine.coys.viewmodels.fragments.table;

import android.content.Context;
import android.databinding.ObservableField;
import android.util.Log;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.fragments.base.BaseLeaguesBarFragment;
import com.fanzine.coys.fragments.table.BaseTableFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.models.table.TablesMatch;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.utils.LoadingStates.DONE;
import static com.fanzine.coys.utils.LoadingStates.ERROR;
import static com.fanzine.coys.utils.LoadingStates.NO_DATA;

/**
 * Created by maximdrobonoh on 25.09.17.
 */

public class FixturesViewModel extends BaseStateViewModel<Match> {

    public ObservableField<Integer> week = new ObservableField<>();

    public ObservableField<Integer> maxWeek = new ObservableField<>();

    public ObservableField<String> weekTitle = new ObservableField<>();

    public FixturesViewModel(Context context, DataListLoadingListener<Match> listener) {
        super(context, listener);
    }

    @Override
    public void loadData(int weekId) {

        if (NetworkUtils.isNetworkAvailable(getContext())) {

            Subscriber<TablesMatch> subscriber = getSubscriber();

            subscription.add(subscriber);


            ApiRequest.getInstance().getApi().getTableMathches(BaseLeaguesBarFragment.getSelectedLeagueId(), "2017/2018",
                    getCurrentDate(), weekId)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            setState(ERROR);
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    public void loadCurrent() {
        Subscriber<TablesMatch> subscriber = getSubscriber();
        subscription.add(subscriber);

        try {
            ApiRequest.getInstance().getApi().getTableMathches(BaseTableFragment.getSelectedLeagueId(), "2017/2018",
                    getCurrentDate())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } catch (Exception e){
            Log.e(getClass().getName(), e.getMessage());
        }
    }

    public boolean canLoadNext() {
        return week.get() < maxWeek.get();
    }

    public boolean canLoadPrev() {
        return  week.get() > 1;
    }

    private Subscriber<TablesMatch> getSubscriber() {
        return new Subscriber<TablesMatch>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                setState(ERROR);
                e.printStackTrace();
                getListener().onError();
            }

            @Override
            public void onNext(TablesMatch tablesMatch) {

                week.set(tablesMatch.getWeek());
                weekTitle.set("Week " + String.valueOf(tablesMatch.getWeek()));
                maxWeek.set(tablesMatch.getMaxWeek());

                if (tablesMatch != null && tablesMatch.getMatches().size() > 0) {
                    getListener().onLoaded(tablesMatch.getMatches());
                    setState(DONE);
                } else {
                    setState(NO_DATA);
                    getListener().onError();
                }
            }
        };
    }

    private String getCurrentDate() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MMM-dd");
        return df.format(c.getTime());
    }
}
