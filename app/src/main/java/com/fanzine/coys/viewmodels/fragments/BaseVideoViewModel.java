package com.fanzine.coys.viewmodels.fragments;

import android.content.Context;

import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.Video;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;

import java.util.List;

import rx.Subscriber;

import static com.fanzine.coys.utils.LoadingStates.DONE;
import static com.fanzine.coys.utils.LoadingStates.ERROR;

/**
 * Created by mbp on 1/12/18.
 */

public abstract class BaseVideoViewModel extends BaseStateViewModel<Video> {

    public BaseVideoViewModel(Context context, DataListLoadingListener<Video> listener) {
        super(context, listener);
    }


    protected Subscriber<List<Video>> getSubscriber(int page) {
        return new Subscriber<List<Video>>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                getListener().onError();

                if (page == 0)
                    setState(ERROR);
            }

            @Override
            public void onNext(List<Video> data) {
                getListener().onLoaded(data);
                setState(DONE);
            }
        };
    }
}
