package com.fanzine.coys.viewmodels.fragments.venue;

import android.content.Context;
import android.databinding.ObservableField;
import android.util.Log;

import com.fanzine.coys.api.YelpConfig;
import com.fanzine.coys.fragments.venue.AutocompleteListener;
import com.fanzine.coys.fragments.venue.SearchRequest;
import com.fanzine.coys.viewmodels.base.BaseViewModel;
import com.google.android.gms.maps.model.LatLng;
import com.yelp.fusion.client.connection.YelpFusionApiFactory;
import com.yelp.fusion.client.models.AutoComplete;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

/**
 * Created by mbp on 12/27/17.
 */

public class VenueSearchActivityViewModel extends BaseViewModel {

    public ObservableField<Boolean> isPendindg = new ObservableField<>();

    private YelpFusionApiFactory apiFactory;
    private AutocompleteListener autocompleteListener;

    private PublishSubject<SearchRequest> subject;


    public VenueSearchActivityViewModel(Context context, AutocompleteListener autocompleteListener) {
        super(context);

        this.apiFactory = new YelpFusionApiFactory();
        this.autocompleteListener = autocompleteListener;

        subject = PublishSubject.create();

        initSearchHandler();

    }

    public void autocomplete(String query, LatLng latLng) {
        subject.onNext(new SearchRequest(query, latLng));
    }

    public void submit() {
        subject.onCompleted();
    }

    private void initSearchHandler() {
        subject.debounce(250, TimeUnit.MILLISECONDS)
                .switchMap(this::dataFromYelp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<AutoComplete>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Response<AutoComplete> autoCompleteResponse) {
                        Log.i(getClass().getName(), "AutoComplete: " + autoCompleteResponse.toString());

                        Log.i(getClass().getName(), "AutoComplete: " + autoCompleteResponse.raw().body().toString());
                        if (autoCompleteResponse.body() != null) {
                            AutoComplete autoComplete = autoCompleteResponse.body();

                            Log.i(getClass().getName(), "TERMS: " + autoComplete.getTerms().size());
                            Log.i(getClass().getName(), "BUSINESS: " + autoComplete.getBusinesses().size());

                            autocompleteListener.showTerms(autoComplete.getTerms());
                            autocompleteListener.showVenues(autoComplete.getBusinesses());
                        } else {
                            autocompleteListener.autocompleteError();
                        }
                    }
                });

    }

    private Observable<Response<AutoComplete>> dataFromYelp(SearchRequest request) {
        return Observable.fromCallable(() -> apiFactory.
                createAPI(YelpConfig.clientId, YelpConfig.clientSecret))
                .onErrorResumeNext(Observable.empty())
                .flatMap(yelpFusionApi -> {

                    Map<String, String> params = new HashMap<>();

                    params.put("text", request.query);
                    params.put("latitude", String.valueOf(request.latLng.latitude));
                    params.put("longitude", String.valueOf(request.latLng.longitude));

                    try {
                        return Observable.just(yelpFusionApi.getAutocomplete(params).execute());
                    } catch (IOException e) {
                        return Observable.empty();
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
