package com.fanzine.coys.viewmodels.items.venues;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.R;
import com.fanzine.coys.helpers.Constants;
import com.fanzine.coys.viewmodels.base.BaseViewModel;
import com.yelp.fusion.client.models.Business;

import java.util.Locale;

/**
 * Created by mbp on 3/23/18.
 */

public class ItemMapBusiness extends BaseViewModel {

    public ObservableField<Business> business = new ObservableField<>();

    private String number;

    public ItemMapBusiness(Context context, Business business, int number) {
        super(context);
        this.business.set(business);
        this.number = String.valueOf(number);
    }

    public String getTitle(Business business) {
        return number + ". " + business.getName();
    }

    public String getDistance(Business business) {
        double disntanceInKm = business.getDistance() / Constants.METTERS_IN_MILE;

        String distance = getString(R.string.mi, String.format(Locale.UK, "%.2f", disntanceInKm));

        return distance;
    }

    public String getRating(Business business) {
        if (business != null) {
            return String.valueOf(business.getRating());
        } else
            return String.valueOf(getString(R.string.int_value_zero));
    }

    public String getReviewCount(Business business) {
        return String.valueOf(business.getReviewCount()) + " " + getString(R.string.item_map_business_reviews);
    }

    public String getAddress(Business business) {
        com.yelp.fusion.client.models.Location location = business.getLocation();
        return location.getAddress1() + ", " + location.getCity();
    }

    public String getCategory(Business business) {
        return business.getCategories().get(0).getTitle();
    }
}
