package com.fanzine.coys.viewmodels.fragments.mails;

import android.app.ProgressDialog;
import android.content.Context;


import com.fanzine.coys.R;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.db.factory.HelperFactory;
import com.fanzine.coys.fragments.mails.NewLabelFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.mails.Color;
import com.fanzine.coys.models.mails.Label;
import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.networking.EmailDataRequestManager;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import rx.Subscriber;

import static com.fanzine.coys.utils.LoadingStates.DONE;
import static com.fanzine.coys.utils.LoadingStates.ERROR;
import static com.fanzine.coys.utils.LoadingStates.LOADING;

/**
 * Created by maximdrobonoh on 14.09.17.
 */

public class ListLabelsViewModel extends BaseStateViewModel<Label> {

    public ListLabelsViewModel(Context context, DataListLoadingListener<Label> listener) {
        super(context, listener);
    }

    @Override
    public void loadData(int page) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            Subscriber<List<Label>> subscriber = new Subscriber<List<Label>>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    if (getStateValue() == LOADING)
                        setState(ERROR);

                    getListener().onError();
                }

                @Override
                public void onNext(List<Label> labels) {
                    setState(DONE);
                    getListener().onLoaded(labels);
                }
            };
            subscription.add(subscriber);

            EmailDataRequestManager.getInstanse()
                    .getLabelsList()
                    .subscribe(subscriber);
        }
    }

    public void deleteLabel(Label label) {
        ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

        Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                pd.dismiss();

                FieldError errors = ErrorResponseParser.getErrors(e);

                if (errors.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                } else {
                    DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                }
            }

            @Override
            public void onNext(ResponseBody response) {

                try {
                    DeleteBuilder<Label, Long> deleteBuilder =
                            HelperFactory.getHelper().getLabelDao().deleteBuilder();
                    deleteBuilder.where().eq(Label.API_LABEL_ID, label.getApiLabelId());
                    deleteBuilder.delete();

                } catch (SQLException e) {

                }

                pd.dismiss();
            }
        };

        EmailDataRequestManager.getInstanse()
                .deleteLabel(label.getApiLabelId())
                .subscribe(subscriber);
    }

    public void addLabelToMail(Label label, Mail mail) {
        ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

        Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                pd.dismiss();

                FieldError errors = ErrorResponseParser.getErrors(e);

                if (errors.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                } else {
                    DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                }
            }

            @Override
            public void onNext(ResponseBody response) {
                pd.dismiss();

                try {
                    Color color = label.getColor();
                    HelperFactory.getHelper().getLabelColorsDao().createOrUpdate(color);

                    label.setFolder(mail.getFolder());
                    label.setMail(mail);
                    label.setColor(color);
                    HelperFactory.getHelper().getLabelDao().create(label);
                } catch (SQLException e) {

                }
                getActivity().onBackPressed();
            }
        };

        EmailDataRequestManager.getInstanse()
                .addLabelToMail(mail.getFolder(), mail.getUid(), label.getApiLabelId())
                .subscribe(subscriber);
    }

    public void loadColors(Mail mail) {
        ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

        Subscriber<List<Color>> subscriber = new Subscriber<List<Color>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                pd.dismiss();

                FieldError errors = ErrorResponseParser.getErrors(e);

                if (errors.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                } else {
                    DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                }
            }

            @Override
            public void onNext(List<Color> colors) {
                pd.dismiss();

                FragmentUtils.changeFragment(getActivity(), R.id.content_frame,
                        NewLabelFragment.newInstance((ArrayList<Color>)colors, mail), false);
            }
        };

        EmailDataRequestManager.getInstanse()
                .getLabelColors()
                .subscribe(subscriber);
    }

    public void loadFromList(List<Label> labels) {
        setState(DONE);
        getListener().onLoaded(labels);
    }
}
