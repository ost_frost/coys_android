package com.fanzine.coys.viewmodels.fragments.match;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.models.summary.Event;
import com.fanzine.coys.models.summary.Summary;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.utils.LoadingStates.DONE;
import static com.fanzine.coys.utils.LoadingStates.ERROR;
import static com.fanzine.coys.utils.LoadingStates.NO_DATA;

/**
 * Created by Siarhei on 10.02.2017.
 */

public class SummaryViewModel extends BaseStateViewModel<Event> {

    public ObservableField<Match> match = new ObservableField<>();

    public SummaryViewModel(Context context, DataListLoadingListener<Event> listener, Match match) {
        super(context, listener);
        this.match.set(match);
    }

    @Override
    public void loadData(int matchId) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            Subscriber<Summary> subscriber = new Subscriber<Summary>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    setState(ERROR);
                    e.printStackTrace();
                    getListener().onError();
                }

                @Override
                public void onNext(Summary summary) {
                    if (summary.getEvents() != null && summary.getEvents().size() > 0) {
                        getListener().onLoaded(summary.getEvents());
                        setState(DONE);
                    } else {
                        setState(NO_DATA);
                        getListener().onError();
                    }
                }
            };
            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getSummary(match.get().getId())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            setState(ERROR);
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }
}
