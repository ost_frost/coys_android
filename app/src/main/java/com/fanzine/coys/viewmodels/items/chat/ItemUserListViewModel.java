package com.fanzine.coys.viewmodels.items.chat;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 17.04.2017.
 */

public class ItemUserListViewModel extends BaseViewModel {

    public ObservableBoolean isOwner = new ObservableBoolean();
    public ObservableBoolean isOneToOne = new ObservableBoolean();
    public ObservableBoolean isBanned = new ObservableBoolean();
    public ObservableField<String> name = new ObservableField<>();

    public ItemUserListViewModel(Context context) {
        super(context);
    }

}
