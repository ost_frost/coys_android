package com.fanzine.coys.viewmodels;

import android.content.Context;

import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.viewmodels.fragments.GoogleMapBaseViewModel;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mbp on 2/5/18.
 */

public class UpcomingFixturesViewModel extends GoogleMapBaseViewModel {

    private OnFixturesLoadingListener onFixturesLoadingListener;


    public interface OnFixturesLoadingListener {
        void onLoaded(List<Match> matches);

        void onAddressReady(Place place);
        void onAddressError();
    }

    public UpcomingFixturesViewModel(Context context,  OnFixturesLoadingListener onFixturesLoadingListener) {
        super(context);
        this.onFixturesLoadingListener = onFixturesLoadingListener;
    }

    @Override
    protected void onPlacesLoaded(PlaceBuffer places) {
        if ( places.getStatus().isSuccess()) {
            Place place = places.get(0);
            onFixturesLoadingListener.onAddressReady(place);
        } else {
            onFixturesLoadingListener.onAddressError();
        }
    }

    @Override
    protected void onPlacesLoadError() {
        onFixturesLoadingListener.onAddressError();
    }


    public void loadFixtures(int limit) {
        Subscriber<List<Match>> subscriber = new Subscriber<List<Match>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<Match> matchList) {
                onFixturesLoadingListener.onLoaded(matchList);
            }
        };

        ApiRequest.getInstance().getApi().getFixturesUpcoming(limit)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}
