package com.fanzine.coys.viewmodels.fragments.table;

import android.content.Context;

import com.fanzine.coys.fragments.table.BaseTableFragment;
import com.fanzine.coys.interfaces.DataListFilterListener;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.table.Filter;
import com.fanzine.coys.models.table.PlayerStats;
import com.fanzine.coys.networking.MatchDataRequestManager;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.Subscriber;

import static com.fanzine.coys.utils.LoadingStates.DONE;
import static com.fanzine.coys.utils.LoadingStates.LOADING;

/**
 * Created by maximdrobonoh on 29.09.17.
 */

public class PlayersViewModel extends BaseStateViewModel<PlayerStats> {

    DataListFilterListener<Filter> filterListener;
    private DataListLoadingListener<PlayerStats> playerStatsDataListLoadingListener;

    public PlayersViewModel(Context context, DataListLoadingListener<PlayerStats> listener,
                            DataListFilterListener<Filter> filterListener) {
        super(context, listener);

        this.filterListener = filterListener;
        this.playerStatsDataListLoadingListener = listener;
    }

    @Override
    public void loadData(int page) {
    }

    public void loadData(Filter filter) {

        setState(LOADING);

        Subscriber<List<JsonObject>> subscriber = new Subscriber<List<JsonObject>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<JsonObject> playerStats) {

                List<PlayerStats> data = new ArrayList<>();

                for (JsonObject item : playerStats) {
                    try {

                        PlayerStats player = new PlayerStats();
                        player.setTotal(item.get(filter.getFieldName()).getAsInt());
                        player.setIcon(item.get("icon").getAsString());
                        player.setTeamName(item.get("team_name").getAsString());
                        player.setTeamIcon(item.get("team_icon").getAsString());
                        player.setName(item.get("name").getAsString());

                        if (player.getTotal() > 0)
                            data.add(player);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                Collections.sort(data, (playerStats1, t1) -> {
                    int compare = playerStats1.getTotal() - t1.getTotal();

                    compare *= -1;

                    return compare;
                });

                int number = 1;
                for (PlayerStats stats : data) {
                    stats.setPosition(number);

                    number++;
                }

                setState(DONE);
                playerStatsDataListLoadingListener.onLoaded(data);
            }
        };

        MatchDataRequestManager.getInstance().getPlayerStats(BaseTableFragment.getSelectedLeagueId(),
                filter.getFieldName()).subscribe(subscriber);
    }

    public void loadFilters() {

        Subscriber<List<Filter>> filterSubscriber = new Subscriber<List<Filter>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<Filter> teamFilters) {
                filterListener.onFilterLoaded(teamFilters);
            }
        };
        MatchDataRequestManager.getInstance().getPlayersFilters()
                .subscribe(filterSubscriber);
    }
}
