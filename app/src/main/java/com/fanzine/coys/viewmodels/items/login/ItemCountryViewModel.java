package com.fanzine.coys.viewmodels.items.login;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.fanzine.coys.models.ProEmail;
import com.fanzine.coys.models.login.Country;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 22.03.2017.
 */

public class ItemCountryViewModel extends BaseViewModel {

    public ObservableField<String> value1 = new ObservableField<>();
    public ObservableField<String> value2 = new ObservableField<>();
    public ObservableBoolean enableCheck = new ObservableBoolean(false);
    public ObservableBoolean checked = new ObservableBoolean(false);

    public ItemCountryViewModel(Context context, Country country) {
        super(context);

        this.value1.set(country.getNicename());
        this.value2.set(country.getPhonecodeText());
    }

    public ItemCountryViewModel(Context context, ProEmail email, boolean enableCheck, boolean checked) {
        super(context);

        this.value1.set(email.getEmail());
//        this.value2.set("£" + Integer.toString(email.getCost()));
        this.enableCheck.set(enableCheck);
        this.checked.set(checked);
    }

}
