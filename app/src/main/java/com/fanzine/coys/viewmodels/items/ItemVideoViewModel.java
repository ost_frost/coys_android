package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.fanzine.coys.R;
import com.fanzine.coys.models.Video;
import com.fanzine.coys.utils.DialogUtils;

/**
 * Created by Siarhei on 19.01.2017.
 */

public class ItemVideoViewModel extends SocialItemViewModel {

    public ObservableField<Video> videoItem = new ObservableField<>();
    public ObservableField<String> headerTitle = new ObservableField<>();

    public ItemVideoViewModel(Context context) {
        super(context);
    }

    public void setHeaderTitle(@NonNull String title) {
        headerTitle.set(title);
    }

    public void setVideoItem(Video videoItem) {
        this.videoItem.set(videoItem);
    }

    public void showVideo() {
        if (!TextUtils.isEmpty(videoItem.get().getUrl()) && getContext() != null)
            getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(videoItem.get().getUrl())));
        else
            DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
    }

    public String getPublishedAgo(@Nullable Video video) {
        if (video != null) {
            return video.getPublishedAgo();
        }
        return "";
    }
}
