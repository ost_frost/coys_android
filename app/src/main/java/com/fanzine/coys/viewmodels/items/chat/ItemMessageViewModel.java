package com.fanzine.coys.viewmodels.items.chat;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.fanzine.coys.viewmodels.base.BaseViewModel;
import com.fanzine.chat.ChatSDK;
import com.fanzine.chat.models.message.FCMessage;
import com.fanzine.chat.models.user.FCUser;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import static com.fanzine.chat.models.message.FCMessage.TYPE_DEFAULT;

/**
 * Created by Woland on 12.04.2017.
 */

public class ItemMessageViewModel extends BaseViewModel {

    private static DateTimeFormatter formatter = DateTimeFormat.forPattern("hh:mm a");

    public ObservableBoolean isSelf = new ObservableBoolean();
    public ObservableBoolean showTime = new ObservableBoolean();
    public ObservableBoolean isOneToOne = new ObservableBoolean();
    public ObservableBoolean isText = new ObservableBoolean();
    public ObservableField<FCMessage> message = new ObservableField<>();
    public ObservableField<FCUser> user = new ObservableField<>();
    public ObservableField<String> imageUrl = new ObservableField<>();

    public ItemMessageViewModel(Context context) {
        super(context);
    }

    public void init(FCMessage message, boolean isOneToOne) {
        isText.set(message.getType().equals(TYPE_DEFAULT));
        showTime.set(false);
        this.isOneToOne.set(isOneToOne);
        this.message.set(message);
        isSelf.set(message.getUser().equals(ChatSDK.getInstance().getCurrentSession().getCurrentUser().getUid()));
    }

    public String getTime(FCMessage message) {
        if (message != null)
            return new DateTime(message.getTimestamp()).toString(formatter);
        else
            return "";
    }

    public void showTime() {
        showTime.set(!showTime.get());
    }

    public String getUserName(FCUser user) {
        if (user != null)
            return user.getFirstName() + " " + user.getLastName();
        else
            return "";
    }

}
