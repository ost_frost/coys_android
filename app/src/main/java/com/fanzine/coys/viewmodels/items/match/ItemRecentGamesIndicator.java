package com.fanzine.coys.viewmodels.items.match;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by mbp on 11/2/17.
 */

public class ItemRecentGamesIndicator extends BaseViewModel {

    public ObservableField<String> title = new ObservableField<>();

    public ItemRecentGamesIndicator(Context context, String title) {
        super(context);
        this.title.set(title.substring(0,1));
    }
}
