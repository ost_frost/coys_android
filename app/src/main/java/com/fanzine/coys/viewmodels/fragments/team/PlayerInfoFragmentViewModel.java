package com.fanzine.coys.viewmodels.fragments.team;

import android.content.Context;
import android.databinding.ObservableField;
import android.text.Html;
import android.text.Spanned;

import com.fanzine.coys.models.team.PlayerInfo;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 23.02.2017.
 */

public class PlayerInfoFragmentViewModel extends BaseViewModel {

    public ObservableField<PlayerInfo> player = new ObservableField<>();

    public PlayerInfoFragmentViewModel(Context context) {
        super(context);
    }

    public Spanned getBio() {
        return Html.fromHtml(player.get().getBio());
    }

    public String valueOf(double value) {
        return value != 0 ? Double.toString(value) : "0";
    }

}
