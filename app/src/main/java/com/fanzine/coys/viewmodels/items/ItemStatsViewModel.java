package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.models.Stats;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Siarhei on 03.02.2017.
 */

public class ItemStatsViewModel extends BaseViewModel {

    public ObservableField<Stats> stats = new ObservableField<>();

    public ItemStatsViewModel(Context context, Stats stats) {
        super(context);
        this.stats.set(stats);
    }

    public void setStats(Stats stats) {
        this.stats.set(stats);
    }
}
