package com.fanzine.coys.viewmodels.dialogs;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.ObservableField;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.models.profile.Notifications;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Woland on 21.04.2017.
 */

public class NotificationsViewModel extends BaseViewModel {

    public ObservableField<Notifications> notif = new ObservableField<>();

    public int leagueId = -1;
    public int teamId = -1;

    public NotificationsViewModel(Context context) {
        super(context);
    }

    public void toggleGoals() {
        notif.get().setGoals(!notif.get().isGoals());
    }

    public void toggleKickOf() {
        notif.get().setKickOf(!notif.get().isKickOf());
    }

    public void toggleFullTime() {
        notif.get().setFullTime(!notif.get().isFullTime());
    }

    public void toggleHalfTime() {
        notif.get().setHalfTime(!notif.get().isHalfTime());
    }

    public void toggleRedCards() {
        notif.get().setRedCards(!notif.get().isRedCards());
    }

    public void saveNotification(DialogInterface dialogInterface) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(ResponseBody uid) {
                    pd.dismiss();
                    dialogInterface.dismiss();
                }
            };
            subscription.add(subscriber);

            notif.get().setLeagueId(leagueId);
            notif.get().setTeamId(teamId);

            ApiRequest.getInstance().getApi().setTeamsNotification(notif.get())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }
}
