package com.fanzine.coys.viewmodels.items.match;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.models.lineups.SidelinedPlayer;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by mbp on 10/20/17.
 */

public class ItemSidelinedPlayer extends BaseViewModel {

    public ObservableField<SidelinedPlayer> player = new ObservableField<>();

    public ItemSidelinedPlayer(Context context) {
        super(context);
    }

    public String getReturnDate() {
        return player.get().getEnddate().length() > 0?  player.get().getEnddate() : "Unknown";
    }
}
