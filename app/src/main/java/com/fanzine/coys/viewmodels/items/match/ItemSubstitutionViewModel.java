package com.fanzine.coys.viewmodels.items.match;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.models.lineups.Substitution;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 09.02.2017.
 */

public class ItemSubstitutionViewModel extends BaseViewModel {

    public ObservableField<Substitution> substitution = new ObservableField<>();

    public ItemSubstitutionViewModel(Context context, Substitution substitution) {
        super(context);
        this.substitution.set(substitution);
    }

    public boolean isOff() {
        return substitution.get().getOffName() != null;
    }

    public boolean isOnlyRedCard() {
        return substitution.get().getOnRedcards() > 0;
    }

    public boolean isOnlyYellowCard() {
        return substitution.get().getOnYellowCards() > 0;
    }

    public boolean isRedAndYellowCards() {
        return substitution.get().getOnRedcards() > 0 && substitution.get().getOnYellowCards() > 0;
    }
}
