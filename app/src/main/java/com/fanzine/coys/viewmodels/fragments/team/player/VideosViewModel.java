package com.fanzine.coys.viewmodels.fragments.team.player;

import android.content.Context;
import android.support.annotation.NonNull;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.Video;
import com.fanzine.coys.models.team.Player;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.fragments.BaseVideoViewModel;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.utils.LoadingStates.ERROR;

/**
 * Created by mbp on 1/12/18.
 */

public class VideosViewModel extends BaseVideoViewModel {

    private Player player;

    public VideosViewModel(Context context, DataListLoadingListener<Video> listener, Player player) {
        super(context, listener);
        this.player = player;
    }

    @Override
    public void loadData(int page) {

        if (NetworkUtils.isNetworkAvailable(getContext())) {
            Subscriber<List<Video>> subscriber = getSubscriber(page);

            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getTeamPlayerVideos(player.getId())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
            setState(ERROR);
        }
    }

    @NonNull
    public String getPlayerName() {
        if (player != null) {
            return player.getName();
        }
        return "";
    }
}
