package com.fanzine.coys.viewmodels.fragments.profile;

import android.content.Context;

import com.fanzine.coys.viewmodels.base.BaseViewModel;
import com.google.firebase.messaging.FirebaseMessaging;

/**
 * Created by mbp on 3/19/18.
 */

public abstract class NotificationViewModel extends BaseViewModel {


    public NotificationViewModel(Context context) {
        super(context);
    }

    public abstract String getTopic();

    public void subsribeOnNotifications() {
        FirebaseMessaging.getInstance().subscribeToTopic(getTopic());
    }

    public void unsubscribeOnNotifications() {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(getTopic());
    }
}
