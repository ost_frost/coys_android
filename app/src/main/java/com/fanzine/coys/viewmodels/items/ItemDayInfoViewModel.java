package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.fanzine.coys.R;
import com.fanzine.coys.models.DayInfo;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Siarhei on 13.01.2017.
 */

public class ItemDayInfoViewModel extends BaseViewModel {

    public ObservableField<DayInfo> dayInfo = new ObservableField<>();
    public ObservableBoolean isSelected = new ObservableBoolean();

    public ItemDayInfoViewModel(Context context, DayInfo dayInfo) {
        super(context);
        this.dayInfo.set(dayInfo);
    }

    public void setDayInfo(DayInfo dayInfo) {
        this.dayInfo.set(dayInfo);
    }

    public String getTitle(DayInfo dayInfo) {
        if (dayInfo.getNameDay().equals(getString(R.string.today)))
            return dayInfo.getNameDay();
        else
            return dayInfo.getDayOfMonth() + " " + dayInfo.getNameMonth();
    }

    public String getNameDay(DayInfo dayInfo) {
        String dayText = dayInfo.getNameDay();

        return dayText.substring(0, 1).toUpperCase() + dayText.substring(1);
    }

    public boolean isToday(DayInfo dayInfo) {
        return dayInfo.getNameDay().equals(getString(R.string.today));
    }
}
