package com.fanzine.coys.viewmodels.fragments.mails;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.R;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.networking.EmailDataRequestManager;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

import okhttp3.ResponseBody;
import rx.Subscriber;

/**
 * Created by Evgenij Krasilnikov on 13-Feb-18.
 */

public class MailNewFolderViewModel extends BaseViewModel {

    public ObservableField<String> folderName = new ObservableField<>();

    public MailNewFolderViewModel(Context context) {
        super(context);
    }

    public void createNewFolder() {
        if (NetworkUtils.isNetworkAvailable(getContext())) {

            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(ResponseBody body) {
                    pd.dismiss();
                    getActivity().finish();
                }
            };
            subscription.add(subscriber);

            EmailDataRequestManager.getInstanse().addMailbox(folderName.get()).subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }
}
