package com.fanzine.coys.viewmodels.dialogs;

import android.content.Context;
import android.content.DialogInterface;

import com.fanzine.coys.R;
import com.fanzine.coys.networking.EmailDataRequestManager;
import com.fanzine.coys.observables.MoveEmailObservable;
import com.fanzine.coys.subscribers.MoveEmailSubscriber;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 21.02.2017.
 */

public class ChangeFolderDialogViewModel extends BaseViewModel {

    private EmailDataRequestManager emailDataRequestManager;

    public ChangeFolderDialogViewModel(Context context) {
        super(context);

        this.emailDataRequestManager = EmailDataRequestManager.getInstanse();
    }

    public void moveMail(String folderFrom, int mailId, String folder, DialogInterface dialogInterface) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {

            MoveEmailObservable moveEmailObservable = new MoveEmailObservable(folder);

            MoveEmailSubscriber moveEmailSubscriber = new MoveEmailSubscriber(
                    getContext(),
                    dialogInterface,
                    folderFrom,
                    mailId);

            emailDataRequestManager
                    .move(folderFrom, folder, mailId)
                    .flatMap(moveEmailObservable)
                    .subscribe(moveEmailSubscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }
}
