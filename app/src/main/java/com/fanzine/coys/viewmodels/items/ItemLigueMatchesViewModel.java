package com.fanzine.coys.viewmodels.items;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.dialogs.MatchNotifications;
import com.fanzine.coys.interfaces.MatchNotificationListener;
import com.fanzine.coys.models.LigueMatches;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.models.matches.MatchNotification;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Siarhei on 13.01.2017.
 */

public class ItemLigueMatchesViewModel extends BaseViewModel {

    public interface IconChangeListener {
        void changed(boolean isActive);
    }

    public ObservableField<LigueMatches> ligueMatches = new ObservableField<>();

    public ObservableField<Boolean> isNotificationActive = new ObservableField<>();

    private IconChangeListener mIconChangeListener;

    public ItemLigueMatchesViewModel(Context context, LigueMatches ligueMatches, boolean isNotificationActive,
                                     IconChangeListener iconChangeListener) {
        super(context);
        this.ligueMatches.set(ligueMatches);
        this.isNotificationActive.set(isNotificationActive);
        this.mIconChangeListener = iconChangeListener;
    }

    public void setLigueMatches(LigueMatches ligueMatches) {
        this.ligueMatches.set(ligueMatches);
    }

    public void loadNotificationSettings(Match match) {
        ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

        ApiRequest.getInstance().getApi().getMatchNotification(match.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MatchNotification>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(MatchNotification matchNotification) {
                        pd.dismiss();
                        /*
                           *Disable notifications of specific match
                           *Configure notifications by league
                         */
                        matchNotification.setMatchIdNull();
                        matchNotification.setLeagueId(ligueMatches.get().getLeagueId());
                        matchNotification.setDate(ligueMatches.get().getDate());

                        MatchNotifications.show(getActivity().getSupportFragmentManager(), matchNotification, new MatchNotificationListener() {
                            @Override
                            public void turnOn() {
                                mIconChangeListener.changed(true);
                                isNotificationActive.set(true);
                            }

                            @Override
                            public void turnOff() {
                                mIconChangeListener.changed(false);
                                isNotificationActive.set(false);
                            }
                        });
                    }
                });
    }
}
