package com.fanzine.coys.viewmodels.fragments.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.interfaces.OnProfileLoader;
import com.fanzine.coys.models.login.Country;
import com.fanzine.coys.models.profile.Profile;
import com.fanzine.coys.models.profile.ProfileBody;
import com.fanzine.coys.models.profile.Terms;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

import java.util.HashMap;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by vitaliygerasymchuk on 2/10/18
 */

public class ProfileAccountViewModel extends BaseViewModel {

    public interface OnResetPasswordCallback {
        void onPasswordReset();
    }

    public interface OnTermsCallback {
        void onTerms(String termsString);
    }


    public ProfileAccountViewModel(Context context) {
        super(context);
    }

    public ObservableField<String> firstName = new ObservableField<>();
    public ObservableField<String> lastName = new ObservableField<>();
    public ObservableField<String> birthday = new ObservableField<>();
    public ObservableField<String> email = new ObservableField<>();
    public ObservableField<String> phoneCode = new ObservableField<>();
    public ObservableField<String> phone = new ObservableField<>();
    public ObservableField<String> cardLastNumbers = new ObservableField<>();
    public ObservableField<String> terms = new ObservableField<>();
    public ObservableField<Boolean> isCardEditable = new ObservableField<>();

    public boolean isVisible;

    private Profile profile;

    public void setProfile(Profile profile) {
        this.profile = profile;
        this.firstName.set(profile.getFirstName());
        this.lastName.set(profile.getLastName());
        this.birthday.set(profile.getBirthDayDate());
        this.email.set(profile.getEmail());
        this.phoneCode.set(profile.getPhoneCodeString());
        this.phone.set(profile.getPhone());
        this.cardLastNumbers.set(profile.getCardLastNumbers());
        this.isCardEditable.set(profile.getBraintreeId() != null);
    }

    public void setCountry(Country country) {
        this.profile.setPhonecode(country.getPhonecode());
        this.phoneCode.set(profile.getPhoneCodeString());
    }

    public void setTerms(String terms) {
        this.terms.set(terms);
    }

    public void setProfile(@NonNull ProfileBody profile, OnProfileLoader profileLoader) {
        doWithNetwork(() -> setProfileInternal(profile, profileLoader));
    }

    private void setProfileInternal(@NonNull ProfileBody profile, OnProfileLoader profileLoader) {
        ProgressDialog pd = DialogUtils.createProgressDialog(getContext(), R.string.please_wait);
        if (isVisible) showDialog(pd);
        Subscriber<Profile> subscriber = new Subscriber<Profile>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                dismissDialog(pd);

                FieldError errors = ErrorResponseParser.getErrors(e);
                if (errors.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                } else {
                    DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                }
            }

            @Override
            public void onNext(Profile profile) {
                profileLoader.onProfileLoaded(profile);
                dismissDialog(pd);
            }
        };

        subscription.add(subscriber);

        ApiRequest.getInstance().getApi().setProfile(profile)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    public void resetPassword(String oldPass, String newPass, String confirmPass, OnResetPasswordCallback callback) {
        doWithNetwork(() -> resetPasswordInternal(oldPass, newPass, confirmPass, callback));
    }

    private void resetPasswordInternal(String oldPass, String newPass, String confirmPass, OnResetPasswordCallback callback) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            final HashMap<String, String> params = new HashMap<>();
            params.put("old_password", oldPass);
            params.put("password", newPass);
            params.put("password_confirmation", confirmPass);

            Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(ResponseBody responseBody) {
                    callback.onPasswordReset();
                    pd.dismiss();
                }
            };

            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().resetPassword(params)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    public void getTermsOfUsage(OnTermsCallback callback) {
        doWithNetwork(() -> getTermsOfUsageInternal(callback));
    }

    private void getTermsOfUsageInternal(OnTermsCallback callback) {
        Subscriber<Terms> subscriber = new Subscriber<Terms>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

                FieldError errors = ErrorResponseParser.getErrors(e);
                if (errors.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                } else {
                    DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                }
            }

            @Override
            public void onNext(Terms responseBody) {
                callback.onTerms(responseBody.getTerms());
            }
        };

        subscription.add(subscriber);

        ApiRequest.getInstance().getApi().getTermsOfUsage()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}
