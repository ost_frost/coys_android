package com.fanzine.coys.viewmodels.items;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.dialogs.MatchNotifications;
import com.fanzine.coys.fragments.venue.VenueFragment;
import com.fanzine.coys.interfaces.MatchNotificationListener;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.models.Venue;
import com.fanzine.coys.models.matches.MatchNotification;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

import org.greenrobot.eventbus.EventBus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Siarhei on 13.01.2017.
 */
public class ItemMatchViewModel extends BaseViewModel {

    public interface OnViewChangeListener {
        void updateUI(Match match);
    }

    public ObservableBoolean isSelected = new ObservableBoolean();
    public int position;
    public int selectedPositions;
    public ObservableField<Match> match = new ObservableField<>();

    public ObservableField<Boolean> isNotificationActive = new ObservableField<>();

    public ItemMatchViewModel(Context context, Match match) {
        super(context);
        this.match.set(match);
        isNotificationActive.set(match.isHasNotification());
    }

    public void setMatch(Match match) {
        this.match.set(match);
    }

    public void setPosition(int position, int selectedPositions) {
        this.position = position;
        this.selectedPositions = selectedPositions;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected.set(isSelected);
    }

    public String getDate() {
        try {
            return getMonth(match.get().getDate());
        } catch (ParseException e) {
            return "";
        }
    }

    private String getMonth(String date) throws ParseException {
        Date d = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        String monthName = new SimpleDateFormat("dd MMM").format(cal.getTime());
        return monthName;
    }


    public void openNotificationSettings() {
        try {
            if (NetworkUtils.isNetworkAvailable(getContext())) {
                ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

                Subscriber<MatchNotification> subscriber = new Subscriber<MatchNotification>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        pd.dismiss();

                        FieldError errors = ErrorResponseParser.getErrors(e);
                        if (errors.isEmpty()) {
                            DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                        } else {
                            DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                        }
                    }

                    @Override
                    public void onNext(MatchNotification matchNotification) {
                        pd.dismiss();

                        MatchNotifications.show(getActivity().getSupportFragmentManager(), matchNotification, new MatchNotificationListener() {
                            @Override
                            public void turnOn() {
                                isNotificationActive.set(true);
                                match.get().setHasNotification(true);
                                EventBus.getDefault().post(match.get());
                            }

                            @Override
                            public void turnOff() {
                                isNotificationActive.set(false);
                                match.get().setHasNotification(false);
                                EventBus.getDefault().post(match.get());
                            }
                        });
                    }
                };
                subscription.add(subscriber);

                ApiRequest.getInstance().getApi().getMatchNotification(match.get().getId())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(subscriber);
            } else {
                DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openVenues() {
        Venue venue = match.get().getVenue();

        String venueAddress = venue.getName() + ", " + venue.getPostalCode();

        Intent startVenues = new Intent(getContext(), VenueFragment.class);
        startVenues.setAction(VenueFragment.ACTION_STADIUM_REQUEST);
        startVenues.putExtra(VenueFragment.VENUE_ADDRESS, venueAddress);

        getContext().startActivity(startVenues);
    }

    public String getTimeBegin(String time) {
        if (!TextUtils.isEmpty(time)) {
            return match.get().getTimeBeginWithLocaleTime();
        } else
            return "";
    }

    @android.databinding.BindingAdapter("app:srcCompat")
    public static void textColor(AppCompatImageView v, Boolean isNotification) {
        if (isNotification) {
            v.setImageResource(R.drawable.ic_bell_red);
        } else {
            v.setImageResource(R.drawable.ic_match_notification);
        }
    }
}
