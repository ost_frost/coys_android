package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.models.League;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Siarhei on 13.01.2017.
 */

public class ItemLeagueViewModel extends BaseViewModel {

    public ObservableField<League> league = new ObservableField<>();

    public ItemLeagueViewModel(Context context, League league) {
        super(context);
        this.league.set(league);
    }

    public void setLeague(League league) {
        this.league.set(league);
    }

    public String getTitle() {
        return league.get().getName();
    }
}
