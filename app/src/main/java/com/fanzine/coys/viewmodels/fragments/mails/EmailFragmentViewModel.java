package com.fanzine.coys.viewmodels.fragments.mails;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.db.factory.HelperFactory;
import com.fanzine.coys.models.mails.Label;
import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.models.mails.MailContent;
import com.fanzine.coys.networking.EmailDataRequestManager;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;
import com.j256.ormlite.stmt.DeleteBuilder;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.ref.WeakReference;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by Siarhei on 21.01.2017.
 */

public class EmailFragmentViewModel extends BaseViewModel {

    public ObservableField<String> toolbarTitle = new ObservableField<>();


    private final WeakReference<OnMailLoading> listener;

    public interface OnMailLoading {
        void onLoaded(MailContent mailContent, Mail mail);

        void onFlagged(boolean isFlagged);
    }

    public ObservableField<MailContent> mail = new ObservableField<>();
    private int position;
    private List<Mail> data = new ArrayList<>();

    private boolean isDeleted = false;

    public EmailFragmentViewModel(Context context, List<Mail> data, int position, OnMailLoading loading) {
        super(context);
        this.data.addAll(data);
        this.position = position;

        listener = new WeakReference<>(loading);
    }

    public void setMailContent(MailContent mailContent) {
        mail.set(mailContent);
    }

    public void previousMail() {
        if (data != null)
            if (NetworkUtils.isNetworkAvailable(getContext())) {
                ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

                Subscriber<MailContent> subscriber = new Subscriber<MailContent>() {

                    @Override
                    public void onCompleted() {
                        pd.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        pd.dismiss();

                        FieldError errors = ErrorResponseParser.getErrors(e);
                        if (errors.isEmpty()) {
                            DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                        } else {
                            DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                        }
                    }

                    @Override
                    public void onNext(MailContent mail) {
                        pd.dismiss();

                        isDeleted = false;

                        if (listener != null && listener.get() != null)
                            listener.get().onLoaded(mail, data.get(position));
                    }
                };
                subscription.add(subscriber);

                if (data.size() != 0) {
                    if (position > 0)
                        --position;
                    else
                        position = data.size() - 1;
                }

                ApiRequest.getInstance().getApi().getMailContent(data.get(position).getFolder(), data.get(position).getUid())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(subscriber);
            } else {
                DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
            }
    }

    public void nextMail() {
        if (data != null && data.size() > 0) {
            if (NetworkUtils.isNetworkAvailable(getContext())) {
                ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

                Subscriber<MailContent> subscriber = new Subscriber<MailContent>() {

                    @Override
                    public void onCompleted() {
                        pd.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        pd.dismiss();

                        FieldError errors = ErrorResponseParser.getErrors(e);
                        if (errors.isEmpty()) {
                            DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                        } else {
                            DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                        }
                    }

                    @Override
                    public void onNext(MailContent mail) {
                        pd.dismiss();

                        isDeleted = false;

                        if (listener != null && listener.get() != null)
                            listener.get().onLoaded(mail, data.get(position));
                    }
                };
                subscription.add(subscriber);

                if (!isDeleted) {
                    if (position < data.size() - 1)
                        ++position;
                    else
                        position = 0;
                }

                ApiRequest.getInstance().getApi().getMailContent(data.get(position).getFolder(), data.get(position).getUid())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(subscriber);
            } else {
                DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
            }
        } else {
            getActivity().onBackPressed();
            ;
        }
    }

    public void deleteLabel(Mail mail) {
        ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

        Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                pd.dismiss();

                FieldError errors = ErrorResponseParser.getErrors(e);

                if (errors.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                } else {
                    DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                }
            }

            @Override
            public void onNext(ResponseBody responseBody) {
                pd.dismiss();


                try {
                    DeleteBuilder<Label, Long> deleteBuilder =
                            HelperFactory.getHelper().getLabelDao().deleteBuilder();

                    deleteBuilder.where()
                            .eq(Label.API_LABEL_ID, mail.getFirstLabel().getApiLabelId())
                            .and()
                            .eq(Label.MAIL_ID, mail.getUid());
                    deleteBuilder.delete();

                } catch (SQLException e) {

                }

                mail.labels().clear();
            }
        };

        int labelId = mail.getFirstLabel().getApiLabelId();
        EmailDataRequestManager.getInstanse()
                .deleteLabelFromMail(mail.getFolder(), mail.getUid(), labelId)
                .subscribe(subscriber);
    }

    public void removeMail() {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {

                @Override
                public void onCompleted() {
                    pd.dismiss();
                }

                @Override
                public void onError(Throwable e) {
                    pd.dismiss();

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        getActivity().onBackPressed();
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(ResponseBody body) {
                    pd.dismiss();

                    if (data != null) {
                        Mail mail = data.get(position);
                        data.remove(position);

                        try {
                            HelperFactory.getHelper().getMailDao().deleteMail(mail.getFolder(), mail.getUid());
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        nextMail();
                    } else
                        getActivity().onBackPressed();
                }
            };
            subscription.add(subscriber);

            String folder;
            int id;
            if (data != null) {
                folder = data.get(position).getFolder();
                id = data.get(position).getUid();
            } else {
                folder = mail.get().getFolder();
                id = mail.get().getUid();
            }

            ApiRequest.getInstance().getApi().removeMail(folder, String.valueOf(id))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    public int getPosition() {
        return position;
    }

    public int getMailsCount() {
        return data.size();
    }

    public Mail getCurrentMail() {
        return data.get(position);
    }

    private DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("h:mm a, d MMM");

    public String getDate(MailContent mail) {
        return mail.getDateFormatted(dateTimeFormatter);
    }

    public void setReaded() {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(ResponseBody body) {

                }
            };

            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().readMail(data.get(position).getFolder(), data.get(position).getUid())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        }
    }

    public void setFlag() {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(ResponseBody body) {
                    mail.get().changeFlag();

                    try {
                        HelperFactory.getHelper().getMailDao().updateFlag(mail.get().getFolder(), mail.get().getUid(), mail.get().getFlagged() ? 1 : 0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (listener != null && listener.get() != null)
                        listener.get().onFlagged(mail.get().isFlagged());
                }
            };

            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().flagMail(data.get(position).getFolder(), data.get(position).getUid())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
    }

    public void setToolbarTitle(String title) {
        toolbarTitle.set(title);
    }
}
