package com.fanzine.coys.viewmodels.fragments.chat;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableField;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import com.fanzine.coys.R;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.ImageUtils;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;
import com.fanzine.chat.ChatSDK;
import com.fanzine.chat.interfaces.FCSuccessListener;
import com.fanzine.chat.models.channels.FCChannel;
import com.fanzine.chat.models.message.FCAttachment;
import com.fanzine.chat.models.message.FCMessage;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Woland on 12.04.2017.
 */

public class ChatFragmentViewModel extends BaseStateViewModel {

    public ObservableField<String> title = new ObservableField<>();

    private final FCChannel channel;
    public ObservableField<String> message = new ObservableField<>();

    public ChatFragmentViewModel(Context context, FCChannel channel) {
        super(context, null);

        this.channel = channel;
    }

    public void send() {
        if (!TextUtils.isEmpty(message.get())) {
            FCMessage message = new FCMessage(FCMessage.TYPE_DEFAULT, new Date().getTime(), this.message.get());
            ChatSDK.getInstance().getChatManager().sendMessage(channel, message);

            this.message.set("");
        }
    }

    public void attach(String data) {
        if (new File(data).exists()) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);
            FCMessage message = new FCMessage(FCMessage.TYPE_PICTURE, new Date().getTime(), this.message.get());
            List<FCAttachment> attachments = new ArrayList<>();
            attachments.add(new FCAttachment(getMimeType(data), new File(data)));
            message.setAttachments(attachments);
            ChatSDK.getInstance().getChatManager().sendMessageWhithAttachments(channel, message, attachments, new FCSuccessListener() {
                @Override
                public void onSuccess() {
                    pd.dismiss();
                    for (FCAttachment attachment : attachments)
                        ImageUtils.deleteImage(getContext(), attachment.getFile());
                }

                @Override
                public void onError(Exception ex) {
                    pd.dismiss();
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                }
            });
        }
        else
            DialogUtils.showAlertDialog(getContext(), R.string.thisFileNotExists);
    }

    private String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    @Override
    public void loadData(int page) {

    }
}
