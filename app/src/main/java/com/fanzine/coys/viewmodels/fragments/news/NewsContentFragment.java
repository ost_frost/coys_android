package com.fanzine.coys.viewmodels.fragments.news;

import android.databinding.ViewDataBinding;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.FragmentNewsContentBinding;
import com.fanzine.coys.databinding.IncludeNewsContentBinding;
import com.fanzine.coys.databinding.IncludeNewsImageBinding;
import com.fanzine.coys.databinding.IncludeToolbarBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.News;
import com.fanzine.coys.viewmodels.items.ItemNewsViewModel;

/**
 * Created by Woland on 12.01.2017.
 */

public class NewsContentFragment extends BaseFragment {

    private static final String NEWS = "news";

    public static NewsContentFragment newInstance(News news) {
        Bundle args = new Bundle();
        args.putParcelable(NEWS, news);
        NewsContentFragment fragment = new NewsContentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        FragmentNewsContentBinding mainBinding = FragmentNewsContentBinding.inflate(inflater, root, attachToRoot);

        News news = getArguments().getParcelable(NEWS);

        IncludeNewsContentBinding binding = IncludeNewsContentBinding.inflate(LayoutInflater.from(getContext()));
        binding.reviews.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        ItemNewsViewModel viewModel = new ItemNewsViewModel(getContext(), news);
        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);

        IncludeNewsImageBinding imageBinding = IncludeNewsImageBinding.inflate(inflater);
        imageBinding.setViewModel(viewModel);

        IncludeToolbarBinding toolbarBinding = IncludeToolbarBinding.inflate(inflater);

        setToolbar(toolbarBinding.toolbar);

        mainBinding.scrollView.setZoomView(imageBinding.getRoot());
        mainBinding.scrollView.setHeaderView(toolbarBinding.getRoot());
        mainBinding.scrollView.setHeaderLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) getResources().getDimension(R.dimen.newsPictureBigHeight)));
        mainBinding.scrollView.setScrollContentView(binding.getRoot());

        return mainBinding;
    }

    private void setToolbar(Toolbar toolbar) {
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(ContextCompat.getColor(getContext(), R.color.white), PorterDuff.Mode.SRC_ATOP);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }
}