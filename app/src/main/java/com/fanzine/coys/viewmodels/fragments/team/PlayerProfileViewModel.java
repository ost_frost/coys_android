package com.fanzine.coys.viewmodels.fragments.team;

import android.content.Context;
import android.databinding.ObservableField;
import android.util.Log;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.models.team.Player;
import com.fanzine.coys.models.team.PlayerProfile;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.models.team.PositionType.*;

/**
 * Created by mbp on 1/10/18.
 */

public class PlayerProfileViewModel extends BaseViewModel {

    public interface ProfileCallback {
        void onSuccess(PlayerProfile profile);

        void onError();
    }

    public ObservableField<PlayerProfile> playerProfile = new ObservableField<>();

    private Player player;
    private ProfileCallback profileCallback;

    public PlayerProfileViewModel(Context context, ProfileCallback profileCallback, Player player) {
        super(context);

        this.profileCallback = profileCallback;
        this.player = player;
    }

    public void setPlayerProfile(PlayerProfile playerProfile) {
        this.playerProfile.set(playerProfile);
    }

    public PlayerProfile.Header getHeader() {
        return playerProfile.get().getHeader();
    }

    public int getImage(PlayerProfile.Header header) {
        switch (header.position) {
            case ATTACKER:
                return R.drawable.team_position_attack;
            case DEFENDER:
                return R.drawable.team_position_defender;
            case MIDFIELDER:
                return R.drawable.team_position_mid;
            default:
                return R.drawable.team_position_goal;
        }
    }

    public void loadProfile() {
        ApiRequest.getInstance().getApi().getPlayerProfile(player.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PlayerProfile>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i(PlayerProfileViewModel.class.getName(), e.getMessage());

                        profileCallback.onError();
                    }

                    @Override
                    public void onNext(PlayerProfile profile) {
                        if (profile != null) {
                            profileCallback.onSuccess(profile);
                        } else {
                            profileCallback.onError();
                        }
                    }
                });
    }
}
