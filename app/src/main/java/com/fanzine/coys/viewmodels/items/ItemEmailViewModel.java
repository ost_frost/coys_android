package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.databinding.ObservableField;
import android.view.View;

import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.viewmodels.base.BaseViewModel;
import com.fanzine.mail.utils.TimeDateUtils;

/**
 * Created by Siarhei on 13.01.2017
 */

public class ItemEmailViewModel extends BaseViewModel {

    public ObservableField<Mail> mail = new ObservableField<>();

    public ItemEmailViewModel(Context context, Mail mail) {
        super(context);
        this.mail.set(mail);
    }

    public void setMail(Mail mail) {
        this.mail.set(mail);
    }

    public String getTime(Mail mail) {
        return TimeDateUtils.getTimeAgo(mail.getDateTime().getMillis());
    }

    public String getLabelName() {
        return mail.get().getLabelName();
    }

    public int labelVisibility() {
        return mail.get().hasLabel() ? View.VISIBLE : View.INVISIBLE;
    }

    public int unreadVisibility() {
        return !mail.get().isRead() ? View.VISIBLE : View.GONE;
    }
}
