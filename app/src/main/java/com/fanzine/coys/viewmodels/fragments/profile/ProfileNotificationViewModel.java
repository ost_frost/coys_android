package com.fanzine.coys.viewmodels.fragments.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.models.profile.League;
import com.fanzine.coys.models.profile.LeagueType;
import com.fanzine.coys.models.profile.Notifications;
import com.fanzine.coys.models.profile.Team;
import com.fanzine.coys.services.MyFirebaseMessagingService;
import com.fanzine.coys.utils.DialogUtils;

import java.util.Collections;
import java.util.List;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by vitaliygerasymchuk on 2/12/18
 */

public class ProfileNotificationViewModel extends NotificationViewModel {

    public interface Callback {
        void onLeagueTypes(@NonNull List<LeagueType> types);
    }

    @NonNull
    public ObservableField<String> headerTitle = new ObservableField<>();

    @Nullable
    private LeagueType leagueType;

    @Nullable
    private League league;

    @Nullable
    private Team team;

    @Override
    public String getTopic() {
        if (team != null)
            return MyFirebaseMessagingService.Types.TEAM_EVENT + team.getId();

        //todo:: throw exception
        return "";
    }

    public boolean isVisible;

    public void setHeaderTitle(@NonNull String headerTitle) {
        this.headerTitle.set(headerTitle);
    }

    public void setLeagueType(@NonNull LeagueType leagueType) {
        this.leagueType = leagueType;
        setHeaderTitle(leagueType.getName());
    }

    public void setLeague(@NonNull League league) {
        this.league = league;
        setHeaderTitle(league.getName());
    }

    public void setTeam(@NonNull Team team) {
        this.team = team;
    }

    public ProfileNotificationViewModel(Context context) {
        super(context);
    }

    public void getLeaguesAndTeams(@NonNull Callback callback) {
        doWithNetwork(() -> getLeaguesAndTeamsInternal(callback));
    }

    private void getLeaguesAndTeamsInternal(@NonNull Callback callback) {
        ProgressDialog pd = DialogUtils.createProgressDialog(getContext(), R.string.please_wait);
        if (isVisible) showDialog(pd);
        Subscriber<List<LeagueType>> subscriber = new Subscriber<List<LeagueType>>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                dismissDialog(pd);

                FieldError errors = ErrorResponseParser.getErrors(e);
                if (errors.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                } else {
                    DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                }
            }

            @Override
            public void onNext(List<LeagueType> body) {
                Collections.sort(body);
                callback.onLeagueTypes(body);
                dismissDialog(pd);
            }
        };

        subscription.add(subscriber);

        ApiRequest.getInstance().getApi().getProfileNotificationsTeams()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    public void setTeamNotifications(@NonNull Notifications notifications) {
        doWithNetwork(() -> setTeamNotificationsInternal(notifications));
    }

    private void setTeamNotificationsInternal(@NonNull Notifications notifications) {

        if (league == null || team == null) {
            DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
            return;
        }

        notifications.setLeagueId(league.getLeagueId());
        notifications.setTeamId(team.getId());
        ProgressDialog pd = DialogUtils.createProgressDialog(getContext(), R.string.please_wait);
        if (isVisible) showDialog(pd);
        Subscriber<ResponseBody> subscriber = new Subscriber<ResponseBody>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                dismissDialog(pd);

                FieldError errors = ErrorResponseParser.getErrors(e);
                if (errors.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                } else {
                    DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                }
            }

            @Override
            public void onNext(ResponseBody body) {
                dismissDialog(pd);
            }
        };

        subscription.add(subscriber);

        ApiRequest.getInstance().getApi().setProfileNotificationsTeams(notifications)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}
