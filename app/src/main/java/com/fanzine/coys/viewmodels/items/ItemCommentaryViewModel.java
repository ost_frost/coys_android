package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.fanzine.coys.models.MatchComment;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

import java.util.Locale;

/**
 * Created by Siarhei on 17.01.2017.
 */

public class ItemCommentaryViewModel extends BaseViewModel{

    public ObservableBoolean isLast = new ObservableBoolean();
    public ObservableField<MatchComment> matchComment = new ObservableField<>();

    public ItemCommentaryViewModel(Context context, boolean isLast) {
        super(context);
        this.isLast.set(isLast);
    }

    public void setLast(boolean isLast) {
        this.isLast.set(isLast);
    }

    public void init(MatchComment matchComment) {
        this.matchComment.set(matchComment);
    }

    public String getMinute(MatchComment comment) {
        if ( isInteger(comment.getMinute())) {
            return String.format(Locale.ENGLISH, "%d'", Integer.parseInt(comment.getMinute()));
        }
        return comment.getMinute();
    }

    private static boolean isInteger(String s) {
        return isInteger(s,10);
    }

    private static boolean isInteger(String s, int radix) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i),radix) < 0) return false;
        }
        return true;
    }
}
