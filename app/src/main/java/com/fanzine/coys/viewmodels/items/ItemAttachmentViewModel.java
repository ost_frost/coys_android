package com.fanzine.coys.viewmodels.items;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.models.mails.Attachment;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by maximdrobonoh on 08.09.17.
 */

public class ItemAttachmentViewModel extends BaseViewModel {

    public ObservableField<Attachment> attachment = new ObservableField<>();

    public ItemAttachmentViewModel(Context context, Attachment attachment) {
        super(context);

        this.attachment.set(attachment);
    }

    public void setAttachment(Attachment attachment)
    {
        this.attachment.set(attachment);
    }
}