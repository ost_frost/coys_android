package com.fanzine.coys.viewmodels.fragments.match;

import android.content.Context;
import android.databinding.ObservableField;

import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.interfaces.PredictionResult;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.models.analysis.Analysis;
import com.fanzine.coys.models.analysis.Prediction;
import com.fanzine.coys.models.analysis.PredictonDiagram;
import com.fanzine.coys.networking.MatchDataRequestManager;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;

import rx.Subscriber;

import static com.fanzine.coys.utils.LoadingStates.DONE;
import static com.fanzine.coys.utils.LoadingStates.LOADING;

/**
 * Created by maximdrobonoh on 05.10.17.
 */

public class AnalysisFragmentViewModel extends BaseStateViewModel<Analysis> {

    public interface DiagramTitle {
        String HOME = "local";
        String DRAW = "draw";
        String AWAY = "visitor";
    }

    private int matchId;
    private PredictionResult predictionResultListener;

    public ObservableField<Analysis> analysis = new ObservableField<>();

    public ObservableField<Match> match = new ObservableField<>();

    public ObservableField<PredictonDiagram> homeDiagram = new ObservableField<>();
    public ObservableField<PredictonDiagram> drawDiagram = new ObservableField<>();
    public ObservableField<PredictonDiagram> awayDiagram = new ObservableField<>();
    public ObservableField<Boolean> isUserVoted = new ObservableField<>();
    public ObservableField<String> voteTotal = new ObservableField<>();


    public AnalysisFragmentViewModel(Context context, DataListLoadingListener<Analysis> listener,
                                     Match match, PredictionResult predictionResultListener) {
        super(context, listener);

        this.matchId = match.getId();
        this.match.set(match);
        this.predictionResultListener = predictionResultListener;
        this.isUserVoted.set(false);
    }

    @Override
    public void loadData(int page) {

        Subscriber<Analysis> subscriber = new Subscriber<Analysis>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(Analysis analysis) {
                setState(DONE);

                getListener().onLoaded(analysis);

                long allVotes = analysis.getPrediction().getAllVotes();
                setVoteTotal(allVotes);
            }
        };

        MatchDataRequestManager.getInstance()
                .getAnalysis(matchId)
                .subscribe(subscriber);
    }

    public ObservableField<PredictonDiagram> getHomeDiagram() {
        return homeDiagram;
    }
    public ObservableField<PredictonDiagram> getDrawDiagram() {
        return drawDiagram;
    }
    public ObservableField<PredictonDiagram> getAwayDiagram() {
        return awayDiagram;
    }

    public void setIsUserVoted(boolean isVoted) {
        this.isUserVoted.set(isVoted);
    }


    public void castVote(String vote) {

        Subscriber<Prediction> subscriber = new Subscriber<Prediction>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Prediction prediction) {
                setState(DONE);
                predictionResultListener.showDiagram(prediction);
                predictionResultListener.showVotedLabel(prediction);

                setIsUserVoted(true);

                setData(prediction);
            }
        };

        setState(LOADING);
        MatchDataRequestManager.getInstance()
                .castVote(matchId, vote)
                .subscribe(subscriber);
    }

    public void setData(Prediction prediction) {
        homeDiagram.set(new PredictonDiagram(prediction.getHomeProcent(),
                prediction.getVoteHome(), isVoted(prediction.getUserVote())));

        drawDiagram.set(new PredictonDiagram(prediction.getDrafProcent(),
                prediction.getVoteDraw(), isVoted(prediction.getUserVote())));

        awayDiagram.set(new PredictonDiagram(prediction.getAwayProcent(),
                prediction.getVoteAway(), isVoted(prediction.getUserVote())));

        setVoteTotal(prediction.getAllVotes());
    }

    private boolean isVoted(String userVote) {
        return userVote == DiagramTitle.AWAY
                || userVote == DiagramTitle.DRAW
                || userVote == DiagramTitle.HOME;
    }

    private void setVoteTotal(long count) {
        String countStr = String.valueOf(count);
        voteTotal.set("(" + countStr + " votes)");
    }
}
