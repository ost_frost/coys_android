package com.fanzine.coys.viewmodels.fragments.login;

import android.app.ProgressDialog;
import android.content.Context;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.MainActivity;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.FirebaseApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.interfaces.CountriesConsumer;
import com.fanzine.coys.models.login.UserToken;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.helpers.Constants.FIREBASE_KEY;

/**
 * Created by mbp on 3/20/18.
 */

public class AuthViewModel extends BaseViewModel {

    protected ProgressDialog pd;

    AuthViewModel(Context context) {
        super(context);
        pd = buildProgressDialog();
    }

    public void auth(Observable<UserToken> tokenObservable) {
        showDialog(pd);

        Subscriber<UserToken> tokenSubscriber = getTokenSubscriber();
        subscription.add(tokenSubscriber);

        tokenObservable
                .flatMap(saveApiToken())
                .flatMap(requestFirebaseToken())
                .flatMap(saveFirebaseToken())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tokenSubscriber);
    }

    public void fetchCountries(CountriesConsumer countriesConsumer) {
        showDialog(pd);
        ApiRequest.getInstance().getApi().getCountries()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(countries -> {
                    dismissDialog(pd);
                    countriesConsumer.accept(countries);
                }, throwable -> {
                    dismissDialog(pd);
                });
    }

    private Func1<UserToken, Observable<UserToken>> saveApiToken() {
        return userToken -> {
            if (userToken.isValid()) {
                SharedPrefs.saveUserToken(userToken);
                return Observable.just(userToken);
            }
            return Observable.error(new Exception(userToken.getError()));
        };
    }

    private Func1<UserToken, Observable<UserToken>> requestFirebaseToken() {
        return userToken ->
                FirebaseApiRequest.getInstance().getApi().getToken(FIREBASE_KEY, userToken.getUserId());
    }

    private Func1<UserToken, Observable<UserToken>> saveFirebaseToken() {
        return userToken -> {
            if (userToken.isValid()) {
                SharedPrefs.saveFIRToken(userToken.getToken());

                return Observable.just(userToken);
            }
            return Observable.error(new Exception(userToken.getError()));
        };
    }

    private void showError(Throwable e) {
        e.printStackTrace();
        List<FieldError> errors = ErrorResponseParser.getErrorList(e);
        if (errors == null || errors.isEmpty()) {
            DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
        } else {
            DialogUtils.showAlertDialog(getContext(), errors.get(0).getMessage());
        }
    }

    private Subscriber<UserToken> getTokenSubscriber() {
        return new Subscriber<UserToken>() {
            @Override
            public void onCompleted() {
                dismissDialog(pd);
            }

            @Override
            public void onError(Throwable e) {
                dismissDialog(pd);

                showError(e);
            }

            @Override
            public void onNext(UserToken userToken) {
                dismissDialog(pd);

                if (userToken.isValid()) {
                    getContext().startActivity(MainActivity.getStartIntent(getContext()));
                    getActivity().finish();
                }
            }
        };
    }
}
