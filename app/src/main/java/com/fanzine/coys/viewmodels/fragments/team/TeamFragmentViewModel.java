package com.fanzine.coys.viewmodels.fragments.team;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.fragments.team.PlayerFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.team.Player;
import com.fanzine.coys.models.team.PlayerInfo;
import com.fanzine.coys.models.team.TeamSquad;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.base.BaseStateViewModel;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.utils.LoadingStates.DONE;
import static com.fanzine.coys.utils.LoadingStates.ERROR;
import static com.fanzine.coys.utils.LoadingStates.NO_DATA;

/**
 * Created by Woland on 22.02.2017.
 */

public class TeamFragmentViewModel extends BaseStateViewModel<Player> {

    private TeamSquad teamSquad;

    public TeamFragmentViewModel(Context context, DataListLoadingListener<Player> listener, TeamSquad teamSquad) {
        super(context, listener);

        this.teamSquad = teamSquad;
    }

    public void setTeamSquad(TeamSquad teamSquad) {
        this.teamSquad = teamSquad;
    }

    @Override
    public void loadData(int page) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {

            Subscriber<List<JsonObject>> subscriber = new Subscriber<List<JsonObject>>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    setState(ERROR);
                    e.printStackTrace();
                    getListener().onError();
                }

                @Override
                public void onNext(List<JsonObject> data) {
                    List<Player> players = new ArrayList<>();

                    Gson gson = new Gson();
                    Type type = new TypeToken<Player>(){}.getType();

                    for (JsonObject item : data) {
                        try {
                            Player player = gson.fromJson(item, type);
                            players.add(player);
                        } catch (Exception e) {
                            Log.i(TeamFragmentViewModel.class.getName(), e.getMessage());
                        }
                    }

                    if (players.size() > 0) {
                        getListener().onLoaded(players);
                        setState(DONE);
                    } else {
                        setState(NO_DATA);
                        getListener().onError();
                    }
                }
            };
            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getPlayers(teamSquad.getValue())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);

            setState(ERROR);
        }
    }

    public void loadPlayer(int id) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);
            Subscriber<PlayerInfo> subscriber = new Subscriber<PlayerInfo>() {

                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    e.printStackTrace();
                    pd.dismiss();

                    FieldError errors = ErrorResponseParser.getErrors(e);
                    if (errors.isEmpty()) {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                    }
                }

                @Override
                public void onNext(PlayerInfo data) {
                    pd.dismiss();
                    FragmentUtils.changeFragment(getActivity(), R.id.content_frame, PlayerFragment.newInstance(data), true);
                }
            };
            subscription.add(subscriber);

            ApiRequest.getInstance().getApi().getPlayerInfo(id)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber);
        } else
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
    }
}
