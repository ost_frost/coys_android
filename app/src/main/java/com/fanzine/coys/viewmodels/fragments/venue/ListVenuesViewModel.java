package com.fanzine.coys.viewmodels.fragments.venue;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableField;
import android.net.Uri;
import android.support.v4.app.FragmentManager;

import com.fanzine.coys.R;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.fragments.GoogleMapBaseViewModel;
import com.google.android.gms.location.places.PlaceBuffer;
import com.yelp.fusion.client.connection.YelpFusionApi;
import com.yelp.fusion.client.connection.YelpFusionApiFactory;
import com.yelp.fusion.client.exception.exceptions.UnexpectedAPIError;
import com.yelp.fusion.client.models.Business;
import com.yelp.fusion.client.models.Review;
import com.yelp.fusion.client.models.Reviews;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * Created by Woland on 15.03.2017.
 */

public class ListVenuesViewModel extends GoogleMapBaseViewModel {

    public interface OnVenueLoadingListener {
        void onVenueDetailLoaded(ArrayList<Review> reviews, Business business);

        void openWebsite(Uri uri);

        void websiteNotFound();
    }

    public ObservableField<Boolean> isNotDataStatusVisible = new ObservableField<>();

    private FragmentManager fragmentManager;

    private OnVenueLoadingListener onVenueLoadingListener;


    private ProgressDialog mPendingDialog;

    public ListVenuesViewModel(Context context, FragmentManager fragmentManager, OnVenueLoadingListener onVenueLoadingListener) {
        super(context);

        isNotDataStatusVisible.set(false);
        this.onVenueLoadingListener = onVenueLoadingListener;
        this.fragmentManager = fragmentManager;
    }

    public void setIsNotDataStatusVisible(boolean isVisible) {
        this.isNotDataStatusVisible.set(isVisible);
    }

    @Override
    protected void onPlacesLoaded(PlaceBuffer places)
    {
        Uri uri = places.get(0).getWebsiteUri();
        if (uri != null)
            onVenueLoadingListener.openWebsite(uri);
        else
            onVenueLoadingListener.websiteNotFound();
    }

    @Override
    protected void onPlacesLoadError() {
        onVenueLoadingListener.websiteNotFound();
    }

    public void loadVenues(String id) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {

            ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

            YelpFusionApiFactory apiFactory = new YelpFusionApiFactory();

            Observable.fromCallable(() -> apiFactory.createAPI("nWMgOmM0oQj0mf1oVdHUNg", "o3HrKq0YZynDU1rJz70E7ftDFwJra2YMEqBVEzr1U2RhlSy91ZfXtRazkUNRKA6J"))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(Schedulers.newThread())      // subscriber on different thread
                    .subscribe(new Subscriber<YelpFusionApi>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            getActivity().runOnUiThread(() -> {
                                if (pd.isShowing())
                                    pd.dismiss();
                                DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                            });
                        }

                        @Override
                        public void onNext(YelpFusionApi yelpAPI) {
                            Call<Business> call = yelpAPI.getBusiness(id);

                            call.enqueue(new Callback<Business>() {
                                @Override
                                public void onResponse(Call<Business> call, Response<Business> responseBus) {
                                    if (responseBus.body().getReviewCount() > 0)
                                        loadReviews(pd, yelpAPI, responseBus.body());
                                    else {
                                        onVenueLoadingListener.onVenueDetailLoaded(new ArrayList<>(), responseBus.body());
                                    }
                                }

                                @Override
                                public void onFailure(Call<Business> call, Throwable t) {
                                    getActivity().runOnUiThread(() -> {
                                        if (pd.isShowing())
                                            pd.dismiss();
                                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                                    });
                                }
                            });
                        }
                    });

        } else {
            DialogUtils.showAlertDialog(getContext(), R.string.checkInetConnection);
        }
    }

    private void loadReviews(ProgressDialog pd, YelpFusionApi yelpAPI, Business business) {
        Call<Reviews> reviewsCall = yelpAPI.getBusinessReviews(business.getId(), "en_GB");

        reviewsCall.enqueue(new Callback<Reviews>() {
            @Override
            public void onResponse(Call<Reviews> call, Response<Reviews> response) {
                if (pd.isShowing())
                    pd.dismiss();

                onVenueLoadingListener.onVenueDetailLoaded(response.body().getReviews(), business);
            }

            @Override
            public void onFailure(Call<Reviews> call, Throwable t) {
                getActivity().runOnUiThread(() -> {
                    if (pd.isShowing())
                        pd.dismiss();

                    if (t instanceof UnexpectedAPIError && ((UnexpectedAPIError) t).getCode().equals("BUSINESS_UNAVAILABLE")) {

                        onVenueLoadingListener.onVenueDetailLoaded(new ArrayList<>(), business);
                    } else {
                        DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                    }
                });
            }
        });
    }

}
