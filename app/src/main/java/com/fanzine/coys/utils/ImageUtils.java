package com.fanzine.coys.utils;

import android.content.Context;
import android.provider.MediaStore;

import java.io.File;

/**
 * Created by Woland on 25.08.2016.
 */
public class ImageUtils {

    public static void deleteImage(Context context, File file) {
        context.getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                MediaStore.MediaColumns.DATA + "='" + file.getAbsolutePath() + "'", null);
        file.delete();
    }

}
