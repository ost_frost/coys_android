package com.fanzine.coys.utils;

/**
 * Created by Woland on 24.01.2017.
 */

public class LoadingStates {

    public static int LOADING = 0x1;
    public static int ERROR = 0x2;
    public static int DONE = 0x0;
    public static int NO_DATA = 0x3;
    public static int NO_DATA_EMPTY = 0x4;

}
