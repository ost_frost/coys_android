package com.fanzine.coys.utils;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

/**
 * Created by user on 09.11.2017.
 */

public class WeeksLayoutManager extends LinearLayoutManager {
    private int mParentWidth;
    private int mItemWidth;

    public WeeksLayoutManager(Context context, int parentWidth, int itemWidth) {
        super(context);
        mParentWidth = parentWidth;
        mItemWidth = itemWidth;
    }

    @Override
    public int getPaddingLeft() {
        return Math.round(mParentWidth / 2f - mItemWidth / 2f);
    }

    @Override
    public int getPaddingRight() {
        return getPaddingLeft();
    }
}
