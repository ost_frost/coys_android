package com.fanzine.coys.utils;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;


public class FragmentUtils {

    public static final String FRAGMENT_TAG = "Content";

    public static void changeFragment(FragmentActivity activity, int contentFrame,
                                      Fragment fr, boolean addToBackStack) {
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        ft.replace(contentFrame, fr, FRAGMENT_TAG);
        if (addToBackStack) {
            ft.addToBackStack(null);
        }
        else
            popBackStack(activity);

        ft.commitAllowingStateLoss();
    }

    public static void changeFragment(FragmentManager fm, int contentFrame,
                                      Fragment fr, boolean addToBackStack) {
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(contentFrame, fr, FRAGMENT_TAG);
        if (addToBackStack) {
            ft.addToBackStack(null);
        }

        ft.commitAllowingStateLoss();
    }

    public static Fragment getCurrent(FragmentActivity activity) {
        FragmentManager fm = activity.getSupportFragmentManager();
        return fm.findFragmentByTag(FRAGMENT_TAG);
    }

    public static void popBackStack(FragmentActivity activity) {
        FragmentManager fm = activity.getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }
}
