package com.fanzine.coys.utils;

import com.fanzine.coys.models.Stats;

import java.util.List;

/**
 * Created by maximdrobonoh on 12.10.17.
 */

public class CollectionUtil {

    public static Stats findStatByTitle(String title, List<Stats> statsList) {

        for (Stats stats : statsList) {
            if (stats.compareTitles(title)) {
                return stats;
            }
        }
        return null;
    }
}
