package com.fanzine.coys.utils;

import android.content.Context;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.v4.provider.DocumentFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

/**
 * Created by maximdrobonoh on 11.09.17.
 */

public class FileUtils {
    public static String IMAGE_TYPE = "image/";
    public static String VIDEO_TYPE = "video/";
    public static String AUDIO_TYPE = "audio/";
    public static String TEXT_TYPE = "text/";
    public static String APPLICATION_TYPE = "application/";
    public static String APPLICATION_PDF = "application/pdf";

    public static String DOWNLOADS_CONTENT_URI = "content://downloads/public_downloads";

    public static String PRIMARY_TYPE = "primary";
    public static String IMAGES = "images";
    public static String IMAGE = "image";
    public static String VIDEO = "video";
    public static String PDF_EXTENSION = "pdf";

    public static String FOLDER_SEPARATOR = "/";

    public static String ID_COLUMN_VALUE = "_id";
    public static String DATA_COLUMN_VALUE = "_data";

    public static String READ_MODE = "r";

    public static int FALSE_SIZE = -1;

    public static String CONTENT = "content";
    public static String MEDIA_AUTHORITY = "media";
    public static String FILE = "file";

    public static String DOWNLOADS_DIRECTORY_AUTHORITY = "com.android.providers.downloads.documents";
    public static String EXTERNAL_STORAGE_AUTHORITY = "com.android.externalstorage.documents";
    public static String MEDIA_DOCUMENTS_AUTHORITY = "com.android.providers.media.documents";

    public static File fileFromUri(Context context, Uri data) throws Exception {
        DocumentFile file = DocumentFile.fromSingleUri(context,data);
        String fileType = file.getType();
        String fileName = file.getName();
        File fileCreated;
        ParcelFileDescriptor parcelFileDescriptor =
                context.getContentResolver().openFileDescriptor(data, READ_MODE);
        InputStream inputStream = null;

        if (parcelFileDescriptor != null) {
            inputStream = new FileInputStream(parcelFileDescriptor.getFileDescriptor());
        }

        String filePath = context.getExternalCacheDir()
                + FOLDER_SEPARATOR
                + fileName;
        String fileExtension = fileName.substring((fileName.lastIndexOf('.')) + 1);
        String mimeType = getMimeType(fileName);


        if (fileType.equals(APPLICATION_PDF)
                && mimeType == null) {
            filePath += "." +PDF_EXTENSION;
        }

        if(!createFile(filePath)) {
            return new File(filePath);
        }

        ReadableByteChannel from = Channels.newChannel(inputStream);
        WritableByteChannel to = Channels.newChannel(new FileOutputStream(filePath));
        fastChannelCopy(from, to);
        from.close();
        to.close();
        fileCreated = new File(filePath);

        return fileCreated;
    }

    public static String getMimeType(String fileName) {
        return URLConnection.guessContentTypeFromName(fileName);
    }

    private static void fastChannelCopy(final ReadableByteChannel src,
                                        final WritableByteChannel dest) throws IOException {
        final ByteBuffer buffer = ByteBuffer.allocateDirect(16 * 1024);
        while (src.read(buffer) != -1) {
            buffer.flip();
            dest.write(buffer);
            buffer.compact();
        }
        buffer.flip();
        while (buffer.hasRemaining()) {
            dest.write(buffer);
        }
    }

    public static String getFileExtension(File file) {
        String name = file.getName();
        try {
            return name.substring(name.lastIndexOf(".") + 1);
        } catch (Exception e) {
            return "";
        }
    }

    public static String readTermsAndPolicy(Context context) {
        StringBuilder contentBuilder = new StringBuilder();

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open("terms_policy.txt")));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                contentBuilder.append(mLine);
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        return contentBuilder.toString();
    }

    private static boolean createFile(String path) throws IOException {
        File temp = new File(path);

        return temp.exists() || temp.createNewFile();
    }
}
