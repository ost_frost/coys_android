package com.fanzine.coys.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.Toast;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.fanzine.mail.MailComposeNewActivity;
import com.fanzine.coys.models.News;
import com.fanzine.coys.models.Video;
import com.fanzine.chat3.view.DialogsListActivity;

/**
 * Created by vitaliygerasymchuk on 3/11/18
 */

public class ShareUtil {
    private static final String CLIPBOARD_COPIED_LABEL = "GunnersClipboardCopy";
    private static final String TWITTER_SHARE = "https://twitter.com/intent/tweet?text=";

    //***************************************** NEWS ***********************************************

    public static void shareNewsToFacebook(@Nullable Activity activity, @Nullable News news) {
        if (activity != null && news != null) {
            shareFacebook(activity, news.getTitle(), news.getContent(), news.getUrl(), news.getImage());
        }
    }

    public static void shareNewsToChat(@Nullable Context context, @Nullable News news) {
        if (news != null) {
            shareToChat(context, news.getUrl());
        }
    }

    public static void shareNewsToMail(@Nullable Context context, @Nullable News news) {
        if (news != null) {
            shareToMail(context, news.getUrl());
        }
    }

    public static void shareNewsToTwitter(@Nullable Context context, @Nullable News news) {
        if (news != null) {
            shareToTwitter(context, news.getUrl());
        }
    }

    public static void copyToClipboardNews(@Nullable Context context, @Nullable News news) {
        if (news != null) {
            copyToClipboard(context, news.getUrl());
        }
    }

    //***************************************** VIDEO **********************************************
    public static void shareVideoToFacebook(@Nullable Activity activity, @Nullable Video video) {
        if (activity != null && video != null) {
            shareFacebook(activity, video.getTitle(), video.getChannelTitle(), video.getUrl(), video.getPreviewImage());
        }
    }

    public static void shareVideoToChat(@Nullable Context context, @Nullable Video video) {
        if (video != null) {
            shareToChat(context, video.getUrl());
        }
    }

    public static void shareVideoToMail(@Nullable Context context, @Nullable Video video) {
        if (video != null) {
            shareToMail(context, video.getUrl());
        }
    }

    public static void shareVideoToTwitter(@Nullable Context context, @Nullable Video video) {
        if (video != null) {
            shareToTwitter(context, video.getUrl());
        }
    }

    public static void copyToClipboardVideo(@Nullable Context context, @Nullable Video video) {
        if (video != null) {
            copyToClipboard(context, video.getUrl());
        }
    }

    private static void shareToMail(@Nullable Context context, @NonNull String url) {
        if (context != null) {
            MailComposeNewActivity.launch(context, url);
        }
    }

    private static void shareToChat(@Nullable Context context, @NonNull String url) {
        if (context != null) {
            DialogsListActivity.launch(context, url);
        }
    }

    private static void shareFacebook(@NonNull Activity activity,
                                      @Nullable String title,
                                      @Nullable String content,
                                      @Nullable String url,
                                      @Nullable String image) {

        if (!TextUtils.isEmpty(url)) {
            ShareLinkContent shareLinkContent = new ShareLinkContent.Builder()
                    .setContentTitle(title)
                    .setContentDescription(content)
                    .setContentUrl(Uri.parse(url))
                    .setImageUrl(Uri.parse(image))
                    .build();

            ShareDialog.show(activity, shareLinkContent);
        }
    }

    private static void shareToTwitter(@Nullable Context context, @NonNull String url) {
        if (context != null) {
            String tweetUrl = TWITTER_SHARE + url;
            Uri uri = Uri.parse(tweetUrl);
            context.startActivity(new Intent(Intent.ACTION_VIEW, uri));
        }
    }

    private static void copyToClipboard(@Nullable Context context, @NonNull String text) {
        if (context != null) {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText(CLIPBOARD_COPIED_LABEL, text);
            if (clipboard != null) {
                clipboard.setPrimaryClip(clip);
            }
            Toast.makeText(context, "Copied to clipboard!", Toast.LENGTH_SHORT).show();
        }
    }
}
