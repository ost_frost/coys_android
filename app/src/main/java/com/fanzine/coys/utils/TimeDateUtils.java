package com.fanzine.coys.utils;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.text.format.DateUtils;
import android.util.Log;

import com.fanzine.coys.viewmodels.fragments.team.player.ProfileViewModel;
import com.fanzine.chat3.ChatData;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static android.text.TextUtils.isEmpty;


public class TimeDateUtils {
    public static final String LOG_TAG = ChatData.LOG_TAG + "CurrentUserDao";
    private static final String dateFormat = "HH:mm:ss";

    private static final String EMPTY_VALUE = "";

    public static final String DF_SERVER = "yyyy-MM-dd HH:mm:ss.SSS";
    private static final String DF_DD_MM_YYYY = "dd/MM/yyyy";

    private static final String DF_BIRTH_YEAR = "dd MMMM yyyy";

    @SuppressLint("SimpleDateFormat")
    public static String convert(String date, String fromPattern, String toPattern) {
        try {
            Date d = new SimpleDateFormat(fromPattern, Locale.ENGLISH)
                    .parse(date);
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            return new SimpleDateFormat(toPattern).format(cal.getTime());
        } catch (ParseException e) {
            Log.i(ProfileViewModel.class.getName(), e.getMessage());
        } catch (NullPointerException e) {
            Log.i(ProfileViewModel.class.getName(), e.getMessage());
        }
        return EMPTY_VALUE;
    }

    //time for chat 12.56
    public static String convertTimestampToLocalTimeDayMon(Long timestamp) {

        if (timestamp != null && timestamp > 0) {

            DateTimeFormatter inputFormatter = DateTimeFormat.forPattern(dateFormat)
                    .withZone(DateTimeZone.UTC);

            DateTime parsed = new DateTime(timestamp);

            String dateFormat = "HH:mm";


            DateTimeFormatter outputFormatter = DateTimeFormat.forPattern(dateFormat)
                    .withZone(DateTimeZone.getDefault());

            Log.i(LOG_TAG, "DateTimeZone.getDefault() =" + DateTimeZone.getDefault());

            String formattedDate = outputFormatter.print(parsed);

            Log.i(LOG_TAG, "localTime =" + formattedDate);

            return formattedDate;
        } else return EMPTY_VALUE;

    }


    // 26 May 2017
    public static String convertTimestampToLocalTime(Long timestamp) {

        if (timestamp != null && timestamp > 0) {

            DateTimeFormatter inputFormatter = DateTimeFormat.forPattern(dateFormat)
                    .withZone(DateTimeZone.UTC);

            DateTime parsed = new DateTime(timestamp);

            String dateFormat = "dd MMM yyyy";


            DateTimeFormatter outputFormatter = DateTimeFormat.forPattern(dateFormat)
                    .withZone(DateTimeZone.getDefault());

            Log.i(LOG_TAG, "DateTimeZone.getDefault() =" + DateTimeZone.getDefault());

            String formattedDate = outputFormatter.print(parsed);

            Log.i(LOG_TAG, "localTime =" + formattedDate);

            return formattedDate;
        } else return EMPTY_VALUE;
    }

    public static String convertUtcToLocalTime2(String timeStr) {

        if (timeStr != null && timeStr.length() > 0) {

            DateTimeFormatter inputFormatter = DateTimeFormat.forPattern(dateFormat)
                    .withZone(DateTimeZone.UTC);

            DateTime parsed = inputFormatter.parseDateTime(timeStr);

            DateTimeFormatter outputFormatter = DateTimeFormat.forPattern(dateFormat)
                    .withZone(DateTimeZone.getDefault());

            Log.i(LOG_TAG, "DateTimeZone.getDefault() =" + DateTimeZone.getDefault());

            String formattedDate = outputFormatter.print(parsed);

            Log.i(LOG_TAG, "utcTime =" + timeStr);

            Log.i(LOG_TAG, "localTime =" + formattedDate);

            return formattedDate;
        } else return EMPTY_VALUE;
    }

    public static String convertUtcToLocalTime(String timeStr) {

        if (timeStr != null && timeStr.length() > 0) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date myDate = null;
            try {
                myDate = simpleDateFormat.parse(timeStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            String strDefaultTimeZone = TimeZone.getDefault().getDisplayName(false, TimeZone.SHORT);

            simpleDateFormat.setTimeZone(TimeZone.getTimeZone(strDefaultTimeZone));
            String formattedDate = simpleDateFormat.format(myDate);

            return formattedDate;
        } else return EMPTY_VALUE;
    }

    public static String formatBirthday(String timeStr) {
        if (!isEmpty(timeStr)) {
            return convert(timeStr, DF_SERVER, DF_DD_MM_YYYY);
        }
        return EMPTY_VALUE;
    }

    public static String formatBirthYear(String timeStr) {
        if (!isEmpty(timeStr)) {
            return convert(timeStr, DF_SERVER, DF_BIRTH_YEAR);
        }
        return EMPTY_VALUE;
    }

    @NonNull
    public static String getCreatedAgo(@NonNull String timeString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            final Date date = dateFormat.parse(timeString);
            return DateUtils.getRelativeTimeSpanString(date.getTime(), Calendar.getInstance().getTimeInMillis(), DateUtils.MINUTE_IN_MILLIS).toString();
        } catch (Exception e) {
            Log.e("TimeDateUtil", "getCreatedAgo :: parse error ", e);
        }
        return "";
    }

    @NonNull
    public static String getMonth(@NonNull String timeString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            final Date date = dateFormat.parse(timeString);

            SimpleDateFormat monthFormat = new SimpleDateFormat("MMMM", Locale.ENGLISH);

            return dateFormat.format(monthFormat);
        } catch (Exception e) {
            Log.e("TimeDateUtil", "getCreatedAgo :: parse error ", e);
        }
        return "";
    }
}
