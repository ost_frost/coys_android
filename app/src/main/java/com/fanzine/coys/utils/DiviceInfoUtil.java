package com.fanzine.coys.utils;

import android.content.pm.PackageManager;
import android.util.Log;

import com.fanzine.coys.App;

/**
 * Created by a on 03.11.17.
 */
public class DiviceInfoUtil {

    private static DiviceInfoUtil instance;
    private static String result;

    public static synchronized DiviceInfoUtil getInstance() {
        if (instance == null) {
            instance = new DiviceInfoUtil();
        }
        return instance;
    }

    public String getDeviceInfo() {

        if (result != null) {
            return result;
        }
        result = new String();

        result = result.concat("Gunners/");
        result = result.concat(getAppVersionName());
        result = result.concat("(");
        result = result.concat(android.os.Build.MANUFACTURER);
        result = result.concat(";");
        result = result.concat("Android");
        result = result.concat(" ");
        result = result.concat(android.os.Build.VERSION.RELEASE);
        result = result.concat(";");
        result = result.concat(")");

        Log.i("@@", "result=" + result);

        return result;
    }


    private String getAppVersionName() {

        String versionName = null;
        try {
            versionName = App.getContext().getPackageManager().getPackageInfo(App.getContext().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }

    private String getAppVersionCode() {

        int versionCode = 0;
        try {
            versionCode = App.getContext().getPackageManager().getPackageInfo(App.getContext().getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return String.valueOf(versionCode);

    }

}
