package com.fanzine.coys.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by mbp on 2/5/18.
 */

public class GeoHelper {

    public static String decodeAddress(Context context, LatLng latLng) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();

            return "";
        }
        if (addresses != null && addresses.size() > 0) {
            List<String> fullAddressList = new ArrayList<>();

            fullAddressList.add(addresses.get(0).getAddressLine(0)); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            fullAddressList.add(addresses.get(0).getLocality());
            fullAddressList.add(addresses.get(0).getAdminArea());
            fullAddressList.add(addresses.get(0).getCountryName());
            fullAddressList.add(addresses.get(0).getPostalCode());
            fullAddressList.add(addresses.get(0).getFeatureName());

            StringBuilder googleMapAddressFormat = new StringBuilder();

            for (String item : fullAddressList) {
                if (item != null && item.length() > 0) {
                    googleMapAddressFormat.append(item).append(", ");
                }
            }

            return googleMapAddressFormat.substring(0, googleMapAddressFormat.length() - 2);
        }

        return "";
    }

    public static String decodeAddress(Context context, String address) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocationName(address,1);
        } catch (IOException e) {
            e.printStackTrace();

            return "";
        }
        if (addresses != null && addresses.size() > 0) {
            List<String> fullAddressList = new ArrayList<>();

            fullAddressList.add(addresses.get(0).getAddressLine(0)); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            fullAddressList.add(addresses.get(0).getLocality());
            fullAddressList.add(addresses.get(0).getAdminArea());
            fullAddressList.add(addresses.get(0).getCountryName());
            fullAddressList.add(addresses.get(0).getPostalCode());
            fullAddressList.add(addresses.get(0).getFeatureName());

            StringBuilder googleMapAddressFormat = new StringBuilder();

            for (String item : fullAddressList) {
                if (item != null && item.length() > 0) {
                    googleMapAddressFormat.append(item).append(", ");
                }
            }

            return googleMapAddressFormat.substring(0, googleMapAddressFormat.length() - 2);
        }

        return "";
    }
}
