package com.fanzine.coys.utils;

/**
 * Created by vitaliygerasymchuk on 2/24/18
 */

public class NumberFormatter {

    public static String formatNumber(long count) {
        if (count < 1000) return "" + count;
        int exp = (int) (Math.log(count) / Math.log(1000));
        return String.format("%.1f%c",
                count / Math.pow(1000, exp),
                "KMGTPE".charAt(exp - 1));
    }
}
