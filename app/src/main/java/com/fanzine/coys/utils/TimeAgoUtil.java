package com.fanzine.coys.utils;

import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Siarhei on 21.01.2017.
 */

public class TimeAgoUtil {
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    public static boolean isPastDate(Date date) {
        if (date != null) {
            long parsedMils =  date.getTime();

            if (parsedMils < 1000000000000L) {
                // if timestamp given in seconds, convert to millis
                parsedMils *= 1000;
            }

            int utcOffset = TimeZone.getDefault().getOffset(System.currentTimeMillis());
            long now = System.currentTimeMillis();

            if (parsedMils > now || parsedMils <= 0) {
                return false;
            }
            // TODO: localize
            final long diff = now - parsedMils - utcOffset;

            return diff >= 24 * HOUR_MILLIS;
        }
        return false;
    }

    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        int utcOffset = TimeZone.getDefault().getOffset(System.currentTimeMillis());
        long now = System.currentTimeMillis();

        if (time > now || time <= 0) {
            return "";
        }

        // TODO: localize
        final long diff = now - time - utcOffset;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "1 min";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " mins";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "1 hr ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hrs";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "1 day ago";
        } else {
            return diff / DAY_MILLIS + " days";
        }
    }
}
