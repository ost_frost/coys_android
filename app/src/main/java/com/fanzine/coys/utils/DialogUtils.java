package com.fanzine.coys.utils;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import java.util.Date;

public class DialogUtils {

    public static ProgressDialog showProgressDialog(Context context, int message) {
        return showProgressDialog(context, context.getString(message));
    }

    public static ProgressDialog showProgressDialog(Context context, String message) {
        ProgressDialog progressDialog = createProgressDialog(context, message);
        try {
            progressDialog.show();
        } catch (Exception ignored) {
        }
        return progressDialog;
    }

    public static ProgressDialog createProgressDialog(Context context, int message) {
        return createProgressDialog(context, context.getString(message));
    }

    public static ProgressDialog createProgressDialog(Context context, String message) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        return progressDialog;
    }


    public static void showAlertDialog(Context context, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", (dialog1, which) -> dialog1.dismiss()).create();
        dialog.show();
    }

    public static void showErrorDialog(Context context, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle("Error")
                .setMessage(message)
                .setPositiveButton("OK", (dialog1, which) -> dialog1.dismiss()).create();
        dialog.show();
    }


    public static void showAlertDialog(Context context, int message) {
        showAlertDialog(context, context.getString(message));
    }

    public static void showAlertDialog(Context context, String message, DialogInterface.OnClickListener listener) {
        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", listener).create();
        dialog.show();
    }

    public static void showAlertDialog(Context context, int message, DialogInterface.OnClickListener listener) {
        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", listener).create();
        dialog.show();
    }

    public static void showDatePickerDialog(Context context, DatePickerDialog.OnDateSetListener listener, int year, int month, int day, boolean setMinDate) {
        final DatePickerDialog tpd = new DatePickerDialog(context, listener, year, month, day);
        if (setMinDate)
            tpd.getDatePicker().setMinDate(new Date().getTime());
        tpd.show();
    }
}
