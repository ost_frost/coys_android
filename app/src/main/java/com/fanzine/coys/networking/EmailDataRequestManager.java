package com.fanzine.coys.networking;

import com.fanzine.coys.api.response.MailboxFolders;
import com.fanzine.coys.models.mails.Color;
import com.fanzine.coys.models.mails.Label;
import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.models.mails.NewMail;

import java.util.List;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.fanzine.coys.api.ApiRequest.getInstance;


/**
 * Created by maximdrobonoh on 05.09.17.
 */

public class EmailDataRequestManager {

    private static EmailDataRequestManager instanse;

    public EmailDataRequestManager() {

    }

    public static EmailDataRequestManager getInstanse() {
        synchronized (EmailDataRequestManager.class) {
            if (instanse == null) {
                instanse = new EmailDataRequestManager();
            }
        }

        return instanse;
    }

    public Observable<ResponseBody> move(String fromFolder, String toFolder, int uid) {
        return getInstance().getApi().moveMail(fromFolder, toFolder, uid)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<Mail>> getAll() {
        return getInstance().getApi().getMails()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<Mail>> getMailsByFolder(String folder) {
        return getInstance().getApi().getMailsByFolder(folder)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> addMailbox(String folderName) {
        return getInstance().getApi().addMailbox(folderName)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<MailboxFolders> getMailboxFolders() {
        return getInstance().getApi().getMailboxFolders()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> deleteMail(String folder, String id) {
        Observable<ResponseBody> obs = getInstance().getApi().removeMail(folder, id);

        return obs;
    }

    public Observable<Mail> deleteMailList(Mail[] mailList) {

        final Mail[] mail1 = new Mail[1];
        Observable<Mail> obs = Observable.from(mailList)
                .concatMap(mail -> {
                    mail1[0] = mail;
                    return deleteMail(mail.getFolder(), String.valueOf(mail.getUid()));
                })
                .flatMap(responseBody -> Observable.just(mail1[0]))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
        return obs;
    }


    public Observable<Mail> moveMailsToFolder(Mail[] mailList, String folder) {

        final Mail[] mail1 = new Mail[1];
        Observable<Mail> obs = Observable.from(mailList)
                .concatMap(mail -> {
                    mail1[0] = mail;
                    return getInstance().getApi().moveMail(mail.getFolder(), folder, mail.getUid());
                })
                .flatMap(responseBody -> Observable.just(mail1[0]))

                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
        return obs;
    }


    public Observable<ResponseBody> removeMailbox(String name) {
        return getInstance().getApi().removeMailbox(name)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<Label>> getLabelsList() {
        return getInstance().getApi().getLabelsList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<Color>> getLabelColors() {
        return getInstance().getApi().getLabelColors()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<Label>> createEmailLabelForUser(String name, int colorId) {
        return getInstance().getApi().createEmailLabel(name, colorId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> deleteLabel(int labelId) {
        return getInstance().getApi().deleteLabel(labelId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> addLabelToMail(String folderName, int mailId, int labelId) {
        return getInstance().getApi().addLabelToMail(folderName, mailId, labelId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> deleteLabelFromMail(String folderName, int mailId, int labelId) {
        return getInstance().getApi().deleteLabelFromMail(folderName, mailId, labelId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> sendMail(NewMail mail) {
        return getInstance().getApi().sendMail(mail.getHeaderPart(), mail.getAttachmentsPart())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> saveToDraft(NewMail mail) {
        return getInstance().getApi().saveToDraft(mail.getHeaderPart(), mail.getAttachmentsPart())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Boolean> setMailStarred(String folder, String uid, boolean isFlagged) {
        return getInstance().getApi().setMailStarred(folder, uid)
                .flatMap(responseBody -> Observable.just(!isFlagged))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<Mail>> getMailStarred() {
        return getMailsByFolder("Inbox")
                .flatMap(Observable::from)
                .filter(mail -> mail.isFlagged())
                .toList();
    }

    public Observable<ResponseBody> setMailUnread(String folderFrom, String uid) {
        return getInstance().getApi().setMailUnread(folderFrom, uid);
    }

}
