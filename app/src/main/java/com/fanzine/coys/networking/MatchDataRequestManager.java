package com.fanzine.coys.networking;

import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.models.LigueMatches;
import com.fanzine.coys.models.analysis.Analysis;
import com.fanzine.coys.models.analysis.Prediction;
import com.fanzine.coys.models.table.Filter;
import com.google.gson.JsonObject;

import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by maximdrobonoh on 22.09.17.
 */

public class MatchDataRequestManager {

    private static MatchDataRequestManager instance;

    private MatchDataRequestManager() {

    }

    public static MatchDataRequestManager getInstance() {
        synchronized (MatchDataRequestManager.class) {
            if (instance == null) {
                instance = new MatchDataRequestManager();
            }
        }
        return instance;
    }

    public Observable<LigueMatches> getFixtures(String date, int leagueId, int week, String season) {
        return ApiRequest.getInstance().getApi()
                .getMathchesByLeague(leagueId,season,date, week)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<JsonObject>> getTeamsScore(int leagueId, String season) {
        return ApiRequest.getInstance().getApi()
                .getTeams(leagueId, season)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<JsonObject>> getPlayerStats(int leagueId, String filterFiled) {
        return ApiRequest.getInstance().getApi()
                .getPlayers(leagueId, filterFiled)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<Filter>> getTeamFilters() {
        return ApiRequest.getInstance().getApi()
                .getTeamsFilters()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<Filter>> getPlayersFilters() {
        return ApiRequest.getInstance().getApi()
                .getPlayersFilters()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Analysis> getAnalysis(int matchId) {
        return ApiRequest.getInstance().getApi()
                .getAnalysis(matchId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Prediction> castVote(int matchId, String vote) {
        return ApiRequest.getInstance().getApi()
                .castVote(matchId, vote)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
