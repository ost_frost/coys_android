package com.fanzine.coys.helpers;

/**
 * Created by mbp on 3/21/18.
 */

public interface Constants {
    String FIREBASE_KEY = "292b1c13-e3bc-4822-8766-9fff6f8d8c43";
    String IN_BILLING_LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApLLIiapushxeO/tdthstr5kMPQZyVRf7do02KQ3N8W2wQ8ggDY9azpx+zPY5t1Sq62zWZgfCMqtQkvEJh+YlfSz/we4E6dcl6YSuLr/pBynX7BI90iRHqfGVZjquSAQv8SCWdApO+siZNKaAPJ26Jw50RkDBAqdgQjg3SBvQ9xtLgxSK7WEtZ1yDXGC33ukw1BobJAH0CqV5f+SFNPol/q4ankZxROZFkH7KD3n83UjdguezXQup6R9q+hsuqV0C+5DQWisalhoMocen6Si18a7SdOiZ+KAvcQZmNzjNUGPIRwZXjRdu2I+1FpuHkk+BYPb8qtIimj8n5Nn0AH54GwIDAQAB";

    String EMAIL_SUBSCRIPTION = "email_subscription_test_01";

    int SUBSCRIPTION_PURCHASED = 765;
    int BILLING_ERROR = 705;

    String PRODUCT_ID = "product_id";
    String TRANSACTION_DETAILS = "transaction_details";

    float METTERS_IN_MILE = 1609.34f;
}
