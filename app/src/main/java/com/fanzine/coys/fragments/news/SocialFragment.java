package com.fanzine.coys.fragments.news;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.SocialAdapter;
import com.fanzine.coys.databinding.FragmentNewsSocialBinding;
import com.fanzine.coys.models.Social;
import com.fanzine.coys.viewmodels.fragments.news.SocialFeedViewModel;


import java.util.List;

/**
 * Created by user on 07.11.2017.
 */

public class SocialFragment extends FeedListAbstractFragment<Social> implements
        SocialAdapter.OnSocialListener {

    private FragmentNewsSocialBinding binding;
    private SocialFeedViewModel viewModel;

    private SocialAdapter mAdapter;

    public static SocialFragment newInstance(int leagueId) {
        Bundle args = new Bundle();
        SocialFragment fragment = new SocialFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root,
                                      boolean attachToRoot) {

        viewModel = new SocialFeedViewModel(getContext(), this);
        binding = FragmentNewsSocialBinding.inflate(inflater, root, false);
        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);

        viewModel.loadData(page);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false);

        binding.rvSocialList.setLayoutManager(layoutManager);

        binding.swipe.setEnabled(true);
        binding.swipe.setColorSchemeResources(R.color.colorAccent);
        binding.swipe.setOnRefreshListener(() -> viewModel.loadData(page));
        binding.rvSocialList.setAnimation(null);

        mAdapter = new SocialAdapter(getContext(), this);
        binding.rvSocialList.setAdapter(mAdapter);
        setupScrollListener(binding.rvSocialList, layoutManager, binding.swipe);

        return binding;
    }

    @Override
    public void loadData(int page) {
        viewModel.loadData(page);
    }

    @Override
    public void openPost(String url) {
        try {
            openBrowser(url);
        } catch (ActivityNotFoundException e) {
            Log.i(getTag(), e.getMessage());
        }
    }

    @Override
    public void openImage(String url) {

    }

    @Override
    public void onLoaded(List<Social> data) {
        binding.swipe.setEnabled(true);
        binding.swipe.setRefreshing(false);


        if (data.size() > 0) {
            page++;

            if (mAdapter == null) {
                mAdapter = new SocialAdapter(getContext(), data, this);
                binding.rvSocialList.setAdapter(mAdapter);
            } else if ( page == 1) {
                mAdapter.clear();
                mAdapter.addAll(data);
            } else {
                mAdapter.addAll(data);
            }

            isLoading = false;
        }
    }

    @Override
    public void onLoaded(Social data) {

    }

    @Override
    public void onError() {

    }

    //todo:: backend return invalid url //www.twitter.com/@Emimartinezz1/status/970089381196378112
    private void openBrowser(String url) {
        if (url.substring(0, 2).equals("//")) {
            url = "https://" + url.substring(2, url.length());
        }
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        getActivity().startActivity(intent);
    }
}
