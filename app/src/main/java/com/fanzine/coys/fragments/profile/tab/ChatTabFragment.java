package com.fanzine.coys.fragments.profile.tab;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.fanzine.coys.databinding.FragmentProfileChatBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.profile.ChatNotificationState;
import com.fanzine.coys.viewmodels.fragments.profile.ChatProfileViewModel;

/**
 * Created by vitaliygerasymchuk on 2/10/18
 */

public class ChatTabFragment extends BaseFragment implements ChatProfileViewModel.ChatStatusCallback, CompoundButton.OnCheckedChangeListener {

    public static ChatTabFragment newInstance() {
        Bundle args = new Bundle();
        ChatTabFragment fragment = new ChatTabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    private FragmentProfileChatBinding binding;
    @Nullable
    private ChatProfileViewModel viewModel;

    private boolean isVisible;

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        viewModel = new ChatProfileViewModel(getContext());
        binding = FragmentProfileChatBinding.inflate(LayoutInflater.from(getContext()));
        binding.setViewModel(viewModel);
        viewModel.getChatNotificationsStatus(this);
        binding.switchChat.setOnCheckedChangeListener(this);

        return binding;
    }

    @Override
    public void onChatState(ChatNotificationState state) {
        if (viewModel != null && binding != null) {
            viewModel.setChatState(state);
            binding.switchChat.setOnCheckedChangeListener(this);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (binding != null && viewModel != null) {
            if (isVisible) {
                if (viewModel.isStatesDiffer(isChecked ? 0 : 1)) {
                    binding.switchChat.setOnCheckedChangeListener(null);
                    viewModel.postChatNotificationState(new ChatNotificationState(isChecked ? 0 : 1), this);
                } else Log.e("ChatTabFragment", "skip server call");

                if (isChecked)
                    viewModel.subsribeOnNotifications();
                else
                    viewModel.unsubscribeOnNotifications();
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
    }
}
