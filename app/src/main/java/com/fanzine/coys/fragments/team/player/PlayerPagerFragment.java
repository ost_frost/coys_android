package com.fanzine.coys.fragments.team.player;


import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.FragmentTeamPlayerBinding;
import com.fanzine.coys.models.team.Player;
import com.fanzine.coys.models.team.PlayerProfile;
import com.fanzine.coys.models.team.TeamSquad;
import com.fanzine.coys.viewmodels.fragments.team.PlayerProfileViewModel;


public class PlayerPagerFragment extends com.fanzine.coys.fragments.base.BaseFragment implements
        PlayerProfileViewModel.ProfileCallback {

    private static final String PLAYER = "player";
    private static final String SQUAD = "squad";

    private PlayerProfileViewModel viewModel;
    private FragmentTeamPlayerBinding binding;
    private Player mPlayer;
    private TeamSquad squad;

    public static PlayerPagerFragment newInstance(Player player, TeamSquad squad) {
        PlayerPagerFragment fragment = new PlayerPagerFragment();
        Bundle args = new Bundle();
        args.putSerializable(SQUAD, squad);
        args.putParcelable(PLAYER, player);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        if (getArguments().containsKey(SQUAD)) {
            squad = (TeamSquad) getArguments().getSerializable(SQUAD);
        }
        binding = FragmentTeamPlayerBinding.inflate(inflater, root, attachToRoot);

        mPlayer = getArguments().getParcelable(PLAYER);

        viewModel = new PlayerProfileViewModel(getContext(), this, mPlayer);
        viewModel.loadProfile();


        return binding;
    }

    @Override
    public void onSuccess(PlayerProfile profile) {
        viewModel.setPlayerProfile(profile);

        PagerAdapter pagerAdapter = new PagerAdapter(getChildFragmentManager(), profile);

        binding.viewPager.setAdapter(pagerAdapter);
        binding.viewPager.setCurrentItem(PagerAdapter.PROFILE);
        binding.tabLayout.setupWithViewPager(binding.viewPager);

        binding.setViewModel(viewModel);
        binding.executePendingBindings();
    }

    @Override
    public void onError() {
        //no-op
    }

    class PagerAdapter extends FragmentPagerAdapter {

        final static int PAGES_COUNT = 5;

        final static int SOCIAL = 0;
        final static int STATS = 1;
        final static int PROFILE = 2;
        final static int PHOTOS = 3;
        final static int VIDEOS = 4;

        PlayerProfile playerProfile;

        PagerAdapter(FragmentManager fm, PlayerProfile playerProfile) {
            super(fm);
            this.playerProfile = playerProfile;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == PROFILE) {
                return ProfileFragment.newInstance(playerProfile.getProfile());
            } else if (position == STATS) {
                return StatsFragment.getInstance(mPlayer, squad);
            } else if (position == SOCIAL) {
                return SocialFragment.getInstance(mPlayer);
            } else if (position == PHOTOS) {
                return PhotosFragment.getInstance(mPlayer);
            } else if (position == VIDEOS) {
                return VideosFragment.getInstance(mPlayer);
            }
            return new Fragment();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case PROFILE:
                    return getString(R.string.tab_player_profile);
                case SOCIAL:
                    return getString(R.string.tab_player_social);
                case STATS:
                    return getString(R.string.tab_player_stats);
                case PHOTOS:
                    return getString(R.string.tab_player_photos);
                case VIDEOS:
                    return getString(R.string.tab_player_videos);
                default:
                    return "";
            }
        }


        @Override
        public int getCount() {
            return PAGES_COUNT;
        }
    }
}
