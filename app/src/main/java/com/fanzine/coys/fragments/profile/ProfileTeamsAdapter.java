package com.fanzine.coys.fragments.profile;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.coys.models.profile.Team;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vitaliygerasymchuk on 2/13/18
 */

public class ProfileTeamsAdapter extends RecyclerView.Adapter<ProfileTeamsAdapter.TeamHolder> {

    public interface Callback {
        void onTeamSelected(@NonNull Team team);
    }

    @NonNull
    private List<Team> teams = new ArrayList<>();

    @NonNull
    private Callback callback;

    public void setTeams(@NonNull List<Team> teams) {
        this.teams.clear();
        this.teams.addAll(teams);
        this.notifyDataSetChanged();
    }

    public void setCallback(@NonNull Callback callback) {
        this.callback = callback;
    }

    @Override
    public TeamHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile_team, parent, false);
        return new TeamHolder(view, adapterPosition -> {
            if (adapterPosition < teams.size()) {
                Team team = teams.get(adapterPosition);
                callback.onTeamSelected(team);
            }
        });
    }

    @Override
    public void onBindViewHolder(TeamHolder holder, int position) {
        if (position < teams.size()) {
            Team team = teams.get(position);
            holder.bind(team);
        }
    }

    @Override
    public int getItemCount() {
        return teams.size();
    }

    static class TeamHolder extends RecyclerView.ViewHolder {

        interface TeamCallback {
            void onTeamAdapterPosition(int adapterPosition);
        }

        private CircleImageView ivLogo;

        private TextView tvTeamName;

        TeamHolder(View itemView, @NonNull TeamCallback callback) {
            super(itemView);
            this.ivLogo = (CircleImageView) itemView.findViewById(R.id.iv_team_logo);
            this.tvTeamName = (TextView) itemView.findViewById(R.id.tv_team_name);
            itemView.findViewById(R.id.click).setOnClickListener(
                    v -> callback.onTeamAdapterPosition(getAdapterPosition()));
        }

        public void bind(@NonNull Team team) {
            loadLogo(team.getTeamIco());
            tvTeamName.setText(team.getName());
        }

        private void loadLogo(String url) {
            Glide.with(itemView.getContext()).load(url).asBitmap().centerCrop().into(new BitmapImageViewTarget(ivLogo) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(itemView.getContext().getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    ivLogo.setImageDrawable(circularBitmapDrawable);
                }
            });
        }
    }
}
