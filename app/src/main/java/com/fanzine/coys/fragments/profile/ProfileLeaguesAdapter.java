package com.fanzine.coys.fragments.profile;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.coys.models.profile.League;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vitaliygerasymchuk on 2/13/18
 */

public class ProfileLeaguesAdapter extends RecyclerView.Adapter<ProfileLeaguesAdapter.LeagueHolder> {

    public interface Callback {
        void onLeagueSelected(@NonNull League league);
    }

    @NonNull
    private List<League> leagues = new ArrayList<>();

    @NonNull
    private Callback callback;

    public void setLeagues(@NonNull List<League> leagues) {
        this.leagues.clear();
        this.leagues.addAll(leagues);
        this.notifyDataSetChanged();
    }

    public void setCallback(@NonNull Callback callback) {
        this.callback = callback;
    }

    @Override
    public LeagueHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile_league, parent, false);
        return new LeagueHolder(view, adapterPosition -> {
            if (adapterPosition < leagues.size()) {
                final League league = leagues.get(adapterPosition);
                callback.onLeagueSelected(league);
            }
        });
    }

    @Override
    public void onBindViewHolder(LeagueHolder holder, int position) {
        if (position < leagues.size()) {
            League league = leagues.get(position);
            holder.bind(league);
        }
    }

    @Override
    public int getItemCount() {
        return leagues.size();
    }

    static class LeagueHolder extends RecyclerView.ViewHolder {

        interface LeagueCallback {
            void onLeagueAdapterPosition(int adapterPosition);
        }

        CircleImageView ivLogo;

        TextView tvTeamName;

        LeagueHolder(View itemView, @NonNull LeagueCallback callback) {
            super(itemView);
            ivLogo = (CircleImageView) itemView.findViewById(R.id.iv_team_logo);
            tvTeamName = (TextView) itemView.findViewById(R.id.tv_league_name);
            itemView.findViewById(R.id.click).setOnClickListener(v -> callback.onLeagueAdapterPosition(getAdapterPosition()));
        }

        public void bind(@NonNull League league) {
            loadLogo(league.getIcon());
            tvTeamName.setText(league.getName());
        }

        private void loadLogo(String url) {
            Glide.with(itemView.getContext()).load(url).asBitmap().centerCrop().into(new BitmapImageViewTarget(ivLogo) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(itemView.getContext().getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    ivLogo.setImageDrawable(circularBitmapDrawable);
                }
            });
        }
    }
}
