package com.fanzine.coys.fragments.team;


import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.CarouselAdapter;
import com.fanzine.coys.databinding.FragmentTeamBinding;
import com.fanzine.coys.models.CarouselFilter;
import com.fanzine.coys.models.team.Position;
import com.fanzine.coys.models.team.PositionType;
import com.gtomato.android.ui.transformer.CoverFlowViewTransformer;
import com.gtomato.android.ui.widget.CarouselView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TeamFragment extends com.fanzine.coys.fragments.base.BaseFragment {

    private FragmentTeamBinding binding;
    private List<CarouselFilter> categories = new ArrayList<>();

    @Nullable
    private TabAdapter tabAdapter;

    private CarouselAdapter carouselAdapter;

    public static TeamFragment newInstance() {
        TeamFragment fragment = new TeamFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        if (binding == null) {

            binding = FragmentTeamBinding.inflate(inflater, root, attachToRoot);

            setupTabs();
            handleTabLayout();
            handleViewPager();
            setupPositions();
        }
        return binding;
    }

    private void setupPositions() {
        CoverFlowViewTransformer transformer1 = new CoverFlowViewTransformer() {
            @Override
            public void transform(View view, float position) {
                int width = view.getMeasuredWidth(), height = view.getMeasuredHeight();
                view.setPivotX(width / 2.0f);
                view.setPivotY(height / 2.0f);
                view.setTranslationX(width * position * mOffsetXPercent);
                view.setRotationY(Math.signum(position) * (float) (Math.log(Math.abs(position) + 1) / Math.log(3)));
                view.setScaleY(1 + mScaleYFactor * Math.abs(position));
                view.setScaleX(1 + mScaleYFactor * Math.abs(position));
            }
        };

        transformer1.setOffsetXPercent(0.3f);
        transformer1.setYProjection(0);
        transformer1.setScaleYFactor(-0.15f);

        CarouselFilter midfielder = new Position(R.drawable.ic_midfielder, PositionType.MIDFIELDER);
        CarouselFilter defender = new Position(R.drawable.ic_defender, PositionType.DEFENDER);
        CarouselFilter attacker = new Position(R.drawable.ic_attacker, PositionType.ATTACKER);
        CarouselFilter goalkeeper = new Position(R.drawable.ic_goalkeeper, PositionType.GOALKEEPER);

        categories.add(goalkeeper);
        categories.add(defender);
        categories.add(midfielder);
        categories.add(attacker);

        binding.filterCarousel.setOnItemClickListener((adapter1, view, position, adapterPosition) -> {
            if (adapter1.getItemCount() > 0) {
                CarouselFilter filter = getCarouselFilter(adapterPosition);
                filterPlayers(filter);
            }
        });

        carouselAdapter = new CarouselAdapter(categories);

        binding.filterCarousel.setTransformer(transformer1);
        binding.filterCarousel.setAdapter(carouselAdapter);
        binding.filterCarousel.setOnItemSelectedListener(new CarouselView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(CarouselView carouselView, int position, int adapterPosition, RecyclerView.Adapter adapter) {
                CarouselFilter filter = getCarouselFilter(adapterPosition);
                filterPlayers(filter);
            }

            @Override
            public void onItemDeselected(CarouselView carouselView, int position, int adapterPosition, RecyclerView.Adapter adapter) {

            }
        });
    }

    private void filterPlayers(@NonNull CarouselFilter filter) {
        if (tabAdapter != null) {
            for (AbsPlayersFragment frg : tabAdapter.frgs) {
                frg.filterPlayers(filter);
            }
        }
    }

    private void setupTabs() {
        this.tabAdapter = new TabAdapter(getChildFragmentManager());
        this.tabAdapter.addFrg(FirstTeamFragment.newInstance(), AcademyFragment.newInstance(), LadiesFragment.newInstance());
        binding.viewPager.setAdapter(tabAdapter);
        binding.viewPager.setOffscreenPageLimit(3);
    }

    private void handleTabLayout() {
        if (binding != null) {
            binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    if (binding != null) {
                        binding.viewPager.setCurrentItem(tab.getPosition());
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {
                    //no-op
                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {
                    //no-op
                }
            });
        }
    }

    private void handleViewPager() {
        if (binding != null) {
            binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    //no-op
                }

                @Override
                public void onPageSelected(int position) {
                    if (binding.tabLayout != null) {
                        TabLayout.Tab tab = binding.tabLayout.getTabAt(position);
                        if (tab != null) {
                            tab.select();
                        }
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                    //no-op
                }
            });
        }
    }

    @NonNull
    private CarouselFilter getCarouselFilter(int adapterPosition) {
        return carouselAdapter.getItem(adapterPosition);
    }

    private class TabAdapter extends FragmentPagerAdapter {

        List<AbsPlayersFragment> frgs = new ArrayList<>();

        void addFrg(AbsPlayersFragment... frg) {
            this.frgs.clear();
            this.frgs.addAll(Arrays.asList(frg));
            this.notifyDataSetChanged();
        }

        TabAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public AbsPlayersFragment getItem(int position) {
            return frgs.get(position);
        }

        @Override
        public int getCount() {
            return frgs.size();
        }
    }
}
