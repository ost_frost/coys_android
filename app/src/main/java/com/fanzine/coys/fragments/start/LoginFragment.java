package com.fanzine.coys.fragments.start;

import android.databinding.ViewDataBinding;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.login.CountryAdapter;
import com.fanzine.coys.databinding.DialogRecyclerviewBinding;
import com.fanzine.coys.databinding.FragmentLoginBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.login.Country;
import com.fanzine.coys.utils.RecyclerItemClickListener;
import com.fanzine.coys.viewmodels.fragments.login.LoginFragmentViewModel;
import com.jakewharton.rxbinding.view.RxView;

import java.util.List;

/**
 * Created by Woland on 05.01.2017.
 */

public class LoginFragment extends BaseFragment {

    private FragmentLoginBinding binding;
    private LoginFragmentViewModel viewModel;
    private AlertDialog dialog;

    private List<Country> mCountries;

    public static LoginFragment newInstance() {
        Bundle args = new Bundle();
        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentLoginBinding.inflate(inflater, root, attachToRoot);
        viewModel = new LoginFragmentViewModel(getContext());
        binding.setViewModel(viewModel);

        binding.forgot.setPaintFlags(binding.forgot.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        setBaseViewModel(viewModel);

        RxView.clicks(binding.country).subscribe((aVoid) -> openCountryDialog());
        binding.country.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus)
                openCountryDialog();
        });

        viewModel.fetchCountries(countryList -> {
            if (countryList.size() > 0) {
                mCountries = countryList;
                viewModel.country.set(countryList.get(0));
            }
        });

        return binding;
    }

    private void openCountryDialog() {

        DialogRecyclerviewBinding bindingDialog = DialogRecyclerviewBinding.inflate(LayoutInflater.from(getContext()));
        bindingDialog.rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        CountryAdapter adapter = new CountryAdapter(getContext(), mCountries);
        bindingDialog.rv.setAdapter(adapter);

        bindingDialog.rv.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), (view, position) -> {
            Country country = mCountries.get(position);

            viewModel.country.set(country);

            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
            }
        }));

        bindingDialog.executePendingBindings();

        dialog = new AlertDialog.Builder(getContext())
                .setView(bindingDialog.getRoot())
                .setNegativeButton(R.string.cancel, null)
                .show();

        dialog.getWindow().setBackgroundDrawableResource(R.color.backgroundDialog);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }
}
