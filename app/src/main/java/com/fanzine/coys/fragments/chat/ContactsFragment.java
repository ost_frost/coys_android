package com.fanzine.coys.fragments.chat;

import android.database.Cursor;
import android.databinding.ViewDataBinding;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.chat.ContactsAdapter;
import com.fanzine.coys.databinding.FragmentContactsBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.Contact;
import com.fanzine.chat.models.channels.FCChannel;
import com.fanzine.chat.models.user.FCUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ContactsFragment extends BaseFragment implements ContactsAdapter.ShowMenuItemListener {

    private static final String USERS = "users";
    private static final String CHANNEL = "channel";
    private Map<Contact, FCUser> selected = new HashMap<>();
    private ContactsAdapter adapter;
    private FCChannel channel;

    public static ContactsFragment newInstance() {
        ContactsFragment fragment = new ContactsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public static Fragment newInstance(FCChannel channel, List<FCUser> users) {
        ContactsFragment fragment = new ContactsFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(USERS, new ArrayList<>(users));
        args.putParcelable(CHANNEL, channel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        FragmentContactsBinding binding = FragmentContactsBinding.inflate(inflater, root, attachToRoot);
        setToolbar(binding);

        List<FCUser> users = getArguments().getParcelableArrayList(USERS);
        channel = getArguments().getParcelable(CHANNEL);

        List<Contact> contacts = new ArrayList<>();
        Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String photoUri = phones.getString(phones.getColumnIndex(ContactsContract.Contacts.Photo.PHOTO_URI));
            String id = phones.getString(phones.getColumnIndex(ContactsContract.PhoneLookup._ID));

            Contact contact = new Contact(id, name, phoneNumber, photoUri);

            boolean exists = false;
            if (users != null) {
                for (FCUser user : users)
                    if (contact.getPhoneCutted().equals(user.getPhone())) {
                        exists = true;
                        break;
                    }
            }

            if (!exists)
                contacts.add(contact);
        }
        phones.close();

        binding.rvListContacts.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        adapter = new ContactsAdapter(getContext(), contacts, this);
        binding.rvListContacts.setAdapter(adapter);
        binding.rvListContacts.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        return binding;
    }

    private void setToolbar(FragmentContactsBinding binding) {
        ((AppCompatActivity) getActivity()).setSupportActionBar(binding.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(ContextCompat.getColor(getContext(), R.color.inbox_bg_home_button), PorterDuff.Mode.SRC_ATOP);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.contacts, menu);
        menu.findItem(R.id.addAll).setVisible(selected.size() != 0);
    }

    @Override
    public void selected(Map<Contact, FCUser> selected) {
        this.selected = selected;
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.addAll) {
            List<FCUser> users = new ArrayList<>();
            for (Map.Entry<Contact, FCUser> entry : selected.entrySet()) {
                users.add(entry.getValue());
            }

            if (channel == null)
                adapter.addUsers(users);
            else
                adapter.insertUsers(channel, users);
        }

        return super.onOptionsItemSelected(item);
    }

}
