package com.fanzine.coys.fragments.start;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.StartActivity;
import com.fanzine.coys.activities.TeaserAcitivity;
import com.fanzine.coys.activities.TermsAndPolicyActivity;
import com.fanzine.coys.activities.base.BaseActivity;
import com.fanzine.coys.adapters.login.CountryAdapter;
import com.fanzine.coys.adapters.login.EmailsAdapter;
import com.fanzine.coys.adapters.login.TextAdapter;
import com.fanzine.coys.databinding.DialogRecyclerviewBinding;
import com.fanzine.coys.databinding.FragmentRegistrationBinding;
import com.fanzine.coys.interfaces.LoadingListener;
import com.fanzine.coys.models.Fullname;
import com.fanzine.coys.models.ProEmail;
import com.fanzine.coys.models.PurchaseDto;
import com.fanzine.coys.models.login.Birthday;
import com.fanzine.coys.models.login.Country;
import com.fanzine.coys.models.login.CountryData;
import com.fanzine.coys.models.login.Teams;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.utils.KeyboardUtils;
import com.fanzine.coys.utils.RecyclerItemClickListener;
import com.fanzine.coys.viewmodels.base.SocialFragment;
import com.fanzine.coys.viewmodels.fragments.login.RegistrationFragmentViewModel;
import com.jakewharton.rxbinding.view.RxView;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Woland on 22.03.2017.
 */

public class RegistrationFragment extends SocialFragment implements
        LoadingListener<Teams>,
        RegistrationFragmentViewModel.SignUpCallback,
        DatePickerDialog.OnDateSetListener {

    private static final String FB_ID = "FB_ID";
    private static final String NAME = "name";
    private static final String COUNTRY = "country";
    private static final String GENDER = "gender";
    private static final String PRO = "pro";
    private static final String FORM_DATA = "form_data";
    public static final int REQUEST_CODE = 1111;


    public static final String PURCHASE = "purchase";
    public static final String PURCHASE_ERROR_CODE = "purchase_error_code";

    public static final int PURCHASE_REQUEST = 4444;
    public static final int PURCHASE_SUCCESS = 20;
    public static final int PURCHASE_ERROR = 40;

    private RegistrationFragmentViewModel viewModel;

    private List<Teams.Team> teams;

    private ProgressDialog pd;

    List<Country> mCountries;

    private BaseActivity mBaseActivity;

    private FragmentRegistrationBinding binding;

    public static RegistrationFragment newInstance(Bundle args) {
        RegistrationFragment fragment = new RegistrationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentRegistrationBinding.inflate(inflater, root, attachToRoot);

        String id = getArguments().getString(FB_ID);
        boolean isPro = getArguments().getBoolean(PRO, false);


        viewModel = new RegistrationFragmentViewModel(getContext(), id, isPro, this, this);
        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);


        if (!isPro && getArguments().getSerializable(FORM_DATA) != null) {
            HashMap<String, String> formData = ( HashMap<String, String>)getArguments().getSerializable(FORM_DATA);
            viewModel.setFormData(formData);
        }

        RxView.clicks(binding.country).subscribe((aVoid) -> openCountryDialog());
        binding.country.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus)
                openCountryDialog();
        });

        RxView.clicks(binding.gender).subscribe((aVoid) -> openGenderDialog());
        binding.gender.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus)
                openGenderDialog();
        });

        RxView.clicks(binding.team).subscribe((aVoid) -> openTeamDialog());
        binding.team.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus)
                openTeamDialog();
        });

        RxView.clicks(binding.birthday).subscribe((aVoid -> openDateBirthdayPicker()));
        binding.birthday.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                openDateBirthdayPicker();
            }
        });

        RxView.clicks(binding.signUp).subscribe(click-> {
            if ( isPro) viewModel.registerInvincible();
            else viewModel.signUp();
        });

        if (!TextUtils.isEmpty(id)) {
            facebookLoggedIn(null, null, id, getArguments().getString(NAME), getArguments().getString(COUNTRY), getArguments().getString(GENDER));
        }

        RxView.clicks(binding.proEmail).subscribe(aVoid -> proClicked());
        binding.proEmail.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus)
                proClicked();
        });

        viewModel.fetchCountries(countryList -> {
            mCountries = countryList;

            if (countryList.size() > 0)
                viewModel.country.set(countryList.get(0));
        });

        mBaseActivity = ((BaseActivity) getActivity());

        configureUI();

        return binding;
    }

    @Override
    public void openTeaser(Fullname fullname) {
        Intent showEmailTeaser = new Intent(getContext(), TeaserAcitivity.class);
        showEmailTeaser.putExtra(TeaserAcitivity.KEY_FULLNAME, fullname);

        getActivity().startActivityForResult(showEmailTeaser, StartActivity.REQUEST_TEASER_EMAIL);
    }

    @Override
    public void openFreeUserPage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.sign_up_pro_alert)
                .setPositiveButton("Yes", (dialog, id) -> {
                    dialog.dismiss();

                    Bundle args = getArguments();
                    args.putBoolean(PRO, false);
                    args.putSerializable(FORM_DATA, viewModel.getFormData());

                    FragmentUtils.changeFragment(getActivity(), R.id.content_frame, RegistrationFragment.newInstance(args), true);
                })
                .setNegativeButton("No", (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                }).show();
    }

    @Override
    public void openTermsAndPolicy() {
        Intent intentTermsAndPolicy = new Intent(getContext(), TermsAndPolicyActivity.class);
        startActivity(intentTermsAndPolicy);
    }

    private void proClicked() {
        viewModel.showTeaser();
        KeyboardUtils.hideSoftKeyboard(getActivity());
    }

    private void configureUI() {
        binding.tvAcceptTerms.setPaintFlags(binding.tvAcceptTerms.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    private void openDateBirthdayPicker() {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        dialog.show();

        KeyboardUtils.hideSoftKeyboard(getActivity());
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        viewModel.birthday.set(new Birthday(dayOfMonth, monthOfYear, year));
    }

    private void openTeamDialog() {
        KeyboardUtils.hideSoftKeyboard(getActivity());
        if (teams != null) {
            DialogRecyclerviewBinding bindingDialog = DialogRecyclerviewBinding.inflate(LayoutInflater.from(getContext()));
            bindingDialog.rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            bindingDialog.rv.setAdapter(new TextAdapter(getContext(), teams));

            bindingDialog.rv.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), (view, position) -> {
                Teams.Team team = teams.get(position);

                viewModel.team.set(team);

                if (dialog != null) {
                    dialog.dismiss();
                    dialog = null;
                }
            }));

            bindingDialog.executePendingBindings();

            dialog = new AlertDialog.Builder(getContext())
                    .setView(bindingDialog.getRoot())
                    .setNegativeButton(R.string.cancel, null)
                    .show();

            dialog.getWindow().setBackgroundDrawableResource(R.color.backgroundDialog);
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        } else {
            new AlertDialog.Builder(getContext())
                    .setMessage(R.string.teamsNotLoaded)
                    .setPositiveButton(R.string.ok, (dialog1, which) -> {
                        pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);
                        viewModel.loadTeams();
                    })
                    .setNegativeButton(R.string.cancel, null)
                    .show();
        }
    }

    private void openGenderDialog() {
        String[] genders = new String[]{"Male", "Female"};
        AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, genders) {
                    @Override
                    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                        View view = super.getView(position, convertView, parent);
                        ((TextView) view).setTextColor(Color.WHITE); // here can be your logic
                        return view;
                    }
                }, (dialog1, which) -> {
                    viewModel.gender.set(genders[which]);
                    dialog1.dismiss();
                })
                .setNegativeButton(R.string.cancel, null)
                .show();
        dialog.getWindow().setBackgroundDrawableResource(R.color.backgroundDialog);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    private AlertDialog dialog = null;

    private void openCountryDialog() {
        KeyboardUtils.hideSoftKeyboard(getActivity());


        DialogRecyclerviewBinding bindingDialog = DialogRecyclerviewBinding.inflate(LayoutInflater.from(getContext()));
        bindingDialog.rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        CountryAdapter adapter = new CountryAdapter(getContext(), mCountries);
        bindingDialog.rv.setAdapter(adapter);

        bindingDialog.rv.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), (view, position) -> {
            Country country = mCountries.get(position);

            viewModel.country.set(country);

            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
            }
        }));

        bindingDialog.executePendingBindings();

        dialog = new AlertDialog.Builder(getContext())
                .setView(bindingDialog.getRoot())
                .setNegativeButton(R.string.cancel, null)
                .show();

        dialog.getWindow().setBackgroundDrawableResource(R.color.backgroundDialog);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    @Override
    public void facebookLoggedIn(ProgressDialog pd, String token, String id, String name, String countryString, String gender) {
        if (pd != null && pd.isShowing())
            pd.dismiss();

        viewModel.setId(id);

        if (!TextUtils.isEmpty(name)) {
            String[] names = name.split(" ", -1);

            if (names.length > 1) {

                String firstName = "";
                for (int i = 0; i < names.length - 1; ++i) {
                    firstName += names[i];

                    if (i < names.length - 2)
                        firstName += " ";
                    else {
                        viewModel.lastName.set(names[i + 1]);
                        break;
                    }
                }

                viewModel.firstName.set(firstName);
            } else
                viewModel.firstName.set(names[0]);
        }

        viewModel.gender.set(gender);

        List<Country> countries = CountryData.getCategories(getContext());
        for (Country country : countries) {
            if (country.getNicename().contains(countryString)) {
                viewModel.country.set(country);

                if (dialog != null) {
                    dialog.dismiss();
                    dialog = null;
                }

                break;
            }
        }

    }

    @Override
    public void onLoaded(Teams data) {
        if (pd != null) {
            pd.dismiss();

            openTeamDialog();
        } else
            teams = data.getTeams();

    }

    @Override
    public void onLoaded(List<ProEmail> data) {
        if (pd != null)
            pd.dismiss();

        DialogRecyclerviewBinding bindingDialog = DialogRecyclerviewBinding.inflate(LayoutInflater.from(getContext()));
        bindingDialog.rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        bindingDialog.rv.setAdapter(new EmailsAdapter(getContext(), data));

        bindingDialog.rv.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), (view, position) -> {
            ProEmail email = data.get(position);

            viewModel.proMail.set(email);

            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
            }
        }));

        bindingDialog.executePendingBindings();

        dialog = new AlertDialog.Builder(getContext())
                .setView(bindingDialog.getRoot())
                .setNegativeButton(R.string.cancel, null)
                .show();

        dialog.getWindow().setBackgroundDrawableResource(R.color.backgroundDialog);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    @Override
    public void onError() {
        if (pd != null)
            pd.dismiss();
    }

    @Override
    public void showPayment() {
        StartActivity startActivity = (StartActivity) getActivity();
        startActivity.startPurchase();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == StartActivity.REQUEST_TEASER_EMAIL && resultCode == RESULT_OK) {
            String email = data.getData().toString();

            ProEmail proEmail = new ProEmail();
            proEmail.setEmail(email);

            viewModel.proMail.set(proEmail);
        } else if (requestCode == PURCHASE_REQUEST) {
            if (resultCode == PURCHASE_SUCCESS) {
                PurchaseDto result = data.getParcelableExtra(PURCHASE);
                // use the result to update your UI and send the payment method nonce to your server
                viewModel.finish(result);
            } else if (resultCode == PURCHASE_ERROR) {
                // the user canceled
                viewModel.deleteUser();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
