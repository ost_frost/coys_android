package com.fanzine.coys.fragments.news;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.GossipsAdapter;
import com.fanzine.coys.databinding.FragmentGossipBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.Gossip;
import com.fanzine.coys.viewmodels.fragments.GossipViewModel;

import java.util.List;

/**
 * Created by Woland on 21.02.2017.
 */

public class GossipFragment extends BaseFragment implements DataListLoadingListener<Gossip> {

    private FragmentGossipBinding binding;

    public static GossipFragment newInstance() {
        Bundle args = new Bundle();
        GossipFragment fragment = new GossipFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentGossipBinding.inflate(inflater, root, attachToRoot);

        GossipViewModel viewModel = new GossipViewModel(getContext(), this);
        binding.setViewModel(viewModel);

        binding.swipe.setColorSchemeResources(R.color.colorAccent);
        binding.swipe.setOnRefreshListener(() -> viewModel.loadData(0));
        binding.rvGossips.setLayoutManager(new LinearLayoutManager(getContext()));

        setBaseViewModel(viewModel);
        viewModel.loadData(0);

        return binding;
    }

    @Override
    public void onLoaded(List<Gossip> data) {
        if (binding.swipe.isRefreshing())
            binding.swipe.setRefreshing(false);
        binding.rvGossips.setAdapter(new GossipsAdapter(getContext(), data));
    }

    @Override
    public void onLoaded(Gossip data) {
        if (binding.swipe.isRefreshing())
            binding.swipe.setRefreshing(false);
    }

    @Override
    public void onError() {
        if (binding.swipe.isRefreshing())
            binding.swipe.setRefreshing(false);
    }
}
