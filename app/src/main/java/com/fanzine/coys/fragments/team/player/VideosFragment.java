package com.fanzine.coys.fragments.team.player;

import android.databinding.ViewDataBinding;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.VideoAdapter;
import com.fanzine.coys.adapters.team.PlayerVideosAdapter;
import com.fanzine.coys.databinding.FragmentTeamPlayerVideosBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.fragments.like.LikeCallback;
import com.fanzine.coys.fragments.like.LikeViewModel;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.Video;
import com.fanzine.coys.models.team.Player;
import com.fanzine.coys.utils.ShareUtil;
import com.fanzine.coys.viewmodels.fragments.team.player.VideosViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbp on 1/10/18.
 */

public class VideosFragment extends BaseFragment
        implements DataListLoadingListener<Video> ,
        LikeCallback<Video> {

    private final static String PLAYER = "player";

    private FragmentTeamPlayerVideosBinding binding;
    private VideosViewModel viewModel;
    private LikeViewModel likeViewModel;

    PlayerVideosAdapter adapter;


    public static VideosFragment getInstance(Player player) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PLAYER, player);

        VideosFragment fragment = new VideosFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentTeamPlayerVideosBinding.inflate(inflater, root, attachToRoot);

        viewModel = new VideosViewModel(getContext(), this, getArguments().getParcelable(PLAYER));
        likeViewModel = new LikeViewModel(getContext());

        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);

        binding.rvvideos.setLayoutManager(new LinearLayoutManager(getContext()));
        DividerItemDecoration divider = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        Drawable dividerDrawable = ContextCompat.getDrawable(getContext(), R.drawable.divider_1dp);
        divider.setDrawable(dividerDrawable);
        binding.rvvideos.addItemDecoration(divider);
        viewModel.loadData(0);

        adapter = new PlayerVideosAdapter(getContext(), new ArrayList<>(), viewModel.getPlayerName());

        return binding;
    }

    @Override
    public void onLoaded(List<Video> data) {

        adapter.addItems(data);

        adapter.setCallback(new VideoAdapter.Callback() {
            @Override
            public void onLike(Video video) {
                if (likeViewModel != null) {
                    if (video.isLiked) {
                        likeViewModel.unlikeVideo(video, VideosFragment.this);
                    } else {
                        likeViewModel.likeVideo(video, VideosFragment.this);
                    }
                }
            }

            @Override
            public void onFbPostShare(Video video) {
                ShareUtil.shareVideoToFacebook(getActivity(), video);
            }

            @Override
            public void onTwitterShare(Video video) {
                ShareUtil.shareVideoToTwitter(getContext(), video);
            }

            @Override
            public void onChatShare(Video video) {
                ShareUtil.shareVideoToChat(getContext(), video);
            }

            @Override
            public void onMailShare(Video video) {
                ShareUtil.shareVideoToMail(getContext(), video);
            }

            @Override
            public void onClipboardCopy(Video video) {
                ShareUtil.copyToClipboardVideo(getContext(), video);
            }
        });

        binding.rvvideos.setAdapter(adapter);
    }

    @Override
    public void onLoaded(Video data) {

    }

    @Override
    public void onError() {

    }


    @Override
    public void onLiked(Video video) {
        if (adapter != null) {
            adapter.refreshItem(video);
        }
    }

    @Override
    public void onUnLiked(Video video) {
        if (adapter != null) {
            adapter.refreshItem(video);
        }
    }
}
