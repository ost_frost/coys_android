package com.fanzine.coys.fragments.table;

import com.fanzine.coys.models.League;
import com.fanzine.coys.models.table.RegionLeague;

/**
 * Created by mbp on 11/22/17.
 */

public interface RegionLeagueClickListener {
    void onLeagueClicked(RegionLeague regionLeague, League league);
}
