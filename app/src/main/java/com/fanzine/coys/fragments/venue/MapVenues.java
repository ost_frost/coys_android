package com.fanzine.coys.fragments.venue;


import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.ViewDataBinding;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.venues.MapBusinessAdapter;
import com.fanzine.coys.databinding.FragmentMapVenuesBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.viewmodels.fragments.venue.MapVenuesViewModel;
import com.fanzine.coys.widgets.MapMarker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.yelp.fusion.client.models.Business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MapVenues extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener,
        MapVenuesViewModel.OnBusinessLoaded, GoogleMap.OnMarkerClickListener, GoogleMap.OnMarkerDragListener,
        MapBusinessAdapter.OnItemClickListener{

    private FragmentMapVenuesBinding binding;
    private MapVenuesViewModel viewModel;

    private static final String DATA = "data";
    private static final String REGION = "region";

    private static final int ZOOM_LEVEL = 14;

    private List<Business> mBusinesses = new ArrayList<>();
    private LatLng mUserLocation;
    private GoogleMap map;

    private Business mSelectedBusiness;

    IconGenerator mIconFactory;

    private Map<Marker, Business> businessMap = new HashMap<>();


    public static MapVenues newInstance() {
        MapVenues fragment = new MapVenues();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        if (binding == null || viewModel == null) {
            binding = FragmentMapVenuesBinding.inflate(inflater, root, false);
            viewModel = new MapVenuesViewModel(getContext(), this);
            binding.setViewModel(viewModel);

            setBaseViewModel(viewModel);

            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

            binding.rvBusiness.setLayoutManager(layoutManager);


            binding.map.onCreate(null);
            binding.map.getMapAsync(this);
            SnapHelper snapHelper = new PagerSnapHelper();
            snapHelper.attachToRecyclerView(binding.rvBusiness);

            int lastSelectedPos = 0;

            binding.rvBusiness.addOnScrollListener(new RecyclerView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if (newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                        View centerView = snapHelper.findSnapView(layoutManager);
                        int pos = layoutManager.getPosition(centerView);

                        MapBusinessAdapter adapter = (MapBusinessAdapter) binding.rvBusiness.getAdapter();

                        if ( pos == adapter.getLastSelectedPosition()) return;


                        Log.i("MAP VENUES SLIDER POS ", String.valueOf(pos));

                        mSelectedBusiness = adapter.getBusiness(pos);

                        focusOnSelectedBusiness();

                        adapter.setLastSelectedPosition(pos);
                    }
                }
            });

            mIconFactory = new IconGenerator(getContext());
        }
        return binding;
    }

    private void focusOnSelectedBusiness() {

        clearMapData();

        addUserMarker(mUserLocation);

        LatLng latLng = new LatLng(mSelectedBusiness.getCoordinates().getLatitude(),
                mSelectedBusiness.getCoordinates().getLongitude());



        attachBusinessMarkers(mBusinesses);
    }

    @Override
    public void showSelectedBusiness(Business business) {
        mSelectedBusiness = business;

        focusOnSelectedBusiness();
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.map.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.map.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.setOnMapClickListener(this);
        map.setOnMarkerClickListener(this);
        map.setOnMarkerDragListener(this);
        this.map = map;
    }

    @Override
    public void onMapClick(LatLng latLng) {
//        if (map != null) {
//            map.clear();
//
//            mUserLocation = latLng;
//
//            getActivity().sendBroadcast(new Intent(ACTION).putExtra("latLng", latLng));
//
//            map.addMarker(new MarkerOptions()
//                    .position(latLng)
//                    .draggable(true)
//                    .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_map_pin))));
//        }
    }

    public void init() {
        if (map != null) {
            map.clear();
            businessMap.clear();
        }
    }

    private void addUserMarker(LatLng latLng) {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        map.getUiSettings().setCompassEnabled(true);
        map.setMyLocationEnabled(true);
    }

    private void focusOnMarker(LatLng latLng) {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, ZOOM_LEVEL));
    }

    private void clearMapData() {
        map.clear();
        businessMap.clear();
    }


    private void attachBusinessMarkers(List<Business> businesses) {
        IconGenerator iconFactory = new IconGenerator(getContext());

        int number = 1;

        for (Business business : businesses) {
            //BitmapDescriptor bitmapDescriptor = getBitmapDescriptor(R.drawable.ic_marker_icon);

            Bitmap icon = getBusinessMarker(business, number);

            Marker markerOptions = map.addMarker(new MarkerOptions()
                    .position(new LatLng(business.getCoordinates().getLatitude(), business.getCoordinates().getLongitude()))
                    .title(business.getName())
                    .icon(BitmapDescriptorFactory.fromBitmap(icon)));

            businessMap.put(markerOptions, business);

            number++;
        }
    }

    private Bitmap getBusinessMarker(Business business, int number) {
        if (mSelectedBusiness == null)
            return buildDefaultPinIcon(number);

        if (!business.getId().equals(mSelectedBusiness.getId()))
            return buildDefaultPinIcon(number);

        return buildSelectedPinIcon(number);
    }

    private Bitmap buildSelectedPinIcon(int number) {
        mIconFactory.setBackground(null);
        mIconFactory.setContentView(new MapMarker(getContext(), String.valueOf(number), R.drawable.ic_marker_icon_white, R.style.BlackText));
        return mIconFactory.makeIcon();
    }

    private Bitmap buildDefaultPinIcon(int number) {
        mIconFactory.setBackground(null);
        mIconFactory.setContentView(new MapMarker(getContext(), String.valueOf(number), R.drawable.ic_marker_icon, R.style.WhiteText));
        return mIconFactory.makeIcon();
    }

    private void setupSliderAdapter(List<Business> businesses) {
        binding.rvBusiness.setAdapter(new MapBusinessAdapter(getContext(), businesses, this));
    }

    public void init(List<Business> businesses, LatLng latLng) {
        mUserLocation = latLng;
        mBusinesses = businesses;

        if (map != null) {
            clearMapData();

            addUserMarker(latLng);
            focusOnMarker(latLng);

            attachBusinessMarkers(businesses);

            setupSliderAdapter(businesses);
        }
    }

    @Override
    public void onLoaded(Business business) {
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (businessMap.size() > 0) {
            Business business = businessMap.get(marker);
            if (business != null) {
                viewModel.business.set(business);

                int toScrollPosition = ((MapBusinessAdapter)binding.rvBusiness.getAdapter()).getPosition(business);
                binding.rvBusiness.smoothScrollToPosition(toScrollPosition);
            }
            return true;
        } else
            return false;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        onMapClick(marker.getPosition());
    }

    public static Marker findMarker(Map<Marker, Business> map, Business value) {
        for (Map.Entry<Marker, Business> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }
}
