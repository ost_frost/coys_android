package com.fanzine.coys.fragments.base;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.databinding.ItemTopNavigationBinding;
import com.fanzine.coys.models.League;
import com.fanzine.coys.viewmodels.items.ItemTopPanelViewModel;
import com.gtomato.android.ui.transformer.CoverFlowViewTransformer;
import com.gtomato.android.ui.widget.CarouselView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximdrobonoh on 25.09.17.
 */

public abstract class BaseLeaguesBarFragment extends BaseFragment {
    public static final int LEAGUE_ONE_PLAY_OFFS = 1206;

    protected static int selected = -1;
    protected static List<League> leagues;

    private LeaguesAdapter leaguesAdapter;

    private boolean isFirsInit = true;

    public abstract void onTopPanelItemSelected(View view, int position);

    public static int getSelectedLeagueId() {
        return selected;
    }

    public static String getSelectedLeagueName() {
        for (League league : leagues) {
            if (league.getLeagueId() == selected)
                return league.getName();
        }
        return "";
    }

    public static boolean isPremierLeague() {
        return selected == LEAGUE_ONE_PLAY_OFFS;
    }

    public static void setLeagueId(int leagueId) {
        selected = leagueId;
    }

    private int getSliderScrollPosition() {
        if (leagues.size() > 0) {
            return leaguesAdapter.getItemPosition(selected);
        }
        return 0;
    }

    public void initTopPanel(List<League> data, CarouselView topPanel, int maxVisibileItems) {
        Resources r = getResources();
        int itemWidth = (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 75, r.getDisplayMetrics()));
        topPanel.getLayoutParams().width = itemWidth * maxVisibileItems;

        if (leagues == null)
            leagues = new ArrayList<>();
        leagues.addAll(data);


        leaguesAdapter = new LeaguesAdapter(data);

        CoverFlowViewTransformer transformer1 = new CoverFlowViewTransformer() {
            @Override
            public void transform(View view, float position) {
                int width = view.getMeasuredWidth(), height = view.getMeasuredHeight();
                view.setPivotX(width / 2.0f);
                view.setPivotY(height / 2.0f);
                view.setTranslationX(width * position * mOffsetXPercent);
                view.setRotationY(Math.signum(position) * (float) (Math.log(Math.abs(position) + 1) / Math.log(3)));
                view.setScaleY(1 + mScaleYFactor * Math.abs(position));
                view.setScaleX(1 + mScaleYFactor * Math.abs(position));
            }
        };

        transformer1.setOffsetXPercent(0.4f);
        transformer1.setYProjection(0);
        transformer1.setScaleYFactor(-0.10f);
        topPanel.setTransformer(transformer1);

        topPanel.setOnItemClickListener((adapter1, view, position, adapterPosition) -> {
            if (adapter1.getItemCount() > 0) {
                topPanel.smoothScrollToPosition(position);
                selected = data.get(position).getLeagueId();

                onTopPanelItemSelected(view, position);
            }
        });

        topPanel.setAdapter(leaguesAdapter);
        topPanel.scrollToPosition(getSliderScrollPosition());
        topPanel.smoothScrollToPosition(getSliderScrollPosition());
        topPanel.getLayoutManager().scrollToPosition(getSliderScrollPosition());

        /*
        todo: uses isFristInit var to detect first init Carousel as callback onItemSelected is firing during initialization
         */
        topPanel.setOnItemSelectedListener(new CarouselView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(CarouselView carouselView, int position, int adapterPosition, RecyclerView.Adapter adapter) {
                    if (data.get(adapterPosition).getLeagueId() != selected && !isFirsInit) {
                    selected = ((LeaguesAdapter) adapter).getLeague(adapterPosition).getLeagueId();
                    onTopPanelItemSelected(carouselView.getFocusedChild(), adapterPosition);
                }
                isFirsInit = false;
            }
            @Override
            public void onItemDeselected(CarouselView carouselView, int position, int adapterPosition, RecyclerView.Adapter adapter) {

            }
        });
    }

    public LeaguesAdapter getLeaguesAdapter() {
        return leaguesAdapter;
    }


    public class LeaguesAdapter extends CarouselView.Adapter<LeaguesAdapter.Holder> {
        private List<League> leagues;

        public LeaguesAdapter(List<League> leagues) {
            if (leagues == null) {
                leagues = new ArrayList<>();
            }
            this.leagues = leagues;
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new LeaguesAdapter.Holder(ItemTopNavigationBinding.inflate
                    (getLayoutInflater(), parent, false));
        }

        public void add(League league) {
            leagues.add(league);
            notifyDataSetChanged();
        }

        public void clear() {
            leagues.clear();
            notifyDataSetChanged();
        }

        public void addAll(List<League> data) {
            this.leagues.addAll(data);
        }

        @Override
        public void onBindViewHolder(LeaguesAdapter.Holder holder, int position) {
            holder.init(position);
        }

        public int getItemPosition(League league) {
            for (int i = 0; i < leagues.size(); i++) {
                if (leagues.get(i).getLeagueId() == league.getLeagueId()) {
                    return i;
                }
            }
            return 0;
        }

        public int getItemPosition(int leagueId) {
            for (int i = 0; i < leagues.size(); i++) {
                if (leagues.get(i).getLeagueId() == leagueId) {
                    return i;
                }
            }
            return 0;
        }

        public League getLeague(int position) {
            return leagues.get(position);
        }

        @Override
        public int getItemCount() {
            return leagues.size();
        }

        class Holder extends RecyclerView.ViewHolder {

            private final ItemTopNavigationBinding binding;

            Holder(ItemTopNavigationBinding binding) {
                super(binding.getRoot());

                this.binding = binding;
            }

            void init(int position) {
                if (binding.getViewModel() == null)
                    binding.setViewModel(new ItemTopPanelViewModel(getContext()));

                League league = leagues.get(position);

                binding.view2.setUseCompatPadding(true);
                Picasso.with(getContext()).load(league.getIcon()).noFade()
                        .into(binding.view2);

                binding.executePendingBindings();
            }
        }
    }
}
