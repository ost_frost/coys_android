package com.fanzine.coys.fragments.team;

import android.os.Bundle;

import com.fanzine.coys.models.team.TeamSquad;

/**
 * Created by vitaliygerasymchuk on 2/20/18
 */

public class AcademyFragment extends AbsPlayersFragment {

    public static AcademyFragment newInstance() {
        AcademyFragment fragment = new AcademyFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected TeamSquad getSquad() {
        return TeamSquad.ACADEMY;
    }
}
