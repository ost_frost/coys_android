package com.fanzine.coys.fragments.chat;


import android.Manifest;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.chat.ListChatsAdapter;
import com.fanzine.coys.databinding.FragmentListChatsBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.viewmodels.fragments.chat.ListChatViewModel;
import com.fanzine.chat.models.channels.FCChannel;
import com.jakewharton.rxbinding.view.RxView;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.util.List;


public class ListChatsFragment extends BaseFragment implements DataListLoadingListener<FCChannel> {

    private FragmentListChatsBinding binding;
    private ListChatViewModel viewModel;

    public static ListChatsFragment newInstance() {
        ListChatsFragment fragment = new ListChatsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        if (binding == null || viewModel == null) {
            binding = FragmentListChatsBinding.inflate(inflater, root, attachToRoot);
            viewModel = new ListChatViewModel(getContext(), this);
            binding.setViewModel(viewModel);

            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            binding.rvListChats.setLayoutManager(linearLayoutManager);

            RxView.clicks(binding.btbNew).subscribe(aVoid -> new RxPermissions(getActivity()).request(Manifest.permission.READ_CONTACTS).subscribe(aBoolean -> {
                if (aBoolean) {
                    FragmentUtils.changeFragment(getActivity(), R.id.content_frame, ContactsFragment.newInstance(), true);
                } else
                    DialogUtils.showAlertDialog(getContext(), R.string.contactsNeeded);
            }));

            binding.rvListChats.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    try {
                        int firstPos = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                        if (firstPos > 0) {
                            binding.swipe.setEnabled(false);
                        } else {
                            binding.swipe.setEnabled(true);
                            if(recyclerView.getScrollState() == 1)
                                if(binding.swipe.isRefreshing())
                                    recyclerView.stopScroll();
                        }

                    }catch(Exception e) {
                        Log.e(ListChatsFragment.class.getName(), "Scroll Error : "+e.getLocalizedMessage());
                    }
                }
            });

            binding.swipe.setOnRefreshListener(() -> viewModel.loadData(0));

            binding.swipe.setColorSchemeResources(R.color.colorAccent);
        }

        return binding;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (viewModel != null) {
            viewModel.loadData(0);
        }
    }

    @Override
    public void onLoaded(List<FCChannel> data) {
        if (binding.swipe.isRefreshing())
            binding.swipe.setRefreshing(false);

        if (binding != null && getContext() != null)
            binding.rvListChats.setAdapter(new ListChatsAdapter(getContext(), data));
    }

    @Override
    public void onLoaded(FCChannel data) {

    }

    @Override
    public void onError() {

    }
}
