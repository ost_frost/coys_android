package com.fanzine.coys.fragments.table;

import android.databinding.ViewDataBinding;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.table.FilterAdapter;
import com.fanzine.coys.adapters.table.TeamsAdapter;
import com.fanzine.coys.databinding.FragmentTableTeamsBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.interfaces.DataListFilterListener;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.interfaces.FilterClickListener;
import com.fanzine.coys.models.table.Filter;
import com.fanzine.coys.models.table.TeamScore;
import com.fanzine.coys.viewmodels.fragments.table.TeamsViewModel;

import java.util.List;

/**
 * Created by maximdrobonoh on 28.09.17.
 */

public class TeamsFragment extends BaseFragment implements
        DataListLoadingListener<TeamScore>,
        DataListFilterListener<Filter>,
        FilterClickListener {

    private FragmentTableTeamsBinding binding;
    private TeamsViewModel viewModel;


    private TeamsAdapter adapter;

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {

        binding = FragmentTableTeamsBinding.inflate(inflater, root, attachToRoot);
        viewModel = new TeamsViewModel(getContext(), this,this);

        binding.setViewModel(viewModel);
        setBaseViewModel(viewModel);

        viewModel.loadFilters();

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        final LinearLayoutManager filterLinearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        binding.teamScoreList.setLayoutManager(linearLayoutManager);
        binding.filtersList.setLayoutManager(filterLinearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(binding.teamScoreList.getContext(),
                linearLayoutManager.getOrientation());
        binding.teamScoreList.addItemDecoration(dividerItemDecoration);

        binding.arrowDown.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.image_click_animation));
            int totalItemCount = binding.filtersList.getAdapter().getItemCount();
            if (totalItemCount <= 0) return;
            int lastVisibleItemIndex =  filterLinearLayoutManager.findLastVisibleItemPosition();

            if (lastVisibleItemIndex >= totalItemCount) return;
            filterLinearLayoutManager.smoothScrollToPosition(binding.filtersList,null,lastVisibleItemIndex+1);
        });

        return binding;
    }

    @Override
    public void onLoaded(List<TeamScore> data) {
        adapter = new TeamsAdapter(getContext(),data);
        binding.teamScoreList.setAdapter(adapter);
    }

    @Override
    public void onLoaded(TeamScore data) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFilterLoaded(List<Filter> data) {
        FilterAdapter filterAdapter = new FilterAdapter(getContext(), data, this);
        binding.filtersList.setAdapter(filterAdapter);
        filterAdapter.notifyDataSetChanged();

        if ( data.size() > 0 ) {
            viewModel.loadData(data.get(0));
        }
    }

    @Override
    public void onFilterLoaded(Filter data) {

    }

    @Override
    public void onFilterError() {

    }

    @Override
    public void onClick(Filter filter) {
        adapter.clear();
        viewModel.loadData(filter);
    }
}
