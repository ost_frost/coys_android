package com.fanzine.coys.fragments.profile.tab;

import android.app.AlertDialog;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.DialogProfileNotificationsBinding;
import com.fanzine.coys.databinding.FragmentProfileNotificationsTabBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.fragments.profile.ProfileLeagueTypesAdapter;
import com.fanzine.coys.fragments.profile.ProfileLeaguesAdapter;
import com.fanzine.coys.fragments.profile.ProfileTeamsAdapter;
import com.fanzine.coys.models.profile.League;
import com.fanzine.coys.models.profile.LeagueType;
import com.fanzine.coys.models.profile.Notifications;
import com.fanzine.coys.models.profile.Team;
import com.fanzine.coys.viewmodels.fragments.profile.ProfileNotificationViewModel;
import com.jakewharton.rxbinding.view.RxView;

import java.util.List;

/**
 * Created by vitaliygerasymchuk on 2/12/18
 */

public class ProfileNotificationsTabFragment extends BaseFragment implements ProfileNotificationViewModel.Callback, ProfileLeagueTypesAdapter.Callback, ProfileLeaguesAdapter.Callback, ProfileTeamsAdapter.Callback {

    public static ProfileNotificationsTabFragment newInstance() {
        Bundle args = new Bundle();
        ProfileNotificationsTabFragment fragment = new ProfileNotificationsTabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    private FragmentProfileNotificationsTabBinding binding;
    @Nullable
    private ProfileNotificationViewModel viewModel;
    @NonNull
    private final ProfileLeagueTypesAdapter leagueTypesAdapter = new ProfileLeagueTypesAdapter();
    @NonNull
    private final ProfileLeaguesAdapter leaguesAdapter = new ProfileLeaguesAdapter();
    @NonNull
    private final ProfileTeamsAdapter teamsAdapter = new ProfileTeamsAdapter();

    @Nullable
    private AlertDialog dialogNotifications;

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentProfileNotificationsTabBinding.inflate(inflater, root, attachToRoot);

        final RecyclerView recyclerView = binding.recView;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(leagueTypesAdapter);
        leagueTypesAdapter.setCallback(this);
        leaguesAdapter.setCallback(this);
        teamsAdapter.setCallback(this);

        viewModel = new ProfileNotificationViewModel(getContext());
        viewModel.setHeaderTitle(getString(R.string.leagues));
        binding.setViewModel(viewModel);
        viewModel.getLeaguesAndTeams(this);
        return binding;
    }

    @Override
    public void onLeagueTypes(@NonNull List<LeagueType> types) {
        leagueTypesAdapter.setLeagueTypes(types);
    }

    @Override
    public void onLeagueTypeSelected(@NonNull LeagueType type) {
        if (viewModel != null && binding != null) {
            viewModel.setLeagueType(type);
            binding.recView.setAdapter(leaguesAdapter);
            leaguesAdapter.setLeagues(type.getLeagues());
        }
    }

    @Override
    public void onLeagueSelected(@NonNull League league) {
        if (viewModel != null && binding != null) {
            viewModel.setLeague(league);
            binding.recView.setAdapter(teamsAdapter);
            teamsAdapter.setTeams(league.getTeams());
        }
    }

    @Override
    public void onTeamSelected(@NonNull Team team) {
        if (viewModel != null) {
            viewModel.setTeam(team);
            openNotificationDialog(team);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (viewModel != null) {
            viewModel.isVisible = isVisibleToUser;
        }
    }

    private void openNotificationDialog(@NonNull Team team) {
        DialogProfileNotificationsBinding dialogBinding = DialogProfileNotificationsBinding.inflate(LayoutInflater.from(getContext()));
        dialogBinding.setNotifications(team.getNotifications());

        dialogNotifications = new AlertDialog.Builder(getContext())
                .setView(dialogBinding.getRoot())
                .show();

        RxView.clicks(dialogBinding.checkSelectAll).subscribe((aVoid -> {
            setAllChecked(dialogBinding, dialogBinding.checkSelectAll.isChecked());
        }));

        RxView.clicks(dialogBinding.save).subscribe((aVoid -> {
            if (dialogNotifications != null) {
                dialogNotifications.dismiss();
                dialogNotifications = null;
            }

            Notifications notifications = team.getNotifications();
            notifications.setFullTime(dialogBinding.checkFullTime.isChecked());
            notifications.setGoals(dialogBinding.checkGoals.isChecked());
            notifications.setHalfTime(dialogBinding.checkHalfTime.isChecked());
            notifications.setKickOf(dialogBinding.checkKickOff.isChecked());
            notifications.setRedCards(dialogBinding.checkRedCard.isChecked());
            notifications.setLineUp(dialogBinding.checkLineUp.isChecked());
            notifications.setPenalty(dialogBinding.checkPenalty.isChecked());

            if (viewModel != null) {
                viewModel.setTeamNotifications(notifications);

                if (notifications.isNotificationsActive())
                    viewModel.subsribeOnNotifications();
                else
                    viewModel.unsubscribeOnNotifications();
            }
        }));

        RxView.clicks(dialogBinding.cancel).subscribe((aVoid -> {
            if (dialogNotifications != null) {
                dialogNotifications.dismiss();
                dialogNotifications = null;
            }
        }));
    }

    private void setAllChecked(DialogProfileNotificationsBinding binding, boolean checked) {
        binding.checkLineUp.setChecked(checked);
        binding.checkKickOff.setChecked(checked);
        binding.checkHalfTime.setChecked(checked);
        binding.checkFullTime.setChecked(checked);
        binding.checkGoals.setChecked(checked);
        binding.checkRedCard.setChecked(checked);
        binding.checkPenalty.setChecked(checked);
    }
}
