package com.fanzine.coys.fragments.table;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.table.TableAdapter;
import com.fanzine.coys.adapters.table.TableSection;
import com.fanzine.coys.databinding.FragmentTableTablesBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.fragments.news.NewsFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.table.Standings;
import com.fanzine.coys.models.table.TeamTable;
import com.fanzine.coys.viewmodels.fragments.table.TablesFragmentViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderDecoration;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;

/**
 * Created by maximdrobonoh on 22.09.17.
 */

public class TablesFragment extends BaseFragment implements
        DataListLoadingListener<Standings> {

    private FragmentTableTablesBinding binding;
    private StickyHeaderDecoration headersDecor;

    public static TablesFragment newInstance() {
        Bundle args = new Bundle();
        TablesFragment fragment = new TablesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {


        binding = FragmentTableTablesBinding.inflate(inflater, root, attachToRoot);

        TablesFragmentViewModel viewModel = new TablesFragmentViewModel(getContext(), this);
        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);

        binding.swipe.setColorSchemeResources(R.color.colorAccent);
        binding.swipe.setOnRefreshListener(() -> viewModel.loadData(0));

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        binding.rvTeamList.setLayoutManager(linearLayoutManager);
        binding.rvTeamList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                try {
                    int firstPos = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                    if (firstPos > 0) {
                        binding.swipe.setEnabled(false);
                    } else {
                        binding.swipe.setEnabled(true);
                        if (recyclerView.getScrollState() == 1)
                            if (binding.swipe.isRefreshing())
                                recyclerView.stopScroll();
                    }

                } catch (Exception e) {
                    Log.e(NewsFragment.class.getName(), "Scroll Error : " + e.getLocalizedMessage());
                }
            }
        });

        viewModel.loadData(0);
        return binding;
    }


    @Override
    public void onLoaded(List<Standings> data) {

    }

    @Override
    public void onLoaded(Standings standing) {
        List<TeamTable> data = standing.getTeams();
        if (binding.swipe.isRefreshing())
            binding.swipe.setRefreshing(false);

        Collections.sort(data, (o1, o2) -> {
            int comp = o1.getGroup().compareTo(o2.getGroup());
            if (comp == 0)
                compare(o1.getPosition(), o2.getPosition());

            return comp;
        });


        if (headersDecor != null)
            binding.rvTeamList.removeItemDecoration(headersDecor);

        if (!TextUtils.isEmpty(data.get(0).getGroup())) {

            Map<String, List<TeamTable>> groups = new TreeMap<>();

            String lastGroup = "";

            for (int i = 0; i < data.size(); ++i) {
                if (!lastGroup.equals(data.get(i).getGroup())) {
                    lastGroup = data.get(i).getGroup();
                    groups.put(lastGroup, new ArrayList<>());
                }
                groups.get(lastGroup).add(data.get(i));
            }

            SectionedRecyclerViewAdapter sectionAdapter = new SectionedRecyclerViewAdapter();

            for (Map.Entry<String, List<TeamTable>> entry : groups.entrySet()) {
                TableSection tableSection =
                        new TableSection(entry.getValue(), entry.getKey(), standing.getBands());
                sectionAdapter.addSection(tableSection);
            }

            binding.rvTeamList.setNestedScrollingEnabled(false);
            binding.rvTeamList.setAdapter(sectionAdapter);

        } else {
            TableAdapter adapter = new TableAdapter(getContext(), data, standing.getBands());
            binding.rvTeamList.setAdapter(adapter);
        }
    }

    @Override
    public void onError() {

    }

    private static int compare(int x, int y) {
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }
}
