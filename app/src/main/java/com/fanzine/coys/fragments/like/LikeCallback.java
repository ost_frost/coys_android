package com.fanzine.coys.fragments.like;

public interface LikeCallback<T> {
    void onLiked(T t);

    void onUnLiked(T t);
}