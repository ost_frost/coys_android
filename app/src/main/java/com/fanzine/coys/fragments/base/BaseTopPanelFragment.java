package com.fanzine.coys.fragments.base;

import com.fanzine.coys.viewmodels.base.BaseTopPanelViewModel;
import com.gtomato.android.ui.widget.CarouselView;

/**
 * Created by Woland on 13.01.2017.
 */

public abstract class BaseTopPanelFragment extends BaseLeaguesBarFragment {

    protected void initTopPanel(BaseTopPanelViewModel viewModel, CarouselView topPanel, boolean canChange) {
        if (leagues == null) {
            viewModel.loadLeagues();
        }
        else {
            viewModel.setLeaguesLoaded(true);
            initTopPanel(leagues, topPanel, 14);
            viewModel.startDataLoading(0);
        }
    }
}
