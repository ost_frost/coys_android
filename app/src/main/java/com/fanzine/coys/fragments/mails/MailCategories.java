package com.fanzine.coys.fragments.mails;


import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.mails.MailCategoriesAdapter;
import com.fanzine.coys.databinding.DialogAddMailboxBinding;
import com.fanzine.coys.databinding.FragmentMailCategoriesBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.mails.Folder;
import com.fanzine.coys.utils.RecyclerItemClickListener;
import com.fanzine.coys.viewmodels.dialogs.AddMailboxViewModel;
import com.fanzine.coys.viewmodels.fragments.mails.MailCategoriesViewModel;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;

@Deprecated
public class MailCategories extends BaseFragment implements DataListLoadingListener<Folder>, MailCategoriesClickCallback {

    private List<String> categories = new ArrayList<>();

    private MailCategoriesAdapter adapter;

    private MailCategoriesViewModel viewModel;
    private FragmentMailCategoriesBinding binding;


    public static MailCategories newInstance() {
        MailCategories fragment = new MailCategories();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void showToolbar() {
        binding.toolbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideToolbar() {
        binding.toolbar.setVisibility(View.GONE);
    }

    @Override
    public void deleteFolder(String folder) {

        viewModel.removeFolder(folder);
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {

        binding = FragmentMailCategoriesBinding.inflate(inflater, root, attachToRoot);
        viewModel = new MailCategoriesViewModel(getContext(), this);
        adapter = new MailCategoriesAdapter(getContext(), categories, this);

        binding.rvCategories.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, attachToRoot));
        binding.rvCategories.setAdapter(adapter);
        binding.rvCategories.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), (view, position)
                        -> {
                    if ( adapter.actionMode != null) {
                        adapter.actionMode.finish();
                    }
//                    FragmentUtils.changeFragment(getActivity(), R.id.content_frame,
//                            ListEmailActivity.newInstance(categories.get(position)), false);

                }));


        setBaseViewModel(viewModel);

        setHasOptionsMenu(true);
        setMenuVisibility(true);

        RxView.clicks(binding.addMailbox).subscribe(aVoid -> {
            openAddMailboxDialog();
        });

        binding.linlaHeaderProgress.setVisibility(View.VISIBLE);
        viewModel.loadData(0);


        return binding;
    }


    @Override
    public void onLoaded(List<Folder> data) {

        List<String> folders = new ArrayList<>();

        for (Folder folder : data) {
            folders.add(folder.getName());
        }

        binding.linlaHeaderProgress.setVisibility(View.GONE);

        //todo:: move the onClick li
        categories = folders;

        //todo
        categories.add("Flagged");

        adapter = new MailCategoriesAdapter(getContext(), folders, this);
        binding.rvCategories.setAdapter(adapter);
    }

    @Override
    public void onLoaded(Folder data) {
        binding.linlaHeaderProgress.setVisibility(View.GONE);
    }

    @Override
    public void onError() {
        binding.linlaHeaderProgress.setVisibility(View.VISIBLE);
    }

    private void openAddMailboxDialog() {
        AddMailboxViewModel viewModel = new AddMailboxViewModel(getContext(), adapter);

        DialogAddMailboxBinding bindingDialog = DialogAddMailboxBinding.inflate(LayoutInflater.from(getContext()));
        bindingDialog.setViewModel(viewModel);
        bindingDialog.executePendingBindings();

        AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setView(bindingDialog.getRoot()).show();

        RxView.clicks(bindingDialog.addMailbox).subscribe(aVoid -> {
            viewModel.addMailbox(dialog);
        });
    }
}
