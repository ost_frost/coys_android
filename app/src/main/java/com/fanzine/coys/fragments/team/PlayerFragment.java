package com.fanzine.coys.fragments.team;


import android.databinding.ViewDataBinding;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.team.PlayerInfoPagerAdapter;
import com.fanzine.coys.databinding.FragmentPlayerBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.team.PlayerInfo;
import com.fanzine.coys.viewmodels.fragments.team.PlayerInfoFragmentViewModel;


public class PlayerFragment extends BaseFragment {

    private static final String PLAYER = "player";

    public static PlayerFragment newInstance(PlayerInfo data) {
        PlayerFragment fragment = new PlayerFragment();
        Bundle args = new Bundle();
        args.putParcelable(PLAYER, data);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        FragmentPlayerBinding binding = FragmentPlayerBinding.inflate(inflater, root, attachToRoot);
        binding.blur.setOverlayColor(Color.parseColor("#60000000"));
        binding.blur.setBlurRadius(getResources().getDimension(R.dimen.blur_radius));

        PlayerInfoFragmentViewModel viewModel = new PlayerInfoFragmentViewModel(getContext());

        PlayerInfo player = getArguments().getParcelable(PLAYER);

        viewModel.player.set(player);

        binding.setViewModel(viewModel);
        setBaseViewModel(viewModel);

        PlayerInfoPagerAdapter pagerAdapterapter = new PlayerInfoPagerAdapter(getContext(), getChildFragmentManager(), player);
        binding.viewPager.setAdapter(pagerAdapterapter);

        binding.tabs.setupWithViewPager(binding.viewPager);

        for (int i = 0; i < binding.tabs.getTabCount(); i++) {
            TabLayout.Tab tab = binding.tabs.getTabAt(i);
            tab.setCustomView(pagerAdapterapter.getTabView(i));
        }

        binding.executePendingBindings();

        return binding;
    }
}
