package com.fanzine.coys.fragments.match;


import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.match.CommentaryAdapter;
import com.fanzine.coys.databinding.FragmentMatchCommentsBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.models.MatchComment;
import com.fanzine.coys.viewmodels.fragments.match.MatchCommentsFragmentViewModel;

import java.util.ArrayList;
import java.util.List;

public class CommentaryFragment extends BaseFragment implements DataListLoadingListener<MatchComment> {


    private static final String MATCH = "match";
    private FragmentMatchCommentsBinding binding;

    public static CommentaryFragment newInstance(Match match) {
        CommentaryFragment fragment = new CommentaryFragment();
        Bundle args = new Bundle();
        args.putParcelable(MATCH, match);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentMatchCommentsBinding.inflate(inflater, root, false);

        Match match = getArguments().getParcelable(MATCH);

        MatchCommentsFragmentViewModel viewModel = new MatchCommentsFragmentViewModel(getContext(), match, this);
        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);

        binding.rvContent.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true));
        binding.rvContent.setNestedScrollingEnabled(false);

        return binding;
    }

    @Override
    public void onLoaded(List<MatchComment> data) {
        List<MatchComment> comments = removeGaps(data);

        binding.rvContent.setAdapter(new CommentaryAdapter(getContext(), comments));
    }

    @Override
    public void onLoaded(MatchComment data) {

    }

    @Override
    public void onError() {

    }

    private List<MatchComment> removeGaps(List<MatchComment> data) {
        List<MatchComment> newList = new ArrayList<>();
        for (MatchComment comment : data) {
            if ( !(comment.getIsGoal() == 0 && comment.getImportant() == 1)) {
                newList.add(comment);
            }
        }
        return newList;
    }
}
