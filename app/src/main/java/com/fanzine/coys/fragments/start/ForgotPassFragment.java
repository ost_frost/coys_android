package com.fanzine.coys.fragments.start;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.WindowManager;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.login.CountryAdapter;
import com.fanzine.coys.databinding.DialogForgotLoginBinding;
import com.fanzine.coys.databinding.DialogRecyclerviewBinding;
import com.fanzine.coys.dialogs.base.BaseDialog;
import com.fanzine.coys.models.login.Country;
import com.fanzine.coys.models.login.CountryData;
import com.fanzine.coys.utils.RecyclerItemClickListener;
import com.fanzine.coys.viewmodels.fragments.login.ForgotPassViewModel;
import com.jakewharton.rxbinding.view.RxView;

import java.util.List;

/**
 * Created by Woland on 03.04.2017.
 */

public class ForgotPassFragment  extends BaseDialog implements DialogInterface {

    private AlertDialog dialog;
    private ForgotPassViewModel viewModel;

    private List<Country> mCountries;

    public static ForgotPassFragment newInstance() {
        Bundle args = new Bundle();
        ForgotPassFragment fragment = new ForgotPassFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void cancel() {
        dismiss();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        DialogForgotLoginBinding binding = DialogForgotLoginBinding.inflate(LayoutInflater.from(getContext()), null);
        viewModel = new ForgotPassViewModel(getContext(), this);

        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);

        builder.setView(binding.getRoot());

        RxView.clicks(binding.country).subscribe((aVoid) -> openCountryDialog());

        binding.country.setFocusable(false);

        mCountries = CountryData.getCategories(getContext());
        viewModel.country.set(mCountries.get(0));

        viewModel.fetchCountries(countryList -> {
            mCountries = countryList;
            if (countryList.size() > 0)
                viewModel.country.set(countryList.get(0));
        });

        return builder.create();
    }


    private void openCountryDialog() {

        DialogRecyclerviewBinding bindingDialog = DialogRecyclerviewBinding.inflate(LayoutInflater.from(getContext()));
        bindingDialog.rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        CountryAdapter adapter = new CountryAdapter(getContext(), mCountries);
        bindingDialog.rv.setAdapter(adapter);

        bindingDialog.rv.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), (view, position) -> {
            Country country = mCountries.get(position);

            viewModel.country.set(country);

            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
            }
        }));

        bindingDialog.executePendingBindings();

        dialog = new AlertDialog.Builder(getContext())
                .setView(bindingDialog.getRoot())
                .setNegativeButton(R.string.cancel, null)
                .show();

        dialog.getWindow().setBackgroundDrawableResource(R.color.backgroundDialog);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

}
