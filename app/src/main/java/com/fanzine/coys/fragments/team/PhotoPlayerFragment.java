package com.fanzine.coys.fragments.team;


import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.databinding.FragmentPhotoPlayerBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.team.Photo;

import java.util.ArrayList;
import java.util.List;


@Deprecated
public class PhotoPlayerFragment extends BaseFragment {

    private static final String DATA = "data";

    public static PhotoPlayerFragment newInstance(List<Photo> photos) {
        PhotoPlayerFragment fragment = new PhotoPlayerFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(DATA, new ArrayList<>(photos));
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        FragmentPhotoPlayerBinding binding = FragmentPhotoPlayerBinding.inflate(inflater, root, attachToRoot);

        List<Photo> photos = getArguments().getParcelableArrayList(DATA);
//
//        binding.rvPhotos.setLayoutManager(new GridLayoutManager(getContext(), 3));
//        binding.rvPhotos.setAdapter(new PhotoPlayerAdapter(getContext(), photos));
//        binding.rvPhotos.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), (view, position) ->
//                FragmentUtils.changeFragment(getActivity(), R.id.content_frame, FullScreenPhotoFragment.newInstance(photos.get(position)), true)));
        return binding;
    }
}
