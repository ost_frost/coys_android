package com.fanzine.coys.fragments.news;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.databinding.FragmentNewsBaseBinding;
import com.fanzine.coys.fragments.base.BaseLeaguesBarFragment;
import com.fanzine.coys.models.League;
import com.fanzine.coys.viewmodels.base.BaseLeaguesBarViewModel;
import com.fanzine.coys.viewmodels.fragments.news.FeedViewModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by mbp on 11/8/17.
 */

public class FeedBaseFragment extends BaseLeaguesBarFragment implements
        BaseLeaguesBarViewModel.LeagueLoadingListener {

    private FragmentNewsBaseBinding binding;
    private FeedViewModel viewModel;

    public static FeedBaseFragment newInstance() {
        Bundle args = new Bundle();
        FeedBaseFragment fragment = new FeedBaseFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        viewModel = new FeedViewModel(getContext(), getChildFragmentManager(), this);

        binding = FragmentNewsBaseBinding.inflate(inflater, root, false);
        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);

        viewModel.loadLeagues();

        return binding;
    }

    @Override
    public void onLeagueLoaded(List<League> list) {

        if ( list.size() > 0) {

            Collections.sort(list, (league, t1) -> league.getListed_region_sort() - t1.getListed_region_sort());


            int leagueId = list.get(list.size() / 2).getLeagueId();

            BaseLeaguesBarFragment.selected = leagueId;
            viewModel.setLeagueId(leagueId);

            initTopPanel(list, binding.topPanel, 4);

            viewModel.openNews();
        }
    }

    @Override
    public void onTopPanelItemSelected(View view, int position) {
        viewModel.setLeagueId(leagues.get(position).getLeagueId());
        viewModel.update();
    }
}
