package com.fanzine.coys.fragments.venue;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.base.NavigationFragmentActivity;
import com.fanzine.coys.adapters.CarouselAdapter;
import com.fanzine.coys.databinding.FragmentVenueBinding;
import com.fanzine.coys.exceptions.GPSProviderIsDisabled;
import com.fanzine.coys.exceptions.LocationIsNull;
import com.fanzine.coys.exceptions.LocationPermissionDenied;
import com.fanzine.coys.exceptions.NetworkLocationIsDisabled;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.CarouselFilter;
import com.fanzine.coys.models.venues.Category;
import com.fanzine.coys.models.venues.Rating;
import com.fanzine.coys.models.venues.VenueFilter;
import com.fanzine.coys.models.venues.VenuesFilter;
import com.fanzine.coys.models.venues.location.BaseRequest;
import com.fanzine.coys.models.venues.location.LatLngRequest;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.utils.GeoHelper;
import com.fanzine.coys.viewmodels.fragments.venue.VenueFragmentViewModel;

import com.fanzine.venues.TravelFragment;
import com.google.android.gms.maps.model.LatLng;
import com.gtomato.android.ui.transformer.CoverFlowViewTransformer;
import com.gtomato.android.ui.widget.CarouselView;
import com.yelp.fusion.client.models.Business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class VenueFragment extends NavigationFragmentActivity implements
        DataListLoadingListener<Business>,
        LocationListener,
        VenueFragmentViewModel.OnVenueLocationLinstener,
        VenueFragmentViewModel.OnMajorCateogrySetListener,
        CarouselView.OnItemSelectedListener,
        OnFragmentChangeListener {

    public final static String ACTION = "com.almet.arsenal.fragments.venue.ACTION";
    public final static String ACTION_LATLNG_REQUEST = "com.almet.arsenal.fragments.venue.ACTION_LATLNG";
    public final static String ACTION_STADIUM_REQUEST = "com.almet.arsenal.fragments.venue.ACTION_LOCATION";

    public final static String ACTION_SEARCH = "com.almet.arsenal.fragments.venue.ACTION_SEARCH";
    public final static String VENUE_ADDRESS = "VENUE_ADDRESS";
    public final static String CATEGORY = "CATEGORY";
    public final static String LAT_LNG = "LAT_LNG";

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public static final int CALL_PHONE_PERMISSION = 1;

    private static final int CATEGORY_TRANSIT_ORDER = 3;
    private static final int PUBS_CATEGORY_POSITION = 1;
    private static final int VENUES_LOADING_DELAY = 500;

    private static final int VIEW_PAGER_ID = 313131;

    private Context mContext;

    //binding,viewmodel
    private FragmentVenueBinding binding;
    private VenueFragmentViewModel viewModel;

    //adapters
    private VenuesPagerAdapter mVenuesPagerAdapter;
    private CarouselAdapter mCarouselAdapter;

    //view pager fragments
    private MapVenues mapVenues;
    private ListVenues listVenues;

    private ViewPager viewPager;

    //data
    private List<CarouselFilter> mCategories = new ArrayList<>();
    private List<Business> businesses = new ArrayList<>();
    private Set<Category> filters;

    //filters
    private Rating currentRating;
    private CarouselFilter filter;

    private int priceLevel = 0;
    private int mCurrentCategoryPosition = 0;

    //location
    private LocationManager mLocationManager;
    private BaseRequest mLocationRequest;
    private LatLng mLatLng;

    private ProgressDialog mPendingDialog;

    private interface SortMenuOptions {
        int DISTANCE = 1;
        int RATING = 2;
        int PRICE_FROM_HIGHT_TO_LOW = 3;
        int PRICE_FROM_LOW_TO_HIGHT = 4;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;

        binding = FragmentVenueBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        viewPager = new ViewPager(this);
        viewPager.setId(VIEW_PAGER_ID);

        binding.container.addView(viewPager);

        viewModel = new VenueFragmentViewModel(this, this, this, this);

        binding.setViewModel(viewModel);

        initLocationManager();
        initViewPagerAdapter();

        setupViewPagerAdapter();

        VenuesFilter.parseCategories(this);

        setClickListenerOnListTab();
        setClickListenerOnMapTab();

        setClickListerOnPriceFilter();
        setClickListerOnSortFilter();

        setClickListerOnSearchIcon();

        setPriceIcon(priceLevel);

        setupCategories();

        mPendingDialog = DialogUtils.showProgressDialog(this, R.string.please_wait);
        mPendingDialog.show();

        //issue with CarouselView item position - delay scrolling
        final Handler handler = new Handler();
        handler.postDelayed(() -> {

            if (isSearchAction()) {
                String category = getIntent().getStringExtra(CATEGORY);

                viewModel.requestMajorCategory(category);
            } else if (isStadaiumRequest()) {
                mCurrentCategoryPosition = PUBS_CATEGORY_POSITION;
                binding.filterCarousel.smoothScrollToPosition(PUBS_CATEGORY_POSITION);

                viewPager.setCurrentItem(VenuesPagerAdapter.MAP);

                binding.btnMap.toggle();

                String address = getIntent().getStringExtra(VENUE_ADDRESS);
                viewModel.requestGeoCode(address);
            } else {
                mCurrentCategoryPosition = PUBS_CATEGORY_POSITION;

                binding.filterCarousel.smoothScrollToPosition(PUBS_CATEGORY_POSITION);

                if (isAction()) {
                    startVenuesLoading();
                }
            }
        }, VENUES_LOADING_DELAY);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLocationManager.removeUpdates(this);
    }

    @Override
    public void onAddressReady(LatLng latLng) {
        Set<Category> allCategories = new HashSet<>();


        VenueFilter venueFilter = ((VenueFilter)mCarouselAdapter.getItem(mCurrentCategoryPosition));

        Category category = new Category();
        category.setTitle(venueFilter.getTitle());
        category.setAlias(venueFilter.getAlias());

        allCategories.add(category);

        mLatLng = latLng;

        mLocationRequest = new LatLngRequest(latLng);
        mLocationRequest.replaceCategories(allCategories);

        viewModel.performRequest(mLocationRequest);

        viewPager.setCurrentItem(VenuesPagerAdapter.MAP);
    }

    @Override
    public void scrollToMajorCategory(String majorCateogry) {
        LatLng latLng = getIntent().getParcelableExtra(LAT_LNG);

        String term = getIntent().getStringExtra(CATEGORY);

        Log.i(this.getClass().getName(), latLng.toString());

        filters = VenuesFilter.getSubCategories(majorCateogry);

        mLocationRequest = new LatLngRequest(latLng);
        mLocationRequest.replaceCategories(filters);
        mLocationRequest.setTerm(term);

        viewModel.performRequest(mLocationRequest);

        int scrollPos = mCarouselAdapter.getItemPosition(majorCateogry);

        mCurrentCategoryPosition = scrollPos;

        binding.filterCarousel.smoothScrollToPosition(scrollPos);
    }

    @Override
    public void onLocationChanged(android.location.Location location) {
        //todo:: handle location changed action
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        //todo:: handle on status location changed
    }

    @Override
    public void onProviderEnabled(String s) {
        //todo:: handle on provider is enabled
    }

    @Override
    public void onProviderDisabled(String s) {
        //todo:: handle on provider is disabled
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void setVenueLocation(LatLng latLng) {
        this.mLatLng = latLng;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            startVenuesLoading();
        }

        if (requestCode == CALL_PHONE_PERMISSION) {
            String phone = ListVenues.phone;

            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + phone));

            startActivity(callIntent);
        }
    }

    @Override
    public void onVenueDetailOpen() {
        hideFilters();
        binding.container.removeAllViews();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }

    @Override
    public void redirectToBack() {
        showFilters();
        addViewPager();
    }

    private void startVenuesLoading() {
        try {
            filters = VenuesFilter.getSubCategories(VenuesFilter.BARS);

            mLocationRequest = buildLocationRequest();
            mLocationRequest.replaceCategories(filters);

            viewModel.performRequest(mLocationRequest);
        } catch (LocationIsNull e) {
            Log.i(getClass().getName(), e.getMessage());

            Toast.makeText(this, R.string.venues_location_is_not_defined, Toast.LENGTH_LONG)
                    .show();

            mPendingDialog.dismiss();
        } catch (GPSProviderIsDisabled e) {
            mPendingDialog.dismiss();

            showLocationProviderIsDisabledPopup();
        } catch (NetworkLocationIsDisabled e) {
            mPendingDialog.dismiss();

            showLocationProviderIsDisabledPopup();
        } catch (LocationPermissionDenied e) {
            mPendingDialog.dismiss();

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
        }
    }

    private void hideFilters() {
        binding.ivSearch.setVisibility(View.GONE);
        binding.venueDirections.setVisibility(View.GONE);
        binding.ivPrice.setVisibility(View.GONE);
    }

    public void showFilters() {
        binding.ivSearch.setVisibility(View.VISIBLE);
        binding.venueDirections.setVisibility(View.VISIBLE);
        binding.ivPrice.setVisibility(View.VISIBLE);
    }

    private void addViewPager() {
        binding.container.removeAllViews();
        binding.container.addView(viewPager);
    }

    private boolean isAction() {
        return getIntent().getAction() != null;
    }

    private BaseRequest buildLocationRequest() throws
            LocationIsNull,
            GPSProviderIsDisabled,
            NetworkLocationIsDisabled,
            LocationPermissionDenied {

        if (isLatLngRequest()) {
            return getLatLngRequest();
        } else if (isSearchAction()) {
            LatLng latLng = getIntent().getParcelableExtra(LAT_LNG);
            return new LatLngRequest(latLng);
        } else if ( isStadaiumRequest()) {
            return new LatLngRequest(mLatLng);
        }
        Log.i(getClass().getName(), "REQUEST IS NOT BUILDED FULLY");
        return new BaseRequest();
    }


    private void initViewPagerAdapter() {
        mVenuesPagerAdapter = new VenuesPagerAdapter(getSupportFragmentManager());
    }

    private void openSearchActivity() {
        Intent startSearchActivity = new Intent(this, SearchActivity.class);
        startSearchActivity.putExtra(SearchActivity.LATLNG, mLatLng);

        startActivity(startSearchActivity);
    }

    private LatLngRequest getLatLngRequest() throws LocationIsNull, GPSProviderIsDisabled, NetworkLocationIsDisabled, LocationPermissionDenied {

        if (!isGPSProviderEnabled()) {
            throw new GPSProviderIsDisabled();
        }

        android.location.Location location = getAndroidDeviceLocation();

        if (location == null)
            throw new LocationIsNull();

        return LatLngRequest.buildInstance(location);
    }


    private android.location.Location getAndroidDeviceLocation() throws LocationPermissionDenied {
        int accessFineLocation = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int accessCoarseLocation = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);

        if (accessCoarseLocation == PackageManager.PERMISSION_GRANTED && accessFineLocation == PackageManager.PERMISSION_GRANTED) {

            if (isGPSProviderEnabled()) {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 10, this);
                return getLastKnownLocation();
            }
        }
        throw new LocationPermissionDenied();
    }


    private boolean isGPSProviderEnabled() {
        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private boolean isLatLngRequest() {
        return getIntent().getAction().equals(ACTION_LATLNG_REQUEST);
    }

    private boolean isStadaiumRequest() {
        return getIntent().getAction().equals(ACTION_STADIUM_REQUEST);
    }

    private void initLocationManager() {
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    }

    private boolean isSearchAction() {
        return isAction() && getIntent().getAction().equals(ACTION_SEARCH);
    }


    @SuppressLint("MissingPermission")
    private Location getLastKnownLocation() {
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = mLocationManager.getLastKnownLocation(provider);

            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    private void setupViewPagerAdapter() {
        viewPager.setAdapter(mVenuesPagerAdapter);
    }


    @Override
    public void onLoaded(List<Business> data) {
        this.businesses = data;

        mPendingDialog.dismiss();

        updateVenuesUIList();
        updateVenuesUIMap();
    }

    private void setViewPager() {
        binding.container.removeAllViews();
        addViewPager();
    }

    private boolean isViewPagerInsideContainer() {
        return binding.container.findViewById(VIEW_PAGER_ID) == null;
    }

    @Override
    public void onItemSelected(CarouselView carouselView, int position, int adapterPosition, RecyclerView.Adapter adapter) {

        if (isViewPagerInsideContainer())
            setViewPager();

        //todo:decompose, duplicate code with onItemClick listener
        if (adapterPosition != mCurrentCategoryPosition) {

            if (position == CATEGORY_TRANSIT_ORDER) {
                binding.container.removeAllViews();
                binding.menu.setVisibility(View.GONE);

                if (isLatLngRequest()) {
                    LatLng latLng = ((LatLngRequest) mLocationRequest).getLatLng();
                    String address = GeoHelper.decodeAddress(this, latLng);

                    FragmentUtils.changeFragment(this, R.id.container,
                            TravelFragment.newInstance(address), true);
                } else if (isSearchAction()) {
                    LatLng latLng = getIntent().getParcelableExtra(LAT_LNG);
                    String address = GeoHelper.decodeAddress(this, latLng);

                    FragmentUtils.changeFragment(this, R.id.container,
                            TravelFragment.newInstance(address), true);

                } else {
                    // TODO: 2/5/18 handle venue location request
                }
            } else {
                binding.menu.setVisibility(View.VISIBLE);
                filter = mCarouselAdapter.getItem(adapterPosition);

                viewPager.setCurrentItem(VenuesPagerAdapter.LIST);
                binding.btnList.toggle();

                loadVanuesByFilter(filter);
            }
        }
        mCurrentCategoryPosition = adapterPosition;
    }

    private void loadVanuesByFilter(CarouselFilter filter) {
        try {
            filters = VenuesFilter.getSubCategories(filter.getTitle());


            mLocationRequest = buildLocationRequest();
            mLocationRequest.replaceCategories(filters);

            viewModel.performRequest(mLocationRequest);
        } catch (LocationIsNull e) {
            Log.i(VenueFragment.class.getName(), e.getMessage());

            new AlertDialog.Builder(mContext)
                    .setTitle(R.string.gps_missing_title)
                    .setMessage(R.string.gps_missing_description)
                    .setNegativeButton("Cancel", (dialogInterface, i) -> {

                        mLocationManager.removeUpdates((VenueFragment) mContext);
                    })
                    .setPositiveButton("Open Settings", (dialogInterface, i) -> {
                        //Prompt the user once explanation has been shown
                        //todo:: open settings profile
                    })
                    .create()
                    .show();
        } catch (GPSProviderIsDisabled e) {
            showLocationProviderIsDisabledPopup();
        } catch (NetworkLocationIsDisabled e) {
            showLocationProviderIsDisabledPopup();
        } catch (LocationPermissionDenied e) {

        }
    }

    @Override
    public void onItemDeselected(CarouselView carouselView, int position, int adapterPosition, RecyclerView.Adapter adapter) {

    }

    @Override
    public void onLoaded(Business data) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mLocationManager.removeUpdates(this);
    }

    private void setupCategories() {
        CoverFlowViewTransformer transformer1 = new CoverFlowViewTransformer() {
            @Override
            public void transform(View view, float position) {
                int width = view.getMeasuredWidth(), height = view.getMeasuredHeight();
                view.setPivotX(width / 2.0f);
                view.setPivotY(height / 2.0f);
                view.setTranslationX(width * position * mOffsetXPercent);
                view.setRotationY(Math.signum(position) * (float) (Math.log(Math.abs(position) + 1) / Math.log(3)));
                view.setScaleY(1 + mScaleYFactor * Math.abs(position));
                view.setScaleX(1 + mScaleYFactor * Math.abs(position));
            }
        };

        transformer1.setOffsetXPercent(0.4f);
        transformer1.setYProjection(0);
        transformer1.setScaleYFactor(-0.10f);

        binding.filterCarousel.setTransformer(transformer1);

        CarouselFilter restaurants = new VenueFilter(VenuesFilter.RESTAURANTS, "restaurants", R.drawable.ic_restaurant);
        CarouselFilter pubs = new VenueFilter(VenuesFilter.BARS, "pubs", R.drawable.ic_pub);
        CarouselFilter hotels = new VenueFilter(VenuesFilter.TRAVEL_HOTELS, "hotelstravel", R.drawable.ic_hotels);
        CarouselFilter transit = new VenueFilter(VenuesFilter.TRANSIT, "transit", R.drawable.ic_transit);

        mCategories.add(hotels);
        mCategories.add(pubs);
        mCategories.add(restaurants);
        mCategories.add(transit);


        mCarouselAdapter = new CarouselAdapter(mCategories);

        binding.filterCarousel.setAdapter(mCarouselAdapter);
        binding.filterCarousel.setOnItemClickListener((adapter1, view, position, adapterPosition) -> {
            if (position != mCurrentCategoryPosition) {

                if (position == CATEGORY_TRANSIT_ORDER) {
                    binding.container.removeAllViews();
                    binding.menu.setVisibility(View.GONE);

                    if (isLatLngRequest()) {
                        LatLng latLng = ((LatLngRequest) mLocationRequest).getLatLng();
                        String address = GeoHelper.decodeAddress(this, latLng);

                        FragmentUtils.changeFragment(this, R.id.container,
                                TravelFragment.newInstance(address), true);
                    } else {
                        // TODO: 2/5/18 handle venue location request
                    }
                } else {
                    filter = ((CarouselAdapter) adapter1).getItem(position);
                    binding.btnList.toggle();
                    loadVanuesByFilter(filter);
                }
            }
        });

        binding.filterCarousel.setOnItemSelectedListener(this);
    }

    private void setClickListenerOnListTab() {
        binding.btnList.setOnClickListener(view -> {
            viewPager.setCurrentItem(VenuesPagerAdapter.LIST);
        });
    }

    private void setClickListenerOnMapTab() {
        binding.btnMap.setOnClickListener(view -> {

            if (isViewPagerInsideContainer())
                setViewPager();

            viewPager.setCurrentItem(VenuesPagerAdapter.MAP);
        });
    }

    private void setClickListerOnPriceFilter() {
        binding.ivPrice.setOnClickListener(view -> {
            showPriceFilterPopup();
        });
    }

    private void setClickListerOnSortFilter() {
        binding.venueDirections.setOnClickListener(view -> {
            showSortFilterPopup();
        });
    }

    private void setClickListerOnSearchIcon() {
        binding.ivSearch.setOnClickListener(view -> {
            openSearchActivity();
        });
    }

    private void showPriceFilterPopup() {
        PopupMenu dropDownMenu = new PopupMenu(this, binding.ivPrice);

        dropDownMenu.getMenuInflater().inflate(R.menu.venue_prices, dropDownMenu.getMenu());
        dropDownMenu.setOnMenuItemClickListener(menuItem -> {

            priceLevel = menuItem.getOrder();

            mLocationRequest.replaceSelectedMoney(getPriceLevels(priceLevel));

            //todo:: handle price filter
            viewModel.performRequest(mLocationRequest);

            setPriceIcon(priceLevel);

            return true;
        });
        dropDownMenu.show();
    }

    private void showSortFilterPopup() {

        PopupMenu dropDownMenu = new PopupMenu(this, binding.venueDirections);
        dropDownMenu.getMenuInflater().inflate(R.menu.venue_sort, dropDownMenu.getMenu());

        dropDownMenu.setOnMenuItemClickListener(menuItem -> {

            switch (menuItem.getOrder()) {
                case SortMenuOptions.DISTANCE:
                    sortBussinessByDistance();
                    break;
                case SortMenuOptions.RATING:
                    sortBussinessByRating();
                    break;
                case SortMenuOptions.PRICE_FROM_HIGHT_TO_LOW:
                    sortBussinessByPriceHightToLow();
                    break;
                case SortMenuOptions.PRICE_FROM_LOW_TO_HIGHT:
                    sortBussinessByPriceLowToHight();
                    break;
            }
            listVenues.init(businesses, mLatLng, this);

            return true;
        });

        dropDownMenu.show();
    }

    private void showLocationProviderIsDisabledPopup() {
        new AlertDialog.Builder(mContext)
                .setTitle("Background Location Access Disabled")
                .setMessage("To search for venues, please provide Location access")
                .setNegativeButton("Cancel", (dialogInterface, i) -> {

                    mLocationManager.removeUpdates((VenueFragment) mContext);
                    //todo:: show error message on fragments
                })
                .setPositiveButton("Open Settings", (dialogInterface, i) -> {
                    //Prompt the user once explanation has been shown
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                })
                .create()
                .show();
    }

    //
    private class VenuesPagerAdapter extends FragmentPagerAdapter {

        private static final int LIST = 0;
        private static final int MAP = 1;
        private static final int TABS_COUNT = 2;

        public VenuesPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return TABS_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == LIST) {
                listVenues = getList();
                return listVenues;
            }

            if (position == MAP) {
                mapVenues = getMap();
                return mapVenues;
            }
            return getList();
        }

        @Override
        public CharSequence getPageTitle(int position) {

            if (position == LIST)
                return getString(R.string.list);

            if (position == MAP)
                return getString(R.string.map);

            return "";
        }

        private ListVenues getList() {
            return ListVenues.newInstance();
        }

        private MapVenues getMap() {
            return MapVenues.newInstance();
        }
    }

    private void updateVenuesUIList() {
        listVenues.init(businesses, mLatLng, this);
    }

    private void updateVenuesUIMap() {
        if (mLatLng == null) {
            mapVenues.init();
        } else {
            mapVenues.init(businesses, mLatLng);
        }
    }

    private Set<Integer> getPriceLevels(int priceLevel) {
        Set<Integer> selectedMoney = new TreeSet<>();
        selectedMoney.add(priceLevel);

        return selectedMoney;
    }

    private void setPriceIcon(int priceLevel) {
        if (priceLevel == PriceFilter.PRICE_$) {
            binding.ivPrice.setImageResource(R.drawable.price_1i);
        } else if (priceLevel == PriceFilter.PRICE_$$) {
            binding.ivPrice.setImageResource(R.drawable.price_2i);
        } else if (priceLevel == PriceFilter.PRICE_$$$) {
            binding.ivPrice.setImageResource(R.drawable.price_3i);
        } else if (priceLevel == PriceFilter.PRICE_$$$$) {
            binding.ivPrice.setImageResource(R.drawable.price_4i);
        }
    }

    private void sortBussinessByRating() {
        Collections.sort(businesses, (business, t1) -> {
            int ratingCompare = Double.compare(business.getRating(),
                    t1.getRating());

            ratingCompare *= -1;
            return ratingCompare;
        });
    }

    private void sortBussinessByDistance() {
        Collections.sort(businesses, (Business business, Business t1) -> Double.compare(business.getDistance(),
                t1.getDistance()));
    }

    private void sortBussinessByPriceHightToLow() {
        Collections.sort(businesses, (business, t1) -> {
            int priceCompare = Integer.compare(business.getPrice().length(),
                    t1.getPrice().length());

            priceCompare *= (-1);
            return priceCompare;
        });
    }

    private void sortBussinessByPriceLowToHight() {
        Collections.sort(businesses, (business, t1) -> {
            int priceCompare = Integer.compare(business.getPrice().length(),
                    t1.getPrice().length());

            return priceCompare;
        });
    }
}