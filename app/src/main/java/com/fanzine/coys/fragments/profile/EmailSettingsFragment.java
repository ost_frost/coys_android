package com.fanzine.coys.fragments.profile;

import android.databinding.ViewDataBinding;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.FragmentEmailSettingsBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.profile.EmailNotifications;
import com.fanzine.coys.viewmodels.fragments.mails.EmailSettingsViewModel;

import com.jakewharton.rxbinding.view.RxView;

/*
 * Created by maximdrobonoh on 13.09.17.
 */

public class EmailSettingsFragment extends BaseFragment {

    private static final String USER = "USER";

    private EmailSettingsViewModel viewModel;
    private FragmentEmailSettingsBinding binding;

    public static EmailSettingsFragment newInstance(EmailNotifications notifications) {
        EmailSettingsFragment fragment = new EmailSettingsFragment();

        Bundle args = new Bundle();
        args.putParcelable(USER, notifications);


        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        setHasOptionsMenu(true);
        binding = FragmentEmailSettingsBinding.inflate(inflater, root, attachToRoot);
        viewModel = new EmailSettingsViewModel(getContext());
        binding.setViewModel(viewModel);
        setBaseViewModel(viewModel);

        setToolbar(binding.toolbar);



        viewModel.notif.set(getArguments().getParcelable(USER));

        RxView.clicks(binding.btnSave).subscribe(aVoid -> {
            viewModel.saveSettings();
        });

        return binding;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.email_settings, menu);
    }


    private void setToolbar(Toolbar toolbar) {
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(ContextCompat.getColor(getContext(), R.color.inbox_bg_home_button), PorterDuff.Mode.SRC_ATOP);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }
}