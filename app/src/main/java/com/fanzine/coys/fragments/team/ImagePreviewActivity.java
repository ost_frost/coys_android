package com.fanzine.coys.fragments.team;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.base.BaseActivity;
import com.fanzine.coys.databinding.ActivityImagePreviewBinding;
import com.fanzine.coys.models.team.PlayerPhoto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vitaliygerasymchuk on 2/23/18
 */

public class ImagePreviewActivity extends BaseActivity {

    private static final String PHOTOS = "PHOTOS";
    private static final String CURRENT_PHOTO = "CURRENT_PHOTO";

    public static void launch(@NonNull Context context, @NonNull PlayerPhoto currentPhoto, @NonNull ArrayList<PlayerPhoto> photos) {
        Intent intent = new Intent(context, ImagePreviewActivity.class);
        if (photos.isEmpty()){
            photos.add(currentPhoto);
        }
        intent.putParcelableArrayListExtra(PHOTOS, photos);
        intent.putExtra(CURRENT_PHOTO, currentPhoto);
        context.startActivity(intent);
    }

    @SuppressWarnings({"FieldCanBeLocal", "NullableProblems"})
    @NonNull
    private ActivityImagePreviewBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final List<PlayerPhoto> playerPhotos = getIntent().getParcelableArrayListExtra(PHOTOS);
        final PlayerPhoto photo = getIntent().getParcelableExtra(CURRENT_PHOTO);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_image_preview);
        binding.viewPager.setAdapter(new ImagePagerAdapter(getSupportFragmentManager(), playerPhotos));
        setupToolbar();

        final int currentIndex = playerPhotos.indexOf(photo);

        if (currentIndex > 0 && currentIndex < playerPhotos.size()) {
            binding.viewPager.setCurrentItem(currentIndex);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setupToolbar() {
        final Toolbar toolbar = binding.toolbar;
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }
    }

    private class ImagePagerAdapter extends FragmentPagerAdapter {

        @NonNull
        private List<PlayerPhoto> photos = new ArrayList<>();

        private void refresh(List<PlayerPhoto> photos) {
            this.photos.clear();
            this.photos.addAll(photos);
            this.notifyDataSetChanged();
        }

        ImagePagerAdapter(FragmentManager fm, List<PlayerPhoto> photos) {
            super(fm);
            refresh(photos);
        }

        @Override
        public Fragment getItem(int position) {
            if (position < photos.size()) {
                final PlayerPhoto photo = photos.get(position);
                return ImagePreviewFragment.newInstance(photo);
            }
            return new Fragment();
        }

        @Override
        public int getCount() {
            return photos.size();
        }
    }
}
