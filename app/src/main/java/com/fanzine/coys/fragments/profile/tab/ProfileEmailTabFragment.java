package com.fanzine.coys.fragments.profile.tab;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.fanzine.coys.activities.MainActivity;
import com.fanzine.coys.databinding.FragmentProfileEmailBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.coys.viewmodels.fragments.profile.EmailProfileViewModel;
import com.jakewharton.rxbinding.view.RxView;

/**
 * Created by vitaliygerasymchuk on 2/12/18
 */

public class ProfileEmailTabFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener {

    public static ProfileEmailTabFragment newInstance() {
        Bundle args = new Bundle();
        ProfileEmailTabFragment fragment = new ProfileEmailTabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    private FragmentProfileEmailBinding binding;
    @Nullable
    private EmailProfileViewModel viewModel;

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        if (viewModel == null) {
            viewModel = new EmailProfileViewModel(getContext());
        }
        binding = FragmentProfileEmailBinding.inflate(inflater, root, attachToRoot);
        binding.setViewModel(viewModel);
        RxView.clicks(binding.editSignClick).subscribe((aVoid) -> {
            viewModel.isPro().subscribe(isPro -> {
                if (isPro)
                    switchContainers();
                else
                    ((MainActivity) getActivity()).openEmails();
            });
        });
        RxView.clicks(binding.cancel).subscribe((aVoid -> switchContainers()));
        RxView.clicks(binding.save).subscribe((aVoid -> {
            switchContainers();
            if (viewModel != null) {
                final String newSign = binding.edtMsg.getText().toString();

                viewModel.isPro().subscribe(isPro -> {
                    viewModel.setEmailSignature(newSign);
                }, Throwable::getStackTrace);

            }
        }));
        viewModel.getEmail();
        binding.switchEmail.setOnCheckedChangeListener(this);
        return binding;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (viewModel != null) {
            viewModel.isVisible = isVisibleToUser;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (viewModel != null && viewModel.isVisible) {

            viewModel.isPro().subscribe(isPro -> {
                if (isPro) {
                    if (viewModel.isNotificationStateDiffers(isChecked)) {
                        viewModel.toggleEmailNotification();

                        if (isChecked)
                            viewModel.subsribeOnNotifications();
                        else
                            viewModel.unsubscribeOnNotifications();
                    }
                } else
                    ((MainActivity) getActivity()).openEmails();
            });
        }
    }

    private void switchContainers() {
        if (binding != null) {
            final boolean editSign = binding.editSignContainer.getVisibility() != View.VISIBLE;
            binding.editSignContainer.setVisibility(editSign ? View.VISIBLE : View.GONE);
            binding.emailAndSignContainer.setVisibility(editSign ? View.GONE : View.VISIBLE);
        }
    }
}
