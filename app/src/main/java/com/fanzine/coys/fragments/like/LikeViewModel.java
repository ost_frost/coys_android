package com.fanzine.coys.fragments.like;

import android.app.ProgressDialog;
import android.content.Context;

import com.fanzine.coys.R;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.models.News;
import com.fanzine.coys.models.Video;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by vitaliygerasymchuk on 3/9/18
 */

public class LikeViewModel extends BaseViewModel {

    public LikeViewModel(Context context) {
        super(context);
    }

    public void likeNews(News news, LikeCallback<News> callback) {
        doWithNetwork(() -> likeNewsInternal(news, callback));
    }

    public void unlikeNews(News news, LikeCallback<News> callback) {
        doWithNetwork(() -> unlikeNewsInternal(news, callback));
    }

    public void likeVideo(Video video, LikeCallback<Video> callback) {
        doWithNetwork(() -> likeVideoInternal(video, callback));
    }

    public void unlikeVideo(Video video, LikeCallback<Video> callback) {
        doWithNetwork(() -> unlikeVideoInternal(video, callback));
    }

    private void likeNewsInternal(News news, LikeCallback<News> callback) {
        ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);
        Subscriber<News> subscriber = new Subscriber<News>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                pd.dismiss();
                FieldError errors = ErrorResponseParser.getErrors(e);
                if (errors.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                } else {
                    DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                }
            }

            @Override
            public void onNext(News news1) {
                pd.dismiss();
                callback.onLiked(news1);
            }
        };
        subscription.add(subscriber);
        ApiRequest.getInstance().getApi().likeNews(news.getNewsId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(response -> {
                    news.isLiked = true;
                    news.setLikesCount((int) response.likesCount);
                    return news;
                })
                .subscribe(subscriber);
    }

    private void unlikeNewsInternal(News news, LikeCallback<News> callback) {
        ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);
        Subscriber<News> subscriber = new Subscriber<News>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                pd.dismiss();
                FieldError errors = ErrorResponseParser.getErrors(e);
                if (errors.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                } else {
                    DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                }
            }

            @Override
            public void onNext(News news1) {
                pd.dismiss();
                callback.onUnLiked(news1);
            }
        };

        subscription.add(subscriber);

        ApiRequest.getInstance().getApi().unlikeNews(news.getNewsId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(response -> {
                    news.isLiked = false;
                    news.setLikesCount(response.likesCount);
                    return news;
                })
                .subscribe(subscriber);
    }

    private void likeVideoInternal(Video video, LikeCallback<Video> callback) {
        ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);

        Subscriber<Video> subscriber = new Subscriber<Video>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                pd.dismiss();
                FieldError errors = ErrorResponseParser.getErrors(e);
                if (errors.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                } else {
                    DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                }
            }

            @Override
            public void onNext(Video video1) {
                pd.dismiss();
                callback.onLiked(video1);
            }
        };
        subscription.add(subscriber);

        ApiRequest.getInstance().getApi().likeVideo((int) video.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(response -> {
                    video.isLiked = true;
                    video.setLikesCount(response.likesCount);
                    return video;
                })
                .subscribe(subscriber);
    }

    private void unlikeVideoInternal(Video video, LikeCallback<Video> callback) {
        ProgressDialog pd = DialogUtils.showProgressDialog(getContext(), R.string.please_wait);
        Subscriber<Video> subscriber = new Subscriber<Video>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                pd.dismiss();
                FieldError errors = ErrorResponseParser.getErrors(e);
                if (errors.isEmpty()) {
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                } else {
                    DialogUtils.showAlertDialog(getContext(), errors.getMessage());
                }
            }

            @Override
            public void onNext(Video video1) {
                pd.dismiss();
                callback.onUnLiked(video1);
            }
        };
        subscription.add(subscriber);

        ApiRequest.getInstance().getApi().unlikeVideo((int) video.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(response -> {
                    video.isLiked = false;
                    video.setLikesCount(response.likesCount);
                    return video;
                })
                .subscribe(subscriber);
    }
}
