package com.fanzine.coys.fragments.team;


import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.VideoAdapter;
import com.fanzine.coys.databinding.FragmentVideoPlayerBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.fragments.like.LikeCallback;
import com.fanzine.coys.fragments.like.LikeViewModel;
import com.fanzine.coys.models.Video;
import com.fanzine.coys.models.VideoItem;
import com.fanzine.coys.utils.ShareUtil;

import java.util.ArrayList;
import java.util.List;

public class PlayerListVideoFragment extends BaseFragment implements LikeCallback<Video> {

    private static final String DATA = "data";
    private LikeViewModel likeViewModel;
    private VideoAdapter adapter;

    public static PlayerListVideoFragment newInstance(List<VideoItem> videos) {
        PlayerListVideoFragment fragment = new PlayerListVideoFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(DATA, new ArrayList<>(videos));
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        FragmentVideoPlayerBinding binding = FragmentVideoPlayerBinding.inflate(inflater, root, attachToRoot);
        likeViewModel = new LikeViewModel(getContext());
        List<Video> data = getArguments().getParcelableArrayList(DATA);

        binding.rvVideos.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new VideoAdapter(getContext(), data, true);
        binding.rvVideos.setAdapter(adapter);

        adapter.setCallback(new VideoAdapter.Callback() {
            @Override
            public void onLike(Video video) {
                if (likeViewModel != null) {
                    if (video.isLiked) {
                        likeViewModel.unlikeVideo(video, PlayerListVideoFragment.this);
                    } else {
                        likeViewModel.likeVideo(video, PlayerListVideoFragment.this);
                    }
                }
            }

            @Override
            public void onFbPostShare(Video video) {
                ShareUtil.shareVideoToFacebook(getActivity(), video);
            }

            @Override
            public void onTwitterShare(Video video) {
                ShareUtil.shareVideoToTwitter(getContext(), video);
            }

            @Override
            public void onChatShare(Video video) {
                ShareUtil.shareVideoToChat(getContext(), video);
            }

            @Override
            public void onMailShare(Video video) {
                ShareUtil.shareVideoToMail(getContext(), video);
            }

            @Override
            public void onClipboardCopy(Video video) {
                ShareUtil.copyToClipboardVideo(getContext(), video);
            }
        });

        return binding;
    }

    @Override
    public void onLiked(Video video) {
        if (adapter != null) {
            adapter.refreshItem(video);
        }
    }

    @Override
    public void onUnLiked(Video video) {
        if (adapter != null) {
            adapter.refreshItem(video);
        }
    }
}
