package com.fanzine.coys.fragments.video;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.FragmentVideoSearchFilterBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.viewmodels.fragments.video.SearchFilterViewModel;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by mbp on 1/8/18.
 */

// TODO: 1/14/18 NEED Refactoring
public class SearchFilter extends BaseFragment {

    private FragmentVideoSearchFilterBinding binding;
    private SearchFilterViewModel viewModel;

    private String sortBy;
    private String uploadDate;

    private static final String ARG_SORT_BY = "ARG_SORT_BY";
    private static final String ARG_UPLOAD_DATE = "ARG_UPLOAD_DATE";

    public static SearchFilter newInstance(@NonNull String sortBy, @NonNull String uploadDate) {
        SearchFilter fragment = new SearchFilter();
        Bundle args = new Bundle();
        args.putString(ARG_SORT_BY, sortBy);
        args.putString(ARG_UPLOAD_DATE, uploadDate);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {

        if (binding == null) {

            sortBy = getArguments().getString(ARG_SORT_BY);
            uploadDate = getArguments().getString(ARG_UPLOAD_DATE);

            viewModel = new SearchFilterViewModel(getContext());

            binding = FragmentVideoSearchFilterBinding.inflate(inflater, root, attachToRoot);
            binding.setViewModel(viewModel);

            binding.tvSearchFilter.setOnClickListener(view -> FragmentUtils.changeFragment(
                    getFragmentManager(),
                    R.id.content,
                    VideosFragment.newInstance(sortBy, uploadDate),
                    false));

            binding.viewCount.setOnClickListener(view -> {
                toggleSortValue("viewCount");
                offFilters(R.id.viewCount, sorftByFilters);
            });
            binding.upload.setOnClickListener(view -> {
                toggleSortValue("published_at");
                offFilters(R.id.upload, sorftByFilters);
            });
            binding.anyTime.setOnClickListener(view -> {

                toggleUploadValue("any_time");
                offFilters(R.id.anyTime, uploadDateFilters);
            });
            binding.today.setOnClickListener(view -> {
                toggleUploadValue("today");
                offFilters(R.id.today, uploadDateFilters);
            });
            binding.thisWeek.setOnClickListener(view -> {
                toggleUploadValue("week");
                offFilters(R.id.thisWeek, uploadDateFilters);
            });
            binding.thisMonth.setOnClickListener(view -> {
                toggleUploadValue("month");
                offFilters(R.id.thisMonth, uploadDateFilters);
            });

            setCheckedSortBy();
            setCheckUploadDate();
        }
        return binding;
    }

    private void toggleSortValue(String desiredValue) {
        if (TextUtils.equals(sortBy, desiredValue)) {
            sortBy = "";
        } else {
            sortBy = desiredValue;
        }
    }

    private void toggleUploadValue(String desiredValue){
        if (TextUtils.equals(uploadDate, desiredValue)) {
            uploadDate = "";
        } else {
            uploadDate = desiredValue;
        }
    }

    private void setCheckedSortBy() {
        if (TextUtils.equals(sortBy, "viewCount")) {
            binding.viewCount.setChecked(true);
        } else if (TextUtils.equals(sortBy, "published_at")) {
            binding.upload.setChecked(true);
        }
    }

    private void setCheckUploadDate() {
        if (TextUtils.equals(uploadDate, "any_time")) {
            binding.anyTime.setChecked(true);
        } else if (TextUtils.equals(uploadDate, "today")) {
            binding.today.setChecked(true);
        } else if (TextUtils.equals(uploadDate, "week")) {
            binding.thisWeek.setChecked(true);
        } else if (TextUtils.equals(uploadDate, "month")) {
            binding.thisMonth.setChecked(true);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (checkedIdsSet != null) {
            for (Integer checkboxId : checkedIdsSet) {
                ((CheckBox) getView().findViewById(checkboxId)).setChecked(true);
            }
        }
    }


    //todo: need refactor
    private int[] uploadDateFilters = {R.id.today, R.id.thisWeek, R.id.thisMonth, R.id.anyTime};
    private int[] sorftByFilters = {R.id.viewCount, R.id.upload};

    public Set<Integer> checkedIdsSet = new HashSet<>();

    private void offFilters(int current, int[] filters) {
        checkedIdsSet.add(current);

        for (Integer uploadDateFilter : filters)
            if (uploadDateFilter != current) {
                ((CheckBox) getView().findViewById(uploadDateFilter)).setChecked(false);

                if (checkedIdsSet.contains(uploadDateFilter)) {
                    checkedIdsSet.remove(uploadDateFilter);
                }
            }
    }
}
