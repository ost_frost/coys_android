package com.fanzine.coys.fragments.profile;

import android.databinding.ViewDataBinding;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.FragmentProfileBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.fragments.profile.tab.AccountTabFragment;
import com.fanzine.coys.fragments.profile.tab.ChatTabFragment;
import com.fanzine.coys.fragments.profile.tab.ProfileEmailTabFragment;
import com.fanzine.coys.fragments.profile.tab.ProfileNotificationsTabFragment;
import com.fanzine.coys.interfaces.OnProfileLoader;
import com.fanzine.coys.models.profile.Profile;
import com.fanzine.coys.viewmodels.fragments.profile.ProfileViewModel;
import com.jakewharton.rxbinding.support.design.widget.RxTabLayout;
import com.jakewharton.rxbinding.support.v4.view.RxViewPager;
import com.jakewharton.rxbinding.view.RxView;
import com.miguelbcr.ui.rx_paparazzo.RxPaparazzo;
import com.miguelbcr.ui.rx_paparazzo.entities.size.CustomMaxSize;
import com.squareup.picasso.Picasso;

import java.io.File;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Woland on 13.01.2017.
 */

public class ProfileFragment extends BaseFragment implements OnProfileLoader {

    private FragmentProfileBinding binding;
    private ProfileViewModel viewModel;
    private Profile profile;

    public static ProfileFragment newInstance() {
        Bundle args = new Bundle();
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        if (binding == null || viewModel == null) {
            binding = FragmentProfileBinding.inflate(inflater, root, attachToRoot);
            viewModel = new ProfileViewModel(getContext());
            binding.setViewModel(viewModel);
            setBaseViewModel(viewModel);

            RxView.clicks(binding.profileImage).subscribe(aVoid -> RxPaparazzo.takeImage(this)
                    .size(new CustomMaxSize(720))
                    .usingGallery()
                    .subscribe(response -> {
                        if (response.resultCode() == RESULT_OK) {
                            response.targetUI().showImage(response.data());
                        }
                    }));

            RxView.clicks(binding.logout).subscribe(aVoid -> {
                if (viewModel != null) {
                    viewModel.logout();
                }
            });

            setupFonts();
        }
        return binding;
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getProfile(this);
    }

    @Override
    public void onProfileLoaded(Profile profile) {
        this.profile = profile;
        viewModel.name.set(profile.getFirstName() + " " + profile.getLastName());
        viewModel.avatar.set(profile.getProfileImage());
        setupViewPager(profile);
    }


    private void setupViewPager(Profile profile) {
        if (binding != null) {
            ProfilePagerAdapter pagerAdapter = new ProfilePagerAdapter(getChildFragmentManager());
            binding.viewPager.setAdapter(pagerAdapter);
            binding.viewPager.setOffscreenPageLimit(4);
            pagerAdapter.addFragment(
                    AccountTabFragment.newInstance(profile),
                    ChatTabFragment.newInstance(),
                    ProfileEmailTabFragment.newInstance(),
                    ProfileNotificationsTabFragment.newInstance()
            );
            setupTabLayoutListener();
            setupViewPagerListener();
        }
    }

    private void setupViewPagerListener() {
        RxViewPager.pageSelections(binding.viewPager).subscribe(tab -> {
            if (binding != null) {
                TabLayout.Tab currentTab = binding.tabLayout.getTabAt(tab);
                if (currentTab != null) {
                    currentTab.select();
                }
            }
        });
    }

    private void setupTabLayoutListener() {

        Typeface bold = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto/Roboto-Bold.ttf");
        Typeface regular = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto/Roboto-Regular.ttf");

        int tabsCount =  binding.tabLayout.getTabCount();

        for (int i = 0; i < tabsCount; i++) {
            //noinspection ConstantConditions
            TextView tv=(TextView)LayoutInflater.from(getContext()).inflate(R.layout.profile_custom_tab,null);
            tv.setTypeface(regular);
            binding.tabLayout.getTabAt(i).setCustomView(tv);
        }


        ((TextView)binding.tabLayout.getTabAt(0).getCustomView()).setTypeface(bold);

        RxTabLayout.selections(binding.tabLayout).subscribe(tab -> {
            if (binding != null) {
                binding.viewPager.setCurrentItem(tab.getPosition());

                binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        TextView tv = (TextView)binding.tabLayout.getTabAt(position).getCustomView();
                        tv.setTypeface(bold);

                        for ( int i =  0; i < 4; i++) {
                            if ( i != position) {
                                TextView otherTabs = (TextView)binding.tabLayout.getTabAt(i).getCustomView();
                                otherTabs.setTypeface(regular);
                            }
                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
            }
        });
    }



    private void showImage(String url) {
        if (!TextUtils.isEmpty(url)) {
            File file = new File(url);
            Picasso.with(getContext()).load(file).into(binding.avatar);
            viewModel.setImageFile(file);
        }
    }

    private void setupFonts() {
        Typeface nameFont = Typeface.createFromAsset(getContext().getAssets(), "fonts/din_condensed_bold.ttf");
        binding.tvUserName.setTypeface(nameFont);
    }
}
