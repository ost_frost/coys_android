package com.fanzine.coys.fragments.mails;


import android.databinding.ViewDataBinding;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.FragmentEmailBinding;
import com.fanzine.coys.db.factory.HelperFactory;
import com.fanzine.coys.dialogs.ChangeFolderDialog;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.models.mails.MailContent;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.viewmodels.fragments.mails.EmailFragmentViewModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmailFragment extends BaseFragment implements View.OnClickListener, EmailFragmentViewModel.OnMailLoading {

    public static final String MAILS = "mails";
    public static final String MAIL = "mail";
    public static final String FOLDER = "folder";

    private FragmentEmailBinding binding;
    public static final String POSITION = "position";
    private EmailFragmentViewModel viewModel;

    public static EmailFragment newInstance(List<Mail> data, int position, MailContent mailContent, String folder) {
        EmailFragment fragment = new EmailFragment();
        Bundle args = new Bundle();
        args.putInt(POSITION, position);
        args.putString(FOLDER, folder);
        args.putParcelable(MAIL, mailContent);
        args.putParcelableArrayList(MAILS, new ArrayList<>(data));
        fragment.setArguments(args);
        return fragment;
    }

    public static EmailFragment newInstance(MailContent mailContent) {
        EmailFragment fragment = new EmailFragment();
        Bundle args = new Bundle();
        args.putParcelable(MAIL, mailContent);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentEmailBinding.inflate(inflater, root, attachToRoot);
        setToolbar();

        List<Mail> mails = getArguments().getParcelableArrayList(MAILS);
        int position = getArguments().getInt(POSITION, 0);

        MailContent mailContent = getArguments().getParcelable(MAIL);
        Mail mail = mails != null ? mails.get(position) : null;

        String folder = getArguments().getString(FOLDER);

        viewModel = new EmailFragmentViewModel(getContext(), mails, position, this);
        viewModel.setToolbarTitle(folder);

        binding.setViewModel(viewModel);
        setBaseViewModel(viewModel);

        binding.emailTools.setClickListener(this);
        binding.message.getSettings();
        binding.message.setBackgroundColor(Color.TRANSPARENT);

        onLoaded(mailContent, mail);

        return binding;
    }

    private void setToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(binding.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(ContextCompat.getColor(getContext(), R.color.inbox_bg_home_button), PorterDuff.Mode.SRC_ATOP);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_email, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_previous:
                viewModel.previousMail();
                break;
            case R.id.action_next:
                viewModel.nextMail();
                break;
	        case android.R.id.home:
	        	getActivity().onBackPressed();
	        	break;
        }

        getActivity().invalidateOptionsMenu();

        return true;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (viewModel.getPosition() == 0) {
            menu.findItem(R.id.action_previous).setEnabled(false);
            menu.findItem(R.id.action_previous).getIcon().setAlpha(100);

        } else if (viewModel.getPosition() == viewModel.getMailsCount() - 1) {
            menu.findItem(R.id.action_next).setEnabled(false);
            menu.findItem(R.id.action_next).getIcon().setAlpha(100);
        } else {
            menu.findItem(R.id.action_previous).setEnabled(true);
            menu.findItem(R.id.action_next).setEnabled(true);
            menu.findItem(R.id.action_previous).getIcon().setAlpha(255);
            menu.findItem(R.id.action_next).getIcon().setAlpha(255);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnFlag:
                viewModel.setFlag();
                break;
            case R.id.btnChangeFolder:
                ChangeFolderDialog.newInstance(viewModel.mail.get()).show(getFragmentManager(), "");
                break;
            case R.id.btnDelete:
                new AlertDialog.Builder(getContext())
                        .setTitle(R.string.delEmail)
                        .setPositiveButton(R.string.yes, (dialog, which) -> viewModel.removeMail())
                        .setNegativeButton(R.string.no, null)
                        .show();
                break;
            case R.id.btnReply:
                FragmentUtils.changeFragment(getActivity(), R.id.content_frame, NewMailFragment.newInstance(viewModel.getCurrentMail()), true);
                break;
            case R.id.btnAddLabel:
                //// TODO: 18.09.17  resolve duplicated code
                List<Mail> mails = getArguments().getParcelableArrayList(MAILS);
                int position = getArguments().getInt(POSITION, 0);
                Mail mail = mails.get(position);

                if ( mail.hasLabel() ) {
                    new AlertDialog.Builder(getContext())
                            .setTitle(R.string.mail_delete_label)
                            .setPositiveButton(R.string.yes, (dialog, which) -> viewModel.deleteLabel(mail))
                            .setNegativeButton(R.string.no, null)
                            .show();
                } else {
                    FragmentUtils.changeFragment(getActivity(), R.id.content_frame, ListLabelFragment.newInstance(mails.get(position)), true);
                }
                break;
        }
    }

    @Override
    public void onLoaded(MailContent mailContent, Mail mail) {
        viewModel.setMailContent(mailContent);

        binding.message.loadDataWithBaseURL("", mailContent.getMessageContent(), "text/html", "UTF-8", "");
        binding.message.reload();

        if (!mailContent.isReaded())
            viewModel.setReaded();

        if (mail != null && !mail.isRead()) {
            try {
                HelperFactory.getHelper().getMailDao().readMail(mail.getFolder(), mail.getUid());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        onFlagged(mailContent.isFlagged());
    }

    @Override
    public void onFlagged(boolean isFlagged) {
        binding.emailTools.setFlag(isFlagged);
    }
}
