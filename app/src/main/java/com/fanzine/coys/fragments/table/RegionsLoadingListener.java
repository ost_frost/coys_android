package com.fanzine.coys.fragments.table;

import com.fanzine.coys.models.table.RegionLeague;

import java.util.List;

/**
 * Created by mbp on 11/22/17.
 */

public interface RegionsLoadingListener {
    void onLeagueRegionsLoaded(List<RegionLeague> data);
}
