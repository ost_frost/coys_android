package com.fanzine.coys.fragments.start;

import android.content.Context;
import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;

import com.fanzine.coys.activities.MainActivity;
import com.fanzine.coys.adapters.teaser.EmailAdapter;
import com.fanzine.coys.databinding.FragmentEmailPopupBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.Fullname;
import com.fanzine.coys.models.ProEmail;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.viewmodels.activities.TeaserViewModel;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by mbp on 4/12/18.
 */

public class TeaserFragment extends BaseFragment implements TeaserViewModel.EmailLoadListener {

    private static final String KEY_FULLNAME = "fullname";
    private static final String KEY_IS_BOTTOM_NAV_ON = "is_bottom_nav_on";

    FragmentEmailPopupBinding binding;
    TeaserViewModel viewModel;

    EmailAdapter mAdapter;

    Fullname mFullname;

    boolean isUpgradeFree = false;

    public static TeaserFragment newInstance(Fullname fullname, boolean isBottomNavOn) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_FULLNAME, fullname);
        args.putBoolean(KEY_IS_BOTTOM_NAV_ON, isBottomNavOn);

        TeaserFragment teaserFragment = new TeaserFragment();
        teaserFragment.setArguments(args);

        return teaserFragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentEmailPopupBinding.inflate(inflater, root, attachToRoot);
        viewModel = new TeaserViewModel(getContext(), this);

        binding.setViewModel(viewModel);
        setBaseViewModel(viewModel);

        mFullname = getArguments().getParcelable(KEY_FULLNAME);
        isUpgradeFree = getArguments().getBoolean(KEY_IS_BOTTOM_NAV_ON, false);

        checkFullName();

        setupEmailsRecyclerView();
        setupCheckout();
        setupSearchView();

        setFonts();

        binding.bottomNav.setVisibility(isUpgradeFree ? View.VISIBLE : View.GONE);

        return binding;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.load(mFullname);
    }

    @Override
    public void showEmails(List<ProEmail> allEmails) {

        if ( !isUpgradeFree) {
            List<ProEmail> availableEmails = new ArrayList<>();

            for (ProEmail proEmail : allEmails) {
                if (!proEmail.isTaken()) {
                    availableEmails.add(proEmail);
                }
            }

            mAdapter.replace(availableEmails);
        } else {
            mAdapter.replace(allEmails);
        }
    }


    private void setupCheckout() {
        binding.btCheckout.setOnClickListener(view -> {
            ProEmail proEmail = mAdapter.getChecked();
            if (proEmail == null) return;

            checkout(proEmail);
        });
    }

    private void setupEmailsRecyclerView() {
        mAdapter = new EmailAdapter(getContext());

        binding.rvEmails.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvEmails.setAdapter(mAdapter);
    }

    private void checkout(ProEmail proEmail) {
        Intent data = new Intent();
        String email = proEmail.getEmail();

        data.setData(Uri.parse(email));

        getActivity().setResult(RESULT_OK, data);

        if (!isUpgradeFree) {
            getActivity().finish();
        } else if (getActivity() instanceof MainActivity) {
            MainActivity activity = (MainActivity) getActivity();

            activity.startCheckout();
            activity.upgrade(email);
        }
    }

    private void setupSearchView() {
        binding.emailHeader.setOnClickListener(v -> {
            enableSearchView();
        });
        binding.defaultSearchTitle.setOnClickListener(view -> {
            enableSearchView();
        });


        binding.svFindEmails.setOnCloseListener(() -> {
            disableSearchView();
            return true;
        });

        binding.svFindEmails.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                viewModel.submit();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                binding.rvEmails.scrollToPosition(0);

                if (newText.contains("@")) {
                    DialogUtils.showErrorDialog(getContext(), "Invalid email");
                    return false;
                }

                if (newText.length() > 2) {
                    Fullname fullname = new Fullname(newText, mFullname.getLastName());
                    viewModel.autocomplete(fullname);

                    return true;
                }
                return false;
            }
        });

        setSearchviewTextSize(binding.svFindEmails, 12);
    }

    private void enableSearchView() {
        binding.defaultSearchTitle.setVisibility(View.GONE);
        binding.svFindEmails.setVisibility(View.VISIBLE);

        binding.svFindEmails.setIconified(false);

        binding.svFindEmails.requestFocus();
        binding.svFindEmails.setActivated(true);
        showKeyboard(binding.svFindEmails);
    }

    private void disableSearchView() {
        binding.svFindEmails.setVisibility(View.GONE);
        binding.defaultSearchTitle.setVisibility(View.VISIBLE);
    }

    private void checkFullName() {
        if (mFullname == null)
            throw new IllegalArgumentException("Full name is required");
    }

    private void showKeyboard(SearchView searchView) {
        final InputMethodManager inputMethodManager = (InputMethodManager) getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(searchView, InputMethodManager.SHOW_IMPLICIT);
    }

    private void setFonts() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/din condensed-bold.otf");

        binding.title1.setTypeface(font);
        binding.title2.setTypeface(font);
        binding.btCheckout.setTypeface(font);
    }

    private void setSearchviewTextSize(SearchView searchView, int fontSize) {
        try {
            AutoCompleteTextView autoCompleteTextViewSearch = (AutoCompleteTextView) searchView.findViewById(searchView.getContext().getResources().getIdentifier("app:id/search_src_text", null, null));
            if (autoCompleteTextViewSearch != null) {
                autoCompleteTextViewSearch.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
            } else {
                LinearLayout linearLayout1 = (LinearLayout) searchView.getChildAt(0);
                LinearLayout linearLayout2 = (LinearLayout) linearLayout1.getChildAt(2);
                LinearLayout linearLayout3 = (LinearLayout) linearLayout2.getChildAt(1);
                AutoCompleteTextView autoComplete = (AutoCompleteTextView) linearLayout3.getChildAt(0);
                autoComplete.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
            }
        } catch (Exception e) {
            LinearLayout linearLayout1 = (LinearLayout) searchView.getChildAt(0);
            LinearLayout linearLayout2 = (LinearLayout) linearLayout1.getChildAt(2);
            LinearLayout linearLayout3 = (LinearLayout) linearLayout2.getChildAt(1);
            AutoCompleteTextView autoComplete = (AutoCompleteTextView) linearLayout3.getChildAt(0);
            autoComplete.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
        }
    }
}
