package com.fanzine.coys.fragments.start;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.FragmentSelectPlanBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.viewmodels.fragments.login.SelectPlanViewModel;
import com.jakewharton.rxbinding.view.RxView;

/**
 * Created by Woland on 30.03.2017.
 */

public class SelectPlanFragment extends BaseFragment {

    private static final String FB_ID = "FB_ID";
    private static final String NAME = "name";
    private static final String COUNTRY = "country";
    private static final String GENDER = "gender";
    private static final String PRO = "pro";

    private FragmentSelectPlanBinding binding;
    private SelectPlanViewModel viewModel;

    public static SelectPlanFragment newInstance() {
        Bundle args = new Bundle();
        SelectPlanFragment fragment = new SelectPlanFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static SelectPlanFragment newInstance(String id, String name, String country, String gender) {
        Bundle args = new Bundle();
        args.putString(FB_ID, id);
        args.putString(NAME, name);
        args.putString(COUNTRY, country);
        args.putString(GENDER, gender);
        SelectPlanFragment fragment = new SelectPlanFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        if (binding == null || viewModel == null) {
            binding = FragmentSelectPlanBinding.inflate(inflater, root, attachToRoot);
            viewModel = new SelectPlanViewModel(getContext());
            binding.setViewModel(viewModel);

            RxView.clicks(binding.next).subscribe(aVoid -> {
                Bundle args = getArguments();
                args.putBoolean(PRO, viewModel.isPro.get());
                FragmentUtils.changeFragment(getActivity(), R.id.content_frame, RegistrationFragment.newInstance(args), true);
            });
        }

        return binding;
    }

}
