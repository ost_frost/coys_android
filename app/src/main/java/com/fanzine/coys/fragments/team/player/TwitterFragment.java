package com.fanzine.coys.fragments.team.player;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.SocialAdapter;
import com.fanzine.coys.databinding.FragmentPlayerSocialPostsBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.Social;

import java.util.ArrayList;

/**
 * Created by mbp on 1/13/18.
 */

public class TwitterFragment extends BaseFragment {

    private final static String POSTS = "posts";

    private FragmentPlayerSocialPostsBinding binding;

    private SocialAdapter mTwitterAdapter;

    public static TwitterFragment getInstance(ArrayList<Social> posts) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(POSTS, posts);

        TwitterFragment fragment = new TwitterFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {

        if (binding == null) {
            binding = FragmentPlayerSocialPostsBinding.inflate(inflater, root, attachToRoot);

            binding.rvList.setLayoutManager(new LinearLayoutManager(getContext()));
            mTwitterAdapter = new SocialAdapter(getContext(), getArguments().getParcelableArrayList(POSTS), new SocialAdapter.OnSocialListener() {
                @Override
                public void openPost(String url) {
                    try {
                        //todo:: backend return invalid url //www.twitter.com/@Emimartinezz1/status/970089381196378112
                        if (url.substring(0, 2).equals("//")) {
                            url = "https://" + url.substring(2, url.length());
                        }
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        getActivity().startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        Log.i(getTag(), e.getMessage());
                    }
                }

                @Override
                public void openImage(String url) {

                }
            });
            DividerItemDecoration divider = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
            Drawable dividerDrawable = ContextCompat.getDrawable(getContext(), R.drawable.divider_1dp);
            divider.setDrawable(dividerDrawable);
            binding.rvList.addItemDecoration(divider);
            binding.rvList.setAdapter(mTwitterAdapter);
        }
        return binding;
    }
}
