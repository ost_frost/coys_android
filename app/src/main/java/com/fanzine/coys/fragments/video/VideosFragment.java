package com.fanzine.coys.fragments.video;

import android.databinding.ViewDataBinding;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.VideoAdapter;
import com.fanzine.coys.databinding.FragmentRvVideosBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.fragments.like.LikeCallback;
import com.fanzine.coys.fragments.like.LikeViewModel;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.Video;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.utils.ShareUtil;
import com.fanzine.coys.viewmodels.fragments.video.ListVideoViewModel;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.List;

import static com.fanzine.coys.utils.LoadingStates.LOADING;
import static com.fanzine.coys.utils.LoadingStates.NO_DATA;

/**
 * Created by mbp on 1/8/18.
 */

public class VideosFragment extends BaseFragment implements DataListLoadingListener<Video>, LikeCallback<Video> {

    private final static String SORT_BY = "sort_by";
    private final static String UPLOAD_DATE = "upload_date";

    private int page = 0;

    private VideoAdapter adapter;

    private boolean isLoading;
    private int totalItemCount;
    private int lastVisibleItem;

    private ListVideoViewModel viewModel;
    private LikeViewModel likeViewModel;

    private FragmentRvVideosBinding binding;

    public static VideosFragment newInstance() {
        VideosFragment fragment = new VideosFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public static VideosFragment newInstance(String sortBy, String uploadDate) {
        VideosFragment fragment = new VideosFragment();
        Bundle args = new Bundle();
        args.putString(SORT_BY, sortBy);
        args.putString(UPLOAD_DATE, uploadDate);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onLoaded(List<Video> data) {
        if (binding.swipe.isRefreshing()) {
            binding.swipe.setRefreshing(false);
            isLoading = false;
        }

        if (data.size() > 0) {
            page++;
            isLoading = false;
        }

        onLoadedData(data);
    }

    @Override
    public void onLoaded(Video data) {

    }

    @Override
    public void onError() {
        if (binding.swipe.isRefreshing())
            binding.swipe.setRefreshing(false);

        isLoading = false;
    }

    private String uploadDate = "";
    private String sortBy = "";

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        likeViewModel = new LikeViewModel(getContext());
        uploadDate = getArguments().getString(UPLOAD_DATE);
        sortBy = getArguments().getString(SORT_BY);

        viewModel = new ListVideoViewModel(getContext(), this, sortBy, uploadDate);

        binding = FragmentRvVideosBinding.inflate(inflater, root, attachToRoot);
        binding.setViewModel(viewModel);
        binding.swipe.setColorSchemeResources(R.color.colorAccent);
        binding.swipe.setOnRefreshListener(() -> viewModel.loadData(0));

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        DividerItemDecoration divider = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        Drawable dividerDrawable = ContextCompat.getDrawable(getContext(), R.drawable.divider_1dp);
        divider.setDrawable(dividerDrawable);
        binding.rvVideoContent.addItemDecoration(divider);
        binding.rvVideoContent.setLayoutManager(linearLayoutManager);
        binding.rvVideoContent.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (NetworkUtils.isNetworkAvailable(getContext())) {
                        viewModel.loadData(page);
                        isLoading = true;
                    }
                }
            }
        });

        binding.tvFilterBtn.setOnClickListener(view -> {
            FragmentUtils.changeFragment(getFragmentManager(), R.id.content, SearchFilter.newInstance(sortBy, uploadDate), true);
        });

        RxTextView.textChanges(binding.search).subscribe(charSequence -> {
            if (charSequence.length() == 0) {
                loadData("");
            }
        });

        binding.search.setOnEditorActionListener((v, keyCode, event) -> {
            if (keyCode == EditorInfo.IME_ACTION_SEARCH) {
                hideKyboard();
                search();
                return true;
            }
            return false;
        });


        viewModel.loadData(page);
        return binding;
    }

    private void search() {
        loadData(binding.search.getText().toString());
    }

    private void loadData(@NonNull String query) {
        if (adapter != null) {
            adapter.clear();
        }
        viewModel.setState(LOADING);
        viewModel.setQuery(query);
        viewModel.loadData(0);
    }

    private void onLoadedData(List<Video> data) {
        if (adapter == null) {
            adapter = new VideoAdapter(getContext(), data);
            adapter.setCallback(new VideoAdapter.Callback() {
                @Override
                public void onLike(Video video) {
                    if (likeViewModel != null) {
                        if (video.isLiked) {
                            likeViewModel.unlikeVideo(video, VideosFragment.this);
                        } else {
                            likeViewModel.likeVideo(video, VideosFragment.this);
                        }
                    }
                }

                @Override
                public void onFbPostShare(Video video) {
                    ShareUtil.shareVideoToFacebook(getActivity(), video);
                }

                @Override
                public void onTwitterShare(Video video) {
                    ShareUtil.shareVideoToTwitter(getContext(), video);
                }

                @Override
                public void onChatShare(Video video) {
                    ShareUtil.shareVideoToChat(getContext(), video);
                }

                @Override
                public void onMailShare(Video video) {
                    ShareUtil.shareVideoToMail(getContext(), video);
                }

                @Override
                public void onClipboardCopy(Video video) {
                    ShareUtil.copyToClipboardVideo(getContext(), video);
                }
            });
            binding.rvVideoContent.setAdapter(adapter);
        } else
            adapter.addItems(data);

        if (data.isEmpty() && adapter.getItemCount() == 0) {
            viewModel.setState(NO_DATA);
        }
    }

    @Override
    public void onLiked(Video video) {
        if (adapter != null) {
            adapter.refreshItem(video);
        }
    }

    @Override
    public void onUnLiked(Video video) {
        if (adapter != null) {
            adapter.refreshItem(video);
        }
    }
}
