package com.fanzine.coys.fragments.video;


import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.FragmentListVideoBinding;
import com.fanzine.coys.fragments.base.BaseLeaguesBarFragment;
import com.fanzine.coys.models.League;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.viewmodels.fragments.video.BaseVideosViewModel;

import java.util.Collections;
import java.util.List;

public class ListVideoFragment extends BaseLeaguesBarFragment implements
        BaseVideosViewModel.LeagueLoadingListener {


    private FragmentListVideoBinding binding;


    public static ListVideoFragment newInstance() {
        ListVideoFragment fragment = new ListVideoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        Log.i(getClass().getName(), "ListVideoFragment onBindView");
        if (binding == null) {
            binding = FragmentListVideoBinding.inflate(inflater, root, false);
            BaseVideosViewModel viewModel = new BaseVideosViewModel(getContext(),this);
            binding.setViewModel(viewModel);
            setBaseViewModel(viewModel);

            viewModel.loadLeagues();

            FragmentUtils.changeFragment(getChildFragmentManager(), R.id.content, VideosFragment.newInstance(), false);
        }
        return binding;
    }

    @Override
    public void onTopPanelItemSelected(View view, int position) {
//        FragmentUtils.changeFragment(getActivity(), R.id.content_frame, newInstance(), false);
    }

    @Override
    public void onLeagueLoaded(List<League> list) {
        if (list.size() > 0) {
            Collections.sort(list, (league, t1) -> league.getListed_region_sort() - t1.getListed_region_sort());
            BaseLeaguesBarFragment.selected = list.get(list.size() / 2).getLeagueId();
            initTopPanel(list, binding.topPanel, 4);
        }
    }
}
