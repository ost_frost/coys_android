package com.fanzine.coys.fragments.venue;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.ImageAdapter;
import com.fanzine.coys.adapters.venues.VenuesReviewsAdapter;
import com.fanzine.coys.databinding.FragmentDetailBusinessNewBinding;
import com.fanzine.coys.databinding.MarkerDetailsBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.viewmodels.fragments.venue.DetailsVenusViewModel;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.yelp.fusion.client.models.Business;
import com.yelp.fusion.client.models.Review;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Woland on 15.03.2017.
 */

public class DetailsVenuesFragment extends BaseFragment implements OnMapReadyCallback {


    public static final String DATA = "data";
    public static final String REVIEWS = "reviews";

    public static final String VENUES_LIST_CALLBACK = "Venues_list_callback";

    private FragmentDetailBusinessNewBinding binding;
    private Business business;

    private OnFragmentChangeListener onFragmentChangeListener;

    public static DetailsVenuesFragment newInstance(ArrayList<Review> reviews, Business business, OnFragmentChangeListener onFragmentChangeListener) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(REVIEWS, reviews);
        bundle.putParcelable(DATA, business);
        bundle.putParcelable(VENUES_LIST_CALLBACK, onFragmentChangeListener);
        DetailsVenuesFragment fragment = new DetailsVenuesFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentDetailBusinessNewBinding.inflate(inflater, root, attachToRoot);

        DetailsVenusViewModel viewModel = new DetailsVenusViewModel(getContext());
        binding.setViewModel(viewModel);

        List<Review> reviews = getArguments().getParcelableArrayList(REVIEWS);
        business = getArguments().getParcelable(DATA);

        binding.rvPhotos.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.rvPhotos.setAdapter(new ImageAdapter(getContext(), business.getPhotos()));

        viewModel.business.set(business);


        if (reviews.size() > 0) {
            binding.rvReviews.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            binding.rvReviews.setAdapter(new VenuesReviewsAdapter(getContext(), reviews));

        } else
            binding.commentsTitle.setVisibility(View.GONE);

        binding.map.onCreate(null);
        binding.map.getMapAsync(this);

        Glide.with(getContext()).load(business.getImageUrl()).into(binding.image);

        onFragmentChangeListener = getArguments().getParcelable(VENUES_LIST_CALLBACK);

        binding.getRoot().setFocusableInTouchMode(true);
        binding.getRoot().requestFocus();
        binding.getRoot().setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                onFragmentChangeListener.redirectToBack();
                return true;
            }
            return false;
        });

        binding.ratingBar.setEmptyDrawableRes(R.drawable.star_empty);
        binding.ratingBar.setFilledDrawableRes(R.drawable.star_full);
        binding.ratingBar.setRating((float) business.getRating());

        return binding;
    }


    private void setupTabs(TabLayout tabLayout) {
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.customerReview)));
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            tabLayout.getTabAt(i).setCustomView(getTabView(tabLayout, i));
        }
    }

    private View getTabView(TabLayout tabLayout, int position) {
        TextView view = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.item_tab_details, tabLayout, false);
        view.setText(tabLayout.getTabAt(position).getText());
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.map.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.map.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.getUiSettings().setScrollGesturesEnabled(false);
        map.setOnMarkerClickListener(marker -> false);

        LatLng latLng = new LatLng(business.getCoordinates().getLatitude(), business.getCoordinates().getLongitude());

        IconGenerator mClusterIconGenerator = new IconGenerator(getContext());
        MarkerDetailsBinding binding = MarkerDetailsBinding.inflate(LayoutInflater.from(getContext()), null, false);
        mClusterIconGenerator.setContentView(binding.getRoot());
        mClusterIconGenerator.setBackground(null);
        binding.count.setText(business.getName());
        map.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromBitmap(mClusterIconGenerator.makeIcon())));

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
    }
}
