package com.fanzine.coys.fragments.profile;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vitaliygerasymchuk on 2/8/18
 */

public class ProfilePagerAdapter extends FragmentPagerAdapter {

    @NonNull
    private List<Fragment> fragments = new ArrayList<>();

    void addFragment(Fragment... fragments) {
        this.fragments.addAll(Arrays.asList(fragments));
        this.notifyDataSetChanged();
    }

    ProfilePagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position < fragments.size()) {
            return fragments.get(position);
        }
        return new Fragment();
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
