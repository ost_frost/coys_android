package com.fanzine.coys.fragments.match;


import android.content.res.Configuration;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.match.LineUpAdapter;
import com.fanzine.coys.adapters.match.SidelinedAdapter;
import com.fanzine.coys.adapters.match.SubstitutionAdapter;
import com.fanzine.coys.databinding.FragmentLineUpReworkBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.models.lineups.LineUp;
import com.fanzine.coys.viewmodels.fragments.match.LineUpFragmentViewModel;

import java.util.List;

import static com.fanzine.coys.models.Match.MATCH;

public class LineUpFragment extends BaseFragment implements DataListLoadingListener<LineUp> {

    private FragmentLineUpReworkBinding binding;

    public static LineUpFragment newInstance(Match match) {
        LineUpFragment fragment = new LineUpFragment();
        Bundle args = new Bundle();
        args.putParcelable(MATCH, match);
        fragment.setArguments(args);
        return fragment;
    }

    Match match;

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentLineUpReworkBinding.inflate(inflater, root, false);

        match = getArguments().getParcelable(MATCH);

        assert match != null;

        LineUpFragmentViewModel viewModel = new LineUpFragmentViewModel(getContext(), match.getId(), this);
        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);


        binding.rvSubstitutionsLocal.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        binding.rvSubstitutionsLocal.setNestedScrollingEnabled(false);

        binding.rvSubstitutionsVisitors.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        binding.rvSubstitutionsVisitors.setNestedScrollingEnabled(false);

        binding.rvSidelinedLocal.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        binding.rvSidelinedLocal.setNestedScrollingEnabled(false);

        binding.rvSidelinedVisitors.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        binding.rvSidelinedVisitors.setNestedScrollingEnabled(false);

        viewModel.loadData(0);

        return binding;
    }

    @Override
    public void onLoaded(LineUp data) {

        if (data.isLocalSquadExist() || data.isVisitorSquadExist()) {

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    int rvHeight = binding.footballField.getMeasuredHeight() / 2;
                    int rvWidth = binding.footballField.getMeasuredWidth();


                    int heightPlayer = (int) (binding.footballField.getMeasuredHeight() / 2 -
                            Math.round((binding.footballField.getMeasuredHeight() / 2) * 0.1));


                    binding.rvPlayers1.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                    binding.rvPlayers1.setNestedScrollingEnabled(false);

                    binding.rvPlayers2.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true));
                    binding.rvPlayers2.setNestedScrollingEnabled(false);

                    binding.rvPlayers1.getLayoutParams().width = rvWidth;
                    binding.rvPlayers2.getLayoutParams().width = rvWidth;
                    binding.rvPlayers1.getLayoutParams().height = rvHeight;
                    binding.rvPlayers2.getLayoutParams().height = rvHeight;


                    int topPading = rvHeight / 9;
                    int bottomPadding = rvHeight / 9;

                    if ((getResources().getConfiguration().screenLayout &
                            Configuration.SCREENLAYOUT_SIZE_MASK) ==
                            Configuration.SCREENLAYOUT_SIZE_XLARGE) {
                        bottomPadding = rvHeight / 12;
                    }


                    binding.rvPlayers1.setPadding(binding.rvPlayers1.getPaddingLeft(),
                            topPading, 0, 0);

                    binding.rvPlayers2.setPadding(binding.rvPlayers2.getPaddingLeft(),
                            0, 0, bottomPadding);

                    LineUpAdapter rvPlayers1Adapter = new LineUpAdapter(
                            getContext(),
                            data.getLocalteam().getSquad(),
                            data.getLocalteam().getSubstitution(),
                            true, heightPlayer, rvWidth);

                    if (data.getLocalteam().getSquad().getGoalkeeper() != null) {
                        binding.rvPlayers1.setAdapter(rvPlayers1Adapter);
                        rvPlayers1Adapter.notifyDataSetChanged();
                        binding.rvPlayers1.invalidate();
                    }

//                    heightPlayer = (int) (binding.footballField.getMeasuredHeight() / 2 -
//                            Math.round((binding.footballField.getMeasuredHeight() / 2) * 0.15));
                    if (data.getVisitorteam().getSquad().getGoalkeeper() != null) {
                        binding.rvPlayers2.setAdapter(new LineUpAdapter(
                                getContext(),
                                data.getVisitorteam().getSquad(),
                                data.getVisitorteam().getSubstitution(),
                                false,
                                heightPlayer, rvWidth));
                        rvPlayers1Adapter.notifyDataSetChanged();
                        binding.rvPlayers2.invalidate();
                    }
                }
            }, 100);
            binding.schema1.setText(data.getLocalteam().getSquad().getSchema());
            binding.schema2.setText(data.getVisitorteam().getSquad().getSchema());


            binding.sidelinedLayout.setVisibility(View.GONE);
            binding.sidelinedHeader.setVisibility(View.GONE);

            binding.rvSubstitutionsLocal.setAdapter(new SubstitutionAdapter(getContext(), data.getLocalteam().getSubstitution(), false));
            binding.rvSubstitutionsVisitors.setAdapter(new SubstitutionAdapter(getContext(), data.getVisitorteam().getSubstitution(), true));

        } else {
            binding.clFiled.setVisibility(View.GONE);
            binding.substitutionLayout.setVisibility(View.GONE);
            binding.substitutionHeader.setVisibility(View.GONE);

            binding.rvSidelinedLocal.setAdapter(new SidelinedAdapter(getContext(),
                    data.getLocalteam().getSidelined(), true));
            binding.rvSidelinedVisitors.setAdapter(new SidelinedAdapter(getContext(),
                    data.getVisitorteam().getSidelined(), false));
        }
    }

    @Override
    public void onLoaded(List<LineUp> data) {

    }

    @Override
    public void onError() {

    }

    private int getPlayerHeight() {
        return (int) (binding.footballField.getMeasuredHeight() / 2 -
                Math.round((binding.footballField.getMeasuredHeight() / 2) * getPlayerHeightCoef()));
    }

    private double getPlayerHeightCoef() {

        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)
                == Configuration.SCREENLAYOUT_SIZE_XLARGE)
            return 0.05;

        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)
                == Configuration.SCREENLAYOUT_SIZE_NORMAL)
            return 0.1;

        return 0.08;
    }
}
