package com.fanzine.coys.fragments.venue;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.venues.SearchItemsAdapter;
import com.fanzine.coys.databinding.FragmentVenuesSearchBinding;
import com.fanzine.coys.models.venues.VenueSearchItem;
import com.fanzine.coys.viewmodels.fragments.venue.VenueSearchActivityViewModel;
import com.fanzine.coys.widgets.UpcomingFixtures;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.yelp.fusion.client.models.Business;
import com.yelp.fusion.client.models.Term;

import java.util.ArrayList;
import java.util.List;

import static com.fanzine.coys.App.getContext;

/**
 * Created by mbp on 12/26/17.
 */

public class SearchActivity extends FragmentActivity implements
        SearchView.OnQueryTextListener,
        AutocompleteListener,
        SearchItemsAdapter.OnClickListener {

    public final static String LATLNG = "LatLng";

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 76;

    private VenueSearchActivityViewModel viewModel;
    private FragmentVenuesSearchBinding binding;

    private SearchItemsAdapter adapter;
    private SearchItemsAdapter mVenuesAdapter;

    private LatLng latLng;

    private LatLng defaultLatLng;

    private String mQueryString = "";
    private Handler mHandler = new Handler();

    private UpcomingFixtures upcomingFixturesWidget;

    private ProgressDialog pd;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_venues_search);

        viewModel = new VenueSearchActivityViewModel(this, this);
        binding.setViewModel(viewModel);

        SearchView searchView = binding.searchView;
        searchView.setOnQueryTextListener(this);

        adapter = new SearchItemsAdapter(this, this);
        mVenuesAdapter = new SearchItemsAdapter(this, this);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(this, R.drawable.divider));

        binding.rvCategories.setLayoutManager(new LinearLayoutManager(this));
        binding.rvCategories.addItemDecoration(itemDecorator);
        binding.rvCategories.setAdapter(adapter);

        binding.rvVenues.setLayoutManager(new LinearLayoutManager(this));
        binding.rvVenues.addItemDecoration(itemDecorator);
        binding.rvVenues.setAdapter(mVenuesAdapter);

        latLng = getIntent().getParcelableExtra(LATLNG);
        defaultLatLng = latLng;

        binding.btCancel.setOnClickListener(view -> {
            finish();
        });

        binding.searchView.setOnClickListener(view -> {
            binding.searchView.setIconified(false);
        });

        binding.llCategories.setVisibility(View.GONE);
        binding.llVenues.setVisibility(View.GONE);

        upcomingFixturesWidget = binding.upcomingFixtures;

        binding.btSearch.setOnClickListener(view -> {
            viewModel.autocomplete(binding.searchView.getQuery().toString(), getCurrentLatLng());
        });

        binding.tvUserLocation.setOnClickListener(view -> {
            try {
                Intent intent =
                        new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                .build(this);
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            } catch (GooglePlayServicesRepairableException e) {
                // TODO: Handle the error.
            } catch (GooglePlayServicesNotAvailableException e) {
                // TODO: Handle the error.
            }
        });

        binding.upcomingFixtures.getBinding().tvCurrentLocationFixtures.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                binding.tvUserLocation.setText("Input Address");

                return true;
            }
        });
    }

    @Override
    public void findBusiness(String category) {
        Intent startVenues = new Intent(getContext(), VenueFragment.class);
        startVenues.setAction(VenueFragment.ACTION_SEARCH);
        startVenues.putExtra(VenueFragment.CATEGORY, category);

        //lat lng by match from fixtures
        startVenues.putExtra(VenueFragment.LAT_LNG, getCurrentLatLng());

        startActivity(startVenues);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        viewModel.submit();
//
//        Log.i(getClass().getName(), "QUERY submit");

        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mQueryString = newText;
        viewModel.autocomplete(mQueryString, getCurrentLatLng());
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void showTerms(List<Term> data) {
        if (data.size() > 0) {
            binding.llCategories.setVisibility(View.VISIBLE);

            List<VenueSearchItem> categories = new ArrayList<>();
            for (Term category : data) {
                categories.add(new VenueSearchItem(category.getText(), category.getText()));
            }

            ((SearchItemsAdapter) binding.rvCategories.getAdapter()).addAll(categories);
        } else {
            binding.llCategories.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);

                latLng = place.getLatLng();

                binding.tvUserLocation.setText(place.getAddress().toString());
                binding.upcomingFixtures.clear();

                Log.i(getClass().getName(), "Place: " + place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getContext(), data);
                // TODO: Handle the error.
                Log.i(getClass().getName(), status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void showVenues(List<Business> businesses) {
        if (businesses.size() > 0) {
            binding.llVenues.setVisibility(View.VISIBLE);

            mVenuesAdapter.clear();

            List<VenueSearchItem> venues = new ArrayList<>();
            for (Business business : businesses) {
                venues.add(new VenueSearchItem(business.getName(), business.getName()));
            }
            mVenuesAdapter.addAll(venues);

            Log.i(SearchActivity.class.getName(), "mQueryString: " + mQueryString);
            Log.i(SearchActivity.class.getName(), "mLatLong: " + getCurrentLatLng().latitude + " ," + getCurrentLatLng().longitude);
        } else {
            binding.llVenues.setVisibility(View.GONE);
        }
    }

    @Override
    public void autocompleteError() {
        mVenuesAdapter.clear();
        adapter.clear();

        if (pd != null) pd.dismiss();
    }

    private LatLng getCurrentLatLng() {
        if (binding.upcomingFixtures.isSearchByMatch()) {
            return binding.upcomingFixtures.getLatLng();
        } else if (binding.upcomingFixtures.isLocationEmpty()) {
            return latLng;
        }
        return defaultLatLng;
    }
}
