package com.fanzine.coys.fragments.table;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.table.FixturesAdapter;
import com.fanzine.coys.databinding.FragmentTableFixturesBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.utils.LoadingStates;
import com.fanzine.coys.utils.NetworkUtils;
import com.fanzine.coys.viewmodels.fragments.table.FixturesViewModel;
import com.google.firebase.crash.FirebaseCrash;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximdrobonoh on 25.09.17.
 */

public class TableFixturesFragment extends BaseFragment
        implements DataListLoadingListener<Match> {

    FragmentTableFixturesBinding binding;
    FixturesViewModel viewModel;

    private FixturesAdapter adapter;

    private List<Match> mMatches = new ArrayList<>();

    public static TableFixturesFragment newInstance() {
        Bundle args = new Bundle();
        TableFixturesFragment fragment = new TableFixturesFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {

        if (binding == null) {
            binding = FragmentTableFixturesBinding.inflate(inflater, root, attachToRoot);

            viewModel = new FixturesViewModel(getContext(), this);

            binding.setViewModel(viewModel);
            setBaseViewModel(viewModel);

            binding.swipe.setColorSchemeResources(R.color.colorAccent);

            viewModel.loadCurrent();

            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);


            binding.rvLigueMatches.setLayoutManager(linearLayoutManager);

            RxView.clicks(binding.fixtureArrowLeft).subscribe(aVoid -> {

                if (viewModel.canLoadPrev()) {
                    adapter.clear();

                    viewModel.setState(LoadingStates.LOADING);
                    viewModel.loadData(viewModel.week.get() - 1);
                }
            });
            RxView.clicks(binding.fixtureArrowRight).subscribe(aVoid -> {
                if (viewModel.canLoadNext()) {
                    adapter.clear();

                    viewModel.setState(LoadingStates.LOADING);
                    viewModel.loadData(viewModel.week.get() + 1);
                }
            });

            adapter = new FixturesAdapter(getContext());
            binding.rvLigueMatches.setAdapter(adapter);

            setupScrollListener(binding.rvLigueMatches, linearLayoutManager, binding.swipe);

        }
        return binding;
    }


    @Override
    public void onLoaded(List<Match> data) {
        mMatches.clear();
        mMatches.addAll(data);

        page = 0;

        adapter.addAll(data.subList(0, data.size() > 10 ? 10 : data.size() - 1));
    }

    @Override
    public void onLoaded(Match data) {

    }

    @Override
    public void onError() {

    }

    protected int page = 1;

    protected int totalItemCount;
    protected int lastVisibleItem;
    protected boolean isLoading;


    public void setupScrollListener(RecyclerView rv, LinearLayoutManager layoutManager, SwipeRefreshLayout refreshLayout) {
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                viewModel.loadData(viewModel.week.get());
                refreshLayout.setEnabled(false);
            }
        });


        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();


                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (NetworkUtils.isNetworkAvailable(getContext())) {
                        isLoading = true;

                        page++;

                        try {
                            int end = (
                                    adapter.getItemCount() + 10 < mMatches.size() ? adapter.getItemCount() + 10 : mMatches.size() - adapter.getItemCount());

                            int start = adapter.getItemCount() - 1;

                            Log.i(getClass().getName(), "START " + start);
                            Log.i(getClass().getName(), "END " + end);

                            if ((start > 0 && end > 0) && start < end)
                                adapter.addAll(mMatches.subList(start, end));

                            handler.postDelayed(() -> {
                                isLoading = false;
                            }, 400);
                        } catch (IllegalArgumentException e) {
                            FirebaseCrash.report(e);
                        }

                    }
                }
            }
        });
    }

    final android.os.Handler handler = new android.os.Handler();

}
