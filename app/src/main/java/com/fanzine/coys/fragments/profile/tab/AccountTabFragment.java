package com.fanzine.coys.fragments.profile.tab;

import android.app.AlertDialog;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.login.CountryAdapter;
import com.fanzine.coys.databinding.DialogProfileCreditDetailsBinding;
import com.fanzine.coys.databinding.DialogProfileResetPasswordBinding;
import com.fanzine.coys.databinding.DialogProfileTermsBinding;
import com.fanzine.coys.databinding.DialogRecyclerviewBinding;
import com.fanzine.coys.databinding.FragmentProfileAccountTabBinding;
import com.fanzine.coys.databinding.PhoneNumberDialogBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.login.Country;
import com.fanzine.coys.models.login.CountryData;
import com.fanzine.coys.models.login.UserToken;
import com.fanzine.coys.models.profile.Profile;
import com.fanzine.coys.models.profile.ProfileBody;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.coys.utils.RecyclerItemClickListener;
import com.fanzine.coys.viewmodels.fragments.profile.ProfileAccountViewModel;
import com.jakewharton.rxbinding.view.RxView;

import java.util.List;

import static android.text.TextUtils.isEmpty;

/**
 * Created by vitaliygerasymchuk on 2/10/18
 */

public class AccountTabFragment extends BaseFragment implements ProfileAccountViewModel.OnTermsCallback {

    public static AccountTabFragment newInstance(Profile profile) {
        Bundle args = new Bundle();
        args.putParcelable(PROFILE, profile);
        AccountTabFragment fragment = new AccountTabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private static final String PROFILE = "PROFILE";

    @Nullable
    private ProfileAccountViewModel viewModel;
    @Nullable
    private FragmentProfileAccountTabBinding binding;

    @Nullable
    private AlertDialog dialogTerms;
    @Nullable
    private AlertDialog dialogCountries;
    @Nullable
    private AlertDialog dialogCardDetails;
    @Nullable
    private AlertDialog dialogResetPassword;
    @Nullable
    private AlertDialog dialogPhoneNo;

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        final Profile profile = getArguments().getParcelable(PROFILE);
        viewModel = new ProfileAccountViewModel(getContext());
        viewModel.setProfile(profile);
        viewModel.getTermsOfUsage(this);
        setBaseViewModel(viewModel);

        binding = FragmentProfileAccountTabBinding.inflate(LayoutInflater.from(getContext()));
        binding.setViewModel(viewModel);
        handleClicks();
        return binding;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (viewModel != null) {
            viewModel.isVisible = isVisibleToUser;
        }
    }

    private void handleClicks() {
        if (binding != null) {
            RxView.clicks(binding.countryCodeClick).subscribe((aVoid) -> openCountryDialog());
            RxView.clicks(binding.save).subscribe((aVoid) -> saveProfile());
            RxView.clicks(binding.cancel).subscribe((aVoid) -> Toast.makeText(getContext(), "CANCEL", Toast.LENGTH_SHORT).show());
            RxView.clicks(binding.passwordResetClick).subscribe((aVoid) -> openResetPasswordDialog());
            RxView.clicks(binding.changeCreditCardClick).subscribe((aVoid) -> openChangeCreditDetailsDialog());
            RxView.clicks(binding.termsClick).subscribe((aVoid) -> openTermsDialog());
            RxView.clicks(binding.changePhoneNoClick).subscribe((aVoid) -> openPhoneNumberDialog());
        }
    }

    private void openCountryDialog() {
        List<Country> countries = CountryData.getCategories(getContext());

        DialogRecyclerviewBinding bindingDialog = DialogRecyclerviewBinding.inflate(LayoutInflater.from(getContext()));
        bindingDialog.rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        CountryAdapter adapter = new CountryAdapter(getContext(), countries);
        bindingDialog.rv.setAdapter(adapter);

        bindingDialog.rv.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), (view, position) -> {
            final Country country = countries.get(position);

            if (viewModel != null) {
                viewModel.setCountry(country);
            }
            dismissDialog(dialogCountries);
        }));

        bindingDialog.executePendingBindings();

        dialogCountries = new AlertDialog.Builder(getContext())
                .setView(bindingDialog.getRoot())
                .setNegativeButton(R.string.cancel, null)
                .show();

        Window window = dialogCountries.getWindow();

        if (window != null) {
            window.setBackgroundDrawableResource(R.color.backgroundDialog);
        }
    }

    private void openResetPasswordDialog() {
        DialogProfileResetPasswordBinding dialogBinding = DialogProfileResetPasswordBinding.inflate(LayoutInflater.from(getContext()));

        dialogResetPassword = new AlertDialog.Builder(getContext())
                .setView(dialogBinding.getRoot())
                .show();

        RxView.clicks(dialogBinding.save).subscribe((aVoid) -> {

            final String currentPass = dialogBinding.edtCurrentPass.getText().toString();
            final String newPass = dialogBinding.edtNewPass.getText().toString();
            final String confirmedPass = dialogBinding.edtConfirmPass.getText().toString();

            if (isEmpty(currentPass)) {
                dialogBinding.edtCurrentPass.setError(getString(R.string.empty_field));
            } else if (isEmpty(newPass)) {
                dialogBinding.edtNewPass.setError(getString(R.string.empty_field));
            } else if (isEmpty(confirmedPass)) {
                dialogBinding.edtConfirmPass.setError(getString(R.string.empty_field));
            } else if (!TextUtils.equals(newPass, confirmedPass)) {
                dialogBinding.edtConfirmPass.setError(getString(R.string.password_not_match));
            } else if (newPass.length() < 6) {
                dialogBinding.edtNewPass.setError(getString(R.string.password_too_week));
            } else {
                if (viewModel != null) {
                    viewModel.resetPassword(currentPass, newPass, confirmedPass, () -> {
                        dismissDialog(dialogResetPassword);
                    });
                }
            }
        });
        dialogBinding.executePendingBindings();
        RxView.clicks(dialogBinding.cancel).subscribe((aVoid) -> dismissDialog(dialogResetPassword));
    }

    private void openPhoneNumberDialog() {
        PhoneNumberDialogBinding dialogBinding = PhoneNumberDialogBinding.inflate(LayoutInflater.from(getContext()));

        dialogPhoneNo = new AlertDialog.Builder(getContext())
                .setView(dialogBinding.getRoot())
                .show();

        RxView.clicks(dialogBinding.save).subscribe((aVoid) -> {

            final String currentNo = dialogBinding.edtCurrentNo.getText().toString();
            final String newNo = dialogBinding.edtNewNo.getText().toString();
            final String confirmedNo = dialogBinding.edtConfirmNo.getText().toString();

            if (isEmpty(currentNo)) {
                dialogBinding.edtCurrentNo.setError(getString(R.string.empty_field));
            } else if (isEmpty(newNo)) {
                dialogBinding.edtNewNo.setError(getString(R.string.empty_field));
            } else if (isEmpty(confirmedNo)) {
                dialogBinding.edtConfirmNo.setError(getString(R.string.empty_field));
            } else if (!TextUtils.equals(newNo, confirmedNo)) {
                dialogBinding.edtConfirmNo.setError(getString(R.string.phone_not_match));
            } else {
                if (binding != null) {
                    binding.edtPhoneNo.setText(confirmedNo);
                    dismissDialog(dialogPhoneNo);
                }
            }

        });
        dialogBinding.executePendingBindings();
        RxView.clicks(dialogBinding.cancel).subscribe((aVoid) -> dismissDialog(dialogPhoneNo));
    }

    private void openChangeCreditDetailsDialog() {
        DialogProfileCreditDetailsBinding dialogBinding = DialogProfileCreditDetailsBinding.inflate(LayoutInflater.from(getContext()));

        dialogCardDetails = new AlertDialog.Builder(getContext())
                .setView(dialogBinding.getRoot())
                .show();

        RxView.clicks(dialogBinding.cancel).subscribe((aVoid) -> dismissDialog(dialogCardDetails));
        RxView.clicks(dialogBinding.save).subscribe((aVoid) -> {
            final String name = dialogBinding.cardName.getText().toString();
            final String cardNo = dialogBinding.cardNum.getText().toString();
            final String expiresAt = dialogBinding.expires.getText().toString();
            final String cvv = dialogBinding.cvv.getText().toString();

            if (isEmpty(name)) {
                dialogBinding.cardName.setError(getErrorString());
            } else if (isEmpty(cardNo)) {
                dialogBinding.cardNum.setError(getErrorString());
            } else if (isEmpty(expiresAt)) {
                dialogBinding.expires.setError(getErrorString());
            } else if (isEmpty(cvv)) {
                dialogBinding.cvv.setError(getErrorString());
            } else {
                dismissDialog(dialogCardDetails);
            }
        });

        dialogBinding.expires.addTextChangedListener(new CardExpiresWatcher(dialogBinding.expires));
        dialogBinding.cardNum.addTextChangedListener(new CardNumberWatcher(dialogBinding.cardNum));

        dialogBinding.executePendingBindings();
    }

    private void openTermsDialog() {
        DialogProfileTermsBinding dialogBinding = DialogProfileTermsBinding.inflate(LayoutInflater.from(getContext()));
        if (viewModel != null) {
            if (isEmpty(viewModel.terms.get())) {
                viewModel.getTermsOfUsage(this);
            }
        }
        dialogBinding.setViewModel(viewModel);
        dialogTerms = new AlertDialog.Builder(getContext())
                .setView(dialogBinding.getRoot())
                .show();
    }

    //TODO - post method does not contain all the fields which are present on the ui!!!
    private void saveProfile() {
        if (viewModel != null && binding != null) {

            final String firstName = getTextViewString(binding.edtFirstName);
            final String lastName = getTextViewString(binding.edtLastName);
            final String birthday = getTextViewString(binding.edtBirthday);
            final String email = getTextViewString(binding.edtEmail);
            final String phoneCode = getTextViewString(binding.tvSelectedCountryCode);
            final String phone = getTextViewString(binding.edtPhoneNo);

            if (isEmpty(firstName)) {
                binding.edtFirstName.setError(getErrorString());
            } else if (isEmpty(lastName)) {
                binding.edtLastName.setError(getErrorString());
            } else if (isEmpty(birthday)) {
                binding.edtBirthday.setError(getErrorString());
            } else if (isEmpty(email)) {
                binding.edtBirthday.setError(getErrorString());
            } else if (isEmpty(phoneCode)) {
                binding.tvSelectedCountryCode.setError(getErrorString());
            } else if (isEmpty(phone)) {
                binding.edtPhoneNo.setError(getErrorString());
            } else {
                final UserToken token = SharedPrefs.getUserToken();
                if (token != null) {

                    ProfileBody body = new ProfileBody();
                    body.setId(token.getUserId());
                    body.setFirstName(firstName);
                    body.setPhoneCode(phoneCode);
                    body.setLastName(lastName);
                    body.setPhone(phone);

                    viewModel.setProfile(body,
                            profile -> {
                                token.setFirstName(firstName);
                                token.setPhonecode(phoneCode);
                                token.setLastName(lastName);
                                token.setPhone(phone);
                                SharedPrefs.saveUserToken(token);
                                Toast.makeText(getContext(), "Profile updated", Toast.LENGTH_SHORT).show();
                            });
                }
            }
        }
    }

    private String getTextViewString(@NonNull TextView tv) {
        return tv.getText().toString();
    }

    private String getErrorString() {
        return getString(R.string.empty_field);
    }

    private void dismissDialog(@Nullable AlertDialog dialog) {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    @Override
    public void onTerms(String termsString) {
        if (viewModel != null) {
            viewModel.setTerms(termsString);
        }
    }

    private class CardNumberWatcher extends AbsTextWatcher {

        private boolean isDelete;

        CardNumberWatcher(EditText editText) {
            super(editText);
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            super.onTextChanged(s, start, before, count);
            isDelete = before != 0;
        }

        @Override
        protected void textChanged(Editable s) {
            String source = s.toString();
            int length = source.length();
            stringBuilder.setLength(0);
            stringBuilder.append(source);

            if (length > 0 && length % 5 == 0) {
                if (isDelete) {
                    stringBuilder.deleteCharAt(length - 1);
                } else {
                    stringBuilder.insert(length - 1, " ");
                }
                editText.setText(stringBuilder);
                editText.setSelection(stringBuilder.length());
            }
        }
    }

    private class CardExpiresWatcher extends AbsTextWatcher {

        CardExpiresWatcher(EditText editText) {
            super(editText);
        }

        @Override
        protected void textChanged(Editable s) {
            if (s.length() >= 3 && !s.toString().contains("/")) {
                stringBuilder.append(s);
                final char lastChar = stringBuilder.charAt(stringBuilder.lastIndexOf(s.toString()));
                stringBuilder.setCharAt(2, '/');
                stringBuilder.append(lastChar);
                editText.setText(stringBuilder);
                editText.setSelection(stringBuilder.length());
                stringBuilder.setLength(0);
            }
        }
    }

    abstract class AbsTextWatcher implements TextWatcher {

        EditText editText;

        StringBuilder stringBuilder = new StringBuilder();

        AbsTextWatcher(EditText editText) {
            this.editText = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            editText.removeTextChangedListener(this);
            textChanged(s);
            editText.addTextChangedListener(this);
        }

        protected abstract void textChanged(Editable s);
    }

}
