package com.fanzine.coys.fragments.match;


import android.content.res.Resources;
import android.databinding.ViewDataBinding;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.match.StatsAdapter;
import com.fanzine.coys.databinding.FragmentMatchInfoContentBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.models.Stats;
import com.fanzine.coys.models.StatsGoals;
import com.fanzine.coys.models.StatsTypes;
import com.fanzine.coys.utils.CollectionUtil;
import com.fanzine.coys.viewmodels.fragments.match.StatsFragmentViewModel;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;
import java.util.List;

public class StatsFragment extends BaseFragment implements DataListLoadingListener<Stats> {

    private FragmentMatchInfoContentBinding binding;
    private Match match;
    private StatsFragmentViewModel viewModel;
    private StatsAdapter adapter;

    public static StatsFragment newInstance(Match match) {
        StatsFragment fragment = new StatsFragment();
        Bundle args = new Bundle();
        args.putParcelable(Match.MATCH, match);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            this.match = getArguments().getParcelable(Match.MATCH);
        }
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentMatchInfoContentBinding.inflate(inflater, root, false);
        viewModel = new StatsFragmentViewModel(getContext(), this, match);
        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);

        adapter = new StatsAdapter(getContext());
        binding.rvContent.setLayoutManager(new GridLayoutManager(getContext(), 2));
        binding.rvContent.setAdapter(adapter);

        viewModel.loadData(0);

        int centerTextFontSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics());

        binding.pieChart.setCenterTextSizePixels(centerTextFontSize);
        binding.pieChart.setDrawCenterText(true);
        binding.pieChart.setHoleRadius(65f);
        binding.pieChart.setCenterText("TOTAL POSSESSION");
        binding.pieChart.getDescription().setEnabled(false);
        binding.pieChart.getLegend().setEnabled(false);
        binding.pieChart.setDrawSlicesUnderHole(false);
        binding.pieChart.setTouchEnabled(false);
        binding.pieChart.setRotationEnabled(false);


        Typeface robotoMedium = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/roboto/Roboto-Medium.ttf");

        binding.pieChart.setCenterTextTypeface(robotoMedium);
        binding.textView14.setTypeface(robotoMedium);
        binding.tvCorners.setTypeface(robotoMedium);
        binding.tvOffsides.setTypeface(robotoMedium);
        binding.tvRedCards.setTypeface(robotoMedium);
        binding.tvYellowCards.setTypeface(robotoMedium);


        return binding;
    }

    @Override
    public void onLoaded(List<Stats> data) {
        Stats possesion = CollectionUtil.findStatByTitle(StatsTypes.PIE_CHART, data);
        Stats shotsTotal = CollectionUtil.findStatByTitle(StatsTypes.SHOATS_TOTAL, data);
        Stats shotsOnGoal = CollectionUtil.findStatByTitle(StatsTypes.SHOATS_ON_GOAL, data);
        Stats corners = CollectionUtil.findStatByTitle(StatsTypes.CORNERS, data);
        Stats offsides = CollectionUtil.findStatByTitle(StatsTypes.OFFSIDES, data);
        Stats yellowCards = CollectionUtil.findStatByTitle(StatsTypes.YELLOW_CARDS, data);
        Stats redCards = CollectionUtil.findStatByTitle(StatsTypes.RED_CARDS, data);

        List<Stats> blackList = new ArrayList<>();

        blackList.add(possesion);
        blackList.add(shotsOnGoal);
        blackList.add(shotsTotal);
        blackList.add(corners);
        blackList.add(offsides);
        blackList.add(yellowCards);
        blackList.add(redCards);

        if (possesion != null) {
            PieEntry item = new PieEntry(possesion.getHomeValue(), 0);
            PieEntry item2 = new PieEntry(possesion.getGuestValue(), 1);

            List<PieEntry> items = new ArrayList<>();
            items.add(item);
            items.add(item2);

            int[] colors = new int[]{R.color.colorPrimary, R.color.stats_red};

            PieDataSet dataset = new PieDataSet(items, "");
            dataset.setColors(colors, getContext());
            dataset.setDrawValues(false);

            Resources r = getResources();

            viewModel.totalPossesion.set(possesion);

            binding.pieChart.setData(new PieData(dataset));
            binding.pieChart.setMaxAngle(180f);
            binding.pieChart.spin(500, 0, -180f, Easing.EasingOption.EaseInOutQuad);
            binding.pieChart.invalidate();
            binding.pieChart.setExtraOffsets(0, 0, 0, 0);
            binding.pieChart.animateY(1300);
            binding.pieChart.setExtraBottomOffset(-25);
            binding.pieChart.getLayoutParams().height = Math.round(
                    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 150, r.getDisplayMetrics()));
            binding.pieChart.setCenterTextOffset(0, -20);

            binding.pieChart.invalidate();
        }

        if (shotsTotal != null && shotsOnGoal != null) {

            StatsGoals statsGoals = new StatsGoals(shotsOnGoal, shotsTotal);
            viewModel.statsGoals.set(statsGoals);

            showShotsDiagram(shotsTotal, binding.homeTeam, binding.guestTeam);
            showShotsDiagram(shotsOnGoal, binding.homeTeamInner, binding.guestTeamInner);
        }

        for (Stats stat : blackList) {
            data.remove(stat);
        }

        viewModel.corners.set(corners);
        viewModel.offsides.set(offsides);
        viewModel.yellowCards.set(yellowCards);
        viewModel.redCards.set(redCards);

        adapter.changeData(data);
    }

    @Override
    public void onLoaded(Stats data) {

    }

    @Override
    public void onError() {

    }

    private void showShotsDiagram(Stats stats, TextView homeTeam, TextView guestTeam) {

        int homeValue = stats.getHomeValue();
        int guestValue = stats.getGuestValue();

        if (guestValue == 0 && homeValue == 0) {

        } else {
            {
                LinearLayout.LayoutParams loparams = (LinearLayout.LayoutParams) homeTeam.getLayoutParams();
                loparams.weight = homeValue;
                homeTeam.setLayoutParams(loparams);
            }

            {
                LinearLayout.LayoutParams loparamsn = (LinearLayout.LayoutParams) guestTeam.getLayoutParams();
                loparamsn.weight = guestValue;
                guestTeam.setLayoutParams(loparamsn);
            }

        }
    }
}
