package com.fanzine.coys.fragments.team;

import android.databinding.ViewDataBinding;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.team.PlayersAdapter;
import com.fanzine.coys.databinding.FragmentTeamRecyclerViewBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.fragments.team.player.PlayerPagerFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.CarouselFilter;
import com.fanzine.coys.models.team.Player;
import com.fanzine.coys.models.team.PositionType;
import com.fanzine.coys.models.team.TeamSquad;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.utils.LoadingStates;
import com.fanzine.coys.viewmodels.fragments.team.TeamFragmentViewModel;

import java.util.List;

/**
 * Created by vitaliygerasymchuk on 2/20/18
 */

public abstract class AbsPlayersFragment extends BaseFragment implements PlayersAdapter.PlayerClickListener, DataListLoadingListener<Player> {

    protected FragmentTeamRecyclerViewBinding binding;

    protected abstract TeamSquad getSquad();

    @Nullable
    private PlayersAdapter adapter;

    @Nullable
    private TeamFragmentViewModel viewModel;

    @NonNull
    private String currentFilter = PositionType.GOALKEEPER;

    protected void load(TeamSquad squad) {
        if (viewModel != null) {
            if (adapter != null) {
                adapter.clear();

                viewModel.setState(LoadingStates.LOADING);
                viewModel.setTeamSquad(squad);
                viewModel.loadData(0);
            }
        }
    }

    public void filterPlayers(@NonNull CarouselFilter carouselFilter) {
        filterPlayers(carouselFilter.getTitle());
    }

    private void filterPlayers(@NonNull String filter) {
        if (adapter != null) {
            this.currentFilter = filter;
            adapter.getFilter().filter(currentFilter);
        }
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentTeamRecyclerViewBinding.inflate(inflater, root, attachToRoot);
        adapter = new PlayersAdapter(getContext(), getSquad(), this);
        viewModel = new TeamFragmentViewModel(getContext(), this, getSquad());
        binding.setViewModel(viewModel);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        binding.recView.setLayoutManager(linearLayoutManager);
        Drawable dividerDrawable = ContextCompat.getDrawable(getContext(), R.drawable.divider_1dp);
        DividerItemDecoration divider = new DividerItemDecoration(getContext(), linearLayoutManager.getOrientation());
        divider.setDrawable(dividerDrawable);
        binding.recView.addItemDecoration(divider);
        binding.recView.setAdapter(adapter);
        load(getSquad());
        setBaseViewModel(viewModel);
        Log.d("AbsPlayerFragment", "Squad " + getSquad());
        return binding;
    }

    @Override
    public void showPlayer(Player player) {
        FragmentUtils.changeFragment(
                getActivity(),
                R.id.content_frame,
                PlayerPagerFragment.newInstance(player, getSquad()),
                true
        );
    }

    @Override
    public void onLoaded(List<Player> data) {
        if (adapter != null) {
            adapter.replace(data);
            filterPlayers(currentFilter);
        }
    }

    @Override
    public void onLoaded(Player data) {
        //no-op
    }

    @Override
    public void onError() {
        //no-op
    }
}

