package com.fanzine.coys.fragments.base;

import android.content.Context;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.fanzine.coys.utils.KeyboardUtils;
import com.fanzine.coys.viewmodels.base.BaseViewModel;

/**
 * Created by Woland on 11/11/2016.
 */

public abstract class BaseFragment extends Fragment {

    private BaseViewModel baseViewModel;

    protected int visibleThreshold = 2;

    @Nullable
    public abstract ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot);

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return onBindView(inflater, container, false).getRoot();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (baseViewModel != null)
            baseViewModel.destroy();
    }

    protected void setBaseViewModel(BaseViewModel model) {
        this.baseViewModel = model;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        KeyboardUtils.hideSoftKeyboard(getActivity());
    }

    public final AppCompatActivity getAppCompatActivity() {
        if (getActivity() instanceof AppCompatActivity)
            return (AppCompatActivity) getActivity();

        return null;
    }


    protected void hideKyboard() {
        if (getActivity() != null) {
            View view = getActivity().getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        }
    }
}
