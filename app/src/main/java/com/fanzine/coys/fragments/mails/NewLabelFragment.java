package com.fanzine.coys.fragments.mails;

import android.databinding.ViewDataBinding;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.FragmentLabelCreateBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.mails.Color;
import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.viewmodels.fragments.mails.NewLabelViewModel;
import com.jakewharton.rxbinding.view.RxView;
import com.thebluealliance.spectrum.SpectrumPalette;;import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximdrobonoh on 14.09.17.
 */

public class NewLabelFragment extends BaseFragment implements SpectrumPalette.OnColorSelectedListener {

    private FragmentLabelCreateBinding binding;
    private NewLabelViewModel viewModel;

    private List<Color> colors;

    private static final String COLORS_LIST = "color_list";
    private static final String MAIL = "mail";

    private String selectedColor = "#F44336";

    public static NewLabelFragment newInstance(ArrayList<Color> colors, Mail mail) {
        NewLabelFragment fragment = new NewLabelFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(COLORS_LIST, colors);
        args.putParcelable(MAIL, mail);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onColorSelected(@ColorInt int color) {
        selectedColor = "#" + Integer.toHexString(color).toUpperCase().substring(2);
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        this.binding = FragmentLabelCreateBinding.inflate(inflater, root, attachToRoot);
        this.viewModel = new NewLabelViewModel(getContext());
        this.binding.setViewModel(viewModel);

        binding.palette.setOnColorSelectedListener(this);

        RxView.clicks(this.binding.createLabel).subscribe(aVoid -> {
            handleOnCreateLabel();
        });

        setPalette();
        setToolbar();
        setBaseViewModel(viewModel);

        return binding;
    }

    private void setToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(binding.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(ContextCompat.getColor(getContext(), R.color.inbox_bg_home_button), PorterDuff.Mode.SRC_ATOP);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }

    private void setPalette() {
        try {
            colors = getArguments().getParcelableArrayList(COLORS_LIST);
            int[] colorsIds = new int[colors.size()];

            for (int i = 0; i < colors.size(); i++) {
                colorsIds[i] = android.graphics.Color.parseColor(colors.get(i).getHex());
            }
            binding.palette.setColors(colorsIds);

        } catch (NullPointerException ex) {
            //// TODO: 18.09.17 handle exception
        }
    }

    private void handleOnCreateLabel() {
        try {
            int colorId = findPickedColor().getId();

            Mail mail = getArguments().getParcelable(MAIL);
            viewModel.createLabel(colorId, mail);
        } catch (Exception ex) {
            //// TODO: 18.09.17 handle exception
        }
    }

    private Color findPickedColor() throws Exception {
        for (Color color : colors) {
            if (color.getHex().equals(selectedColor)) {
                return color;
            }
        }
        throw new Exception("Color does not found");
    }
}
