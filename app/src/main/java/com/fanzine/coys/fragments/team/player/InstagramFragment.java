package com.fanzine.coys.fragments.team.player;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.fanzine.coys.R;
import com.fanzine.coys.adapters.SocialAdapter;
import com.fanzine.coys.databinding.FragmentPlayerSocialPostsBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.fragments.team.ImagePreviewActivity;
import com.fanzine.coys.models.Social;
import com.fanzine.coys.models.team.PlayerPhoto;
import com.fanzine.coys.utils.GridDividerDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbp on 1/13/18.
 */

public class InstagramFragment extends BaseFragment {

    private final static String POSTS = "posts";

    private FragmentPlayerSocialPostsBinding binding;

    private SocialAdapter listAdapter;

    @NonNull
    private final InstagramGridAdapter gridAdapter = new InstagramGridAdapter();

    private List<Social> data = new ArrayList<>();

    private GridLayoutManager gridLayoutManager;

    private LinearLayoutManager linearLayoutManager;

    private GridDividerDecoration gridDividerDecoration;

    public static InstagramFragment getInstance(ArrayList<Social> posts) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(POSTS, posts);

        InstagramFragment fragment = new InstagramFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        gridLayoutManager = new GridLayoutManager(getContext(), 3);
        linearLayoutManager = new LinearLayoutManager(getContext());
        gridDividerDecoration = new GridDividerDecoration(getContext(), false);
        if (binding == null) {
            final List<Social> socials = getArguments().getParcelableArrayList(POSTS);
            if (socials != null) {
                data.clear();
                data.addAll(socials);
            }
            listAdapter = new SocialAdapter(getContext(), data, new SocialAdapter.OnSocialListener() {
                @Override
                public void openPost(String url) {

                }

                @Override
                public void openImage(String url) {
                    loadPhoto(url);
                }
            });
            binding = FragmentPlayerSocialPostsBinding.inflate(inflater, root, attachToRoot);
            displayList();
        }
        return binding;
    }

    private void loadPhoto(String url) {
        if (getContext() != null) {
            PlayerPhoto playerPhoto = new PlayerPhoto();
            playerPhoto.setUrl(url);
            ImagePreviewActivity.launch(getContext(), playerPhoto, new ArrayList<>());
        }
    }

    public void displayGrid() {
        if (binding != null) {
            binding.rvList.setLayoutManager(null);
            binding.rvList.setLayoutManager(gridLayoutManager);
            binding.rvList.setAdapter(null);
            binding.rvList.addItemDecoration(gridDividerDecoration);
            binding.rvList.setAdapter(gridAdapter);
            gridAdapter.refresh(data);
        }
    }

    public void displayList() {
        if (binding != null) {
            binding.rvList.setLayoutManager(null);
            binding.rvList.removeItemDecoration(gridDividerDecoration);
            binding.rvList.setLayoutManager(linearLayoutManager);
            binding.rvList.setAdapter(null);
            binding.rvList.setAdapter(listAdapter);
            listAdapter.notifyDataSetChanged();
        }
    }

    private class InstagramGridAdapter extends RecyclerView.Adapter<InstagramGridAdapter.PhotoHolder> {

        private List<Social> data = new ArrayList<>();

        void refresh(final @NonNull List<Social> data) {
            this.data.clear();
            this.data.addAll(data);
            this.notifyDataSetChanged();
        }

        @Override
        public InstagramGridAdapter.PhotoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_instagram_photo, parent, false);
            return new PhotoHolder(view);
        }

        @Override
        public void onBindViewHolder(InstagramGridAdapter.PhotoHolder holder, int position) {
            if (position < data.size()) {
                Social social = data.get(position);
                holder.loadImage(social);
            }
        }

        private void openImage(int adapterPosition) {
            if (adapterPosition < data.size()) {
                loadPhoto(data.get(adapterPosition).getImage());
            }
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class PhotoHolder extends RecyclerView.ViewHolder {

            private ImageView iv;
            private ProgressBar pb;

            PhotoHolder(View itemView) {
                super(itemView);
                iv = (ImageView) itemView.findViewById(R.id.image);
                pb = (ProgressBar) itemView.findViewById(R.id.pb);
                iv.setOnClickListener(v -> openImage(getAdapterPosition()));
            }

            public void loadImage(@NonNull Social social) {
                pb.setVisibility(View.VISIBLE);
                Glide.with(itemView.getContext())
                        .load(social.getImage())
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                pb.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                pb.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(iv);
            }
        }
    }
}
