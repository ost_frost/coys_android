package com.fanzine.coys.fragments.team.player;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.FragmentTeamPlayerStatsBinding;
import com.fanzine.coys.databinding.ItemTeamPlayerStatBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.team.Player;
import com.fanzine.coys.models.team.PlayerStat;
import com.fanzine.coys.models.team.TeamSquad;
import com.fanzine.coys.viewmodels.fragments.team.player.StatsViewModel;
import com.fanzine.coys.viewmodels.items.team.ItemPlayerStatsViewModel;

import java.util.List;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

/**
 * Created by mbp on 1/10/18.
 */

public class StatsFragment extends BaseFragment implements StatsViewModel.OnStatsLoadListener {

    private final static String PLAYER = "player";
    private final static String SQUAD = "SQUAD";

    private FragmentTeamPlayerStatsBinding binding;
    private StatsViewModel viewModel;
    private TeamSquad squad;

    public static StatsFragment getInstance(Player player, TeamSquad squad) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PLAYER, player);
        bundle.putSerializable(SQUAD, squad);

        StatsFragment fragment = new StatsFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentTeamPlayerStatsBinding.inflate(inflater, root, attachToRoot);


        viewModel = new StatsViewModel(getContext(), this);

        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);

        Player player = getArguments().getParcelable(PLAYER);
        squad = (TeamSquad) getArguments().getSerializable(SQUAD);

        viewModel.load(player);

        return binding;
    }

    @Override
    public void onSuccess(List<PlayerStat> playerStats) {

        SectionedRecyclerViewAdapter sectionAdapter = new SectionedRecyclerViewAdapter();

        int itemsInRow = 3;

        GridLayoutManager glm = new GridLayoutManager(getContext(), itemsInRow);
        glm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (sectionAdapter.getSectionItemViewType(position)) {
                    case SectionedRecyclerViewAdapter.VIEW_TYPE_FOOTER:
                        return 3;
                    default:
                        return 1;
                }
            }
        });
        binding.rvStats.setLayoutManager(glm);
        binding.rvStats.setAdapter(sectionAdapter);

        for (int i = 0; i < playerStats.size(); i += itemsInRow) {
            int from = i;
            int to = i + itemsInRow;

            sectionAdapter.addSection(new StatsSection(playerStats.subList(from, to)));
        }


        sectionAdapter.notifyDataSetChanged();
    }

    class StatsSection extends StatelessSection {

        List<PlayerStat> statsList;

        public StatsSection(List<PlayerStat> statsList) {
            super(new SectionParameters.Builder(R.layout.item_team_player_stat)
                    .footerResourceId(R.layout.player_stats_line)
                    .build());
            this.statsList = statsList;
        }

        @Override
        public int getContentItemsTotal() {
            return statsList.size();
        }

        @Override
        public RecyclerView.ViewHolder getItemViewHolder(View view) {
            return new ItemHolder(ItemTeamPlayerStatBinding.bind(view));
        }

        @Override
        public void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((ItemHolder) holder).bind(statsList.get(position));
        }

        @Override
        public RecyclerView.ViewHolder getFooterViewHolder(View view) {
            return new FooterHolder(view);
        }

        class FooterHolder extends RecyclerView.ViewHolder {
            public FooterHolder(View itemView) {
                super(itemView);
            }
        }

        class ItemHolder extends RecyclerView.ViewHolder {
            private ItemTeamPlayerStatBinding binding;

            public ItemHolder(ItemTeamPlayerStatBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }

            public void bind(PlayerStat stat) {
                binding.setViewModel(new ItemPlayerStatsViewModel(getContext(), stat, squad));
            }
        }
    }
}
