package com.fanzine.coys.fragments.match;

import com.fanzine.coys.models.Match;

import java.util.Date;

/**
 * Created by mbp on 12/12/17.
 */

public interface OnMatchClickListener {

    void onClick(Match match, String leagueTitle);

    boolean isToday();

    Date getCurrentDate();
}
