package com.fanzine.coys.fragments.news;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.utils.NetworkUtils;

/**
 * Created by mbp on 11/9/17.
 */

public abstract class FeedListAbstractFragment<T> extends BaseFragment
        implements DataListLoadingListener<T> {

    public static final String LEAGUE_ID = "league_id";

    protected int page = 1;

    protected int totalItemCount;
    protected int lastVisibleItem;
    protected boolean isLoading;

    protected int getLeagueId() {
        return getArguments().getInt(LEAGUE_ID);
    }

    public abstract void loadData(int page);

    public void setupScrollListener(RecyclerView rv, LinearLayoutManager layoutManager, SwipeRefreshLayout refreshLayout) {
        refreshLayout.setOnRefreshListener(() -> {
            loadData(1);
            refreshLayout.setEnabled(false);
        });

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (NetworkUtils.isNetworkAvailable(getContext())) {
                        loadData(page);
                        isLoading = true;
                    }
                }
            }
        });
    }
}
