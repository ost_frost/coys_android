package com.fanzine.coys.fragments.mails;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.mails.AttachmentsAdapter;
import com.fanzine.coys.databinding.FragmentNewMailBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.Person;
import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.observables.CreateFileFromUriObservable;
import com.fanzine.coys.subscribers.CreateFileFromUriSubscriber;
import com.fanzine.coys.viewmodels.fragments.mails.NewMailViewModel;
import com.jakewharton.rxbinding.view.RxView;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx_activity_result.RxActivityResult;

import static android.app.Activity.RESULT_OK;

@Deprecated
public class NewMailFragment extends BaseFragment {

    private static final String MAIL = "mail";
    private static final int TAKE_FILES = 42;

    private FragmentNewMailBinding binding;
    private NewMailViewModel viewModel;
    private Mail mail;
    private AttachmentsAdapter adapter;

    public static NewMailFragment newInstance(Mail currentMail) {
        NewMailFragment fragment = new NewMailFragment();
        Bundle args = new Bundle();
        if (currentMail != null) {
            args.putParcelable(MAIL, currentMail);
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mail = getArguments().getParcelable(MAIL);
        }
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentNewMailBinding.inflate(inflater, root, attachToRoot);

        adapter = new AttachmentsAdapter(getContext());

        viewModel = new NewMailViewModel(getContext(), adapter);

        binding.attachments.setAdapter(adapter);

        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);
        setToolbar();

        if (mail != null) {
            viewModel.setMail(mail);

            binding.to.addObject(new Person("", mail.getFrom()));
        }
        binding.to.allowDuplicates(false);
        binding.cc.allowDuplicates(false);
        binding.bcc.allowDuplicates(false);

        RxView.clicks(binding.btbSend).subscribe(aVoid -> {
            viewModel.sendMail(binding.to.getObjects(), binding.cc.getObjects(), binding.bcc.getObjects());
        });

        RxView.clicks(binding.btnFileAttach).subscribe(aVoid -> {
            performAttachFiles();
        });


        binding.attachments.setLayoutManager(new LinearLayoutManager(getContext()) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });



        return binding;
    }

    private void setToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(binding.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(ContextCompat.getColor(getContext(), R.color.inbox_bg_home_button), PorterDuff.Mode.SRC_ATOP);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }

    private void performAttachFiles() {
        Intent takeFile = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        takeFile.addCategory(Intent.CATEGORY_OPENABLE);
        takeFile.setType("*/*");


        RxActivityResult.on(this).startIntent(takeFile).subscribe(result -> {
            Intent data = result.data();

            if (result.resultCode() == RESULT_OK) {
                result.targetUI().loadAttachments(data.getData());
            }
        });
    }

    private void loadAttachments(Uri data) {

        ProgressDialog pd = ProgressDialog.show(
                getContext(), "", "Please wait", true, false);
        pd.setProgress(ProgressDialog.STYLE_SPINNER);

        Observable.defer(new CreateFileFromUriObservable(getContext(), data))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CreateFileFromUriSubscriber(adapter, pd));
    }

}
