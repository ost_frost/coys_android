package com.fanzine.coys.fragments.team;

import android.Manifest;
import android.databinding.ViewDataBinding;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.alexvasilkov.gestures.Settings;
import com.fanzine.coys.R;
import com.fanzine.coys.databinding.FragmentFullscreenPhotoBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.team.Photo;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.viewmodels.fragments.team.FullScreenPhotoViewModel;
import com.fanzine.chat.models.message.FCAttachment;
import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.io.File;

/**
 * Created by razir on 9/1/2016.
 */
public class FullScreenPhotoFragment extends BaseFragment {

    FragmentFullscreenPhotoBinding binding;
    FullScreenPhotoViewModel viewModel;
    private FCAttachment attachment;

    public static FullScreenPhotoFragment newInstance(Photo photo) {
        Bundle args = new Bundle();
        args.putParcelable("photo", photo);
        FullScreenPhotoFragment fragment = new FullScreenPhotoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static FullScreenPhotoFragment newInstance(FCAttachment fcAttachment) {
        Bundle args = new Bundle();
        args.putParcelable("attachment", fcAttachment);
        FullScreenPhotoFragment fragment = new FullScreenPhotoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentFullscreenPhotoBinding.inflate(inflater, root, false);
        viewModel = new FullScreenPhotoViewModel(getContext());
        viewModel.photo.set(getArguments().getParcelable("photo"));
        binding.setViewModel(viewModel);
        setBaseViewModel(viewModel);

        attachment = getArguments().getParcelable("attachment");
        if (attachment != null)
            Glide.with(getContext())
                    .using(new FirebaseImageLoader())
                    .load(attachment.getReference())
                    .into(binding.image);

        getAppCompatActivity().setSupportActionBar(binding.toolbar);
        getAppCompatActivity().getSupportActionBar().setDisplayShowTitleEnabled(false);
        getAppCompatActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getAppCompatActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(ContextCompat.getColor(getContext(), R.color.white), PorterDuff.Mode.SRC_ATOP);
        getAppCompatActivity().getSupportActionBar().setHomeAsUpIndicator(upArrow);

        return binding;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding.gestureView.getController().getSettings()
                .setMaxZoom(4)
                .setRestrictRotation(false)
                .setOverscrollDistance(0f, 0f)
                .setOverzoomFactor(2f)
                .setFillViewport(false)
                .setFitMethod(Settings.Fit.INSIDE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (attachment != null)
            inflater.inflate(R.menu.download, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.download) {
            new RxPermissions(getActivity()).request(Manifest.permission.WRITE_EXTERNAL_STORAGE).subscribe(aBoolean -> {
                if (aBoolean) {
                    File dir = new File(Environment.getExternalStorageDirectory(), "Pictures/" + getString(R.string.app_name));

                    if (!dir.exists())
                        dir.mkdirs();

                    attachment.getReference().getFile(new File(dir, attachment.getUid() + "." + attachment.getType().replace("image/", "")))
                            .addOnSuccessListener(taskSnapshot -> Snackbar.make(binding.getRoot(), R.string.fileDownladed, Snackbar.LENGTH_SHORT).show())
                            .addOnFailureListener(e -> Snackbar.make(binding.getRoot(), R.string.smth_goes_wrong, Snackbar.LENGTH_SHORT).show());
                }
                else
                    DialogUtils.showAlertDialog(getContext(), R.string.writeStorageNeeded);
            });
        }


        return super.onOptionsItemSelected(item);
    }
}
