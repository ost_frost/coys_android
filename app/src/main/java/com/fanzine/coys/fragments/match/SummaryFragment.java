package com.fanzine.coys.fragments.match;


import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.adapters.match.SummaryAdapter;
import com.fanzine.coys.databinding.FragmentSummaryBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.models.summary.Event;
import com.fanzine.coys.viewmodels.fragments.match.SummaryViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SummaryFragment extends BaseFragment implements DataListLoadingListener<Event> {


    private FragmentSummaryBinding binding;
    private SummaryAdapter adapter;
    private SummaryViewModel viewModel;

    public static SummaryFragment newInstance(Match match) {
        SummaryFragment fragment = new SummaryFragment();
        Bundle args = new Bundle();
        args.putParcelable(Match.MATCH, match);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentSummaryBinding.inflate(inflater, root, false);
        viewModel = new SummaryViewModel(getContext(), this, getArguments().getParcelable(Match.MATCH));
        binding.setViewModel(viewModel);
        setBaseViewModel(viewModel);
        binding.rvContent.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true));
        adapter = new SummaryAdapter(getContext());
        binding.rvContent.setAdapter(adapter);
        binding.rvContent.setNestedScrollingEnabled(false);

        viewModel.loadData(0);

        return binding;
    }


    @Override
    public void onLoaded(List<Event> data) {
        adapter.setData(sortEvents(data));
    }

    @Override
    public void onLoaded(Event data) {

    }

    @Override
    public void onError() {

    }

    private List<Event> sortEvents(List<Event> events) {
        List<Event> items = new ArrayList<>(events);
        Collections.sort(items, (event, t1) -> Double.compare(Double.parseDouble(event.getMinute()),Double.parseDouble(t1.getMinute())));

        return items;
    }
}
