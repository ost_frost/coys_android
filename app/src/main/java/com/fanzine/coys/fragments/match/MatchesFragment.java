package com.fanzine.coys.fragments.match;

import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.match.LeagueSection;
import com.fanzine.coys.databinding.FragmentMatchesBinding;
import com.fanzine.coys.fragments.base.BaseTopCalendarFragment;
import com.fanzine.coys.fragments.news.NewsFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.LigueMatches;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.utils.LoadingStates;
import com.fanzine.coys.viewmodels.fragments.match.MatchesFragmentViewModel;

import java.util.Date;
import java.util.List;

import io.github.luizgrp.sectionedrecyclerviewadapter.Section;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;

public class MatchesFragment extends BaseTopCalendarFragment
        implements DataListLoadingListener<LigueMatches>, OnMatchClickListener, LeagueSection.DataSetChangedListener {

    private FragmentMatchesBinding binding;
    private MatchesFragmentViewModel viewModel;

    private Parcelable rvState;

    LinearLayoutManager linearLayoutManager;

    SectionedRecyclerViewAdapter sectionAdapter;


    public static MatchesFragment newInstance() {
        MatchesFragment fragment = new MatchesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentMatchesBinding.inflate(inflater, root, attachToRoot);
        viewModel = new MatchesFragmentViewModel(getAppCompatActivity(), this);
        binding.setViewModel(viewModel);
        setBaseViewModel(viewModel);

        binding.swipe.setColorSchemeResources(R.color.colorAccent);
        binding.swipe.setOnRefreshListener(() -> viewModel.loadData(0));

        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        binding.rvLigueMatches.setLayoutManager(linearLayoutManager);
        binding.rvLigueMatches.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                try {
                    int firstPos = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                    if (firstPos > 0) {
                        binding.swipe.setEnabled(false);
                    } else {
                        binding.swipe.setEnabled(true);
                        if (recyclerView.getScrollState() == 1)
                            if (binding.swipe.isRefreshing())
                                recyclerView.stopScroll();
                    }
                } catch (Exception e) {
                    Log.e(NewsFragment.class.getName(), "Scroll Error : " + e.getLocalizedMessage());
                }
            }
        });

        initTopCalendar(binding.topPanelDates);

        sectionAdapter = new SectionedRecyclerViewAdapter();

        binding.rvLigueMatches.setHasFixedSize(true);
        binding.rvLigueMatches.setNestedScrollingEnabled(false);
        binding.rvLigueMatches.setAdapter(sectionAdapter);

        this.rvState = linearLayoutManager.onSaveInstanceState();

        viewModel.loadData(0);


        return binding;
    }

    @Override
    public Date getCurrentDate() {
        return getSelectedDayInfo().getDate();
    }

    @Override
    public void onTopCalendarItemSelected(View view, int position) {
        if (isToday()) {
            viewModel.startIntervalUpdating();
        } else {
            viewModel.stopReceiveMatches();
        }
        viewModel.loadData(0);
    }

    @Override
    public void onLoaded(List<LigueMatches> data) {
        sectionAdapter.removeAllSections();
        sectionAdapter.notifyDataSetChanged();
        viewModel.setState(LoadingStates.DONE);

        if (binding.swipe.isRefreshing())
            binding.swipe.setRefreshing(false);

        for (int i = 0; i < data.size(); i++) {
            data.get(i).setDate(viewModel.mSelectedDate.get());
            Section section = new LeagueSection(getAppCompatActivity(), data.get(i), this, this);
            sectionAdapter.addSection(section);
        }

        linearLayoutManager.onRestoreInstanceState(rvState);
        linearLayoutManager.scrollToPosition(0);
    }

    @Override
    public void dataSetChanged() {
//        viewModel.update(ligueMatches -> {
//            if (binding.swipe.isRefreshing())
//                binding.swipe.setRefreshing(false);
//
//            sectionAdapter.removeAllSections();
//            for (int i = 0; i < ligueMatches.size(); i++) {
//                ligueMatches.get(i).setDate(viewModel.mSelectedDate.get());
//                Section section = new LeagueSection(getAppCompatActivity(), ligueMatches.get(i), this, this);
//
//                sectionAdapter.addSection(section);
//            }
//            //sectionAdapter.notifyItem
//
//            linearLayoutManager.onRestoreInstanceState(rvState);
//        });
        sectionAdapter.notifyDataSetChanged();
    }

    @Override
    public void sectionChanged(LeagueSection section) {
        sectionAdapter.notifyHeaderChangedInSection(section);
    }

    @Override
    public void onLoaded(LigueMatches data) {
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

//    @Override
//    public void onResume() {
//        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//        getActivity().getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), R.color.toobar_color));
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            Window w = getActivity().getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS, WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//        }
//        super.onResume();
//    }


    @Override
    public void onResume() {
        super.onResume();

        if (isToday()) {
            viewModel.startIntervalUpdating();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        viewModel.stopReceiveMatches();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        viewModel.clearSubscriptions();
    }

    @Override
    public void onError() {
        if (binding.swipe.isRefreshing())
            binding.swipe.setRefreshing(false);
    }

    @Override
    public void onClick(Match match, String leagueTitle) {
        Intent openMatch = new Intent(getContext(), MatchFragment.class);
        openMatch.putExtra(MatchFragment.LEAGUE_TITLE, leagueTitle);
        openMatch.putExtra(Match.MATCH, match);

        startActivity(openMatch);
    }

    @Override
    public boolean isToday() {
        return super.isToday();
    }
}
