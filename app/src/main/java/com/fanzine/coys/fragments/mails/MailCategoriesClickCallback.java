package com.fanzine.coys.fragments.mails;

/**
 * Created by maximdrobonoh on 14.09.17.
 */

@Deprecated
public interface MailCategoriesClickCallback {
    void showToolbar();

    void hideToolbar();

    void deleteFolder(String folder);
}
