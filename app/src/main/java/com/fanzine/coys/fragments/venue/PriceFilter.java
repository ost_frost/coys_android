package com.fanzine.coys.fragments.venue;

/**
 * Created by mbp on 12/25/17.
 */

public interface PriceFilter {

    int PRICE_$ = 1;
    int PRICE_$$ = 2;
    int PRICE_$$$ = 3;
    int PRICE_$$$$ = 4;

    String PRICE_1 = "$";
    String PRICE_2 = "$$";
    String PRICE_3 = "$$$";
    String PRICE_4 = "$$$$";
}
