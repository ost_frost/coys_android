package com.fanzine.coys.fragments.venue;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by mbp on 4/10/18.
 */

public class SearchRequest {

    public String query;
    public LatLng latLng;

    public SearchRequest(String query, LatLng latLng) {
        this.query = query;
        this.latLng = latLng;
    }
}
