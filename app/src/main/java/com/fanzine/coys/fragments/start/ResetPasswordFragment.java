package com.fanzine.coys.fragments.start;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.databinding.FragmentResetPasswordBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.viewmodels.fragments.login.ResetPasswordViewModel;


/**
 * Created by maximdrobonoh on 22.08.17.
 */

public class ResetPasswordFragment extends BaseFragment {

    public static ResetPasswordFragment newInstance() {
        Bundle args = new Bundle();
        ResetPasswordFragment fragment = new ResetPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        FragmentResetPasswordBinding binding = FragmentResetPasswordBinding.inflate(inflater, root, attachToRoot);

        String phone = this.getArguments().getString("phone");
        String phonecode = this.getArguments().getString("phonecode");

        ResetPasswordViewModel viewModel = new ResetPasswordViewModel(getContext(), phonecode, phone);
        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);

        return binding;
    }
}
