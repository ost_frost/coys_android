package com.fanzine.coys.fragments.mails;

import com.fanzine.coys.models.mails.Label;

/**
 * Created by maximdrobonoh on 18.09.17.
 */

public interface OnActionLabelLinster {

    void onLabelDelete(Label label);

    void onLabelClick(Label label);
}
