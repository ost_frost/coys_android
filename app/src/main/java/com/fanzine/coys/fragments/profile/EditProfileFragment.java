package com.fanzine.coys.fragments.profile;

import android.databinding.ViewDataBinding;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.login.CountryAdapter;
import com.fanzine.coys.databinding.DialogRecyclerviewBinding;
import com.fanzine.coys.databinding.FragmentEditProfileBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.login.Country;
import com.fanzine.coys.models.login.CountryData;
import com.fanzine.coys.models.profile.Profile;
import com.fanzine.coys.utils.RecyclerItemClickListener;
import com.fanzine.coys.viewmodels.fragments.profile.EditProfileViewModel;
import com.jakewharton.rxbinding.view.RxView;
import com.miguelbcr.ui.rx_paparazzo.RxPaparazzo;
import com.miguelbcr.ui.rx_paparazzo.entities.size.CustomMaxSize;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Woland on 20.04.2017.
 */

public class EditProfileFragment extends BaseFragment {

    private static final String PROFILE = "profile";
    private EditProfileViewModel viewModel;
    private FragmentEditProfileBinding binding;
    private AlertDialog dialog;

    public static EditProfileFragment newInstance(Profile profile) {
        Bundle args = new Bundle();
        args.putParcelable(PROFILE, profile);
        EditProfileFragment fragment = new EditProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentEditProfileBinding.inflate(inflater, root, attachToRoot);
        viewModel = new EditProfileViewModel(getContext());
        binding.setViewModel(viewModel);
        setBaseViewModel(viewModel);

        Profile profile = getArguments().getParcelable(PROFILE);
        if (profile != null) {
            viewModel.profile.set(profile);
            //viewModel.country.set(new Country(profile.getCountryId(), profile.getPhonecode()));
            viewModel.phone.set(profile.getPhone());

            RxView.clicks(binding.profileImage).subscribe(aVoid -> RxPaparazzo.takeImage(this)
                    .size(new CustomMaxSize(720))
                    .usingGallery()
                    .subscribe(response -> {
                        if (response.resultCode() == RESULT_OK) {
                            response.targetUI().showImage(response.data());
                        }
                    }));
            RxView.clicks(binding.country).subscribe((aVoid) -> openCountryDialog());
            binding.country.setOnFocusChangeListener((v, hasFocus) -> {
                if (hasFocus)
                    openCountryDialog();
            });
        }

        setToolbar(binding.toolbar);


        return binding;
    }

    private void showImage(String url) {
        if (!TextUtils.isEmpty(url)) {
            File file = new File(url);
            Picasso.with(getContext()).load(file).into(binding.avatar);
            viewModel.setImageFile(file);
        }
    }

    private void setToolbar(Toolbar toolbar) {
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(ContextCompat.getColor(getContext(), R.color.inbox_bg_home_button), PorterDuff.Mode.SRC_ATOP);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }

    private void openCountryDialog() {
        List<Country> countries = CountryData.getCategories(getContext());

        DialogRecyclerviewBinding bindingDialog = DialogRecyclerviewBinding.inflate(LayoutInflater.from(getContext()));
        bindingDialog.rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        CountryAdapter adapter = new CountryAdapter(getContext(), countries);
        bindingDialog.rv.setAdapter(adapter);

        bindingDialog.rv.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), (view, position) -> {
            Country country = countries.get(position);

            viewModel.country.set(country);

            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
            }
        }));

        bindingDialog.executePendingBindings();

        dialog = new AlertDialog.Builder(getContext())
                .setView(bindingDialog.getRoot())
                .setNegativeButton(R.string.cancel, null)
                .show();

        dialog.getWindow().setBackgroundDrawableResource(R.color.backgroundDialog);
    }

}
