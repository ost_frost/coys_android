package com.fanzine.coys.fragments.start;

import android.app.ProgressDialog;
import android.databinding.ViewDataBinding;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.FragmentStartBinding;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.viewmodels.base.SocialFragment;
import com.jakewharton.rxbinding.view.RxView;

/**
 * Created by Woland on 21.03.2017.
 */

public class StartFragment extends SocialFragment {

    public static StartFragment newInstance() {
        Bundle args = new Bundle();
        StartFragment fragment = new StartFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        FragmentStartBinding binding = FragmentStartBinding.inflate(inflater, root, attachToRoot);

        binding.forgot.setPaintFlags(binding.forgot.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        RxView.clicks(binding.login).subscribe(aVoid -> FragmentUtils.changeFragment(getActivity(), R.id.content_frame, LoginFragment.newInstance(), true));

        RxView.clicks(binding.signUp).subscribe(aVoid -> FragmentUtils.changeFragment(getActivity(), R.id.content_frame, SelectPlanFragment.newInstance(), true));

        RxView.clicks(binding.forgot).subscribe(aVoid -> forgotPass());

        return binding;
    }

    @Override
    public void facebookLoggedIn(ProgressDialog pd, String token, String id, String name, String country, String gender) {
        checkUserData(pd, token, id, name, country, gender);
    }

    private void forgotPass() {
      ForgotPassFragment forgotPassFragment = new ForgotPassFragment();
      forgotPassFragment.show(getFragmentManager(), ForgotPassFragment.class.getName());
    }
}
