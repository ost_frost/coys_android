package com.fanzine.coys.fragments.venue;


import com.yelp.fusion.client.models.Business;
import com.yelp.fusion.client.models.Term;

import java.util.List;

/**
 * Created by mbp on 12/27/17.
 */

public interface AutocompleteListener {

    void showTerms(List<Term> data);

    void autocompleteError();

    void showVenues(List<Business> businesses);
}
