package com.fanzine.coys.fragments.table;

import android.databinding.ViewDataBinding;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.table.RegionLeaguesAdapter;
import com.fanzine.coys.databinding.FragmentTableBinding;
import com.fanzine.coys.fragments.base.BaseLeaguesBarFragment;
import com.fanzine.coys.fragments.base.BaseTopPanelFragment;
import com.fanzine.coys.models.League;
import com.fanzine.coys.models.table.RegionLeague;
import com.fanzine.coys.viewmodels.base.BaseLeaguesBarViewModel;
import com.fanzine.coys.viewmodels.fragments.table.BaseTableFragmentViewModel;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * Created by Woland on 17.01.2017.
 */

public class BaseTableFragment extends BaseLeaguesBarFragment implements
        BaseLeaguesBarViewModel.LeagueLoadingListener, RegionLeagueClickListener, RegionsLoadingListener {

    private static final int TABLES = 0;
    private static final int FIXTURES = 1;
    private static final int TEAMS = 2;
    private static final int PLAYERS = 3;

    public static final int PRIMEIRA_LIGA = 1204;

    private static final String TEAM_LEAGUE = "TEAM_LEAGUE";

    private FragmentTableBinding binding;
    private BaseTableFragmentViewModel viewModel;

    private List<RegionLeague> regionLeagues;

    private int teamLeagueId = -1;

    public static BaseTableFragment newInstance() {
        Bundle args = new Bundle();
        BaseTableFragment fragment = new BaseTableFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static BaseTableFragment newInstance(int leagueId) {
        Bundle args = new Bundle();
        args.putInt(TEAM_LEAGUE, leagueId);

        BaseTableFragment fragment = new BaseTableFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        if (binding == null) {
            binding = FragmentTableBinding.inflate(inflater, root, attachToRoot);

            viewModel = new BaseTableFragmentViewModel(getContext(), getChildFragmentManager(), this, this);

            binding.setViewModel(viewModel);
            setBaseViewModel(viewModel);

            teamLeagueId = getArguments().getInt(TEAM_LEAGUE);

            viewModel.loadRegions();

            RxView.clicks(binding.ivRegionsButton).subscribe(aVoid -> {
                if (binding.regions.getVisibility() == View.GONE) {
                    binding.tableTabs.setVisibility(View.GONE);
                    binding.regions.setVisibility(View.VISIBLE);
                    binding.ivRegionsButton.setImageResource(R.drawable.table_dropdown);
                } else {
                    binding.tableTabs.setVisibility(View.VISIBLE);
                    binding.regions.setVisibility(View.GONE);
                    binding.ivRegionsButton.setImageResource(R.drawable.table_dropdown_off);
                }
            });

            binding.regions.setHasFixedSize(true);
            binding.regions.setLayoutManager(
                    new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        }

        binding.tableTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == TABLES) {
                    viewModel.openTables();
                } else if (tab.getPosition() == FIXTURES) {
                    viewModel.openFixtures();
                } else if (tab.getPosition() == TEAMS) {
                    viewModel.openTeams();
                } else if (tab.getPosition() == PLAYERS) {
                    viewModel.openPlayers();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return binding;
    }

    @Override
    public void onTopPanelItemSelected(View view, int position) {
        viewModel.restore();
    }

    @Override
    public void onLeagueLoaded(List<League> list) {
    }

    @Override
    public void onLeagueRegionsLoaded(List<RegionLeague> data) {
        RegionLeaguesAdapter regionLeaguesAdapter = new RegionLeaguesAdapter(getContext(), this);

        regionLeagues = data;


        if (data.size() > 0) {
            Collections.sort(data, (regionLeague, t1) -> regionLeague.getOrder() - t1.getOrder());

            List<League> sliderLeagues = new ArrayList<>();

            if (teamLeagueId == PRIMEIRA_LIGA) {
                sliderLeagues.addAll(getSliderSortedLeagues(data.get(0).getLeagues()));
            } else {
                RegionLeague regionLeague = getRegionByLeagueId(teamLeagueId);
                sliderLeagues.addAll(getSliderSortedLeagues(regionLeague != null ? regionLeague.getLeagues() : new ArrayList<>()));
            }

            BaseTopPanelFragment.setLeagueId(teamLeagueId);
            initTopPanel(sliderLeagues, binding.topPanel, 4);


            viewModel.openTables();

            regionLeaguesAdapter.setData(data);
            binding.regions.setAdapter(regionLeaguesAdapter);
        }
    }

    @Override
    public void onLeagueClicked(RegionLeague regionLeague, League league) {

        List<League> sliderLeauges = getSliderSortedLeagues(regionLeague.getLeagues());
        getLeaguesAdapter().clear();
        getLeaguesAdapter().addAll(sliderLeauges);

        int position = getLeaguesAdapter().getItemPosition(league);
        selected = league.getLeagueId();
        leagues.clear();
        leagues.addAll(sliderLeauges);

        binding.topPanel.smoothScrollToPosition(position);

        viewModel.openTables();

        binding.tableTabs.setVisibility(View.VISIBLE);
        binding.regions.setVisibility(View.GONE);

        binding.tableTabs.getTabAt(TABLES).select();
    }

    class LeagueSortComparator implements Comparator<League> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        public int compare(League l1, League l2) {
            return ((Integer) l1.getSort()).compareTo(l2.getSort());
        }
    }

    private RegionLeague getRegionByLeagueId(int leagueId) {
        for (RegionLeague region : regionLeagues) {
            for (League regionLeague : region.getLeagues()) {
                if (regionLeague.getLeagueId() == leagueId) {
                    return region;
                }
            }
        }
        return null;
    }


    private List<League> getSliderSortedLeagues(List<League> leagues) {
        List<League> sliderLeagues = new ArrayList<>(leagues);
        Collections.sort(sliderLeagues, (league, t1) -> league.getSort() - t1.getSort());

        return sliderLeagues;
    }
}
