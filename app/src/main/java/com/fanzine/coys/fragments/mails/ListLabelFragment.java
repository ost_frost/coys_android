package com.fanzine.coys.fragments.mails;

import android.databinding.ViewDataBinding;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.mails.LabelsAdapter;

import com.fanzine.coys.databinding.FragmentListLabelsBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.mails.Label;
import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.viewmodels.fragments.mails.ListLabelsViewModel;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximdrobonoh on 14.09.17.
 */

public class ListLabelFragment extends BaseFragment implements
        DataListLoadingListener<Label>, OnActionLabelLinster {

    private FragmentListLabelsBinding binding;
    private ListLabelsViewModel viewModel;
    private LabelsAdapter adapter;

    private Mail mail;

    private final static String LABLES = "lables_list";
    private final static String MAIL = "mail";


    public static ListLabelFragment newInstance(Mail mail) {
        Bundle args = new Bundle();
        args.putParcelable(MAIL, mail);

        ListLabelFragment fragment = new ListLabelFragment();
        fragment.setArguments(args);

        return fragment;
    }

    public static ListLabelFragment newInstance(List<Label> labels, Mail mail) {
        Bundle args = new Bundle();

        args.putParcelableArrayList(LABLES, (ArrayList<Label>) labels);
        args.putParcelable(MAIL, mail);
        ListLabelFragment fragment = new ListLabelFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean
            attachToRoot) {

        this.binding = FragmentListLabelsBinding.inflate(inflater, root, attachToRoot);
        this.viewModel = new ListLabelsViewModel(getContext(), this);
        this.adapter = new LabelsAdapter(getContext());

        this.binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);
        setToolbar();

        binding.rvListLabels.setLayoutManager(
                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, attachToRoot));
        binding.rvListLabels.setAdapter(adapter);

        if (getArguments().getParcelableArrayList(LABLES) != null) {
            List<Label> labels = getArguments().getParcelableArrayList(LABLES);
            this.viewModel.loadFromList(labels);
        } else {
            this.viewModel.loadData(0);
        }

        this.mail = (Mail)getArguments().get(MAIL);

        RxView.clicks(binding.createLabel).subscribe(aVoid -> {
            viewModel.loadColors(mail);
        });

        return binding;
    }

    @Override
    public void onLoaded(List<Label> data) {
        adapter = new LabelsAdapter(getContext(), data, this);
        binding.rvListLabels.setAdapter(adapter);
    }

    @Override
    public void onLoaded(Label data) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onLabelDelete(Label label) {
        this.viewModel.deleteLabel(label);
    }

    @Override
    public void onLabelClick(Label label) {
        this.viewModel.addLabelToMail(label, mail);
    }

    private void setToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(binding.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(ContextCompat.getColor(getContext(), R.color.inbox_bg_home_button), PorterDuff.Mode.SRC_ATOP);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }
}
