package com.fanzine.coys.fragments.match;

import android.animation.ValueAnimator;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.match.RecentGamesAdapter;
import com.fanzine.coys.adapters.match.RecentGamesIdicatorAdapter;
import com.fanzine.coys.databinding.FragmentMatchAnalysisBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.interfaces.PredictionResult;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.models.analysis.Analysis;
import com.fanzine.coys.models.analysis.Prediction;
import com.fanzine.coys.models.analysis.RecentGames;
import com.fanzine.coys.viewmodels.fragments.match.AnalysisFragmentViewModel;
import com.google.firebase.crash.FirebaseCrash;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.fanzine.coys.models.Match.MATCH;

/**
 * Created by maximdrobonoh on 06.10.17.
 */

public class AnalysisFragment extends BaseFragment
        implements DataListLoadingListener<Analysis>, PredictionResult {

    private AnalysisFragmentViewModel viewModel;
    private FragmentMatchAnalysisBinding binding;
    private Match match;

    public static AnalysisFragment newInstance(Match match) {
        AnalysisFragment fragment = new AnalysisFragment();
        Bundle args = new Bundle();
        args.putParcelable(MATCH, match);
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        match = getArguments().getParcelable(MATCH);

        binding = FragmentMatchAnalysisBinding.inflate(inflater, root, false);
        viewModel = new AnalysisFragmentViewModel(getContext(), this, match, this);

        binding.setViewModel(viewModel);
        setBaseViewModel(viewModel);

        initRecentForm();

        viewModel.loadData(0);

        return binding;
    }

    @Override
    public void onLoaded(List<Analysis> data) {
    }


    @Override
    public void onLoaded(Analysis analysis) {
        showPrediction(analysis);
        showRecentGames(analysis);
    }

    @Override
    public void onError() {

    }

    @Override
    public void showDiagram(Prediction prediction) {
        int height = getResources().getDimensionPixelSize(R.dimen.analysis_vote_defaultHeight);
        int minHeight = getResources().getDimensionPixelSize(R.dimen.analysis_vote_minHeight);

        int homeHeight = Math.round(height * prediction.getVoteHome() / prediction.getAllVotes());
        int drawHeight = Math.round(height * prediction.getVoteDraw() / prediction.getAllVotes());
        int awayHeight = Math.round(height * prediction.getVoteAway() / prediction.getAllVotes());

        if (homeHeight < minHeight) {
            homeHeight = minHeight;
        }
        if (drawHeight < minHeight) {
            drawHeight = minHeight;
        }
        if (awayHeight < minHeight) {
            awayHeight = minHeight;
        }

        animateChartBar(binding.homeFilling, homeHeight);
        animateChartBar(binding.drawFilling, drawHeight);
        animateChartBar(binding.guestFilling, awayHeight);
    }

    @Deprecated
    public void showVotedLabel(Prediction prediction) {

        if (prediction.getUserVote().equals(AnalysisFragmentViewModel.DiagramTitle.HOME)) {
            binding.votedHome.setVisibility(View.VISIBLE);

        } else if (prediction.getUserVote().equals(AnalysisFragmentViewModel.DiagramTitle.DRAW)) {
            binding.votedDraw.setVisibility(View.VISIBLE);

        } else if (prediction.getUserVote().equals(AnalysisFragmentViewModel.DiagramTitle.AWAY)) {
            binding.votedAway.setVisibility(View.VISIBLE);
        }
    }


    private void enableVoteBar() {
        RxView.clicks(binding.diagramHome).subscribe(aVoid -> {
            viewModel.castVote(AnalysisFragmentViewModel.DiagramTitle.HOME);
        });
        RxView.clicks(binding.diagramDraw).subscribe(aVoid -> {
            viewModel.castVote(AnalysisFragmentViewModel.DiagramTitle.DRAW);
        });
        RxView.clicks(binding.diagramAway).subscribe(aVoid -> {
            viewModel.castVote(AnalysisFragmentViewModel.DiagramTitle.AWAY);
        });
    }

    private void initRecentForm() {


        binding.rvLocalTeamIndicators.setLayoutManager(
                getRecyclerLayoutManager(LinearLayoutManager.HORIZONTAL));
        binding.rvVisitorTeamIndicators.setLayoutManager(
                getRecyclerLayoutManager(LinearLayoutManager.HORIZONTAL));
        binding.localTeamList.setLayoutManager(
                getRecyclerLayoutManager(LinearLayoutManager.VERTICAL));
        binding.visitTeamList.setLayoutManager(
                getRecyclerLayoutManager(LinearLayoutManager.VERTICAL));
    }

    private void animateChartBar(RelativeLayout item, int endHeight) {
        int duration = 1200;
        int startHeight = getResources().getDimensionPixelSize(R.dimen.analysis_vote_defaultHeight);

        ValueAnimator animator = ValueAnimator.ofFloat(startHeight, endHeight);
        animator.setDuration(duration);

        animator.addUpdateListener(valueAnimator -> {
            float animatedValue = (float) valueAnimator.getAnimatedValue();

            ViewGroup.LayoutParams voteHomeLayoutParams = item.getLayoutParams();
            voteHomeLayoutParams.height = Math.round(animatedValue);
            item.setLayoutParams(voteHomeLayoutParams);
        });
        animator.start();
    }

    private LinearLayoutManager getRecyclerLayoutManager(int orientation) {
        return new LinearLayoutManager(getContext(),
                orientation, false);
    }

    //todo:: refactor duplicated methods showLocalTeamGames, showGuestTeamGames
    private void showRecentGames(Analysis analysis) {
        List<RecentGames.Item> localTeam = analysis.getRecentGames().getLocalTeam();
        List<RecentGames.Item> guestTeam = analysis.getRecentGames().getVisitorTeam();

        if (isRecentGamesValid(localTeam)) {
            showLocalTeamGames(localTeam);
        }

        if (isRecentGamesValid(guestTeam)) {
            showGuestTeamGames(guestTeam);
        }
    }

    private void showLocalTeamGames(List<RecentGames.Item> localTeam) {
        try {
            RecentGamesAdapter localTeamAdapter = new RecentGamesAdapter(
                    getContext(), localTeam, true, match.getHomeTeam().getName());

            binding.localTeamList.setAdapter(localTeamAdapter);

            List<RecentGames.Item> localTeamIcons = new ArrayList<>(localTeam);
            Collections.reverse(localTeamIcons);

            binding.rvLocalTeamIndicators.setAdapter(
                    new RecentGamesIdicatorAdapter(getContext(), localTeamIcons));

        } catch (Exception e) {
            FirebaseCrash.log(e.getMessage());
            FirebaseCrash.report(e);
        }
    }

    private void showGuestTeamGames(List<RecentGames.Item> guestTeam) {
        try {
            RecentGamesAdapter visitorTeamAdapter = new RecentGamesAdapter(
                    getContext(), guestTeam, false, match.getGuestTeam().getName());

            binding.visitTeamList.setAdapter(visitorTeamAdapter);

            List<RecentGames.Item> guestTeamIcons = new ArrayList<>(guestTeam);
            Collections.reverse(guestTeamIcons);

            binding.rvVisitorTeamIndicators.setAdapter(
                    new RecentGamesIdicatorAdapter(getContext(), guestTeamIcons));

        } catch (Exception e) {
            FirebaseCrash.log(e.getMessage());
            FirebaseCrash.report(e);
        }
    }

    private boolean isRecentGamesValid(List<RecentGames.Item> items) {
        for (RecentGames.Item item : items) {
            if (item.getHome() == null || item.getAway() == null)
                return false;
        }
        return true;
    }


    private void showPrediction(Analysis analysis) {
        try {
            if (analysis.getPrediction().getUserVote() == null && match.getState() == Match.NEXT
                    || match.getState() == Match.LIVE) {
                enableVoteBar();
            } else if (analysis.getPrediction().getUserVote() == null && match.getState() == Match.LAST) {
                showDiagram(analysis.getPrediction());
                viewModel.setData(analysis.getPrediction());
            } else {
                viewModel.setIsUserVoted(true);
                showDiagram(analysis.getPrediction());

                viewModel.setData(analysis.getPrediction());
                //showVotedLabel(analysis.getPrediction());
            }
        } catch (Exception e) {
            FirebaseCrash.log(e.getMessage());
            FirebaseCrash.report(e);
        }
    }
}
