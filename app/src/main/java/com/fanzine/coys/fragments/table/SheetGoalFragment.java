package com.fanzine.coys.fragments.table;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.table.GoalsSheetAdapter;
import com.fanzine.coys.api.response.PlayerStatsData;
import com.fanzine.coys.databinding.FragmentSheetGoalsBinding;
import com.fanzine.coys.fragments.base.BaseTopPanelFragment;
import com.fanzine.coys.fragments.news.NewsFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.League;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.utils.RecyclerItemClickListener;
import com.fanzine.coys.viewmodels.base.BaseTopPanelViewModel;
import com.fanzine.coys.viewmodels.fragments.table.SheetGoalViewModel;

import java.util.List;

/**
 * Created by Woland on 17.01.2017.
 */

public class SheetGoalFragment extends BaseTopPanelFragment implements DataListLoadingListener<PlayerStatsData>,BaseTopPanelViewModel.LeagueLoadingListener {

    private FragmentSheetGoalsBinding binding;

    public static SheetGoalFragment newInstance() {
        Bundle args = new Bundle();
        SheetGoalFragment fragment = new SheetGoalFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        if (binding == null) {
            binding = FragmentSheetGoalsBinding.inflate(inflater, root, attachToRoot);

            SheetGoalViewModel viewModel = new SheetGoalViewModel(getContext(), this, this);
            binding.setViewModel(viewModel);

            setBaseViewModel(viewModel);

            binding.swipe.setColorSchemeResources(R.color.colorAccent);
            binding.swipe.setOnRefreshListener(() -> viewModel.startDataLoading(0));

            initTopPanel(viewModel, binding.topPanel, true);

            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            binding.rv.setLayoutManager(linearLayoutManager);
            binding.rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    try {
                        int firstPos = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                        if (firstPos > 0) {
                            binding.swipe.setEnabled(false);
                        } else {
                            binding.swipe.setEnabled(true);
                            if(recyclerView.getScrollState() == 1)
                                if(binding.swipe.isRefreshing())
                                    recyclerView.stopScroll();
                        }

                    }catch(Exception e) {
                        Log.e(NewsFragment.class.getName(), "Scroll Error : "+e.getLocalizedMessage());
                    }
                }
            });

            GoalsSheetAdapter adapter = new GoalsSheetAdapter(getContext());
            binding.rv.setAdapter(adapter);
            binding.rv.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), adapter));
        }

        return binding;
    }

    @Override
    public void onTopPanelItemSelected(View view, int position) {
        FragmentUtils.changeFragment(getActivity(), R.id.content_frame, newInstance(), false);
    }

    @Override
    public void onLoaded(List<PlayerStatsData> data) {
    }

    @Override
    public void onLoaded(PlayerStatsData data) {
        if (binding.swipe.isRefreshing())
            binding.swipe.setRefreshing(false);

        final GoalsSheetAdapter adapter = ((GoalsSheetAdapter) binding.rv.getAdapter());
        if (binding.goals.isChecked())
            adapter.setData(data.getGoals());
        else if (binding.assists.isChecked())
            adapter.setData(data.getAssists());
        else if (binding.reds.isChecked())
            adapter.setData(data.getReds());
        else if (binding.yellows.isChecked())
            adapter.setData(data.getReds());

        binding.goals.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                adapter.setData(data.getGoals());
        });

        binding.assists.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                adapter.setData(data.getAssists());
        });

        binding.reds.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                adapter.setData(data.getReds());
        });

        binding.yellows.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                adapter.setData(data.getYellows());
        });
    }

    @Override
    public void onError() {
        if (binding.swipe.isRefreshing())
            binding.swipe.setRefreshing(false);
    }

    @Override
    public void onLeagueLoaded(List<League> list) {
        initTopPanel(list, binding.topPanel, 14);
    }
}
