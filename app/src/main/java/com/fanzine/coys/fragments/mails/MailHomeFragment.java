package com.fanzine.coys.fragments.mails;

import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.widgets.RecyclerViewLineItemDecoration;
import com.fanzine.coys.R;
import com.fanzine.mail.MailComposeNewActivity;
import com.fanzine.mail.MailNewFolderActivity;
import com.fanzine.coys.adapters.mails.MailFoldersAdapter;
import com.fanzine.coys.databinding.FragmentMailHomeBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.mails.Folder;
import com.fanzine.coys.viewmodels.fragments.mails.MailHomeViewModel;
import com.fanzine.mail.view.activity.folder.ListEmailActivity;
import com.google.common.collect.Ordering;
import com.jakewharton.rxbinding.view.RxView;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Evgenij Krasilnikov on 09-Feb-18
 */

public class MailHomeFragment extends BaseFragment
        implements DataListLoadingListener<Folder>, MailFoldersAdapter.OnFolderClickListener {

    private final static Ordering<Folder> ORDERING = Ordering.natural().onResultOf(Folder::getOrder);

    private interface StaticFolders {
        String SENT = "Sent";
        String DRAFTS = "Drafts";
        String SPAM = "Spam";
        String INBOX = "Inbox";
        String FLAGGED = "Flagged";
        String TRASH = "Trash";
        String STARRED = "StarredPresenter";
    }

    private Map<String, Integer> folderIconMap = new HashMap<>();

    private FragmentMailHomeBinding binding;
    private MailHomeViewModel viewModel;

    public static MailHomeFragment newInstance() {
        return new MailHomeFragment();
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentMailHomeBinding.inflate(inflater, root, attachToRoot);
        viewModel = new MailHomeViewModel(getContext(), this);
        setBaseViewModel(viewModel);
        binding.rvFolder.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, attachToRoot));
        RxView.clicks(binding.vwMahoCreateNewFolder).subscribe(aVoid -> MailNewFolderActivity.start(getContext()));
        RxView.clicks(binding.fabComposeMail).subscribe(aVoid ->{

            Intent intent = new Intent(getActivity(), MailComposeNewActivity.class);
            intent.putExtra(MailComposeNewActivity.MODE, MailComposeNewActivity.MODE_NEW_MAIL);
            startActivity(intent);
        });
        viewModel.loadData(0);

        folderIconMap.put(StaticFolders.SENT, R.drawable.mail_outbox);
        folderIconMap.put(StaticFolders.DRAFTS, R.drawable.mail_drafts);
        folderIconMap.put(StaticFolders.SPAM, R.drawable.mail_spam);
        folderIconMap.put(StaticFolders.FLAGGED, R.drawable.mail_star);
        folderIconMap.put(StaticFolders.INBOX, R.drawable.mail_inbox);
        folderIconMap.put(StaticFolders.TRASH, R.drawable.ic_mail_trash_red);

        return binding;
    }

    @Override
    public void onLoaded(List<Folder> data) {
        Log.i("FOLDER", "folder list.size()="+data.size());

        Collections.sort(data, ORDERING);

        for (Folder folder : data) {
            Log.i("FOLDER", "folder name="+folder.getDisplayName());

            if (folderIconMap.containsKey(folder.getName()))
                folder.setDrawableIconId(folderIconMap.get(folder.getName()));
            else
                folder.setDrawableIconId(R.drawable.mail_work);

            if ( folder.getName().equals(StaticFolders.FLAGGED)) {
                folder.setDisplayName(StaticFolders.STARRED);
            } else {
                folder.setDisplayName(folder.getName());
            }
        }

        MailFoldersAdapter adapter = new MailFoldersAdapter(getContext(), data, this);

        //Divide default and custom folders
        binding.rvFolder.addItemDecoration(new RecyclerViewLineItemDecoration(getContext(), 5));
        binding.rvFolder.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (viewModel != null)
            viewModel.loadData(0);
    }

    @Override
    public void showMailsList(Folder folder) {
        Intent listMailsIntent = new Intent(getContext(), ListEmailActivity.class);
        listMailsIntent.putExtra(ListEmailActivity.FOLDER, folder.getName());

        startActivity(listMailsIntent);
    }

    @Override
    public void onLoaded(Folder data) {

    }

    @Override
    public void onError() {

    }
}
