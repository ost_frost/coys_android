package com.fanzine.coys.fragments.team.player;

import android.databinding.ViewDataBinding;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.FragmentTeamPlayerSocialBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.interfaces.DataListLoadingListener;
import com.fanzine.coys.models.Social;
import com.fanzine.coys.models.team.Player;
import com.fanzine.coys.viewmodels.fragments.team.player.SocialViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mbp on 1/10/18.
 */

public class SocialFragment extends BaseFragment implements DataListLoadingListener<Social> {

    private static final String PLAYER = "player";

    enum SocialType {
        TWITTER("Twitter"), INSTAGRAM("Instagram");

        private final String title;

        SocialType(String title) {
            this.title = title;
        }

        public String getValue() {
            return title;
        }
    }

    private static final int TAB_TWITTER = 0;
    private static final int TAB_INSTAGRAM = 1;

    private FragmentTeamPlayerSocialBinding binding;
    private SocialViewModel viewModel;

    private List<Social> mSocialNews;

    private SocialTabAdapter tabAdapter;


    public static SocialFragment getInstance(Player player) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PLAYER, player);

        SocialFragment fragment = new SocialFragment();
        fragment.setArguments(bundle);

        return fragment;
    }


    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        if (binding == null || viewModel == null) {

            Player player = getArguments().getParcelable(PLAYER);

            viewModel = new SocialViewModel(getContext(), this, player);
            binding = FragmentTeamPlayerSocialBinding.inflate(inflater, root, attachToRoot);

            binding.setViewModel(viewModel);
            setBaseViewModel(viewModel);

            viewModel.loadData(0);

            binding.btnTwitter.setOnClickListener(view -> openTwitter());
            binding.btnInstagram.setOnClickListener(view -> openInstagram());

            tabAdapter = new SocialTabAdapter(getChildFragmentManager());

            binding.list.setOnClickListener(v -> {
                applyDrawableColor(binding.list.getDrawable(), true);
                applyDrawableColor(binding.grid.getDrawable(), false);
                setInstagramFragmentDisplayList(true);
            });
            binding.grid.setOnClickListener(v -> {
                applyDrawableColor(binding.list.getDrawable(), false);
                applyDrawableColor(binding.grid.getDrawable(), true);
                setInstagramFragmentDisplayList(false);
            });

            binding.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    switch (position) {
                        case TAB_INSTAGRAM:
                            binding.btnInstagram.setChecked(true);
                            break;
                        case TAB_TWITTER:
                            binding.btnTwitter.setChecked(true);
                            break;
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            binding.viewpager.setAdapter(tabAdapter);
        }
        return binding;
    }

    private void applyDrawableColor(@Nullable Drawable drawable, boolean isSelected) {
        if (drawable != null && getContext() != null) {
            drawable.setColorFilter(ContextCompat.getColor(getContext(), isSelected ? R.color.colorAccent : R.color.black),
                    PorterDuff.Mode.SRC_IN);
        }
    }

    @Override
    public void onLoaded(List<Social> data) {
        mSocialNews = data;
        tabAdapter.addFragment(
                TwitterFragment.getInstance(filterPosts(SocialType.TWITTER)),
                InstagramFragment.getInstance(filterPosts(SocialType.INSTAGRAM))
        );
        if (binding.viewpager != null) {
            binding.viewpager.setCurrentItem(TAB_TWITTER);
        }
    }

    @Override
    public void onLoaded(Social data) {

    }

    @Override
    public void onError() {

    }

    private ArrayList<Social> filterPosts(SocialType socialType) {
        ArrayList<Social> posts = new ArrayList<>();

        for (Social social : mSocialNews) {
            if (social.getSocialNetwork().equals(socialType.getValue())) {
                posts.add(social);
            }
        }
        return posts;
    }

    private void openInstagram() {
        setInstagramListAlignBtnsVisible(true);
        if (binding != null) {
            binding.viewpager.setCurrentItem(TAB_INSTAGRAM);
        }
    }

    private void openTwitter() {
        setInstagramListAlignBtnsVisible(false);
        if (binding != null) {
            binding.viewpager.setCurrentItem(TAB_TWITTER);
        }
    }

    private void setInstagramListAlignBtnsVisible(boolean visible) {
        if (binding != null) {
            int v = visible ? View.VISIBLE : View.GONE;
            binding.grid.setVisibility(v);
            binding.list.setVisibility(v);
        }
    }

    private void setInstagramFragmentDisplayList(boolean isList) {
        Fragment fragment = tabAdapter.getItem(TAB_INSTAGRAM);
        if (fragment instanceof InstagramFragment) {
            if (isList) {
                ((InstagramFragment) fragment).displayList();
            } else {
                ((InstagramFragment) fragment).displayGrid();
            }
        }
    }

    private class SocialTabAdapter extends FragmentPagerAdapter {

        void addFragment(Fragment... frg) {
            fragments.addAll(Arrays.asList(frg));
            notifyDataSetChanged();
        }

        public List<Fragment> fragments = new ArrayList<>();

        SocialTabAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
}
