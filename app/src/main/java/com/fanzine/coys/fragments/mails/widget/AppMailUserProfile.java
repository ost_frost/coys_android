package com.fanzine.coys.fragments.mails.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.fanzine.coys.databinding.WidgetMailUserProfileBinding;
import com.fanzine.coys.interfaces.OnProfileLoader;
import com.fanzine.coys.models.profile.Profile;
import com.fanzine.coys.viewmodels.fragments.profile.ProfileViewModel;

/**
 * Created by Evgenij Krasilnikov on 09-Feb-18.
 */

public class AppMailUserProfile extends FrameLayout implements OnProfileLoader {

    private WidgetMailUserProfileBinding binding;

    public AppMailUserProfile(@NonNull Context context) {
        super(context);
        initialize(context);
    }

    public AppMailUserProfile(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    public AppMailUserProfile(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    private void initialize(Context context) {
        binding = WidgetMailUserProfileBinding.inflate(LayoutInflater.from(context), this, false);
        ProfileViewModel viewModel = new ProfileViewModel(context);
        viewModel.getProfile(this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (isInEditMode() || binding == null) return;
        addView(binding.getRoot());
    }

    @Override
    public void onProfileLoaded(Profile profile) {
        binding.setProfile(profile);
    }
}
