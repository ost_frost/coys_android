package com.fanzine.coys.fragments.venue;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.ViewDataBinding;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.venues.VenuesAdapter;
import com.fanzine.coys.databinding.FragmentListVenuesBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.interfaces.OnBusinessReceiver;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.utils.GeoHelper;
import com.fanzine.coys.viewmodels.fragments.venue.ListVenuesViewModel;

import com.google.android.gms.maps.model.LatLng;
import com.yelp.fusion.client.models.Business;
import com.yelp.fusion.client.models.Review;

import java.util.ArrayList;
import java.util.List;

public class ListVenues extends BaseFragment
        implements
        OnBusinessReceiver,
        VenuesAdapter.OnItemClickListener,
        ListVenuesViewModel.OnVenueLoadingListener {

    private FragmentListVenuesBinding binding;

    private static final String DATA = "data";

    private ListVenuesViewModel viewModel;
    private List<Business> businesses;
    private VenuesAdapter adapter;

    private LatLng currentLocation;

    public static String phone;

    private OnFragmentChangeListener onFragmentChangeListener;


    public static ListVenues newInstance() {
        ListVenues fragment = new ListVenues();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {

        binding = FragmentListVenuesBinding.inflate(inflater, root, false);
        viewModel = new ListVenuesViewModel(getContext(), getFragmentManager(), this);
        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);


        binding.rvVenues.setLayoutManager(new LinearLayoutManager(getContext()));

        return binding;
    }

    @Override
    public void showVenue(Business business) {
        onFragmentChangeListener.onVenueDetailOpen();
        viewModel.loadVenues(business.getId());
    }


    @Override
    public void callPhone(String phone) {
        if (phone.length() == 0) {
            Toast.makeText(getContext(), "Phone does not exist", Toast.LENGTH_LONG).show();
        } else {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + phone));


            new AlertDialog.Builder(getContext())
                    .setMessage(phone)
                    .setPositiveButton("Call", (dialog, which) -> {
                        if (checkCallPermissions()) {
                            getContext().startActivity(callIntent);
                        } else {
                            ListVenues.phone = phone;

                            ActivityCompat.requestPermissions(getActivity(),
                                    new String[]{Manifest.permission.CALL_PHONE}, VenueFragment.CALL_PHONE_PERMISSION);
                        }

                    }).setNegativeButton("Cancel", null).show();
        }
    }

    @Override
    public void openWebsite(Uri uri) {
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    @Override
    public void websiteNotFound() {
        Toast.makeText(getContext(), R.string.venues_website_is_not_found, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void findWebsiteByGoogleApi(Business business) {

        List<String> address = new ArrayList<>();
        address.add(business.getName());
        address.add(business.getLocation().getAddress1());

        if (business.getLocation().getCity() != null) {
            address.add(business.getLocation().getCity());
        }

        StringBuilder addressBuilder = new StringBuilder();

        for (String item : address)
            addressBuilder.append(item).append(", ");

        viewModel.requestGeoCode(addressBuilder.substring(0, addressBuilder.length()));
    }

    @Override
    public void onVenueDetailLoaded(ArrayList<Review> reviews, Business business) {
        FragmentUtils.changeFragment(getFragmentManager(), R.id.container,
                DetailsVenuesFragment.newInstance(reviews, business, onFragmentChangeListener), false);
    }

    //todo:: refactor
    //todo:: duplicated code with TravelFragment
    @Override
    public void showDirection(Business business) {
        //https://www.google.com/maps/dir/?api=1&origin=Space+Needle+Seattle+WA&destination=Pike+Place+Market+Seattle+WA&travelmode=bicycling

        String desLocation = "&destination=";
        String orignLocation = "&origin=";

        String from = GeoHelper.decodeAddress(getContext(), currentLocation);
        StringBuilder toBuilder = new StringBuilder();

        List<String> buildAddress = new ArrayList<>();
        buildAddress.add(business.getName());
        buildAddress.add(business.getLocation().getAddress1());
        buildAddress.add(business.getLocation().getCity());
        buildAddress.add(business.getLocation().getZipCode());
        buildAddress.add(business.getLocation().getCountry());

        for ( String item : buildAddress) {
            if ( item != null && item.length() > 0) {
                toBuilder.append(item).append(",");
            }
        }


        if (from != null) {
            from = from.replace(' ', '+');
            orignLocation += from;
        }

        String to = toBuilder.substring(0, toBuilder.length()-1);

        to =  to.replace(' ', '+');
        desLocation += to;

        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("https://www.google.com/maps/dir/?api=1" + orignLocation
                        + desLocation + "&travelmode=bicycling"));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                & Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.setClassName("com.google.android.apps.maps",
                "com.google.android.maps.MapsActivity");
        startActivity(intent);
    }

    public void init(List<Business> businesses, LatLng currentLocation, OnFragmentChangeListener onFragmentChangeListener) {
        this.businesses = businesses;
        this.onFragmentChangeListener = onFragmentChangeListener;
        this.currentLocation = currentLocation;

        adapter = new VenuesAdapter(getContext(), businesses, this);
        binding.rvVenues.setAdapter(adapter);

        adapter.notifyDataSetChanged();

        if (businesses.size() == 0) {
            viewModel.setIsNotDataStatusVisible(true);
        } else {
            viewModel.setIsNotDataStatusVisible(false);
        }

    }

    private boolean checkCallPermissions() {
        return ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED;
    }
}
