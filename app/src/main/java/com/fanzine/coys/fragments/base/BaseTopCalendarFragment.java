package com.fanzine.coys.fragments.base;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.fanzine.coys.adapters.match.TopCalendarAdapter;
import com.fanzine.coys.models.DayInfo;
import com.fanzine.coys.utils.RecyclerItemClickListener;

import java.util.Date;

/**
 * Created by Woland on 13.01.2017.
 */

public abstract class BaseTopCalendarFragment extends BaseFragment {

    private static TopCalendarAdapter adapter;

    private int selected = -1;
    private int mScrollPosition;

    private RecyclerView mTopPanel;

    public boolean isToday() {
        if (mTopPanel == null) return false;

        return adapter.isToday(adapter.getSelectedIndex());
    }

    protected void initTopCalendar(RecyclerView topPanel) {
        mTopPanel = topPanel;
        topPanel.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        adapter = new TopCalendarAdapter(getContext(), selected);
        topPanel.setAdapter(adapter);
        topPanel.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), (view, position) -> {
            selected = position;
            ((TopCalendarAdapter) topPanel.getAdapter()).setSelected(position);
            onTopCalendarItemSelected(view, position);
        }));

        topPanel.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mScrollPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
            }
        });


        LinearSnapHelper snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                View centerView = findSnapView(layoutManager);
                if (centerView == null)
                    return RecyclerView.NO_POSITION;

                int position = layoutManager.getPosition(centerView);
                int targetPosition = -1;
                if (layoutManager.canScrollHorizontally()) {
                    if (velocityX < 0) {
                        targetPosition = position - 1;
                    } else {
                        targetPosition = position + 1;
                    }
                }

                if (layoutManager.canScrollVertically()) {
                    if (velocityY < 0) {
                        targetPosition = position - 1;
                    } else {
                        targetPosition = position + 1;
                    }
                }

                final int firstItem = 0;
                final int lastItem = layoutManager.getItemCount() - 1;
                targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));
                return targetPosition;
            }
        };
        snapHelper.attachToRecyclerView(topPanel);

        topPanel.scrollToPosition(adapter.getSelectedIndex()-2);
    }

    public static Date getSelectedDate() {
        return adapter.getSelectedDate();
    }

    public DayInfo getSelectedDayInfo() {
        return adapter.getSelectedDayInfo();
    }

    public abstract void onTopCalendarItemSelected(View view, int position);

}
