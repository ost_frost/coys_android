package com.fanzine.coys.fragments.team.player;

import android.databinding.ViewDataBinding;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.fragments.team.ImagePreviewActivity;
import com.fanzine.coys.R;
import com.fanzine.coys.adapters.team.player.PhotoPlayerAdapter;
import com.fanzine.coys.databinding.FragmentTeamPlayerPhotosBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.team.Player;
import com.fanzine.coys.models.team.PlayerPhoto;
import com.fanzine.coys.viewmodels.fragments.team.player.PhotosViewModel;

import java.util.ArrayList;
import java.util.List;

public class PhotosFragment extends BaseFragment implements PhotosViewModel.OnPhotosLoadListener {

    private final static String PLAYER = "player";

    public static PhotosFragment getInstance(Player player) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PLAYER, player);

        PhotosFragment fragment = new PhotosFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    private PhotoPlayerAdapter adapter;
    @Nullable
    private FragmentTeamPlayerPhotosBinding binding;

    private Drawable divider;

    private DividerItemDecoration dividerItemDecoration;

    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        PhotosViewModel viewModel = new PhotosViewModel(getContext(), getArguments().getParcelable(PLAYER), this);
        binding = FragmentTeamPlayerPhotosBinding.inflate(inflater, root, attachToRoot);
        binding.setViewModel(viewModel);
        viewModel.loadPhotos();
        divider = ContextCompat.getDrawable(getContext(), R.drawable.gradient_horizontal_line);
        dividerItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(divider);
        return binding;
    }

    @Override
    public void onSuccess(List<PlayerPhoto> photos) {
        setupRecyclerView(photos);
    }

    private void setupRecyclerView(List<PlayerPhoto> photos) {
        if (binding != null) {
            binding.recView.addItemDecoration(dividerItemDecoration);
            StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
            adapter = new PhotoPlayerAdapter(getContext(), layoutManager, playerPhoto -> {
                if (adapter != null) {
                    ImagePreviewActivity.launch(getContext(), playerPhoto, new ArrayList<>(adapter.data));
                }
            });
            binding.recView.setLayoutManager(layoutManager);
            binding.recView.setAdapter(adapter);
            adapter.refresh(photos);
        }
    }
}
