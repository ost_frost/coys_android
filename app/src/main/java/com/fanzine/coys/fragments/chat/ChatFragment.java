package com.fanzine.coys.fragments.chat;

import android.databinding.ViewDataBinding;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.chat.ChatMessageAdapter;
import com.fanzine.coys.databinding.FragmentChatBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.viewmodels.fragments.chat.ChatFragmentViewModel;
import com.fanzine.chat.ChatSDK;
import com.fanzine.chat.interfaces.FCUsersListener;
import com.fanzine.chat.interfaces.blacklist.FCBlacklistListener;
import com.fanzine.chat.interfaces.sync.FCUserSyncListener;
import com.fanzine.chat.models.channels.FCChannel;
import com.fanzine.chat.models.user.FCUser;
import com.jakewharton.rxbinding.view.RxView;
import com.miguelbcr.ui.rx_paparazzo.RxPaparazzo;
import com.miguelbcr.ui.rx_paparazzo.entities.size.CustomMaxSize;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.fanzine.coys.utils.LoadingStates.DONE;

/**
 * Created by Woland on 12.04.2017.
 */

public class ChatFragment extends BaseFragment implements FCBlacklistListener {

    public static String channelId = null;

    private static final String CHANNEL = "channel";
    private static final String IS_EMPTY = "isEmpty";

    private FCChannel channel;
    private ChatFragmentViewModel viewModel;

    private List<FCUser> users = new ArrayList<>();

    private String title = "";
    private FragmentChatBinding binding;

    public static ChatFragment newInstance(FCChannel channel, boolean isEmpty) {
        Bundle args = new Bundle();
        args.putParcelable(CHANNEL, channel);
        args.putBoolean(IS_EMPTY, isEmpty);
        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        if (binding == null || viewModel == null) {
            binding = FragmentChatBinding.inflate(inflater, root, attachToRoot);

            binding.rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true));

            channel = getArguments().getParcelable(CHANNEL);

            if (TextUtils.isEmpty(channel.getOwner()))
                channel.sync(null);

            channelId = channel.getUid();

            viewModel = new ChatFragmentViewModel(getContext(), channel);
            binding.setViewModel(viewModel);

            ChatMessageAdapter adapter = new ChatMessageAdapter(getContext(), channel, new ChatMessageAdapter.LoadedListener() {
                @Override
                public void loaded() {
                    viewModel.setState(DONE);
                }

                @Override
                public void added() {
                    binding.rv.scrollToPosition(0);
                }
            });
            channel.subscribe(adapter);
            binding.rv.setAdapter(adapter);

            setToolbar(binding.toolbar);

            ChatSDK.getInstance().getBlacklistManager().setBlacklistListener(this);

            if (getArguments().getBoolean(IS_EMPTY, true))
                viewModel.setState(DONE);

            RxView.clicks(binding.attach).subscribe(aVoid -> RxPaparazzo.takeImage(this)
                    .size(new CustomMaxSize(720))
                    .usingGallery()
                    .subscribe(response -> {
                        if (response.resultCode() == RESULT_OK) {
                            response.targetUI().viewModel.attach(response.data());
                        }
                    }));
        }
        return binding;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!channel.isOneToOne() || TextUtils.isEmpty(viewModel.title.get())) {
            ChatSDK.getInstance().getChatManager().getAllChatUsers(channel, new FCUsersListener() {
                @Override
                public void onUsersReceived(List<FCUser> list) {
                    title = "";
                    users = list;
                    syncUser(list, 0);
                }

                @Override
                public void onError(Exception e) {

                }
            });
        }
    }

    private void syncUser(List<FCUser> users, int position) {
        if (!users.get(position).getUid().equals(ChatSDK.getInstance().getCurrentSession().getCurrentUser().getUid())) {
            users.get(position).sync(new FCUserSyncListener() {
                @Override
                public void onError(Exception e) {

                }

                @Override
                public void onSync(FCUser user) {
                    if (!TextUtils.isEmpty(title))
                        title += ", " + user.getFirstName() + " " + user.getLastName();
                    else
                        title = user.getFirstName() + " " + user.getLastName();

                    if (position == users.size() - 1)
                        viewModel.title.set(title);
                    else
                        syncUser(users, position + 1);
                }
            });
        }
        else {
            if (position == users.size() - 1)
                viewModel.title.set(title);
            else
                syncUser(users, position + 1);
        }
    }

    private void setToolbar(Toolbar toolbar) {
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(ContextCompat.getColor(getContext(), R.color.inbox_bg_home_button), PorterDuff.Mode.SRC_ATOP);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        channelId = null;
        if (channel != null)
            channel.unsubscribe();
    }

    @Override
    public void addedToBlackList(String chatId) {
        if (channel.getUid().equals(chatId)) {
            channel.unsubscribe();
            channel = null;

            DialogUtils.showAlertDialog(getContext(), R.string.yourAreBannedFromThisChat, (dialog, which) -> getActivity().onBackPressed());
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.chat_userlist, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.info) {
            FragmentUtils.changeFragment(getActivity(), R.id.content_frame, UserListFragment.newInstance(channel, users), true);
        }

        return super.onOptionsItemSelected(item);
    }
}
