package com.fanzine.coys.fragments.profile;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.coys.models.profile.LeagueType;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vitaliygerasymchuk on 2/12/18
 */

public class ProfileLeagueTypesAdapter extends RecyclerView.Adapter<ProfileLeagueTypesAdapter.LeagueHolder> {

    public interface Callback{
        void onLeagueTypeSelected(@NonNull LeagueType type);
    }

    @NonNull
    private List<LeagueType> leagueTypes = new ArrayList<>();

    @NonNull
    private Callback callback;

    public void setLeagueTypes(@NonNull List<LeagueType> leagueTypes) {
        this.leagueTypes.clear();
        this.leagueTypes.addAll(leagueTypes);
        this.notifyDataSetChanged();
    }

    public void setCallback(@NonNull Callback callback) {
        this.callback = callback;
    }

    @Override
    public LeagueHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile_league_type, parent, false);
        return new LeagueHolder(view, adapterPosition -> {
            if (adapterPosition < leagueTypes.size()) {
                final LeagueType type = leagueTypes.get(adapterPosition);
                callback.onLeagueTypeSelected(type);
            }
        });
    }

    @Override
    public void onBindViewHolder(LeagueHolder holder, int position) {
        if (position < leagueTypes.size()) {
            LeagueType type = leagueTypes.get(position);
            holder.bind(type);
        }
    }

    @Override
    public int getItemCount() {
        return leagueTypes.size();
    }

    static class LeagueHolder extends RecyclerView.ViewHolder {

        TextView tvLeagueTypeName;

        interface OnLeagueClickListener {
            void onLeagueAdapterPosition(int adapterPosition);
        }

        LeagueHolder(View itemView, @NonNull OnLeagueClickListener leagueClickListener) {
            super(itemView);
            tvLeagueTypeName = (TextView) itemView.findViewById(R.id.tv_league_name);
            RxView.clicks(itemView.findViewById(R.id.click)).subscribe((aVoid) -> {
                leagueClickListener.onLeagueAdapterPosition(getAdapterPosition());
            });
        }

        public void bind(@NonNull LeagueType type){
            tvLeagueTypeName.setText(type.getName());
        }
    }
}
