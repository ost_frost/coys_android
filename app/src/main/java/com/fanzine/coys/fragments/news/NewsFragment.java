package com.fanzine.coys.fragments.news;

import android.databinding.ViewDataBinding;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.NewsAdapter;
import com.fanzine.coys.databinding.FragmentNewsBinding;
import com.fanzine.coys.fragments.like.LikeCallback;
import com.fanzine.coys.fragments.like.LikeViewModel;
import com.fanzine.coys.models.News;
import com.fanzine.coys.utils.ShareUtil;
import com.fanzine.coys.viewmodels.fragments.news.NewsFeedViewModel;

import java.util.List;

/**
 * Created by Woland on 10.01.2017.
 */

public class NewsFragment extends FeedListAbstractFragment<News> implements NewsAdapter.NewsCallback, LikeCallback<News> {

    private FragmentNewsBinding binding;
    private NewsAdapter adapter;
    private LikeViewModel likeViewModel;


    public static NewsFragment newInstance(int leagueId) {
        Bundle args = new Bundle();
        args.putInt(LEAGUE_ID, leagueId);
        NewsFragment fragment = new NewsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        if (binding == null) {
            likeViewModel = new LikeViewModel(getContext());
            binding = FragmentNewsBinding.inflate(inflater, root, attachToRoot);
            NewsFeedViewModel viewModel = new NewsFeedViewModel(getContext(), this, getLeagueId());
            binding.setViewModel(viewModel);

            setBaseViewModel(viewModel);

            final LinearLayoutManager linearLayoutManager =
                    new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            binding.rv.setLayoutManager(linearLayoutManager);

            viewModel.loadData(page);

            getAppCompatActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getAppCompatActivity().getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), R.color.news_bar_color));

            binding.swipe.setEnabled(true);
            binding.swipe.setColorSchemeResources(R.color.colorAccent);
            binding.swipe.setOnRefreshListener(() -> viewModel.loadData(page));
            binding.rv.setAnimation(null);
            binding.swipe.setAnimation(null);


            setupScrollListener(binding.rv, linearLayoutManager, binding.swipe);

            ((SimpleItemAnimator) binding.rv.getItemAnimator()).setSupportsChangeAnimations(false);
        }
        return binding;
    }

    @Override
    public void loadData(int page) {
        binding.getViewModel().loadData(page);
    }

    @Override
    public void onFbPostShare(News news) {
        ShareUtil.shareNewsToFacebook(getActivity(), news);
    }

    @Override
    public void onTwitterShare(News news) {
        ShareUtil.shareNewsToTwitter(getContext(), news);
    }

    @Override
    public void onChatShare(News news) {
        ShareUtil.shareNewsToChat(getContext(), news);
    }

    @Override
    public void onMailShare(News news) {
        ShareUtil.shareNewsToMail(getContext(), news);
    }

    @Override
    public void onClipboardCopy(News news) {
        ShareUtil.copyToClipboardNews(getContext(), news);
    }

    @Override
    public void onLiked(News news) {
        if (adapter != null) {
            adapter.refreshItem(news);
        }
    }

    @Override
    public void onUnLiked(News news) {
        if (adapter != null) {
            adapter.refreshItem(news);
        }
    }

    @Override
    public void onLikePost(News news) {
        if (likeViewModel != null) {
            if (news.isLiked) {
                likeViewModel.unlikeNews(news, this);
            } else {
                likeViewModel.likeNews(news, this);
            }
        }
    }

    @Override
    public void onLoaded(List<News> data) {

        if ( !binding.swipe.isEnabled()) {
            adapter.refresh(data);
            binding.swipe.setRefreshing(false);
            binding.swipe.setEnabled(true);
        }
        else {
            //todo:: add constants
            if (data.size() > 0) {
                page++;

                if (adapter == null) {
                    adapter = new NewsAdapter(getContext(), data, this);
                    binding.rv.setAdapter(adapter);
                } else if (page == 1) {
                    data.clear();
                    adapter.addNews(data);
                } else {
                    adapter.addNews(data);
                }

                isLoading = false;
            }
        }
    }

    @Override
    public void onLoaded(News data) {

    }


    @Override
    public void onError() {
        if (binding.swipe.isRefreshing())
            binding.swipe.setRefreshing(false);

        isLoading = false;
    }
}
