package com.fanzine.coys.fragments.team;


import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.databinding.FragmentTweetsBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;
import com.twitter.sdk.android.tweetui.UserTimeline;


public class TweetsFragment extends BaseFragment {

    private static final String DATA = "tweet";

    public static TweetsFragment newInstance(String tweet) {
        TweetsFragment fragment = new TweetsFragment();
        Bundle args = new Bundle();
        args.putString(DATA, tweet);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        FragmentTweetsBinding binding = FragmentTweetsBinding.inflate(inflater, root, attachToRoot);

        final UserTimeline userTimeline = new UserTimeline.Builder()
                .screenName(getArguments().getString(DATA, ""))
                .build();
        final TweetTimelineListAdapter adapter = new TweetTimelineListAdapter.Builder(getContext())
                .setTimeline(userTimeline)
                .build();

        binding.rvTweets.setAdapter(adapter);


        return binding;
    }
}
