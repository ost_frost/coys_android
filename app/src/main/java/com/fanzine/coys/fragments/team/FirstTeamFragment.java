package com.fanzine.coys.fragments.team;

import android.os.Bundle;

import com.fanzine.coys.models.team.TeamSquad;

/**
 * Created by vitaliygerasymchuk on 2/20/18
 */

public class FirstTeamFragment extends AbsPlayersFragment {

    public static FirstTeamFragment newInstance() {
        FirstTeamFragment fragment = new FirstTeamFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected TeamSquad getSquad() {
        return TeamSquad.FIRST_TEAM;
    }
}
