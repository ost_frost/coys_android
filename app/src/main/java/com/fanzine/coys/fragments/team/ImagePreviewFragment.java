package com.fanzine.coys.fragments.team;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fanzine.coys.databinding.FragmentImagePreviewBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.team.PlayerPhoto;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class ImagePreviewFragment extends BaseFragment {

    public static final String ARG_PHOTO = "ARG_PHOTO";

    public static ImagePreviewFragment newInstance(@NonNull PlayerPhoto photo) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_PHOTO, photo);

        ImagePreviewFragment fragment = new ImagePreviewFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    private FragmentImagePreviewBinding binding;

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentImagePreviewBinding.inflate(inflater, root, attachToRoot);
        final PlayerPhoto photo = getArguments().getParcelable(ARG_PHOTO);
        if (photo != null) {
            if (!TextUtils.isEmpty(photo.getUrl())) {
                loadImage(binding.iv, photo.getUrl());
            }
        }
        return binding;
    }

    private void loadImage(@NonNull ImageView imageView, @NonNull String imageUrl) {
        Glide.with(this)
                .load(imageUrl)
                .fitCenter()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        hideProgress();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        hideProgress();
                        return false;
                    }
                }).into(imageView);
    }

    private void hideProgress() {
        if (binding != null) {
            binding.pb.setVisibility(View.GONE);
        }
    }
}