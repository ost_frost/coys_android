package com.fanzine.coys.fragments.base;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.fanzine.coys.adapters.match.TopScrollWeeksAdapter;
import com.fanzine.coys.utils.RecyclerItemClickListener;

/**
 * Created by maximdrobonoh on 27.09.17.
 */

public abstract class BaseTopScrollWeeksFragment extends BaseFragment {

    protected static TopScrollWeeksAdapter topScrollWeeksAdapter;

    private static int selected = -1;
    private static int mScrollPosition;

    protected void initTopWeeksBar(RecyclerView topPanel) {
        if (selected == -1) {
            selected = 0;
            mScrollPosition = 12;
        }

        topPanel.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false) {
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }
        });
        topScrollWeeksAdapter = new TopScrollWeeksAdapter(getContext(), selected);
        topPanel.setAdapter(topScrollWeeksAdapter);
        topPanel.scrollToPosition(mScrollPosition);
        topPanel.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), (view, position) -> {
            selected = position;
            ((TopScrollWeeksAdapter) topPanel.getAdapter()).setSelected(position);
            initTopWeeksBarItemSelected(view, position);
        }));

        topPanel.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mScrollPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
            }
        });

    }

    public static String getSelectedDate() {
        return topScrollWeeksAdapter.getSelectedDate();
    }

    public abstract void initTopWeeksBarItemSelected(View view, int position);
}