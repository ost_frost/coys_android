package com.fanzine.coys.fragments.venue;

import android.os.Parcelable;

/**
 * Created by mbp on 2/1/18.
 */

public interface OnFragmentChangeListener extends Parcelable {

    void onVenueDetailOpen();

    void redirectToBack();
}
