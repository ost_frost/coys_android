package com.fanzine.coys.fragments.match;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.base.NavigationFragmentActivity;
import com.fanzine.coys.databinding.FragmentMatchBinding;
import com.fanzine.coys.interfaces.MatchDetailLoading;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.models.MatchChannel;
import com.fanzine.coys.viewmodels.fragments.match.MatchFragmentViewModel;
import com.bumptech.glide.Glide;

import java.util.List;

public class MatchFragment extends NavigationFragmentActivity implements MatchDetailLoading {

    private static final int LINE_UP = 0;
    private static final int COMMENTARY = 1;
    private static final int SUMMARY = 2;
    private static final int STATS = 3;
    private static final int ANALYSIS = 4;


    public static final String LEAGUE_TITLE = "league_title";

    private FragmentMatchBinding binding;
    private MatchFragmentViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = FragmentMatchBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        Match match = getIntent().getParcelableExtra(Match.MATCH);

        if (match != null) {

            viewModel = new MatchFragmentViewModel(this, getSupportFragmentManager(), match,
                    this);
            viewModel.getMatchDetail();

            binding.setViewModel(viewModel);
            binding.arrowBack.setOnClickListener(view -> onBackPressed());

            binding.matchTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    if (tab.getPosition() == LINE_UP) {
                        viewModel.openLineUp();
                    } else if (tab.getPosition() == COMMENTARY) {
                        viewModel.openCommentary();
                    } else if (tab.getPosition() == SUMMARY) {
                        viewModel.openSummary();
                    } else if (tab.getPosition() == STATS) {
                        viewModel.openStats();
                    } else if (tab.getPosition() == ANALYSIS) {
                        viewModel.openAnalysis();
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
        }
    }


    @Override
    public void onLoaded(Match match) {
        if (match != null && match.getMatchChannel() != null) {
            viewModel.setLeagueTitle(match.getAltName());

            binding.rvChannels.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            binding.rvChannels.setAdapter(new ChannelsAdapter(match.getMatchChannel()));
        }
    }


    private class ChannelsAdapter extends RecyclerView.Adapter<ChannelsAdapter.Holder> {

        private List<MatchChannel> channels;

        public ChannelsAdapter(List<MatchChannel> channels) {
            this.channels = channels;
        }

        @Override
        public ChannelsAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            return new Holder(inflater.inflate(R.layout.item_channel, parent, false));
        }

        @Override
        public void onBindViewHolder(ChannelsAdapter.Holder holder, int position) {
            holder.onBind(position);
        }

        @Override
        public int getItemCount() {
            return channels.size();
        }

        class Holder extends RecyclerView.ViewHolder {
            private ImageView view;

            public Holder(View itemView) {
                super(itemView);
                this.view = (ImageView) itemView.findViewById(R.id.image);
            }

            public void onBind(int pos) {

                view.setOnClickListener(view -> {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(channels.get(pos).getLink()));
                    startActivity(browserIntent);
                });
                Glide.with(view.getContext()).load(channels.get(pos).getIcon()).override(180, 85).into(view);
            }
        }
    }
}
