package com.fanzine.coys.fragments.team.player;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.databinding.FragmentTeamPlayerProfileBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.team.PlayerProfile.Profile;
import com.fanzine.coys.viewmodels.fragments.team.player.ProfileViewModel;

/**
 * Created by mbp on 1/10/18.
 */

public class ProfileFragment extends BaseFragment {

    private static final String PROFILE = "player_profile";

    public static ProfileFragment newInstance(Profile profile) {
        ProfileFragment profileFragment = new ProfileFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable(PROFILE, profile);
        profileFragment.setArguments(bundle);

        return profileFragment;
    }

    private FragmentTeamPlayerProfileBinding binding;
    private ProfileViewModel viewModel;

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        Profile profile = getArguments().getParcelable(PROFILE);
        viewModel = new ProfileViewModel(getContext(), profile);

        binding = FragmentTeamPlayerProfileBinding.inflate(inflater, root, attachToRoot);
        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);

        return binding;
    }
}
