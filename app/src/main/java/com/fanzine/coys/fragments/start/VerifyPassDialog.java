package com.fanzine.coys.fragments.start;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;

import com.fanzine.coys.databinding.DialogPhoneVerifyBinding;
import com.fanzine.coys.dialogs.base.BaseDialog;
import com.fanzine.coys.viewmodels.fragments.login.VerifyPassViewModel;

/**
 * Created by Woland on 03.04.2017.
 */

public class VerifyPassDialog extends BaseDialog implements DialogInterface {

    private static final String PHONE = "phone";
    private static final String PHONE_CODE = "phone_code";
    private static final String NEW_PASSWORD = "new_password";

    public static VerifyPassDialog newInstance(String phoneCode, String phone, String newPassword) {
        Bundle args = new Bundle();
        args.putString(PHONE, phone);
        args.putString(PHONE_CODE, phoneCode);
        args.putString(NEW_PASSWORD, newPassword);

        VerifyPassDialog fragment = new VerifyPassDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void cancel() {
        dismiss();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        String phone = getArguments().getString(PHONE);
        String phoneCocde = getArguments().getString(PHONE_CODE);
        String newPassword = getArguments().getString(NEW_PASSWORD);

        DialogPhoneVerifyBinding binding = DialogPhoneVerifyBinding.inflate(LayoutInflater.from(getContext()), null);
        VerifyPassViewModel viewModel = new VerifyPassViewModel(getContext(), phoneCocde, phone, newPassword);

        binding.setViewModel(viewModel);
        setBaseViewModel(viewModel);

        builder.setView(binding.getRoot());

        binding.cancel.setOnClickListener(view -> {
            dismiss();
        });

        return builder.create();
    }
}
