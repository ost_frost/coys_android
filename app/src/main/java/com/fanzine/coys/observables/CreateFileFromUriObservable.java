package com.fanzine.coys.observables;

import android.content.Context;
import android.net.Uri;

import com.fanzine.coys.utils.FileUtils;

import java.io.File;

import rx.Observable;
import rx.functions.Func0;

/**
 * Created by maximdrobonoh on 11.09.17.
 */

public class CreateFileFromUriObservable implements Func0<Observable<File>> {

    private Context context;
    private Uri data;

    public CreateFileFromUriObservable(Context context, Uri data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public Observable<File> call() {
        try {
            return Observable.just(FileUtils.fileFromUri(context, data));
        } catch (Exception ex) {
            return Observable.error(ex);
        }
    }
}
