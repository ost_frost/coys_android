package com.fanzine.coys.observables;

import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.networking.EmailDataRequestManager;

import java.util.List;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by maximdrobonoh on 06.09.17.
 */

public class MoveEmailObservable implements Func1<ResponseBody, Observable<List<Mail>>> {

    private String folder;

    public MoveEmailObservable(String folder) {
        this.folder = folder;
    }

    @Override
    public Observable<List<Mail>> call(ResponseBody responseBody) {
        return EmailDataRequestManager.getInstanse().getMailsByFolder(folder);
    }
}
