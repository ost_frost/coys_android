package com.fanzine.coys.subscribers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.fanzine.coys.R;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.models.mails.Mail;
import com.fanzine.coys.repository.MailsRepo;
import com.fanzine.coys.utils.DialogUtils;

import java.util.Collections;
import java.util.List;

import rx.Subscriber;

/**
 * Created by maximdrobonoh on 06.09.17.
 */

public class MoveEmailSubscriber extends Subscriber<List<Mail>> {

    private Context context;
    private ProgressDialog pd;
    private DialogInterface di;
    private String folderFrom;
    private int mailId;

    public MoveEmailSubscriber(Context context, DialogInterface di, String folderFrom, int mailId) {
        this.context = context;
        this.pd = DialogUtils.showProgressDialog(context, R.string.please_wait);
        this.di = di;
        this.folderFrom = folderFrom;
        this.mailId = mailId;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        pd.dismiss();

        FieldError errors = ErrorResponseParser.getErrors(e);
        if (errors.isEmpty()) {
            DialogUtils.showAlertDialog(context, R.string.smth_goes_wrong);
        } else {
            DialogUtils.showAlertDialog(context, errors.getMessage());
        }
    }

    @Override
    public void onNext(List<Mail> list) {
        Collections.sort(list, (o1, o2) -> o2.getDateTime().compareTo(o1.getDateTime()));

        MailsRepo.getInstance().deleteMail(folderFrom, mailId).subscribe();
        MailsRepo.getInstance().updateMails(list).subscribe();

        pd.dismiss();
        di.dismiss();

        ((Activity) context).onBackPressed();
    }
}
