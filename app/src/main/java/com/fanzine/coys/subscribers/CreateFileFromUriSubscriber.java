package com.fanzine.coys.subscribers;

import android.app.ProgressDialog;

import com.fanzine.coys.adapters.mails.AttachmentsAdapter;
import com.fanzine.coys.models.mails.Attachment;
import com.fanzine.coys.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;

/**
 * Created by maximdrobonoh on 11.09.17.
 */

public class CreateFileFromUriSubscriber extends Subscriber<File> {

    private AttachmentsAdapter adapter;
    private ProgressDialog pd;

    public CreateFileFromUriSubscriber(AttachmentsAdapter adapter, ProgressDialog pd) {
        this.adapter = adapter;
        this.pd = pd;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        pd.dismiss();
    }

    @Override
    public void onNext(File file) {
        String fileExt = FileUtils.getFileExtension(file);
        String fileSize = String.valueOf(file.length());

        Attachment attachment = new Attachment(file, file.getName(), fileExt, fileSize);

        List<Attachment> attachmentList = new ArrayList<>();
        attachmentList.add(attachment);

        pd.dismiss();

        adapter.add(attachmentList);
    }
}
