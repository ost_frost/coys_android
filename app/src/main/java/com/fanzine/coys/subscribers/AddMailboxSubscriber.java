package com.fanzine.coys.subscribers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.mails.MailCategoriesAdapter;
import com.fanzine.coys.api.error.ErrorResponseParser;
import com.fanzine.coys.api.error.FieldError;
import com.fanzine.coys.utils.DialogUtils;

import okhttp3.ResponseBody;
import rx.Subscriber;

/**
 * Created by maximdrobonoh on 13.09.17.
 */

public class AddMailboxSubscriber  extends Subscriber<ResponseBody> {

    private Context context;
    private ProgressDialog pd;
    private DialogInterface di;
    private MailCategoriesAdapter adapter;
    private String mailbox;
    private AlertDialog alertDialog;

    public AddMailboxSubscriber(Context context, MailCategoriesAdapter adapter,
                                String mailbox, AlertDialog alertDialog) {
        this.pd = DialogUtils.showProgressDialog(context, R.string.please_wait);
        this.adapter = adapter;
        this.mailbox = mailbox;
        this.context = context;

        this.alertDialog = alertDialog;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        pd.dismiss();

        FieldError errors = ErrorResponseParser.getErrors(e);
        if (errors.isEmpty()) {
            DialogUtils.showAlertDialog(context, R.string.smth_goes_wrong);
        } else {
            DialogUtils.showAlertDialog(context, errors.getMessage());
        }
    }

    @Override
    public void onNext(ResponseBody body) {
        pd.dismiss();

        adapter.add(mailbox);

        alertDialog.dismiss();
    }
}
