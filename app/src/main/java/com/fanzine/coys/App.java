package com.fanzine.coys;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;

import com.fanzine.coys.db.factory.HelperFactory;

import com.fanzine.coys.helpers.Constants;
import com.fanzine.chat.ChatSDK;
import com.facebook.FacebookSdk;
import com.google.firebase.FirebaseApp;
import com.miguelbcr.ui.rx_paparazzo.RxPaparazzo;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import org.solovyev.android.checkout.Billing;
import org.solovyev.android.checkout.Cache;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import io.fabric.sdk.android.Fabric;
import rx_activity_result.RxActivityResult;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Woland on 05.01.2017.
 */

public class App extends MultiDexApplication {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "SoaUMxGluRwe0EdiB7TpmNnhJ";
    private static final String TWITTER_SECRET = "2gv1cP1IW1uvip4zpht6AJs49h38iGQoGp10b8Ugv7Nd5xf7Hv";

    static Context context;

    @Nonnull
    private static App instance;

    private final Billing mBilling = new Billing(this, new Billing.DefaultConfiguration() {
        @Override
        public String getPublicKey() {
            return Constants.IN_BILLING_LICENSE_KEY;
        }

        @Nullable
        @Override
        public Cache getCache() {
            return Billing.newCache();
        }
    });

    public App() {
        instance = this;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public Billing getBilling() {
        return mBilling;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        context = getApplicationContext();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/SanFranciscoText-Regular.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        HelperFactory.setHelper(this);

        FacebookSdk.sdkInitialize(getApplicationContext());

        FirebaseApp.initializeApp(this);

        ChatSDK.getInstance().init();
        ChatSDK.getInstance().offlineModeEnable(true);

        RxPaparazzo.register(this);
        RxActivityResult.register(this);
    }


    public static App get() {
        return instance;
    }

    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        ChatSDK.getInstance().terminate();
        super.onTerminate();
    }

    public static Context getContext() {
        return context;
    }
}
