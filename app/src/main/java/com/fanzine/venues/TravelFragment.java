package com.fanzine.venues;

import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.venues.FixturesAdapter;
import com.fanzine.coys.adapters.venues.VenuesAdapter;
import com.fanzine.coys.databinding.FragmentVenuesTravelBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.models.Match;
import com.fanzine.coys.utils.GeoHelper;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.firebase.crash.FirebaseCrash;
import com.yelp.fusion.client.models.Business;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class TravelFragment extends BaseFragment implements
        TravelViewModel.TravelLoadingCallback,
        FixturesAdapter.OnMatchClickListener {

    private FragmentVenuesTravelBinding binding;

    private static final String DATA = "data";
    private static final String USER_LOCATION = "user_location";

    private TravelViewModel viewModel;
    private List<Business> businesses;
    private VenuesAdapter adapter;

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 76;

    private GoogleApiClient mGoogleApiClient;

    private String fromLocation;
    private String toLocation;

    public static TravelFragment newInstance(ArrayList<Business> businesses) {
        TravelFragment fragment = new TravelFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(DATA, businesses);
        fragment.setArguments(args);
        return fragment;
    }

    public static TravelFragment newInstance(String userAddress) {
        TravelFragment fragment = new TravelFragment();
        Bundle args = new Bundle();
        args.putString(USER_LOCATION, userAddress);

        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {

        binding = FragmentVenuesTravelBinding.inflate(inflater, root, false);

        viewModel = new TravelViewModel(getContext(), this);
        binding.setViewModel(viewModel);

        setBaseViewModel(viewModel);

        fromLocation = getArguments().getString(USER_LOCATION);

        binding.tvUserLocation.setOnClickListener(view -> {
            try {
                Intent intent =
                        new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                .build(getActivity());
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            } catch (GooglePlayServicesRepairableException e) {
                FirebaseCrash.report(e);
            } catch (GooglePlayServicesNotAvailableException e) {
                FirebaseCrash.report(e);
            }
        });

        binding.showDirectionBtn.setOnClickListener(view -> {
            toLocation = binding.upcomingFixtures.getAddress();

            showMapFromLocation(fromLocation, toLocation);
        });

        binding.upcomingFixtures.attachOnMatchClickListener(this);
        binding.upcomingFixtures.setTitle(getString(R.string.travel_fixtures_title));
        binding.upcomingFixtures.hideCurrentLocationHeader();

        return binding;
    }

    @Override
    public void onMatchClicked(Match match) {
        String venueAddress = match.getVenue().getName() + "," + match.getVenue().getPostalCode();

        String fullAddress = GeoHelper.decodeAddress(getContext(), venueAddress);

        viewModel.requestGeoCode(fullAddress);

        binding.tvMatchAddress.setText(fullAddress);
    }

    @Override
    public void onPlaceImageReady(Bitmap bitmap) {
        if (bitmap != null) {
            binding.ivAddressPhoto.setImageBitmap(bitmap);
        }
    }

    //todo:: dupllicated code with SearchActivity
    //todo: refactor
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);

                fromLocation = place.getAddress().toString();

                binding.tvUserLocation.setText(fromLocation);

                Log.i(getTag(), "Place: " + place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getContext(), data);
                // TODO: Handle the error.
                Log.i(getTag(), status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void showMapFromLocation(String from, String to) {
        String desLocation = "&destination=";
        String currLocation = "&origin=";

        if (from != null) {
            from = from.replace(' ', '+');
            currLocation += from;
        }

        if (to != null) {
            to = to.replace(' ', '+');
            desLocation += to;
        }

        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("https://www.google.com/maps/dir/?api=1" + currLocation
                        + desLocation + "&travelmode=bicycling"));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                & Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.setClassName("com.google.android.apps.maps",
                "com.google.android.maps.MapsActivity");
        startActivity(intent);
    }
}
