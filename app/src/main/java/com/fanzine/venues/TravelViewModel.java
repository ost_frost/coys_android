package com.fanzine.venues;

import android.content.Context;
import android.graphics.Bitmap;

import com.fanzine.coys.viewmodels.fragments.GoogleMapBaseViewModel;
import com.google.android.gms.location.places.PlaceBuffer;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Woland on 15.03.2017.
 */

public class TravelViewModel extends GoogleMapBaseViewModel {

    public interface TravelLoadingCallback {
        void onPlaceImageReady(Bitmap bitmap);
    }

    private TravelLoadingCallback callback;

    public TravelViewModel(Context context, TravelLoadingCallback callback) {
        super(context);
        this.callback = callback;
    }

    @Override
    protected void onPlacesLoaded(PlaceBuffer places) {

        Observable.fromCallable(() -> requestPhotoByPlaceId(places.get(0).getId()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((result) -> {
                    callback.onPlaceImageReady(result);
                });
    }

    @Override
    protected void onPlacesLoadError() {

    }

}
