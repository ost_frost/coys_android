package com.fanzine.chat3.model.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mbp on 12/20/17.
 */

public class RenameGroup {

    @SerializedName("name")
    public String name;

    public RenameGroup(String name) {
        this.name = name;
    }

    public RenameGroup() {
    }
}
