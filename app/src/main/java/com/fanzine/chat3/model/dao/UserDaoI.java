package com.fanzine.chat3.model.dao;

import android.graphics.Bitmap;

import com.fanzine.chat3.model.pojo.FbChannel;
import com.fanzine.chat3.model.pojo.FbUser;

import rx.Observable;

/**
 * Created by a on 17.11.17.
 */

public interface UserDaoI {

    Observable<FbUser> isGunnerUserExist(String phone);

    Observable<FbUser> getUserByUid(String uid);

    void saveUserToFb(FbUser fbUser);

    Bitmap getContactPhotoByPhone(String number);

    Observable<FbChannel> getJointDialog(String userUid, String user2Uid);
}