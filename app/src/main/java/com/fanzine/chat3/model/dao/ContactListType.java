package com.fanzine.chat3.model.dao;

/**
 * Created by mbp on 11/17/17
 */

public enum ContactListType {
    SINGLE, MULTIPLE, MULTIPLE_GROUP_CONTACT
}
