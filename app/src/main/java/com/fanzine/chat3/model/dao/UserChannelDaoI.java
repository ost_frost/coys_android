package com.fanzine.chat3.model.dao;

import com.fanzine.chat3.model.pojo.FbUser;
import com.fanzine.chat3.model.pojo.FbUserChannel;

import java.util.List;

import rx.Observable;
import rx.Single;

/**
 * Created by a on 30.11.17.
 */

public interface UserChannelDaoI {
    Observable<List<FbUser>> getFbUsers(String chanelUid);

    Single<Boolean> removeUserFromChannel(String userChannelUid);

    Single<Boolean> removeUserFromChannel(String userUid, String channelUid);

    Observable<List<FbUserChannel>> getUserChannelByChannel(String chanelUid);

    void leaveDialog(String userUid, String chanelUid);

    Observable<FbUserChannel> getUserChannelByUid(String userChannelUid);
}
