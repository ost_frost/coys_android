package com.fanzine.chat3.model.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mbp on 4/18/18.
 */

public class ImageCapture implements Parcelable {

    public String msgText;
    public String imagePath;

    public ImageCapture(String msgText, String imagePath) {
        this.msgText = msgText;
        this.imagePath = imagePath;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.msgText);
        dest.writeString(this.imagePath);
    }

    protected ImageCapture(Parcel in) {
        this.msgText = in.readString();
        this.imagePath = in.readString();
    }

    public static final Parcelable.Creator<ImageCapture> CREATOR = new Parcelable.Creator<ImageCapture>() {
        @Override
        public ImageCapture createFromParcel(Parcel source) {
            return new ImageCapture(source);
        }

        @Override
        public ImageCapture[] newArray(int size) {
            return new ImageCapture[size];
        }
    };
}
