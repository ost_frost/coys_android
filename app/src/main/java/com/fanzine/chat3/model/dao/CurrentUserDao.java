package com.fanzine.chat3.model.dao;

import android.util.Log;

import com.fanzine.coys.models.login.UserToken;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.chat3.ChatData;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by a on 23.11.17.
 */

public class CurrentUserDao implements CurrentUserDaoI {
    public static final String LOG_TAG = ChatData.LOG_TAG + "CurrentUserDao";

    @Override
    public String getUserTokenId() {

        String currentUserUidFbAuth = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Log.i(LOG_TAG, "currentUserUidFbAuth =" + currentUserUidFbAuth);

        UserToken currentUserToken = SharedPrefs.getUserToken();
        String currentUserUid = currentUserToken.getUserId();

        //Log.i(LOG_TAG, "currentUserUidApiToken =" + currentUserUid);

        return currentUserUid;
    }
}
