package com.fanzine.chat3.model.dao;


import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;

import com.fanzine.coys.App;
import com.fanzine.coys.R;
import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.pojo.FbChannel;
import com.fanzine.chat3.model.pojo.FbUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.kelvinapps.rxfirebase.DataSnapshotMapper;
import com.kelvinapps.rxfirebase.RxFirebaseDatabase;

import java.io.IOException;

import rx.Observable;

public class UserDao implements UserDaoI {
    public static final String LOG_TAG = ChatData.LOG_TAG + "UserDao";

    @Override
    public Observable<FbUser> isGunnerUserExist(String phone) {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();

        Query query = database.child(ChatData.FB_ROOT).child(ChatData.FB_USER)
                .orderByChild(ChatData.FB_USER_PHONE).equalTo(phone);

        return RxFirebaseDatabase
                .observeSingleValueEvent(query, DataSnapshotMapper.listOf(FbUser.class))
                .flatMap(userList -> {
                    FbUser user = null;
                    if (userList.size() > 0) {
                        user = userList.get(0);
                    }
                    return Observable.just(user);
                });
    }

    @Override
    public Observable<FbUser> getUserByUid(String uid) {

        DatabaseReference database = FirebaseDatabase.getInstance().getReference();

        Query query = database.child(ChatData.FB_ROOT).child(ChatData.FB_USER).child(uid);

        return RxFirebaseDatabase
                .observeSingleValueEvent(query, DataSnapshotMapper.of(FbUser.class)).doOnError(throwable -> throwable.printStackTrace());
    }

    @Override
    public void saveUserToFb(FbUser fbUser) {
        //TODO
    }

    @Override
    public Bitmap getContactPhotoByPhone(String number) {
        Log.i(LOG_TAG, "String number=" + number);

        Context context = App.getContext();
        ContentResolver contentResolver = context.getContentResolver();
        String contactId = null;
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.PHOTO_URI, ContactsContract.PhoneLookup._ID};

        Cursor cursor =
                contentResolver.query(
                        uri,
                        projection,
                        null,
                        null,
                        null);
        String photoUri = null;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));

                photoUri = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));

            }
            cursor.close();
        }
        Bitmap bitmap = null;
        if (photoUri != null) {
            try {
                bitmap = MediaStore.Images.Media
                        .getBitmap(context.getContentResolver(),
                                Uri.parse(photoUri));

            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            bitmap = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.chat_default_avatar);
        }
        return bitmap;

    }


    //
    // user 1, user2
    //
    //1. Получили list user_channel для  user 1
    //
    //2. Взяли user_channel 1 по полю channel получили все user_channel
    //
    //3. Обошли list и проверили на совпадение с user2 uid
    @Override
    public Observable<FbChannel> getJointDialog(String user1Uid, String user2Uid) {
        return null;
    }
}
