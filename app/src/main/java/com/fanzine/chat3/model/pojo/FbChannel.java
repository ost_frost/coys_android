package com.fanzine.chat3.model.pojo;

import com.google.firebase.database.IgnoreExtraProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

@IgnoreExtraProperties
public class FbChannel {
    public static final String TYPE_ONE_TO_ONE = "one-to-one";
    public static final String TYPE_ONE_TO_MANY = "one-to-many";

    public String uid;

    @SerializedName("name")
    @Expose
    public String name;

    public String owner;
    public Long timestamp;

    @SerializedName("type")
    @Expose
    public String type;

    public Map<String, FbMessageAtachment> logo;
}
