package com.fanzine.chat3.model.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class FbUser implements Parcelable {

    public String nickname;
    public String phone;
    public String email;
    public String firstName;
    public String lastName;
    public String avatar;
    public String uid;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nickname);
        dest.writeString(this.phone);
        dest.writeString(this.email);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.avatar);
        dest.writeString(this.uid);
    }

    public FbUser() {
    }

    protected FbUser(Parcel in) {
        this.nickname = in.readString();
        this.phone = in.readString();
        this.email = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.avatar = in.readString();
        this.uid = in.readString();
    }

    public static final Creator<FbUser> CREATOR = new Creator<FbUser>() {
        @Override
        public FbUser createFromParcel(Parcel source) {
            return new FbUser(source);
        }

        @Override
        public FbUser[] newArray(int size) {
            return new FbUser[size];
        }
    };

    public String getFullName() {
        return firstName + " " + lastName;
    }
}
