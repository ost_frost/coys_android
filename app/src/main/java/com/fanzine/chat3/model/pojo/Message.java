package com.fanzine.chat3.model.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mbp on 12/18/17.
 */

public class Message implements Parcelable {

    public static final String TYPE_PICTURE = "photo";
    public static final String TYPE_SYSTEM = "system";
    public static final String TYPE_LOCATION = "location";
    public static final String TYPE_DEFAULT = "default";

    public static final String TYPE_ONE_TO_ONE = "one-to-one";

    @SerializedName("channel_id")
    private long channelId;

    @SerializedName("text")
    private String text;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("user_name")
    private String userName;

    @SerializedName("type")
    private String type;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("attachment")
    private Attachment attachment;

    public void setText(String text) {
        this.text = text;
    }

    public Message(String text, String userId, String type) {
        this.text = text;
        this.userId = userId;
        this.type = type;
    }

    public String getDate() {
        if (createdAt != null) return createdAt;
        return "";
    }

    public long getChannelId() {
        return channelId;
    }

    public String getText() {
        return text;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getType() {
        return type;
    }

    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    public void setChannelId(long channelId) {
        this.channelId = channelId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.channelId);
        dest.writeString(this.text);
        dest.writeString(this.userId);
        dest.writeString(this.userName);
        dest.writeString(this.type);
        dest.writeString(this.createdAt);
        dest.writeParcelable(this.attachment, flags);
    }

    protected Message(Parcel in) {
        this.channelId = in.readLong();
        this.text = in.readString();
        this.userId = in.readString();
        this.userName = in.readString();
        this.type = in.readString();
        this.createdAt = in.readString();
        this.attachment = in.readParcelable(Attachment.class.getClassLoader());
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel source) {
            return new Message(source);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };
}
