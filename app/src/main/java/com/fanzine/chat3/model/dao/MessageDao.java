package com.fanzine.chat3.model.dao;

import android.net.Uri;
import android.webkit.MimeTypeMap;

import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.pojo.FbMessage;
import com.fanzine.chat3.model.pojo.Message;
import com.fanzine.chat3.model.pojo.MessagePost;
import com.fanzine.chat3.networking.Service;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.kelvinapps.rxfirebase.DataSnapshotMapper;
import com.kelvinapps.rxfirebase.RxFirebaseDatabase;
import com.kelvinapps.rxfirebase.RxFirebaseStorage;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import rx.Observable;
import rx.Single;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MessageDao implements MessageDaoI {
    public static final String LOG_TAG = ChatData.LOG_TAG + "MessageDao";

    private Service service;

    public MessageDao() {
        this.service = new Service();
    }

    @Override
    public Observable<Message> postMessage(MessagePost message) {
        return service.postMessage(message)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }


    @Override
    public Observable<List<Message>> getMessages(String channelId) {
        return service.getMessages(channelId).map(messageList -> {
            Collections.reverse(messageList);
            return messageList;
        });
    }


    public Observable<Message> postMessageImage(String imagePath, String msgText, String channelUid) {

        Observable<Message> obsMsg = service.postMessageImage(imagePath)
                .flatMap(atachUid -> {

                    MessagePost message = new MessagePost(msgText, channelUid, MessagePost.TYPE_PICTURE, atachUid);
                    return service.postMessage(message);
                })
                .subscribeOn(Schedulers.newThread());
        return obsMsg;
    }


    public Observable<Message> postMessageLocation(String channelUid, Double latitude, Double longitude, String msgText) {

        Observable<Message> obsMsg = service.postMessageLocation(latitude, longitude)
                .flatMap(atachUid -> {

                    MessagePost message = new MessagePost(msgText, channelUid, MessagePost.TYPE_LOCATION, atachUid);
                    return service.postMessage(message);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread());
        return obsMsg;
    }

    //OLD------------------------------------------------------------------
    @Override
    public void sendTextMessage(FbMessage fbMessage, String chanelUid) {

        String currentUserUid = new CurrentUserDao().getUserTokenId();

        fbMessage.timestamp = System.currentTimeMillis();
        fbMessage.user = currentUserUid;

        DatabaseReference database = FirebaseDatabase.getInstance().getReference();

        String messageUid = database.child(ChatData.FB_ROOT).child(ChatData.FB_MESSAGES).child(chanelUid).push().getKey();

        database.child(ChatData.FB_ROOT).child(ChatData.FB_MESSAGES).child(chanelUid).child(messageUid).setValue(fbMessage);

        database.child(ChatData.FB_ROOT).child(ChatData.FB_CHANEL).child(chanelUid).child("timestamp").setValue(System.currentTimeMillis());
    }

    @Override
    public Observable<byte[]> getFileFromFBStorage(String fileUid) {

        StorageReference storageRef = FirebaseStorage.getInstance().getReference().child(ChatData.FB_STORAGE_ROOT);

        return RxFirebaseStorage.getBytes(storageRef.child(fileUid), 1024 * 100);
    }

    public Single<String> saveFileToFBStorage(String path) {

        StorageReference storageRef = FirebaseStorage.getInstance().getReference().child(ChatData.FB_STORAGE_ROOT)
                .child("messages");

        String fileUid = UUID.randomUUID().toString();
        StorageReference riversRef = storageRef.child(fileUid);

        Single<String> single = Single.create(
                singleSubscriber -> {
                    Uri uri = Uri.fromFile(new File(path));
                    riversRef.putFile(uri).addOnCompleteListener(task -> singleSubscriber.onSuccess(fileUid));
                }
        );

        /*Single<String> single =
                Single.just(path)
                        .map(pathFile -> {

                            return fileUid;
                        });*/

        return single;

    }

    public Observable<FbMessage> getLastChannelMessage(String channelUid) {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();

        Query query = database.child(ChatData.FB_ROOT).child(ChatData.FB_MESSAGES).child(channelUid).limitToLast(1);

        Observable<FbMessage> obs = RxFirebaseDatabase
                .observeSingleValueEvent(query, DataSnapshotMapper.listOf(FbMessage.class))
                .flatMap(userList -> {
                    FbMessage user = null;
                    if (userList.size() > 0) {
                        user = userList.get(0);
                    }
                    return Observable.just(user);
                });
        return obs;
    }

    @Override
    public Observable<FbMessage> clearChannel(String channelUid) {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();

        Query query = database.child(ChatData.FB_ROOT).child(ChatData.FB_MESSAGES).child(channelUid).limitToLast(1);

        Observable<FbMessage> obs = RxFirebaseDatabase
                .observeSingleValueEvent(query, DataSnapshotMapper.listOf(FbMessage.class))
                .flatMap(userList -> {
                    FbMessage user = null;
                    if (userList.size() > 0) {
                        user = userList.get(0);
                    }
                    return Observable.just(user);
                });
        return obs;
    }

    private String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }
}
