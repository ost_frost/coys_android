package com.fanzine.chat3.model.dao;

import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ChannelPost;
import com.fanzine.chat3.model.pojo.ChatUser;
import com.fanzine.chat3.model.pojo.FbChannel;
import com.fanzine.chat3.model.pojo.FbUser;
import com.fanzine.chat3.model.pojo.FbUserChannel;

import java.util.List;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.Single;


public interface ChannelDaoI {

    Single<FbChannel> addChannelForUsers(List<String> listUserUid, String chanelName);

    Observable<Channel> getChannelByUid(String uid);

    Observable<List<ChatUser>> getChannelUser(String dialogId);

    @Deprecated
    Observable<List<FbUser>> getChannelUsers(String channelUid);

    Single<FbChannel> addChannelForUsers(List<String> listUserUid, FbChannel FbChannel);

    Single<String> addUserToChannel(String channelUid, FbUser user);


    Single<Boolean> renameChannel(String channelUid, String channelName);

    //TODO
    Single<Boolean> removeUserFromChannel(String userUid, String channelUid);

    Single<Boolean> setChannelName(String channelName);

    Observable<List<FbUserChannel>> findDialogByUserUid(String uid);

    Single<String> saveDialogIconFBStorage(String path);

    Single<Boolean> setChannelImage(String channelUid, String image);

    Single<Boolean> removeChannelMessages(String channelUid);

    Observable<Channel> postChannel(ChannelPost channelPost);

    Observable<List<Channel>> getDialogs();

    Observable<ResponseBody> leaveChannel(String channelId);

    Observable<ResponseBody> clearMessages(String channelId);

    Observable<ResponseBody> deleteChannel(String channelId);
}
