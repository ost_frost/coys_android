package com.fanzine.chat3.model.dao;

import com.fanzine.chat3.model.pojo.FbMessage;
import com.fanzine.chat3.model.pojo.Message;
import com.fanzine.chat3.model.pojo.MessagePost;

import java.util.List;

import rx.Observable;
import rx.Single;

/**
 * Created by a on 14.11.17.
 */

public interface MessageDaoI {

    Observable<Message> postMessage(MessagePost message);

    void sendTextMessage(FbMessage fbMessage, String chanelUid);

    Observable<List<Message>> getMessages(String channelId);

    Observable<byte[]> getFileFromFBStorage(String filePatch);

    Single<String> saveFileToFBStorage(String uri);

    Observable<FbMessage> getLastChannelMessage(String channelUid);

    Observable<FbMessage> clearChannel(String channelUid);
}
