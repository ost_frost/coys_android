package com.fanzine.chat3.model.dao;

import android.database.Cursor;
import android.provider.ContactsContract;

import com.fanzine.coys.App;
import com.fanzine.chat3.model.pojo.ChatUser;
import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.networking.Service;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mbp on 12/15/17.
 */

public class ContactPhoneDao {

    public Observable<List<ContactPhone>> getPhoneContacts() {

        String[] projection = new String[]{
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.PHOTO_URI,
                ContactsContract.Contacts.PHOTO_ID,
                ContactsContract.PhoneLookup._ID,
                ContactsContract.Contacts.Photo.PHOTO,
                ContactsContract.Contacts.HAS_PHONE_NUMBER,
                ContactsContract.CommonDataKinds.Phone.NUMBER
        };

        return Observable.create(new Observable.OnSubscribe<List<ContactPhone>>() {
            @Override
            public void call(Subscriber<? super List<ContactPhone>> subscriber) {
                try {
                    subscriber.onNext(readCursor(query()));
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }

            Cursor query() {
                return App.getContext().getContentResolver()
                        .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                projection,
                                null,
                                null,
                                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            }

            List<ContactPhone> readCursor(Cursor cursor) {
                List<ContactPhone> list = new ArrayList<>();

                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    ContactPhone contact = new ContactPhone();

                    contact.name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    contact.imageUri = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
                    contact.phone = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                    //todo: send only numeric number, without special symbols
                    if (isValid(contact.phone)) {
                        list.add(contact);
                    }
                }
                return list;
            }
        }).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<ChatUser>> addParticipantsToChannel(String channelId, List<ContactPhone> contactPhones) {
        Service service = new Service();

        List<String> ids = new ArrayList<>();

        for (ContactPhone contactPhone : contactPhones) {
            ids.add(contactPhone.id);
        }

        return service.addParticipantsToChannel(channelId, ids);
    }

    private static boolean isValid(String str) {
        return str.matches("^[+]?\\d{5,14}$");
    }

}
