package com.fanzine.chat3.model.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mbp on 12/18/17.
 */

public class MessagePost {

    public static final String TYPE_DEFAULT = "default";
    public static final String TYPE_PICTURE = "photo";
    public static final String TYPE_LOCATION = "location";


    @SerializedName("text")
    private String text;

    @SerializedName("channel_id")
    private String channelId;

    @SerializedName("type")
    private String type;

    @SerializedName("attachable_id")
    private Long attachableId;

    public MessagePost(String text, String channelId, String type) {
        this.text = text;
        this.channelId = channelId;
        this.type = type;
    }

    public MessagePost(String text, String channelId, String type, long attachableId) {
        this.text = text;
        this.channelId = channelId;
        this.type = type;
        this.attachableId = attachableId;
    }

    public String getChannelId() {
        return channelId;
    }
}
