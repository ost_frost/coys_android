package com.fanzine.chat3.model.dao;

import android.util.Log;

import com.fanzine.coys.api.ApiRequest;
import com.fanzine.chat3.ChatData;

import rx.Observable;

/**
 * Created by a on 24.11.17
 */
public class UserInvitationDao implements UserInvitationDaoI {
    public static final String LOG_TAG = ChatData.LOG_TAG + "CurrentUserDao";

    @Override
    public Observable<Boolean> inviteUser(String phone) {

        Observable<Boolean> obs = ApiRequest.getInstance().getApi().sendInviteSms(phone)
                .flatMap(responseBody -> {
                    String responseStr = responseBody.toString();
                    Log.i(LOG_TAG, "sendInviteSms responce=" + responseStr);

                    if (responseStr.equals("success")) {
                        return Observable.just(true);
                    } else {
                        return Observable.just(false);
                    }
                });
        return obs;
    }
}

