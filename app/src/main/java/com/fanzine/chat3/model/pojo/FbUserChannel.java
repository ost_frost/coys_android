package com.fanzine.chat3.model.pojo;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class FbUserChannel {
    public static final String TYPE_ONE_TO_ONE = "one-to-one";
    public static final String TYPE_ONE_TO_MANY = "one-to-many";

    public static final String STATUS_ACTIVE= "active";
    public static final String STATUS_ARCHIVE = "archive";

    public String uid;
    public String channel;
    public String type;
    public String user;

    public String status;

    private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
