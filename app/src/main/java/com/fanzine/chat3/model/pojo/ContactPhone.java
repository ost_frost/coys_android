package com.fanzine.chat3.model.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by a on 15.11.17.
 */

public class ContactPhone implements Parcelable {

    public static final String AVAILABLE = "true";

    @SerializedName("phone")
    public String phone;

    @SerializedName("name")
    public String name;

    @SerializedName("uid")
    @Expose(serialize = false)
    public String id;

    @SerializedName("status")
    @Expose(serialize = false)
    public  String status;

    public transient String imageUri;

    public transient boolean isHeader = false;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.phone);
        dest.writeString(this.name);
        dest.writeString(this.id);
        dest.writeString(this.status);
    }

    public ContactPhone() {
    }

    protected ContactPhone(Parcel in) {
        this.phone = in.readString();
        this.name = in.readString();
        this.id = in.readString();
        this.status = in.readString();
    }

    public static final Parcelable.Creator<ContactPhone> CREATOR = new Parcelable.Creator<ContactPhone>() {
        @Override
        public ContactPhone createFromParcel(Parcel source) {
            return new ContactPhone(source);
        }

        @Override
        public ContactPhone[] newArray(int size) {
            return new ContactPhone[size];
        }
    };
}
