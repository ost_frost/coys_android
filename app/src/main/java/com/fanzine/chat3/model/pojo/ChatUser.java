package com.fanzine.chat3.model.pojo;

/**
 * Created by a on 20.12.17
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatUser implements Parcelable {

    @SerializedName("uid")
    @Expose
    public String uid;

    @SerializedName("first_name")
    @Expose
    public String firstName;

    @SerializedName("last_name")
    @Expose
    public String lastName;

    @SerializedName("profile_image")
    @Expose
    public String profileImage;

    public String getFullName() {
        return firstName + " " + lastName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        ChatUser other = (ChatUser) obj;
        return uid.equals(other.uid);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uid);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.profileImage);
    }

    public ChatUser() {
    }

    protected ChatUser(Parcel in) {
        this.uid = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.profileImage = in.readString();
    }

    public static final Parcelable.Creator<ChatUser> CREATOR = new Parcelable.Creator<ChatUser>() {
        @Override
        public ChatUser createFromParcel(Parcel source) {
            return new ChatUser(source);
        }

        @Override
        public ChatUser[] newArray(int size) {
            return new ChatUser[size];
        }
    };
}
