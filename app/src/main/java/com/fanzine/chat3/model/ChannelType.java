package com.fanzine.chat3.model;

/**
 * Created by mbp on 12/18/17.
 */

public interface ChannelType {

    String ONE_TO_ONE = "one-to-one";

    String ONE_TO_MANY = "one-to-many";
}
