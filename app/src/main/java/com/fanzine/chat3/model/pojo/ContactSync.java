package com.fanzine.chat3.model.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mbp on 12/15/17.
 */

public class ContactSync {

    @SerializedName("contacts")
    public List<ContactPhone> contacts;

    public ContactSync(List<ContactPhone> contacts) {
        this.contacts = contacts;
    }
}
