package com.fanzine.chat3.model.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mbp on 12/18/17.
 */

public class ChannelPost {

    @SerializedName("name")
    private String name;

    @SerializedName("type")
    private String type;

    @SerializedName("user_ids")
    private String[] userIds;

    public ChannelPost(String name, String type, String[] userIds) {
        this.name = name;
        this.type = type;
        this.userIds = userIds;
    }
}
