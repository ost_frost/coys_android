package com.fanzine.chat3.model.dao;

public interface CurrentUserDaoI {

   public String getUserTokenId();
}
