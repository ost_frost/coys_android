package com.fanzine.chat3.model.dao;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ChannelPost;
import com.fanzine.chat3.model.pojo.ChatUser;
import com.fanzine.chat3.model.pojo.FbChannel;
import com.fanzine.chat3.model.pojo.FbMessageAtachment;
import com.fanzine.chat3.model.pojo.FbUser;
import com.fanzine.chat3.model.pojo.FbUserChannel;
import com.fanzine.chat3.networking.Responce.PostDialogImageResponce;
import com.fanzine.chat3.networking.Service;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kelvinapps.rxfirebase.DataSnapshotMapper;
import com.kelvinapps.rxfirebase.RxFirebaseDatabase;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.Single;
import rx.schedulers.Schedulers;

public class ChannelDao implements ChannelDaoI {
    public static final String LOG_TAG = ChatData.LOG_TAG + "ChannelDao";

    private Service service;

    public ChannelDao() {
        service = new Service();
    }

    @Override
    public Observable<Channel> postChannel(ChannelPost channelPost) {
        return service.postDialog(channelPost).filter(channel -> channel != null);
    }

    @Override
    public Observable<Channel> getChannelByUid(String  uid) {
        return service.getDialog(uid);
    }

    public Observable<List<Channel>> getDialogs() {
        return service.getDialogs().filter(channels -> channels != null);
    }

    public Observable<List<ChatUser>> getChannelUser(String dialogId) {
        return service.getDialogUser(dialogId).filter(channels -> channels != null);
    }

    public Observable<ResponseBody> deleteDialogUser(String channelId, List<String> users) {
        return service.deleteDialogUser(channelId, users);
    }

    public Observable<PostDialogImageResponce> postDialogImage(String channelId, String imagePath) {

        Observable<PostDialogImageResponce> obsMsg = service.postDialogImage(channelId, imagePath)
                .subscribeOn(Schedulers.newThread());
        return obsMsg;
    }


    // OLD-------------------------------------
    @Deprecated
    @Override
    public Single<FbChannel> addChannelForUsers(List<String> listUserUid, String chanelName) {

        //String currentUserUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        //String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        CurrentUserDaoI currentUserDao = new CurrentUserDao();
        String userUid = currentUserDao.getUserTokenId();

        //add current user to user list
        listUserUid.add(userUid);

        Single<FbChannel> single = Single.just(userUid)
                .map(currentUserUid -> {
                    String channelUid = database.child(ChatData.FB_ROOT).child(ChatData.FB_CHANEL).push().getKey();
                    return channelUid;
                })
                .map(channelUid -> {
                    FbChannel fbChannel = new FbChannel();
                    fbChannel.uid = channelUid;
                    fbChannel.owner = userUid;
                    fbChannel.timestamp = System.currentTimeMillis();
                    fbChannel.name = chanelName;

                    if (listUserUid.size() == 2) {
                        fbChannel.type = FbChannel.TYPE_ONE_TO_ONE;
                        fbChannel.owner = userUid;
                    } else fbChannel.type = FbChannel.TYPE_ONE_TO_MANY;

                    return fbChannel;
                })
                .map(fbChannel -> {
                    database.child(ChatData.FB_ROOT).child(ChatData.FB_CHANEL)
                            .child(fbChannel.uid).setValue(fbChannel);
                    return fbChannel;
                })
                .map(fbChannel -> {

                            for (String uid : listUserUid) {
                                FbUserChannel fbUserChannel = new FbUserChannel();
                                fbUserChannel.channel = fbChannel.uid;
                                fbUserChannel.user = uid;
                                if (listUserUid.size() == 2) {
                                    fbUserChannel.type = FbUserChannel.TYPE_ONE_TO_ONE;

                                } else {
                                    fbUserChannel.type = FbUserChannel.TYPE_ONE_TO_MANY;
                                }
                                String userChannelUid = uid + fbChannel.uid;
                                //String userChannelUid = database.child(ChatData.FB_ROOT).child(ChatData.FB_USER_CHANEL).push().getKey();
                                fbUserChannel.uid = userChannelUid;

                                database.child(ChatData.FB_ROOT).child(ChatData.FB_USER_CHANEL)
                                        .child(userChannelUid).setValue(fbUserChannel);
                            }

                            return fbChannel;

                        }
                );
        return single;
    }

    @Override
    @Deprecated
    public Observable<List<FbUser>> getChannelUsers(String chanelUid) {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();

        Query query = database.child(ChatData.FB_ROOT).child(ChatData.FB_USER_CHANEL)
                .orderByChild(ChatData.CHANNEL_UID).equalTo(chanelUid);

        UserDaoI userDao = new UserDao();

        Observable<List<FbUser>> hhh = RxFirebaseDatabase
                .observeSingleValueEvent(query, DataSnapshotMapper.listOf(FbUserChannel.class))
                .flatMap(Observable::from)
                .filter(fbUserChannel -> fbUserChannel != null)
                .filter(fbUserChannel -> fbUserChannel.user != null)
                .flatMap(fbUserChannel -> {
                    Observable<FbUser> hh = userDao.getUserByUid(fbUserChannel.user);
                    return hh;
                })
                .toList();

        return hhh;
    }

    @Override
    @Deprecated
    public Single<FbChannel> addChannelForUsers(List<String> listUserUid, FbChannel fbChannel) {

        DatabaseReference database = FirebaseDatabase.getInstance().getReference();

        CurrentUserDaoI currentUserDao = new CurrentUserDao();
        String userUid = currentUserDao.getUserTokenId();

        //add current user to user list
        listUserUid.add(userUid);

        Single<FbChannel> single = Single.just(userUid)
                .map(currentUserUid -> {
                    String channelUid = database.child(ChatData.FB_ROOT).child(ChatData.FB_CHANEL).push().getKey();
                    return channelUid;
                })
                .map(channelUid -> {
                    fbChannel.uid = channelUid;
                    fbChannel.owner = userUid;
                    fbChannel.timestamp = System.currentTimeMillis();

                    if (listUserUid.size() == 2) {
                        fbChannel.type = FbChannel.TYPE_ONE_TO_ONE;
                        fbChannel.owner = userUid;
                    } else fbChannel.type = FbChannel.TYPE_ONE_TO_MANY;

                    return fbChannel;
                })
                .map(fbChannel2 -> {
                    database.child(ChatData.FB_ROOT).child(ChatData.FB_CHANEL)
                            .child(fbChannel2.uid).setValue(fbChannel2);
                    return fbChannel2;
                })
                .map(fbChannel3 -> {

                            for (String uid : listUserUid) {
                                FbUserChannel fbUserChannel = new FbUserChannel();
                                fbUserChannel.channel = fbChannel3.uid;
                                fbUserChannel.user = uid;
                                if (listUserUid.size() == 2) {
                                    fbUserChannel.type = FbUserChannel.TYPE_ONE_TO_ONE;

                                } else {
                                    fbUserChannel.type = FbUserChannel.TYPE_ONE_TO_MANY;
                                }

                                String userChannelUid = database.child(ChatData.FB_ROOT).child(ChatData.FB_USER_CHANEL).push().getKey();
                                fbUserChannel.uid = userChannelUid;

                                database.child(ChatData.FB_ROOT).child(ChatData.FB_USER_CHANEL)
                                        .child(userChannelUid).setValue(fbUserChannel);
                            }

                            return fbChannel3;

                        }
                );
        return single;
    }

    @Override
    @Deprecated
    public Single<String> addUserToChannel(final String channelUid, FbUser user) {

        Observable<List<FbUser>> observableChannelExistUsers = getChannelUsers(channelUid);

        Observable<String> single = observableChannelExistUsers
                .flatMap(fb -> {

                            try {

                                Log.i(LOG_TAG, "fb.size()=" + String.valueOf(fb.size()));
                                for (FbUser fdUs : fb) {
                                    if (fdUs.uid.equals(user.uid)) {
                                        return Observable.just(null);
                                    }

                                }

                            } catch (NullPointerException e) {

                                e.printStackTrace();
                                return Observable.just(null);

                            }

                            return Observable.just(user);
                        }

                ).flatMap(fbUser -> {
                    Log.i(LOG_TAG, "fbUser=" + fbUser);

                    if (fbUser != null) {
                        FbUserChannel fbUserChannel = new FbUserChannel();
                        fbUserChannel.channel = channelUid;
                        fbUserChannel.type = FbUserChannel.TYPE_ONE_TO_MANY;
                        fbUserChannel.user = user.uid;


                        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
                        //String userChannelUid = database.child(ChatData.FB_ROOT).child(ChatData.FB_USER_CHANEL).push().getKey();
                        String userChannelUid = user.uid + channelUid;

                        fbUserChannel.uid = userChannelUid;

                        Task<Void> query = database.child(ChatData.FB_ROOT).child(ChatData.FB_USER_CHANEL).child(userChannelUid).setValue(fbUserChannel);
                    }

                    return Observable.just(channelUid);
                })
                .defaultIfEmpty(channelUid);


        return single.toSingle();
    }


    @Deprecated
    public Observable<List<FbUserChannel>> findDialogByUserUid(String uid) {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        Query reference = database.child(ChatData.FB_ROOT).child("users_channels").
                orderByChild("user").equalTo(uid);

        Observable<List<FbUserChannel>> result = RxFirebaseDatabase.observeSingleValueEvent(
                reference, DataSnapshotMapper.listOf(FbUserChannel.class));
        return result;
    }

    @Override
    @Deprecated
    public Single<Boolean> renameChannel(String channelUid, String channelName) {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        Task<Void> tyu = database.child(ChatData.FB_ROOT).child(ChatData.FB_CHANEL).child(channelUid).child("name").setValue(channelName);

        Single<Boolean> kkkk = Single.create(
                singleSubscriber -> {
                    tyu.addOnSuccessListener(aVoid -> singleSubscriber.onSuccess(true));
                });

        return kkkk;
    }


    //TODO
    @Override
    public Single<Boolean> removeUserFromChannel(String userUid, String channelUid) {
        return null;
    }

    //TODO
    @Override
    public Single<Boolean> setChannelName(String channelName) {
        return null;
    }

    @Override
    @Deprecated
    public Single<String> saveDialogIconFBStorage(String path) {

        //StorageReference storageRef = FirebaseStorage.getInstance().getReference().child(ChatData.FB_STORAGE_ROOT);
        StorageReference storageRef = FirebaseStorage.getInstance().getReference().child(ChatData.FB_STORAGE_ROOT)
                .child("messages");

        String fileUid = UUID.randomUUID().toString();
        StorageReference riversRef = storageRef.child(fileUid);

        Single<String> booleanSingleImageSave = Single.create(
                singleSubscriber -> {
                    Uri uri = Uri.fromFile(new File(path));
                    UploadTask jjj = riversRef.putFile(uri);
                    jjj.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                            singleSubscriber.onSuccess(true);
                        }
                    });

                }).flatMap(ret -> {

            return Single.just(fileUid);
        });

        return booleanSingleImageSave;
    }

    @Override
    @Deprecated
    public Single<Boolean> setChannelImage(String channelUid, String image) {

        Log.i(LOG_TAG, "setChannelImage" + image);

        Single<String> resultImageSaveSingle = saveDialogIconFBStorage(image);

        DatabaseReference database = FirebaseDatabase.getInstance().getReference();


        Single<Boolean> fff = resultImageSaveSingle
                .flatMap(uid -> {
                    FbMessageAtachment atachment = new FbMessageAtachment();

                    Map<String, FbMessageAtachment> logo = new HashMap<>();
                    logo.put(uid, atachment);
                    atachment.type = "image/jpeg";


                    Task<Void> query = database.child(ChatData.FB_ROOT).child(ChatData.FB_CHANEL).child(channelUid).child("logo").setValue(logo);

                    Single<Boolean> kkkk = Single.create(
                            singleSubscriber -> {
                                query.addOnCompleteListener(task -> singleSubscriber.onSuccess(true));
                            });

                    return kkkk;
                });

        return fff;
    }

    @Override
    @Deprecated
    public Single<Boolean> removeChannelMessages(String channelUid) {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        database.child(ChatData.FB_ROOT).child(ChatData.FB_MESSAGES).child(channelUid).setValue(null);

        return Single.just(true);
    }

    @Override
    public Observable<ResponseBody> leaveChannel(String channelId) {
        return service.leaveChannel(channelId);
    }

    @Override
    public Observable<ResponseBody> clearMessages(String channelId) {
        return service.deleteChannelMessages(channelId);
    }

    @Override
    public Observable<ResponseBody> deleteChannel(String channelId) {
        return service.deleteChannel(channelId);
    }
}
