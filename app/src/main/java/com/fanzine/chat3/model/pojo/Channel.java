package com.fanzine.chat3.model.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.fanzine.coys.utils.TimeDateUtils;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mbp on 12/18/17.
 */

public class Channel implements Parcelable {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("owner")
    private String owner;

    @SerializedName("type")
    private String type;

    @SerializedName("image_path")
    private String imagePath;

    @SerializedName("last_message")
    private Message lastMessage;

    @SerializedName("left")
    private Boolean left = false;

    @SerializedName("created_at")
    private String date;

    public Channel() {
    }

    public boolean isOwner(String ownerId) {
        return owner.equals(ownerId);
    }

    public boolean isOwnerActive(String ownerId) {
        return  owner.equals(ownerId) && !getLeft();
    }

    public String formatCreateTime() {
        return TimeDateUtils.convert(date, "yyyy-MM-dd HH:mm:ss", "dd MMMM yyyy");
    }


    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getOwner() {
        return owner;
    }

    public String getType() {
        return type;
    }

    public String getImagePath() {
        return imagePath;
    }

    public Message getLastMessage() {
        return lastMessage;
    }

    public void updateName(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setLastMessage(Message lastMessage) {
        this.lastMessage = lastMessage;
    }

    public Boolean getLeft() {
        return left;
    }

    public String getDate() {
        return date;
    }

    public void setLeft(Boolean left) {
        this.left = left;
    }

    public boolean isActive() {
        return !getLeft();
    }

    public boolean isNotActive() {
        return getLeft();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.owner);
        dest.writeString(this.type);
        dest.writeString(this.imagePath);
        dest.writeParcelable(this.lastMessage, flags);
        dest.writeValue(this.left);
        dest.writeString(this.date);
    }

    protected Channel(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.owner = in.readString();
        this.type = in.readString();
        this.imagePath = in.readString();
        this.lastMessage = in.readParcelable(Message.class.getClassLoader());
        this.left = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.date = in.readString();
    }

    public static final Creator<Channel> CREATOR = new Creator<Channel>() {
        @Override
        public Channel createFromParcel(Parcel source) {
            return new Channel(source);
        }

        @Override
        public Channel[] newArray(int size) {
            return new Channel[size];
        }
    };
}
