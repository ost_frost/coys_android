package com.fanzine.chat3.model.dao;

import rx.Observable;

/**
 * Created by a on 24.11.17.
 */

public interface UserInvitationDaoI {
    Observable<Boolean> inviteUser(String phone);
}
