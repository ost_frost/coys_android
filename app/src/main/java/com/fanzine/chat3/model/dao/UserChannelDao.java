package com.fanzine.chat3.model.dao;

import android.util.Log;

import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.pojo.FbUser;
import com.fanzine.chat3.model.pojo.FbUserChannel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.kelvinapps.rxfirebase.DataSnapshotMapper;
import com.kelvinapps.rxfirebase.RxFirebaseDatabase;

import java.util.List;

import rx.Observable;
import rx.Single;

/**
 * Created by mbp on 11/28/17
 */

public class UserChannelDao implements UserChannelDaoI {
    private static final String LOG_TAG = ChatData.LOG_TAG + "UserChannelDao";

    @Override
    public Observable<List<FbUser>> getFbUsers(String chanelUid) {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();

        Query query = database.child(ChatData.FB_ROOT).child(ChatData.FB_USER_CHANEL)
                .orderByChild(ChatData.CHANNEL_UID).equalTo(chanelUid);

        UserDao userDao = new UserDao();

        return RxFirebaseDatabase
                .observeSingleValueEvent(query, DataSnapshotMapper.listOf(FbUserChannel.class))
                .flatMap(Observable::from)
                .flatMap(fbUserChannel -> userDao.getUserByUid(fbUserChannel.user))
                .toList();
    }

    @Override
    public Single<Boolean> removeUserFromChannel(String userChannelUid) {

        DatabaseReference database = FirebaseDatabase.getInstance().getReference();

        database.child(ChatData.FB_ROOT).child(ChatData.FB_USER_CHANEL).child(userChannelUid).setValue(null);

        return Single.just(true);
    }

    @Deprecated
    @Override
    public Single<Boolean> removeUserFromChannel(String userUid, String channelUid) {

        DatabaseReference database = FirebaseDatabase.getInstance().getReference();

        database.child(ChatData.FB_ROOT).child(ChatData.FB_USER_CHANEL)
                .orderByChild(ChatData.CHANNEL_UID).equalTo(channelUid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String userChannelUid = snapshot.getKey();

                    database.child(ChatData.FB_ROOT).child(ChatData.FB_USER_CHANEL)
                            .child(userChannelUid).setValue(null);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return Single.just(true);
    }

    @Override
    public Observable<List<FbUserChannel>> getUserChannelByChannel(String chanelUid) {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();

        Query query = database.child(ChatData.FB_ROOT).child(ChatData.FB_USER_CHANEL)
                .orderByChild(ChatData.CHANNEL_UID).equalTo(chanelUid);

        return RxFirebaseDatabase
                .observeSingleValueEvent(query, DataSnapshotMapper.listOf(FbUserChannel.class));
    }

    @Override
    public void leaveDialog(String userUid, String chanelUid) {

        DatabaseReference database = FirebaseDatabase.getInstance().getReference();

        String channelUid = userUid + chanelUid;
        Log.i(LOG_TAG, "channelUid=" + channelUid);

        database.child(ChatData.FB_ROOT).child(ChatData.FB_USER_CHANEL).child(channelUid).child("status").setValue(FbUserChannel.STATUS_ARCHIVE);
    }

    @Override
    public Observable<FbUserChannel> getUserChannelByUid(String userChannelUid) {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();

        Query query = database.child(ChatData.FB_ROOT).child(ChatData.FB_USER_CHANEL).child(userChannelUid);

        Observable<FbUserChannel> obs = RxFirebaseDatabase
                .observeSingleValueEvent(query, FbUserChannel.class);

        return obs;
    }
}
