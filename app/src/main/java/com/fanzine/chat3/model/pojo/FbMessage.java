package com.fanzine.chat3.model.pojo;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.Map;

@IgnoreExtraProperties
public final class FbMessage {
    public static final String TYPE_DEFAULT = "message_type_default";
    public static final String TYPE_PICTURE = "message_type_picture";
    public static final String TYPE_VIDEO = "message_type_video";
    public static final String TYPE_FILE = "message_type_file";

    public String uid;
    public String user;
    public String type;
    public long timestamp;
    public String message;

    public Map<String, FbMessageAtachment> attachments;
}