package com.fanzine.chat3.utils;

import com.fanzine.chat3.model.pojo.FbUser;

/**
 * Created by a on 03.12.17
 */
public class FbUserUtil {

    public static String getFullName(FbUser fbUser) {

        String fullName = "";
        if (fbUser != null) {
            if (fbUser.firstName != null) {

                fullName = fullName.concat(fbUser.firstName);
                fullName = fullName.concat(" ");
            }

            if (fbUser.lastName != null) {
                fullName = fullName.concat(fbUser.lastName);
            }
        }

        return fullName;
    }
}
