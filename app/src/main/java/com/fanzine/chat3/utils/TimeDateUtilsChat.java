package com.fanzine.chat3.utils;

import android.util.Log;

import com.fanzine.chat3.ChatData;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class TimeDateUtilsChat {

    private static final String dateFormat = "HH:mm:ss";

    // "date": "2017-12-19 09:48:41.000000",
    public static String convertUtcToLocalTimeDialog(String timeStr) {
        final String dateFormat = "yyyy-MM-dd HH:mm:ss";
        //final String dateFormat = "yyyy-MM-dd hh:mm:ss";

        if (timeStr != null && timeStr.length() > 0) {

            DateTimeFormatter inputFormatter = DateTimeFormat.forPattern(dateFormat)
                    .withZone(DateTimeZone.UTC);

            DateTime parsedDateTime = inputFormatter.parseDateTime(timeStr);

            long parsedMils = parsedDateTime.getMillis();

            String timeAgo = getTimeAgo(parsedMils);

            return timeAgo;
        } else return "";
    }

    public static DateTime convertUtcToDateTime(String timeStr) {
        final String dateFormat = "yyyy-MM-dd HH:mm:ss";
        //final String dateFormat = "yyyy-MM-dd hh:mm:ss";

        if (timeStr != null && timeStr.length() > 0) {

            DateTimeFormatter inputFormatter = DateTimeFormat.forPattern(dateFormat)
                    .withZone(DateTimeZone.UTC);

            DateTime parsedDateTime = inputFormatter.parseDateTime(timeStr);



            return parsedDateTime;
        }
        return  null;
    }


    //time for chat 12.56
    public static String convertTimestampToLocalTimeDayMon(Long timestamp) {

        if (timestamp != null && timestamp > 0) {

            DateTimeFormatter inputFormatter = DateTimeFormat.forPattern(dateFormat)
                    .withZone(DateTimeZone.UTC);

            DateTime parsed = new DateTime(timestamp);

            String dateFormat = "HH:mm";


            DateTimeFormatter outputFormatter = DateTimeFormat.forPattern(dateFormat)
                    .withZone(DateTimeZone.getDefault());

            Log.i(LOG_TAG, "DateTimeZone.getDefault() =" + DateTimeZone.getDefault());

            String formattedDate = outputFormatter.print(parsed);

            Log.i(LOG_TAG, "localTime =" + formattedDate);

            return formattedDate;
        } else return "";
    }

    // 26 May 2017
    public static String convertTimestampToLocalTime(Long timestamp) {

        if (timestamp != null && timestamp > 0) {

            DateTimeFormatter inputFormatter = DateTimeFormat.forPattern(dateFormat)
                    .withZone(DateTimeZone.UTC);

            DateTime parsed = new DateTime(timestamp);

            String dateFormat = "dd MMM yyyy";


            DateTimeFormatter outputFormatter = DateTimeFormat.forPattern(dateFormat)
                    .withZone(DateTimeZone.getDefault());

            Log.i(LOG_TAG, "DateTimeZone.getDefault() =" + DateTimeZone.getDefault());

            String formattedDate = outputFormatter.print(parsed);

            Log.i(LOG_TAG, "localTime =" + formattedDate);

            return formattedDate;
        } else return "";
    }

    public static String convertUtcToLocalTime2(String timeStr) {

        if (timeStr != null && timeStr.length() > 0) {

            DateTimeFormatter inputFormatter = DateTimeFormat.forPattern(dateFormat)
                    .withZone(DateTimeZone.UTC);

            DateTime parsed = inputFormatter.parseDateTime(timeStr);

            DateTimeFormatter outputFormatter = DateTimeFormat.forPattern(dateFormat)
                    .withZone(DateTimeZone.getDefault());

            Log.i(LOG_TAG, "DateTimeZone.getDefault() =" + DateTimeZone.getDefault());

            String formattedDate = outputFormatter.print(parsed);

            Log.i(LOG_TAG, "utcTime =" + timeStr);

            Log.i(LOG_TAG, "localTime =" + formattedDate);

            return formattedDate;
        } else return "";
    }

    public static String convertUtcToLocalTime(String timeStr) {

        if (timeStr != null && timeStr.length() > 0) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date myDate = null;
            try {
                myDate = simpleDateFormat.parse(timeStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            String strDefaultTimeZone = TimeZone.getDefault().getDisplayName(false, TimeZone.SHORT);

            simpleDateFormat.setTimeZone(TimeZone.getTimeZone(strDefaultTimeZone));
            String formattedDate = simpleDateFormat.format(myDate);

            return formattedDate;
        } else return "";
    }


    public static final String LOG_TAG = ChatData.LOG_TAG + "CurrentUserDao";

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return "";
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "1 min";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " mins";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "1 hr";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hrs";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "1 day";
        } else {
            return diff / DAY_MILLIS + " days";
        }
    }


}
