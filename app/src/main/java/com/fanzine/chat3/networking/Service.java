package com.fanzine.chat3.networking;

import com.fanzine.coys.api.ApiInterface;
import com.fanzine.coys.api.ApiRequest;
import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ChannelPost;
import com.fanzine.chat3.model.pojo.ChatUser;
import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.model.pojo.ContactSync;
import com.fanzine.chat3.model.pojo.Message;
import com.fanzine.chat3.model.pojo.MessagePost;
import com.fanzine.chat3.model.pojo.RenameGroup;
import com.fanzine.chat3.networking.Responce.DialogNameChangeRequestResponce;
import com.fanzine.chat3.networking.Responce.PostDialogImageResponce;
import com.fanzine.chat3.networking.Responce.PostMsgImageResponce;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by a on 14.12.17
 */

public class Service {
    public static final String LOG_TAG = ChatData.LOG_TAG + "Service";


    private ApiInterface api;

    public Service() {
        api = ApiRequest.getInstance().getApi();
    }

    public rx.Observable<List<Channel>> getDialogs() {
        return api.getChatDialogs()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public rx.Observable<Channel> getDialog(String uid) {
        return api.getChannelByuid(uid)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public rx.Observable<Channel> postDialog(ChannelPost channelPost) {
        return api.postDialog(channelPost)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<ChatUser>> getDialogUser(String dialogId) {
        return api.getChannelUser(dialogId)
                .subscribeOn(Schedulers.newThread());
    }

    public Observable<ResponseBody> deleteDialogUser(String channelId, List<String> ids) {

        return api.deleteChannelUser(channelId,ids )
                .subscribeOn(Schedulers.newThread());
    }

    public Observable<DialogNameChangeRequestResponce> renameDialog(String dialogId, String name) {
        DialogNameChangeRequestResponce changeNameRequest = new DialogNameChangeRequestResponce();
        changeNameRequest.name = name;
        return api.renameChannel(dialogId, changeNameRequest)
                .subscribeOn(Schedulers.newThread());
    }


    //TODO
    public Observable<PostDialogImageResponce> postDialogImage(String channelId, String path) {

        File file = new File(path);
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/jpeg"), file);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("content", "image.jpg", reqFile);

        Observable<PostDialogImageResponce> obs = api.postDialogImage(channelId, body);
        return obs;
    }


    public rx.Observable<List<ContactPhone>> syncContacts(ContactSync contactSync) {
        return api.syncContatcs(contactSync)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public rx.Observable<Message> postMessage(MessagePost message) {
        return api.postMessage(message.getChannelId(), message)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }


    public rx.Observable<List<Message>> getMessages(String channelId) {
        return api.getMessages(channelId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public rx.Observable<RenameGroup> renameGroup(String channelId, RenameGroup renameGroup) {
        return api.renameGroup(channelId, renameGroup)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Integer> postMessageImage(String path) {

        //iOS name: "content", fileName: "image.jpg", mimeType: "image/jpeg")
        File file = new File(path);
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/jpeg"), file);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("content", "image.jpg", reqFile);

        String type = "photo";
        Observable<Integer> obs = api.postMessageImage(type, body)
                .subscribeOn(Schedulers.newThread())
                .flatMap(responseBody -> {
                    String responseBodyStr = null;
                    try {
                        responseBodyStr = responseBody.string();
                    } catch (IOException e) {
                        Observable.error(e);
                    }

                    PostMsgImageResponce resp = new Gson().fromJson(responseBodyStr, PostMsgImageResponce.class);
                    return Observable.just(resp.attachmentId);
                })

                .observeOn(AndroidSchedulers.mainThread());
        return obs;
    }


    public Observable<Integer> postMessageLocation(Double latitude, Double longitude) {

        //String latitudeStr = String.valueOf(latitude);

        //String longitudeStr = String.valueOf(longitude);
        //latitude= Double.valueOf(36.000000);
        //longitude=Double.valueOf(36.000000);

        DecimalFormat numberFormat = new DecimalFormat("#.000000");
        String latitudeStr = numberFormat.format(latitude);
        String longitudeStr = numberFormat.format(longitude);

        Observable<Integer> obs = api.postMessageLocation(latitudeStr, longitudeStr)
                .subscribeOn(Schedulers.newThread())
                .flatMap(responseBody -> {
                    String responseBodyStr = null;
                    try {
                        responseBodyStr = responseBody.string();
                    } catch (IOException e) {
                        Observable.error(e);
                    }

                    PostMsgImageResponce resp = new Gson().fromJson(responseBodyStr, PostMsgImageResponce.class);
                    return Observable.just(resp.attachmentId);
                });
        return obs;
    }


    public Observable<ResponseBody> leaveChannel(String channelId) {
        return api.leaveChannel(channelId).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> deleteChannel(String channelId) {
        return api.deleteChannel(channelId).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> deleteChannelMessages(String channelId) {
        return api.deleteMessagesFromChannel(channelId).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<ChatUser>> addParticipantsToChannel(String channelId, List<String> ids) {
        return api.addParticipantsToChannel(channelId, ids).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
