package com.fanzine.chat3.networking.Responce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by a on 20.12.17
 */

public class DialogNameChangeRequestResponce {

    @SerializedName("name")
    @Expose
    public String name;

}
