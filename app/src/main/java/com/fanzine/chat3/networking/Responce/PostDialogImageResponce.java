package com.fanzine.chat3.networking.Responce;

/**
 * Created by a on 21.12.17
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostDialogImageResponce {

    @SerializedName("image_path")
    @Expose
    public String imagePath;

}