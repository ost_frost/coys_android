package com.fanzine.chat3.presenter.messages;

import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.view.fragment.MessageListI;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by a on 13.11.17
 */

public interface MessageListPresenterI {

    void bindDialog(Channel channel);

    void bindView(MessageListI view);

    void unBindView();

    void onButtonSendClick(String msgText);

    void onImageDialogButtonSendClick(String msgText, String imagePath);

    void leave(String channelUid);

    void delete(String channelUid);

    void clearMessages(String channelId);

    void onResume();

    void onLocationSend(LatLng latLng, String msgText);

    void loadPartcipants(String channelUid);

    void loadChannel(String uid);
}
