package com.fanzine.chat3.presenter.info;

import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.view.fragment.ContactInfoI;
import com.fanzine.chat3.view.fragment.group.GroupInfoI;

/**
 * Created by a on 27.11.17
 */

public interface ContactInfoPresenterI {
    void bindView(ContactInfoI view);

    void unbindView();

    void bindChannel(Channel channel);

    void onButtonClearChatClick();

    void onButtonLeaveDialog();

    void setChannelName(String title);

    void setCreatedTime(String text);

    void navigateBack();
}
