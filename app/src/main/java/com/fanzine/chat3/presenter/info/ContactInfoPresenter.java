package com.fanzine.chat3.presenter.info;

import com.fanzine.coys.models.login.UserToken;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.coys.utils.TimeDateUtils;
import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.dao.ChannelDao;
import com.fanzine.chat3.model.dao.ChannelDaoI;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.view.fragment.ContactInfoI;

import okhttp3.ResponseBody;
import rx.Subscriber;

/**
 * Created by a on 27.11.17
 */

public class ContactInfoPresenter implements ContactInfoPresenterI {
    public static final String LOG_TAG = ChatData.LOG_TAG + "ContactInfo";

    private ContactInfoI view;
    private Channel channel;

    @Override
    public void bindView(ContactInfoI view) {
        this.view = view;
    }

    @Override
    public void unbindView() {

    }

    @Override
    public void bindChannel(Channel channel) {
        this.channel = channel;

        view.setChannelName(channel.getName());
        view.setImage(channel.getImagePath());
        view.setOwnerName(channel.getOwner());

        String time = TimeDateUtils.convert(channel.getDate(), "yyyy-MM-dd HH:mm:ss", "MMMM HH:mm");

        view.setCreatedTime(time);

        UserToken user = SharedPrefs.getUserToken();

        if (user != null && user.getUserId().equals(channel.getOwner())) {
            view.setOwnerName("You");
        } else {
            view.setOwnerName(channel.getName());
        }
    }

    @Override
    public void onButtonClearChatClick() {
        view.showProgressBar();

        ChannelDaoI channelDao = new ChannelDao();
        channelDao.clearMessages(channel.getId()).subscribe(responseBody -> {
            view.hideProgressBar();
            view.navigateBack();
        });
    }

    @Override
    public void setChannelName(String title) {
        view.setChannelName(title);
    }

    @Override
    public void navigateBack() {
        view.navigateBack();
    }


    @Override
    public void onButtonLeaveDialog() {
        view.showProgressBar();
        ChannelDaoI channelDaoI = new ChannelDao();
        channelDaoI.deleteChannel(channel.getId()).subscribe(new Subscriber<ResponseBody>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(ResponseBody responseBody) {
                view.hideProgressBar();
                view.openChatsList();
            }
        });
    }

    @Override
    public void setCreatedTime(String text) {
        view.setCreatedTime(text);
    }
}
