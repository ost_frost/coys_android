package com.fanzine.chat3.presenter.contacts;

import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.view.holder.contacts.base.ContactsPhoneHolderI;

/**
 * Created by a on 17.11.17.
 */

public interface ContactsPhoneHolderPresenterI {
    void bindView(ContactsPhoneHolderI view);

    void bindContact(ContactPhone contact);

    void onContactItemClick();

}
