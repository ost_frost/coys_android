package com.fanzine.chat3.presenter.participants;

import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.view.holder.ParticipantHolderI;

/**
 * Created by mbp on 11/17/17.
 */

public interface ParticipantHolderPresenterI {

    void bindView(ParticipantHolderI view);

    void bindParticipant(ContactPhone user);
}
