package com.fanzine.chat3.presenter.group.info;

import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.view.fragment.group.GroupInfoI;

/**
 * Created by a on 27.11.17
 */

public interface GroupInfoPresenterI {
    void bindView(GroupInfoI view);

    void unbindView();

    void bindChannel(Channel channel);

    void onButtonImageSendClick(String data);

    void onButtonClearChatClick();

    void onButtonLeaveDialog();

    void onButtonRenameGroupClick(String name);
}
