package com.fanzine.chat3.presenter.messages;

import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.ChannelType;
import com.fanzine.chat3.model.dao.ChannelDao;
import com.fanzine.chat3.model.dao.ChannelDaoI;
import com.fanzine.chat3.model.dao.MessageDao;
import com.fanzine.chat3.model.dao.MessageDaoI;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ChatUser;
import com.fanzine.chat3.model.pojo.Message;
import com.fanzine.chat3.model.pojo.MessagePost;
import com.fanzine.chat3.view.fragment.MessageListI;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by a: on 13.11.17
 */
public class MessageListPresenter implements MessageListPresenterI {

    private MessageListI view;
    private static final String LOG_TAG = ChatData.LOG_TAG + "ChatDialogMessageP";
    private String channelUid;

    @Override
    public void bindDialog(Channel channel) {
        channelUid = channel.getId();

        MessageDaoI messageDaoI = new MessageDao();

        view.showProgressBar();

        messageDaoI.getMessages(channelUid).subscribe(messageList -> {
                    view.hideProgressBar();
                    view.showMessagesList(messageList);
                },
                Throwable::printStackTrace);

        view.setGroupImage(channel.getImagePath());


        if (channel.getType().equals(ChannelType.ONE_TO_MANY)) {
            view.addGroupEditClickListener();
        }

        if (channel.getLeft())
            view.blockMessagePanel();

        view.setTitle(channel.getName());
    }

    public void loadChannel(String uid) {
        ChannelDaoI channelDao = new ChannelDao();

        channelDao.getChannelByUid(uid).subscribe(new Subscriber<Channel>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Channel channel) {
                view.onChannelReady(channel);
            }
        });
    }

    @Override
    public void bindView(MessageListI view) {
        this.view = view;
    }

    @Override
    public void unBindView() {

    }

    @Override
    public void leave(String channelUid) {
        ChannelDaoI channelDao = new ChannelDao();
        channelDao.leaveChannel(channelUid).subscribe(responseBody -> {

        }, Throwable::printStackTrace);
    }

    @Override
    public void delete(String channelUid) {
        ChannelDaoI channelDao = new ChannelDao();
        channelDao.deleteChannel(channelUid).subscribe(responseBody -> {
        }, Throwable::printStackTrace);
    }

    @Override
    public void clearMessages(String channelId) {
        ChannelDao channelDao = new ChannelDao();
        channelDao.clearMessages(channelId).subscribe(responseBody -> {
        }, Throwable::printStackTrace);
    }

    @Override
    public void onResume() {
        MessageDaoI messageDaoI = new MessageDao();

        messageDaoI.getMessages(channelUid).subscribe(messageList -> view.showMessagesList(messageList),
                Throwable::printStackTrace);
    }


    @Override
    public void onButtonSendClick(String msgText) {

        view.hideKeyboard();
        view.showBottomMenu();
        view.showProgressBar();

        if (msgText != null && msgText.length() > 1) {
            MessageDaoI messageDao = new MessageDao();
            MessagePost message = new MessagePost(msgText, channelUid, MessagePost.TYPE_DEFAULT);

            messageDao.postMessage(message).subscribe(sentMessage -> {
                        view.hideProgressBar();
                        view.showSentMessage(sentMessage);
                    }
                    , Throwable::printStackTrace);
            view.setMessageText("");
        }
    }

    public void onLocationSend(LatLng latLng, String msgText) {

        view.showProgressBar();

        Observable<Message> obs = new MessageDao().postMessageLocation(channelUid, latLng.latitude,
                latLng.longitude, msgText);

        obs.subscribe(new Subscriber<Message>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                int i = 1;
            }

            @Override
            public void onNext(Message message) {
                view.hideProgressBar();
                view.showSentMessage(message);
            }
        });
    }

    @Override
    public void onImageDialogButtonSendClick(String msgText, String imagePath) {

        view.showProgressBar();

        Observable<Message> obs = new MessageDao().postMessageImage(imagePath, msgText, channelUid);
        obs.observeOn(AndroidSchedulers.mainThread())
                .subscribe(message -> {
                            view.hideProgressBar();
                            view.showSentMessage(message);
                        },
                        throwable -> throwable.printStackTrace());

    }

    @Override
    public void loadPartcipants(String channelUid) {
        ChannelDaoI channelDaoI = new ChannelDao();

        channelDaoI.getChannelUser(channelUid)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<List<ChatUser>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(List<ChatUser> chatUsers) {
                        if (chatUsers == null)
                            view.setTitle("");
                        else if (chatUsers.size() > 2)
                            view.showParticipants(chatUsers);
                    }
                });
    }
}
