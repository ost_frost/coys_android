package com.fanzine.chat3.presenter.dialog;

import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.utils.TimeDateUtilsChat;
import com.fanzine.chat3.view.holder.dialog.DialogHolderI;

/**
 * Created by a on 18.11.17.
 */
public class DialogHolderPresenter implements DialogHolderPresenterI {

    private DialogHolderI view;

    @Override
    public void bindView(DialogHolderI view) {
        this.view = view;
    }

    @Override
    public void bindChannel(Channel channel) {

        view.setChannelName(channel.getName());
        view.setImage(channel.getImagePath());

        if (channel.getLastMessage() != null) {
            view.setLastMessage(channel.getLastMessage().getText());

            String lastMessageTime = channel.getLastMessage().getDate();
            String lastMessageTimeAgo = TimeDateUtilsChat.convertUtcToLocalTimeDialog(lastMessageTime);
            view.setTimeAgo(lastMessageTimeAgo);

        }
    }
}
