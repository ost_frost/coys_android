package com.fanzine.chat3.presenter.participants;

import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.view.holder.ParticipantHolderI;

/**
 * Created by mbp on 11/17/17.
 */

public class ParticipantHolderPresenter implements ParticipantHolderPresenterI {

    private ParticipantHolderI view;

    @Override
    public void bindParticipant(ContactPhone user) {
        view.setName(user.name);
    }

    @Override
    public void bindView(ParticipantHolderI view) {
        this.view = view;
    }
}
