package com.fanzine.chat3.presenter.contacts.list;

import com.fanzine.chat3.model.dao.ContactPhoneDao;
import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.model.pojo.ContactSync;
import com.fanzine.chat3.networking.Service;
import com.fanzine.chat3.view.fragment.contacts.base.ContactsPhoneListI;

import java.util.Collections;
import java.util.List;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by a on 15.11.17
 */
public class ContactsPhoneListPresenter implements ContactsPhoneListPresenterI {

    private Service service;
    private ContactsPhoneListI view;

    public ContactsPhoneListPresenter(Service service) {
        this.service = service;
    }

    @Override
    public void bindView(ContactsPhoneListI view) {
        this.view = view;
    }

    @Override
    public void setSearchText(String s) {
        view.setSearchText(s);
    }

    @Override
    public void syncContacts() {

        ContactPhoneDao contactPhoneDao = new ContactPhoneDao();

        Observable<List<ContactPhone>> phoneContacts = contactPhoneDao.getPhoneContacts();

        phoneContacts.flatMap(contactPhones -> {
            ContactSync contactSync = new ContactSync(contactPhones);
            return service.syncContacts(contactSync);
        }).subscribe(new Subscriber<List<ContactPhone>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                view.onError(e.getMessage());
            }

            @Override
            public void onNext(List<ContactPhone> contactPhones) {
                Collections.sort(contactPhones, (contactPhone, t1)
                        -> contactPhone.name.compareToIgnoreCase(t1.name));

                view.showContacts(contactPhones);
            }
        });
    }
}
