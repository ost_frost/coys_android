package com.fanzine.chat3.presenter.messages;

import com.fanzine.chat3.model.pojo.Message;
import com.fanzine.chat3.view.holder.MessageHolderI;

/**
 * Created by a on 09.12.17.
 */

public interface MessageHolderPresenterI {
    void bindView(MessageHolderI view);

    void bindMessage(Message message, Message nextMessage, int viewType);
}
