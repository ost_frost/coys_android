package com.fanzine.chat3.presenter.group.info;

import com.fanzine.coys.models.login.UserToken;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.dao.ChannelDao;
import com.fanzine.chat3.model.dao.ChannelDaoI;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ChatUser;
import com.fanzine.chat3.networking.Responce.DialogNameChangeRequestResponce;
import com.fanzine.chat3.networking.Responce.PostDialogImageResponce;
import com.fanzine.chat3.networking.Service;
import com.fanzine.chat3.view.fragment.group.GroupInfoI;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by a on 27.11.17
 */

public class GroupInfoPresenter implements GroupInfoPresenterI {
    public static final String LOG_TAG = ChatData.LOG_TAG + "GroupInfoP";

    private GroupInfoI view;
    private Channel channel;
    private UserToken user;

    public GroupInfoPresenter() {
        user = SharedPrefs.getUserToken();
    }

    @Override
    public void bindView(GroupInfoI view) {
        this.view = view;
    }

    @Override
    public void unbindView() {

    }

    @Override
    public void bindChannel(Channel channel) {
        this.channel = channel;

        view.setChannelName(channel.getName());
        view.setGroupImage(channel.getImagePath());
        view.setPersipientInfo(user.getFirstName() + " " + user.getLastName());
        view.showProgressBar();

        view.setCreatedTime(channel.formatCreateTime());

        if (channel.isOwner(user.getUserId()) && channel.isActive()) {
            view.turnOnAdminMode();
        } else {
            view.turnOffAdminMode();
        }

        if (channel.isNotActive()) {
            view.setReadOnlyMode();
        }

        loadParticipants();
    }

    @Override
    public void onButtonImageSendClick(String data) {

        view.showProgressBar();

        ChannelDao channelDao = new ChannelDao();

        Observable<PostDialogImageResponce> obs = channelDao.postDialogImage(channel.getId(), data);
        obs.observeOn(AndroidSchedulers.mainThread())
                .subscribe(postDialogImageResponce -> {
                            view.setGroupImage(postDialogImageResponce.imagePath);
                            view.hideProgressBar();
                        },

                        Throwable::printStackTrace);

        view.dismissSelectImageDialog();
    }

    @Override
    public void onButtonClearChatClick() {
        view.showProgressBar();

        ChannelDaoI channelDao = new ChannelDao();
        channelDao.clearMessages(channel.getId()).subscribe(responseBody -> {
            view.hideProgressBar();
            view.navigateBack();
        });
    }

    @Override
    public void onButtonLeaveDialog() {
        view.showProgressBar();
        ChannelDaoI channelDaoI = new ChannelDao();
        channelDaoI.leaveChannel(channel.getId()).subscribe(new Subscriber<ResponseBody>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(ResponseBody responseBody) {
                view.hideProgressBar();
                view.navigateBack();
            }
        });
    }

    @Override
    public void onButtonRenameGroupClick(String name) {
        Observable<DialogNameChangeRequestResponce> obs = new Service().renameDialog(channel.getId(), name);
        obs.observeOn(AndroidSchedulers.mainThread())
                .subscribe(requestResponce -> view.setChannelName(requestResponce.name),
                        Throwable::printStackTrace);
    }

    private void loadParticipants() {
        new ChannelDao().getChannelUser(channel.getId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> {
                            UserToken userToken = SharedPrefs.getUserToken();
                            List<ChatUser> participants = new ArrayList<>();

                            for ( ChatUser user: list) {
                                if ( !user.uid.equals(userToken.getUserId())){
                                    participants.add(user);
                                }
                            }

                            view.setCount(String.valueOf(participants.size()));
                            view.setParticipantsUserList(participants);

                            boolean isOwnerActive = channel.isOwnerActive(user.getUserId());

                            view.showParticipants(isOwnerActive);
                            view.hideProgressBar();
                        },
                        Throwable::printStackTrace);

    }
}
