package com.fanzine.chat3.presenter.dialog;

import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.view.fragment.dialogs.DialogListI;

import java.util.List;

public interface DialogListPresenterI {

    void onButtonEditClick();

    void onCraetView();

    void onButtonEddContactClick();

    void onButtonNewGroupClick();

    void onButtonCameraClick();

    void onButtonChatsClick();

    void bindView(DialogListI dialogListI);

    void loadDialogs();

    void leaveChannels(List<Channel> channelList);

    void leaveChannel(Channel channel);

    void deleteChannels(List<Channel> channels);

    void setSearchText(String s);
}
