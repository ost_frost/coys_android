package com.fanzine.chat3.presenter.contacts.single;

import com.fanzine.chat3.model.ChannelType;
import com.fanzine.chat3.model.dao.ChannelDao;
import com.fanzine.chat3.model.dao.ChannelDaoI;
import com.fanzine.chat3.model.pojo.ChannelPost;
import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.presenter.contacts.BaseContactsPhoneHolderPresenter;

/**
 * Created by a on 17.11.17
 */
public class ContactsPhoneHolderPresenter extends BaseContactsPhoneHolderPresenter {

    @Override
    public void onContactItemClick() {

        if (contact.status.equals(ContactPhone.AVAILABLE)) {

            ChannelDaoI channelDao = new ChannelDao();

            String name = contact.name;
            String type = ChannelType.ONE_TO_ONE;
            String[] ids = {contact.id};

            ChannelPost channelPost = new ChannelPost(name, type, ids);

            channelDao.postChannel(channelPost).subscribe(channel -> {
                view.showMessagesList(channel);
            }, Throwable::printStackTrace);

        } else {
            sendInviteToNewUser(contact.name);
        }
    }
}
