package com.fanzine.chat3.presenter.group.create;

import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.view.fragment.NewGroupFragmentI;

import java.util.List;

/**
 * Created by a on 23.11.17.
 */

public interface NewGroupFragmentPresenterI {


    void bindView(NewGroupFragmentI view);

    void onButtonImageSendClick(String data);

    void onButtonCraetClick(List<ContactPhone> fbUserList, String imagePath);

    void subjectChanged(String subject, int partcipantsCount);
}
