package com.fanzine.chat3.presenter.participants;

import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.view.fragment.group.AddParticipantFragmentI;

import java.util.List;

/**
 * Created by mbp on 1/22/18.
 */

public interface GroupParticipantsPresenterI {
    void bindView(AddParticipantFragmentI view);

    void bindChannel(Channel channel);

    void unbindView();

    void addParticipantsToChannel(List<ContactPhone> phoneList);
}
