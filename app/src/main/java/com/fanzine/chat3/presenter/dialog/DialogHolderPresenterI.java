package com.fanzine.chat3.presenter.dialog;

import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.view.holder.dialog.DialogHolderI;

/**
 * Created by a on 18.11.17.
 */

public interface DialogHolderPresenterI {
    void bindView(DialogHolderI view);

    void bindChannel(Channel channel);
}
