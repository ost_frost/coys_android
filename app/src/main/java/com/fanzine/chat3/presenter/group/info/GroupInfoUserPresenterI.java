package com.fanzine.chat3.presenter.group.info;

import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ChatUser;
import com.fanzine.chat3.view.holder.GroupInfoUserI;

/**
 * Created by a on 27.11.17.
 */

public interface GroupInfoUserPresenterI {
    void bindView(GroupInfoUserI groupInfoUserHolder);

    void bindDialogUser(ChatUser user);

    void onButtonRemoveUserClick();

    void bindDialog(Channel channel);
}
