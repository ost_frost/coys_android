package com.fanzine.chat3.presenter.group.info;

import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.view.fragment.group.GroupRenameFragmentI;

/**
 * Created by mbp on 12/20/17.
 */

public interface GroupRenamePresenterI {

    void bindView(GroupRenameFragmentI view);

    void bindChannel(Channel channel);

    void rename(String name);
}
