package com.fanzine.chat3.presenter.group.info;

import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.dao.ChannelDao;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ChatUser;
import com.fanzine.chat3.view.holder.GroupInfoUserI;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by a on 27.11.17
 */
public class GroupInfoUserPresenter implements GroupInfoUserPresenterI {
    public static final String LOG_TAG = ChatData.LOG_TAG + "GroupInfoUserP";

    private GroupInfoUserI view;
    private ChatUser user;
    private Channel channel;

    @Override
    public void bindView(GroupInfoUserI view) {

        this.view = view;
    }

    @Override
    public void bindDialogUser(ChatUser user) {

        this.user = user;

        view.setName(user.firstName + " " + user.lastName);

        view.setImage(user.profileImage);
    }

    @Override
    public void onButtonRemoveUserClick() {
        ChannelDao userChannelDao = new ChannelDao();

        List<String> userList = new ArrayList<>();
        userList.add(user.uid);
        userChannelDao.deleteDialogUser(channel.getId(), userList)
                .subscribe(chatUsers -> {
                    view.onPartcipantDeleted(user);
                }, Throwable::printStackTrace);

    }

    @Override
    public void bindDialog(Channel channel) {

        this.channel = channel;
    }
}
