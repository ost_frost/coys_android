package com.fanzine.chat3.presenter.messages;

import android.util.Log;

import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.dao.ChannelDao;
import com.fanzine.chat3.model.dao.ChannelDaoI;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.Message;
import com.fanzine.chat3.utils.TimeDateUtilsChat;
import com.fanzine.chat3.view.holder.MessageHolder;
import com.fanzine.chat3.view.holder.MessageHolderI;

import rx.Subscriber;

/**
 * Created by a on 09.12.17
 */

public class MessageHolderPresenter implements MessageHolderPresenterI {
    public static final String LOG_TAG = ChatData.LOG_TAG + "MessageHolderP";

    private MessageHolderI view;
    private Message message;

    @Override
    public void bindView(MessageHolderI view) {
        this.view = view;
    }

    @Override
    public void bindMessage(Message message, Message nextMessage, int viewType) {
        this.message = message;

        String messageTime = message.getDate();
        String lastMessageTimeAgo = TimeDateUtilsChat.convertUtcToLocalTimeDialog(messageTime);
        view.setTime(lastMessageTimeAgo);

        if (viewType == MessageHolder.MSG_TYPE_IN) {
            setUserName();
        }

        view.setText(message.getText());
        view.setTailVisibility(true);

        if (message.getType().equals(Message.TYPE_PICTURE)) {
            String msgUrl = message.getAttachment().getPath();
            Log.i(LOG_TAG, "msgUrl=" + msgUrl);
            view.setMsgImage(msgUrl);
        }

        if (message.getType().equals(Message.TYPE_LOCATION)) {
            view.setMapVisible();
            view.setMapLocation(message);
        }
    }


    private void setUserName() {
        String title = message.getUserName();
        view.setUserName(title);
    }
}
