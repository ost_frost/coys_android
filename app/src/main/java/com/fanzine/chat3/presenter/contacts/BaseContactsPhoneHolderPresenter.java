package com.fanzine.chat3.presenter.contacts;

import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.dao.UserInvitationDao;
import com.fanzine.chat3.model.dao.UserInvitationDaoI;
import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.view.holder.contacts.base.ContactsPhoneHolderI;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mbp on 12/20/17.
 */

public abstract class BaseContactsPhoneHolderPresenter implements ContactsPhoneHolderPresenterI {
    protected ContactsPhoneHolderI view;
    public static final String LOG_TAG = ChatData.LOG_TAG + "ContactsPhoneHolderP";

    protected ContactPhone contact;

    @Override
    public void bindView(ContactsPhoneHolderI view) {
        this.view = view;
    }

    @Override
    public void bindContact(ContactPhone contact) {
        this.contact = contact;

        view.setName(contact.name);

        if (contact.isHeader) {
            view.showHeader(contact.name);
        }

        if (contact.status.equals(ContactPhone.AVAILABLE)) {
            view.setAvailableStatus();
        } else {
            view.setUnavailableStatus();
        }
    }

    protected void sendInviteToNewUser(String userName) {
        UserInvitationDaoI useInvitationDao = new UserInvitationDao();
        useInvitationDao.inviteUser(contact.phone).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(v -> view.showMessageToUser("Invitation to the user " + userName + " successfully sent")
                        , throwable -> throwable.printStackTrace());
    }
}
