package com.fanzine.chat3.presenter.group.info;

import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.RenameGroup;
import com.fanzine.chat3.networking.Service;
import com.fanzine.chat3.view.fragment.group.GroupRenameFragmentI;

/**
 * Created by mbp on 12/20/17.
 */

public class GroupRenamePresenter implements GroupRenamePresenterI {

    GroupRenameFragmentI view;

    private Service service;
    private Channel channel;

    public GroupRenamePresenter() {
        this.service = new Service();
    }

    @Override
    public void bindView(GroupRenameFragmentI view) {
        this.view = view;
    }

    @Override
    public void bindChannel(Channel channel) {
        this.channel = channel;

        view.setGroupTitle(channel.getName());
    }

    @Override
    public void rename(String name) {

        RenameGroup renameGroup = new RenameGroup(name);

        service.renameGroup(view.getChannel().getId(), renameGroup).subscribe(response -> {
            channel.updateName(name);

            view.updateChannel(channel);
            view.navigateBack();
        });
    }
}
