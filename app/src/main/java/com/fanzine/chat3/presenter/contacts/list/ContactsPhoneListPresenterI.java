package com.fanzine.chat3.presenter.contacts.list;

import com.fanzine.chat3.view.fragment.contacts.base.ContactsPhoneListI;

/**
 * Created by a on 15.11.17
 */

public interface ContactsPhoneListPresenterI {

    void bindView(ContactsPhoneListI view);

    void setSearchText(String s);

    void syncContacts();
}
