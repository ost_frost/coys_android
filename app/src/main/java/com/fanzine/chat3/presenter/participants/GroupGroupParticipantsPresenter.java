package com.fanzine.chat3.presenter.participants;

import com.fanzine.chat3.model.dao.ContactPhoneDao;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ChatUser;
import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.view.fragment.group.AddParticipantFragmentI;

import java.util.List;

import rx.Subscriber;

/**
 * Created by mbp on 1/22/18.
 */

public class GroupGroupParticipantsPresenter implements GroupParticipantsPresenterI {

    private Channel channel;
    private AddParticipantFragmentI view;

    @Override
    public void bindView(AddParticipantFragmentI view) {
        this.view = view;
    }

    @Override
    public void bindChannel(Channel channel) {
        this.channel = channel;
    }

    @Override
    public void unbindView() {

    }

    @Override
    public void addParticipantsToChannel(List<ContactPhone> phoneList) {
        ContactPhoneDao contactPhoneDao = new ContactPhoneDao();

        contactPhoneDao.addParticipantsToChannel(channel.getId(), phoneList)
                .subscribe(new Subscriber<List<ChatUser>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<ChatUser> chatUsers) {
                        view.onParticipantsAdded(chatUsers);
                    }
                });
    }
}
