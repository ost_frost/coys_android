package com.fanzine.chat3.presenter.group.create;

import com.fanzine.chat3.model.ChannelType;
import com.fanzine.chat3.model.dao.ChannelDao;
import com.fanzine.chat3.model.pojo.ChannelPost;
import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.networking.Responce.PostDialogImageResponce;
import com.fanzine.chat3.view.fragment.NewGroupFragmentI;

import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by: a on 23.11.17.
 */
public class NewGroupFragmentPresenter implements NewGroupFragmentPresenterI {
    private NewGroupFragmentI view;

    @Override
    public void bindView(NewGroupFragmentI view) {
        this.view = view;
    }

    @Override
    public void onButtonImageSendClick(String data) {


//        ChannelDao channelDao = new ChannelDao();
//
//
//        ChannelDaoI channelDao = new ChannelDao();
//        Single<String> single = channelDao.saveDialogIconFBStorage(data);
//        single.observeOn(AndroidSchedulers.mainThread())
//                .doOnError(throwable -> throwable.printStackTrace())
//                .subscribe(v -> {
//                    view.setGroupImage(data);
//                });
    }

    @Override
    public void subjectChanged(String subject, int participantsCount) {
        if (subject.length() > 0 && participantsCount > 1) {
            view.makeCreateButtonActive();
        } else {
            view.makeCreateButtonNotActive();
        }
    }

    //todo:: refactor
    @Override
    public void onButtonCraetClick(List<ContactPhone> fbUserList, String imagePath) {

        if (!view.isSubjectValid()) return;

        view.showProgressBar();

        String[] uids = new String[fbUserList.size()];

        for (int i = 0; i < fbUserList.size(); i++) {
            uids[i] = fbUserList.get(i).id;
        }


        ChannelDao channelDao = new ChannelDao();

        String channelTitle = view.getGroupNameText();
        String channelType = ChannelType.ONE_TO_MANY;

        ChannelPost channelPost = new ChannelPost(channelTitle, channelType, uids);


        channelDao.postChannel(channelPost).subscribe(channel -> {

            if (imagePath != null) {

                ChannelDao channelDao2 = new ChannelDao();

                Observable<PostDialogImageResponce> obs = channelDao2.postDialogImage(channel.getId(), imagePath);
                obs.observeOn(AndroidSchedulers.mainThread())
                        .subscribe(postDialogImageResponce -> {
                                    if (postDialogImageResponce != null)
                                        channel.setImagePath(postDialogImageResponce.imagePath);

                                    view.hideProgressBar();
                                    view.runMessagesListFragment(channel);
                                },

                                Throwable::printStackTrace);
            } else {

                view.runMessagesListFragment(channel);
            }
        });
    }
}
