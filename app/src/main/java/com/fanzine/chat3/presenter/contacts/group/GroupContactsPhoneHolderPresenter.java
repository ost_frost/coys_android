package com.fanzine.chat3.presenter.contacts.group;

import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.presenter.contacts.BaseContactsPhoneHolderPresenter;
import com.fanzine.chat3.view.holder.contacts.group.GroupContactsPhoneHolder;

/**
 * Created by mbp on 12/20/17.
 */

public class GroupContactsPhoneHolderPresenter extends BaseContactsPhoneHolderPresenter {


    @Override
    public void onContactItemClick() {

        GroupContactsPhoneHolder holder = ((GroupContactsPhoneHolder) view);

        if ( !contact.status.equals(ContactPhone.AVAILABLE)) {
            sendInviteToNewUser(contact.name);

            return;
        }


        if (!holder.isSelected(contact)) {
            ((GroupContactsPhoneHolder) view).markAsSelected();
        } else {
            ((GroupContactsPhoneHolder) view).markAsNotSelected();
        }

        ((GroupContactsPhoneHolder) view).onContactSelected(contact);
    }
}
