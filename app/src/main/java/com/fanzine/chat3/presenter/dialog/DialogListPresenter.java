package com.fanzine.chat3.presenter.dialog;

import com.fanzine.chat3.model.ChannelType;
import com.fanzine.chat3.model.dao.ChannelDao;
import com.fanzine.chat3.model.dao.ChannelDaoI;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.utils.TimeDateUtilsChat;
import com.fanzine.chat3.view.fragment.dialogs.DialogListI;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import okhttp3.ResponseBody;
import rx.Subscriber;

/**
 * Created by a on 09.11.17.
 */

public class DialogListPresenter implements DialogListPresenterI {

    private DialogListI view;

    private ChannelDaoI channelDao;

    public DialogListPresenter() {
        this.channelDao = new ChannelDao();
    }

    @Override
    public void setSearchText(String s) {
        view.setSearchText(s);
    }

    @Override
    public void deleteChannels(List<Channel> channels) {
        for (Channel channel : channels) {
            this.channelDao.deleteChannel(channel.getId()).subscribe(new Subscriber<ResponseBody>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onNext(ResponseBody responseBody) {

                }
            });
        }
    }

    @Override
    public void loadDialogs() {
        this.channelDao.getDialogs().subscribe(channels -> {

            channels = skipOneToOneDialogsNotActive(channels);

            Collections.sort(channels, (channel, t1) -> {

                if (channel.getLastMessage() != null && t1.getLastMessage() != null) {


                    DateTime date1 = TimeDateUtilsChat.convertUtcToDateTime(channel.getLastMessage().getDate());
                    DateTime date2 = TimeDateUtilsChat.convertUtcToDateTime(t1.getLastMessage().getDate());

                    if (date1.compareTo(date2) == -1)
                        return 1;
                    if (date1.compareTo(date2) == 1)
                        return -1;
                    else
                        return 0;
                }
                return -1;
            });

            view.showDialogs(channels);
        }, Throwable::printStackTrace);
    }


    @Override
    public void leaveChannels(List<Channel> channelList) {
        for (Channel channel : channelList) {
            this.channelDao.leaveChannel(channel.getId()).subscribe(new Subscriber<ResponseBody>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onNext(ResponseBody responseBody) {

                }
            });
        }
    }

    @Override
    public void leaveChannel(Channel channel) {
        this.channelDao.leaveChannel(channel.getId());
    }

    @Override
    public void onButtonEditClick() {

    }

    @Override
    public void onCraetView() {

    }

    @Override
    public void onButtonEddContactClick() {

    }

    @Override
    public void onButtonNewGroupClick() {

    }

    @Override
    public void onButtonCameraClick() {

    }

    @Override
    public void onButtonChatsClick() {

    }

    @Override
    public void bindView(DialogListI dialogListI) {
        this.view = dialogListI;
    }

    private List<Channel> skipOneToOneDialogsNotActive(List<Channel> channels) {
        List<Channel> filtered = new ArrayList<>();

        for (Channel channel : channels) {
            if (!(channel.getType().equals(ChannelType.ONE_TO_ONE) && channel.getLeft()))
                filtered.add(channel);

        }
        return filtered;
    }

}
