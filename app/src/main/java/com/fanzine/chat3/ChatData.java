package com.fanzine.chat3;

public class ChatData {
    public static final String LOG_TAG = "@@";

    public static final String FB_ROOT = "FIREBASE_CHAT_ROOT";
    public static final String FB_STORAGE_ROOT = "FIREBASE_CHAT_ROOT";

    //FB Users

    //FB Messages
    public static final String FB_MESSAGES = "messages";
    public static final String FB_MESSAGES_TIMESTAMP ="timestamp" ;


    public static final String FB_USER_CHANEL = "users_channels";

    public static final String USER_UID = "user";
    public static final String FB_USER = "users";
    public static final String FB_USER_PHONE = "phone";


    public static final String FB_CHANEL = "channels";
    public static final String CHANNEL_UID ="channel" ;
}
