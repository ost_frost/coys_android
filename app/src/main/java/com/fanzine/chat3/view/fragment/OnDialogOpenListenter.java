package com.fanzine.chat3.view.fragment;

import com.fanzine.chat3.model.pojo.Channel;

/**
 * Created by mbp on 12/18/17.
 */

public interface OnDialogOpenListenter {

    void openDialog(Channel channel);
}
