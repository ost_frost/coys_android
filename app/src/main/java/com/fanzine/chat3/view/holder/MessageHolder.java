package com.fanzine.chat3.view.holder;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.pojo.Message;
import com.fanzine.chat3.presenter.messages.MessageHolderPresenter;
import com.fanzine.chat3.presenter.messages.MessageHolderPresenterI;
import com.fanzine.chat3.view.activity.map.MapsActivity;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

//ChannelPost-user holder
public class MessageHolder extends RecyclerView.ViewHolder
        implements MessageHolderI, OnMapReadyCallback {
    public static final String LOG_TAG = ChatData.LOG_TAG + "MessageHolder";

    private Context context;
    private TextView tvTime, tvMessage;
    private ImageView iv, ivTail, ivImageShare;
    private int viewType;
    public static final int MSG_TYPE_IN = 1;
    public static final int MSG_TYPE_OUT = 2;

    public static final int MSG_PICTURE_IN = 3;
    public static final int MSG_PICTURE_OUT = 4;

    public static final int MSG_LOCATION_OUT = 5;
    public static final int MSG_LOCATION_IN = 6;

    public static final int MSG_SYSTEM = 7;


    private Message message;
    private MessageHolderPresenterI presenter;

    MapView mapView;
    GoogleMap map;

    View itemVIew;

    private LatLng mMapLocation;

    public MessageHolder(View itemView, int viewType) {
        super(itemView);
        context = itemView.getContext();
        this.viewType = viewType;

        tvTime = (TextView) itemView.findViewById(R.id.tvTime);

        tvMessage = (TextView) itemView.findViewById(R.id.tvMessage);

        iv = (ImageView) itemView.findViewById(R.id.iv);
        ivTail = (ImageView) itemView.findViewById(R.id.ivTail);

        mapView = (MapView) itemView.findViewById(R.id.mapView);
        ivImageShare = (ImageView) itemView.findViewById(R.id.ivImageShare);

        presenter = new MessageHolderPresenter();
        presenter.bindView(this);

        this.itemVIew = itemView;

        initializeMapView();
    }

    public void bind(Message message, Message nextMessage) {
        this.message = message;

        presenter.bindMessage(message, nextMessage, viewType);

    }

    private void showImagePreviewDialog(String imageUid) {
        //presenter.onButtonImageSendClick(imagePath);
        Dialog imagePreviewDialog = new Dialog(context);

        imagePreviewDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imagePreviewDialog.setContentView(R.layout.dialog_chat_image_viewer);

        View ivImagePreviewClose = imagePreviewDialog.findViewById(R.id.ivImagePreviewClose);
        ivImagePreviewClose.setOnClickListener(view -> {
            imagePreviewDialog.dismiss();
        });

        ImageView ivPreviewImage = (ImageView) imagePreviewDialog.findViewById(R.id.ivPreviewImage);

        Glide.with(context)
                .load(imageUid)
                .into(ivPreviewImage);

        final int width = context.getResources().getDisplayMetrics().widthPixels;
        final int height = context.getResources().getDisplayMetrics().heightPixels;

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(imagePreviewDialog.getWindow().getAttributes());
        lp.width = width + 20;
        lp.height = height + 20;
        imagePreviewDialog.getWindow().setAttributes(lp);
        imagePreviewDialog.getWindow().setBackgroundDrawable(null);
        //imagePreviewDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
        //WindowManager.LayoutParams.MATCH_PARENT);

        imagePreviewDialog.show();
    }

    @Override
    public void setUserName(String userName) {
        TextView tvUserName = (TextView) itemView.findViewById(R.id.tvUserName);
        tvUserName.setText(userName);
    }

    @Override
    public void setTailVisibility(boolean visibility) {
//        if (ivTail == null) return;
//
//        if (visibility) {
//            ivTail.setVisibility(View.VISIBLE);
//        } else {
//            ivTail.setVisibility(View.INVISIBLE);
//        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(itemVIew.getContext());
        map = googleMap;


        if (map != null && mMapLocation != null) {
            // Add a marker for this item and set the camera
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(mMapLocation, 10f));
            map.addMarker(new MarkerOptions().position(mMapLocation));
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            // Set the map type back to normal.
        }
    }

    public GoogleMap getMap() {
        return map;
    }

    public void setMapLocation(Message data) {
        if ( data.getAttachment() == null) return;;

        mMapLocation = data.getAttachment().getLatLng();
    }

    public void setMapVisible() {
        mapView.setVisibility(View.VISIBLE);
    }

    public void setMapGone() {
        mapView.setVisibility(View.GONE);
    }

    private void initializeMapView() {
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    @Override
    public void setImageShare(Uri uri) {
        if (ivImageShare == null) return;

        ivImageShare.setVisibility(View.VISIBLE);
        ivImageShare.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, "Hey view/download this image");

            intent.putExtra(Intent.EXTRA_STREAM, uri);
            intent.setType("image/*");
            context.startActivity(Intent.createChooser(intent, "Share image via..."));
        });
    }

    @Override
    public void setTime(String timeAgo) {
        tvTime.setText(timeAgo);
    }

    @Override
    public void setText(String message) {
        tvMessage.setText(message);
    }

    @Override
    public void setMsgImage(String imageUid) {

        iv.setVisibility(View.VISIBLE);
        ivImageShare.setVisibility(View.VISIBLE);

        Glide.with(context).load(imageUid).into(iv);

        Uri uri = Uri.parse(imageUid);
        setImageShare(uri);

        iv.setOnClickListener(view -> showImagePreviewDialog(imageUid));
    }

    @Override
    public void showMapView(double lat, double lon) {
        iv.setVisibility(View.VISIBLE);

        iv.setImageResource(R.drawable.ic_chat_location);

        iv.setOnClickListener(view -> {
            Intent intent = new Intent(context, MapsActivity.class);

            intent.putExtra(MapsActivity.MODE, MapsActivity.MODE_VIEW_LOCATION);
            intent.putExtra(MapsActivity.LATITUDE, lat);
            intent.putExtra(MapsActivity.LONGITUDE, lon);

            context.startActivity(intent);

        });

    }
}



