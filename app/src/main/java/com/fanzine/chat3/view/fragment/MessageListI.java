package com.fanzine.chat3.view.fragment;

import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ChatUser;
import com.fanzine.chat3.model.pojo.Message;

import java.util.List;

/**
 * Created by a on 13.11.17.
 */

public interface MessageListI extends ProgressBarI {

    void showSentMessage(Message message);

    void setMessageText(String s);

    void setGroupImage(String fbImageUid);

    void addGroupEditClickListener();

    void showParticipants(List<ChatUser> participants);

    void setTitle(String titleText);

    void dismissImageGroupImageSelectDialog();

    void setLayMsgSendPanelVisibility(boolean isVisible);

    void showMessagesList(List<Message> messageList);

    void hideKeyboard();

    void onChannelReady(Channel channel);

    void blockMessagePanel();

    void hideBottomMenu();

    void showBottomMenu();
}
