package com.fanzine.chat3.view.adapter.base;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Created by a on 11/17/17.
 */
public abstract class BaseArrayListAdapter<T, VH extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<VH> {

    private Class<T> mModelClass;
    protected Class<VH> mViewHolderClass;
    private List<T> snapshots;
    protected int mModelLayout;

    public BaseArrayListAdapter(Class<T> modelClass,
                                @LayoutRes int modelLayout,
                                Class<VH> viewHolderClass,
                                List<T> snapshots) {

        mModelClass = modelClass;
        mModelLayout = modelLayout;
        mViewHolderClass = viewHolderClass;
        this.snapshots = snapshots;
    }

    @Override
    public int getItemCount() {
        return snapshots.size();
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        try {
            Constructor<VH> constructor = mViewHolderClass.getConstructor(View.class);
            return constructor.newInstance(view);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }


    public T getItem(int position) {
        return snapshots.get(position);
    }

    public void remove(T item) {
        snapshots.remove(item);
    }

    @Override
    public void onBindViewHolder(VH viewHolder, int position) {
        T model = getItem(position);
        populateViewHolder(viewHolder, model, position);
    }

    @Override
    public int getItemViewType(int position) {
        return mModelLayout;
    }

    protected abstract void populateViewHolder(VH viewHolder, T model, int position);

}
