package com.fanzine.chat3.view.activity.group;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.MainActivity;
import com.fanzine.coys.activities.base.NavigationFragmentActivity;
import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.presenter.group.create.NewGroupFragmentPresenter;
import com.fanzine.chat3.presenter.group.create.NewGroupFragmentPresenterI;
import com.fanzine.chat3.view.activity.messages.MessagesListActivity;
import com.fanzine.chat3.view.adapter.ParticipantsAdapter;
import com.fanzine.chat3.view.fragment.NewGroupFragmentI;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.miguelbcr.ui.rx_paparazzo.RxPaparazzo;
import com.miguelbcr.ui.rx_paparazzo.entities.size.CustomMaxSize;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by: mbp on 11/17/17.
 */
public class GroupNewActivity extends NavigationFragmentActivity implements NewGroupFragmentI {
    public static final String LOG_TAG = ChatData.LOG_TAG + "GroupNewActivity";
    private Context context;

    public static final String PARTICIPANTS = "participants_list";

    private static final int REQUEST_TAKE_IMAGE = 54;

    private List<ContactPhone> fbUserList = new ArrayList<>();

    private ImageView ivAvatar;
    private EditText etSubject;
    private TextView btCreate;
    private View progressBar;

    private NewGroupFragmentPresenterI presenter;

    private String imagePath;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_chat_new_group);

        context = this;

        RecyclerView recycler = (RecyclerView) findViewById(R.id.rvParticipants);

        initParticipants();

        progressBar = findViewById(R.id.progressBar);

        presenter = new NewGroupFragmentPresenter();
        presenter.bindView(this);

        ivAvatar = (ImageView) findViewById(R.id.ivAvatar);

        ivAvatar.setOnClickListener(view -> startTakImageIntent());

        ParticipantsAdapter adapter = new ParticipantsAdapter(this, R.layout.item_participant,
                fbUserList);

        recycler.setLayoutManager(new LinearLayoutManager(context,
                LinearLayoutManager.HORIZONTAL, false));
        recycler.setAdapter(adapter);

        btCreate = (TextView)findViewById(R.id.btCreate);
        btCreate.setOnClickListener(view -> presenter.onButtonCraetClick(fbUserList, imagePath));

        etSubject = (EditText) findViewById(R.id.etSubject);

        etSubject.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                presenter.subjectChanged(charSequence.toString(), adapter.getItemCount());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }


    private void initParticipants() {
        List<ContactPhone> users = getIntent().getParcelableArrayListExtra(PARTICIPANTS);

        if (users != null) {
            fbUserList.addAll(users);
        }
    }

    @Override
    public void onBackPressed() {
        //todo:: handle  correct havigate to back

        Intent chatListIntent = new Intent(this, MainActivity.class);
        chatListIntent.setAction(MainActivity.OPEN_CHAT_FRAGMENT);

        startActivity(chatListIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ( resultCode != RESULT_OK) return;

        if (requestCode == REQUEST_TAKE_IMAGE) {
            List<Image> images = ImagePicker.getImages(data);

            if ( images.size() > 0) {
                imagePath = images.get(0).getPath();

                setGroupImage(imagePath);
            }
        }
    }

    @Override
    public boolean isSubjectValid() {
        return etSubject.length() > 0;
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Deprecated
    private void showImageSelectDialog() {

        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_chat_select_image);

        View layCamera = dialog.findViewById(R.id.tvCamera);
        layCamera.setOnClickListener(view -> RxPaparazzo.takeImage(this)
                .size(new CustomMaxSize(240))
                .usingCamera()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if (response.resultCode() == RESULT_OK) {
                        Log.i(LOG_TAG, "RESULT_OK response.data()=" + response.data());

                        presenter.onButtonImageSendClick(response.data());
                        dialog.dismiss();
                    }

                }));

        View layLibrary = dialog.findViewById(R.id.layLibrary);
        layLibrary.setOnClickListener(view -> RxPaparazzo.takeImage(this)
                .size(new CustomMaxSize(160))
                .usingGallery()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if (response.resultCode() == RESULT_OK) {
                        Log.i(LOG_TAG, "RESULT_OK response.data()=" + response.data());

                        presenter.onButtonImageSendClick(response.data());
                        dialog.dismiss();

                    }
                }));

        View layCancel = dialog.findViewById(R.id.layCancel);
        layCancel.setOnClickListener(view -> dialog.dismiss());

        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            window.getAttributes().gravity = Gravity.BOTTOM;
            final int width = this.getResources().getDisplayMetrics().widthPixels;
            window.setLayout((int) (width * 0.9), ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        dialog.show();
    }

    @Override
    public void setGroupImage(String imagePath) {

        File imgFile = new File(imagePath);

        if (imgFile.exists()) {

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            ivAvatar.setImageBitmap(myBitmap);
        }
    }

    @Override
    public String getGroupNameText() {
        return etSubject.getText().toString();
    }

    @Override
    public void runMessagesListFragment(Channel channel) {
        Intent activity = new Intent(this, MessagesListActivity.class);
        activity.putExtra(MessagesListActivity.CHANNEL_POJO, channel);

        startActivity(activity);
    }

    @Override
    public void makeCreateButtonActive() {
        btCreate.setClickable(true);
        btCreate.setTextColor(ContextCompat.getColor(this, R.color.white));
    }

    @Override
    public void makeCreateButtonNotActive() {
        btCreate.setClickable(false);
        btCreate.setTextColor(ContextCompat.getColor(this, R.color.dark_red));
    }

    private void startTakImageIntent() {
        Intent startImageIntent = ImagePicker.create(this)
                .single()
                .showCamera(true)
                .imageDirectory("Gunners")
                .theme(R.style.AppTheme)
                .enableLog(true)
                .getIntent(context);

        startActivityForResult(startImageIntent, REQUEST_TAKE_IMAGE);
    }
}
