package com.fanzine.chat3.view.adapter.contacts.group;

import android.content.Context;
import android.view.View;

import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.view.adapter.contacts.base.BaseContactsPhoneAdapter;
import com.fanzine.chat3.view.fragment.OnContactSelectListener;
import com.fanzine.chat3.view.holder.contacts.base.BaseContactsPhoneHolder;
import com.fanzine.chat3.view.holder.contacts.group.GroupContactsPhoneHolder;

import java.util.List;

/**
 * Created by mbp on 12/18/17.
 */

public class GroupContactsPhoneAdapter extends BaseContactsPhoneAdapter {

    private OnContactSelectListener onContactSelectListener;

    public GroupContactsPhoneAdapter(List<ContactPhone> contactPhones, Context context, int layoutId,
                                     OnContactSelectListener onContactSelectListener) {
        super(contactPhones, context, layoutId);

        this.onContactSelectListener = onContactSelectListener;
    }

    @Override
    protected BaseContactsPhoneHolder getHolderInstance(View view) {
        return new GroupContactsPhoneHolder(view, onContactSelectListener);
    }

    @Override
    public void onBindViewHolder(BaseContactsPhoneHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        GroupContactsPhoneHolder holder1 = ((GroupContactsPhoneHolder) holder);

        if (holder1.isSelected(getContactPhoneListFiltered().get(position))) {
            holder1.markAsSelected();
        } else {
            ((GroupContactsPhoneHolder) holder).markAsNotSelected();
        }
    }
}
