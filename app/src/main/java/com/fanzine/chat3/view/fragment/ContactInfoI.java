package com.fanzine.chat3.view.fragment;

import com.fanzine.chat3.model.pojo.ChatUser;

import java.util.List;

public interface ContactInfoI {

    void setChannelName(String title);

    void setCreatedTime(String text);

    void showProgressBar();

    void hideProgressBar();

    void navigateBack();

    void setImage(String url);

    void setOwnerName(String name);

    void openChatsList();
}
