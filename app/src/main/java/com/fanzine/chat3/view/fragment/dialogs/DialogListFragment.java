package com.fanzine.chat3.view.fragment.dialogs;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.esafirm.imagepicker.features.camera.DefaultCameraModule;
import com.esafirm.imagepicker.features.camera.OnImageReadyListener;
import com.esafirm.imagepicker.model.Image;
import com.fanzine.coys.R;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.ChannelType;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ImageCapture;
import com.fanzine.chat3.presenter.dialog.DialogListPresenter;
import com.fanzine.chat3.presenter.dialog.DialogListPresenterI;
import com.fanzine.chat3.view.activity.messages.MessagesListActivity;
import com.fanzine.chat3.view.adapter.DialogListAdapter;
import com.fanzine.chat3.view.adapter.DialogListEditAdapter;
import com.fanzine.chat3.view.fragment.OnDialogOpenListenter;
import com.fanzine.chat3.view.fragment.OnDialogSelectListener;
import com.fanzine.chat3.view.fragment.RecentChats;
import com.fanzine.chat3.view.fragment.contacts.group.GroupContactsPhoneListFragment;
import com.fanzine.chat3.view.fragment.contacts.single.ContactsPhoneListFragment;
import com.jakewharton.rxbinding.view.RxView;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DialogListFragment extends Fragment implements
        DialogListI, OnDialogOpenListenter, OnDialogSelectListener, OnImageReadyListener {
    public static final String LOG_TAG = ChatData.LOG_TAG + "ChatDialogList";

    public static final String NEWS_URL = "NEWS_URL";
    private static final String POST_LINK = "POST_LINK";

    private List<Channel> selectedChannes = new ArrayList<>();

    private View btAdd;
    private View btDeleteDialogs;
    private View laySelectChat;
    private View layBottomBar;
    private RecyclerView recyclerView;
    private TextView btEdit;
    private View btNewGroup;
    private DialogListPresenterI presenter;
    private Context context;
    private SearchView etSearch;
    private View btCamera;
    private View btCancel;

    private DialogListAdapter mDialogsListAdapter;
    private DialogListEditAdapter mDialogsListEditAdapter;

    private List<Channel> mDialogs = new ArrayList<>();

    public static Fragment newInstance() {
        Fragment fragment = new DialogListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public static Fragment newInstance(String postLink) {
        Fragment fragment = new DialogListFragment();
        Bundle args = new Bundle();
        args.putString(POST_LINK, postLink);
        fragment.setArguments(args);
        return fragment;
    }


    public static Fragment newInstanceNews(String newsUrl) {
        Fragment fragment = new DialogListFragment();
        Bundle args = new Bundle();
        args.putString(NEWS_URL, newsUrl);
        fragment.setArguments(args);
        return fragment;
    }

    public void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = getActivity().getCurrentFocus();
        if (v == null)
            return;

        if (inputManager != null) {
            inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    public void showKeyboard(SearchView edittext) {
        final InputMethodManager inputMethodManager = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(edittext, InputMethodManager.SHOW_IMPLICIT);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dialog_list, container, false);
        context = getContext();

        recyclerView = (RecyclerView) rootView.findViewById(R.id.fChatList_RvListChats);
        btEdit = (TextView) rootView.findViewById(R.id.fChatList_btEdit);
        laySelectChat = rootView.findViewById(R.id.laySelectChat);
        layBottomBar = rootView.findViewById(R.id.layBottomBar);
        btDeleteDialogs = rootView.findViewById(R.id.btDeleteDialogs);
        btAdd = rootView.findViewById(R.id.btAdd);
        btNewGroup = rootView.findViewById(R.id.btnNewGroup);
        etSearch = (SearchView) rootView.findViewById(R.id.etSearch);
        btCamera = rootView.findViewById(R.id.ivCamera);

        btCancel = rootView.findViewById(R.id.fChatList_btCancel);

        etSearch.setOnClickListener(view -> {
            etSearch.setFocusableInTouchMode(true);
            etSearch.requestFocus();
            etSearch.setActivated(true);
            etSearch.setIconified(false);

            showKeyboard(etSearch);
        });

        btCamera.setOnClickListener(view -> {
            requestCameraPermission();
        });

        RxView.clicks(btAdd).subscribe(
                aVoid -> new RxPermissions(getActivity())
                        .request(Manifest.permission.READ_CONTACTS).subscribe(aBoolean -> {
                            if (aBoolean) {
                                FragmentUtils.changeFragment(getActivity(), R.id.content_frame, ContactsPhoneListFragment.newInstance(), true);
                            } else
                                DialogUtils.showAlertDialog(getContext(), R.string.contactsNeeded);
                        }, throwable -> throwable.printStackTrace()));


        RxView.clicks(btEdit).subscribe(aVoid -> {
            switchMode(true);
        });

        RxView.clicks(btCancel).subscribe(aVoid -> {
            switchMode(false);
        });

        RxView.clicks(btDeleteDialogs).subscribe(aVoid -> {
            showDeleteChatsPopup();
        });

        RxView.clicks(btNewGroup).subscribe(
                aVoid -> new RxPermissions(getActivity())
                        .request(Manifest.permission.READ_CONTACTS).subscribe(aBoolean -> {
                            if (aBoolean) {
                                FragmentUtils.changeFragment(getActivity(), R.id.content_frame, GroupContactsPhoneListFragment.newInstance(), true);
                            } else
                                DialogUtils.showAlertDialog(getContext(), R.string.contactsNeeded);
                        }));


        etSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                presenter.setSearchText(newText);

                return true;
            }
        });

        etSearch.setIconified(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        presenter = new DialogListPresenter();
        presenter.bindView(this);


        return rootView;
    }


    @Override
    public void showDialogs(List<Channel> dialogs) {
        mDialogs.clear();
        mDialogs.addAll(dialogs);

        mDialogsListAdapter =
                new DialogListAdapter(getContext(), R.layout.item_chat_dialog, mDialogs, this);
        mDialogsListEditAdapter =
                new DialogListEditAdapter(getContext(), R.layout.item_chat_dialog_edit, mDialogs, this);

        recyclerView.setAdapter(mDialogsListAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        hideKeyboard();

        if (presenter != null)
            presenter.loadDialogs();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hideKeyboard();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        if (imm != null) {
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }
    }


    @Override
    public void openDialog(Channel channel) {
        Intent activity = new Intent(getContext(), MessagesListActivity.class);

        if (channel.getLeft() && channel.getType().equals(ChannelType.ONE_TO_MANY))
            activity.putExtra(MessagesListActivity.CHANNEL_POJO, channel);
        else
            activity.putExtra(MessagesListActivity.CHANNEL_ID, Long.valueOf(channel.getId()));

        if (getArguments() != null && getArguments().containsKey(NEWS_URL)) {
            activity.putExtra(NEWS_URL, getArguments().getString(NEWS_URL));
        }
        startActivity(activity);
    }

    @Override
    public void addDialog(Channel channel) {
        selectedChannes.add(channel);
    }

    @Override
    public void removeDialog(Channel channel) {
        selectedChannes.remove(channel);
    }

    @Override
    public boolean isSelected(Channel channel) {
        return selectedChannes.contains(channel);
    }

    @Override
    public void setSearchText(String q) {
        mDialogsListAdapter.getFilter().filter(q);
    }

    private void switchMode(boolean isEdit) {
        btCancel.setVisibility(isEdit ? View.VISIBLE : View.GONE);
        laySelectChat.setVisibility(isEdit ? View.VISIBLE : View.GONE);

        btEdit.setVisibility(isEdit ? View.GONE : View.VISIBLE);
        layBottomBar.setVisibility(isEdit ? View.GONE : View.VISIBLE);

        if (isEdit) {
            recyclerView.setAdapter(mDialogsListEditAdapter);
        } else {
            recyclerView.setAdapter(mDialogsListAdapter);
        }
    }

    private void showDeleteChatsPopup() {

        String title = "";
        String message = " Are you sure you want to delete chat ?";

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setTitle(title);
        alertBuilder.setMessage(message);

        alertBuilder.setPositiveButton("Yes", (dialog, arg1) -> {
            deleteChatsFromAdapters();
            switchMode(false);

            presenter.deleteChannels(selectedChannes);
        });
        alertBuilder.setNegativeButton("No", (dialogInterface, i) -> dialogInterface.dismiss());

        alertBuilder.show();
    }

    private void deleteChatsFromAdapters() {
        mDialogs.removeAll(selectedChannes);
        mDialogsListEditAdapter.notifyDataSetChanged();
        mDialogsListAdapter.notifyDataSetChanged();
    }

    DefaultCameraModule cameraModule = new DefaultCameraModule();
    private final static int REQUEST_CAMERA = 5;

    private void showImagePreviewDialog(String imagePath) {
        //presenter.onButtonImageSendClick(imagePath);
        Dialog imagePreviewDialog = new Dialog(getContext());

        imagePreviewDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imagePreviewDialog.setContentView(R.layout.dialog_chat_preview_image);

        EditText etMessageDialogPrevImage = (EditText) imagePreviewDialog.findViewById(R.id.etMessageText);

        View vSendDialogImagePreview = imagePreviewDialog.findViewById(R.id.vSendDialogImagePreview);
        vSendDialogImagePreview.setOnClickListener(view -> {
            String msgText = etMessageDialogPrevImage.getText().toString();
//            presenter.onImageDialogButtonSendClick(msgText, imagePath);
//
//            imagePreviewDialog.dismiss();
            ImageCapture imageCapture = new ImageCapture(msgText, imagePath);

            FragmentUtils.changeFragment(getFragmentManager(), R.id.content_frame,
                    RecentChats.newInstance((ArrayList<Channel>)mDialogs, imageCapture), true);


            imagePreviewDialog.dismiss();
        });


        View ivImagePreviewClose = imagePreviewDialog.findViewById(R.id.ivImagePreviewClose);
        ivImagePreviewClose.setOnClickListener(view -> {
            imagePreviewDialog.dismiss();
        });

        ImageView ivPreviewImage = (ImageView) imagePreviewDialog.findViewById(R.id.ivPreviewImage);
        File imgFile = new File(imagePath);

        if (imgFile.exists()) {
            Observable.fromCallable(() -> BitmapFactory.decodeFile(imgFile.getAbsolutePath()))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(ivPreviewImage::setImageBitmap);
        }

        final int width = this.getResources().getDisplayMetrics().widthPixels;
        final int height = this.getResources().getDisplayMetrics().heightPixels;

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(imagePreviewDialog.getWindow().getAttributes());
        lp.width = width;
        lp.height = height;

        imagePreviewDialog.getWindow().setAttributes(lp);
        imagePreviewDialog.getWindow().setBackgroundDrawable(null);
        imagePreviewDialog.show();
    }

    private void startCameraIntent() {
        Intent startCamersIntent = cameraModule.getCameraIntent(getContext());
        startActivityForResult(startCamersIntent, REQUEST_CAMERA);
    }

    private void requestCameraPermission() {
        new RxPermissions(getActivity()).request(Manifest.permission.CAMERA).subscribe(aBoolean -> {
            if (aBoolean) {
                startCameraIntent();
            }
        }, throwable -> throwable.printStackTrace());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA) {
            cameraModule.getImage(context, data, this);
        }
    }

    @Override
    public void onImageReady(List<Image> list) {
        Observable.just(getFirstImagePath(list))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showImagePreviewDialog);
    }

    private String getFirstImagePath(List<Image> list) {
        if (list != null && list.size() > 0) {
            return list.get(0).getPath();
        }
        return "";
    }
}
