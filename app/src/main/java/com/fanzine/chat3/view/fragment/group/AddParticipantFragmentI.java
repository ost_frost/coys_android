package com.fanzine.chat3.view.fragment.group;

import com.fanzine.chat3.model.pojo.ChatUser;

import java.util.List;

/**
 * Created by mbp on 1/22/18.
 */

public interface AddParticipantFragmentI {
    void onParticipantsAdded(List<ChatUser> chatUsers);
}
