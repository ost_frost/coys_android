package com.fanzine.chat3.view.custom;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;

/**
 * Created by a on 27.03.18
 */
public class AcceptRejectDialog {
    private Context context;
    private String message;
    private AlertDialog.Builder alertBuilder;

    private DialogInterface.OnClickListener onPositiveButtonClickListener;

    public AcceptRejectDialog(@NonNull Context context, @NonNull String message, @NonNull DialogInterface.OnClickListener onPositiveButtonClickListener) {
        this.context = context;
        this.message = message;
        this.onPositiveButtonClickListener = onPositiveButtonClickListener;

        alertBuilder = new AlertDialog.Builder(context);

        String title = "";

        alertBuilder.setTitle(title);
        alertBuilder.setMessage(message);

        alertBuilder.setPositiveButton("YES", onPositiveButtonClickListener);
        alertBuilder.setNegativeButton("NO", (dialogInterface, i) -> dialogInterface.dismiss());
    }

    public void show() {

        alertBuilder.show();
    }

}
