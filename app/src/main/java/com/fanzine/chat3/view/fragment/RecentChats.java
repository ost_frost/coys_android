package com.fanzine.chat3.view.fragment;

import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.databinding.FragmentRecentChatsBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.chat3.model.ChannelType;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ImageCapture;
import com.fanzine.chat3.view.activity.messages.MessagesListActivity;
import com.fanzine.chat3.view.adapter.DialogListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbp on 4/17/18.
 */

public class RecentChats extends BaseFragment implements OnDialogOpenListenter {

    int TOOLBAR_MODE_MAIN = 10;
    int TOOLBAR_MODE_SEARCH = 11;

    private final static String KEY_CHANNELS = "channels";
    private final static String KEY_IMAGE_CAPTURE = "image_capture";

    private FragmentRecentChatsBinding binding;

    private DialogListAdapter mAdapter;


    public static RecentChats newInstance(ArrayList<Channel> channels, ImageCapture imageCapture) {
        Bundle data = new Bundle();
        data.putParcelableArrayList(KEY_CHANNELS, channels);
        data.putParcelable(KEY_IMAGE_CAPTURE, imageCapture);

        RecentChats recentChats = new RecentChats();
        recentChats.setArguments(data);

        return recentChats;
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        binding = FragmentRecentChatsBinding.inflate(inflater, root, attachToRoot);

        List<Channel> channels = getArguments().getParcelableArrayList(KEY_CHANNELS);

        if (channels == null) {
            channels = new ArrayList<>();
        }
        mAdapter = new DialogListAdapter(getContext(), R.layout.item_chat_dialog, channels, this);
        binding.rvChats.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvChats.setAdapter(mAdapter);

        binding.btBack.setOnClickListener(view -> {

        });

        binding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mAdapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.ivBack.setOnClickListener(view -> {

            setToolbarMode(TOOLBAR_MODE_MAIN);

        });


        binding.ivSearch.setOnClickListener(view -> {
            binding.laySearch.setVisibility(View.VISIBLE);
            binding.layToolbarMain.setVisibility(View.GONE);
            binding.toolbar.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));

            binding.etSearch.requestFocus();
        });
        setToolbarMode(TOOLBAR_MODE_MAIN);

        return binding;
    }

    @Override
    public void openDialog(Channel channel) {
        ImageCapture imageCapture = getArguments().getParcelable(KEY_IMAGE_CAPTURE);

        Intent activity = new Intent(getContext(), MessagesListActivity.class);

        if (channel.getLeft() && channel.getType().equals(ChannelType.ONE_TO_MANY))
            activity.putExtra(MessagesListActivity.CHANNEL_POJO, channel);
        else
            activity.putExtra(MessagesListActivity.CHANNEL_ID, Long.valueOf(channel.getId()));

        activity.putExtra(MessagesListActivity.KEY_IMAGE_CAPTURE, imageCapture);
        activity.setAction(MessagesListActivity.ACTION_IMAGE_CAPTURE);

        startActivity(activity);
    }

    public void setToolbarMode(int toolbarMode) {
        if (toolbarMode == TOOLBAR_MODE_MAIN) {
            binding.etSearch.setText("");
            binding.toolbar.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.toobar_color));

            binding.layToolbarMain.setVisibility(View.VISIBLE);

            binding.laySearch.setVisibility(View.GONE);
        } else if (toolbarMode == TOOLBAR_MODE_SEARCH) {
            binding.toolbar.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.toobar_color));

            binding.laySearch.setVisibility(View.VISIBLE);

            binding.layToolbarMain.setVisibility(View.GONE);

        }
    }
}
