package com.fanzine.chat3.view.holder.contacts.single;

import android.view.View;
import android.widget.Toast;

import com.fanzine.coys.App;
import com.fanzine.coys.R;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.presenter.contacts.ContactsPhoneHolderPresenterI;
import com.fanzine.chat3.presenter.contacts.single.ContactsPhoneHolderPresenter;
import com.fanzine.chat3.view.fragment.OnDialogOpenListenter;
import com.fanzine.chat3.view.holder.contacts.base.BaseContactsPhoneHolder;

/**
 * Created by mbp on 12/15/17.
 */

public class ContactsPhoneHolder extends BaseContactsPhoneHolder {

    private OnDialogOpenListenter onDialogOpenListenter;

    private View tvInviteBySmsLabel;

    public ContactsPhoneHolder(View itemView, OnDialogOpenListenter onDialogOpenListenter) {
        super(itemView);

        this.onDialogOpenListenter = onDialogOpenListenter;
        this.tvInviteBySmsLabel = itemView.findViewById(R.id.tvIsInviteBySMS);
    }

    @Override
    public ContactsPhoneHolderPresenterI bindPresenter() {
        return new ContactsPhoneHolderPresenter();
    }

    @Override
    public void setAvailableStatus() {
        super.setAvailableStatus();
        tvInviteBySmsLabel.setVisibility(View.GONE);
    }

    @Override
    public void bindViews() {
    }

    @Override
    public int getNameId() {
        return R.id.tvName;
    }

    @Override
    public int getIsAvailableId() {
        return R.id.tvIsAvailable;
    }

    @Override
    public int getHeaderTextId() {
        return R.id.tvHeader;
    }

    @Override
    public int getHeaderLayoutId() {
        return R.id.llHeader;
    }

    @Override
    public int getAvatarId() {
        return R.id.ivAvatar;
    }

    @Override
    public void onClick() {
        if (contact.status.equals(ContactPhone.AVAILABLE)) {
            presenterI.onContactItemClick();
        } else {
            Toast.makeText(
                    App.getContext(),
                    App.getContext().getString(R.string.sms_invitation_sent), Toast.LENGTH_SHORT
            ).show();
        }
    }

    @Override
    public void setIsGunnerUser(String text) {

    }

    @Override
    public void setInvisibleStatus() {

    }

    @Override
    public void setPhoto(long id) {

    }

    @Override
    public void showMessagesList(Channel channel) {
        onDialogOpenListenter.openDialog(channel);
    }

    @Override
    public void showMessageToUser(String msgText) {

    }
}
