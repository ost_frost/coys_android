package com.fanzine.chat3.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.view.fragment.OnDialogSelectListener;
import com.fanzine.chat3.view.holder.dialog.DialogHolder;
import com.jakewharton.rxbinding.view.RxView;

import java.util.List;

/**
 * Created by mbp on 11/28/17.
 */

public class DialogListEditAdapter extends RecyclerView.Adapter<DialogHolder> {

    private Context context;
    private int layoutId;

    private List<Channel> dialogsList;

    private OnDialogSelectListener onDialogSelectListener;


    public DialogListEditAdapter(Context context, int layoutId, List<Channel> dialogs, OnDialogSelectListener onDialogSelectListener) {
        this.context = context;

        this.layoutId = layoutId;
        this.dialogsList = dialogs;

        this.onDialogSelectListener = onDialogSelectListener;
    }

    @Override
    public DialogHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        return new DialogHolder(v);
    }

    @Override
    public void onBindViewHolder(DialogHolder holder, int position) {

        Channel channel = dialogsList.get(position);

        holder.bind(channel);

        boolean isSelected = onDialogSelectListener.isSelected(channel);

        RxView.clicks(holder.itemView).subscribe(aVoid -> {
            boolean isSelectedAfterClick = onDialogSelectListener.isSelected(channel);

            if (isSelectedAfterClick) {
                onDialogSelectListener.removeDialog(channel);

                holder.itemView.setBackgroundResource(R.color.transparent);
                holder.itemView.findViewById(R.id.civChatSelect).setBackgroundResource(R.drawable.chat_contact_unselected);
            } else {
                onDialogSelectListener.addDialog(channel);

                holder.itemView.setBackgroundResource(R.color.transparent);
                holder.itemView.findViewById(R.id.civChatSelect).setBackgroundResource(R.drawable.chat_contact_selected);
            }


        });

        holder.itemView.setBackgroundResource(isSelected
                ? R.color.transparent : R.color.transparent);
        holder.itemView.findViewById(R.id.civChatSelect).setBackgroundResource(isSelected ? R.drawable.chat_contact_selected : R.drawable.chat_contact_unselected);
    }

    @Override
    public int getItemCount() {
        return dialogsList.size();
    }

//    private void markAsSelected(DialogHolder holder) {
//        holder.itemView.setBackgroundResource(R.color.selected_contact_background);
//        holder.itemView.findViewById(R.id.civChatSelect).setBackgroundResource(R.drawable.select_active);
//    }
//
//    private void markAsNotSelected(DialogHolder holder) {
//        holder.itemView.setBackgroundResource(R.color.transparent);
//        holder.itemView.findViewById(R.id.civChatSelect).setBackgroundResource(R.drawable.chat_contact_unselected);
//    }
}
