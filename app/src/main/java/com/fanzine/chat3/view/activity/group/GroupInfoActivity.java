package com.fanzine.chat3.view.activity.group;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.MainActivity;
import com.fanzine.coys.activities.base.NavigationFragmentActivity;
import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ChatUser;
import com.fanzine.chat3.presenter.group.info.GroupInfoPresenter;
import com.fanzine.chat3.presenter.group.info.GroupInfoPresenterI;
import com.fanzine.chat3.view.activity.messages.MessagesListActivity;
import com.fanzine.chat3.view.adapter.base.BaseArrayListAdapter;
import com.fanzine.chat3.view.fragment.group.GroupInfoI;
import com.fanzine.chat3.view.holder.GroupInfoUserHolder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.camera.DefaultCameraModule;
import com.esafirm.imagepicker.features.camera.OnImageReadyListener;
import com.esafirm.imagepicker.model.Image;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

public class GroupInfoActivity extends NavigationFragmentActivity
        implements GroupInfoI, OnImageReadyListener {
    public static final String LOG_TAG = ChatData.LOG_TAG + "ChatDialogMsg";

    private static final String TITLE = "title";
    private static final String RECIPIENTS_LIST = "recipients_list";

    public static final String CHANNEL_POJO = "channel_pojo";

    private final static String CAMERA_INSTANCE = "picker_image_camera";


    public final static String GROUP_TITLE = "group_title";
    public final static int REQUEST_GROUP_RENAME = 10;

    private final static int REQUEST_GALLERY = 4;
    private final static int REQUEST_CAMERA = 5;

    private DefaultCameraModule cameraModule;

    private TextView tvGroupTitle, tvCount, tvPercipientInfo, tvCreatedTime;

    private RecyclerView recyclerView;
    private ImageView ivGroupImage;

    private ImageView imageView16, ivSelectImage;
    private View tvAddParticipiant, btClearChat, icChatNameEdit;

    private GroupInfoPresenterI presenter;

    private View include;
    private TextView btEdit;
    private Bundle arg;

    private View btSendMessage, btBack, vPhotoButton;
    private EditText etMessage;
    private Context context;

    private TextView btExitChat;
    private View layAdd;
    private View layClearChat, layExitChat;

    private View content;
    private View progressBar;

    private Channel channel;

    private List<ChatUser> mChatUsers;

    private Dialog dialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_chat_group_info);

        context = this;

        this.channel = getIntent().getParcelableExtra(CHANNEL_POJO);

        btBack = findViewById(R.id.btBack);

        content = findViewById(R.id.content);

        btBack.setOnClickListener(view -> {
            onBackPressed();
        });

        progressBar = findViewById(R.id.progressBar);

        tvGroupTitle = (TextView) findViewById(R.id.tvGroupTitle);

        icChatNameEdit = findViewById(R.id.icChatNameEdit);
        icChatNameEdit.setOnClickListener(view -> {
            Intent groupRenameIntent = new Intent(this, GroupRenameActivity.class);
            groupRenameIntent.putExtra(GroupRenameActivity.CHANNEL_POJO, channel);
            startActivityForResult(groupRenameIntent, REQUEST_GROUP_RENAME);
        });


        layClearChat = findViewById(R.id.layClearChat);
        layExitChat = findViewById(R.id.layExitChat);
        btExitChat = (TextView) findViewById(R.id.btExitChat);


        ivGroupImage = (ImageView) findViewById(R.id.ivSelectImage);

        imageView16 = (ImageView) findViewById(R.id.imageView16);

        recyclerView = (RecyclerView) findViewById(R.id.rv);

        btExitChat = (TextView) findViewById(R.id.btExitChat);

        tvCount = (TextView) findViewById(R.id.tvCount);

        tvAddParticipiant = findViewById(R.id.ivImageAddParticipant);
        layAdd = findViewById(R.id.layAdd);

        layAdd.setOnClickListener(view -> {
            showAddParticipantsFramgent();
        });

        btExitChat.setOnClickListener(view -> {
            presenter.onButtonLeaveDialog();
        });

        layAdd = findViewById(R.id.layAdd);

        ivSelectImage = (ImageView) findViewById(R.id.ivSelectImage);

        btClearChat = findViewById(R.id.btClearChat);
        btClearChat.setOnClickListener(view -> showClearChatWarningDialog());

        tvPercipientInfo = (TextView) findViewById(R.id.tvPercipientInfo);

        tvCreatedTime = (TextView) findViewById(R.id.tvCreatedTime);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);

        presenter = new GroupInfoPresenter();
        presenter.bindView(this);
        presenter.bindChannel(channel);
    }

    @Override
    public void showProgressBar() {
        content.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        content.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setChannelName(String title) {
        tvGroupTitle.setText(title);
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MessagesListActivity.class);
        intent.putExtra(MessagesListActivity.CHANNEL_POJO, channel);

        startActivity(intent);
    }

    @Override
    public void setGroupImage(String url) {

        Glide.with(context).load(url).asBitmap().into(new BitmapImageViewTarget(ivGroupImage) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                ivGroupImage.setImageDrawable(circularBitmapDrawable);
            }
        });
    }

    @Override
    public void setCount(String count) {
        tvCount.setText(count + " Participants");
    }

    @Override
    public void dismissSelectImageDialog() {
        dialog.dismiss();
    }

    @Override
    public void setPersipientInfo(String text) {
        tvPercipientInfo.setText(text);
    }

    @Override
    public void setCreatedTime(String text) {
        tvCreatedTime.setText(text);
    }


    @Override
    public void setParticipantsUserList(List<ChatUser> list) {
        mChatUsers = list;
    }

    @Override
    public void showParticipants(boolean isOwnerActive) {
        if (mChatUsers == null) return;

        int layout;

        if (isOwnerActive) {
            layout = R.layout.item_chat_group_info_user_edit;
        } else {
            layout = R.layout.item_chat_group_info_user;
        }

        ParticipantsAdapter adapter = new ParticipantsAdapter(ChatUser.class, layout,
                GroupInfoUserHolder.class, mChatUsers);

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void navigateBack() {
        Intent chatListIntent = new Intent(this, MainActivity.class);
        chatListIntent.setAction(MainActivity.OPEN_CHAT_FRAGMENT);
        startActivity(chatListIntent);
    }

    @Override
    public void setReadOnlyMode() {
        View clButtons = findViewById(R.id.clButtons);
        clButtons.setVisibility(View.GONE);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(CAMERA_INSTANCE, cameraModule);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        cameraModule = (DefaultCameraModule) savedInstanceState.getSerializable(CAMERA_INSTANCE);
    }

    //todo: decompose
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;

        if (requestCode == REQUEST_GROUP_RENAME) {
            channel = data.getParcelableExtra(CHANNEL_POJO);
            setChannelName(channel.getName());
        } else if (requestCode == REQUEST_GALLERY) {
            List<Image> images = ImagePicker.getImages(data);
            onImageReady(images);
        } else if (requestCode == REQUEST_CAMERA) {
            cameraModule.getImage(context, data, this);
        }
    }

    @Override
    public void onImageReady(List<Image> list) {
        Observable.just(list.get(0))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    presenter.onButtonImageSendClick(result.getPath());
                });
    }

    @Override
    public void turnOnAdminMode() {
        tvAddParticipiant.setVisibility(View.VISIBLE);
        icChatNameEdit.setVisibility(View.VISIBLE);
        ivSelectImage.setOnClickListener(view -> showImageSelectDialog());
        tvAddParticipiant.setOnClickListener(view -> {
            showAddParticipantsFramgent();
        });
    }

    @Override
    public void turnOffAdminMode() {
        layAdd.setVisibility(View.GONE);
        tvAddParticipiant.setVisibility(View.GONE);
        icChatNameEdit.setVisibility(View.GONE);
        layClearChat.setVisibility(View.GONE);
    }

    private void showImageSelectDialog() {
        dialog = new Dialog(context);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_chat_select_image);

        View layCamera = dialog.findViewById(R.id.tvCamera);
        layCamera.setOnClickListener(view -> {
            new RxPermissions(this).request(Manifest.permission.CAMERA).subscribe(aBoolean -> {
                if (aBoolean) {
                    startCameraIntent();
                }
            }, throwable -> throwable.printStackTrace());
        });

        View layLibrary = dialog.findViewById(R.id.layLibrary);

        layLibrary.setOnClickListener(view -> {
            Intent startGalleryIntent = ImagePicker.create(this)
                    .single()
                    .showCamera(false)
                    .imageDirectory("Gunners")
                    .theme(R.style.AppTheme_NoActionBar)
                    .enableLog(true)
                    .getIntent(context);

            startActivityForResult(startGalleryIntent, REQUEST_GALLERY);
        });

        View layCancel = dialog.findViewById(R.id.layCancel);
        layCancel.setOnClickListener(view -> {
            dialog.dismiss();
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;

        final int width = this.getResources().getDisplayMetrics().widthPixels;
        dialog.getWindow()
                .setLayout((int) (width * 0.9), ViewGroup.LayoutParams.WRAP_CONTENT);

        dialog.show();
    }

    private void startCameraIntent() {
        Intent startCamersIntent = cameraModule.getCameraIntent(this);
        startActivityForResult(startCamersIntent, REQUEST_CAMERA);
    }

    private void showClearChatWarningDialog() {
        String title = "";
        String message = " Are you sure clear this chat…?";

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setTitle(title);
        alertBuilder.setMessage(message);

        alertBuilder.setPositiveButton("Yes", (dialog, arg1) -> presenter.onButtonClearChatClick());
        alertBuilder.setNegativeButton("No", (dialogInterface, i) -> dialogInterface.dismiss());

        alertBuilder.show();
    }

    private void showAddParticipantsFramgent() {
        if (mChatUsers == null) return;
        if (channel == null) return;

        Intent startContacts = new Intent(this, MainActivity.class);
        startContacts.setAction(MainActivity.OPEN_GROUP_INFO_ADD_PARTICIPANTS);
        startContacts.putExtra(MainActivity.CHANT_CHANNEL, channel);
        startContacts.putExtra(MainActivity.CHAT_GROUP_PARTICIPANTS_LIST, (ArrayList<ChatUser>) mChatUsers);

        startActivity(startContacts);
    }

    class ParticipantsAdapter extends BaseArrayListAdapter<ChatUser, GroupInfoUserHolder>
            implements GroupInfoUserHolder.OnParticipantsListener {
        public ParticipantsAdapter(Class<ChatUser> modelClass, int modelLayout, Class<GroupInfoUserHolder> viewHolderClass, List<ChatUser> snapshots) {
            super(modelClass, modelLayout, viewHolderClass, snapshots);
        }

        @Override
        public GroupInfoUserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            GroupInfoUserHolder holder = super.onCreateViewHolder(parent, viewType);

            holder.setOnParticipantsListener(this);

            return holder;
        }

        @Override
        protected void populateViewHolder(GroupInfoUserHolder viewHolder, ChatUser model, int position) {
            viewHolder.bind(model);
            viewHolder.bindDialog(channel);
        }

        @Override
        public void onDeleted(ChatUser user) {
            Observable.just(user).observeOn(AndroidSchedulers.mainThread()).subscribe(sub -> {
                remove(user);
                notifyDataSetChanged();
            });
        }
    }
}
