package com.fanzine.chat3.view.adapter.contacts.single;

import android.content.Context;
import android.view.View;

import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.view.adapter.contacts.base.BaseContactsPhoneAdapter;
import com.fanzine.chat3.view.fragment.OnDialogOpenListenter;
import com.fanzine.chat3.view.holder.contacts.base.BaseContactsPhoneHolder;
import com.fanzine.chat3.view.holder.contacts.single.ContactsPhoneHolder;

import java.util.List;

/**
 * Created by mbp on 12/15/17.
 */

public class ContactPhoneAdapter extends BaseContactsPhoneAdapter {

    private OnDialogOpenListenter onDialogOpenListenter;

    public ContactPhoneAdapter(List<ContactPhone> contactPhones, Context context, int layoutId,
                               OnDialogOpenListenter onDialogOpenListenter) {
        super(contactPhones, context, layoutId);

        this.onDialogOpenListenter = onDialogOpenListenter;
    }

    @Override
    protected BaseContactsPhoneHolder getHolderInstance(View view) {
        return new ContactsPhoneHolder(view, onDialogOpenListenter);
    }
}
