package com.fanzine.chat3.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.fanzine.coys.R;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.chat3.view.fragment.dialogs.DialogListFragment;

/**
 * Created by vitaliygerasymchuk on 3/9/18
 */

public class DialogsListActivity extends AppCompatActivity {

    public static final String NEWS_URL = "NEWS_URL";

    public static void launch(@NonNull Context context, @NonNull String url) {
        Intent intent = new Intent(context, DialogsListActivity.class);
        intent.putExtra(NEWS_URL, url);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chats_list);
        FragmentUtils.changeFragment(this, R.id.container, DialogListFragment.newInstanceNews(getNewsUrl()), false);
    }

    @NonNull
    private String getNewsUrl() {
        final Intent intent = getIntent();
        if (intent.hasExtra(NEWS_URL)){
            return intent.getStringExtra(NEWS_URL);
        }
        return "";
    }
}
