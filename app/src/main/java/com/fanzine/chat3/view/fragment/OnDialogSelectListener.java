package com.fanzine.chat3.view.fragment;

import com.fanzine.chat3.model.pojo.Channel;

/**
 * Created by mbp on 12/21/17.
 */

public interface OnDialogSelectListener {

    void addDialog(Channel channel);

    void removeDialog(Channel channel);

    boolean isSelected(Channel channel);
}
