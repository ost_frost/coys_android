package com.fanzine.chat3.view.holder;

import com.fanzine.chat3.model.pojo.ChatUser;

/**
 * Created by a on 27.11.17.
 */

public interface GroupInfoUserI {

    void setName(String name);

    void setImage(String photo);

    void showDeleteUserAlertDialog();

    void onPartcipantDeleted(ChatUser user);
}
