package com.fanzine.chat3.view.holder.contacts.group;

import android.view.View;
import android.widget.Toast;

import com.fanzine.coys.App;
import com.fanzine.coys.R;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.presenter.contacts.ContactsPhoneHolderPresenterI;
import com.fanzine.chat3.presenter.contacts.group.GroupContactsPhoneHolderPresenter;
import com.fanzine.chat3.view.fragment.OnContactSelectListener;
import com.fanzine.chat3.view.holder.contacts.base.BaseContactsPhoneHolder;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by mbp on 12/18/17.
 */

public class GroupContactsPhoneHolder extends BaseContactsPhoneHolder {

    private CircleImageView civChecked;
    private View tvInviteBySMSlabel;
    private OnContactSelectListener contactSelectListener;

    public GroupContactsPhoneHolder(View itemView, OnContactSelectListener contactSelectListener) {
        super(itemView);
        this.contactSelectListener = contactSelectListener;

        tvInviteBySMSlabel = itemView.findViewById(R.id.tvIsInviteBySMS);

    }

    @Override
    public ContactsPhoneHolderPresenterI bindPresenter() {
        return new GroupContactsPhoneHolderPresenter();
    }

    @Override
    public void bindViews() {
        civChecked = (CircleImageView) itemView.findViewById(R.id.contactSelected);
    }

    @Override
    public void onClick() {
        if (contact.status.equals(ContactPhone.AVAILABLE)) {
            presenterI.onContactItemClick();
        }
    }

    @Override
    public int getNameId() {
        return R.id.tvName;
    }

    @Override
    public int getIsAvailableId() {
        return R.id.tvIsAvailable;
    }

    @Override
    public int getHeaderTextId() {
        return R.id.tvHeader;
    }

    @Override
    public int getHeaderLayoutId() {
        return R.id.llHeader;
    }

    @Override
    public int getAvatarId() {
        return R.id.ivAvatar;
    }

    @Override
    public void setAvailableStatus() {
        super.setAvailableStatus();

        tvInviteBySMSlabel.setVisibility(View.GONE);
    }

    @Override
    public void setIsGunnerUser(String text) {

    }

    @Override
    public void setInvisibleStatus() {

    }

    @Override
    public void setPhoto(long id) {

    }

    @Override
    public void showMessagesList(Channel channel) {

    }

    @Override
    public void showMessageToUser(String msgText) {
        Toast.makeText(
                App.getContext(),
                App.getContext().getString(R.string.sms_invitation_sent), Toast.LENGTH_SHORT
        ).show();
    }

    public void onContactSelected(ContactPhone contactPhone) {
        contactSelectListener.onSelect(contactPhone);
        contactSelectListener.setStatusToNextButton();
    }

    public boolean isSelected(ContactPhone contactPhone) {
        return contactSelectListener.isSelected(contactPhone);
    }

    public void markAsSelected() {
        civChecked.setBackgroundResource(R.drawable.select_active);
        itemView.setBackgroundResource(R.color.selected_contact_background);
    }

    public void markAsNotSelected() {
        civChecked.setBackgroundResource(R.drawable.chat_contact_unselected);
        itemView.setBackgroundResource(R.color.transparent);
    }
}
