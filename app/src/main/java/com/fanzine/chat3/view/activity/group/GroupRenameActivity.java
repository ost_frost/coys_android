package com.fanzine.chat3.view.activity.group;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.base.NavigationFragmentActivity;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.presenter.group.info.GroupRenamePresenter;
import com.fanzine.chat3.presenter.group.info.GroupRenamePresenterI;
import com.fanzine.chat3.view.fragment.group.GroupRenameFragmentI;
import com.jakewharton.rxbinding.view.RxView;

/**
 * Created by mbp on 12/20/17.
 */

//tod:: refactor
public class GroupRenameActivity extends NavigationFragmentActivity implements GroupRenameFragmentI {

    public static final String CHANNEL_POJO = "channel_pojo";

    private Channel channel;

    private EditText etGroupTitle;
    private View btSave;

    private GroupRenamePresenterI groupRenamePresenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_group_rename);

        etGroupTitle = (EditText) findViewById(R.id.etGroupTitle);
        btSave =  findViewById(R.id.btSave);

        this.channel = getIntent().getParcelableExtra(CHANNEL_POJO);

        groupRenamePresenter = new GroupRenamePresenter();
        groupRenamePresenter.bindView(this);
        groupRenamePresenter.bindChannel(channel);

        RxView.clicks(btSave).subscribe(aVoid -> {
            String title = etGroupTitle.getText().toString();

            if (title.length() > 0) {
                groupRenamePresenter.rename(title);
            } else {
                new AlertDialog.Builder(this)
                        .setTitle("Group title is too short")
                        .setMessage("Please, enter group name")
                        .show();
            }
        });
    }


    @Override
    public void setGroupTitle(String title) {
        etGroupTitle.setHint(title);
    }

    @Override
    public Channel getChannel() {
        return channel;
    }

    @Override
    public void updateChannel(Channel channel) {
        this.channel = channel;
    }



    @Override
    public void navigateBack() {
        hideSoftKeyboard(this, etGroupTitle);


        Intent intent = new Intent();
        intent.putExtra(GroupInfoActivity.CHANNEL_POJO, channel);
        setResult(RESULT_OK, intent);
        finish();
    }

    public static void hideSoftKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

        if ( imm == null)
            return;

        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }
}
