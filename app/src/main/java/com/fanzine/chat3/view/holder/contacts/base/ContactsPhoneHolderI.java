package com.fanzine.chat3.view.holder.contacts.base;

import com.fanzine.chat3.model.pojo.Channel;

/**
 * Created by a on 16.11.17.
 */
public interface ContactsPhoneHolderI {

    void setAvailableStatus();

    void setUnavailableStatus();

    void setName(String name);

    void setIsGunnerUser(String text);

    void setInvisibleStatus();

    void setPhoto(long id);

    void showMessagesList(Channel channel);

    void showHeader(String header);

    void hideHeader();

    void showMessageToUser(String msgText);
}
