package com.fanzine.chat3.view.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.ViewDataBinding;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.chat.UsersListAdapter;
import com.fanzine.coys.databinding.FragmentChatUsersListBinding;
import com.fanzine.coys.fragments.base.BaseFragment;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.coys.viewmodels.fragments.chat.ChatUserListViewModel;
import com.fanzine.chat.ChatSDK;
import com.fanzine.chat.interfaces.FCSuccessListener;
import com.fanzine.chat.interfaces.FCUsersListener;
import com.fanzine.chat.models.channels.FCChannel;
import com.fanzine.chat.models.user.FCUser;
import com.fanzine.chat3.view.fragment.contacts.single.ContactsPhoneListFragment;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Woland on 17.04.2017.
 */

public class UserListFragment extends BaseFragment {

    private static final String CHANNEL = "channel";
    private static final String USERS = "users";
    private UsersListAdapter adapter;
    private FCChannel channel;
    private UsersReceiver receiver;
    private FragmentChatUsersListBinding binding;

    public static UserListFragment newInstance(FCChannel channel, List<FCUser> users) {
        Bundle args = new Bundle();
        args.putParcelable(CHANNEL, channel);
        args.putParcelableArrayList(USERS, new ArrayList<>(users));
        UserListFragment fragment = new UserListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public ViewDataBinding onBindView(LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        if (binding == null) {
            binding = FragmentChatUsersListBinding.inflate(inflater, root, attachToRoot);
            ChatUserListViewModel viewModel = new ChatUserListViewModel(getContext());
            binding.setViewModel(viewModel);

            channel = getArguments().getParcelable(CHANNEL);
            List<FCUser> users = getArguments().getParcelableArrayList(USERS);

            FCUser currentUser = ChatSDK.getInstance().getCurrentSession().getCurrentUser();

            boolean isOwner = channel.getOwner().equals(currentUser.getUid());

            for (FCUser user : users) {
                if (user.getUid().equals(currentUser.getUid())) {
                    users.remove(user);
                    break;
                }
            }

            viewModel.isOwner.set(isOwner);
            viewModel.isOneToOne.set(channel.isOneToOne());

            if (isOwner) {
                viewModel.admin.set(currentUser.getFirstName() + " " + currentUser.getLastName());
            } else {
                for (FCUser user : users) {
                    if (user.getUid().equals(channel.getOwner())) {
                        viewModel.admin.set(user.getFirstName() + " " + user.getLastName());
                        break;
                    }
                }
            }

            binding.rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            adapter = new UsersListAdapter(getContext(), users, channel);
            binding.rv.setAdapter(adapter);

            viewModel.loadBanned(channel, new FCUsersListener() {
                @Override
                public void onUsersReceived(List<FCUser> list) {
                    Set<String> banned = new HashSet<>();
                    for (FCUser user : list)
                        banned.add(user.getUid());
                    adapter.setBanned(banned);
                }

                @Override
                public void onError(Exception e) {

                }
            });

            RxView.clicks(binding.leaveRoom).subscribe(aVoid -> ChatSDK.getInstance().getChatManager().leaveChatRoom(channel, new FCSuccessListener() {
                @Override
                public void onSuccess() {
                    getActivity().onBackPressed();
                }

                @Override
                public void onError(Exception e) {
                    DialogUtils.showAlertDialog(getContext(), R.string.smth_goes_wrong);
                }
            }));

            setToolbar(binding.toolbar);
        }

        return binding;
    }

    private void setToolbar(Toolbar toolbar) {
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        final Drawable upArrow = ContextCompat.getDrawable(getContext(), R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(ContextCompat.getColor(getContext(), R.color.inbox_bg_home_button), PorterDuff.Mode.SRC_ATOP);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_contacts, menu);
        menu.findItem(R.id.add).setVisible(!channel.isOneToOne());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.add) {
            FragmentUtils.changeFragment(getActivity(), R.id.content_frame, ContactsPhoneListFragment.newInstance(), true);

            receiver = new UsersReceiver();
            getActivity().registerReceiver(receiver, new IntentFilter(UsersReceiver.ACTION));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        if (receiver != null) {
            getActivity().unregisterReceiver(receiver);
            receiver = null;
        }

        super.onDestroy();
    }

    public class UsersReceiver extends BroadcastReceiver {

        public static final String ACTION = "com.almet.arsenal.fragments.chat.UsersReceiver.ACTION";
        public static final String USERS = "users";

        public UsersReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION)) {
                List<FCUser> users = intent.getParcelableArrayListExtra(USERS);
                adapter.addUsers(users);
            }
        }
    }
}
