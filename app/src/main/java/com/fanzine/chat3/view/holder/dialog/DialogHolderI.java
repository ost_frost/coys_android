package com.fanzine.chat3.view.holder.dialog;

/**
 * Created by a on 18.11.17.
 */

public interface DialogHolderI {
    void setChannelName(String name);

    void setTimeAgo(String timeAgo);

    void setLastMessage(String lastMessage);

    void setImage(String fbImageUid);
}
