package com.fanzine.chat3.view.activity.map;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.EditText;

import com.fanzine.coys.R;
import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.view.activity.messages.MessagesListActivity;

import com.fanzine.map.CurrentLocation;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    public static final String MODE = "mode";
    public static final String MODE_VIEW_LOCATION = "mode_view";
    public static final String MODE_SET_LOCATION = "mode_edit";

    public static final String LOCATION = "location";

    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";


    private View vSendImage, layMsgSendPanel, imClose;
    private EditText etMessageText;
    final LatLng[] myPoint = new LatLng[1];
    private String channelUid;
    private String mode;

    private LatLng startPosition;

    public static final double START_LATITUDE = -34;
    public static final double START_LONGITUDE = 151;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_maps);

        startPosition = new LatLng(START_LATITUDE, START_LONGITUDE);

        myPoint[0] = startPosition;

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        imClose = findViewById(R.id.imClose);
        imClose.setOnClickListener(view -> finish());

        layMsgSendPanel = findViewById(R.id.layMsgSendPanel);

        etMessageText = (EditText) findViewById(R.id.etMessageText);

        vSendImage = findViewById(R.id.vSendImage);

        Intent intent = getIntent();
        this.channelUid = intent.getStringExtra(ChatData.CHANNEL_UID);
        this.mode = intent.getStringExtra(MODE);

        if (mode.equals(MODE_SET_LOCATION)) {
            layMsgSendPanel.setVisibility(View.VISIBLE);

            vSendImage.setOnClickListener(view -> {
                String msgText = etMessageText.getText().toString();

                LatLng myPointIu = myPoint[0];

                Intent showMessagesIntent = new Intent();

                showMessagesIntent.putExtra(MessagesListActivity.LAT_LNG, myPointIu);
                showMessagesIntent.putExtra(MessagesListActivity.MAP_MESSAGE_TEXT, msgText);

                setResult(RESULT_OK, showMessagesIntent);
                finish();
            });

        } else if (mode.equals(MODE_VIEW_LOCATION)) {
            layMsgSendPanel.setVisibility(View.GONE);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        CurrentLocation currentLocation = new CurrentLocation(this, googleMap);

        if (mode.equals(MODE_SET_LOCATION)) {
            currentLocation.show();

            googleMap.setOnMapClickListener(point -> {
                myPoint[0] = point;
                // TODO Auto-generated method stub
                googleMap.clear();
                googleMap.addMarker(new MarkerOptions().position(point));
            });

        } else if (mode.equals(MODE_VIEW_LOCATION)) {
            Intent intent = getIntent();
            double lat = intent.getDoubleExtra(LATITUDE, 0);
            double lon = intent.getDoubleExtra(LONGITUDE, 0);

            startPosition = new LatLng(lat, lon);
            googleMap.addMarker(new MarkerOptions().position(startPosition).title("Marker in Sydney"));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(startPosition, 12.0f));
        }
    }
}
