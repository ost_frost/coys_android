package com.fanzine.chat3.view.fragment.group;

import com.fanzine.chat3.model.pojo.ChatUser;

import java.util.List;

public interface GroupInfoI {

    void setChannelName(String title);

    void setGroupImage(String fbImageUid);

    void setCount(String count);

    void dismissSelectImageDialog();

    void setPersipientInfo(String text);

    void setCreatedTime(String text);

    void setParticipantsUserList(List<ChatUser> list);

    void showParticipants(boolean isOwner);

    void setReadOnlyMode();

    void showProgressBar();

    void hideProgressBar();

    void navigateBack();

    void turnOnAdminMode();

    void turnOffAdminMode();
}
