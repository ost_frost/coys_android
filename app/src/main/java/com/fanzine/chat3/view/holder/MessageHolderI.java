package com.fanzine.chat3.view.holder;

import android.net.Uri;

import com.fanzine.chat3.model.pojo.Message;


/**
 * Created by a on 09.12.17.
 */

public interface MessageHolderI {
    void setUserName(String userName);

    void setTailVisibility(boolean visibility);

    void setImageShare(Uri uri);

    void setTime(String timeAgo);

    void setText(String message);

    void setMsgImage(String uid);

    void showMapView(double lat, double lon);

    void setMapLocation(Message data);

    void setMapVisible();

    void setMapGone();
}
