package com.fanzine.chat3.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.coys.adapters.base.BaseAdapter;
import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.presenter.participants.ParticipantHolderPresenter;
import com.fanzine.chat3.presenter.participants.ParticipantHolderPresenterI;
import com.fanzine.chat3.view.holder.ParticipantHolderI;

import java.util.List;

/**
 * Created by mbp on 11/17/17.
 */

public class ParticipantsAdapter extends BaseAdapter<ParticipantsAdapter.Holder> {

    private List<ContactPhone> participants;
    private int mLayout;

    public ParticipantsAdapter(Context context, int layout, List<ContactPhone> participants) {
        super(context);
        this.mLayout = layout;
        this.participants = participants;
    }

    @Override
    public Holder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(mLayout, parent, false);
        return new ParticipantsAdapter.Holder(v);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bind(participants.get(position), position);
    }

    @Override
    public int getItemCount() {
        return participants.size();
    }

    class Holder extends RecyclerView.ViewHolder implements ParticipantHolderI {

        private TextView name;
        private View btRemove;
        private ParticipantHolderPresenterI presenterI;

        public Holder(View itemView) {
            super(itemView);

            presenterI = new ParticipantHolderPresenter();
            presenterI.bindView(this);

            name = (TextView) itemView.findViewById(R.id.tvName);
            btRemove = itemView.findViewById(R.id.btRemove);

        }

        public void bind(ContactPhone user, int pos) {
            presenterI.bindParticipant(user);

            btRemove.setOnClickListener(view -> {
                participants.remove(user);
                notifyItemRemoved(pos);
            });
        }

        @Override
        public void setName(String name) {
            this.name.setText(name);
        }
    }
}
