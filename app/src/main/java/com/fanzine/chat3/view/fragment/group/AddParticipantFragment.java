package com.fanzine.chat3.view.fragment.group;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ChatUser;
import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.presenter.participants.GroupGroupParticipantsPresenter;
import com.fanzine.chat3.presenter.participants.GroupParticipantsPresenterI;
import com.fanzine.chat3.view.activity.group.GroupInfoActivity;
import com.fanzine.chat3.view.fragment.contacts.group.GroupContactsPhoneListFragment;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbp on 1/22/18.
 */

public class AddParticipantFragment extends GroupContactsPhoneListFragment
        implements AddParticipantFragmentI {

    private static final String CHANNEL = "channel";
    private static final String GROUP_PARTICIPANTS = "group_participants";

    private Channel channel;
    private List<ChatUser> chatUsers;

    public static Fragment newInstance(Channel channel, List<ChatUser> chatUsers) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(CHANNEL, channel);
        bundle.putParcelableArrayList(GROUP_PARTICIPANTS, (ArrayList<ChatUser>)chatUsers);

        AddParticipantFragment fragment = new AddParticipantFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    GroupParticipantsPresenterI presenterI;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        Channel channel = getArguments().getParcelable(CHANNEL);
        List<ChatUser> chatUsers = getArguments().getParcelableArrayList(GROUP_PARTICIPANTS);

        assert view != null;
        assert channel != null;
        assert chatUsers != null;

        this.channel = channel;
        this.chatUsers = chatUsers;

        presenterI = new GroupGroupParticipantsPresenter();
        presenterI.bindChannel(channel);
        presenterI.bindView(this);

        tvParticipantsCount = (TextView) view.findViewById(R.id.tvParcipantsCount);
        btNext = (TextView)view.findViewById(R.id.btNext);

        RxView.clicks(btNext).subscribe(aVoid -> {
            if (selectedParticipants.size() > 0) {
                presenterI.addParticipantsToChannel(selectedParticipants);
            }
        });

        btNext.setText("Next");

        return view;
    }

    @Override
    public void showContacts(List<ContactPhone> contacts) {
        progressBar.setVisibility(View.GONE);

        List<ContactPhone> contactPhonesWithoutDuplicate = new ArrayList<>();

        for ( ContactPhone contactPhone : contacts) {
            if ( !checkIfContactExistInChannel(contactPhone)) {
                contactPhonesWithoutDuplicate.add(contactPhone);
            }
        }
        adapter = getAdapter(contactPhonesWithoutDuplicate);

        rvList.setLayoutManager(new LinearLayoutManager(getContext()));
        rvList.setAdapter(adapter);
    }

    @Override
    public void onParticipantsAdded(List<ChatUser> chatUsers) {
        Intent showGroupInfo = new Intent(getContext(), GroupInfoActivity.class);
        showGroupInfo.putExtra(GroupInfoActivity.CHANNEL_POJO, channel);

        startActivity(showGroupInfo);
    }

    @Override
    protected boolean isSufficientNumberOfParticipants() {
        return selectedParticipants.size() > 0;
    }

    @Override
    public void setStatusToNextButton() {
        super.setStatusToNextButton();
    }

    private boolean checkIfContactExistInChannel(ContactPhone contactPhone) {
        for ( ChatUser chatUser : chatUsers) {
            if ( chatUser.uid.equals(contactPhone.id)) {
                return true;
            }
        }
        return false;
    }
}
