package com.fanzine.chat3.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.view.fragment.OnDialogOpenListenter;
import com.fanzine.chat3.view.holder.dialog.DialogHolder;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbp on 11/28/17.
 */

public class DialogListAdapter extends RecyclerView.Adapter<DialogHolder> implements Filterable {

    private Context context;
    private int layoutId;

    private List<Channel> dialogsList;
    private List<Channel> dialogsListFiltered;

    private OnDialogOpenListenter onDialogOpenListenter;


    public DialogListAdapter(Context context, int layoutId, List<Channel> dialogs, OnDialogOpenListenter onDialogOpenListenter) {
        this.context = context;

        this.layoutId = layoutId;
        this.dialogsList = dialogs;
        this.dialogsListFiltered = dialogs;

        this.onDialogOpenListenter = onDialogOpenListenter;
    }

    @Override
    public DialogHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        return new DialogHolder(v);
    }

    @Override
    public void onBindViewHolder(DialogHolder holder, int position) {
        holder.bind(dialogsListFiltered.get(position));

        RxView.clicks(holder.itemView).subscribe(aVoid -> {
            onDialogOpenListenter.openDialog(dialogsListFiltered.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return dialogsListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    dialogsListFiltered = dialogsList;
                } else {
                    List<Channel> filteredList = new ArrayList<>();
                    for (Channel row : dialogsList) {
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) || row.getName().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    dialogsListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dialogsListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dialogsListFiltered = (ArrayList<Channel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
