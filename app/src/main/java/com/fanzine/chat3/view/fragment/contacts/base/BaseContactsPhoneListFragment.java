package com.fanzine.chat3.view.fragment.contacts.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.networking.Service;
import com.fanzine.chat3.presenter.contacts.list.ContactsPhoneListPresenter;
import com.fanzine.chat3.presenter.contacts.list.ContactsPhoneListPresenterI;
import com.fanzine.chat3.view.activity.messages.MessagesListActivity;
import com.fanzine.chat3.view.adapter.contacts.base.BaseContactsPhoneAdapter;

import java.util.List;

import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;

/**
 * Created by mbp on 12/18/17.
 */

public abstract class BaseContactsPhoneListFragment extends Fragment implements ContactsPhoneListI {

    private ContactsPhoneListPresenterI presenter;
    protected BaseContactsPhoneAdapter adapter;

    protected IndexFastScrollRecyclerView rvList;
    protected SearchView etSearch;
    protected View progressBar;
    protected TextView tvLetters;

    protected abstract BaseContactsPhoneAdapter getAdapter(List<ContactPhone> contactsList);

    protected abstract int getSearchFieldId();
    protected abstract int getFragmentLayoutId();
    protected abstract int gerRvListId();
    protected abstract int getProgressBarId();
    protected abstract int searchBarId();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(getFragmentLayoutId(),
                container, false);

        rvList = (IndexFastScrollRecyclerView) rootView.findViewById(gerRvListId());
        etSearch = (SearchView) rootView.findViewById(getSearchFieldId());
        progressBar = rootView.findViewById(getProgressBarId());

        rvList.setIndexBarVisibility(true);

        String redColor = "#DD2C00";
        String whiteColor = "#ffffff";

        View searchBar = rootView.findViewById(searchBarId());

        searchBar.setOnClickListener(view -> {
            etSearch.setFocusableInTouchMode(true);
            etSearch.requestFocus();

            showKeyboard(etSearch);
        });

        rvList.setIndexBarTextColor(redColor);
        rvList.setIndexBarColor(whiteColor);
        rvList.setIndexBarTransparentValue(0);
        rvList.setIndexBarCornerRadius(0);
        rvList.setIndexbarMargin(0);
        rvList.setPreviewPadding(0);


        etSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                presenter.setSearchText(s);

                return true;
            }
        });



        presenter = new ContactsPhoneListPresenter(new Service());
        presenter.bindView(this);
        presenter.syncContacts();

        progressBar.setVisibility(View.VISIBLE);

        return rootView;
    }

    public void showKeyboard(SearchView searchView) {
        final InputMethodManager inputMethodManager = (InputMethodManager) getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(searchView, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void showContacts(List<ContactPhone> contacts) {
        progressBar.setVisibility(View.GONE);

        adapter = getAdapter(contacts);

        rvList.setLayoutManager(new LinearLayoutManager(getContext()));
        rvList.setAdapter(adapter);
    }

    @Override
    public void openMessagesFragment(Channel channel) {
        Intent activity = new Intent(getContext(), MessagesListActivity.class);
        activity.putExtra(MessagesListActivity.CHANNEL_POJO, channel);

        startActivity(activity);
    }

    @Override
    public void setSearchText(String searchText) {
        adapter.getFilter().filter(searchText);
    }

    @Override
    public void onError(String message) {
        progressBar.setVisibility(View.GONE);
    }

    public void onResume() {
        super.onResume();
    }

    public int getCountOfAllParticipants() {
        return adapter.getContactPhoneList().size();
    }
}
