package com.fanzine.chat3.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.chat3.model.pojo.Message;
import com.fanzine.chat3.view.holder.MessageHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mbp on 12/18/17.
 */

public class MessagesAdapter extends RecyclerView.Adapter<MessageHolder> {

    private List<Message> messageList;

    private String currentUserId;

    private Context context;

    private Map<String, Integer> messegeInTypeViewType = new HashMap<>();
    private Map<String, Integer> messegeOutTypeViewType = new HashMap<>();

    private MessageView messageView;


    public MessagesAdapter(String currentUserId, Context context) {
        this.currentUserId = currentUserId;
        this.context = context;

        this.messageList = new ArrayList<>();
        this.messageView = new MessageView();

        //in message
        messegeInTypeViewType.put(Message.TYPE_PICTURE, MessageHolder.MSG_PICTURE_IN);
        messegeInTypeViewType.put(Message.TYPE_LOCATION, MessageHolder.MSG_LOCATION_IN);
        messegeInTypeViewType.put(Message.TYPE_DEFAULT, MessageHolder.MSG_TYPE_IN);

        //out message
        messegeOutTypeViewType.put(Message.TYPE_PICTURE, MessageHolder.MSG_PICTURE_OUT);
        messegeOutTypeViewType.put(Message.TYPE_DEFAULT, MessageHolder.MSG_TYPE_OUT);
        messegeOutTypeViewType.put(Message.TYPE_LOCATION, MessageHolder.MSG_TYPE_OUT);
    }

    public void add(Message message) {
        messageList.add(message);
        notifyDataSetChanged();
    }

    public void addAll(List<Message> list) {
        messageList.addAll(list);
        notifyDataSetChanged();
    }

    public void clear() {
        messageList.clear();
        notifyDataSetChanged();
    }

    public int getLastItemPosition() {
        return getItemCount() - 1;
    }

    @Override
    public int getItemViewType(int position) {

        if ( getItemType(position).equals(Message.TYPE_SYSTEM)) {
            return MessageHolder.MSG_SYSTEM;
        }

        if (isOut(position)) {
            return messegeOutTypeViewType.get(getItemType(position));
        }
        return messegeInTypeViewType.get(getItemType(position));
    }

    @Override
    public void onViewRecycled(MessageHolder holder) {
        // Cleanup MapView here?
//        if (holder.getMap() != null)
//        {
//            holder.getMap().clear();
//            holder.getMap().setMapType(GoogleMap.);
//        }
    }

    @Override
    public void onBindViewHolder(MessageHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = messageView.getView(viewType, parent);
        return new MessageHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(MessageHolder holder, int position) {
//        Message nextMessage = null;
//
//        if (position < this.getItemCount() - 1) {
//            nextMessage = this.messageList.get(position + 1);
//        }

        MessageHolder messageHolder = (MessageHolder) holder;
        messageHolder.bind(messageList.get(position), messageList.get(position));
    }

    @Override
    public int getItemCount() {

        return messageList.size();
    }

    public String getItemType(int position) {
        return messageList.get(position).getType();
    }

    private boolean isOut(int position) {
        Message item = messageList.get(position);
        return  item.getUserId().equals(currentUserId);
    }

    class MessageView {

        private List<Integer> inTypes;
        private List<Integer> outTypes;

        public MessageView() {
            inTypes = new ArrayList<>();
            outTypes = new ArrayList<>();

            inTypes.add(MessageHolder.MSG_TYPE_IN);
            inTypes.add(MessageHolder.MSG_PICTURE_IN);
            inTypes.add(MessageHolder.MSG_LOCATION_IN);

            outTypes.add(MessageHolder.MSG_LOCATION_OUT);
            outTypes.add(MessageHolder.MSG_PICTURE_OUT);
            outTypes.add(MessageHolder.MSG_TYPE_OUT);
        }

        public View getView(int viewType, ViewGroup parent) {
            if ( viewType == MessageHolder.MSG_SYSTEM ) {
                return LayoutInflater.from(context).inflate(R.layout.item_chat_message_system, parent, false);
            }

            if ( outTypes.contains(viewType)) {
                return LayoutInflater.from(context).inflate(R.layout.item_chat_message_out, parent, false);
            }
            if ( inTypes.contains(viewType)) {
                return LayoutInflater.from(context).inflate(R.layout.item_chat_message_in, parent, false);
            }

            return new View(context);
        }
    }
}
