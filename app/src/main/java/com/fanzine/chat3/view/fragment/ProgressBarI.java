package com.fanzine.chat3.view.fragment;

/**
 * Created by mbp on 1/24/18.
 */

public interface ProgressBarI {

    void showProgressBar();
    void hideProgressBar();
}
