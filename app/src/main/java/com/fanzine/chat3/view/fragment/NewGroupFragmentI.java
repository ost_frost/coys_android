package com.fanzine.chat3.view.fragment;

import com.fanzine.chat3.model.pojo.Channel;

/**
 * Created by a on 23.11.17.
 */

public interface NewGroupFragmentI extends ProgressBarI {

    void setGroupImage(String imagePath);

    String getGroupNameText();

    void runMessagesListFragment(Channel channel);

    void makeCreateButtonActive();

    void makeCreateButtonNotActive();

    boolean isSubjectValid();
}
