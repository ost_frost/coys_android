package com.fanzine.chat3.view.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.MainActivity;
import com.fanzine.coys.activities.base.NavigationFragmentActivity;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.presenter.info.ContactInfoPresenter;
import com.fanzine.chat3.presenter.info.ContactInfoPresenterI;
import com.fanzine.chat3.view.activity.messages.MessagesListActivity;
import com.fanzine.chat3.view.fragment.ContactInfoI;
import com.squareup.picasso.Picasso;

//todo:: DUBLICATE code from .MessagesListActivity - refator
public class ContactInfoActivity extends NavigationFragmentActivity
        implements ContactInfoI {
    public static final String LOG_TAG = ContactInfoActivity.class.getName();

    public static final String CHANNEL_POJO = "channel_pojo";

    private ImageView ivContantImage;
    private ContactInfoPresenterI presenter;

    private View btBack, btClearChat;

    private Context context;

    private TextView btExitChat;
    private View layClearChat;

    private View content;
    private View progressBar;

    private Dialog dialog;

    private Channel channel;

    private TextView tvOwnerName, tvCreatedTime, tvGroupTitle;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_chat_contact_info);

        context = this;

        this.channel = getIntent().getParcelableExtra(CHANNEL_POJO);

        btBack = findViewById(R.id.btBack);
        content = findViewById(R.id.content);

        btBack.setOnClickListener(view -> {
            onBackPressed();
        });

        progressBar = findViewById(R.id.progressBar);
        tvGroupTitle = (TextView) findViewById(R.id.tvGroupTitle);
        layClearChat = findViewById(R.id.layClearChat);
        btExitChat = (TextView) findViewById(R.id.btExitChat);
        ivContantImage = (ImageView) findViewById(R.id.ivContantImage);
        btExitChat = (TextView) findViewById(R.id.btExitChat);

        tvCreatedTime = (TextView) findViewById(R.id.tvCreatedTime);
        tvOwnerName = (TextView) findViewById(R.id.tvOwnerName);

        btExitChat.setOnClickListener(view -> {
            presenter.onButtonLeaveDialog();

            Intent chatListIntent = new Intent(this, MainActivity.class);
            chatListIntent.setAction(MainActivity.OPEN_CHAT_FRAGMENT);

            startActivity(chatListIntent);
        });

        btClearChat = findViewById(R.id.btClearChat);
        btClearChat.setOnClickListener(view -> showClearChatWarningDialog());


        presenter = new ContactInfoPresenter();
        presenter.bindView(this);
        presenter.bindChannel(channel);
    }

    @Override
    public void showProgressBar() {
        content.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        content.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setOwnerName(String name) {
        tvOwnerName.setText(name);
    }

    @Override
    public void setImage(String url) {
        Picasso.with(this).load(url).into(ivContantImage);
    }

    @Override
    public void setChannelName(String title) {
        tvGroupTitle.setText(title);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MessagesListActivity.class);
        intent.putExtra(MessagesListActivity.CHANNEL_POJO, channel);

        startActivity(intent);
    }

    @Override
    public void setCreatedTime(String text) {
        tvCreatedTime.setText(text);
    }

    @Override
    public void navigateBack() {
        onBackPressed();
    }

    @Override
    public void openChatsList() {
        Intent chatListIntent = new Intent(this, MainActivity.class);
        chatListIntent.setAction(MainActivity.OPEN_CHAT_FRAGMENT);
        startActivity(chatListIntent);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void showClearChatWarningDialog() {

        String title = "";
        String message = " Are you sure clear this cha?";

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setTitle(title);
        alertBuilder.setMessage(message);

        alertBuilder.setPositiveButton("Yes", (dialog, arg1) -> presenter.onButtonClearChatClick());
        alertBuilder.setNegativeButton("No", (dialogInterface, i) -> dialogInterface.dismiss());

        alertBuilder.show();
    }
}
