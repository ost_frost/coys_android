package com.fanzine.chat3.view.fragment.contacts.base;

import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ContactPhone;

import java.util.List;

/**
 * Created by a on 15.11.17.
 */

public interface ContactsPhoneListI {

    void setSearchText(String searchText);

    void showContacts(List<ContactPhone> contacts);

    void openMessagesFragment(Channel channel);

    void onError(String message);
}
