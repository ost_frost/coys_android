package com.fanzine.chat3.view.fragment;

import com.fanzine.chat3.model.pojo.ContactPhone;

/**
 * Created by mbp on 12/20/17.
 */

public interface OnContactSelectListener {

    void onSelect(ContactPhone contact);

    boolean isSelected(ContactPhone contact);

    void setStatusToNextButton();
}
