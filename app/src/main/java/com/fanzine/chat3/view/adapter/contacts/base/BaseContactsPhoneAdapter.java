package com.fanzine.chat3.view.adapter.contacts.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.SectionIndexer;

import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.view.holder.contacts.base.BaseContactsPhoneHolder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by mbp on 12/18/17.
 */

public abstract class BaseContactsPhoneAdapter extends RecyclerView.Adapter<BaseContactsPhoneHolder>
        implements Filterable, SectionIndexer {

    private List<ContactPhone> contactPhoneList;
    private List<ContactPhone> contactPhoneListFiltered;

    private LinkedHashMap<String, Integer> mMapIndex;
    private ArrayList<String> mSectionList;
    private String[] mSections;
    private ArrayList<Integer> mSectionPositions;

    private Context context;
    private int layoutId;

    public BaseContactsPhoneAdapter(List<ContactPhone> contactPhones, Context context, int layoutId) {
        this.contactPhoneList = contactPhones;
        this.contactPhoneListFiltered = contactPhones;

        this.context = context;
        this.layoutId = layoutId;

        fillSections();
    }

    protected abstract BaseContactsPhoneHolder getHolderInstance(View view);

    @Override
    public BaseContactsPhoneHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        return getHolderInstance(v);
    }

    @Override
    public void onBindViewHolder(BaseContactsPhoneHolder holder, int position) {
        holder.bind(contactPhoneListFiltered.get(position));

        String section = getSection(getItem(position));

        if (mMapIndex.get(section) == position) {
            holder.showHeader(section);
        } else {
            holder.hideHeader();
        }
    }

    @Override
    public int getItemCount() {
        return contactPhoneListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactPhoneListFiltered = contactPhoneList;
                } else {
                    List<ContactPhone> filteredList = new ArrayList<>();
                    for (ContactPhone row : contactPhoneList) {
                        if (row.name.toLowerCase().contains(charString.toLowerCase()) || row.phone.contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    contactPhoneListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactPhoneListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactPhoneListFiltered = (ArrayList<ContactPhone>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public String[] getSections() {
        List<String> sections = new ArrayList<>(26);
        mSectionPositions = new ArrayList<>(26);
        for (int i = 0, size = contactPhoneListFiltered.size(); i < size; i++) {
            String section = String.valueOf(contactPhoneListFiltered.get(i).name.charAt(0)).toUpperCase();
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int i) {
        return mSectionPositions.get(i);
    }

    @Override
    public int getSectionForPosition(int i) {
        return 0;
    }

    public String getAllSectionLetters() {
        StringBuilder letters = new StringBuilder();

        for (String mSection : mSections) {
            letters.append(mSection.concat("\n"));
        }
        return letters.toString();
    }

    protected String getSection(ContactPhone pContact) {
        return pContact.name.substring(0, 1).toUpperCase();
    }

    protected ContactPhone getItem(int pPosition) {
        return contactPhoneListFiltered.get(pPosition);
    }

    private void fillSections() {
        mMapIndex = new LinkedHashMap<String, Integer>();

        for (int x = 0; x < contactPhoneListFiltered.size(); x++) {
            String fruit = contactPhoneListFiltered.get(x).name;
            if (fruit.length() > 1) {
                String ch = fruit.substring(0, 1);
                ch = ch.toUpperCase();
                if (!mMapIndex.containsKey(ch)) {
                    mMapIndex.put(ch, x);
                }
            }
        }
        Set<String> sectionLetters = mMapIndex.keySet();
        // create a list from the set to sort
        mSectionList = new ArrayList<String>(sectionLetters);
        Collections.sort(mSectionList);

        mSections = new String[mSectionList.size()];
        mSectionList.toArray(mSections);
    }

    public List<ContactPhone> getContactPhoneList() {
        return contactPhoneList;
    }

    public List<ContactPhone> getContactPhoneListFiltered() {
        return contactPhoneListFiltered;
    }
}
