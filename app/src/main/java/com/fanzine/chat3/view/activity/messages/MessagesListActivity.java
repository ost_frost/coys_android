package com.fanzine.chat3.view.activity.messages;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.coys.activities.MainActivity;
import com.fanzine.coys.activities.base.NavigationFragmentActivity;
import com.fanzine.coys.models.login.UserToken;
import com.fanzine.coys.sprefs.SharedPrefs;
import com.fanzine.coys.utils.SoftKeyboard;
import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.ChannelType;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ChatUser;
import com.fanzine.chat3.model.pojo.ImageCapture;
import com.fanzine.chat3.model.pojo.Message;
import com.fanzine.chat3.presenter.messages.MessageListPresenter;
import com.fanzine.chat3.presenter.messages.MessageListPresenterI;
import com.fanzine.chat3.view.activity.ContactInfoActivity;
import com.fanzine.chat3.view.activity.group.GroupNewActivity;
import com.fanzine.chat3.view.activity.map.MapsActivity;
import com.fanzine.chat3.view.adapter.MessagesAdapter;
import com.fanzine.chat3.view.activity.group.GroupInfoActivity;
import com.fanzine.chat3.view.fragment.MessageListI;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.camera.DefaultCameraModule;
import com.esafirm.imagepicker.features.camera.OnImageReadyListener;
import com.esafirm.imagepicker.model.Image;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;
import com.tbruyelle.rxpermissions.RxPermissions;


import java.io.File;
import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MessagesListActivity extends NavigationFragmentActivity implements
        MessageListI, OnImageReadyListener, OnMapReadyCallback {

    private static final String NEWS_URL = "NEWS_URL";

    public static void launch(@NonNull Context context, @NonNull String newsUrl) {
        Intent intent = new Intent(context, MessagesListActivity.class);
        intent.putExtra(NEWS_URL, newsUrl);
        context.startActivity(intent);
    }

    public static final String ACTION_NOTIFICATION = " com.fanzine.chat3.view.activity.messages.MessagesListActivity.notification";

    public static final String LOG_TAG = ChatData.LOG_TAG + "ChatDialogMsg";

    public static final String TITLE = "title";
    public static final String RECIPIENTS_LIST = "recipients_list";

    public static final String CHANNEL_ID = "channel_id";

    public static final long CHANNEL_DEFAULT_ID = -1;

    public static final String ACTION_IMAGE_CAPTURE= "image_capture";
    public static final String KEY_IMAGE_CAPTURE = "image_capture";

    public static final String LAT_LNG = "lat_lng";
    public static final String MAP_IMAGE = "lat_lng";
    public static final String MAP_MESSAGE_TEXT = "map_message_text";

    public static final String CHANNEL_POJO = "channel_pojo";

    private final static String CAMERA_INSTANCE = "picker_image_camera";

    private final static int REQUEST_GROUP_UPDATE = 15;

    private GoogleMap map;

    private View include;
    private RecyclerView recyclerView;
    private TextView btEdit, tvTitle, tvRecipients;
    private MessageListPresenterI presenter;

    private Bundle arg;

    private View btMedia, btBack, vPhotoButton, layMsgSendPanel, header;
    private EditText etMessage;
    private Context context;

    private Dialog optionsDialog;

    private ImageView ivGroupImage;

    private Channel channel;

    private MessagesAdapter messagesAdapter;
    private LinearLayoutManager mManager;

    private View layMsgSendPanelDisabled;
    private View vMoreButton;
    private View progressBar;
    private View bottomNavigation;
    private View btSend;

    private DefaultCameraModule cameraModule;
    private final static int REQUEST_GALLERY = 4;
    private final static int REQUEST_CAMERA = 5;

    private final static int REQUEST_MAP = 17;

    private UserToken mUserToken;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_chat_dialog_message);

        mUserToken = SharedPrefs.getUserToken();
        if (mUserToken != null) {
            messagesAdapter = new MessagesAdapter(mUserToken.getUserId(), this);
        }

        bottomNavigation = findViewById(R.id.bottomNavigation3);
        tvTitle = (TextView) findViewById(R.id.tvGroupTitle);
        tvRecipients = (TextView) findViewById(R.id.tvRecipients);

        btBack = findViewById(R.id.btBack);
        btMedia = findViewById(R.id.btMedia);
        btSend = findViewById(R.id.btSend);

        vMoreButton = findViewById(R.id.vMoreButton);
        vPhotoButton = findViewById(R.id.vPhotoButton);

        layMsgSendPanelDisabled = findViewById(R.id.layMsgSendPanelDisabled);

        ivGroupImage = (ImageView) findViewById(R.id.ivSelectImage);

        recyclerView = (RecyclerView) findViewById(R.id.f_chat_dialog_msg_rv);
        header = findViewById(R.id.header);
        progressBar = findViewById(R.id.progressBar);
        etMessage = (EditText) findViewById(R.id.etMessage);

        btBack.setOnClickListener(view -> onBackPressed());

        etMessage.setText(getNewsUrl());
        etMessage.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == R.integer.keyboard_return) {
                hideKeyboard();
                return true;
            }
            return false;
        });


        btSend.setOnClickListener(view -> {
            presenter.onButtonSendClick(etMessage.getText().toString());
            vPhotoButton.setVisibility(View.VISIBLE);
            vMoreButton.setVisibility(View.VISIBLE);
        });

        etMessage.setOnClickListener(view -> {
            bottomNavigation.setVisibility(View.GONE);
        });

        etMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    vPhotoButton.setVisibility(View.GONE);
                    vMoreButton.setVisibility(View.GONE);
                    btSend.setVisibility(View.VISIBLE);
                } else {
                    vPhotoButton.setVisibility(View.VISIBLE);
                    vMoreButton.setVisibility(View.VISIBLE);
                    btSend.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        InputMethodManager im = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);

        ConstraintLayout mainLayout = (ConstraintLayout) findViewById(R.id.root);

        SoftKeyboard softKeyboard;
        softKeyboard = new SoftKeyboard(mainLayout, im);
        softKeyboard.setSoftKeyboardCallback(new SoftKeyboard.SoftKeyboardChanged() {

            @Override
            public void onSoftKeyboardHide() {
                runOnUiThread(() -> bottomNavigation.setVisibility(View.VISIBLE));
            }

            @Override
            public void onSoftKeyboardShow() {
                runOnUiThread(() -> bottomNavigation.setVisibility(View.GONE));
            }
        });

        vPhotoButton.setOnClickListener(view -> {
            requestCameraPermission();
        });
        vMoreButton.setOnClickListener(view -> showOptionsDialog());

        mManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(mManager);
        recyclerView.setAdapter(messagesAdapter);

        cameraModule = new DefaultCameraModule();
        context = this;

        presenter = new MessageListPresenter();
        presenter.bindView(this);

        etMessage.clearFocus();
    }

    private void handleIntent() {
        if (getIntent() != null && getIntent().getLongExtra(CHANNEL_ID, CHANNEL_DEFAULT_ID) != CHANNEL_DEFAULT_ID) {
            String uid = String.valueOf(getIntent().getLongExtra(CHANNEL_ID, CHANNEL_DEFAULT_ID));

            presenter.loadChannel(uid);
        } else if (getIntent() != null && getIntent().getParcelableExtra(CHANNEL_POJO) != null) {
            initChannel(getIntent().getParcelableExtra(CHANNEL_POJO));
        }
    }

    private void initChannel(Channel channel) {
        if (channel != null) {
            this.channel = channel;

            btMedia.setOnClickListener(view -> {
                showImageSelectDialog();
            });

            presenter.bindDialog(channel);
            presenter.loadPartcipants(channel.getId());

            if ( getIntent().getAction() != null && getIntent().getAction().equals(ACTION_IMAGE_CAPTURE)) {
                ImageCapture imageCapture = getIntent().getParcelableExtra(KEY_IMAGE_CAPTURE);

                presenter.onImageDialogButtonSendClick(imageCapture.msgText, imageCapture.imagePath);
            }
        }
        cameraModule = new DefaultCameraModule();
    }

    @Override
    public void hideBottomMenu() {
        bottomNavigation.setVisibility(View.GONE);
    }

    @Override
    public void showBottomMenu() {
        bottomNavigation.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        openDialogsListFragment();
    }

    @Override
    public void onChannelReady(Channel channel) {
        initChannel(channel);
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMessagesList(List<Message> messageList) {
        messagesAdapter.clear();
        messagesAdapter.addAll(messageList);

        mManager.scrollToPosition(messageList.size()-1);
    }

    @Override
    public void showSentMessage(Message message) {
        messagesAdapter.add(message);

        int lastMessagePosition = messagesAdapter.getItemCount()-1;
        mManager.scrollToPosition(lastMessagePosition);
    }

    @Override
    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(CAMERA_INSTANCE, cameraModule);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        cameraModule = (DefaultCameraModule) savedInstanceState.getSerializable(CAMERA_INSTANCE);
    }

    private void openDialogsListFragment() {
        Intent chatListIntent = new Intent(this, MainActivity.class);
        chatListIntent.setAction(MainActivity.OPEN_CHAT_FRAGMENT);
        startActivity(chatListIntent);
    }

    private boolean isNotification() {
        return getIntent().getAction() != null && getIntent().getAction().equals(ACTION_NOTIFICATION);
    }

    private boolean isCalledByGroupNewActivity() {
        return getCallingActivity() != null &&
                getCallingActivity().getClassName().equals(GroupNewActivity.class.getName());
    }

    private void showImageSelectDialog() {
        optionsDialog = new Dialog(context);
        optionsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        optionsDialog.setContentView(R.layout.dialog_chat_select_image_and_location);

        View layCamera = optionsDialog.findViewById(R.id.tvCamera);
        layCamera.setOnClickListener(view -> requestCameraPermission());

        View layLibrary = optionsDialog.findViewById(R.id.layLibrary);
        layLibrary.setOnClickListener(view -> startGalleryIntent());

        View layLocation = optionsDialog.findViewById(R.id.layLocation);
        layLocation.setOnClickListener(view -> {
                    optionsDialog.dismiss();
                    startGoogleMapIntent();
                }
        );

        View layCancel = optionsDialog.findViewById(R.id.layCancel);
        layCancel.setOnClickListener(view -> optionsDialog.dismiss());

        optionsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        optionsDialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;

        final int width = this.getResources().getDisplayMetrics().widthPixels;
        optionsDialog.getWindow()
                .setLayout((int) (width * 0.9), ViewGroup.LayoutParams.WRAP_CONTENT);

        optionsDialog.show();
    }

    private void requestCameraPermission() {
        new RxPermissions(this).request(Manifest.permission.CAMERA).subscribe(aBoolean -> {
            if (aBoolean) {
                startCameraIntent();
            }
        }, throwable -> throwable.printStackTrace());
    }

    private void startGoogleMapIntent() {
        Intent intent = new Intent(context, MapsActivity.class);
        intent.putExtra(MapsActivity.MODE, MapsActivity.MODE_SET_LOCATION);

        intent.putExtra(ChatData.CHANNEL_UID, channel.getId());
        startActivityForResult(intent, REQUEST_MAP);
    }

    private void startGalleryIntent() {
        Intent startGalleryIntent = ImagePicker.create(this)
                .single()
                .showCamera(false)
                .imageDirectory("Gunners")
                .theme(R.style.AppTheme)
                .enableLog(true)
                .getIntent(context);

        startActivityForResult(startGalleryIntent, REQUEST_GALLERY);
    }

    private void startCameraIntent() {
        Intent startCamersIntent = cameraModule.getCameraIntent(this);
        startActivityForResult(startCamersIntent, REQUEST_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_GROUP_UPDATE) {
            if (resultCode == RESULT_OK) {
                channel = data.getParcelableExtra(CHANNEL_POJO);

                setTitle(channel.getName());
            }
        } else if (requestCode == REQUEST_GALLERY) {
            List<Image> images = ImagePicker.getImages(data);
            onImageReady(images);
        } else if (requestCode == REQUEST_CAMERA) {
            cameraModule.getImage(context, data, this);
        } else if (requestCode == REQUEST_MAP) {
            if (resultCode == RESULT_OK) {
                String message = data.getStringExtra(MAP_MESSAGE_TEXT);
                LatLng latLng = data.getParcelableExtra(LAT_LNG);

                presenter.onLocationSend(latLng, message);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
    }

    @Override
    public void onImageReady(List<Image> list) {
        Observable.just(getFirstImagePath(list))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showImagePreviewDialog);
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideOptionsDialog();
        handleIntent();
    }

    private String getFirstImagePath(List<Image> list) {
        if (list != null && list.size() > 0) {
            return list.get(0).getPath();
        }
        return "";
    }

    private void showOptionsDialog() {
        optionsDialog = new Dialog(context);
        optionsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        optionsDialog.setContentView(R.layout.dialog_chat_options);

        View layLeaveChat = optionsDialog.findViewById(R.id.layLeaveChat);

        if (channel.getType().equals(ChannelType.ONE_TO_MANY)) {
            layLeaveChat.setOnClickListener(view -> {
                hideOptionsDialog();
                showLeaveChatWarningDialog();
            });
            layLeaveChat.setVisibility(View.VISIBLE);
        } else {
            View layContacntInfo = optionsDialog.findViewById(R.id.layContacntInfo);
            layContacntInfo.setVisibility(View.VISIBLE);

            layContacntInfo.setOnClickListener(view -> {
                Intent startContactInfo = new Intent(this, ContactInfoActivity.class);
                startContactInfo.putExtra(ContactInfoActivity.CHANNEL_POJO, channel);
                startActivityForResult(startContactInfo, REQUEST_GROUP_UPDATE);
            });
        }

        View layDeleteChat = optionsDialog.findViewById(R.id.layDelete);
        layDeleteChat.setOnClickListener(view -> {
            hideOptionsDialog();
            showDeleteChatWarningDialog();
        });

        View layClearMessages = optionsDialog.findViewById(R.id.layClearMsg);
        layClearMessages.setOnClickListener(view -> {
            hideOptionsDialog();
            showClearChatWarningDialog();
        });

        View btCancel = optionsDialog.findViewById(R.id.layCancel);
        btCancel.setOnClickListener(view -> {
            optionsDialog.dismiss();
        });

        optionsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        optionsDialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;

        final int width = this.getResources().getDisplayMetrics().widthPixels;
        optionsDialog.getWindow()
                .setLayout((int) (width * 0.9), ViewGroup.LayoutParams.WRAP_CONTENT);

        optionsDialog.show();
    }

    private Dialog imagePreviewDialog;

    private void showImagePreviewDialog(String imagePath) {
        //presenter.onButtonImageSendClick(imagePath);
        imagePreviewDialog = new Dialog(this);

        imagePreviewDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imagePreviewDialog.setContentView(R.layout.dialog_chat_preview_image);

        EditText etMessageDialogPrevImage = (EditText) imagePreviewDialog.findViewById(R.id.etMessageText);

        View vSendDialogImagePreview = imagePreviewDialog.findViewById(R.id.vSendDialogImagePreview);
        vSendDialogImagePreview.setOnClickListener(view -> {
            String msgText = etMessageDialogPrevImage.getText().toString();
            presenter.onImageDialogButtonSendClick(msgText, imagePath);

            imagePreviewDialog.dismiss();

            if (optionsDialog != null)
                optionsDialog.dismiss();
        });


        View ivImagePreviewClose = imagePreviewDialog.findViewById(R.id.ivImagePreviewClose);
        ivImagePreviewClose.setOnClickListener(view -> {
            imagePreviewDialog.dismiss();
        });

        ImageView ivPreviewImage = (ImageView) imagePreviewDialog.findViewById(R.id.ivPreviewImage);
        File imgFile = new File(imagePath);

        if (imgFile.exists()) {
            Observable.fromCallable(() -> BitmapFactory.decodeFile(imgFile.getAbsolutePath()))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(ivPreviewImage::setImageBitmap);
        }

        final int width = this.getResources().getDisplayMetrics().widthPixels;
        final int height = this.getResources().getDisplayMetrics().heightPixels;

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(imagePreviewDialog.getWindow().getAttributes());
        lp.width = width;
        lp.height = height;

        imagePreviewDialog.getWindow().setAttributes(lp);
        imagePreviewDialog.getWindow().setBackgroundDrawable(null);
        imagePreviewDialog.show();
    }

    @Override
    public void setMessageText(String s) {
        etMessage.setText(s);
    }

    @Override
    public void showParticipants(List<ChatUser> participants) {
        StringBuilder participantsNames = new StringBuilder();

        for (ChatUser user : participants) {
            if (!user.uid.equals(mUserToken.getUserId())) {
                participantsNames.append(user.getFullName()).append(", ");
            }
        }

        if (participantsNames.length() - 2 > 0) {
            String subtitle = participantsNames.substring(0, participantsNames.length() - 2);
            subtitle += ", You";

            tvRecipients.setVisibility(View.VISIBLE);
            tvRecipients.setText(subtitle);
        }
    }

    @Override
    public void setGroupImage(String fbImageUid) {

        if (fbImageUid == null)
            ivGroupImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.nav_bar_icon));
        else if (fbImageUid.contains("default-chat-channel"))
            ivGroupImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.nav_bar_icon));
        else {
            ivGroupImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Picasso.with(this).load(fbImageUid).into(ivGroupImage);
        }
    }

    @Override
    public void addGroupEditClickListener() {
        header.setOnClickListener(view -> {
            openGroupInfoFragment();
        });
        ivGroupImage.setOnClickListener(view -> {
            openGroupInfoFragment();
        });
    }

    @Override
    public void setTitle(String titleText) {
        if (titleText != null && titleText.length() > 0) {
            tvTitle.setText(titleText);
        }
    }


    @Override
    public void dismissImageGroupImageSelectDialog() {
        optionsDialog.dismiss();
    }

    @Override
    public void setLayMsgSendPanelVisibility(boolean isVisible) {
        if (isVisible) {
            layMsgSendPanel.setVisibility(View.VISIBLE);
        } else {
            layMsgSendPanel.setVisibility(View.GONE);
        }
    }

    @Override
    public void blockMessagePanel() {
        layMsgSendPanelDisabled.setVisibility(View.VISIBLE);
    }

    private void showDeleteChatWarningDialog() {

        String title = "";
        String message = " Are you sure delete this chat?";

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setTitle(title);
        alertBuilder.setMessage(message);

        alertBuilder.setPositiveButton("Yes", (dialog, arg1) -> {
            presenter.delete(channel.getId());

            onBackPressed();
        });
        alertBuilder.setNegativeButton("No", (dialogInterface, i) -> dialogInterface.dismiss());

        alertBuilder.show();
    }

    private void showLeaveChatWarningDialog() {

        String title = "";
        String message = " Are you sure leave this chat?";

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setTitle(title);
        alertBuilder.setMessage(message);

        alertBuilder.setPositiveButton("Yes", (dialog, arg1) -> {
            layMsgSendPanelDisabled.setVisibility(View.VISIBLE);
            presenter.leave(channel.getId());
            channel.setLeft(true);
        });
        alertBuilder.setNegativeButton("No", (dialogInterface, i) -> {
            dialogInterface.dismiss();
        });

        alertBuilder.show();
    }

    private void showClearChatWarningDialog() {

        String title = "";
        String message = " Are you sure clear this chat?";

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setTitle(title);
        alertBuilder.setMessage(message);

        alertBuilder.setPositiveButton("Yes", (dialog, arg1) -> {
            presenter.clearMessages(channel.getId());
            messagesAdapter.clear();
        });
        alertBuilder.setNegativeButton("No", (dialogInterface, i) -> {
            dialogInterface.dismiss();
        });

        alertBuilder.show();
    }

    private void hideOptionsDialog() {
        if (optionsDialog != null) {
            optionsDialog.dismiss();
        }
    }

    private void openGroupInfoFragment() {
        Intent groupSettignsIntent = new Intent(this, GroupInfoActivity.class);
        groupSettignsIntent.putExtra(GroupInfoActivity.CHANNEL_POJO, channel);
        startActivityForResult(groupSettignsIntent, REQUEST_GROUP_UPDATE);
    }

    @NonNull
    private String getNewsUrl() {
        if (getIntent().hasExtra(NEWS_URL)) {
            return getIntent().getStringExtra(NEWS_URL);
        }
        return "";
    }
}
