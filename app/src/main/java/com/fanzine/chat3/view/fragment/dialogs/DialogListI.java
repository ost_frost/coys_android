package com.fanzine.chat3.view.fragment.dialogs;

import com.fanzine.chat3.model.pojo.Channel;

import java.util.List;

public interface DialogListI {

    void showDialogs(List<Channel> dialogs);

    void setSearchText(String q);
}
