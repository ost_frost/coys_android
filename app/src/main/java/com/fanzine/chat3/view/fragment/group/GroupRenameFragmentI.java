package com.fanzine.chat3.view.fragment.group;

import com.fanzine.chat3.model.pojo.Channel;

/**
 * Created by mbp on 12/20/17.
 */

public interface GroupRenameFragmentI {

    void setGroupTitle(String title);

    Channel getChannel();

    void updateChannel(Channel channel);

    void navigateBack();
}
