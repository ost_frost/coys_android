package com.fanzine.chat3.view.holder.dialog;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.presenter.dialog.DialogHolderPresenter;
import com.fanzine.chat3.presenter.dialog.DialogHolderPresenterI;
import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;

//ChannelPost-user holder
public class DialogHolder extends RecyclerView.ViewHolder implements DialogHolderI {
    public static final String LOG_TAG = ChatData.LOG_TAG + "DialogHolder";
    private Context context;

    private DialogHolderPresenterI presenter;

    private TextView tvName, tvTime, tvMessage;

    private ImageView ivImage;

    private CircleImageView civChatSelect;

    private Channel channel;

    public DialogHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();

        tvName = (TextView) itemView.findViewById(R.id.chat_chats_tvTitle);

        tvTime = (TextView) itemView.findViewById(R.id.chat_chats_tvTime);

        tvMessage = (TextView) itemView.findViewById(R.id.chat_chats_tvMessage);

        ivImage = (ImageView) itemView.findViewById(R.id.ivImage);

        civChatSelect = (CircleImageView) itemView.findViewById(R.id.civChatSelect);

        if (this.presenter == null) {
            this.presenter = new DialogHolderPresenter();
        }
    }

    public void bind(Channel channel) {
        presenter.bindView(this);
        presenter.bindChannel(channel);

        this.channel = channel;
    }

    @Override
    public void setChannelName(String name) {
        tvName.setText(name);
    }

    @Override
    public void setTimeAgo(String timeAgo) {
        tvTime.setText(timeAgo);
    }

    @Override
    public void setLastMessage(String lastMessage) {
        tvMessage.setText(lastMessage);
    }

    @Override
    public void setImage(String url) {
        if (url != null && !url.contains("default-chat-channel")) {
            Glide.with(context).load(url).into(ivImage);
        } else if ((url != null && url.contains("default-chat-channel")) || url == null)
            ivImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.whatsappgroup));
    }
}