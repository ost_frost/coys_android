package com.fanzine.chat3.view.fragment.contacts.single;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanzine.coys.R;
import com.fanzine.coys.utils.DialogUtils;
import com.fanzine.coys.utils.FragmentUtils;
import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.view.activity.messages.MessagesListActivity;
import com.fanzine.chat3.view.adapter.contacts.base.BaseContactsPhoneAdapter;
import com.fanzine.chat3.view.adapter.contacts.single.ContactPhoneAdapter;
import com.fanzine.chat3.view.fragment.OnDialogOpenListenter;
import com.fanzine.chat3.view.fragment.contacts.base.BaseContactsPhoneListFragment;
import com.fanzine.chat3.view.fragment.contacts.group.GroupContactsPhoneListFragment;
import com.jakewharton.rxbinding.view.RxView;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.util.List;

public class ContactsPhoneListFragment extends BaseContactsPhoneListFragment implements OnDialogOpenListenter {
    public static final String LOG_TAG = ChatData.LOG_TAG + "ContactsPhone";

    public static Fragment newInstance() {
        Fragment fragment = new ContactsPhoneListFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        View btNewGroup = view.findViewById(R.id.tvAddParticipiant);

        RxView.clicks(btNewGroup).subscribe(
                aVoid -> new RxPermissions(getActivity())
                        .request(Manifest.permission.READ_CONTACTS).subscribe(aBoolean -> {
                            if (aBoolean) {
                                FragmentUtils.changeFragment(getActivity(), R.id.content_frame, GroupContactsPhoneListFragment.newInstance(), true);
                            } else
                                DialogUtils.showAlertDialog(getContext(), R.string.contactsNeeded);
                        }));

        View btCancel = view.findViewById(R.id.fChatList_btCancel);
        btCancel.setOnClickListener(view1 ->  {
            getActivity().onBackPressed();
        });

        return view;
    }

    @Override
    protected BaseContactsPhoneAdapter getAdapter(List<ContactPhone> contactsList) {
        return new ContactPhoneAdapter(contactsList, getContext(),R.layout.item_chat_phone_contact, this);
    }

    @Override
    protected int getSearchFieldId() {
        return R.id.etSearch;
    }

    @Override
    protected int searchBarId() {
        return R.id.searchBar;
    }

    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_chat_contacts_phone_list;
    }

    @Override
    protected int gerRvListId() {
        return R.id.rv;
    }

    @Override
    protected int getProgressBarId() {
        return R.id.progressBar;
    }


    @Override
    public void openDialog(Channel channel) {
        Intent activity = new Intent(getContext(), MessagesListActivity.class);
        activity.putExtra(MessagesListActivity.CHANNEL_POJO, channel);

        startActivity(activity);
    }
}
