package com.fanzine.chat3.view.holder;

import android.app.AlertDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.chat3.ChatData;
import com.fanzine.chat3.model.pojo.Channel;
import com.fanzine.chat3.model.pojo.ChatUser;
import com.fanzine.chat3.presenter.group.info.GroupInfoUserPresenter;
import com.fanzine.chat3.presenter.group.info.GroupInfoUserPresenterI;
import com.bumptech.glide.Glide;

public class GroupInfoUserHolder extends RecyclerView.ViewHolder implements GroupInfoUserI {

    public interface OnParticipantsListener {
        void onDeleted(ChatUser user);
    }

    public static final String LOG_TAG = ChatData.LOG_TAG + "GroupInfoUser";
    private Context context;

    private GroupInfoUserPresenterI presenter;

    private TextView tvName, tvTime, tvMessage;

    private ImageView ivImage, ivRemove;

    private  OnParticipantsListener onParticipantsListener;

    public GroupInfoUserHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();

        tvName = (TextView) itemView.findViewById(R.id.tvName);

        ivImage = (ImageView) itemView.findViewById(R.id.ivImage);

        ivRemove = (ImageView) itemView.findViewById(R.id.ivRemove);

        if ( ivRemove != null) {
            ivRemove.setOnClickListener(view -> showDeleteUserAlertDialog());
        }

        if (this.presenter == null) {
            this.presenter = new GroupInfoUserPresenter();
        }

        this.onParticipantsListener = onParticipantsListener;
    }

    public void setOnParticipantsListener(OnParticipantsListener onParticipantsListener) {
        this.onParticipantsListener = onParticipantsListener;
    }


    public void bind(ChatUser channel) {
        presenter.bindView(this);
        presenter.bindDialogUser(channel);
    }

    public void bindDialog(Channel channel) {
        presenter.bindDialog(channel);
    }

    @Override
    public void setName(String name) {
        tvName.setText(name);
    }

    @Override
    public void setImage(String photo) {

        Glide.with(context).load(photo).into(ivImage);
    }

    @Override
    public void onPartcipantDeleted(ChatUser user) {
        onParticipantsListener.onDeleted(user);
    }

    public void showDeleteUserAlertDialog() {
        String title = "";
        String message = " Are you sure you want to remove?";

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setTitle(title);
        alertBuilder.setMessage(message);
        alertBuilder.setPositiveButton("YES", (dialog, arg1) -> presenter.onButtonRemoveUserClick());
        alertBuilder.setNegativeButton("NO", (dialogInterface, i) -> dialogInterface.dismiss());

        alertBuilder.show();
    }
}