package com.fanzine.chat3.view.holder.contacts.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.presenter.contacts.ContactsPhoneHolderPresenterI;
import com.jakewharton.rxbinding.view.RxView;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by mbp on 12/18/17.
 */

public abstract class BaseContactsPhoneHolder extends RecyclerView.ViewHolder implements ContactsPhoneHolderI {

    protected ContactPhone contact;
    protected ContactsPhoneHolderPresenterI presenterI;

    protected TextView tvName, tvIsAvailable, tvHeader;
    protected LinearLayout llHeader;
    protected CircleImageView ivAvatar;

    public BaseContactsPhoneHolder(View itemView) {
        super(itemView);

        presenterI = bindPresenter();

        bindViews();

        tvName = (TextView) itemView.findViewById(getNameId());
        tvIsAvailable = (TextView) itemView.findViewById(getIsAvailableId());
        llHeader = (LinearLayout) itemView.findViewById(getHeaderLayoutId());
        tvHeader = (TextView) itemView.findViewById(getHeaderTextId());
        ivAvatar = (CircleImageView)itemView.findViewById(getAvatarId());

        RxView.clicks(itemView).subscribe(aVoid -> {
            presenterI.onContactItemClick();
        });

        presenterI.bindView(this);
    }

    public abstract ContactsPhoneHolderPresenterI bindPresenter();

    public abstract void bindViews();

    public abstract int getNameId();

    public abstract int getIsAvailableId();

    public abstract int getHeaderTextId();

    public abstract int getHeaderLayoutId();

    public abstract int getAvatarId();

    public abstract void onClick();

    public void bind(ContactPhone contact) {
        this.contact = contact;

        presenterI.bindContact(contact);
    }

    @Override
    public void setName(String name) {
        tvName.setText(name);
    }

    @Override
    public void setAvailableStatus() {
        tvIsAvailable.setVisibility(View.VISIBLE);
    }

    @Override
    public void setUnavailableStatus() {
        tvIsAvailable.setVisibility(View.GONE);
    }

    @Override
    public void showHeader(String header) {
        tvHeader.setText(header);

        llHeader.setVisibility(View.VISIBLE);
        tvHeader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideHeader() {
        llHeader.setVisibility(View.GONE);
        tvHeader.setVisibility(View.GONE);
    }
}
