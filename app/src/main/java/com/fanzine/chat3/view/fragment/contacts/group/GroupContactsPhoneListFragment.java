package com.fanzine.chat3.view.fragment.contacts.group;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fanzine.coys.R;
import com.fanzine.chat3.model.pojo.ContactPhone;
import com.fanzine.chat3.view.activity.group.GroupNewActivity;
import com.fanzine.chat3.view.adapter.contacts.base.BaseContactsPhoneAdapter;
import com.fanzine.chat3.view.adapter.contacts.group.GroupContactsPhoneAdapter;
import com.fanzine.chat3.view.fragment.OnContactSelectListener;
import com.fanzine.chat3.view.fragment.contacts.base.BaseContactsPhoneListFragment;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbp on 12/20/17.
 */

public class GroupContactsPhoneListFragment extends BaseContactsPhoneListFragment
        implements OnContactSelectListener {

    protected TextView tvParticipantsCount;
    protected TextView btNext;

    protected List<ContactPhone> selectedParticipants = new ArrayList<>();

    public static Fragment newInstance() {
        Fragment fragment = new GroupContactsPhoneListFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        assert view != null;

        tvParticipantsCount = (TextView) view.findViewById(R.id.tvParcipantsCount);
        btNext = (TextView)view.findViewById(R.id.btNext);

        View btCancel = view.findViewById(R.id.fChatList_btEdit);

        btCancel.setOnClickListener(view1 -> {
            getActivity().onBackPressed();
        });

        RxView.clicks(btNext).subscribe(aVoid -> {
            if (selectedParticipants.size() > 1) {
                openNewGroupFragment();
            }
        });

        btNext.setText("Next");

        return view;
    }

    @Override
    protected BaseContactsPhoneAdapter getAdapter(List<ContactPhone> contactsList) {
        return new GroupContactsPhoneAdapter(contactsList, getContext(),
                R.layout.item_chat_phone_contact_multiple, this);
    }

    @Override
    protected int getSearchFieldId() {
        return R.id.etSearch;
    }

    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_chat_new_group_contacts_phone_list;
    }

    @Override
    protected int searchBarId() {
        return R.id.searchBar;
    }

    @Override
    protected int gerRvListId() {
        return R.id.rv;
    }

    @Override
    protected int getProgressBarId() {
        return R.id.progressBar;
    }

    @Override
    public void onSelect(ContactPhone contact) {
        if (!selectedParticipants.contains(contact)) {
            selectedParticipants.add(contact);
        } else {
            selectedParticipants.remove(contact);
        }

        updateParcipantsLabel();
    }

    @Override
    public boolean isSelected(ContactPhone contact) {
        return selectedParticipants.contains(contact);
    }

    @Override
    public void showContacts(List<ContactPhone> contacts) {
        super.showContacts(contacts);

        updateParcipantsLabel();
    }

    @Override
    public void onResume() {
        super.onResume();

        selectedParticipants.clear();
    }

    protected boolean isSufficientNumberOfParticipants() {
        return selectedParticipants.size() > 1;
    }

    @Override
    public void setStatusToNextButton() {
        if ( isSufficientNumberOfParticipants()) {
            btNext.setTextColor((ContextCompat.getColor(getContext(), R.color.white)));
        } else{
            btNext.setTextColor((ContextCompat.getColor(getContext(), R.color.dark_red)));
        }
    }

    private void updateParcipantsLabel() {
        String participantsLabel = selectedParticipants.size() + "/" + getCountOfAllParticipants();

        tvParticipantsCount.setText(participantsLabel);
    }

    private void openNewGroupFragment() {
        Intent startNewGroup = new Intent(getContext(), GroupNewActivity.class);
        startNewGroup.putParcelableArrayListExtra(
                GroupNewActivity.PARTICIPANTS,
                (ArrayList<ContactPhone>) selectedParticipants);

        startActivity(startNewGroup);
    }
}
