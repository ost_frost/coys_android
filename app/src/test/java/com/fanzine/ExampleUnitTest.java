package com.fanzine;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.parser.Tag;
import org.junit.Test;

import static org.junit.Assert.*;


public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }


    @Test
    public void htmlAddHeaderTest() throws Exception {
        String mailContent = "<body><p>Heyo</p></body>";

        Document doc = Jsoup.parse(mailContent, "", Parser.xmlParser());


        Element e = new Element(Tag.valueOf("p"), "jhjjj");
        e.html(doc.select("body").html());

        doc.select("body").html(e.toString());

        System.out.println(doc.toString());
        assertEquals(4, 2 + 2);
    }
}

