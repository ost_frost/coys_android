package com.yelp.fusion.client.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonGetter;

import java.io.Serializable;

/**
 * Created by Ranga on 2/24/2017.
 */

public class Review implements Serializable, Parcelable {

    public Review() {

    }

    protected Review(Parcel in) {
        text = in.readString();
        user = in.readParcelable(User.class.getClassLoader());
        rating = in.readInt();
        timeCreated = in.readString();
        url = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(text);
        dest.writeParcelable(user, flags);
        dest.writeInt(rating);
        dest.writeString(timeCreated);
        dest.writeString(url);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Review> CREATOR = new Creator<Review>() {
        @Override
        public Review createFromParcel(Parcel in) {
            return new Review(in);
        }

        @Override
        public Review[] newArray(int size) {
            return new Review[size];
        }
    };

    @JsonGetter("text")
    public String getText() {
        return this.text;
    }
    public void setText(String text) {
        this.text = text;
    }
    String text;

    @JsonGetter("user")
    public User getUser() {
        return this.user;
    }
    public void setUser(User user) {
        this.user = user;
    }
    User user;

    @JsonGetter("rating")
    public int getRating() {
        return this.rating;
    }
    public void setRating(int rating) {
        this.rating = rating;
    }
    int rating;

    @JsonGetter("time_created")
    public String getTimeCreated() {
        return this.timeCreated;
    }
    public void setTimeCreated(String timeCreated) {
        this.timeCreated = timeCreated;
    }
    String timeCreated;

    @JsonGetter("url")
    public String getUrl() {
        return this.url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    String url;
}
