package com.yelp.fusion.client.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonGetter;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ranga on 2/24/2017.
 */

public class Hour implements Serializable, Parcelable {

    public Hour() {}

    protected Hour(Parcel in) {
        isOpenNow = in.readByte() != 0;
        hoursType = in.readString();
    }

    public static final Creator<Hour> CREATOR = new Creator<Hour>() {
        @Override
        public Hour createFromParcel(Parcel in) {
            return new Hour(in);
        }

        @Override
        public Hour[] newArray(int size) {
            return new Hour[size];
        }
    };

    @JsonGetter("is_open_now")
    public boolean getIsOpenNow() {
        return this.isOpenNow;
    }
    public void setIsOpenNow(boolean isOpenNow) {
        this.isOpenNow = isOpenNow;
    }
    boolean isOpenNow;

    @JsonGetter("hours_type")
    public String getHoursType() {
        return this.hoursType;
    }
    public void setHoursType(String hoursType) {
        this.hoursType = hoursType;
    }
    String hoursType;

    @JsonGetter("open")
    public ArrayList<Open> getOpen() {
        return this.open;
    }
    public void setOpen(ArrayList<Open> open) {
        this.open = open;
    }
    ArrayList<Open> open;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (isOpenNow ? 1 : 0));
        dest.writeString(hoursType);
    }
}
